create table CLab_Prj_Project (
	uuid_ VARCHAR(75) null,
	projectId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	identifier VARCHAR(75) null,
	name VARCHAR(75) null,
	description VARCHAR(75) null,
	projectType INTEGER
);

create table CLab_Prj_ProjectDelivery (
	uuid_ VARCHAR(75) null,
	deliveryId LONG not null primary key,
	stepId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	name VARCHAR(75) null,
	type_ INTEGER,
	phase VARCHAR(75) null,
	domain VARCHAR(75) null,
	status INTEGER,
	startDate DATE null,
	endDate DATE null,
	cost INTEGER,
	outcomeCategory INTEGER,
	comment_ VARCHAR(75) null,
	isAvailable BOOLEAN,
	reviewerIsCompleted BOOLEAN,
	reviewerRate INTEGER,
	reviewerComment VARCHAR(75) null,
	fundingSource INTEGER,
	isEditPlanning BOOLEAN,
	budget DOUBLE,
	personResponsible VARCHAR(75) null,
	startPlanningDate DATE null,
	endPlanningDate DATE null
);

create table CLab_Prj_ProjectDomain (
	uuid_ VARCHAR(75) null,
	projectDomainId LONG not null primary key,
	projectStructureId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	identifier VARCHAR(75) null,
	name VARCHAR(75) null,
	description VARCHAR(75) null,
	priority LONG
);

create table CLab_Prj_ProjectPhase (
	uuid_ VARCHAR(75) null,
	projectPhaseId LONG not null primary key,
	projectStructureId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	identifier VARCHAR(75) null,
	name VARCHAR(75) null,
	description VARCHAR(75) null,
	priority LONG
);

create table CLab_Prj_ProjectStep (
	uuid_ VARCHAR(75) null,
	projectStepId LONG not null primary key,
	projectId LONG,
	projectDomainId LONG,
	projectPhaseId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	percentage LONG
);

create table CLab_Prj_ProjectStructure (
	uuid_ VARCHAR(75) null,
	projectStructureId LONG not null primary key,
	projectId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	identifier VARCHAR(75) null,
	name VARCHAR(75) null,
	description VARCHAR(75) null
);