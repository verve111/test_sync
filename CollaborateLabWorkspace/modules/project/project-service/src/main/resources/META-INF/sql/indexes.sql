create index IX_8A4183F5 on CLab_Prj_Project (companyId);
create index IX_72A98214 on CLab_Prj_Project (groupId, identifier[$COLUMN_LENGTH:75$]);
create index IX_BD54EE36 on CLab_Prj_Project (groupId, name[$COLUMN_LENGTH:75$]);
create index IX_9A671187 on CLab_Prj_Project (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_B3C44DC9 on CLab_Prj_Project (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_E55CC460 on CLab_Prj_ProjectDelivery (companyId, name[$COLUMN_LENGTH:75$]);
create index IX_E8BCDB1C on CLab_Prj_ProjectDelivery (companyId, stepId);
create index IX_6530022 on CLab_Prj_ProjectDelivery (stepId);
create index IX_F2DE229B on CLab_Prj_ProjectDelivery (uuid_[$COLUMN_LENGTH:75$], companyId);

create index IX_64B9B2AE on CLab_Prj_ProjectDomain (companyId, identifier[$COLUMN_LENGTH:75$]);
create index IX_C9141A50 on CLab_Prj_ProjectDomain (companyId, name[$COLUMN_LENGTH:75$]);
create index IX_AAC6211A on CLab_Prj_ProjectDomain (companyId, projectStructureId);
create index IX_CC222AD1 on CLab_Prj_ProjectDomain (projectDomainId);
create index IX_F7E9581D on CLab_Prj_ProjectDomain (projectStructureId, identifier[$COLUMN_LENGTH:75$]);
create index IX_86118AAB on CLab_Prj_ProjectDomain (uuid_[$COLUMN_LENGTH:75$], companyId);

create index IX_492CC691 on CLab_Prj_ProjectPhase (companyId, identifier[$COLUMN_LENGTH:75$]);
create index IX_BF7DFCC5 on CLab_Prj_ProjectPhase (projectPhaseId);
create index IX_755AF39A on CLab_Prj_ProjectPhase (projectStructureId, identifier[$COLUMN_LENGTH:75$]);
create index IX_FE454E8 on CLab_Prj_ProjectPhase (uuid_[$COLUMN_LENGTH:75$], companyId);

create index IX_7015475A on CLab_Prj_ProjectStep (projectDomainId, projectPhaseId);
create index IX_D7127434 on CLab_Prj_ProjectStep (projectId, projectDomainId, projectPhaseId);
create index IX_528CBA76 on CLab_Prj_ProjectStep (projectId, projectPhaseId);
create index IX_AE0C6FF3 on CLab_Prj_ProjectStep (uuid_[$COLUMN_LENGTH:75$], companyId);

create index IX_83422B19 on CLab_Prj_ProjectStructure (companyId, identifier[$COLUMN_LENGTH:75$]);
create index IX_33CDFEFB on CLab_Prj_ProjectStructure (companyId, name[$COLUMN_LENGTH:75$]);
create index IX_54CF2CD8 on CLab_Prj_ProjectStructure (projectId);
create index IX_72943B60 on CLab_Prj_ProjectStructure (uuid_[$COLUMN_LENGTH:75$], companyId);