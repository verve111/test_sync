/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.service.impl;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.project.service.base.ProjectStructureServiceBaseImpl;

/**
 * The implementation of the project structure remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.collaboratelab.project.service.ProjectStructureService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Claudio Carlenzoli
 * @see ProjectStructureServiceBaseImpl
 * @see com.collaboratelab.project.service.ProjectStructureServiceUtil
 */
@ProviderType
public class ProjectStructureServiceImpl extends ProjectStructureServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link com.collaboratelab.project.service.ProjectStructureServiceUtil} to access the project structure remote service.
	 */
}