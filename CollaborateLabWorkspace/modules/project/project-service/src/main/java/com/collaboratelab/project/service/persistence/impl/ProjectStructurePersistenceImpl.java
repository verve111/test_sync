/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.project.exception.NoSuchProjectStructureException;
import com.collaboratelab.project.model.ProjectStructure;
import com.collaboratelab.project.model.impl.ProjectStructureImpl;
import com.collaboratelab.project.model.impl.ProjectStructureModelImpl;
import com.collaboratelab.project.service.persistence.ProjectStructurePersistence;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.CompanyProvider;
import com.liferay.portal.kernel.service.persistence.CompanyProviderWrapper;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the project structure service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Claudio Carlenzoli
 * @see ProjectStructurePersistence
 * @see com.collaboratelab.project.service.persistence.ProjectStructureUtil
 * @generated
 */
@ProviderType
public class ProjectStructurePersistenceImpl extends BasePersistenceImpl<ProjectStructure>
	implements ProjectStructurePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ProjectStructureUtil} to access the project structure persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ProjectStructureImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ProjectStructureModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStructureModelImpl.FINDER_CACHE_ENABLED,
			ProjectStructureImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ProjectStructureModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStructureModelImpl.FINDER_CACHE_ENABLED,
			ProjectStructureImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ProjectStructureModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStructureModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID = new FinderPath(ProjectStructureModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStructureModelImpl.FINDER_CACHE_ENABLED,
			ProjectStructureImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByUuid",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID = new FinderPath(ProjectStructureModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStructureModelImpl.FINDER_CACHE_ENABLED,
			ProjectStructureImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] { String.class.getName() },
			ProjectStructureModelImpl.UUID_COLUMN_BITMASK |
			ProjectStructureModelImpl.NAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID = new FinderPath(ProjectStructureModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStructureModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] { String.class.getName() });

	/**
	 * Returns all the project structures where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching project structures
	 */
	@Override
	public List<ProjectStructure> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the project structures where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of project structures
	 * @param end the upper bound of the range of project structures (not inclusive)
	 * @return the range of matching project structures
	 */
	@Override
	public List<ProjectStructure> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the project structures where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of project structures
	 * @param end the upper bound of the range of project structures (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching project structures
	 */
	@Override
	public List<ProjectStructure> findByUuid(String uuid, int start, int end,
		OrderByComparator<ProjectStructure> orderByComparator) {
		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the project structures where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of project structures
	 * @param end the upper bound of the range of project structures (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching project structures
	 */
	@Override
	public List<ProjectStructure> findByUuid(String uuid, int start, int end,
		OrderByComparator<ProjectStructure> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid, start, end, orderByComparator };
		}

		List<ProjectStructure> list = null;

		if (retrieveFromCache) {
			list = (List<ProjectStructure>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ProjectStructure projectStructure : list) {
					if (!Objects.equals(uuid, projectStructure.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PROJECTSTRUCTURE_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProjectStructureModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<ProjectStructure>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ProjectStructure>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first project structure in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project structure
	 * @throws NoSuchProjectStructureException if a matching project structure could not be found
	 */
	@Override
	public ProjectStructure findByUuid_First(String uuid,
		OrderByComparator<ProjectStructure> orderByComparator)
		throws NoSuchProjectStructureException {
		ProjectStructure projectStructure = fetchByUuid_First(uuid,
				orderByComparator);

		if (projectStructure != null) {
			return projectStructure;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectStructureException(msg.toString());
	}

	/**
	 * Returns the first project structure in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project structure, or <code>null</code> if a matching project structure could not be found
	 */
	@Override
	public ProjectStructure fetchByUuid_First(String uuid,
		OrderByComparator<ProjectStructure> orderByComparator) {
		List<ProjectStructure> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last project structure in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project structure
	 * @throws NoSuchProjectStructureException if a matching project structure could not be found
	 */
	@Override
	public ProjectStructure findByUuid_Last(String uuid,
		OrderByComparator<ProjectStructure> orderByComparator)
		throws NoSuchProjectStructureException {
		ProjectStructure projectStructure = fetchByUuid_Last(uuid,
				orderByComparator);

		if (projectStructure != null) {
			return projectStructure;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectStructureException(msg.toString());
	}

	/**
	 * Returns the last project structure in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project structure, or <code>null</code> if a matching project structure could not be found
	 */
	@Override
	public ProjectStructure fetchByUuid_Last(String uuid,
		OrderByComparator<ProjectStructure> orderByComparator) {
		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<ProjectStructure> list = findByUuid(uuid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the project structures before and after the current project structure in the ordered set where uuid = &#63;.
	 *
	 * @param projectStructureId the primary key of the current project structure
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next project structure
	 * @throws NoSuchProjectStructureException if a project structure with the primary key could not be found
	 */
	@Override
	public ProjectStructure[] findByUuid_PrevAndNext(long projectStructureId,
		String uuid, OrderByComparator<ProjectStructure> orderByComparator)
		throws NoSuchProjectStructureException {
		ProjectStructure projectStructure = findByPrimaryKey(projectStructureId);

		Session session = null;

		try {
			session = openSession();

			ProjectStructure[] array = new ProjectStructureImpl[3];

			array[0] = getByUuid_PrevAndNext(session, projectStructure, uuid,
					orderByComparator, true);

			array[1] = projectStructure;

			array[2] = getByUuid_PrevAndNext(session, projectStructure, uuid,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ProjectStructure getByUuid_PrevAndNext(Session session,
		ProjectStructure projectStructure, String uuid,
		OrderByComparator<ProjectStructure> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PROJECTSTRUCTURE_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProjectStructureModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(projectStructure);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ProjectStructure> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the project structures where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (ProjectStructure projectStructure : findByUuid(uuid,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(projectStructure);
		}
	}

	/**
	 * Returns the number of project structures where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching project structures
	 */
	@Override
	public int countByUuid(String uuid) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID;

		Object[] finderArgs = new Object[] { uuid };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PROJECTSTRUCTURE_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_1 = "projectStructure.uuid IS NULL";
	private static final String _FINDER_COLUMN_UUID_UUID_2 = "projectStructure.uuid = ?";
	private static final String _FINDER_COLUMN_UUID_UUID_3 = "(projectStructure.uuid IS NULL OR projectStructure.uuid = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C = new FinderPath(ProjectStructureModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStructureModelImpl.FINDER_CACHE_ENABLED,
			ProjectStructureImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C =
		new FinderPath(ProjectStructureModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStructureModelImpl.FINDER_CACHE_ENABLED,
			ProjectStructureImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() },
			ProjectStructureModelImpl.UUID_COLUMN_BITMASK |
			ProjectStructureModelImpl.COMPANYID_COLUMN_BITMASK |
			ProjectStructureModelImpl.NAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_C = new FinderPath(ProjectStructureModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStructureModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns all the project structures where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching project structures
	 */
	@Override
	public List<ProjectStructure> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(uuid, companyId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the project structures where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of project structures
	 * @param end the upper bound of the range of project structures (not inclusive)
	 * @return the range of matching project structures
	 */
	@Override
	public List<ProjectStructure> findByUuid_C(String uuid, long companyId,
		int start, int end) {
		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the project structures where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of project structures
	 * @param end the upper bound of the range of project structures (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching project structures
	 */
	@Override
	public List<ProjectStructure> findByUuid_C(String uuid, long companyId,
		int start, int end,
		OrderByComparator<ProjectStructure> orderByComparator) {
		return findByUuid_C(uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the project structures where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of project structures
	 * @param end the upper bound of the range of project structures (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching project structures
	 */
	@Override
	public List<ProjectStructure> findByUuid_C(String uuid, long companyId,
		int start, int end,
		OrderByComparator<ProjectStructure> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] { uuid, companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] {
					uuid, companyId,
					
					start, end, orderByComparator
				};
		}

		List<ProjectStructure> list = null;

		if (retrieveFromCache) {
			list = (List<ProjectStructure>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ProjectStructure projectStructure : list) {
					if (!Objects.equals(uuid, projectStructure.getUuid()) ||
							(companyId != projectStructure.getCompanyId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_PROJECTSTRUCTURE_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProjectStructureModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				if (!pagination) {
					list = (List<ProjectStructure>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ProjectStructure>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first project structure in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project structure
	 * @throws NoSuchProjectStructureException if a matching project structure could not be found
	 */
	@Override
	public ProjectStructure findByUuid_C_First(String uuid, long companyId,
		OrderByComparator<ProjectStructure> orderByComparator)
		throws NoSuchProjectStructureException {
		ProjectStructure projectStructure = fetchByUuid_C_First(uuid,
				companyId, orderByComparator);

		if (projectStructure != null) {
			return projectStructure;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectStructureException(msg.toString());
	}

	/**
	 * Returns the first project structure in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project structure, or <code>null</code> if a matching project structure could not be found
	 */
	@Override
	public ProjectStructure fetchByUuid_C_First(String uuid, long companyId,
		OrderByComparator<ProjectStructure> orderByComparator) {
		List<ProjectStructure> list = findByUuid_C(uuid, companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last project structure in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project structure
	 * @throws NoSuchProjectStructureException if a matching project structure could not be found
	 */
	@Override
	public ProjectStructure findByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<ProjectStructure> orderByComparator)
		throws NoSuchProjectStructureException {
		ProjectStructure projectStructure = fetchByUuid_C_Last(uuid, companyId,
				orderByComparator);

		if (projectStructure != null) {
			return projectStructure;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectStructureException(msg.toString());
	}

	/**
	 * Returns the last project structure in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project structure, or <code>null</code> if a matching project structure could not be found
	 */
	@Override
	public ProjectStructure fetchByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<ProjectStructure> orderByComparator) {
		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<ProjectStructure> list = findByUuid_C(uuid, companyId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the project structures before and after the current project structure in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param projectStructureId the primary key of the current project structure
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next project structure
	 * @throws NoSuchProjectStructureException if a project structure with the primary key could not be found
	 */
	@Override
	public ProjectStructure[] findByUuid_C_PrevAndNext(
		long projectStructureId, String uuid, long companyId,
		OrderByComparator<ProjectStructure> orderByComparator)
		throws NoSuchProjectStructureException {
		ProjectStructure projectStructure = findByPrimaryKey(projectStructureId);

		Session session = null;

		try {
			session = openSession();

			ProjectStructure[] array = new ProjectStructureImpl[3];

			array[0] = getByUuid_C_PrevAndNext(session, projectStructure, uuid,
					companyId, orderByComparator, true);

			array[1] = projectStructure;

			array[2] = getByUuid_C_PrevAndNext(session, projectStructure, uuid,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ProjectStructure getByUuid_C_PrevAndNext(Session session,
		ProjectStructure projectStructure, String uuid, long companyId,
		OrderByComparator<ProjectStructure> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_PROJECTSTRUCTURE_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProjectStructureModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(projectStructure);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ProjectStructure> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the project structures where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (ProjectStructure projectStructure : findByUuid_C(uuid, companyId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(projectStructure);
		}
	}

	/**
	 * Returns the number of project structures where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching project structures
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_C;

		Object[] finderArgs = new Object[] { uuid, companyId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_PROJECTSTRUCTURE_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_1 = "projectStructure.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_2 = "projectStructure.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_3 = "(projectStructure.uuid IS NULL OR projectStructure.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 = "projectStructure.companyId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYID =
		new FinderPath(ProjectStructureModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStructureModelImpl.FINDER_CACHE_ENABLED,
			ProjectStructureImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByCompanyId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID =
		new FinderPath(ProjectStructureModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStructureModelImpl.FINDER_CACHE_ENABLED,
			ProjectStructureImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCompanyId",
			new String[] { Long.class.getName() },
			ProjectStructureModelImpl.COMPANYID_COLUMN_BITMASK |
			ProjectStructureModelImpl.NAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANYID = new FinderPath(ProjectStructureModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStructureModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCompanyId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the project structures where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching project structures
	 */
	@Override
	public List<ProjectStructure> findByCompanyId(long companyId) {
		return findByCompanyId(companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the project structures where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of project structures
	 * @param end the upper bound of the range of project structures (not inclusive)
	 * @return the range of matching project structures
	 */
	@Override
	public List<ProjectStructure> findByCompanyId(long companyId, int start,
		int end) {
		return findByCompanyId(companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the project structures where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of project structures
	 * @param end the upper bound of the range of project structures (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching project structures
	 */
	@Override
	public List<ProjectStructure> findByCompanyId(long companyId, int start,
		int end, OrderByComparator<ProjectStructure> orderByComparator) {
		return findByCompanyId(companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the project structures where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of project structures
	 * @param end the upper bound of the range of project structures (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching project structures
	 */
	@Override
	public List<ProjectStructure> findByCompanyId(long companyId, int start,
		int end, OrderByComparator<ProjectStructure> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID;
			finderArgs = new Object[] { companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYID;
			finderArgs = new Object[] { companyId, start, end, orderByComparator };
		}

		List<ProjectStructure> list = null;

		if (retrieveFromCache) {
			list = (List<ProjectStructure>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ProjectStructure projectStructure : list) {
					if ((companyId != projectStructure.getCompanyId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PROJECTSTRUCTURE_WHERE);

			query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProjectStructureModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (!pagination) {
					list = (List<ProjectStructure>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ProjectStructure>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first project structure in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project structure
	 * @throws NoSuchProjectStructureException if a matching project structure could not be found
	 */
	@Override
	public ProjectStructure findByCompanyId_First(long companyId,
		OrderByComparator<ProjectStructure> orderByComparator)
		throws NoSuchProjectStructureException {
		ProjectStructure projectStructure = fetchByCompanyId_First(companyId,
				orderByComparator);

		if (projectStructure != null) {
			return projectStructure;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectStructureException(msg.toString());
	}

	/**
	 * Returns the first project structure in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project structure, or <code>null</code> if a matching project structure could not be found
	 */
	@Override
	public ProjectStructure fetchByCompanyId_First(long companyId,
		OrderByComparator<ProjectStructure> orderByComparator) {
		List<ProjectStructure> list = findByCompanyId(companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last project structure in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project structure
	 * @throws NoSuchProjectStructureException if a matching project structure could not be found
	 */
	@Override
	public ProjectStructure findByCompanyId_Last(long companyId,
		OrderByComparator<ProjectStructure> orderByComparator)
		throws NoSuchProjectStructureException {
		ProjectStructure projectStructure = fetchByCompanyId_Last(companyId,
				orderByComparator);

		if (projectStructure != null) {
			return projectStructure;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectStructureException(msg.toString());
	}

	/**
	 * Returns the last project structure in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project structure, or <code>null</code> if a matching project structure could not be found
	 */
	@Override
	public ProjectStructure fetchByCompanyId_Last(long companyId,
		OrderByComparator<ProjectStructure> orderByComparator) {
		int count = countByCompanyId(companyId);

		if (count == 0) {
			return null;
		}

		List<ProjectStructure> list = findByCompanyId(companyId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the project structures before and after the current project structure in the ordered set where companyId = &#63;.
	 *
	 * @param projectStructureId the primary key of the current project structure
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next project structure
	 * @throws NoSuchProjectStructureException if a project structure with the primary key could not be found
	 */
	@Override
	public ProjectStructure[] findByCompanyId_PrevAndNext(
		long projectStructureId, long companyId,
		OrderByComparator<ProjectStructure> orderByComparator)
		throws NoSuchProjectStructureException {
		ProjectStructure projectStructure = findByPrimaryKey(projectStructureId);

		Session session = null;

		try {
			session = openSession();

			ProjectStructure[] array = new ProjectStructureImpl[3];

			array[0] = getByCompanyId_PrevAndNext(session, projectStructure,
					companyId, orderByComparator, true);

			array[1] = projectStructure;

			array[2] = getByCompanyId_PrevAndNext(session, projectStructure,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ProjectStructure getByCompanyId_PrevAndNext(Session session,
		ProjectStructure projectStructure, long companyId,
		OrderByComparator<ProjectStructure> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PROJECTSTRUCTURE_WHERE);

		query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProjectStructureModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(projectStructure);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ProjectStructure> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the project structures where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 */
	@Override
	public void removeByCompanyId(long companyId) {
		for (ProjectStructure projectStructure : findByCompanyId(companyId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(projectStructure);
		}
	}

	/**
	 * Returns the number of project structures where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching project structures
	 */
	@Override
	public int countByCompanyId(long companyId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_COMPANYID;

		Object[] finderArgs = new Object[] { companyId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PROJECTSTRUCTURE_WHERE);

			query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COMPANYID_COMPANYID_2 = "projectStructure.companyId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER = new FinderPath(ProjectStructureModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStructureModelImpl.FINDER_CACHE_ENABLED,
			ProjectStructureImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByCompanyIdIdentifier",
			new String[] { Long.class.getName(), String.class.getName() },
			ProjectStructureModelImpl.COMPANYID_COLUMN_BITMASK |
			ProjectStructureModelImpl.IDENTIFIER_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANYIDIDENTIFIER = new FinderPath(ProjectStructureModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStructureModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByCompanyIdIdentifier",
			new String[] { Long.class.getName(), String.class.getName() });

	/**
	 * Returns the project structure where companyId = &#63; and identifier = &#63; or throws a {@link NoSuchProjectStructureException} if it could not be found.
	 *
	 * @param companyId the company ID
	 * @param identifier the identifier
	 * @return the matching project structure
	 * @throws NoSuchProjectStructureException if a matching project structure could not be found
	 */
	@Override
	public ProjectStructure findByCompanyIdIdentifier(long companyId,
		String identifier) throws NoSuchProjectStructureException {
		ProjectStructure projectStructure = fetchByCompanyIdIdentifier(companyId,
				identifier);

		if (projectStructure == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("companyId=");
			msg.append(companyId);

			msg.append(", identifier=");
			msg.append(identifier);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchProjectStructureException(msg.toString());
		}

		return projectStructure;
	}

	/**
	 * Returns the project structure where companyId = &#63; and identifier = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param companyId the company ID
	 * @param identifier the identifier
	 * @return the matching project structure, or <code>null</code> if a matching project structure could not be found
	 */
	@Override
	public ProjectStructure fetchByCompanyIdIdentifier(long companyId,
		String identifier) {
		return fetchByCompanyIdIdentifier(companyId, identifier, true);
	}

	/**
	 * Returns the project structure where companyId = &#63; and identifier = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param companyId the company ID
	 * @param identifier the identifier
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching project structure, or <code>null</code> if a matching project structure could not be found
	 */
	@Override
	public ProjectStructure fetchByCompanyIdIdentifier(long companyId,
		String identifier, boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { companyId, identifier };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER,
					finderArgs, this);
		}

		if (result instanceof ProjectStructure) {
			ProjectStructure projectStructure = (ProjectStructure)result;

			if ((companyId != projectStructure.getCompanyId()) ||
					!Objects.equals(identifier, projectStructure.getIdentifier())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_PROJECTSTRUCTURE_WHERE);

			query.append(_FINDER_COLUMN_COMPANYIDIDENTIFIER_COMPANYID_2);

			boolean bindIdentifier = false;

			if (identifier == null) {
				query.append(_FINDER_COLUMN_COMPANYIDIDENTIFIER_IDENTIFIER_1);
			}
			else if (identifier.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_COMPANYIDIDENTIFIER_IDENTIFIER_3);
			}
			else {
				bindIdentifier = true;

				query.append(_FINDER_COLUMN_COMPANYIDIDENTIFIER_IDENTIFIER_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (bindIdentifier) {
					qPos.add(identifier);
				}

				List<ProjectStructure> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER,
						finderArgs, list);
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							_log.warn(
								"ProjectStructurePersistenceImpl.fetchByCompanyIdIdentifier(long, String, boolean) with parameters (" +
								StringUtil.merge(finderArgs) +
								") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					ProjectStructure projectStructure = list.get(0);

					result = projectStructure;

					cacheResult(projectStructure);

					if ((projectStructure.getCompanyId() != companyId) ||
							(projectStructure.getIdentifier() == null) ||
							!projectStructure.getIdentifier().equals(identifier)) {
						finderCache.putResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER,
							finderArgs, projectStructure);
					}
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (ProjectStructure)result;
		}
	}

	/**
	 * Removes the project structure where companyId = &#63; and identifier = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param identifier the identifier
	 * @return the project structure that was removed
	 */
	@Override
	public ProjectStructure removeByCompanyIdIdentifier(long companyId,
		String identifier) throws NoSuchProjectStructureException {
		ProjectStructure projectStructure = findByCompanyIdIdentifier(companyId,
				identifier);

		return remove(projectStructure);
	}

	/**
	 * Returns the number of project structures where companyId = &#63; and identifier = &#63;.
	 *
	 * @param companyId the company ID
	 * @param identifier the identifier
	 * @return the number of matching project structures
	 */
	@Override
	public int countByCompanyIdIdentifier(long companyId, String identifier) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_COMPANYIDIDENTIFIER;

		Object[] finderArgs = new Object[] { companyId, identifier };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_PROJECTSTRUCTURE_WHERE);

			query.append(_FINDER_COLUMN_COMPANYIDIDENTIFIER_COMPANYID_2);

			boolean bindIdentifier = false;

			if (identifier == null) {
				query.append(_FINDER_COLUMN_COMPANYIDIDENTIFIER_IDENTIFIER_1);
			}
			else if (identifier.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_COMPANYIDIDENTIFIER_IDENTIFIER_3);
			}
			else {
				bindIdentifier = true;

				query.append(_FINDER_COLUMN_COMPANYIDIDENTIFIER_IDENTIFIER_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (bindIdentifier) {
					qPos.add(identifier);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COMPANYIDIDENTIFIER_COMPANYID_2 = "projectStructure.companyId = ? AND ";
	private static final String _FINDER_COLUMN_COMPANYIDIDENTIFIER_IDENTIFIER_1 = "projectStructure.identifier IS NULL";
	private static final String _FINDER_COLUMN_COMPANYIDIDENTIFIER_IDENTIFIER_2 = "projectStructure.identifier = ?";
	private static final String _FINDER_COLUMN_COMPANYIDIDENTIFIER_IDENTIFIER_3 = "(projectStructure.identifier IS NULL OR projectStructure.identifier = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYIDINCLUDENAME =
		new FinderPath(ProjectStructureModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStructureModelImpl.FINDER_CACHE_ENABLED,
			ProjectStructureImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByCompanyIdIncludeName",
			new String[] {
				Long.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_COMPANYIDINCLUDENAME =
		new FinderPath(ProjectStructureModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStructureModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"countByCompanyIdIncludeName",
			new String[] { Long.class.getName(), String.class.getName() });

	/**
	 * Returns all the project structures where companyId = &#63; and name LIKE &#63;.
	 *
	 * @param companyId the company ID
	 * @param name the name
	 * @return the matching project structures
	 */
	@Override
	public List<ProjectStructure> findByCompanyIdIncludeName(long companyId,
		String name) {
		return findByCompanyIdIncludeName(companyId, name, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the project structures where companyId = &#63; and name LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param name the name
	 * @param start the lower bound of the range of project structures
	 * @param end the upper bound of the range of project structures (not inclusive)
	 * @return the range of matching project structures
	 */
	@Override
	public List<ProjectStructure> findByCompanyIdIncludeName(long companyId,
		String name, int start, int end) {
		return findByCompanyIdIncludeName(companyId, name, start, end, null);
	}

	/**
	 * Returns an ordered range of all the project structures where companyId = &#63; and name LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param name the name
	 * @param start the lower bound of the range of project structures
	 * @param end the upper bound of the range of project structures (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching project structures
	 */
	@Override
	public List<ProjectStructure> findByCompanyIdIncludeName(long companyId,
		String name, int start, int end,
		OrderByComparator<ProjectStructure> orderByComparator) {
		return findByCompanyIdIncludeName(companyId, name, start, end,
			orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the project structures where companyId = &#63; and name LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param name the name
	 * @param start the lower bound of the range of project structures
	 * @param end the upper bound of the range of project structures (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching project structures
	 */
	@Override
	public List<ProjectStructure> findByCompanyIdIncludeName(long companyId,
		String name, int start, int end,
		OrderByComparator<ProjectStructure> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYIDINCLUDENAME;
		finderArgs = new Object[] { companyId, name, start, end, orderByComparator };

		List<ProjectStructure> list = null;

		if (retrieveFromCache) {
			list = (List<ProjectStructure>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ProjectStructure projectStructure : list) {
					if ((companyId != projectStructure.getCompanyId()) ||
							!StringUtil.wildcardMatches(
								projectStructure.getName(), name,
								CharPool.UNDERLINE, CharPool.PERCENT,
								CharPool.BACK_SLASH, true)) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_PROJECTSTRUCTURE_WHERE);

			query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_COMPANYID_2);

			boolean bindName = false;

			if (name == null) {
				query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_1);
			}
			else if (name.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_3);
			}
			else {
				bindName = true;

				query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProjectStructureModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (bindName) {
					qPos.add(name);
				}

				if (!pagination) {
					list = (List<ProjectStructure>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ProjectStructure>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first project structure in the ordered set where companyId = &#63; and name LIKE &#63;.
	 *
	 * @param companyId the company ID
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project structure
	 * @throws NoSuchProjectStructureException if a matching project structure could not be found
	 */
	@Override
	public ProjectStructure findByCompanyIdIncludeName_First(long companyId,
		String name, OrderByComparator<ProjectStructure> orderByComparator)
		throws NoSuchProjectStructureException {
		ProjectStructure projectStructure = fetchByCompanyIdIncludeName_First(companyId,
				name, orderByComparator);

		if (projectStructure != null) {
			return projectStructure;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", name=");
		msg.append(name);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectStructureException(msg.toString());
	}

	/**
	 * Returns the first project structure in the ordered set where companyId = &#63; and name LIKE &#63;.
	 *
	 * @param companyId the company ID
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project structure, or <code>null</code> if a matching project structure could not be found
	 */
	@Override
	public ProjectStructure fetchByCompanyIdIncludeName_First(long companyId,
		String name, OrderByComparator<ProjectStructure> orderByComparator) {
		List<ProjectStructure> list = findByCompanyIdIncludeName(companyId,
				name, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last project structure in the ordered set where companyId = &#63; and name LIKE &#63;.
	 *
	 * @param companyId the company ID
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project structure
	 * @throws NoSuchProjectStructureException if a matching project structure could not be found
	 */
	@Override
	public ProjectStructure findByCompanyIdIncludeName_Last(long companyId,
		String name, OrderByComparator<ProjectStructure> orderByComparator)
		throws NoSuchProjectStructureException {
		ProjectStructure projectStructure = fetchByCompanyIdIncludeName_Last(companyId,
				name, orderByComparator);

		if (projectStructure != null) {
			return projectStructure;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", name=");
		msg.append(name);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectStructureException(msg.toString());
	}

	/**
	 * Returns the last project structure in the ordered set where companyId = &#63; and name LIKE &#63;.
	 *
	 * @param companyId the company ID
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project structure, or <code>null</code> if a matching project structure could not be found
	 */
	@Override
	public ProjectStructure fetchByCompanyIdIncludeName_Last(long companyId,
		String name, OrderByComparator<ProjectStructure> orderByComparator) {
		int count = countByCompanyIdIncludeName(companyId, name);

		if (count == 0) {
			return null;
		}

		List<ProjectStructure> list = findByCompanyIdIncludeName(companyId,
				name, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the project structures before and after the current project structure in the ordered set where companyId = &#63; and name LIKE &#63;.
	 *
	 * @param projectStructureId the primary key of the current project structure
	 * @param companyId the company ID
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next project structure
	 * @throws NoSuchProjectStructureException if a project structure with the primary key could not be found
	 */
	@Override
	public ProjectStructure[] findByCompanyIdIncludeName_PrevAndNext(
		long projectStructureId, long companyId, String name,
		OrderByComparator<ProjectStructure> orderByComparator)
		throws NoSuchProjectStructureException {
		ProjectStructure projectStructure = findByPrimaryKey(projectStructureId);

		Session session = null;

		try {
			session = openSession();

			ProjectStructure[] array = new ProjectStructureImpl[3];

			array[0] = getByCompanyIdIncludeName_PrevAndNext(session,
					projectStructure, companyId, name, orderByComparator, true);

			array[1] = projectStructure;

			array[2] = getByCompanyIdIncludeName_PrevAndNext(session,
					projectStructure, companyId, name, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ProjectStructure getByCompanyIdIncludeName_PrevAndNext(
		Session session, ProjectStructure projectStructure, long companyId,
		String name, OrderByComparator<ProjectStructure> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_PROJECTSTRUCTURE_WHERE);

		query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_COMPANYID_2);

		boolean bindName = false;

		if (name == null) {
			query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_1);
		}
		else if (name.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_3);
		}
		else {
			bindName = true;

			query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProjectStructureModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (bindName) {
			qPos.add(name);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(projectStructure);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ProjectStructure> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the project structures where companyId = &#63; and name LIKE &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param name the name
	 */
	@Override
	public void removeByCompanyIdIncludeName(long companyId, String name) {
		for (ProjectStructure projectStructure : findByCompanyIdIncludeName(
				companyId, name, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(projectStructure);
		}
	}

	/**
	 * Returns the number of project structures where companyId = &#63; and name LIKE &#63;.
	 *
	 * @param companyId the company ID
	 * @param name the name
	 * @return the number of matching project structures
	 */
	@Override
	public int countByCompanyIdIncludeName(long companyId, String name) {
		FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_COMPANYIDINCLUDENAME;

		Object[] finderArgs = new Object[] { companyId, name };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_PROJECTSTRUCTURE_WHERE);

			query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_COMPANYID_2);

			boolean bindName = false;

			if (name == null) {
				query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_1);
			}
			else if (name.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_3);
			}
			else {
				bindName = true;

				query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (bindName) {
					qPos.add(name);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COMPANYIDINCLUDENAME_COMPANYID_2 = "projectStructure.companyId = ? AND ";
	private static final String _FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_1 = "projectStructure.name IS NULL";
	private static final String _FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_2 = "projectStructure.name LIKE ?";
	private static final String _FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_3 = "(projectStructure.name IS NULL OR projectStructure.name LIKE '')";
	public static final FinderPath FINDER_PATH_FETCH_BY_PROJECTID = new FinderPath(ProjectStructureModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStructureModelImpl.FINDER_CACHE_ENABLED,
			ProjectStructureImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByProjectId", new String[] { Long.class.getName() },
			ProjectStructureModelImpl.PROJECTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PROJECTID = new FinderPath(ProjectStructureModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStructureModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByProjectId",
			new String[] { Long.class.getName() });

	/**
	 * Returns the project structure where projectId = &#63; or throws a {@link NoSuchProjectStructureException} if it could not be found.
	 *
	 * @param projectId the project ID
	 * @return the matching project structure
	 * @throws NoSuchProjectStructureException if a matching project structure could not be found
	 */
	@Override
	public ProjectStructure findByProjectId(long projectId)
		throws NoSuchProjectStructureException {
		ProjectStructure projectStructure = fetchByProjectId(projectId);

		if (projectStructure == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("projectId=");
			msg.append(projectId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchProjectStructureException(msg.toString());
		}

		return projectStructure;
	}

	/**
	 * Returns the project structure where projectId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param projectId the project ID
	 * @return the matching project structure, or <code>null</code> if a matching project structure could not be found
	 */
	@Override
	public ProjectStructure fetchByProjectId(long projectId) {
		return fetchByProjectId(projectId, true);
	}

	/**
	 * Returns the project structure where projectId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param projectId the project ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching project structure, or <code>null</code> if a matching project structure could not be found
	 */
	@Override
	public ProjectStructure fetchByProjectId(long projectId,
		boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { projectId };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_PROJECTID,
					finderArgs, this);
		}

		if (result instanceof ProjectStructure) {
			ProjectStructure projectStructure = (ProjectStructure)result;

			if ((projectId != projectStructure.getProjectId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_PROJECTSTRUCTURE_WHERE);

			query.append(_FINDER_COLUMN_PROJECTID_PROJECTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(projectId);

				List<ProjectStructure> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_PROJECTID,
						finderArgs, list);
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							_log.warn(
								"ProjectStructurePersistenceImpl.fetchByProjectId(long, boolean) with parameters (" +
								StringUtil.merge(finderArgs) +
								") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					ProjectStructure projectStructure = list.get(0);

					result = projectStructure;

					cacheResult(projectStructure);

					if ((projectStructure.getProjectId() != projectId)) {
						finderCache.putResult(FINDER_PATH_FETCH_BY_PROJECTID,
							finderArgs, projectStructure);
					}
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_PROJECTID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (ProjectStructure)result;
		}
	}

	/**
	 * Removes the project structure where projectId = &#63; from the database.
	 *
	 * @param projectId the project ID
	 * @return the project structure that was removed
	 */
	@Override
	public ProjectStructure removeByProjectId(long projectId)
		throws NoSuchProjectStructureException {
		ProjectStructure projectStructure = findByProjectId(projectId);

		return remove(projectStructure);
	}

	/**
	 * Returns the number of project structures where projectId = &#63;.
	 *
	 * @param projectId the project ID
	 * @return the number of matching project structures
	 */
	@Override
	public int countByProjectId(long projectId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PROJECTID;

		Object[] finderArgs = new Object[] { projectId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PROJECTSTRUCTURE_WHERE);

			query.append(_FINDER_COLUMN_PROJECTID_PROJECTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(projectId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PROJECTID_PROJECTID_2 = "projectStructure.projectId = ?";

	public ProjectStructurePersistenceImpl() {
		setModelClass(ProjectStructure.class);
	}

	/**
	 * Caches the project structure in the entity cache if it is enabled.
	 *
	 * @param projectStructure the project structure
	 */
	@Override
	public void cacheResult(ProjectStructure projectStructure) {
		entityCache.putResult(ProjectStructureModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStructureImpl.class, projectStructure.getPrimaryKey(),
			projectStructure);

		finderCache.putResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER,
			new Object[] {
				projectStructure.getCompanyId(),
				projectStructure.getIdentifier()
			}, projectStructure);

		finderCache.putResult(FINDER_PATH_FETCH_BY_PROJECTID,
			new Object[] { projectStructure.getProjectId() }, projectStructure);

		projectStructure.resetOriginalValues();
	}

	/**
	 * Caches the project structures in the entity cache if it is enabled.
	 *
	 * @param projectStructures the project structures
	 */
	@Override
	public void cacheResult(List<ProjectStructure> projectStructures) {
		for (ProjectStructure projectStructure : projectStructures) {
			if (entityCache.getResult(
						ProjectStructureModelImpl.ENTITY_CACHE_ENABLED,
						ProjectStructureImpl.class,
						projectStructure.getPrimaryKey()) == null) {
				cacheResult(projectStructure);
			}
			else {
				projectStructure.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all project structures.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(ProjectStructureImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the project structure.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ProjectStructure projectStructure) {
		entityCache.removeResult(ProjectStructureModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStructureImpl.class, projectStructure.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache((ProjectStructureModelImpl)projectStructure);
	}

	@Override
	public void clearCache(List<ProjectStructure> projectStructures) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ProjectStructure projectStructure : projectStructures) {
			entityCache.removeResult(ProjectStructureModelImpl.ENTITY_CACHE_ENABLED,
				ProjectStructureImpl.class, projectStructure.getPrimaryKey());

			clearUniqueFindersCache((ProjectStructureModelImpl)projectStructure);
		}
	}

	protected void cacheUniqueFindersCache(
		ProjectStructureModelImpl projectStructureModelImpl, boolean isNew) {
		if (isNew) {
			Object[] args = new Object[] {
					projectStructureModelImpl.getCompanyId(),
					projectStructureModelImpl.getIdentifier()
				};

			finderCache.putResult(FINDER_PATH_COUNT_BY_COMPANYIDIDENTIFIER,
				args, Long.valueOf(1));
			finderCache.putResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER,
				args, projectStructureModelImpl);

			args = new Object[] { projectStructureModelImpl.getProjectId() };

			finderCache.putResult(FINDER_PATH_COUNT_BY_PROJECTID, args,
				Long.valueOf(1));
			finderCache.putResult(FINDER_PATH_FETCH_BY_PROJECTID, args,
				projectStructureModelImpl);
		}
		else {
			if ((projectStructureModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						projectStructureModelImpl.getCompanyId(),
						projectStructureModelImpl.getIdentifier()
					};

				finderCache.putResult(FINDER_PATH_COUNT_BY_COMPANYIDIDENTIFIER,
					args, Long.valueOf(1));
				finderCache.putResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER,
					args, projectStructureModelImpl);
			}

			if ((projectStructureModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_PROJECTID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						projectStructureModelImpl.getProjectId()
					};

				finderCache.putResult(FINDER_PATH_COUNT_BY_PROJECTID, args,
					Long.valueOf(1));
				finderCache.putResult(FINDER_PATH_FETCH_BY_PROJECTID, args,
					projectStructureModelImpl);
			}
		}
	}

	protected void clearUniqueFindersCache(
		ProjectStructureModelImpl projectStructureModelImpl) {
		Object[] args = new Object[] {
				projectStructureModelImpl.getCompanyId(),
				projectStructureModelImpl.getIdentifier()
			};

		finderCache.removeResult(FINDER_PATH_COUNT_BY_COMPANYIDIDENTIFIER, args);
		finderCache.removeResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER, args);

		if ((projectStructureModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER.getColumnBitmask()) != 0) {
			args = new Object[] {
					projectStructureModelImpl.getOriginalCompanyId(),
					projectStructureModelImpl.getOriginalIdentifier()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_COMPANYIDIDENTIFIER,
				args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER,
				args);
		}

		args = new Object[] { projectStructureModelImpl.getProjectId() };

		finderCache.removeResult(FINDER_PATH_COUNT_BY_PROJECTID, args);
		finderCache.removeResult(FINDER_PATH_FETCH_BY_PROJECTID, args);

		if ((projectStructureModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_PROJECTID.getColumnBitmask()) != 0) {
			args = new Object[] { projectStructureModelImpl.getOriginalProjectId() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_PROJECTID, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_PROJECTID, args);
		}
	}

	/**
	 * Creates a new project structure with the primary key. Does not add the project structure to the database.
	 *
	 * @param projectStructureId the primary key for the new project structure
	 * @return the new project structure
	 */
	@Override
	public ProjectStructure create(long projectStructureId) {
		ProjectStructure projectStructure = new ProjectStructureImpl();

		projectStructure.setNew(true);
		projectStructure.setPrimaryKey(projectStructureId);

		String uuid = PortalUUIDUtil.generate();

		projectStructure.setUuid(uuid);

		projectStructure.setCompanyId(companyProvider.getCompanyId());

		return projectStructure;
	}

	/**
	 * Removes the project structure with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param projectStructureId the primary key of the project structure
	 * @return the project structure that was removed
	 * @throws NoSuchProjectStructureException if a project structure with the primary key could not be found
	 */
	@Override
	public ProjectStructure remove(long projectStructureId)
		throws NoSuchProjectStructureException {
		return remove((Serializable)projectStructureId);
	}

	/**
	 * Removes the project structure with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the project structure
	 * @return the project structure that was removed
	 * @throws NoSuchProjectStructureException if a project structure with the primary key could not be found
	 */
	@Override
	public ProjectStructure remove(Serializable primaryKey)
		throws NoSuchProjectStructureException {
		Session session = null;

		try {
			session = openSession();

			ProjectStructure projectStructure = (ProjectStructure)session.get(ProjectStructureImpl.class,
					primaryKey);

			if (projectStructure == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchProjectStructureException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(projectStructure);
		}
		catch (NoSuchProjectStructureException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ProjectStructure removeImpl(ProjectStructure projectStructure) {
		projectStructure = toUnwrappedModel(projectStructure);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(projectStructure)) {
				projectStructure = (ProjectStructure)session.get(ProjectStructureImpl.class,
						projectStructure.getPrimaryKeyObj());
			}

			if (projectStructure != null) {
				session.delete(projectStructure);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (projectStructure != null) {
			clearCache(projectStructure);
		}

		return projectStructure;
	}

	@Override
	public ProjectStructure updateImpl(ProjectStructure projectStructure) {
		projectStructure = toUnwrappedModel(projectStructure);

		boolean isNew = projectStructure.isNew();

		ProjectStructureModelImpl projectStructureModelImpl = (ProjectStructureModelImpl)projectStructure;

		if (Validator.isNull(projectStructure.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			projectStructure.setUuid(uuid);
		}

		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();

		Date now = new Date();

		if (isNew && (projectStructure.getCreateDate() == null)) {
			if (serviceContext == null) {
				projectStructure.setCreateDate(now);
			}
			else {
				projectStructure.setCreateDate(serviceContext.getCreateDate(now));
			}
		}

		if (!projectStructureModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				projectStructure.setModifiedDate(now);
			}
			else {
				projectStructure.setModifiedDate(serviceContext.getModifiedDate(
						now));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (projectStructure.isNew()) {
				session.save(projectStructure);

				projectStructure.setNew(false);
			}
			else {
				projectStructure = (ProjectStructure)session.merge(projectStructure);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ProjectStructureModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((projectStructureModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						projectStructureModelImpl.getOriginalUuid()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);

				args = new Object[] { projectStructureModelImpl.getUuid() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);
			}

			if ((projectStructureModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						projectStructureModelImpl.getOriginalUuid(),
						projectStructureModelImpl.getOriginalCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);

				args = new Object[] {
						projectStructureModelImpl.getUuid(),
						projectStructureModelImpl.getCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);
			}

			if ((projectStructureModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						projectStructureModelImpl.getOriginalCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_COMPANYID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID,
					args);

				args = new Object[] { projectStructureModelImpl.getCompanyId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_COMPANYID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID,
					args);
			}
		}

		entityCache.putResult(ProjectStructureModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStructureImpl.class, projectStructure.getPrimaryKey(),
			projectStructure, false);

		clearUniqueFindersCache(projectStructureModelImpl);
		cacheUniqueFindersCache(projectStructureModelImpl, isNew);

		projectStructure.resetOriginalValues();

		return projectStructure;
	}

	protected ProjectStructure toUnwrappedModel(
		ProjectStructure projectStructure) {
		if (projectStructure instanceof ProjectStructureImpl) {
			return projectStructure;
		}

		ProjectStructureImpl projectStructureImpl = new ProjectStructureImpl();

		projectStructureImpl.setNew(projectStructure.isNew());
		projectStructureImpl.setPrimaryKey(projectStructure.getPrimaryKey());

		projectStructureImpl.setUuid(projectStructure.getUuid());
		projectStructureImpl.setProjectStructureId(projectStructure.getProjectStructureId());
		projectStructureImpl.setProjectId(projectStructure.getProjectId());
		projectStructureImpl.setCompanyId(projectStructure.getCompanyId());
		projectStructureImpl.setUserId(projectStructure.getUserId());
		projectStructureImpl.setUserName(projectStructure.getUserName());
		projectStructureImpl.setCreateDate(projectStructure.getCreateDate());
		projectStructureImpl.setModifiedDate(projectStructure.getModifiedDate());
		projectStructureImpl.setIdentifier(projectStructure.getIdentifier());
		projectStructureImpl.setName(projectStructure.getName());
		projectStructureImpl.setDescription(projectStructure.getDescription());

		return projectStructureImpl;
	}

	/**
	 * Returns the project structure with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the project structure
	 * @return the project structure
	 * @throws NoSuchProjectStructureException if a project structure with the primary key could not be found
	 */
	@Override
	public ProjectStructure findByPrimaryKey(Serializable primaryKey)
		throws NoSuchProjectStructureException {
		ProjectStructure projectStructure = fetchByPrimaryKey(primaryKey);

		if (projectStructure == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchProjectStructureException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return projectStructure;
	}

	/**
	 * Returns the project structure with the primary key or throws a {@link NoSuchProjectStructureException} if it could not be found.
	 *
	 * @param projectStructureId the primary key of the project structure
	 * @return the project structure
	 * @throws NoSuchProjectStructureException if a project structure with the primary key could not be found
	 */
	@Override
	public ProjectStructure findByPrimaryKey(long projectStructureId)
		throws NoSuchProjectStructureException {
		return findByPrimaryKey((Serializable)projectStructureId);
	}

	/**
	 * Returns the project structure with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the project structure
	 * @return the project structure, or <code>null</code> if a project structure with the primary key could not be found
	 */
	@Override
	public ProjectStructure fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(ProjectStructureModelImpl.ENTITY_CACHE_ENABLED,
				ProjectStructureImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		ProjectStructure projectStructure = (ProjectStructure)serializable;

		if (projectStructure == null) {
			Session session = null;

			try {
				session = openSession();

				projectStructure = (ProjectStructure)session.get(ProjectStructureImpl.class,
						primaryKey);

				if (projectStructure != null) {
					cacheResult(projectStructure);
				}
				else {
					entityCache.putResult(ProjectStructureModelImpl.ENTITY_CACHE_ENABLED,
						ProjectStructureImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(ProjectStructureModelImpl.ENTITY_CACHE_ENABLED,
					ProjectStructureImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return projectStructure;
	}

	/**
	 * Returns the project structure with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param projectStructureId the primary key of the project structure
	 * @return the project structure, or <code>null</code> if a project structure with the primary key could not be found
	 */
	@Override
	public ProjectStructure fetchByPrimaryKey(long projectStructureId) {
		return fetchByPrimaryKey((Serializable)projectStructureId);
	}

	@Override
	public Map<Serializable, ProjectStructure> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, ProjectStructure> map = new HashMap<Serializable, ProjectStructure>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			ProjectStructure projectStructure = fetchByPrimaryKey(primaryKey);

			if (projectStructure != null) {
				map.put(primaryKey, projectStructure);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(ProjectStructureModelImpl.ENTITY_CACHE_ENABLED,
					ProjectStructureImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (ProjectStructure)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_PROJECTSTRUCTURE_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(String.valueOf(primaryKey));

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (ProjectStructure projectStructure : (List<ProjectStructure>)q.list()) {
				map.put(projectStructure.getPrimaryKeyObj(), projectStructure);

				cacheResult(projectStructure);

				uncachedPrimaryKeys.remove(projectStructure.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(ProjectStructureModelImpl.ENTITY_CACHE_ENABLED,
					ProjectStructureImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the project structures.
	 *
	 * @return the project structures
	 */
	@Override
	public List<ProjectStructure> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the project structures.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of project structures
	 * @param end the upper bound of the range of project structures (not inclusive)
	 * @return the range of project structures
	 */
	@Override
	public List<ProjectStructure> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the project structures.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of project structures
	 * @param end the upper bound of the range of project structures (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of project structures
	 */
	@Override
	public List<ProjectStructure> findAll(int start, int end,
		OrderByComparator<ProjectStructure> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the project structures.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of project structures
	 * @param end the upper bound of the range of project structures (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of project structures
	 */
	@Override
	public List<ProjectStructure> findAll(int start, int end,
		OrderByComparator<ProjectStructure> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ProjectStructure> list = null;

		if (retrieveFromCache) {
			list = (List<ProjectStructure>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_PROJECTSTRUCTURE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PROJECTSTRUCTURE;

				if (pagination) {
					sql = sql.concat(ProjectStructureModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ProjectStructure>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ProjectStructure>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the project structures from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (ProjectStructure projectStructure : findAll()) {
			remove(projectStructure);
		}
	}

	/**
	 * Returns the number of project structures.
	 *
	 * @return the number of project structures
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PROJECTSTRUCTURE);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return ProjectStructureModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the project structure persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(ProjectStructureImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = CompanyProviderWrapper.class)
	protected CompanyProvider companyProvider;
	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_PROJECTSTRUCTURE = "SELECT projectStructure FROM ProjectStructure projectStructure";
	private static final String _SQL_SELECT_PROJECTSTRUCTURE_WHERE_PKS_IN = "SELECT projectStructure FROM ProjectStructure projectStructure WHERE projectStructureId IN (";
	private static final String _SQL_SELECT_PROJECTSTRUCTURE_WHERE = "SELECT projectStructure FROM ProjectStructure projectStructure WHERE ";
	private static final String _SQL_COUNT_PROJECTSTRUCTURE = "SELECT COUNT(projectStructure) FROM ProjectStructure projectStructure";
	private static final String _SQL_COUNT_PROJECTSTRUCTURE_WHERE = "SELECT COUNT(projectStructure) FROM ProjectStructure projectStructure WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "projectStructure.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ProjectStructure exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No ProjectStructure exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(ProjectStructurePersistenceImpl.class);
	private static final Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"uuid"
			});
}