/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.project.model.ProjectStructure;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing ProjectStructure in entity cache.
 *
 * @author Claudio Carlenzoli
 * @see ProjectStructure
 * @generated
 */
@ProviderType
public class ProjectStructureCacheModel implements CacheModel<ProjectStructure>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ProjectStructureCacheModel)) {
			return false;
		}

		ProjectStructureCacheModel projectStructureCacheModel = (ProjectStructureCacheModel)obj;

		if (projectStructureId == projectStructureCacheModel.projectStructureId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, projectStructureId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", projectStructureId=");
		sb.append(projectStructureId);
		sb.append(", projectId=");
		sb.append(projectId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", identifier=");
		sb.append(identifier);
		sb.append(", name=");
		sb.append(name);
		sb.append(", description=");
		sb.append(description);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ProjectStructure toEntityModel() {
		ProjectStructureImpl projectStructureImpl = new ProjectStructureImpl();

		if (uuid == null) {
			projectStructureImpl.setUuid(StringPool.BLANK);
		}
		else {
			projectStructureImpl.setUuid(uuid);
		}

		projectStructureImpl.setProjectStructureId(projectStructureId);
		projectStructureImpl.setProjectId(projectId);
		projectStructureImpl.setCompanyId(companyId);
		projectStructureImpl.setUserId(userId);

		if (userName == null) {
			projectStructureImpl.setUserName(StringPool.BLANK);
		}
		else {
			projectStructureImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			projectStructureImpl.setCreateDate(null);
		}
		else {
			projectStructureImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			projectStructureImpl.setModifiedDate(null);
		}
		else {
			projectStructureImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (identifier == null) {
			projectStructureImpl.setIdentifier(StringPool.BLANK);
		}
		else {
			projectStructureImpl.setIdentifier(identifier);
		}

		if (name == null) {
			projectStructureImpl.setName(StringPool.BLANK);
		}
		else {
			projectStructureImpl.setName(name);
		}

		if (description == null) {
			projectStructureImpl.setDescription(StringPool.BLANK);
		}
		else {
			projectStructureImpl.setDescription(description);
		}

		projectStructureImpl.resetOriginalValues();

		return projectStructureImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		projectStructureId = objectInput.readLong();

		projectId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		identifier = objectInput.readUTF();
		name = objectInput.readUTF();
		description = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(projectStructureId);

		objectOutput.writeLong(projectId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (identifier == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(identifier);
		}

		if (name == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(name);
		}

		if (description == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(description);
		}
	}

	public String uuid;
	public long projectStructureId;
	public long projectId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String identifier;
	public String name;
	public String description;
}