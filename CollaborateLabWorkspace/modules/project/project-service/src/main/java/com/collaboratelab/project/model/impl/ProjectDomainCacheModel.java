/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.project.model.ProjectDomain;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing ProjectDomain in entity cache.
 *
 * @author Claudio Carlenzoli
 * @see ProjectDomain
 * @generated
 */
@ProviderType
public class ProjectDomainCacheModel implements CacheModel<ProjectDomain>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ProjectDomainCacheModel)) {
			return false;
		}

		ProjectDomainCacheModel projectDomainCacheModel = (ProjectDomainCacheModel)obj;

		if (projectDomainId == projectDomainCacheModel.projectDomainId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, projectDomainId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", projectDomainId=");
		sb.append(projectDomainId);
		sb.append(", projectStructureId=");
		sb.append(projectStructureId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", identifier=");
		sb.append(identifier);
		sb.append(", name=");
		sb.append(name);
		sb.append(", description=");
		sb.append(description);
		sb.append(", priority=");
		sb.append(priority);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ProjectDomain toEntityModel() {
		ProjectDomainImpl projectDomainImpl = new ProjectDomainImpl();

		if (uuid == null) {
			projectDomainImpl.setUuid(StringPool.BLANK);
		}
		else {
			projectDomainImpl.setUuid(uuid);
		}

		projectDomainImpl.setProjectDomainId(projectDomainId);
		projectDomainImpl.setProjectStructureId(projectStructureId);
		projectDomainImpl.setCompanyId(companyId);
		projectDomainImpl.setUserId(userId);

		if (userName == null) {
			projectDomainImpl.setUserName(StringPool.BLANK);
		}
		else {
			projectDomainImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			projectDomainImpl.setCreateDate(null);
		}
		else {
			projectDomainImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			projectDomainImpl.setModifiedDate(null);
		}
		else {
			projectDomainImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (identifier == null) {
			projectDomainImpl.setIdentifier(StringPool.BLANK);
		}
		else {
			projectDomainImpl.setIdentifier(identifier);
		}

		if (name == null) {
			projectDomainImpl.setName(StringPool.BLANK);
		}
		else {
			projectDomainImpl.setName(name);
		}

		if (description == null) {
			projectDomainImpl.setDescription(StringPool.BLANK);
		}
		else {
			projectDomainImpl.setDescription(description);
		}

		projectDomainImpl.setPriority(priority);

		projectDomainImpl.resetOriginalValues();

		return projectDomainImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		projectDomainId = objectInput.readLong();

		projectStructureId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		identifier = objectInput.readUTF();
		name = objectInput.readUTF();
		description = objectInput.readUTF();

		priority = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(projectDomainId);

		objectOutput.writeLong(projectStructureId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (identifier == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(identifier);
		}

		if (name == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(name);
		}

		if (description == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(description);
		}

		objectOutput.writeLong(priority);
	}

	public String uuid;
	public long projectDomainId;
	public long projectStructureId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String identifier;
	public String name;
	public String description;
	public long priority;
}