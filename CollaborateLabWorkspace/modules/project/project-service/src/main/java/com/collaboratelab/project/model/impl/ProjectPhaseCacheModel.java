/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.project.model.ProjectPhase;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing ProjectPhase in entity cache.
 *
 * @author Claudio Carlenzoli
 * @see ProjectPhase
 * @generated
 */
@ProviderType
public class ProjectPhaseCacheModel implements CacheModel<ProjectPhase>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ProjectPhaseCacheModel)) {
			return false;
		}

		ProjectPhaseCacheModel projectPhaseCacheModel = (ProjectPhaseCacheModel)obj;

		if (projectPhaseId == projectPhaseCacheModel.projectPhaseId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, projectPhaseId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", projectPhaseId=");
		sb.append(projectPhaseId);
		sb.append(", projectStructureId=");
		sb.append(projectStructureId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", identifier=");
		sb.append(identifier);
		sb.append(", name=");
		sb.append(name);
		sb.append(", description=");
		sb.append(description);
		sb.append(", priority=");
		sb.append(priority);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ProjectPhase toEntityModel() {
		ProjectPhaseImpl projectPhaseImpl = new ProjectPhaseImpl();

		if (uuid == null) {
			projectPhaseImpl.setUuid(StringPool.BLANK);
		}
		else {
			projectPhaseImpl.setUuid(uuid);
		}

		projectPhaseImpl.setProjectPhaseId(projectPhaseId);
		projectPhaseImpl.setProjectStructureId(projectStructureId);
		projectPhaseImpl.setCompanyId(companyId);
		projectPhaseImpl.setUserId(userId);

		if (userName == null) {
			projectPhaseImpl.setUserName(StringPool.BLANK);
		}
		else {
			projectPhaseImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			projectPhaseImpl.setCreateDate(null);
		}
		else {
			projectPhaseImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			projectPhaseImpl.setModifiedDate(null);
		}
		else {
			projectPhaseImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (identifier == null) {
			projectPhaseImpl.setIdentifier(StringPool.BLANK);
		}
		else {
			projectPhaseImpl.setIdentifier(identifier);
		}

		if (name == null) {
			projectPhaseImpl.setName(StringPool.BLANK);
		}
		else {
			projectPhaseImpl.setName(name);
		}

		if (description == null) {
			projectPhaseImpl.setDescription(StringPool.BLANK);
		}
		else {
			projectPhaseImpl.setDescription(description);
		}

		projectPhaseImpl.setPriority(priority);

		projectPhaseImpl.resetOriginalValues();

		return projectPhaseImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		projectPhaseId = objectInput.readLong();

		projectStructureId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		identifier = objectInput.readUTF();
		name = objectInput.readUTF();
		description = objectInput.readUTF();

		priority = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(projectPhaseId);

		objectOutput.writeLong(projectStructureId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (identifier == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(identifier);
		}

		if (name == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(name);
		}

		if (description == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(description);
		}

		objectOutput.writeLong(priority);
	}

	public String uuid;
	public long projectPhaseId;
	public long projectStructureId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String identifier;
	public String name;
	public String description;
	public long priority;
}