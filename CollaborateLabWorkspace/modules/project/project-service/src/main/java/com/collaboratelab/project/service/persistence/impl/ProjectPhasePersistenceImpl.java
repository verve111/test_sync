/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.project.exception.NoSuchProjectPhaseException;
import com.collaboratelab.project.model.ProjectPhase;
import com.collaboratelab.project.model.impl.ProjectPhaseImpl;
import com.collaboratelab.project.model.impl.ProjectPhaseModelImpl;
import com.collaboratelab.project.service.persistence.ProjectPhasePersistence;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.CompanyProvider;
import com.liferay.portal.kernel.service.persistence.CompanyProviderWrapper;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the project phase service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Claudio Carlenzoli
 * @see ProjectPhasePersistence
 * @see com.collaboratelab.project.service.persistence.ProjectPhaseUtil
 * @generated
 */
@ProviderType
public class ProjectPhasePersistenceImpl extends BasePersistenceImpl<ProjectPhase>
	implements ProjectPhasePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ProjectPhaseUtil} to access the project phase persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ProjectPhaseImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ProjectPhaseModelImpl.ENTITY_CACHE_ENABLED,
			ProjectPhaseModelImpl.FINDER_CACHE_ENABLED, ProjectPhaseImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ProjectPhaseModelImpl.ENTITY_CACHE_ENABLED,
			ProjectPhaseModelImpl.FINDER_CACHE_ENABLED, ProjectPhaseImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ProjectPhaseModelImpl.ENTITY_CACHE_ENABLED,
			ProjectPhaseModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID = new FinderPath(ProjectPhaseModelImpl.ENTITY_CACHE_ENABLED,
			ProjectPhaseModelImpl.FINDER_CACHE_ENABLED, ProjectPhaseImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID = new FinderPath(ProjectPhaseModelImpl.ENTITY_CACHE_ENABLED,
			ProjectPhaseModelImpl.FINDER_CACHE_ENABLED, ProjectPhaseImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] { String.class.getName() },
			ProjectPhaseModelImpl.UUID_COLUMN_BITMASK |
			ProjectPhaseModelImpl.NAME_COLUMN_BITMASK |
			ProjectPhaseModelImpl.PROJECTSTRUCTUREID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID = new FinderPath(ProjectPhaseModelImpl.ENTITY_CACHE_ENABLED,
			ProjectPhaseModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] { String.class.getName() });

	/**
	 * Returns all the project phases where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching project phases
	 */
	@Override
	public List<ProjectPhase> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the project phases where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of project phases
	 * @param end the upper bound of the range of project phases (not inclusive)
	 * @return the range of matching project phases
	 */
	@Override
	public List<ProjectPhase> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the project phases where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of project phases
	 * @param end the upper bound of the range of project phases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching project phases
	 */
	@Override
	public List<ProjectPhase> findByUuid(String uuid, int start, int end,
		OrderByComparator<ProjectPhase> orderByComparator) {
		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the project phases where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of project phases
	 * @param end the upper bound of the range of project phases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching project phases
	 */
	@Override
	public List<ProjectPhase> findByUuid(String uuid, int start, int end,
		OrderByComparator<ProjectPhase> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid, start, end, orderByComparator };
		}

		List<ProjectPhase> list = null;

		if (retrieveFromCache) {
			list = (List<ProjectPhase>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ProjectPhase projectPhase : list) {
					if (!Objects.equals(uuid, projectPhase.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PROJECTPHASE_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProjectPhaseModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<ProjectPhase>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ProjectPhase>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first project phase in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project phase
	 * @throws NoSuchProjectPhaseException if a matching project phase could not be found
	 */
	@Override
	public ProjectPhase findByUuid_First(String uuid,
		OrderByComparator<ProjectPhase> orderByComparator)
		throws NoSuchProjectPhaseException {
		ProjectPhase projectPhase = fetchByUuid_First(uuid, orderByComparator);

		if (projectPhase != null) {
			return projectPhase;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectPhaseException(msg.toString());
	}

	/**
	 * Returns the first project phase in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project phase, or <code>null</code> if a matching project phase could not be found
	 */
	@Override
	public ProjectPhase fetchByUuid_First(String uuid,
		OrderByComparator<ProjectPhase> orderByComparator) {
		List<ProjectPhase> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last project phase in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project phase
	 * @throws NoSuchProjectPhaseException if a matching project phase could not be found
	 */
	@Override
	public ProjectPhase findByUuid_Last(String uuid,
		OrderByComparator<ProjectPhase> orderByComparator)
		throws NoSuchProjectPhaseException {
		ProjectPhase projectPhase = fetchByUuid_Last(uuid, orderByComparator);

		if (projectPhase != null) {
			return projectPhase;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectPhaseException(msg.toString());
	}

	/**
	 * Returns the last project phase in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project phase, or <code>null</code> if a matching project phase could not be found
	 */
	@Override
	public ProjectPhase fetchByUuid_Last(String uuid,
		OrderByComparator<ProjectPhase> orderByComparator) {
		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<ProjectPhase> list = findByUuid(uuid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the project phases before and after the current project phase in the ordered set where uuid = &#63;.
	 *
	 * @param projectPhaseId the primary key of the current project phase
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next project phase
	 * @throws NoSuchProjectPhaseException if a project phase with the primary key could not be found
	 */
	@Override
	public ProjectPhase[] findByUuid_PrevAndNext(long projectPhaseId,
		String uuid, OrderByComparator<ProjectPhase> orderByComparator)
		throws NoSuchProjectPhaseException {
		ProjectPhase projectPhase = findByPrimaryKey(projectPhaseId);

		Session session = null;

		try {
			session = openSession();

			ProjectPhase[] array = new ProjectPhaseImpl[3];

			array[0] = getByUuid_PrevAndNext(session, projectPhase, uuid,
					orderByComparator, true);

			array[1] = projectPhase;

			array[2] = getByUuid_PrevAndNext(session, projectPhase, uuid,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ProjectPhase getByUuid_PrevAndNext(Session session,
		ProjectPhase projectPhase, String uuid,
		OrderByComparator<ProjectPhase> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PROJECTPHASE_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProjectPhaseModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(projectPhase);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ProjectPhase> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the project phases where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (ProjectPhase projectPhase : findByUuid(uuid, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(projectPhase);
		}
	}

	/**
	 * Returns the number of project phases where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching project phases
	 */
	@Override
	public int countByUuid(String uuid) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID;

		Object[] finderArgs = new Object[] { uuid };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PROJECTPHASE_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_1 = "projectPhase.uuid IS NULL";
	private static final String _FINDER_COLUMN_UUID_UUID_2 = "projectPhase.uuid = ?";
	private static final String _FINDER_COLUMN_UUID_UUID_3 = "(projectPhase.uuid IS NULL OR projectPhase.uuid = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C = new FinderPath(ProjectPhaseModelImpl.ENTITY_CACHE_ENABLED,
			ProjectPhaseModelImpl.FINDER_CACHE_ENABLED, ProjectPhaseImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C =
		new FinderPath(ProjectPhaseModelImpl.ENTITY_CACHE_ENABLED,
			ProjectPhaseModelImpl.FINDER_CACHE_ENABLED, ProjectPhaseImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() },
			ProjectPhaseModelImpl.UUID_COLUMN_BITMASK |
			ProjectPhaseModelImpl.COMPANYID_COLUMN_BITMASK |
			ProjectPhaseModelImpl.NAME_COLUMN_BITMASK |
			ProjectPhaseModelImpl.PROJECTSTRUCTUREID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_C = new FinderPath(ProjectPhaseModelImpl.ENTITY_CACHE_ENABLED,
			ProjectPhaseModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns all the project phases where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching project phases
	 */
	@Override
	public List<ProjectPhase> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(uuid, companyId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the project phases where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of project phases
	 * @param end the upper bound of the range of project phases (not inclusive)
	 * @return the range of matching project phases
	 */
	@Override
	public List<ProjectPhase> findByUuid_C(String uuid, long companyId,
		int start, int end) {
		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the project phases where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of project phases
	 * @param end the upper bound of the range of project phases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching project phases
	 */
	@Override
	public List<ProjectPhase> findByUuid_C(String uuid, long companyId,
		int start, int end, OrderByComparator<ProjectPhase> orderByComparator) {
		return findByUuid_C(uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the project phases where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of project phases
	 * @param end the upper bound of the range of project phases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching project phases
	 */
	@Override
	public List<ProjectPhase> findByUuid_C(String uuid, long companyId,
		int start, int end, OrderByComparator<ProjectPhase> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] { uuid, companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] {
					uuid, companyId,
					
					start, end, orderByComparator
				};
		}

		List<ProjectPhase> list = null;

		if (retrieveFromCache) {
			list = (List<ProjectPhase>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ProjectPhase projectPhase : list) {
					if (!Objects.equals(uuid, projectPhase.getUuid()) ||
							(companyId != projectPhase.getCompanyId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_PROJECTPHASE_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProjectPhaseModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				if (!pagination) {
					list = (List<ProjectPhase>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ProjectPhase>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first project phase in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project phase
	 * @throws NoSuchProjectPhaseException if a matching project phase could not be found
	 */
	@Override
	public ProjectPhase findByUuid_C_First(String uuid, long companyId,
		OrderByComparator<ProjectPhase> orderByComparator)
		throws NoSuchProjectPhaseException {
		ProjectPhase projectPhase = fetchByUuid_C_First(uuid, companyId,
				orderByComparator);

		if (projectPhase != null) {
			return projectPhase;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectPhaseException(msg.toString());
	}

	/**
	 * Returns the first project phase in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project phase, or <code>null</code> if a matching project phase could not be found
	 */
	@Override
	public ProjectPhase fetchByUuid_C_First(String uuid, long companyId,
		OrderByComparator<ProjectPhase> orderByComparator) {
		List<ProjectPhase> list = findByUuid_C(uuid, companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last project phase in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project phase
	 * @throws NoSuchProjectPhaseException if a matching project phase could not be found
	 */
	@Override
	public ProjectPhase findByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<ProjectPhase> orderByComparator)
		throws NoSuchProjectPhaseException {
		ProjectPhase projectPhase = fetchByUuid_C_Last(uuid, companyId,
				orderByComparator);

		if (projectPhase != null) {
			return projectPhase;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectPhaseException(msg.toString());
	}

	/**
	 * Returns the last project phase in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project phase, or <code>null</code> if a matching project phase could not be found
	 */
	@Override
	public ProjectPhase fetchByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<ProjectPhase> orderByComparator) {
		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<ProjectPhase> list = findByUuid_C(uuid, companyId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the project phases before and after the current project phase in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param projectPhaseId the primary key of the current project phase
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next project phase
	 * @throws NoSuchProjectPhaseException if a project phase with the primary key could not be found
	 */
	@Override
	public ProjectPhase[] findByUuid_C_PrevAndNext(long projectPhaseId,
		String uuid, long companyId,
		OrderByComparator<ProjectPhase> orderByComparator)
		throws NoSuchProjectPhaseException {
		ProjectPhase projectPhase = findByPrimaryKey(projectPhaseId);

		Session session = null;

		try {
			session = openSession();

			ProjectPhase[] array = new ProjectPhaseImpl[3];

			array[0] = getByUuid_C_PrevAndNext(session, projectPhase, uuid,
					companyId, orderByComparator, true);

			array[1] = projectPhase;

			array[2] = getByUuid_C_PrevAndNext(session, projectPhase, uuid,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ProjectPhase getByUuid_C_PrevAndNext(Session session,
		ProjectPhase projectPhase, String uuid, long companyId,
		OrderByComparator<ProjectPhase> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_PROJECTPHASE_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProjectPhaseModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(projectPhase);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ProjectPhase> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the project phases where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (ProjectPhase projectPhase : findByUuid_C(uuid, companyId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(projectPhase);
		}
	}

	/**
	 * Returns the number of project phases where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching project phases
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_C;

		Object[] finderArgs = new Object[] { uuid, companyId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_PROJECTPHASE_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_1 = "projectPhase.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_2 = "projectPhase.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_3 = "(projectPhase.uuid IS NULL OR projectPhase.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 = "projectPhase.companyId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PROJECTSTRUCTUREID =
		new FinderPath(ProjectPhaseModelImpl.ENTITY_CACHE_ENABLED,
			ProjectPhaseModelImpl.FINDER_CACHE_ENABLED, ProjectPhaseImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByProjectStructureId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTSTRUCTUREID =
		new FinderPath(ProjectPhaseModelImpl.ENTITY_CACHE_ENABLED,
			ProjectPhaseModelImpl.FINDER_CACHE_ENABLED, ProjectPhaseImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByProjectStructureId", new String[] { Long.class.getName() },
			ProjectPhaseModelImpl.PROJECTSTRUCTUREID_COLUMN_BITMASK |
			ProjectPhaseModelImpl.NAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PROJECTSTRUCTUREID = new FinderPath(ProjectPhaseModelImpl.ENTITY_CACHE_ENABLED,
			ProjectPhaseModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByProjectStructureId", new String[] { Long.class.getName() });

	/**
	 * Returns all the project phases where projectStructureId = &#63;.
	 *
	 * @param projectStructureId the project structure ID
	 * @return the matching project phases
	 */
	@Override
	public List<ProjectPhase> findByProjectStructureId(long projectStructureId) {
		return findByProjectStructureId(projectStructureId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the project phases where projectStructureId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param projectStructureId the project structure ID
	 * @param start the lower bound of the range of project phases
	 * @param end the upper bound of the range of project phases (not inclusive)
	 * @return the range of matching project phases
	 */
	@Override
	public List<ProjectPhase> findByProjectStructureId(
		long projectStructureId, int start, int end) {
		return findByProjectStructureId(projectStructureId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the project phases where projectStructureId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param projectStructureId the project structure ID
	 * @param start the lower bound of the range of project phases
	 * @param end the upper bound of the range of project phases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching project phases
	 */
	@Override
	public List<ProjectPhase> findByProjectStructureId(
		long projectStructureId, int start, int end,
		OrderByComparator<ProjectPhase> orderByComparator) {
		return findByProjectStructureId(projectStructureId, start, end,
			orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the project phases where projectStructureId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param projectStructureId the project structure ID
	 * @param start the lower bound of the range of project phases
	 * @param end the upper bound of the range of project phases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching project phases
	 */
	@Override
	public List<ProjectPhase> findByProjectStructureId(
		long projectStructureId, int start, int end,
		OrderByComparator<ProjectPhase> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTSTRUCTUREID;
			finderArgs = new Object[] { projectStructureId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PROJECTSTRUCTUREID;
			finderArgs = new Object[] {
					projectStructureId,
					
					start, end, orderByComparator
				};
		}

		List<ProjectPhase> list = null;

		if (retrieveFromCache) {
			list = (List<ProjectPhase>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ProjectPhase projectPhase : list) {
					if ((projectStructureId != projectPhase.getProjectStructureId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PROJECTPHASE_WHERE);

			query.append(_FINDER_COLUMN_PROJECTSTRUCTUREID_PROJECTSTRUCTUREID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProjectPhaseModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(projectStructureId);

				if (!pagination) {
					list = (List<ProjectPhase>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ProjectPhase>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first project phase in the ordered set where projectStructureId = &#63;.
	 *
	 * @param projectStructureId the project structure ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project phase
	 * @throws NoSuchProjectPhaseException if a matching project phase could not be found
	 */
	@Override
	public ProjectPhase findByProjectStructureId_First(
		long projectStructureId,
		OrderByComparator<ProjectPhase> orderByComparator)
		throws NoSuchProjectPhaseException {
		ProjectPhase projectPhase = fetchByProjectStructureId_First(projectStructureId,
				orderByComparator);

		if (projectPhase != null) {
			return projectPhase;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("projectStructureId=");
		msg.append(projectStructureId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectPhaseException(msg.toString());
	}

	/**
	 * Returns the first project phase in the ordered set where projectStructureId = &#63;.
	 *
	 * @param projectStructureId the project structure ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project phase, or <code>null</code> if a matching project phase could not be found
	 */
	@Override
	public ProjectPhase fetchByProjectStructureId_First(
		long projectStructureId,
		OrderByComparator<ProjectPhase> orderByComparator) {
		List<ProjectPhase> list = findByProjectStructureId(projectStructureId,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last project phase in the ordered set where projectStructureId = &#63;.
	 *
	 * @param projectStructureId the project structure ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project phase
	 * @throws NoSuchProjectPhaseException if a matching project phase could not be found
	 */
	@Override
	public ProjectPhase findByProjectStructureId_Last(long projectStructureId,
		OrderByComparator<ProjectPhase> orderByComparator)
		throws NoSuchProjectPhaseException {
		ProjectPhase projectPhase = fetchByProjectStructureId_Last(projectStructureId,
				orderByComparator);

		if (projectPhase != null) {
			return projectPhase;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("projectStructureId=");
		msg.append(projectStructureId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectPhaseException(msg.toString());
	}

	/**
	 * Returns the last project phase in the ordered set where projectStructureId = &#63;.
	 *
	 * @param projectStructureId the project structure ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project phase, or <code>null</code> if a matching project phase could not be found
	 */
	@Override
	public ProjectPhase fetchByProjectStructureId_Last(
		long projectStructureId,
		OrderByComparator<ProjectPhase> orderByComparator) {
		int count = countByProjectStructureId(projectStructureId);

		if (count == 0) {
			return null;
		}

		List<ProjectPhase> list = findByProjectStructureId(projectStructureId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the project phases before and after the current project phase in the ordered set where projectStructureId = &#63;.
	 *
	 * @param projectPhaseId the primary key of the current project phase
	 * @param projectStructureId the project structure ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next project phase
	 * @throws NoSuchProjectPhaseException if a project phase with the primary key could not be found
	 */
	@Override
	public ProjectPhase[] findByProjectStructureId_PrevAndNext(
		long projectPhaseId, long projectStructureId,
		OrderByComparator<ProjectPhase> orderByComparator)
		throws NoSuchProjectPhaseException {
		ProjectPhase projectPhase = findByPrimaryKey(projectPhaseId);

		Session session = null;

		try {
			session = openSession();

			ProjectPhase[] array = new ProjectPhaseImpl[3];

			array[0] = getByProjectStructureId_PrevAndNext(session,
					projectPhase, projectStructureId, orderByComparator, true);

			array[1] = projectPhase;

			array[2] = getByProjectStructureId_PrevAndNext(session,
					projectPhase, projectStructureId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ProjectPhase getByProjectStructureId_PrevAndNext(
		Session session, ProjectPhase projectPhase, long projectStructureId,
		OrderByComparator<ProjectPhase> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PROJECTPHASE_WHERE);

		query.append(_FINDER_COLUMN_PROJECTSTRUCTUREID_PROJECTSTRUCTUREID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProjectPhaseModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(projectStructureId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(projectPhase);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ProjectPhase> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the project phases where projectStructureId = &#63; from the database.
	 *
	 * @param projectStructureId the project structure ID
	 */
	@Override
	public void removeByProjectStructureId(long projectStructureId) {
		for (ProjectPhase projectPhase : findByProjectStructureId(
				projectStructureId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(projectPhase);
		}
	}

	/**
	 * Returns the number of project phases where projectStructureId = &#63;.
	 *
	 * @param projectStructureId the project structure ID
	 * @return the number of matching project phases
	 */
	@Override
	public int countByProjectStructureId(long projectStructureId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PROJECTSTRUCTUREID;

		Object[] finderArgs = new Object[] { projectStructureId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PROJECTPHASE_WHERE);

			query.append(_FINDER_COLUMN_PROJECTSTRUCTUREID_PROJECTSTRUCTUREID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(projectStructureId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PROJECTSTRUCTUREID_PROJECTSTRUCTUREID_2 =
		"projectPhase.projectStructureId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER = new FinderPath(ProjectPhaseModelImpl.ENTITY_CACHE_ENABLED,
			ProjectPhaseModelImpl.FINDER_CACHE_ENABLED, ProjectPhaseImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByCompanyIdIdentifier",
			new String[] { Long.class.getName(), String.class.getName() },
			ProjectPhaseModelImpl.COMPANYID_COLUMN_BITMASK |
			ProjectPhaseModelImpl.IDENTIFIER_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANYIDIDENTIFIER = new FinderPath(ProjectPhaseModelImpl.ENTITY_CACHE_ENABLED,
			ProjectPhaseModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByCompanyIdIdentifier",
			new String[] { Long.class.getName(), String.class.getName() });

	/**
	 * Returns the project phase where companyId = &#63; and identifier = &#63; or throws a {@link NoSuchProjectPhaseException} if it could not be found.
	 *
	 * @param companyId the company ID
	 * @param identifier the identifier
	 * @return the matching project phase
	 * @throws NoSuchProjectPhaseException if a matching project phase could not be found
	 */
	@Override
	public ProjectPhase findByCompanyIdIdentifier(long companyId,
		String identifier) throws NoSuchProjectPhaseException {
		ProjectPhase projectPhase = fetchByCompanyIdIdentifier(companyId,
				identifier);

		if (projectPhase == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("companyId=");
			msg.append(companyId);

			msg.append(", identifier=");
			msg.append(identifier);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchProjectPhaseException(msg.toString());
		}

		return projectPhase;
	}

	/**
	 * Returns the project phase where companyId = &#63; and identifier = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param companyId the company ID
	 * @param identifier the identifier
	 * @return the matching project phase, or <code>null</code> if a matching project phase could not be found
	 */
	@Override
	public ProjectPhase fetchByCompanyIdIdentifier(long companyId,
		String identifier) {
		return fetchByCompanyIdIdentifier(companyId, identifier, true);
	}

	/**
	 * Returns the project phase where companyId = &#63; and identifier = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param companyId the company ID
	 * @param identifier the identifier
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching project phase, or <code>null</code> if a matching project phase could not be found
	 */
	@Override
	public ProjectPhase fetchByCompanyIdIdentifier(long companyId,
		String identifier, boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { companyId, identifier };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER,
					finderArgs, this);
		}

		if (result instanceof ProjectPhase) {
			ProjectPhase projectPhase = (ProjectPhase)result;

			if ((companyId != projectPhase.getCompanyId()) ||
					!Objects.equals(identifier, projectPhase.getIdentifier())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_PROJECTPHASE_WHERE);

			query.append(_FINDER_COLUMN_COMPANYIDIDENTIFIER_COMPANYID_2);

			boolean bindIdentifier = false;

			if (identifier == null) {
				query.append(_FINDER_COLUMN_COMPANYIDIDENTIFIER_IDENTIFIER_1);
			}
			else if (identifier.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_COMPANYIDIDENTIFIER_IDENTIFIER_3);
			}
			else {
				bindIdentifier = true;

				query.append(_FINDER_COLUMN_COMPANYIDIDENTIFIER_IDENTIFIER_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (bindIdentifier) {
					qPos.add(identifier);
				}

				List<ProjectPhase> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER,
						finderArgs, list);
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							_log.warn(
								"ProjectPhasePersistenceImpl.fetchByCompanyIdIdentifier(long, String, boolean) with parameters (" +
								StringUtil.merge(finderArgs) +
								") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					ProjectPhase projectPhase = list.get(0);

					result = projectPhase;

					cacheResult(projectPhase);

					if ((projectPhase.getCompanyId() != companyId) ||
							(projectPhase.getIdentifier() == null) ||
							!projectPhase.getIdentifier().equals(identifier)) {
						finderCache.putResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER,
							finderArgs, projectPhase);
					}
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (ProjectPhase)result;
		}
	}

	/**
	 * Removes the project phase where companyId = &#63; and identifier = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param identifier the identifier
	 * @return the project phase that was removed
	 */
	@Override
	public ProjectPhase removeByCompanyIdIdentifier(long companyId,
		String identifier) throws NoSuchProjectPhaseException {
		ProjectPhase projectPhase = findByCompanyIdIdentifier(companyId,
				identifier);

		return remove(projectPhase);
	}

	/**
	 * Returns the number of project phases where companyId = &#63; and identifier = &#63;.
	 *
	 * @param companyId the company ID
	 * @param identifier the identifier
	 * @return the number of matching project phases
	 */
	@Override
	public int countByCompanyIdIdentifier(long companyId, String identifier) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_COMPANYIDIDENTIFIER;

		Object[] finderArgs = new Object[] { companyId, identifier };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_PROJECTPHASE_WHERE);

			query.append(_FINDER_COLUMN_COMPANYIDIDENTIFIER_COMPANYID_2);

			boolean bindIdentifier = false;

			if (identifier == null) {
				query.append(_FINDER_COLUMN_COMPANYIDIDENTIFIER_IDENTIFIER_1);
			}
			else if (identifier.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_COMPANYIDIDENTIFIER_IDENTIFIER_3);
			}
			else {
				bindIdentifier = true;

				query.append(_FINDER_COLUMN_COMPANYIDIDENTIFIER_IDENTIFIER_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (bindIdentifier) {
					qPos.add(identifier);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COMPANYIDIDENTIFIER_COMPANYID_2 = "projectPhase.companyId = ? AND ";
	private static final String _FINDER_COLUMN_COMPANYIDIDENTIFIER_IDENTIFIER_1 = "projectPhase.identifier IS NULL";
	private static final String _FINDER_COLUMN_COMPANYIDIDENTIFIER_IDENTIFIER_2 = "projectPhase.identifier = ?";
	private static final String _FINDER_COLUMN_COMPANYIDIDENTIFIER_IDENTIFIER_3 = "(projectPhase.identifier IS NULL OR projectPhase.identifier = '')";
	public static final FinderPath FINDER_PATH_FETCH_BY_PROJECTSTRUCTUREIDIDENTIFIER =
		new FinderPath(ProjectPhaseModelImpl.ENTITY_CACHE_ENABLED,
			ProjectPhaseModelImpl.FINDER_CACHE_ENABLED, ProjectPhaseImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByProjectStructureIdIdentifier",
			new String[] { Long.class.getName(), String.class.getName() },
			ProjectPhaseModelImpl.PROJECTSTRUCTUREID_COLUMN_BITMASK |
			ProjectPhaseModelImpl.IDENTIFIER_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PROJECTSTRUCTUREIDIDENTIFIER =
		new FinderPath(ProjectPhaseModelImpl.ENTITY_CACHE_ENABLED,
			ProjectPhaseModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByProjectStructureIdIdentifier",
			new String[] { Long.class.getName(), String.class.getName() });

	/**
	 * Returns the project phase where projectStructureId = &#63; and identifier = &#63; or throws a {@link NoSuchProjectPhaseException} if it could not be found.
	 *
	 * @param projectStructureId the project structure ID
	 * @param identifier the identifier
	 * @return the matching project phase
	 * @throws NoSuchProjectPhaseException if a matching project phase could not be found
	 */
	@Override
	public ProjectPhase findByProjectStructureIdIdentifier(
		long projectStructureId, String identifier)
		throws NoSuchProjectPhaseException {
		ProjectPhase projectPhase = fetchByProjectStructureIdIdentifier(projectStructureId,
				identifier);

		if (projectPhase == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("projectStructureId=");
			msg.append(projectStructureId);

			msg.append(", identifier=");
			msg.append(identifier);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchProjectPhaseException(msg.toString());
		}

		return projectPhase;
	}

	/**
	 * Returns the project phase where projectStructureId = &#63; and identifier = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param projectStructureId the project structure ID
	 * @param identifier the identifier
	 * @return the matching project phase, or <code>null</code> if a matching project phase could not be found
	 */
	@Override
	public ProjectPhase fetchByProjectStructureIdIdentifier(
		long projectStructureId, String identifier) {
		return fetchByProjectStructureIdIdentifier(projectStructureId,
			identifier, true);
	}

	/**
	 * Returns the project phase where projectStructureId = &#63; and identifier = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param projectStructureId the project structure ID
	 * @param identifier the identifier
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching project phase, or <code>null</code> if a matching project phase could not be found
	 */
	@Override
	public ProjectPhase fetchByProjectStructureIdIdentifier(
		long projectStructureId, String identifier, boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { projectStructureId, identifier };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_PROJECTSTRUCTUREIDIDENTIFIER,
					finderArgs, this);
		}

		if (result instanceof ProjectPhase) {
			ProjectPhase projectPhase = (ProjectPhase)result;

			if ((projectStructureId != projectPhase.getProjectStructureId()) ||
					!Objects.equals(identifier, projectPhase.getIdentifier())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_PROJECTPHASE_WHERE);

			query.append(_FINDER_COLUMN_PROJECTSTRUCTUREIDIDENTIFIER_PROJECTSTRUCTUREID_2);

			boolean bindIdentifier = false;

			if (identifier == null) {
				query.append(_FINDER_COLUMN_PROJECTSTRUCTUREIDIDENTIFIER_IDENTIFIER_1);
			}
			else if (identifier.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_PROJECTSTRUCTUREIDIDENTIFIER_IDENTIFIER_3);
			}
			else {
				bindIdentifier = true;

				query.append(_FINDER_COLUMN_PROJECTSTRUCTUREIDIDENTIFIER_IDENTIFIER_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(projectStructureId);

				if (bindIdentifier) {
					qPos.add(identifier);
				}

				List<ProjectPhase> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_PROJECTSTRUCTUREIDIDENTIFIER,
						finderArgs, list);
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							_log.warn(
								"ProjectPhasePersistenceImpl.fetchByProjectStructureIdIdentifier(long, String, boolean) with parameters (" +
								StringUtil.merge(finderArgs) +
								") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					ProjectPhase projectPhase = list.get(0);

					result = projectPhase;

					cacheResult(projectPhase);

					if ((projectPhase.getProjectStructureId() != projectStructureId) ||
							(projectPhase.getIdentifier() == null) ||
							!projectPhase.getIdentifier().equals(identifier)) {
						finderCache.putResult(FINDER_PATH_FETCH_BY_PROJECTSTRUCTUREIDIDENTIFIER,
							finderArgs, projectPhase);
					}
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_PROJECTSTRUCTUREIDIDENTIFIER,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (ProjectPhase)result;
		}
	}

	/**
	 * Removes the project phase where projectStructureId = &#63; and identifier = &#63; from the database.
	 *
	 * @param projectStructureId the project structure ID
	 * @param identifier the identifier
	 * @return the project phase that was removed
	 */
	@Override
	public ProjectPhase removeByProjectStructureIdIdentifier(
		long projectStructureId, String identifier)
		throws NoSuchProjectPhaseException {
		ProjectPhase projectPhase = findByProjectStructureIdIdentifier(projectStructureId,
				identifier);

		return remove(projectPhase);
	}

	/**
	 * Returns the number of project phases where projectStructureId = &#63; and identifier = &#63;.
	 *
	 * @param projectStructureId the project structure ID
	 * @param identifier the identifier
	 * @return the number of matching project phases
	 */
	@Override
	public int countByProjectStructureIdIdentifier(long projectStructureId,
		String identifier) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PROJECTSTRUCTUREIDIDENTIFIER;

		Object[] finderArgs = new Object[] { projectStructureId, identifier };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_PROJECTPHASE_WHERE);

			query.append(_FINDER_COLUMN_PROJECTSTRUCTUREIDIDENTIFIER_PROJECTSTRUCTUREID_2);

			boolean bindIdentifier = false;

			if (identifier == null) {
				query.append(_FINDER_COLUMN_PROJECTSTRUCTUREIDIDENTIFIER_IDENTIFIER_1);
			}
			else if (identifier.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_PROJECTSTRUCTUREIDIDENTIFIER_IDENTIFIER_3);
			}
			else {
				bindIdentifier = true;

				query.append(_FINDER_COLUMN_PROJECTSTRUCTUREIDIDENTIFIER_IDENTIFIER_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(projectStructureId);

				if (bindIdentifier) {
					qPos.add(identifier);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PROJECTSTRUCTUREIDIDENTIFIER_PROJECTSTRUCTUREID_2 =
		"projectPhase.projectStructureId = ? AND ";
	private static final String _FINDER_COLUMN_PROJECTSTRUCTUREIDIDENTIFIER_IDENTIFIER_1 =
		"projectPhase.identifier IS NULL";
	private static final String _FINDER_COLUMN_PROJECTSTRUCTUREIDIDENTIFIER_IDENTIFIER_2 =
		"projectPhase.identifier = ?";
	private static final String _FINDER_COLUMN_PROJECTSTRUCTUREIDIDENTIFIER_IDENTIFIER_3 =
		"(projectPhase.identifier IS NULL OR projectPhase.identifier = '')";
	public static final FinderPath FINDER_PATH_FETCH_BY_PROJECTPHASEID = new FinderPath(ProjectPhaseModelImpl.ENTITY_CACHE_ENABLED,
			ProjectPhaseModelImpl.FINDER_CACHE_ENABLED, ProjectPhaseImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByProjectPhaseId",
			new String[] { Long.class.getName() },
			ProjectPhaseModelImpl.PROJECTPHASEID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PROJECTPHASEID = new FinderPath(ProjectPhaseModelImpl.ENTITY_CACHE_ENABLED,
			ProjectPhaseModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByProjectPhaseId",
			new String[] { Long.class.getName() });

	/**
	 * Returns the project phase where projectPhaseId = &#63; or throws a {@link NoSuchProjectPhaseException} if it could not be found.
	 *
	 * @param projectPhaseId the project phase ID
	 * @return the matching project phase
	 * @throws NoSuchProjectPhaseException if a matching project phase could not be found
	 */
	@Override
	public ProjectPhase findByProjectPhaseId(long projectPhaseId)
		throws NoSuchProjectPhaseException {
		ProjectPhase projectPhase = fetchByProjectPhaseId(projectPhaseId);

		if (projectPhase == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("projectPhaseId=");
			msg.append(projectPhaseId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchProjectPhaseException(msg.toString());
		}

		return projectPhase;
	}

	/**
	 * Returns the project phase where projectPhaseId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param projectPhaseId the project phase ID
	 * @return the matching project phase, or <code>null</code> if a matching project phase could not be found
	 */
	@Override
	public ProjectPhase fetchByProjectPhaseId(long projectPhaseId) {
		return fetchByProjectPhaseId(projectPhaseId, true);
	}

	/**
	 * Returns the project phase where projectPhaseId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param projectPhaseId the project phase ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching project phase, or <code>null</code> if a matching project phase could not be found
	 */
	@Override
	public ProjectPhase fetchByProjectPhaseId(long projectPhaseId,
		boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { projectPhaseId };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_PROJECTPHASEID,
					finderArgs, this);
		}

		if (result instanceof ProjectPhase) {
			ProjectPhase projectPhase = (ProjectPhase)result;

			if ((projectPhaseId != projectPhase.getProjectPhaseId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_PROJECTPHASE_WHERE);

			query.append(_FINDER_COLUMN_PROJECTPHASEID_PROJECTPHASEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(projectPhaseId);

				List<ProjectPhase> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_PROJECTPHASEID,
						finderArgs, list);
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							_log.warn(
								"ProjectPhasePersistenceImpl.fetchByProjectPhaseId(long, boolean) with parameters (" +
								StringUtil.merge(finderArgs) +
								") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					ProjectPhase projectPhase = list.get(0);

					result = projectPhase;

					cacheResult(projectPhase);

					if ((projectPhase.getProjectPhaseId() != projectPhaseId)) {
						finderCache.putResult(FINDER_PATH_FETCH_BY_PROJECTPHASEID,
							finderArgs, projectPhase);
					}
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_PROJECTPHASEID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (ProjectPhase)result;
		}
	}

	/**
	 * Removes the project phase where projectPhaseId = &#63; from the database.
	 *
	 * @param projectPhaseId the project phase ID
	 * @return the project phase that was removed
	 */
	@Override
	public ProjectPhase removeByProjectPhaseId(long projectPhaseId)
		throws NoSuchProjectPhaseException {
		ProjectPhase projectPhase = findByProjectPhaseId(projectPhaseId);

		return remove(projectPhase);
	}

	/**
	 * Returns the number of project phases where projectPhaseId = &#63;.
	 *
	 * @param projectPhaseId the project phase ID
	 * @return the number of matching project phases
	 */
	@Override
	public int countByProjectPhaseId(long projectPhaseId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PROJECTPHASEID;

		Object[] finderArgs = new Object[] { projectPhaseId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PROJECTPHASE_WHERE);

			query.append(_FINDER_COLUMN_PROJECTPHASEID_PROJECTPHASEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(projectPhaseId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PROJECTPHASEID_PROJECTPHASEID_2 = "projectPhase.projectPhaseId = ?";

	public ProjectPhasePersistenceImpl() {
		setModelClass(ProjectPhase.class);
	}

	/**
	 * Caches the project phase in the entity cache if it is enabled.
	 *
	 * @param projectPhase the project phase
	 */
	@Override
	public void cacheResult(ProjectPhase projectPhase) {
		entityCache.putResult(ProjectPhaseModelImpl.ENTITY_CACHE_ENABLED,
			ProjectPhaseImpl.class, projectPhase.getPrimaryKey(), projectPhase);

		finderCache.putResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER,
			new Object[] {
				projectPhase.getCompanyId(), projectPhase.getIdentifier()
			}, projectPhase);

		finderCache.putResult(FINDER_PATH_FETCH_BY_PROJECTSTRUCTUREIDIDENTIFIER,
			new Object[] {
				projectPhase.getProjectStructureId(),
				projectPhase.getIdentifier()
			}, projectPhase);

		finderCache.putResult(FINDER_PATH_FETCH_BY_PROJECTPHASEID,
			new Object[] { projectPhase.getProjectPhaseId() }, projectPhase);

		projectPhase.resetOriginalValues();
	}

	/**
	 * Caches the project phases in the entity cache if it is enabled.
	 *
	 * @param projectPhases the project phases
	 */
	@Override
	public void cacheResult(List<ProjectPhase> projectPhases) {
		for (ProjectPhase projectPhase : projectPhases) {
			if (entityCache.getResult(
						ProjectPhaseModelImpl.ENTITY_CACHE_ENABLED,
						ProjectPhaseImpl.class, projectPhase.getPrimaryKey()) == null) {
				cacheResult(projectPhase);
			}
			else {
				projectPhase.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all project phases.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(ProjectPhaseImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the project phase.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ProjectPhase projectPhase) {
		entityCache.removeResult(ProjectPhaseModelImpl.ENTITY_CACHE_ENABLED,
			ProjectPhaseImpl.class, projectPhase.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache((ProjectPhaseModelImpl)projectPhase);
	}

	@Override
	public void clearCache(List<ProjectPhase> projectPhases) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ProjectPhase projectPhase : projectPhases) {
			entityCache.removeResult(ProjectPhaseModelImpl.ENTITY_CACHE_ENABLED,
				ProjectPhaseImpl.class, projectPhase.getPrimaryKey());

			clearUniqueFindersCache((ProjectPhaseModelImpl)projectPhase);
		}
	}

	protected void cacheUniqueFindersCache(
		ProjectPhaseModelImpl projectPhaseModelImpl, boolean isNew) {
		if (isNew) {
			Object[] args = new Object[] {
					projectPhaseModelImpl.getCompanyId(),
					projectPhaseModelImpl.getIdentifier()
				};

			finderCache.putResult(FINDER_PATH_COUNT_BY_COMPANYIDIDENTIFIER,
				args, Long.valueOf(1));
			finderCache.putResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER,
				args, projectPhaseModelImpl);

			args = new Object[] {
					projectPhaseModelImpl.getProjectStructureId(),
					projectPhaseModelImpl.getIdentifier()
				};

			finderCache.putResult(FINDER_PATH_COUNT_BY_PROJECTSTRUCTUREIDIDENTIFIER,
				args, Long.valueOf(1));
			finderCache.putResult(FINDER_PATH_FETCH_BY_PROJECTSTRUCTUREIDIDENTIFIER,
				args, projectPhaseModelImpl);

			args = new Object[] { projectPhaseModelImpl.getProjectPhaseId() };

			finderCache.putResult(FINDER_PATH_COUNT_BY_PROJECTPHASEID, args,
				Long.valueOf(1));
			finderCache.putResult(FINDER_PATH_FETCH_BY_PROJECTPHASEID, args,
				projectPhaseModelImpl);
		}
		else {
			if ((projectPhaseModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						projectPhaseModelImpl.getCompanyId(),
						projectPhaseModelImpl.getIdentifier()
					};

				finderCache.putResult(FINDER_PATH_COUNT_BY_COMPANYIDIDENTIFIER,
					args, Long.valueOf(1));
				finderCache.putResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER,
					args, projectPhaseModelImpl);
			}

			if ((projectPhaseModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_PROJECTSTRUCTUREIDIDENTIFIER.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						projectPhaseModelImpl.getProjectStructureId(),
						projectPhaseModelImpl.getIdentifier()
					};

				finderCache.putResult(FINDER_PATH_COUNT_BY_PROJECTSTRUCTUREIDIDENTIFIER,
					args, Long.valueOf(1));
				finderCache.putResult(FINDER_PATH_FETCH_BY_PROJECTSTRUCTUREIDIDENTIFIER,
					args, projectPhaseModelImpl);
			}

			if ((projectPhaseModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_PROJECTPHASEID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						projectPhaseModelImpl.getProjectPhaseId()
					};

				finderCache.putResult(FINDER_PATH_COUNT_BY_PROJECTPHASEID,
					args, Long.valueOf(1));
				finderCache.putResult(FINDER_PATH_FETCH_BY_PROJECTPHASEID,
					args, projectPhaseModelImpl);
			}
		}
	}

	protected void clearUniqueFindersCache(
		ProjectPhaseModelImpl projectPhaseModelImpl) {
		Object[] args = new Object[] {
				projectPhaseModelImpl.getCompanyId(),
				projectPhaseModelImpl.getIdentifier()
			};

		finderCache.removeResult(FINDER_PATH_COUNT_BY_COMPANYIDIDENTIFIER, args);
		finderCache.removeResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER, args);

		if ((projectPhaseModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER.getColumnBitmask()) != 0) {
			args = new Object[] {
					projectPhaseModelImpl.getOriginalCompanyId(),
					projectPhaseModelImpl.getOriginalIdentifier()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_COMPANYIDIDENTIFIER,
				args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER,
				args);
		}

		args = new Object[] {
				projectPhaseModelImpl.getProjectStructureId(),
				projectPhaseModelImpl.getIdentifier()
			};

		finderCache.removeResult(FINDER_PATH_COUNT_BY_PROJECTSTRUCTUREIDIDENTIFIER,
			args);
		finderCache.removeResult(FINDER_PATH_FETCH_BY_PROJECTSTRUCTUREIDIDENTIFIER,
			args);

		if ((projectPhaseModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_PROJECTSTRUCTUREIDIDENTIFIER.getColumnBitmask()) != 0) {
			args = new Object[] {
					projectPhaseModelImpl.getOriginalProjectStructureId(),
					projectPhaseModelImpl.getOriginalIdentifier()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_PROJECTSTRUCTUREIDIDENTIFIER,
				args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_PROJECTSTRUCTUREIDIDENTIFIER,
				args);
		}

		args = new Object[] { projectPhaseModelImpl.getProjectPhaseId() };

		finderCache.removeResult(FINDER_PATH_COUNT_BY_PROJECTPHASEID, args);
		finderCache.removeResult(FINDER_PATH_FETCH_BY_PROJECTPHASEID, args);

		if ((projectPhaseModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_PROJECTPHASEID.getColumnBitmask()) != 0) {
			args = new Object[] {
					projectPhaseModelImpl.getOriginalProjectPhaseId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_PROJECTPHASEID, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_PROJECTPHASEID, args);
		}
	}

	/**
	 * Creates a new project phase with the primary key. Does not add the project phase to the database.
	 *
	 * @param projectPhaseId the primary key for the new project phase
	 * @return the new project phase
	 */
	@Override
	public ProjectPhase create(long projectPhaseId) {
		ProjectPhase projectPhase = new ProjectPhaseImpl();

		projectPhase.setNew(true);
		projectPhase.setPrimaryKey(projectPhaseId);

		String uuid = PortalUUIDUtil.generate();

		projectPhase.setUuid(uuid);

		projectPhase.setCompanyId(companyProvider.getCompanyId());

		return projectPhase;
	}

	/**
	 * Removes the project phase with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param projectPhaseId the primary key of the project phase
	 * @return the project phase that was removed
	 * @throws NoSuchProjectPhaseException if a project phase with the primary key could not be found
	 */
	@Override
	public ProjectPhase remove(long projectPhaseId)
		throws NoSuchProjectPhaseException {
		return remove((Serializable)projectPhaseId);
	}

	/**
	 * Removes the project phase with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the project phase
	 * @return the project phase that was removed
	 * @throws NoSuchProjectPhaseException if a project phase with the primary key could not be found
	 */
	@Override
	public ProjectPhase remove(Serializable primaryKey)
		throws NoSuchProjectPhaseException {
		Session session = null;

		try {
			session = openSession();

			ProjectPhase projectPhase = (ProjectPhase)session.get(ProjectPhaseImpl.class,
					primaryKey);

			if (projectPhase == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchProjectPhaseException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(projectPhase);
		}
		catch (NoSuchProjectPhaseException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ProjectPhase removeImpl(ProjectPhase projectPhase) {
		projectPhase = toUnwrappedModel(projectPhase);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(projectPhase)) {
				projectPhase = (ProjectPhase)session.get(ProjectPhaseImpl.class,
						projectPhase.getPrimaryKeyObj());
			}

			if (projectPhase != null) {
				session.delete(projectPhase);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (projectPhase != null) {
			clearCache(projectPhase);
		}

		return projectPhase;
	}

	@Override
	public ProjectPhase updateImpl(ProjectPhase projectPhase) {
		projectPhase = toUnwrappedModel(projectPhase);

		boolean isNew = projectPhase.isNew();

		ProjectPhaseModelImpl projectPhaseModelImpl = (ProjectPhaseModelImpl)projectPhase;

		if (Validator.isNull(projectPhase.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			projectPhase.setUuid(uuid);
		}

		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();

		Date now = new Date();

		if (isNew && (projectPhase.getCreateDate() == null)) {
			if (serviceContext == null) {
				projectPhase.setCreateDate(now);
			}
			else {
				projectPhase.setCreateDate(serviceContext.getCreateDate(now));
			}
		}

		if (!projectPhaseModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				projectPhase.setModifiedDate(now);
			}
			else {
				projectPhase.setModifiedDate(serviceContext.getModifiedDate(now));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (projectPhase.isNew()) {
				session.save(projectPhase);

				projectPhase.setNew(false);
			}
			else {
				projectPhase = (ProjectPhase)session.merge(projectPhase);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ProjectPhaseModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((projectPhaseModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						projectPhaseModelImpl.getOriginalUuid()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);

				args = new Object[] { projectPhaseModelImpl.getUuid() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);
			}

			if ((projectPhaseModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						projectPhaseModelImpl.getOriginalUuid(),
						projectPhaseModelImpl.getOriginalCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);

				args = new Object[] {
						projectPhaseModelImpl.getUuid(),
						projectPhaseModelImpl.getCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);
			}

			if ((projectPhaseModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTSTRUCTUREID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						projectPhaseModelImpl.getOriginalProjectStructureId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_PROJECTSTRUCTUREID,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTSTRUCTUREID,
					args);

				args = new Object[] {
						projectPhaseModelImpl.getProjectStructureId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_PROJECTSTRUCTUREID,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTSTRUCTUREID,
					args);
			}
		}

		entityCache.putResult(ProjectPhaseModelImpl.ENTITY_CACHE_ENABLED,
			ProjectPhaseImpl.class, projectPhase.getPrimaryKey(), projectPhase,
			false);

		clearUniqueFindersCache(projectPhaseModelImpl);
		cacheUniqueFindersCache(projectPhaseModelImpl, isNew);

		projectPhase.resetOriginalValues();

		return projectPhase;
	}

	protected ProjectPhase toUnwrappedModel(ProjectPhase projectPhase) {
		if (projectPhase instanceof ProjectPhaseImpl) {
			return projectPhase;
		}

		ProjectPhaseImpl projectPhaseImpl = new ProjectPhaseImpl();

		projectPhaseImpl.setNew(projectPhase.isNew());
		projectPhaseImpl.setPrimaryKey(projectPhase.getPrimaryKey());

		projectPhaseImpl.setUuid(projectPhase.getUuid());
		projectPhaseImpl.setProjectPhaseId(projectPhase.getProjectPhaseId());
		projectPhaseImpl.setProjectStructureId(projectPhase.getProjectStructureId());
		projectPhaseImpl.setCompanyId(projectPhase.getCompanyId());
		projectPhaseImpl.setUserId(projectPhase.getUserId());
		projectPhaseImpl.setUserName(projectPhase.getUserName());
		projectPhaseImpl.setCreateDate(projectPhase.getCreateDate());
		projectPhaseImpl.setModifiedDate(projectPhase.getModifiedDate());
		projectPhaseImpl.setIdentifier(projectPhase.getIdentifier());
		projectPhaseImpl.setName(projectPhase.getName());
		projectPhaseImpl.setDescription(projectPhase.getDescription());
		projectPhaseImpl.setPriority(projectPhase.getPriority());

		return projectPhaseImpl;
	}

	/**
	 * Returns the project phase with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the project phase
	 * @return the project phase
	 * @throws NoSuchProjectPhaseException if a project phase with the primary key could not be found
	 */
	@Override
	public ProjectPhase findByPrimaryKey(Serializable primaryKey)
		throws NoSuchProjectPhaseException {
		ProjectPhase projectPhase = fetchByPrimaryKey(primaryKey);

		if (projectPhase == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchProjectPhaseException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return projectPhase;
	}

	/**
	 * Returns the project phase with the primary key or throws a {@link NoSuchProjectPhaseException} if it could not be found.
	 *
	 * @param projectPhaseId the primary key of the project phase
	 * @return the project phase
	 * @throws NoSuchProjectPhaseException if a project phase with the primary key could not be found
	 */
	@Override
	public ProjectPhase findByPrimaryKey(long projectPhaseId)
		throws NoSuchProjectPhaseException {
		return findByPrimaryKey((Serializable)projectPhaseId);
	}

	/**
	 * Returns the project phase with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the project phase
	 * @return the project phase, or <code>null</code> if a project phase with the primary key could not be found
	 */
	@Override
	public ProjectPhase fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(ProjectPhaseModelImpl.ENTITY_CACHE_ENABLED,
				ProjectPhaseImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		ProjectPhase projectPhase = (ProjectPhase)serializable;

		if (projectPhase == null) {
			Session session = null;

			try {
				session = openSession();

				projectPhase = (ProjectPhase)session.get(ProjectPhaseImpl.class,
						primaryKey);

				if (projectPhase != null) {
					cacheResult(projectPhase);
				}
				else {
					entityCache.putResult(ProjectPhaseModelImpl.ENTITY_CACHE_ENABLED,
						ProjectPhaseImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(ProjectPhaseModelImpl.ENTITY_CACHE_ENABLED,
					ProjectPhaseImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return projectPhase;
	}

	/**
	 * Returns the project phase with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param projectPhaseId the primary key of the project phase
	 * @return the project phase, or <code>null</code> if a project phase with the primary key could not be found
	 */
	@Override
	public ProjectPhase fetchByPrimaryKey(long projectPhaseId) {
		return fetchByPrimaryKey((Serializable)projectPhaseId);
	}

	@Override
	public Map<Serializable, ProjectPhase> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, ProjectPhase> map = new HashMap<Serializable, ProjectPhase>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			ProjectPhase projectPhase = fetchByPrimaryKey(primaryKey);

			if (projectPhase != null) {
				map.put(primaryKey, projectPhase);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(ProjectPhaseModelImpl.ENTITY_CACHE_ENABLED,
					ProjectPhaseImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (ProjectPhase)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_PROJECTPHASE_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(String.valueOf(primaryKey));

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (ProjectPhase projectPhase : (List<ProjectPhase>)q.list()) {
				map.put(projectPhase.getPrimaryKeyObj(), projectPhase);

				cacheResult(projectPhase);

				uncachedPrimaryKeys.remove(projectPhase.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(ProjectPhaseModelImpl.ENTITY_CACHE_ENABLED,
					ProjectPhaseImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the project phases.
	 *
	 * @return the project phases
	 */
	@Override
	public List<ProjectPhase> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the project phases.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of project phases
	 * @param end the upper bound of the range of project phases (not inclusive)
	 * @return the range of project phases
	 */
	@Override
	public List<ProjectPhase> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the project phases.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of project phases
	 * @param end the upper bound of the range of project phases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of project phases
	 */
	@Override
	public List<ProjectPhase> findAll(int start, int end,
		OrderByComparator<ProjectPhase> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the project phases.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of project phases
	 * @param end the upper bound of the range of project phases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of project phases
	 */
	@Override
	public List<ProjectPhase> findAll(int start, int end,
		OrderByComparator<ProjectPhase> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ProjectPhase> list = null;

		if (retrieveFromCache) {
			list = (List<ProjectPhase>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_PROJECTPHASE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PROJECTPHASE;

				if (pagination) {
					sql = sql.concat(ProjectPhaseModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ProjectPhase>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ProjectPhase>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the project phases from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (ProjectPhase projectPhase : findAll()) {
			remove(projectPhase);
		}
	}

	/**
	 * Returns the number of project phases.
	 *
	 * @return the number of project phases
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PROJECTPHASE);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return ProjectPhaseModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the project phase persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(ProjectPhaseImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = CompanyProviderWrapper.class)
	protected CompanyProvider companyProvider;
	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_PROJECTPHASE = "SELECT projectPhase FROM ProjectPhase projectPhase";
	private static final String _SQL_SELECT_PROJECTPHASE_WHERE_PKS_IN = "SELECT projectPhase FROM ProjectPhase projectPhase WHERE projectPhaseId IN (";
	private static final String _SQL_SELECT_PROJECTPHASE_WHERE = "SELECT projectPhase FROM ProjectPhase projectPhase WHERE ";
	private static final String _SQL_COUNT_PROJECTPHASE = "SELECT COUNT(projectPhase) FROM ProjectPhase projectPhase";
	private static final String _SQL_COUNT_PROJECTPHASE_WHERE = "SELECT COUNT(projectPhase) FROM ProjectPhase projectPhase WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "projectPhase.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ProjectPhase exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No ProjectPhase exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(ProjectPhasePersistenceImpl.class);
	private static final Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"uuid"
			});
}