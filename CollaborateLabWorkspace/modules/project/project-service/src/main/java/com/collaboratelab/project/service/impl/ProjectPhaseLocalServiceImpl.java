/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.service.impl;

import java.util.List;

import com.collaboratelab.project.exception.NoSuchProjectPhaseException;
import com.collaboratelab.project.model.ProjectPhase;
import com.collaboratelab.project.service.base.ProjectPhaseLocalServiceBaseImpl;
import com.liferay.portal.kernel.exception.SystemException;

import aQute.bnd.annotation.ProviderType;

/**
 * The implementation of the project phase local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.collaboratelab.project.service.ProjectPhaseLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Claudio Carlenzoli
 * @see ProjectPhaseLocalServiceBaseImpl
 * @see com.collaboratelab.project.service.ProjectPhaseLocalServiceUtil
 */
@ProviderType
public class ProjectPhaseLocalServiceImpl
	extends ProjectPhaseLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link com.collaboratelab.project.service.ProjectPhaseLocalServiceUtil} to access the project phase local service.
	 */
	
	public List<ProjectPhase> findByProjectStructureId(long projectStructureId) throws SystemException
	{
		
	    return this.projectPhasePersistence.findByProjectStructureId(projectStructureId);
	   
	}
	
	public ProjectPhase findByProjectStructureIdIdentifier(long projectStructureId, String identifier) throws SystemException
	{
	    try {
			return this.projectPhasePersistence.findByProjectStructureIdIdentifier(projectStructureId, identifier);
		} catch (NoSuchProjectPhaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    return null;
	}
	
	public ProjectPhase findByProjectPhaseId(long projectPhaseId) throws SystemException
	{
	    try {
			return this.projectPhasePersistence.findByProjectPhaseId(projectPhaseId);
		} catch (NoSuchProjectPhaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    return null;
	}	
}