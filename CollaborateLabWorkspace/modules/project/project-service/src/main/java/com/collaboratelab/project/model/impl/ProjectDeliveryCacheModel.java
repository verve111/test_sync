/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.project.model.ProjectDelivery;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing ProjectDelivery in entity cache.
 *
 * @author Claudio Carlenzoli
 * @see ProjectDelivery
 * @generated
 */
@ProviderType
public class ProjectDeliveryCacheModel implements CacheModel<ProjectDelivery>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ProjectDeliveryCacheModel)) {
			return false;
		}

		ProjectDeliveryCacheModel projectDeliveryCacheModel = (ProjectDeliveryCacheModel)obj;

		if (deliveryId == projectDeliveryCacheModel.deliveryId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, deliveryId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(57);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", deliveryId=");
		sb.append(deliveryId);
		sb.append(", stepId=");
		sb.append(stepId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", name=");
		sb.append(name);
		sb.append(", type=");
		sb.append(type);
		sb.append(", phase=");
		sb.append(phase);
		sb.append(", domain=");
		sb.append(domain);
		sb.append(", status=");
		sb.append(status);
		sb.append(", startDate=");
		sb.append(startDate);
		sb.append(", endDate=");
		sb.append(endDate);
		sb.append(", cost=");
		sb.append(cost);
		sb.append(", outcomeCategory=");
		sb.append(outcomeCategory);
		sb.append(", comment=");
		sb.append(comment);
		sb.append(", isAvailable=");
		sb.append(isAvailable);
		sb.append(", reviewerIsCompleted=");
		sb.append(reviewerIsCompleted);
		sb.append(", reviewerRate=");
		sb.append(reviewerRate);
		sb.append(", reviewerComment=");
		sb.append(reviewerComment);
		sb.append(", fundingSource=");
		sb.append(fundingSource);
		sb.append(", isEditPlanning=");
		sb.append(isEditPlanning);
		sb.append(", budget=");
		sb.append(budget);
		sb.append(", personResponsible=");
		sb.append(personResponsible);
		sb.append(", startPlanningDate=");
		sb.append(startPlanningDate);
		sb.append(", endPlanningDate=");
		sb.append(endPlanningDate);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ProjectDelivery toEntityModel() {
		ProjectDeliveryImpl projectDeliveryImpl = new ProjectDeliveryImpl();

		if (uuid == null) {
			projectDeliveryImpl.setUuid(StringPool.BLANK);
		}
		else {
			projectDeliveryImpl.setUuid(uuid);
		}

		projectDeliveryImpl.setDeliveryId(deliveryId);
		projectDeliveryImpl.setStepId(stepId);
		projectDeliveryImpl.setCompanyId(companyId);
		projectDeliveryImpl.setUserId(userId);

		if (userName == null) {
			projectDeliveryImpl.setUserName(StringPool.BLANK);
		}
		else {
			projectDeliveryImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			projectDeliveryImpl.setCreateDate(null);
		}
		else {
			projectDeliveryImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			projectDeliveryImpl.setModifiedDate(null);
		}
		else {
			projectDeliveryImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (name == null) {
			projectDeliveryImpl.setName(StringPool.BLANK);
		}
		else {
			projectDeliveryImpl.setName(name);
		}

		projectDeliveryImpl.setType(type);

		if (phase == null) {
			projectDeliveryImpl.setPhase(StringPool.BLANK);
		}
		else {
			projectDeliveryImpl.setPhase(phase);
		}

		if (domain == null) {
			projectDeliveryImpl.setDomain(StringPool.BLANK);
		}
		else {
			projectDeliveryImpl.setDomain(domain);
		}

		projectDeliveryImpl.setStatus(status);

		if (startDate == Long.MIN_VALUE) {
			projectDeliveryImpl.setStartDate(null);
		}
		else {
			projectDeliveryImpl.setStartDate(new Date(startDate));
		}

		if (endDate == Long.MIN_VALUE) {
			projectDeliveryImpl.setEndDate(null);
		}
		else {
			projectDeliveryImpl.setEndDate(new Date(endDate));
		}

		projectDeliveryImpl.setCost(cost);
		projectDeliveryImpl.setOutcomeCategory(outcomeCategory);

		if (comment == null) {
			projectDeliveryImpl.setComment(StringPool.BLANK);
		}
		else {
			projectDeliveryImpl.setComment(comment);
		}

		projectDeliveryImpl.setIsAvailable(isAvailable);
		projectDeliveryImpl.setReviewerIsCompleted(reviewerIsCompleted);
		projectDeliveryImpl.setReviewerRate(reviewerRate);

		if (reviewerComment == null) {
			projectDeliveryImpl.setReviewerComment(StringPool.BLANK);
		}
		else {
			projectDeliveryImpl.setReviewerComment(reviewerComment);
		}

		projectDeliveryImpl.setFundingSource(fundingSource);
		projectDeliveryImpl.setIsEditPlanning(isEditPlanning);
		projectDeliveryImpl.setBudget(budget);

		if (personResponsible == null) {
			projectDeliveryImpl.setPersonResponsible(StringPool.BLANK);
		}
		else {
			projectDeliveryImpl.setPersonResponsible(personResponsible);
		}

		if (startPlanningDate == Long.MIN_VALUE) {
			projectDeliveryImpl.setStartPlanningDate(null);
		}
		else {
			projectDeliveryImpl.setStartPlanningDate(new Date(startPlanningDate));
		}

		if (endPlanningDate == Long.MIN_VALUE) {
			projectDeliveryImpl.setEndPlanningDate(null);
		}
		else {
			projectDeliveryImpl.setEndPlanningDate(new Date(endPlanningDate));
		}

		projectDeliveryImpl.resetOriginalValues();

		return projectDeliveryImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		deliveryId = objectInput.readLong();

		stepId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		name = objectInput.readUTF();

		type = objectInput.readInt();
		phase = objectInput.readUTF();
		domain = objectInput.readUTF();

		status = objectInput.readInt();
		startDate = objectInput.readLong();
		endDate = objectInput.readLong();

		cost = objectInput.readInt();

		outcomeCategory = objectInput.readInt();
		comment = objectInput.readUTF();

		isAvailable = objectInput.readBoolean();

		reviewerIsCompleted = objectInput.readBoolean();

		reviewerRate = objectInput.readInt();
		reviewerComment = objectInput.readUTF();

		fundingSource = objectInput.readInt();

		isEditPlanning = objectInput.readBoolean();

		budget = objectInput.readDouble();
		personResponsible = objectInput.readUTF();
		startPlanningDate = objectInput.readLong();
		endPlanningDate = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(deliveryId);

		objectOutput.writeLong(stepId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (name == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(name);
		}

		objectOutput.writeInt(type);

		if (phase == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(phase);
		}

		if (domain == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(domain);
		}

		objectOutput.writeInt(status);
		objectOutput.writeLong(startDate);
		objectOutput.writeLong(endDate);

		objectOutput.writeInt(cost);

		objectOutput.writeInt(outcomeCategory);

		if (comment == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(comment);
		}

		objectOutput.writeBoolean(isAvailable);

		objectOutput.writeBoolean(reviewerIsCompleted);

		objectOutput.writeInt(reviewerRate);

		if (reviewerComment == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(reviewerComment);
		}

		objectOutput.writeInt(fundingSource);

		objectOutput.writeBoolean(isEditPlanning);

		objectOutput.writeDouble(budget);

		if (personResponsible == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(personResponsible);
		}

		objectOutput.writeLong(startPlanningDate);
		objectOutput.writeLong(endPlanningDate);
	}

	public String uuid;
	public long deliveryId;
	public long stepId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String name;
	public int type;
	public String phase;
	public String domain;
	public int status;
	public long startDate;
	public long endDate;
	public int cost;
	public int outcomeCategory;
	public String comment;
	public boolean isAvailable;
	public boolean reviewerIsCompleted;
	public int reviewerRate;
	public String reviewerComment;
	public int fundingSource;
	public boolean isEditPlanning;
	public double budget;
	public String personResponsible;
	public long startPlanningDate;
	public long endPlanningDate;
}