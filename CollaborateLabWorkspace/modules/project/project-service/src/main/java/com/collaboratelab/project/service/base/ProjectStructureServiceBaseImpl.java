/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.service.base;

import com.collaboratelab.project.model.ProjectStructure;
import com.collaboratelab.project.service.ProjectStructureService;
import com.collaboratelab.project.service.persistence.ProjectDeliveryPersistence;
import com.collaboratelab.project.service.persistence.ProjectDomainPersistence;
import com.collaboratelab.project.service.persistence.ProjectPersistence;
import com.collaboratelab.project.service.persistence.ProjectPhasePersistence;
import com.collaboratelab.project.service.persistence.ProjectStepPersistence;
import com.collaboratelab.project.service.persistence.ProjectStructurePersistence;

import com.liferay.asset.kernel.service.persistence.AssetEntryPersistence;
import com.liferay.asset.kernel.service.persistence.AssetTagPersistence;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.dao.db.DB;
import com.liferay.portal.kernel.dao.db.DBManagerUtil;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdate;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdateFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.module.framework.service.IdentifiableOSGiService;
import com.liferay.portal.kernel.service.BaseServiceImpl;
import com.liferay.portal.kernel.service.persistence.ClassNamePersistence;
import com.liferay.portal.kernel.service.persistence.UserPersistence;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import javax.sql.DataSource;

/**
 * Provides the base implementation for the project structure remote service.
 *
 * <p>
 * This implementation exists only as a container for the default service methods generated by ServiceBuilder. All custom service methods should be put in {@link com.collaboratelab.project.service.impl.ProjectStructureServiceImpl}.
 * </p>
 *
 * @author Claudio Carlenzoli
 * @see com.collaboratelab.project.service.impl.ProjectStructureServiceImpl
 * @see com.collaboratelab.project.service.ProjectStructureServiceUtil
 * @generated
 */
public abstract class ProjectStructureServiceBaseImpl extends BaseServiceImpl
	implements ProjectStructureService, IdentifiableOSGiService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link com.collaboratelab.project.service.ProjectStructureServiceUtil} to access the project structure remote service.
	 */

	/**
	 * Returns the project local service.
	 *
	 * @return the project local service
	 */
	public com.collaboratelab.project.service.ProjectLocalService getProjectLocalService() {
		return projectLocalService;
	}

	/**
	 * Sets the project local service.
	 *
	 * @param projectLocalService the project local service
	 */
	public void setProjectLocalService(
		com.collaboratelab.project.service.ProjectLocalService projectLocalService) {
		this.projectLocalService = projectLocalService;
	}

	/**
	 * Returns the project remote service.
	 *
	 * @return the project remote service
	 */
	public com.collaboratelab.project.service.ProjectService getProjectService() {
		return projectService;
	}

	/**
	 * Sets the project remote service.
	 *
	 * @param projectService the project remote service
	 */
	public void setProjectService(
		com.collaboratelab.project.service.ProjectService projectService) {
		this.projectService = projectService;
	}

	/**
	 * Returns the project persistence.
	 *
	 * @return the project persistence
	 */
	public ProjectPersistence getProjectPersistence() {
		return projectPersistence;
	}

	/**
	 * Sets the project persistence.
	 *
	 * @param projectPersistence the project persistence
	 */
	public void setProjectPersistence(ProjectPersistence projectPersistence) {
		this.projectPersistence = projectPersistence;
	}

	/**
	 * Returns the project delivery local service.
	 *
	 * @return the project delivery local service
	 */
	public com.collaboratelab.project.service.ProjectDeliveryLocalService getProjectDeliveryLocalService() {
		return projectDeliveryLocalService;
	}

	/**
	 * Sets the project delivery local service.
	 *
	 * @param projectDeliveryLocalService the project delivery local service
	 */
	public void setProjectDeliveryLocalService(
		com.collaboratelab.project.service.ProjectDeliveryLocalService projectDeliveryLocalService) {
		this.projectDeliveryLocalService = projectDeliveryLocalService;
	}

	/**
	 * Returns the project delivery remote service.
	 *
	 * @return the project delivery remote service
	 */
	public com.collaboratelab.project.service.ProjectDeliveryService getProjectDeliveryService() {
		return projectDeliveryService;
	}

	/**
	 * Sets the project delivery remote service.
	 *
	 * @param projectDeliveryService the project delivery remote service
	 */
	public void setProjectDeliveryService(
		com.collaboratelab.project.service.ProjectDeliveryService projectDeliveryService) {
		this.projectDeliveryService = projectDeliveryService;
	}

	/**
	 * Returns the project delivery persistence.
	 *
	 * @return the project delivery persistence
	 */
	public ProjectDeliveryPersistence getProjectDeliveryPersistence() {
		return projectDeliveryPersistence;
	}

	/**
	 * Sets the project delivery persistence.
	 *
	 * @param projectDeliveryPersistence the project delivery persistence
	 */
	public void setProjectDeliveryPersistence(
		ProjectDeliveryPersistence projectDeliveryPersistence) {
		this.projectDeliveryPersistence = projectDeliveryPersistence;
	}

	/**
	 * Returns the project domain local service.
	 *
	 * @return the project domain local service
	 */
	public com.collaboratelab.project.service.ProjectDomainLocalService getProjectDomainLocalService() {
		return projectDomainLocalService;
	}

	/**
	 * Sets the project domain local service.
	 *
	 * @param projectDomainLocalService the project domain local service
	 */
	public void setProjectDomainLocalService(
		com.collaboratelab.project.service.ProjectDomainLocalService projectDomainLocalService) {
		this.projectDomainLocalService = projectDomainLocalService;
	}

	/**
	 * Returns the project domain remote service.
	 *
	 * @return the project domain remote service
	 */
	public com.collaboratelab.project.service.ProjectDomainService getProjectDomainService() {
		return projectDomainService;
	}

	/**
	 * Sets the project domain remote service.
	 *
	 * @param projectDomainService the project domain remote service
	 */
	public void setProjectDomainService(
		com.collaboratelab.project.service.ProjectDomainService projectDomainService) {
		this.projectDomainService = projectDomainService;
	}

	/**
	 * Returns the project domain persistence.
	 *
	 * @return the project domain persistence
	 */
	public ProjectDomainPersistence getProjectDomainPersistence() {
		return projectDomainPersistence;
	}

	/**
	 * Sets the project domain persistence.
	 *
	 * @param projectDomainPersistence the project domain persistence
	 */
	public void setProjectDomainPersistence(
		ProjectDomainPersistence projectDomainPersistence) {
		this.projectDomainPersistence = projectDomainPersistence;
	}

	/**
	 * Returns the project phase local service.
	 *
	 * @return the project phase local service
	 */
	public com.collaboratelab.project.service.ProjectPhaseLocalService getProjectPhaseLocalService() {
		return projectPhaseLocalService;
	}

	/**
	 * Sets the project phase local service.
	 *
	 * @param projectPhaseLocalService the project phase local service
	 */
	public void setProjectPhaseLocalService(
		com.collaboratelab.project.service.ProjectPhaseLocalService projectPhaseLocalService) {
		this.projectPhaseLocalService = projectPhaseLocalService;
	}

	/**
	 * Returns the project phase remote service.
	 *
	 * @return the project phase remote service
	 */
	public com.collaboratelab.project.service.ProjectPhaseService getProjectPhaseService() {
		return projectPhaseService;
	}

	/**
	 * Sets the project phase remote service.
	 *
	 * @param projectPhaseService the project phase remote service
	 */
	public void setProjectPhaseService(
		com.collaboratelab.project.service.ProjectPhaseService projectPhaseService) {
		this.projectPhaseService = projectPhaseService;
	}

	/**
	 * Returns the project phase persistence.
	 *
	 * @return the project phase persistence
	 */
	public ProjectPhasePersistence getProjectPhasePersistence() {
		return projectPhasePersistence;
	}

	/**
	 * Sets the project phase persistence.
	 *
	 * @param projectPhasePersistence the project phase persistence
	 */
	public void setProjectPhasePersistence(
		ProjectPhasePersistence projectPhasePersistence) {
		this.projectPhasePersistence = projectPhasePersistence;
	}

	/**
	 * Returns the project step local service.
	 *
	 * @return the project step local service
	 */
	public com.collaboratelab.project.service.ProjectStepLocalService getProjectStepLocalService() {
		return projectStepLocalService;
	}

	/**
	 * Sets the project step local service.
	 *
	 * @param projectStepLocalService the project step local service
	 */
	public void setProjectStepLocalService(
		com.collaboratelab.project.service.ProjectStepLocalService projectStepLocalService) {
		this.projectStepLocalService = projectStepLocalService;
	}

	/**
	 * Returns the project step remote service.
	 *
	 * @return the project step remote service
	 */
	public com.collaboratelab.project.service.ProjectStepService getProjectStepService() {
		return projectStepService;
	}

	/**
	 * Sets the project step remote service.
	 *
	 * @param projectStepService the project step remote service
	 */
	public void setProjectStepService(
		com.collaboratelab.project.service.ProjectStepService projectStepService) {
		this.projectStepService = projectStepService;
	}

	/**
	 * Returns the project step persistence.
	 *
	 * @return the project step persistence
	 */
	public ProjectStepPersistence getProjectStepPersistence() {
		return projectStepPersistence;
	}

	/**
	 * Sets the project step persistence.
	 *
	 * @param projectStepPersistence the project step persistence
	 */
	public void setProjectStepPersistence(
		ProjectStepPersistence projectStepPersistence) {
		this.projectStepPersistence = projectStepPersistence;
	}

	/**
	 * Returns the project structure local service.
	 *
	 * @return the project structure local service
	 */
	public com.collaboratelab.project.service.ProjectStructureLocalService getProjectStructureLocalService() {
		return projectStructureLocalService;
	}

	/**
	 * Sets the project structure local service.
	 *
	 * @param projectStructureLocalService the project structure local service
	 */
	public void setProjectStructureLocalService(
		com.collaboratelab.project.service.ProjectStructureLocalService projectStructureLocalService) {
		this.projectStructureLocalService = projectStructureLocalService;
	}

	/**
	 * Returns the project structure remote service.
	 *
	 * @return the project structure remote service
	 */
	public ProjectStructureService getProjectStructureService() {
		return projectStructureService;
	}

	/**
	 * Sets the project structure remote service.
	 *
	 * @param projectStructureService the project structure remote service
	 */
	public void setProjectStructureService(
		ProjectStructureService projectStructureService) {
		this.projectStructureService = projectStructureService;
	}

	/**
	 * Returns the project structure persistence.
	 *
	 * @return the project structure persistence
	 */
	public ProjectStructurePersistence getProjectStructurePersistence() {
		return projectStructurePersistence;
	}

	/**
	 * Sets the project structure persistence.
	 *
	 * @param projectStructurePersistence the project structure persistence
	 */
	public void setProjectStructurePersistence(
		ProjectStructurePersistence projectStructurePersistence) {
		this.projectStructurePersistence = projectStructurePersistence;
	}

	/**
	 * Returns the counter local service.
	 *
	 * @return the counter local service
	 */
	public com.liferay.counter.kernel.service.CounterLocalService getCounterLocalService() {
		return counterLocalService;
	}

	/**
	 * Sets the counter local service.
	 *
	 * @param counterLocalService the counter local service
	 */
	public void setCounterLocalService(
		com.liferay.counter.kernel.service.CounterLocalService counterLocalService) {
		this.counterLocalService = counterLocalService;
	}

	/**
	 * Returns the class name local service.
	 *
	 * @return the class name local service
	 */
	public com.liferay.portal.kernel.service.ClassNameLocalService getClassNameLocalService() {
		return classNameLocalService;
	}

	/**
	 * Sets the class name local service.
	 *
	 * @param classNameLocalService the class name local service
	 */
	public void setClassNameLocalService(
		com.liferay.portal.kernel.service.ClassNameLocalService classNameLocalService) {
		this.classNameLocalService = classNameLocalService;
	}

	/**
	 * Returns the class name remote service.
	 *
	 * @return the class name remote service
	 */
	public com.liferay.portal.kernel.service.ClassNameService getClassNameService() {
		return classNameService;
	}

	/**
	 * Sets the class name remote service.
	 *
	 * @param classNameService the class name remote service
	 */
	public void setClassNameService(
		com.liferay.portal.kernel.service.ClassNameService classNameService) {
		this.classNameService = classNameService;
	}

	/**
	 * Returns the class name persistence.
	 *
	 * @return the class name persistence
	 */
	public ClassNamePersistence getClassNamePersistence() {
		return classNamePersistence;
	}

	/**
	 * Sets the class name persistence.
	 *
	 * @param classNamePersistence the class name persistence
	 */
	public void setClassNamePersistence(
		ClassNamePersistence classNamePersistence) {
		this.classNamePersistence = classNamePersistence;
	}

	/**
	 * Returns the resource local service.
	 *
	 * @return the resource local service
	 */
	public com.liferay.portal.kernel.service.ResourceLocalService getResourceLocalService() {
		return resourceLocalService;
	}

	/**
	 * Sets the resource local service.
	 *
	 * @param resourceLocalService the resource local service
	 */
	public void setResourceLocalService(
		com.liferay.portal.kernel.service.ResourceLocalService resourceLocalService) {
		this.resourceLocalService = resourceLocalService;
	}

	/**
	 * Returns the user local service.
	 *
	 * @return the user local service
	 */
	public com.liferay.portal.kernel.service.UserLocalService getUserLocalService() {
		return userLocalService;
	}

	/**
	 * Sets the user local service.
	 *
	 * @param userLocalService the user local service
	 */
	public void setUserLocalService(
		com.liferay.portal.kernel.service.UserLocalService userLocalService) {
		this.userLocalService = userLocalService;
	}

	/**
	 * Returns the user remote service.
	 *
	 * @return the user remote service
	 */
	public com.liferay.portal.kernel.service.UserService getUserService() {
		return userService;
	}

	/**
	 * Sets the user remote service.
	 *
	 * @param userService the user remote service
	 */
	public void setUserService(
		com.liferay.portal.kernel.service.UserService userService) {
		this.userService = userService;
	}

	/**
	 * Returns the user persistence.
	 *
	 * @return the user persistence
	 */
	public UserPersistence getUserPersistence() {
		return userPersistence;
	}

	/**
	 * Sets the user persistence.
	 *
	 * @param userPersistence the user persistence
	 */
	public void setUserPersistence(UserPersistence userPersistence) {
		this.userPersistence = userPersistence;
	}

	/**
	 * Returns the asset entry local service.
	 *
	 * @return the asset entry local service
	 */
	public com.liferay.asset.kernel.service.AssetEntryLocalService getAssetEntryLocalService() {
		return assetEntryLocalService;
	}

	/**
	 * Sets the asset entry local service.
	 *
	 * @param assetEntryLocalService the asset entry local service
	 */
	public void setAssetEntryLocalService(
		com.liferay.asset.kernel.service.AssetEntryLocalService assetEntryLocalService) {
		this.assetEntryLocalService = assetEntryLocalService;
	}

	/**
	 * Returns the asset entry remote service.
	 *
	 * @return the asset entry remote service
	 */
	public com.liferay.asset.kernel.service.AssetEntryService getAssetEntryService() {
		return assetEntryService;
	}

	/**
	 * Sets the asset entry remote service.
	 *
	 * @param assetEntryService the asset entry remote service
	 */
	public void setAssetEntryService(
		com.liferay.asset.kernel.service.AssetEntryService assetEntryService) {
		this.assetEntryService = assetEntryService;
	}

	/**
	 * Returns the asset entry persistence.
	 *
	 * @return the asset entry persistence
	 */
	public AssetEntryPersistence getAssetEntryPersistence() {
		return assetEntryPersistence;
	}

	/**
	 * Sets the asset entry persistence.
	 *
	 * @param assetEntryPersistence the asset entry persistence
	 */
	public void setAssetEntryPersistence(
		AssetEntryPersistence assetEntryPersistence) {
		this.assetEntryPersistence = assetEntryPersistence;
	}

	/**
	 * Returns the asset tag local service.
	 *
	 * @return the asset tag local service
	 */
	public com.liferay.asset.kernel.service.AssetTagLocalService getAssetTagLocalService() {
		return assetTagLocalService;
	}

	/**
	 * Sets the asset tag local service.
	 *
	 * @param assetTagLocalService the asset tag local service
	 */
	public void setAssetTagLocalService(
		com.liferay.asset.kernel.service.AssetTagLocalService assetTagLocalService) {
		this.assetTagLocalService = assetTagLocalService;
	}

	/**
	 * Returns the asset tag remote service.
	 *
	 * @return the asset tag remote service
	 */
	public com.liferay.asset.kernel.service.AssetTagService getAssetTagService() {
		return assetTagService;
	}

	/**
	 * Sets the asset tag remote service.
	 *
	 * @param assetTagService the asset tag remote service
	 */
	public void setAssetTagService(
		com.liferay.asset.kernel.service.AssetTagService assetTagService) {
		this.assetTagService = assetTagService;
	}

	/**
	 * Returns the asset tag persistence.
	 *
	 * @return the asset tag persistence
	 */
	public AssetTagPersistence getAssetTagPersistence() {
		return assetTagPersistence;
	}

	/**
	 * Sets the asset tag persistence.
	 *
	 * @param assetTagPersistence the asset tag persistence
	 */
	public void setAssetTagPersistence(AssetTagPersistence assetTagPersistence) {
		this.assetTagPersistence = assetTagPersistence;
	}

	public void afterPropertiesSet() {
	}

	public void destroy() {
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return ProjectStructureService.class.getName();
	}

	protected Class<?> getModelClass() {
		return ProjectStructure.class;
	}

	protected String getModelClassName() {
		return ProjectStructure.class.getName();
	}

	/**
	 * Performs a SQL query.
	 *
	 * @param sql the sql query
	 */
	protected void runSQL(String sql) {
		try {
			DataSource dataSource = projectStructurePersistence.getDataSource();

			DB db = DBManagerUtil.getDB();

			sql = db.buildSQL(sql);
			sql = PortalUtil.transformSQL(sql);

			SqlUpdate sqlUpdate = SqlUpdateFactoryUtil.getSqlUpdate(dataSource,
					sql);

			sqlUpdate.update();
		}
		catch (Exception e) {
			throw new SystemException(e);
		}
	}

	@BeanReference(type = com.collaboratelab.project.service.ProjectLocalService.class)
	protected com.collaboratelab.project.service.ProjectLocalService projectLocalService;
	@BeanReference(type = com.collaboratelab.project.service.ProjectService.class)
	protected com.collaboratelab.project.service.ProjectService projectService;
	@BeanReference(type = ProjectPersistence.class)
	protected ProjectPersistence projectPersistence;
	@BeanReference(type = com.collaboratelab.project.service.ProjectDeliveryLocalService.class)
	protected com.collaboratelab.project.service.ProjectDeliveryLocalService projectDeliveryLocalService;
	@BeanReference(type = com.collaboratelab.project.service.ProjectDeliveryService.class)
	protected com.collaboratelab.project.service.ProjectDeliveryService projectDeliveryService;
	@BeanReference(type = ProjectDeliveryPersistence.class)
	protected ProjectDeliveryPersistence projectDeliveryPersistence;
	@BeanReference(type = com.collaboratelab.project.service.ProjectDomainLocalService.class)
	protected com.collaboratelab.project.service.ProjectDomainLocalService projectDomainLocalService;
	@BeanReference(type = com.collaboratelab.project.service.ProjectDomainService.class)
	protected com.collaboratelab.project.service.ProjectDomainService projectDomainService;
	@BeanReference(type = ProjectDomainPersistence.class)
	protected ProjectDomainPersistence projectDomainPersistence;
	@BeanReference(type = com.collaboratelab.project.service.ProjectPhaseLocalService.class)
	protected com.collaboratelab.project.service.ProjectPhaseLocalService projectPhaseLocalService;
	@BeanReference(type = com.collaboratelab.project.service.ProjectPhaseService.class)
	protected com.collaboratelab.project.service.ProjectPhaseService projectPhaseService;
	@BeanReference(type = ProjectPhasePersistence.class)
	protected ProjectPhasePersistence projectPhasePersistence;
	@BeanReference(type = com.collaboratelab.project.service.ProjectStepLocalService.class)
	protected com.collaboratelab.project.service.ProjectStepLocalService projectStepLocalService;
	@BeanReference(type = com.collaboratelab.project.service.ProjectStepService.class)
	protected com.collaboratelab.project.service.ProjectStepService projectStepService;
	@BeanReference(type = ProjectStepPersistence.class)
	protected ProjectStepPersistence projectStepPersistence;
	@BeanReference(type = com.collaboratelab.project.service.ProjectStructureLocalService.class)
	protected com.collaboratelab.project.service.ProjectStructureLocalService projectStructureLocalService;
	@BeanReference(type = ProjectStructureService.class)
	protected ProjectStructureService projectStructureService;
	@BeanReference(type = ProjectStructurePersistence.class)
	protected ProjectStructurePersistence projectStructurePersistence;
	@ServiceReference(type = com.liferay.counter.kernel.service.CounterLocalService.class)
	protected com.liferay.counter.kernel.service.CounterLocalService counterLocalService;
	@ServiceReference(type = com.liferay.portal.kernel.service.ClassNameLocalService.class)
	protected com.liferay.portal.kernel.service.ClassNameLocalService classNameLocalService;
	@ServiceReference(type = com.liferay.portal.kernel.service.ClassNameService.class)
	protected com.liferay.portal.kernel.service.ClassNameService classNameService;
	@ServiceReference(type = ClassNamePersistence.class)
	protected ClassNamePersistence classNamePersistence;
	@ServiceReference(type = com.liferay.portal.kernel.service.ResourceLocalService.class)
	protected com.liferay.portal.kernel.service.ResourceLocalService resourceLocalService;
	@ServiceReference(type = com.liferay.portal.kernel.service.UserLocalService.class)
	protected com.liferay.portal.kernel.service.UserLocalService userLocalService;
	@ServiceReference(type = com.liferay.portal.kernel.service.UserService.class)
	protected com.liferay.portal.kernel.service.UserService userService;
	@ServiceReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	@ServiceReference(type = com.liferay.asset.kernel.service.AssetEntryLocalService.class)
	protected com.liferay.asset.kernel.service.AssetEntryLocalService assetEntryLocalService;
	@ServiceReference(type = com.liferay.asset.kernel.service.AssetEntryService.class)
	protected com.liferay.asset.kernel.service.AssetEntryService assetEntryService;
	@ServiceReference(type = AssetEntryPersistence.class)
	protected AssetEntryPersistence assetEntryPersistence;
	@ServiceReference(type = com.liferay.asset.kernel.service.AssetTagLocalService.class)
	protected com.liferay.asset.kernel.service.AssetTagLocalService assetTagLocalService;
	@ServiceReference(type = com.liferay.asset.kernel.service.AssetTagService.class)
	protected com.liferay.asset.kernel.service.AssetTagService assetTagService;
	@ServiceReference(type = AssetTagPersistence.class)
	protected AssetTagPersistence assetTagPersistence;
}