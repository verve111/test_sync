/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.project.model.ProjectStep;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing ProjectStep in entity cache.
 *
 * @author Claudio Carlenzoli
 * @see ProjectStep
 * @generated
 */
@ProviderType
public class ProjectStepCacheModel implements CacheModel<ProjectStep>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ProjectStepCacheModel)) {
			return false;
		}

		ProjectStepCacheModel projectStepCacheModel = (ProjectStepCacheModel)obj;

		if (projectStepId == projectStepCacheModel.projectStepId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, projectStepId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", projectStepId=");
		sb.append(projectStepId);
		sb.append(", projectId=");
		sb.append(projectId);
		sb.append(", projectDomainId=");
		sb.append(projectDomainId);
		sb.append(", projectPhaseId=");
		sb.append(projectPhaseId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", percentage=");
		sb.append(percentage);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ProjectStep toEntityModel() {
		ProjectStepImpl projectStepImpl = new ProjectStepImpl();

		if (uuid == null) {
			projectStepImpl.setUuid(StringPool.BLANK);
		}
		else {
			projectStepImpl.setUuid(uuid);
		}

		projectStepImpl.setProjectStepId(projectStepId);
		projectStepImpl.setProjectId(projectId);
		projectStepImpl.setProjectDomainId(projectDomainId);
		projectStepImpl.setProjectPhaseId(projectPhaseId);
		projectStepImpl.setCompanyId(companyId);
		projectStepImpl.setUserId(userId);

		if (userName == null) {
			projectStepImpl.setUserName(StringPool.BLANK);
		}
		else {
			projectStepImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			projectStepImpl.setCreateDate(null);
		}
		else {
			projectStepImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			projectStepImpl.setModifiedDate(null);
		}
		else {
			projectStepImpl.setModifiedDate(new Date(modifiedDate));
		}

		projectStepImpl.setPercentage(percentage);

		projectStepImpl.resetOriginalValues();

		return projectStepImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		projectStepId = objectInput.readLong();

		projectId = objectInput.readLong();

		projectDomainId = objectInput.readLong();

		projectPhaseId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		percentage = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(projectStepId);

		objectOutput.writeLong(projectId);

		objectOutput.writeLong(projectDomainId);

		objectOutput.writeLong(projectPhaseId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(percentage);
	}

	public String uuid;
	public long projectStepId;
	public long projectId;
	public long projectDomainId;
	public long projectPhaseId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long percentage;
}