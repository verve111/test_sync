/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.project.exception.NoSuchProjectDomainException;
import com.collaboratelab.project.model.ProjectDomain;
import com.collaboratelab.project.model.impl.ProjectDomainImpl;
import com.collaboratelab.project.model.impl.ProjectDomainModelImpl;
import com.collaboratelab.project.service.persistence.ProjectDomainPersistence;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.CompanyProvider;
import com.liferay.portal.kernel.service.persistence.CompanyProviderWrapper;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the project domain service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Claudio Carlenzoli
 * @see ProjectDomainPersistence
 * @see com.collaboratelab.project.service.persistence.ProjectDomainUtil
 * @generated
 */
@ProviderType
public class ProjectDomainPersistenceImpl extends BasePersistenceImpl<ProjectDomain>
	implements ProjectDomainPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ProjectDomainUtil} to access the project domain persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ProjectDomainImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDomainModelImpl.FINDER_CACHE_ENABLED,
			ProjectDomainImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDomainModelImpl.FINDER_CACHE_ENABLED,
			ProjectDomainImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDomainModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID = new FinderPath(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDomainModelImpl.FINDER_CACHE_ENABLED,
			ProjectDomainImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByUuid",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID = new FinderPath(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDomainModelImpl.FINDER_CACHE_ENABLED,
			ProjectDomainImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByUuid", new String[] { String.class.getName() },
			ProjectDomainModelImpl.UUID_COLUMN_BITMASK |
			ProjectDomainModelImpl.NAME_COLUMN_BITMASK |
			ProjectDomainModelImpl.PRIORITY_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID = new FinderPath(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDomainModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] { String.class.getName() });

	/**
	 * Returns all the project domains where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching project domains
	 */
	@Override
	public List<ProjectDomain> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the project domains where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of project domains
	 * @param end the upper bound of the range of project domains (not inclusive)
	 * @return the range of matching project domains
	 */
	@Override
	public List<ProjectDomain> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the project domains where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of project domains
	 * @param end the upper bound of the range of project domains (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching project domains
	 */
	@Override
	public List<ProjectDomain> findByUuid(String uuid, int start, int end,
		OrderByComparator<ProjectDomain> orderByComparator) {
		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the project domains where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of project domains
	 * @param end the upper bound of the range of project domains (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching project domains
	 */
	@Override
	public List<ProjectDomain> findByUuid(String uuid, int start, int end,
		OrderByComparator<ProjectDomain> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid, start, end, orderByComparator };
		}

		List<ProjectDomain> list = null;

		if (retrieveFromCache) {
			list = (List<ProjectDomain>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ProjectDomain projectDomain : list) {
					if (!Objects.equals(uuid, projectDomain.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PROJECTDOMAIN_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProjectDomainModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<ProjectDomain>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ProjectDomain>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first project domain in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project domain
	 * @throws NoSuchProjectDomainException if a matching project domain could not be found
	 */
	@Override
	public ProjectDomain findByUuid_First(String uuid,
		OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException {
		ProjectDomain projectDomain = fetchByUuid_First(uuid, orderByComparator);

		if (projectDomain != null) {
			return projectDomain;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectDomainException(msg.toString());
	}

	/**
	 * Returns the first project domain in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project domain, or <code>null</code> if a matching project domain could not be found
	 */
	@Override
	public ProjectDomain fetchByUuid_First(String uuid,
		OrderByComparator<ProjectDomain> orderByComparator) {
		List<ProjectDomain> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last project domain in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project domain
	 * @throws NoSuchProjectDomainException if a matching project domain could not be found
	 */
	@Override
	public ProjectDomain findByUuid_Last(String uuid,
		OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException {
		ProjectDomain projectDomain = fetchByUuid_Last(uuid, orderByComparator);

		if (projectDomain != null) {
			return projectDomain;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectDomainException(msg.toString());
	}

	/**
	 * Returns the last project domain in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project domain, or <code>null</code> if a matching project domain could not be found
	 */
	@Override
	public ProjectDomain fetchByUuid_Last(String uuid,
		OrderByComparator<ProjectDomain> orderByComparator) {
		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<ProjectDomain> list = findByUuid(uuid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the project domains before and after the current project domain in the ordered set where uuid = &#63;.
	 *
	 * @param projectDomainId the primary key of the current project domain
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next project domain
	 * @throws NoSuchProjectDomainException if a project domain with the primary key could not be found
	 */
	@Override
	public ProjectDomain[] findByUuid_PrevAndNext(long projectDomainId,
		String uuid, OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException {
		ProjectDomain projectDomain = findByPrimaryKey(projectDomainId);

		Session session = null;

		try {
			session = openSession();

			ProjectDomain[] array = new ProjectDomainImpl[3];

			array[0] = getByUuid_PrevAndNext(session, projectDomain, uuid,
					orderByComparator, true);

			array[1] = projectDomain;

			array[2] = getByUuid_PrevAndNext(session, projectDomain, uuid,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ProjectDomain getByUuid_PrevAndNext(Session session,
		ProjectDomain projectDomain, String uuid,
		OrderByComparator<ProjectDomain> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PROJECTDOMAIN_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProjectDomainModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(projectDomain);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ProjectDomain> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the project domains where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (ProjectDomain projectDomain : findByUuid(uuid, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(projectDomain);
		}
	}

	/**
	 * Returns the number of project domains where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching project domains
	 */
	@Override
	public int countByUuid(String uuid) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID;

		Object[] finderArgs = new Object[] { uuid };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PROJECTDOMAIN_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_1 = "projectDomain.uuid IS NULL";
	private static final String _FINDER_COLUMN_UUID_UUID_2 = "projectDomain.uuid = ?";
	private static final String _FINDER_COLUMN_UUID_UUID_3 = "(projectDomain.uuid IS NULL OR projectDomain.uuid = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C = new FinderPath(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDomainModelImpl.FINDER_CACHE_ENABLED,
			ProjectDomainImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C =
		new FinderPath(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDomainModelImpl.FINDER_CACHE_ENABLED,
			ProjectDomainImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() },
			ProjectDomainModelImpl.UUID_COLUMN_BITMASK |
			ProjectDomainModelImpl.COMPANYID_COLUMN_BITMASK |
			ProjectDomainModelImpl.NAME_COLUMN_BITMASK |
			ProjectDomainModelImpl.PRIORITY_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_C = new FinderPath(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDomainModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns all the project domains where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching project domains
	 */
	@Override
	public List<ProjectDomain> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(uuid, companyId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the project domains where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of project domains
	 * @param end the upper bound of the range of project domains (not inclusive)
	 * @return the range of matching project domains
	 */
	@Override
	public List<ProjectDomain> findByUuid_C(String uuid, long companyId,
		int start, int end) {
		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the project domains where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of project domains
	 * @param end the upper bound of the range of project domains (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching project domains
	 */
	@Override
	public List<ProjectDomain> findByUuid_C(String uuid, long companyId,
		int start, int end, OrderByComparator<ProjectDomain> orderByComparator) {
		return findByUuid_C(uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the project domains where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of project domains
	 * @param end the upper bound of the range of project domains (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching project domains
	 */
	@Override
	public List<ProjectDomain> findByUuid_C(String uuid, long companyId,
		int start, int end, OrderByComparator<ProjectDomain> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] { uuid, companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] {
					uuid, companyId,
					
					start, end, orderByComparator
				};
		}

		List<ProjectDomain> list = null;

		if (retrieveFromCache) {
			list = (List<ProjectDomain>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ProjectDomain projectDomain : list) {
					if (!Objects.equals(uuid, projectDomain.getUuid()) ||
							(companyId != projectDomain.getCompanyId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_PROJECTDOMAIN_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProjectDomainModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				if (!pagination) {
					list = (List<ProjectDomain>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ProjectDomain>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first project domain in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project domain
	 * @throws NoSuchProjectDomainException if a matching project domain could not be found
	 */
	@Override
	public ProjectDomain findByUuid_C_First(String uuid, long companyId,
		OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException {
		ProjectDomain projectDomain = fetchByUuid_C_First(uuid, companyId,
				orderByComparator);

		if (projectDomain != null) {
			return projectDomain;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectDomainException(msg.toString());
	}

	/**
	 * Returns the first project domain in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project domain, or <code>null</code> if a matching project domain could not be found
	 */
	@Override
	public ProjectDomain fetchByUuid_C_First(String uuid, long companyId,
		OrderByComparator<ProjectDomain> orderByComparator) {
		List<ProjectDomain> list = findByUuid_C(uuid, companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last project domain in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project domain
	 * @throws NoSuchProjectDomainException if a matching project domain could not be found
	 */
	@Override
	public ProjectDomain findByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException {
		ProjectDomain projectDomain = fetchByUuid_C_Last(uuid, companyId,
				orderByComparator);

		if (projectDomain != null) {
			return projectDomain;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectDomainException(msg.toString());
	}

	/**
	 * Returns the last project domain in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project domain, or <code>null</code> if a matching project domain could not be found
	 */
	@Override
	public ProjectDomain fetchByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<ProjectDomain> orderByComparator) {
		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<ProjectDomain> list = findByUuid_C(uuid, companyId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the project domains before and after the current project domain in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param projectDomainId the primary key of the current project domain
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next project domain
	 * @throws NoSuchProjectDomainException if a project domain with the primary key could not be found
	 */
	@Override
	public ProjectDomain[] findByUuid_C_PrevAndNext(long projectDomainId,
		String uuid, long companyId,
		OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException {
		ProjectDomain projectDomain = findByPrimaryKey(projectDomainId);

		Session session = null;

		try {
			session = openSession();

			ProjectDomain[] array = new ProjectDomainImpl[3];

			array[0] = getByUuid_C_PrevAndNext(session, projectDomain, uuid,
					companyId, orderByComparator, true);

			array[1] = projectDomain;

			array[2] = getByUuid_C_PrevAndNext(session, projectDomain, uuid,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ProjectDomain getByUuid_C_PrevAndNext(Session session,
		ProjectDomain projectDomain, String uuid, long companyId,
		OrderByComparator<ProjectDomain> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_PROJECTDOMAIN_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProjectDomainModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(projectDomain);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ProjectDomain> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the project domains where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (ProjectDomain projectDomain : findByUuid_C(uuid, companyId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(projectDomain);
		}
	}

	/**
	 * Returns the number of project domains where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching project domains
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_C;

		Object[] finderArgs = new Object[] { uuid, companyId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_PROJECTDOMAIN_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_1 = "projectDomain.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_2 = "projectDomain.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_3 = "(projectDomain.uuid IS NULL OR projectDomain.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 = "projectDomain.companyId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYID =
		new FinderPath(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDomainModelImpl.FINDER_CACHE_ENABLED,
			ProjectDomainImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByCompanyId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID =
		new FinderPath(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDomainModelImpl.FINDER_CACHE_ENABLED,
			ProjectDomainImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByCompanyId", new String[] { Long.class.getName() },
			ProjectDomainModelImpl.COMPANYID_COLUMN_BITMASK |
			ProjectDomainModelImpl.NAME_COLUMN_BITMASK |
			ProjectDomainModelImpl.PRIORITY_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANYID = new FinderPath(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDomainModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCompanyId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the project domains where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching project domains
	 */
	@Override
	public List<ProjectDomain> findByCompanyId(long companyId) {
		return findByCompanyId(companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the project domains where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of project domains
	 * @param end the upper bound of the range of project domains (not inclusive)
	 * @return the range of matching project domains
	 */
	@Override
	public List<ProjectDomain> findByCompanyId(long companyId, int start,
		int end) {
		return findByCompanyId(companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the project domains where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of project domains
	 * @param end the upper bound of the range of project domains (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching project domains
	 */
	@Override
	public List<ProjectDomain> findByCompanyId(long companyId, int start,
		int end, OrderByComparator<ProjectDomain> orderByComparator) {
		return findByCompanyId(companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the project domains where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of project domains
	 * @param end the upper bound of the range of project domains (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching project domains
	 */
	@Override
	public List<ProjectDomain> findByCompanyId(long companyId, int start,
		int end, OrderByComparator<ProjectDomain> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID;
			finderArgs = new Object[] { companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYID;
			finderArgs = new Object[] { companyId, start, end, orderByComparator };
		}

		List<ProjectDomain> list = null;

		if (retrieveFromCache) {
			list = (List<ProjectDomain>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ProjectDomain projectDomain : list) {
					if ((companyId != projectDomain.getCompanyId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PROJECTDOMAIN_WHERE);

			query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProjectDomainModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (!pagination) {
					list = (List<ProjectDomain>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ProjectDomain>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first project domain in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project domain
	 * @throws NoSuchProjectDomainException if a matching project domain could not be found
	 */
	@Override
	public ProjectDomain findByCompanyId_First(long companyId,
		OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException {
		ProjectDomain projectDomain = fetchByCompanyId_First(companyId,
				orderByComparator);

		if (projectDomain != null) {
			return projectDomain;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectDomainException(msg.toString());
	}

	/**
	 * Returns the first project domain in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project domain, or <code>null</code> if a matching project domain could not be found
	 */
	@Override
	public ProjectDomain fetchByCompanyId_First(long companyId,
		OrderByComparator<ProjectDomain> orderByComparator) {
		List<ProjectDomain> list = findByCompanyId(companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last project domain in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project domain
	 * @throws NoSuchProjectDomainException if a matching project domain could not be found
	 */
	@Override
	public ProjectDomain findByCompanyId_Last(long companyId,
		OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException {
		ProjectDomain projectDomain = fetchByCompanyId_Last(companyId,
				orderByComparator);

		if (projectDomain != null) {
			return projectDomain;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectDomainException(msg.toString());
	}

	/**
	 * Returns the last project domain in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project domain, or <code>null</code> if a matching project domain could not be found
	 */
	@Override
	public ProjectDomain fetchByCompanyId_Last(long companyId,
		OrderByComparator<ProjectDomain> orderByComparator) {
		int count = countByCompanyId(companyId);

		if (count == 0) {
			return null;
		}

		List<ProjectDomain> list = findByCompanyId(companyId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the project domains before and after the current project domain in the ordered set where companyId = &#63;.
	 *
	 * @param projectDomainId the primary key of the current project domain
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next project domain
	 * @throws NoSuchProjectDomainException if a project domain with the primary key could not be found
	 */
	@Override
	public ProjectDomain[] findByCompanyId_PrevAndNext(long projectDomainId,
		long companyId, OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException {
		ProjectDomain projectDomain = findByPrimaryKey(projectDomainId);

		Session session = null;

		try {
			session = openSession();

			ProjectDomain[] array = new ProjectDomainImpl[3];

			array[0] = getByCompanyId_PrevAndNext(session, projectDomain,
					companyId, orderByComparator, true);

			array[1] = projectDomain;

			array[2] = getByCompanyId_PrevAndNext(session, projectDomain,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ProjectDomain getByCompanyId_PrevAndNext(Session session,
		ProjectDomain projectDomain, long companyId,
		OrderByComparator<ProjectDomain> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PROJECTDOMAIN_WHERE);

		query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProjectDomainModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(projectDomain);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ProjectDomain> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the project domains where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 */
	@Override
	public void removeByCompanyId(long companyId) {
		for (ProjectDomain projectDomain : findByCompanyId(companyId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(projectDomain);
		}
	}

	/**
	 * Returns the number of project domains where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching project domains
	 */
	@Override
	public int countByCompanyId(long companyId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_COMPANYID;

		Object[] finderArgs = new Object[] { companyId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PROJECTDOMAIN_WHERE);

			query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COMPANYID_COMPANYID_2 = "projectDomain.companyId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER = new FinderPath(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDomainModelImpl.FINDER_CACHE_ENABLED,
			ProjectDomainImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByCompanyIdIdentifier",
			new String[] { Long.class.getName(), String.class.getName() },
			ProjectDomainModelImpl.COMPANYID_COLUMN_BITMASK |
			ProjectDomainModelImpl.IDENTIFIER_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANYIDIDENTIFIER = new FinderPath(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDomainModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByCompanyIdIdentifier",
			new String[] { Long.class.getName(), String.class.getName() });

	/**
	 * Returns the project domain where companyId = &#63; and identifier = &#63; or throws a {@link NoSuchProjectDomainException} if it could not be found.
	 *
	 * @param companyId the company ID
	 * @param identifier the identifier
	 * @return the matching project domain
	 * @throws NoSuchProjectDomainException if a matching project domain could not be found
	 */
	@Override
	public ProjectDomain findByCompanyIdIdentifier(long companyId,
		String identifier) throws NoSuchProjectDomainException {
		ProjectDomain projectDomain = fetchByCompanyIdIdentifier(companyId,
				identifier);

		if (projectDomain == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("companyId=");
			msg.append(companyId);

			msg.append(", identifier=");
			msg.append(identifier);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchProjectDomainException(msg.toString());
		}

		return projectDomain;
	}

	/**
	 * Returns the project domain where companyId = &#63; and identifier = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param companyId the company ID
	 * @param identifier the identifier
	 * @return the matching project domain, or <code>null</code> if a matching project domain could not be found
	 */
	@Override
	public ProjectDomain fetchByCompanyIdIdentifier(long companyId,
		String identifier) {
		return fetchByCompanyIdIdentifier(companyId, identifier, true);
	}

	/**
	 * Returns the project domain where companyId = &#63; and identifier = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param companyId the company ID
	 * @param identifier the identifier
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching project domain, or <code>null</code> if a matching project domain could not be found
	 */
	@Override
	public ProjectDomain fetchByCompanyIdIdentifier(long companyId,
		String identifier, boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { companyId, identifier };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER,
					finderArgs, this);
		}

		if (result instanceof ProjectDomain) {
			ProjectDomain projectDomain = (ProjectDomain)result;

			if ((companyId != projectDomain.getCompanyId()) ||
					!Objects.equals(identifier, projectDomain.getIdentifier())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_PROJECTDOMAIN_WHERE);

			query.append(_FINDER_COLUMN_COMPANYIDIDENTIFIER_COMPANYID_2);

			boolean bindIdentifier = false;

			if (identifier == null) {
				query.append(_FINDER_COLUMN_COMPANYIDIDENTIFIER_IDENTIFIER_1);
			}
			else if (identifier.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_COMPANYIDIDENTIFIER_IDENTIFIER_3);
			}
			else {
				bindIdentifier = true;

				query.append(_FINDER_COLUMN_COMPANYIDIDENTIFIER_IDENTIFIER_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (bindIdentifier) {
					qPos.add(identifier);
				}

				List<ProjectDomain> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER,
						finderArgs, list);
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							_log.warn(
								"ProjectDomainPersistenceImpl.fetchByCompanyIdIdentifier(long, String, boolean) with parameters (" +
								StringUtil.merge(finderArgs) +
								") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					ProjectDomain projectDomain = list.get(0);

					result = projectDomain;

					cacheResult(projectDomain);

					if ((projectDomain.getCompanyId() != companyId) ||
							(projectDomain.getIdentifier() == null) ||
							!projectDomain.getIdentifier().equals(identifier)) {
						finderCache.putResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER,
							finderArgs, projectDomain);
					}
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (ProjectDomain)result;
		}
	}

	/**
	 * Removes the project domain where companyId = &#63; and identifier = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param identifier the identifier
	 * @return the project domain that was removed
	 */
	@Override
	public ProjectDomain removeByCompanyIdIdentifier(long companyId,
		String identifier) throws NoSuchProjectDomainException {
		ProjectDomain projectDomain = findByCompanyIdIdentifier(companyId,
				identifier);

		return remove(projectDomain);
	}

	/**
	 * Returns the number of project domains where companyId = &#63; and identifier = &#63;.
	 *
	 * @param companyId the company ID
	 * @param identifier the identifier
	 * @return the number of matching project domains
	 */
	@Override
	public int countByCompanyIdIdentifier(long companyId, String identifier) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_COMPANYIDIDENTIFIER;

		Object[] finderArgs = new Object[] { companyId, identifier };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_PROJECTDOMAIN_WHERE);

			query.append(_FINDER_COLUMN_COMPANYIDIDENTIFIER_COMPANYID_2);

			boolean bindIdentifier = false;

			if (identifier == null) {
				query.append(_FINDER_COLUMN_COMPANYIDIDENTIFIER_IDENTIFIER_1);
			}
			else if (identifier.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_COMPANYIDIDENTIFIER_IDENTIFIER_3);
			}
			else {
				bindIdentifier = true;

				query.append(_FINDER_COLUMN_COMPANYIDIDENTIFIER_IDENTIFIER_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (bindIdentifier) {
					qPos.add(identifier);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COMPANYIDIDENTIFIER_COMPANYID_2 = "projectDomain.companyId = ? AND ";
	private static final String _FINDER_COLUMN_COMPANYIDIDENTIFIER_IDENTIFIER_1 = "projectDomain.identifier IS NULL";
	private static final String _FINDER_COLUMN_COMPANYIDIDENTIFIER_IDENTIFIER_2 = "projectDomain.identifier = ?";
	private static final String _FINDER_COLUMN_COMPANYIDIDENTIFIER_IDENTIFIER_3 = "(projectDomain.identifier IS NULL OR projectDomain.identifier = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYIDPROJECTSTRUCTUREID =
		new FinderPath(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDomainModelImpl.FINDER_CACHE_ENABLED,
			ProjectDomainImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByCompanyIdProjectStructureId",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYIDPROJECTSTRUCTUREID =
		new FinderPath(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDomainModelImpl.FINDER_CACHE_ENABLED,
			ProjectDomainImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByCompanyIdProjectStructureId",
			new String[] { Long.class.getName(), Long.class.getName() },
			ProjectDomainModelImpl.COMPANYID_COLUMN_BITMASK |
			ProjectDomainModelImpl.PROJECTSTRUCTUREID_COLUMN_BITMASK |
			ProjectDomainModelImpl.NAME_COLUMN_BITMASK |
			ProjectDomainModelImpl.PRIORITY_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANYIDPROJECTSTRUCTUREID =
		new FinderPath(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDomainModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByCompanyIdProjectStructureId",
			new String[] { Long.class.getName(), Long.class.getName() });

	/**
	 * Returns all the project domains where companyId = &#63; and projectStructureId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param projectStructureId the project structure ID
	 * @return the matching project domains
	 */
	@Override
	public List<ProjectDomain> findByCompanyIdProjectStructureId(
		long companyId, long projectStructureId) {
		return findByCompanyIdProjectStructureId(companyId, projectStructureId,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the project domains where companyId = &#63; and projectStructureId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param projectStructureId the project structure ID
	 * @param start the lower bound of the range of project domains
	 * @param end the upper bound of the range of project domains (not inclusive)
	 * @return the range of matching project domains
	 */
	@Override
	public List<ProjectDomain> findByCompanyIdProjectStructureId(
		long companyId, long projectStructureId, int start, int end) {
		return findByCompanyIdProjectStructureId(companyId, projectStructureId,
			start, end, null);
	}

	/**
	 * Returns an ordered range of all the project domains where companyId = &#63; and projectStructureId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param projectStructureId the project structure ID
	 * @param start the lower bound of the range of project domains
	 * @param end the upper bound of the range of project domains (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching project domains
	 */
	@Override
	public List<ProjectDomain> findByCompanyIdProjectStructureId(
		long companyId, long projectStructureId, int start, int end,
		OrderByComparator<ProjectDomain> orderByComparator) {
		return findByCompanyIdProjectStructureId(companyId, projectStructureId,
			start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the project domains where companyId = &#63; and projectStructureId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param projectStructureId the project structure ID
	 * @param start the lower bound of the range of project domains
	 * @param end the upper bound of the range of project domains (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching project domains
	 */
	@Override
	public List<ProjectDomain> findByCompanyIdProjectStructureId(
		long companyId, long projectStructureId, int start, int end,
		OrderByComparator<ProjectDomain> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYIDPROJECTSTRUCTUREID;
			finderArgs = new Object[] { companyId, projectStructureId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYIDPROJECTSTRUCTUREID;
			finderArgs = new Object[] {
					companyId, projectStructureId,
					
					start, end, orderByComparator
				};
		}

		List<ProjectDomain> list = null;

		if (retrieveFromCache) {
			list = (List<ProjectDomain>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ProjectDomain projectDomain : list) {
					if ((companyId != projectDomain.getCompanyId()) ||
							(projectStructureId != projectDomain.getProjectStructureId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_PROJECTDOMAIN_WHERE);

			query.append(_FINDER_COLUMN_COMPANYIDPROJECTSTRUCTUREID_COMPANYID_2);

			query.append(_FINDER_COLUMN_COMPANYIDPROJECTSTRUCTUREID_PROJECTSTRUCTUREID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProjectDomainModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(projectStructureId);

				if (!pagination) {
					list = (List<ProjectDomain>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ProjectDomain>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first project domain in the ordered set where companyId = &#63; and projectStructureId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param projectStructureId the project structure ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project domain
	 * @throws NoSuchProjectDomainException if a matching project domain could not be found
	 */
	@Override
	public ProjectDomain findByCompanyIdProjectStructureId_First(
		long companyId, long projectStructureId,
		OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException {
		ProjectDomain projectDomain = fetchByCompanyIdProjectStructureId_First(companyId,
				projectStructureId, orderByComparator);

		if (projectDomain != null) {
			return projectDomain;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", projectStructureId=");
		msg.append(projectStructureId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectDomainException(msg.toString());
	}

	/**
	 * Returns the first project domain in the ordered set where companyId = &#63; and projectStructureId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param projectStructureId the project structure ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project domain, or <code>null</code> if a matching project domain could not be found
	 */
	@Override
	public ProjectDomain fetchByCompanyIdProjectStructureId_First(
		long companyId, long projectStructureId,
		OrderByComparator<ProjectDomain> orderByComparator) {
		List<ProjectDomain> list = findByCompanyIdProjectStructureId(companyId,
				projectStructureId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last project domain in the ordered set where companyId = &#63; and projectStructureId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param projectStructureId the project structure ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project domain
	 * @throws NoSuchProjectDomainException if a matching project domain could not be found
	 */
	@Override
	public ProjectDomain findByCompanyIdProjectStructureId_Last(
		long companyId, long projectStructureId,
		OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException {
		ProjectDomain projectDomain = fetchByCompanyIdProjectStructureId_Last(companyId,
				projectStructureId, orderByComparator);

		if (projectDomain != null) {
			return projectDomain;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", projectStructureId=");
		msg.append(projectStructureId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectDomainException(msg.toString());
	}

	/**
	 * Returns the last project domain in the ordered set where companyId = &#63; and projectStructureId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param projectStructureId the project structure ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project domain, or <code>null</code> if a matching project domain could not be found
	 */
	@Override
	public ProjectDomain fetchByCompanyIdProjectStructureId_Last(
		long companyId, long projectStructureId,
		OrderByComparator<ProjectDomain> orderByComparator) {
		int count = countByCompanyIdProjectStructureId(companyId,
				projectStructureId);

		if (count == 0) {
			return null;
		}

		List<ProjectDomain> list = findByCompanyIdProjectStructureId(companyId,
				projectStructureId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the project domains before and after the current project domain in the ordered set where companyId = &#63; and projectStructureId = &#63;.
	 *
	 * @param projectDomainId the primary key of the current project domain
	 * @param companyId the company ID
	 * @param projectStructureId the project structure ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next project domain
	 * @throws NoSuchProjectDomainException if a project domain with the primary key could not be found
	 */
	@Override
	public ProjectDomain[] findByCompanyIdProjectStructureId_PrevAndNext(
		long projectDomainId, long companyId, long projectStructureId,
		OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException {
		ProjectDomain projectDomain = findByPrimaryKey(projectDomainId);

		Session session = null;

		try {
			session = openSession();

			ProjectDomain[] array = new ProjectDomainImpl[3];

			array[0] = getByCompanyIdProjectStructureId_PrevAndNext(session,
					projectDomain, companyId, projectStructureId,
					orderByComparator, true);

			array[1] = projectDomain;

			array[2] = getByCompanyIdProjectStructureId_PrevAndNext(session,
					projectDomain, companyId, projectStructureId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ProjectDomain getByCompanyIdProjectStructureId_PrevAndNext(
		Session session, ProjectDomain projectDomain, long companyId,
		long projectStructureId,
		OrderByComparator<ProjectDomain> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_PROJECTDOMAIN_WHERE);

		query.append(_FINDER_COLUMN_COMPANYIDPROJECTSTRUCTUREID_COMPANYID_2);

		query.append(_FINDER_COLUMN_COMPANYIDPROJECTSTRUCTUREID_PROJECTSTRUCTUREID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProjectDomainModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		qPos.add(projectStructureId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(projectDomain);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ProjectDomain> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the project domains where companyId = &#63; and projectStructureId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param projectStructureId the project structure ID
	 */
	@Override
	public void removeByCompanyIdProjectStructureId(long companyId,
		long projectStructureId) {
		for (ProjectDomain projectDomain : findByCompanyIdProjectStructureId(
				companyId, projectStructureId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(projectDomain);
		}
	}

	/**
	 * Returns the number of project domains where companyId = &#63; and projectStructureId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param projectStructureId the project structure ID
	 * @return the number of matching project domains
	 */
	@Override
	public int countByCompanyIdProjectStructureId(long companyId,
		long projectStructureId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_COMPANYIDPROJECTSTRUCTUREID;

		Object[] finderArgs = new Object[] { companyId, projectStructureId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_PROJECTDOMAIN_WHERE);

			query.append(_FINDER_COLUMN_COMPANYIDPROJECTSTRUCTUREID_COMPANYID_2);

			query.append(_FINDER_COLUMN_COMPANYIDPROJECTSTRUCTUREID_PROJECTSTRUCTUREID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(projectStructureId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COMPANYIDPROJECTSTRUCTUREID_COMPANYID_2 =
		"projectDomain.companyId = ? AND ";
	private static final String _FINDER_COLUMN_COMPANYIDPROJECTSTRUCTUREID_PROJECTSTRUCTUREID_2 =
		"projectDomain.projectStructureId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYIDINCLUDENAME =
		new FinderPath(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDomainModelImpl.FINDER_CACHE_ENABLED,
			ProjectDomainImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByCompanyIdIncludeName",
			new String[] {
				Long.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_COMPANYIDINCLUDENAME =
		new FinderPath(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDomainModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"countByCompanyIdIncludeName",
			new String[] { Long.class.getName(), String.class.getName() });

	/**
	 * Returns all the project domains where companyId = &#63; and name LIKE &#63;.
	 *
	 * @param companyId the company ID
	 * @param name the name
	 * @return the matching project domains
	 */
	@Override
	public List<ProjectDomain> findByCompanyIdIncludeName(long companyId,
		String name) {
		return findByCompanyIdIncludeName(companyId, name, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the project domains where companyId = &#63; and name LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param name the name
	 * @param start the lower bound of the range of project domains
	 * @param end the upper bound of the range of project domains (not inclusive)
	 * @return the range of matching project domains
	 */
	@Override
	public List<ProjectDomain> findByCompanyIdIncludeName(long companyId,
		String name, int start, int end) {
		return findByCompanyIdIncludeName(companyId, name, start, end, null);
	}

	/**
	 * Returns an ordered range of all the project domains where companyId = &#63; and name LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param name the name
	 * @param start the lower bound of the range of project domains
	 * @param end the upper bound of the range of project domains (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching project domains
	 */
	@Override
	public List<ProjectDomain> findByCompanyIdIncludeName(long companyId,
		String name, int start, int end,
		OrderByComparator<ProjectDomain> orderByComparator) {
		return findByCompanyIdIncludeName(companyId, name, start, end,
			orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the project domains where companyId = &#63; and name LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param name the name
	 * @param start the lower bound of the range of project domains
	 * @param end the upper bound of the range of project domains (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching project domains
	 */
	@Override
	public List<ProjectDomain> findByCompanyIdIncludeName(long companyId,
		String name, int start, int end,
		OrderByComparator<ProjectDomain> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYIDINCLUDENAME;
		finderArgs = new Object[] { companyId, name, start, end, orderByComparator };

		List<ProjectDomain> list = null;

		if (retrieveFromCache) {
			list = (List<ProjectDomain>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ProjectDomain projectDomain : list) {
					if ((companyId != projectDomain.getCompanyId()) ||
							!StringUtil.wildcardMatches(
								projectDomain.getName(), name,
								CharPool.UNDERLINE, CharPool.PERCENT,
								CharPool.BACK_SLASH, true)) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_PROJECTDOMAIN_WHERE);

			query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_COMPANYID_2);

			boolean bindName = false;

			if (name == null) {
				query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_1);
			}
			else if (name.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_3);
			}
			else {
				bindName = true;

				query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProjectDomainModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (bindName) {
					qPos.add(name);
				}

				if (!pagination) {
					list = (List<ProjectDomain>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ProjectDomain>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first project domain in the ordered set where companyId = &#63; and name LIKE &#63;.
	 *
	 * @param companyId the company ID
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project domain
	 * @throws NoSuchProjectDomainException if a matching project domain could not be found
	 */
	@Override
	public ProjectDomain findByCompanyIdIncludeName_First(long companyId,
		String name, OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException {
		ProjectDomain projectDomain = fetchByCompanyIdIncludeName_First(companyId,
				name, orderByComparator);

		if (projectDomain != null) {
			return projectDomain;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", name=");
		msg.append(name);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectDomainException(msg.toString());
	}

	/**
	 * Returns the first project domain in the ordered set where companyId = &#63; and name LIKE &#63;.
	 *
	 * @param companyId the company ID
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project domain, or <code>null</code> if a matching project domain could not be found
	 */
	@Override
	public ProjectDomain fetchByCompanyIdIncludeName_First(long companyId,
		String name, OrderByComparator<ProjectDomain> orderByComparator) {
		List<ProjectDomain> list = findByCompanyIdIncludeName(companyId, name,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last project domain in the ordered set where companyId = &#63; and name LIKE &#63;.
	 *
	 * @param companyId the company ID
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project domain
	 * @throws NoSuchProjectDomainException if a matching project domain could not be found
	 */
	@Override
	public ProjectDomain findByCompanyIdIncludeName_Last(long companyId,
		String name, OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException {
		ProjectDomain projectDomain = fetchByCompanyIdIncludeName_Last(companyId,
				name, orderByComparator);

		if (projectDomain != null) {
			return projectDomain;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", name=");
		msg.append(name);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectDomainException(msg.toString());
	}

	/**
	 * Returns the last project domain in the ordered set where companyId = &#63; and name LIKE &#63;.
	 *
	 * @param companyId the company ID
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project domain, or <code>null</code> if a matching project domain could not be found
	 */
	@Override
	public ProjectDomain fetchByCompanyIdIncludeName_Last(long companyId,
		String name, OrderByComparator<ProjectDomain> orderByComparator) {
		int count = countByCompanyIdIncludeName(companyId, name);

		if (count == 0) {
			return null;
		}

		List<ProjectDomain> list = findByCompanyIdIncludeName(companyId, name,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the project domains before and after the current project domain in the ordered set where companyId = &#63; and name LIKE &#63;.
	 *
	 * @param projectDomainId the primary key of the current project domain
	 * @param companyId the company ID
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next project domain
	 * @throws NoSuchProjectDomainException if a project domain with the primary key could not be found
	 */
	@Override
	public ProjectDomain[] findByCompanyIdIncludeName_PrevAndNext(
		long projectDomainId, long companyId, String name,
		OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException {
		ProjectDomain projectDomain = findByPrimaryKey(projectDomainId);

		Session session = null;

		try {
			session = openSession();

			ProjectDomain[] array = new ProjectDomainImpl[3];

			array[0] = getByCompanyIdIncludeName_PrevAndNext(session,
					projectDomain, companyId, name, orderByComparator, true);

			array[1] = projectDomain;

			array[2] = getByCompanyIdIncludeName_PrevAndNext(session,
					projectDomain, companyId, name, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ProjectDomain getByCompanyIdIncludeName_PrevAndNext(
		Session session, ProjectDomain projectDomain, long companyId,
		String name, OrderByComparator<ProjectDomain> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_PROJECTDOMAIN_WHERE);

		query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_COMPANYID_2);

		boolean bindName = false;

		if (name == null) {
			query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_1);
		}
		else if (name.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_3);
		}
		else {
			bindName = true;

			query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProjectDomainModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (bindName) {
			qPos.add(name);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(projectDomain);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ProjectDomain> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the project domains where companyId = &#63; and name LIKE &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param name the name
	 */
	@Override
	public void removeByCompanyIdIncludeName(long companyId, String name) {
		for (ProjectDomain projectDomain : findByCompanyIdIncludeName(
				companyId, name, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(projectDomain);
		}
	}

	/**
	 * Returns the number of project domains where companyId = &#63; and name LIKE &#63;.
	 *
	 * @param companyId the company ID
	 * @param name the name
	 * @return the number of matching project domains
	 */
	@Override
	public int countByCompanyIdIncludeName(long companyId, String name) {
		FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_COMPANYIDINCLUDENAME;

		Object[] finderArgs = new Object[] { companyId, name };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_PROJECTDOMAIN_WHERE);

			query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_COMPANYID_2);

			boolean bindName = false;

			if (name == null) {
				query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_1);
			}
			else if (name.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_3);
			}
			else {
				bindName = true;

				query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (bindName) {
					qPos.add(name);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COMPANYIDINCLUDENAME_COMPANYID_2 = "projectDomain.companyId = ? AND ";
	private static final String _FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_1 = "projectDomain.name IS NULL";
	private static final String _FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_2 = "projectDomain.name LIKE ?";
	private static final String _FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_3 = "(projectDomain.name IS NULL OR projectDomain.name LIKE '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PROJECTSTRUCTUREID =
		new FinderPath(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDomainModelImpl.FINDER_CACHE_ENABLED,
			ProjectDomainImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByProjectStructureId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTSTRUCTUREID =
		new FinderPath(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDomainModelImpl.FINDER_CACHE_ENABLED,
			ProjectDomainImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByProjectStructureId", new String[] { Long.class.getName() },
			ProjectDomainModelImpl.PROJECTSTRUCTUREID_COLUMN_BITMASK |
			ProjectDomainModelImpl.NAME_COLUMN_BITMASK |
			ProjectDomainModelImpl.PRIORITY_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PROJECTSTRUCTUREID = new FinderPath(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDomainModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByProjectStructureId", new String[] { Long.class.getName() });

	/**
	 * Returns all the project domains where projectStructureId = &#63;.
	 *
	 * @param projectStructureId the project structure ID
	 * @return the matching project domains
	 */
	@Override
	public List<ProjectDomain> findByProjectStructureId(long projectStructureId) {
		return findByProjectStructureId(projectStructureId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the project domains where projectStructureId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param projectStructureId the project structure ID
	 * @param start the lower bound of the range of project domains
	 * @param end the upper bound of the range of project domains (not inclusive)
	 * @return the range of matching project domains
	 */
	@Override
	public List<ProjectDomain> findByProjectStructureId(
		long projectStructureId, int start, int end) {
		return findByProjectStructureId(projectStructureId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the project domains where projectStructureId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param projectStructureId the project structure ID
	 * @param start the lower bound of the range of project domains
	 * @param end the upper bound of the range of project domains (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching project domains
	 */
	@Override
	public List<ProjectDomain> findByProjectStructureId(
		long projectStructureId, int start, int end,
		OrderByComparator<ProjectDomain> orderByComparator) {
		return findByProjectStructureId(projectStructureId, start, end,
			orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the project domains where projectStructureId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param projectStructureId the project structure ID
	 * @param start the lower bound of the range of project domains
	 * @param end the upper bound of the range of project domains (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching project domains
	 */
	@Override
	public List<ProjectDomain> findByProjectStructureId(
		long projectStructureId, int start, int end,
		OrderByComparator<ProjectDomain> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTSTRUCTUREID;
			finderArgs = new Object[] { projectStructureId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PROJECTSTRUCTUREID;
			finderArgs = new Object[] {
					projectStructureId,
					
					start, end, orderByComparator
				};
		}

		List<ProjectDomain> list = null;

		if (retrieveFromCache) {
			list = (List<ProjectDomain>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ProjectDomain projectDomain : list) {
					if ((projectStructureId != projectDomain.getProjectStructureId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PROJECTDOMAIN_WHERE);

			query.append(_FINDER_COLUMN_PROJECTSTRUCTUREID_PROJECTSTRUCTUREID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProjectDomainModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(projectStructureId);

				if (!pagination) {
					list = (List<ProjectDomain>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ProjectDomain>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first project domain in the ordered set where projectStructureId = &#63;.
	 *
	 * @param projectStructureId the project structure ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project domain
	 * @throws NoSuchProjectDomainException if a matching project domain could not be found
	 */
	@Override
	public ProjectDomain findByProjectStructureId_First(
		long projectStructureId,
		OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException {
		ProjectDomain projectDomain = fetchByProjectStructureId_First(projectStructureId,
				orderByComparator);

		if (projectDomain != null) {
			return projectDomain;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("projectStructureId=");
		msg.append(projectStructureId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectDomainException(msg.toString());
	}

	/**
	 * Returns the first project domain in the ordered set where projectStructureId = &#63;.
	 *
	 * @param projectStructureId the project structure ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project domain, or <code>null</code> if a matching project domain could not be found
	 */
	@Override
	public ProjectDomain fetchByProjectStructureId_First(
		long projectStructureId,
		OrderByComparator<ProjectDomain> orderByComparator) {
		List<ProjectDomain> list = findByProjectStructureId(projectStructureId,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last project domain in the ordered set where projectStructureId = &#63;.
	 *
	 * @param projectStructureId the project structure ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project domain
	 * @throws NoSuchProjectDomainException if a matching project domain could not be found
	 */
	@Override
	public ProjectDomain findByProjectStructureId_Last(
		long projectStructureId,
		OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException {
		ProjectDomain projectDomain = fetchByProjectStructureId_Last(projectStructureId,
				orderByComparator);

		if (projectDomain != null) {
			return projectDomain;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("projectStructureId=");
		msg.append(projectStructureId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectDomainException(msg.toString());
	}

	/**
	 * Returns the last project domain in the ordered set where projectStructureId = &#63;.
	 *
	 * @param projectStructureId the project structure ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project domain, or <code>null</code> if a matching project domain could not be found
	 */
	@Override
	public ProjectDomain fetchByProjectStructureId_Last(
		long projectStructureId,
		OrderByComparator<ProjectDomain> orderByComparator) {
		int count = countByProjectStructureId(projectStructureId);

		if (count == 0) {
			return null;
		}

		List<ProjectDomain> list = findByProjectStructureId(projectStructureId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the project domains before and after the current project domain in the ordered set where projectStructureId = &#63;.
	 *
	 * @param projectDomainId the primary key of the current project domain
	 * @param projectStructureId the project structure ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next project domain
	 * @throws NoSuchProjectDomainException if a project domain with the primary key could not be found
	 */
	@Override
	public ProjectDomain[] findByProjectStructureId_PrevAndNext(
		long projectDomainId, long projectStructureId,
		OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException {
		ProjectDomain projectDomain = findByPrimaryKey(projectDomainId);

		Session session = null;

		try {
			session = openSession();

			ProjectDomain[] array = new ProjectDomainImpl[3];

			array[0] = getByProjectStructureId_PrevAndNext(session,
					projectDomain, projectStructureId, orderByComparator, true);

			array[1] = projectDomain;

			array[2] = getByProjectStructureId_PrevAndNext(session,
					projectDomain, projectStructureId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ProjectDomain getByProjectStructureId_PrevAndNext(
		Session session, ProjectDomain projectDomain, long projectStructureId,
		OrderByComparator<ProjectDomain> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PROJECTDOMAIN_WHERE);

		query.append(_FINDER_COLUMN_PROJECTSTRUCTUREID_PROJECTSTRUCTUREID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProjectDomainModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(projectStructureId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(projectDomain);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ProjectDomain> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the project domains where projectStructureId = &#63; from the database.
	 *
	 * @param projectStructureId the project structure ID
	 */
	@Override
	public void removeByProjectStructureId(long projectStructureId) {
		for (ProjectDomain projectDomain : findByProjectStructureId(
				projectStructureId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(projectDomain);
		}
	}

	/**
	 * Returns the number of project domains where projectStructureId = &#63;.
	 *
	 * @param projectStructureId the project structure ID
	 * @return the number of matching project domains
	 */
	@Override
	public int countByProjectStructureId(long projectStructureId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PROJECTSTRUCTUREID;

		Object[] finderArgs = new Object[] { projectStructureId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PROJECTDOMAIN_WHERE);

			query.append(_FINDER_COLUMN_PROJECTSTRUCTUREID_PROJECTSTRUCTUREID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(projectStructureId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PROJECTSTRUCTUREID_PROJECTSTRUCTUREID_2 =
		"projectDomain.projectStructureId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_PROJECTSTRUCTUREIDIDENTIFIER =
		new FinderPath(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDomainModelImpl.FINDER_CACHE_ENABLED,
			ProjectDomainImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByProjectStructureIdIdentifier",
			new String[] { Long.class.getName(), String.class.getName() },
			ProjectDomainModelImpl.PROJECTSTRUCTUREID_COLUMN_BITMASK |
			ProjectDomainModelImpl.IDENTIFIER_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PROJECTSTRUCTUREIDIDENTIFIER =
		new FinderPath(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDomainModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByProjectStructureIdIdentifier",
			new String[] { Long.class.getName(), String.class.getName() });

	/**
	 * Returns the project domain where projectStructureId = &#63; and identifier = &#63; or throws a {@link NoSuchProjectDomainException} if it could not be found.
	 *
	 * @param projectStructureId the project structure ID
	 * @param identifier the identifier
	 * @return the matching project domain
	 * @throws NoSuchProjectDomainException if a matching project domain could not be found
	 */
	@Override
	public ProjectDomain findByProjectStructureIdIdentifier(
		long projectStructureId, String identifier)
		throws NoSuchProjectDomainException {
		ProjectDomain projectDomain = fetchByProjectStructureIdIdentifier(projectStructureId,
				identifier);

		if (projectDomain == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("projectStructureId=");
			msg.append(projectStructureId);

			msg.append(", identifier=");
			msg.append(identifier);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchProjectDomainException(msg.toString());
		}

		return projectDomain;
	}

	/**
	 * Returns the project domain where projectStructureId = &#63; and identifier = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param projectStructureId the project structure ID
	 * @param identifier the identifier
	 * @return the matching project domain, or <code>null</code> if a matching project domain could not be found
	 */
	@Override
	public ProjectDomain fetchByProjectStructureIdIdentifier(
		long projectStructureId, String identifier) {
		return fetchByProjectStructureIdIdentifier(projectStructureId,
			identifier, true);
	}

	/**
	 * Returns the project domain where projectStructureId = &#63; and identifier = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param projectStructureId the project structure ID
	 * @param identifier the identifier
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching project domain, or <code>null</code> if a matching project domain could not be found
	 */
	@Override
	public ProjectDomain fetchByProjectStructureIdIdentifier(
		long projectStructureId, String identifier, boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { projectStructureId, identifier };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_PROJECTSTRUCTUREIDIDENTIFIER,
					finderArgs, this);
		}

		if (result instanceof ProjectDomain) {
			ProjectDomain projectDomain = (ProjectDomain)result;

			if ((projectStructureId != projectDomain.getProjectStructureId()) ||
					!Objects.equals(identifier, projectDomain.getIdentifier())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_PROJECTDOMAIN_WHERE);

			query.append(_FINDER_COLUMN_PROJECTSTRUCTUREIDIDENTIFIER_PROJECTSTRUCTUREID_2);

			boolean bindIdentifier = false;

			if (identifier == null) {
				query.append(_FINDER_COLUMN_PROJECTSTRUCTUREIDIDENTIFIER_IDENTIFIER_1);
			}
			else if (identifier.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_PROJECTSTRUCTUREIDIDENTIFIER_IDENTIFIER_3);
			}
			else {
				bindIdentifier = true;

				query.append(_FINDER_COLUMN_PROJECTSTRUCTUREIDIDENTIFIER_IDENTIFIER_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(projectStructureId);

				if (bindIdentifier) {
					qPos.add(identifier);
				}

				List<ProjectDomain> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_PROJECTSTRUCTUREIDIDENTIFIER,
						finderArgs, list);
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							_log.warn(
								"ProjectDomainPersistenceImpl.fetchByProjectStructureIdIdentifier(long, String, boolean) with parameters (" +
								StringUtil.merge(finderArgs) +
								") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					ProjectDomain projectDomain = list.get(0);

					result = projectDomain;

					cacheResult(projectDomain);

					if ((projectDomain.getProjectStructureId() != projectStructureId) ||
							(projectDomain.getIdentifier() == null) ||
							!projectDomain.getIdentifier().equals(identifier)) {
						finderCache.putResult(FINDER_PATH_FETCH_BY_PROJECTSTRUCTUREIDIDENTIFIER,
							finderArgs, projectDomain);
					}
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_PROJECTSTRUCTUREIDIDENTIFIER,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (ProjectDomain)result;
		}
	}

	/**
	 * Removes the project domain where projectStructureId = &#63; and identifier = &#63; from the database.
	 *
	 * @param projectStructureId the project structure ID
	 * @param identifier the identifier
	 * @return the project domain that was removed
	 */
	@Override
	public ProjectDomain removeByProjectStructureIdIdentifier(
		long projectStructureId, String identifier)
		throws NoSuchProjectDomainException {
		ProjectDomain projectDomain = findByProjectStructureIdIdentifier(projectStructureId,
				identifier);

		return remove(projectDomain);
	}

	/**
	 * Returns the number of project domains where projectStructureId = &#63; and identifier = &#63;.
	 *
	 * @param projectStructureId the project structure ID
	 * @param identifier the identifier
	 * @return the number of matching project domains
	 */
	@Override
	public int countByProjectStructureIdIdentifier(long projectStructureId,
		String identifier) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PROJECTSTRUCTUREIDIDENTIFIER;

		Object[] finderArgs = new Object[] { projectStructureId, identifier };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_PROJECTDOMAIN_WHERE);

			query.append(_FINDER_COLUMN_PROJECTSTRUCTUREIDIDENTIFIER_PROJECTSTRUCTUREID_2);

			boolean bindIdentifier = false;

			if (identifier == null) {
				query.append(_FINDER_COLUMN_PROJECTSTRUCTUREIDIDENTIFIER_IDENTIFIER_1);
			}
			else if (identifier.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_PROJECTSTRUCTUREIDIDENTIFIER_IDENTIFIER_3);
			}
			else {
				bindIdentifier = true;

				query.append(_FINDER_COLUMN_PROJECTSTRUCTUREIDIDENTIFIER_IDENTIFIER_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(projectStructureId);

				if (bindIdentifier) {
					qPos.add(identifier);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PROJECTSTRUCTUREIDIDENTIFIER_PROJECTSTRUCTUREID_2 =
		"projectDomain.projectStructureId = ? AND ";
	private static final String _FINDER_COLUMN_PROJECTSTRUCTUREIDIDENTIFIER_IDENTIFIER_1 =
		"projectDomain.identifier IS NULL";
	private static final String _FINDER_COLUMN_PROJECTSTRUCTUREIDIDENTIFIER_IDENTIFIER_2 =
		"projectDomain.identifier = ?";
	private static final String _FINDER_COLUMN_PROJECTSTRUCTUREIDIDENTIFIER_IDENTIFIER_3 =
		"(projectDomain.identifier IS NULL OR projectDomain.identifier = '')";
	public static final FinderPath FINDER_PATH_FETCH_BY_PROJECTDOMAINID = new FinderPath(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDomainModelImpl.FINDER_CACHE_ENABLED,
			ProjectDomainImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByProjectDomainId", new String[] { Long.class.getName() },
			ProjectDomainModelImpl.PROJECTDOMAINID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PROJECTDOMAINID = new FinderPath(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDomainModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByProjectDomainId", new String[] { Long.class.getName() });

	/**
	 * Returns the project domain where projectDomainId = &#63; or throws a {@link NoSuchProjectDomainException} if it could not be found.
	 *
	 * @param projectDomainId the project domain ID
	 * @return the matching project domain
	 * @throws NoSuchProjectDomainException if a matching project domain could not be found
	 */
	@Override
	public ProjectDomain findByProjectDomainId(long projectDomainId)
		throws NoSuchProjectDomainException {
		ProjectDomain projectDomain = fetchByProjectDomainId(projectDomainId);

		if (projectDomain == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("projectDomainId=");
			msg.append(projectDomainId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchProjectDomainException(msg.toString());
		}

		return projectDomain;
	}

	/**
	 * Returns the project domain where projectDomainId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param projectDomainId the project domain ID
	 * @return the matching project domain, or <code>null</code> if a matching project domain could not be found
	 */
	@Override
	public ProjectDomain fetchByProjectDomainId(long projectDomainId) {
		return fetchByProjectDomainId(projectDomainId, true);
	}

	/**
	 * Returns the project domain where projectDomainId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param projectDomainId the project domain ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching project domain, or <code>null</code> if a matching project domain could not be found
	 */
	@Override
	public ProjectDomain fetchByProjectDomainId(long projectDomainId,
		boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { projectDomainId };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_PROJECTDOMAINID,
					finderArgs, this);
		}

		if (result instanceof ProjectDomain) {
			ProjectDomain projectDomain = (ProjectDomain)result;

			if ((projectDomainId != projectDomain.getProjectDomainId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_PROJECTDOMAIN_WHERE);

			query.append(_FINDER_COLUMN_PROJECTDOMAINID_PROJECTDOMAINID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(projectDomainId);

				List<ProjectDomain> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_PROJECTDOMAINID,
						finderArgs, list);
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							_log.warn(
								"ProjectDomainPersistenceImpl.fetchByProjectDomainId(long, boolean) with parameters (" +
								StringUtil.merge(finderArgs) +
								") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					ProjectDomain projectDomain = list.get(0);

					result = projectDomain;

					cacheResult(projectDomain);

					if ((projectDomain.getProjectDomainId() != projectDomainId)) {
						finderCache.putResult(FINDER_PATH_FETCH_BY_PROJECTDOMAINID,
							finderArgs, projectDomain);
					}
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_PROJECTDOMAINID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (ProjectDomain)result;
		}
	}

	/**
	 * Removes the project domain where projectDomainId = &#63; from the database.
	 *
	 * @param projectDomainId the project domain ID
	 * @return the project domain that was removed
	 */
	@Override
	public ProjectDomain removeByProjectDomainId(long projectDomainId)
		throws NoSuchProjectDomainException {
		ProjectDomain projectDomain = findByProjectDomainId(projectDomainId);

		return remove(projectDomain);
	}

	/**
	 * Returns the number of project domains where projectDomainId = &#63;.
	 *
	 * @param projectDomainId the project domain ID
	 * @return the number of matching project domains
	 */
	@Override
	public int countByProjectDomainId(long projectDomainId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PROJECTDOMAINID;

		Object[] finderArgs = new Object[] { projectDomainId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PROJECTDOMAIN_WHERE);

			query.append(_FINDER_COLUMN_PROJECTDOMAINID_PROJECTDOMAINID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(projectDomainId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PROJECTDOMAINID_PROJECTDOMAINID_2 =
		"projectDomain.projectDomainId = ?";

	public ProjectDomainPersistenceImpl() {
		setModelClass(ProjectDomain.class);
	}

	/**
	 * Caches the project domain in the entity cache if it is enabled.
	 *
	 * @param projectDomain the project domain
	 */
	@Override
	public void cacheResult(ProjectDomain projectDomain) {
		entityCache.putResult(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDomainImpl.class, projectDomain.getPrimaryKey(),
			projectDomain);

		finderCache.putResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER,
			new Object[] {
				projectDomain.getCompanyId(), projectDomain.getIdentifier()
			}, projectDomain);

		finderCache.putResult(FINDER_PATH_FETCH_BY_PROJECTSTRUCTUREIDIDENTIFIER,
			new Object[] {
				projectDomain.getProjectStructureId(),
				projectDomain.getIdentifier()
			}, projectDomain);

		finderCache.putResult(FINDER_PATH_FETCH_BY_PROJECTDOMAINID,
			new Object[] { projectDomain.getProjectDomainId() }, projectDomain);

		projectDomain.resetOriginalValues();
	}

	/**
	 * Caches the project domains in the entity cache if it is enabled.
	 *
	 * @param projectDomains the project domains
	 */
	@Override
	public void cacheResult(List<ProjectDomain> projectDomains) {
		for (ProjectDomain projectDomain : projectDomains) {
			if (entityCache.getResult(
						ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
						ProjectDomainImpl.class, projectDomain.getPrimaryKey()) == null) {
				cacheResult(projectDomain);
			}
			else {
				projectDomain.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all project domains.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(ProjectDomainImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the project domain.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ProjectDomain projectDomain) {
		entityCache.removeResult(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDomainImpl.class, projectDomain.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache((ProjectDomainModelImpl)projectDomain);
	}

	@Override
	public void clearCache(List<ProjectDomain> projectDomains) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ProjectDomain projectDomain : projectDomains) {
			entityCache.removeResult(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
				ProjectDomainImpl.class, projectDomain.getPrimaryKey());

			clearUniqueFindersCache((ProjectDomainModelImpl)projectDomain);
		}
	}

	protected void cacheUniqueFindersCache(
		ProjectDomainModelImpl projectDomainModelImpl, boolean isNew) {
		if (isNew) {
			Object[] args = new Object[] {
					projectDomainModelImpl.getCompanyId(),
					projectDomainModelImpl.getIdentifier()
				};

			finderCache.putResult(FINDER_PATH_COUNT_BY_COMPANYIDIDENTIFIER,
				args, Long.valueOf(1));
			finderCache.putResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER,
				args, projectDomainModelImpl);

			args = new Object[] {
					projectDomainModelImpl.getProjectStructureId(),
					projectDomainModelImpl.getIdentifier()
				};

			finderCache.putResult(FINDER_PATH_COUNT_BY_PROJECTSTRUCTUREIDIDENTIFIER,
				args, Long.valueOf(1));
			finderCache.putResult(FINDER_PATH_FETCH_BY_PROJECTSTRUCTUREIDIDENTIFIER,
				args, projectDomainModelImpl);

			args = new Object[] { projectDomainModelImpl.getProjectDomainId() };

			finderCache.putResult(FINDER_PATH_COUNT_BY_PROJECTDOMAINID, args,
				Long.valueOf(1));
			finderCache.putResult(FINDER_PATH_FETCH_BY_PROJECTDOMAINID, args,
				projectDomainModelImpl);
		}
		else {
			if ((projectDomainModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						projectDomainModelImpl.getCompanyId(),
						projectDomainModelImpl.getIdentifier()
					};

				finderCache.putResult(FINDER_PATH_COUNT_BY_COMPANYIDIDENTIFIER,
					args, Long.valueOf(1));
				finderCache.putResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER,
					args, projectDomainModelImpl);
			}

			if ((projectDomainModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_PROJECTSTRUCTUREIDIDENTIFIER.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						projectDomainModelImpl.getProjectStructureId(),
						projectDomainModelImpl.getIdentifier()
					};

				finderCache.putResult(FINDER_PATH_COUNT_BY_PROJECTSTRUCTUREIDIDENTIFIER,
					args, Long.valueOf(1));
				finderCache.putResult(FINDER_PATH_FETCH_BY_PROJECTSTRUCTUREIDIDENTIFIER,
					args, projectDomainModelImpl);
			}

			if ((projectDomainModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_PROJECTDOMAINID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						projectDomainModelImpl.getProjectDomainId()
					};

				finderCache.putResult(FINDER_PATH_COUNT_BY_PROJECTDOMAINID,
					args, Long.valueOf(1));
				finderCache.putResult(FINDER_PATH_FETCH_BY_PROJECTDOMAINID,
					args, projectDomainModelImpl);
			}
		}
	}

	protected void clearUniqueFindersCache(
		ProjectDomainModelImpl projectDomainModelImpl) {
		Object[] args = new Object[] {
				projectDomainModelImpl.getCompanyId(),
				projectDomainModelImpl.getIdentifier()
			};

		finderCache.removeResult(FINDER_PATH_COUNT_BY_COMPANYIDIDENTIFIER, args);
		finderCache.removeResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER, args);

		if ((projectDomainModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER.getColumnBitmask()) != 0) {
			args = new Object[] {
					projectDomainModelImpl.getOriginalCompanyId(),
					projectDomainModelImpl.getOriginalIdentifier()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_COMPANYIDIDENTIFIER,
				args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER,
				args);
		}

		args = new Object[] {
				projectDomainModelImpl.getProjectStructureId(),
				projectDomainModelImpl.getIdentifier()
			};

		finderCache.removeResult(FINDER_PATH_COUNT_BY_PROJECTSTRUCTUREIDIDENTIFIER,
			args);
		finderCache.removeResult(FINDER_PATH_FETCH_BY_PROJECTSTRUCTUREIDIDENTIFIER,
			args);

		if ((projectDomainModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_PROJECTSTRUCTUREIDIDENTIFIER.getColumnBitmask()) != 0) {
			args = new Object[] {
					projectDomainModelImpl.getOriginalProjectStructureId(),
					projectDomainModelImpl.getOriginalIdentifier()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_PROJECTSTRUCTUREIDIDENTIFIER,
				args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_PROJECTSTRUCTUREIDIDENTIFIER,
				args);
		}

		args = new Object[] { projectDomainModelImpl.getProjectDomainId() };

		finderCache.removeResult(FINDER_PATH_COUNT_BY_PROJECTDOMAINID, args);
		finderCache.removeResult(FINDER_PATH_FETCH_BY_PROJECTDOMAINID, args);

		if ((projectDomainModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_PROJECTDOMAINID.getColumnBitmask()) != 0) {
			args = new Object[] {
					projectDomainModelImpl.getOriginalProjectDomainId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_PROJECTDOMAINID, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_PROJECTDOMAINID, args);
		}
	}

	/**
	 * Creates a new project domain with the primary key. Does not add the project domain to the database.
	 *
	 * @param projectDomainId the primary key for the new project domain
	 * @return the new project domain
	 */
	@Override
	public ProjectDomain create(long projectDomainId) {
		ProjectDomain projectDomain = new ProjectDomainImpl();

		projectDomain.setNew(true);
		projectDomain.setPrimaryKey(projectDomainId);

		String uuid = PortalUUIDUtil.generate();

		projectDomain.setUuid(uuid);

		projectDomain.setCompanyId(companyProvider.getCompanyId());

		return projectDomain;
	}

	/**
	 * Removes the project domain with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param projectDomainId the primary key of the project domain
	 * @return the project domain that was removed
	 * @throws NoSuchProjectDomainException if a project domain with the primary key could not be found
	 */
	@Override
	public ProjectDomain remove(long projectDomainId)
		throws NoSuchProjectDomainException {
		return remove((Serializable)projectDomainId);
	}

	/**
	 * Removes the project domain with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the project domain
	 * @return the project domain that was removed
	 * @throws NoSuchProjectDomainException if a project domain with the primary key could not be found
	 */
	@Override
	public ProjectDomain remove(Serializable primaryKey)
		throws NoSuchProjectDomainException {
		Session session = null;

		try {
			session = openSession();

			ProjectDomain projectDomain = (ProjectDomain)session.get(ProjectDomainImpl.class,
					primaryKey);

			if (projectDomain == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchProjectDomainException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(projectDomain);
		}
		catch (NoSuchProjectDomainException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ProjectDomain removeImpl(ProjectDomain projectDomain) {
		projectDomain = toUnwrappedModel(projectDomain);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(projectDomain)) {
				projectDomain = (ProjectDomain)session.get(ProjectDomainImpl.class,
						projectDomain.getPrimaryKeyObj());
			}

			if (projectDomain != null) {
				session.delete(projectDomain);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (projectDomain != null) {
			clearCache(projectDomain);
		}

		return projectDomain;
	}

	@Override
	public ProjectDomain updateImpl(ProjectDomain projectDomain) {
		projectDomain = toUnwrappedModel(projectDomain);

		boolean isNew = projectDomain.isNew();

		ProjectDomainModelImpl projectDomainModelImpl = (ProjectDomainModelImpl)projectDomain;

		if (Validator.isNull(projectDomain.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			projectDomain.setUuid(uuid);
		}

		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();

		Date now = new Date();

		if (isNew && (projectDomain.getCreateDate() == null)) {
			if (serviceContext == null) {
				projectDomain.setCreateDate(now);
			}
			else {
				projectDomain.setCreateDate(serviceContext.getCreateDate(now));
			}
		}

		if (!projectDomainModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				projectDomain.setModifiedDate(now);
			}
			else {
				projectDomain.setModifiedDate(serviceContext.getModifiedDate(
						now));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (projectDomain.isNew()) {
				session.save(projectDomain);

				projectDomain.setNew(false);
			}
			else {
				projectDomain = (ProjectDomain)session.merge(projectDomain);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ProjectDomainModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((projectDomainModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						projectDomainModelImpl.getOriginalUuid()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);

				args = new Object[] { projectDomainModelImpl.getUuid() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);
			}

			if ((projectDomainModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						projectDomainModelImpl.getOriginalUuid(),
						projectDomainModelImpl.getOriginalCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);

				args = new Object[] {
						projectDomainModelImpl.getUuid(),
						projectDomainModelImpl.getCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);
			}

			if ((projectDomainModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						projectDomainModelImpl.getOriginalCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_COMPANYID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID,
					args);

				args = new Object[] { projectDomainModelImpl.getCompanyId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_COMPANYID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID,
					args);
			}

			if ((projectDomainModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYIDPROJECTSTRUCTUREID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						projectDomainModelImpl.getOriginalCompanyId(),
						projectDomainModelImpl.getOriginalProjectStructureId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_COMPANYIDPROJECTSTRUCTUREID,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYIDPROJECTSTRUCTUREID,
					args);

				args = new Object[] {
						projectDomainModelImpl.getCompanyId(),
						projectDomainModelImpl.getProjectStructureId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_COMPANYIDPROJECTSTRUCTUREID,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYIDPROJECTSTRUCTUREID,
					args);
			}

			if ((projectDomainModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTSTRUCTUREID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						projectDomainModelImpl.getOriginalProjectStructureId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_PROJECTSTRUCTUREID,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTSTRUCTUREID,
					args);

				args = new Object[] {
						projectDomainModelImpl.getProjectStructureId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_PROJECTSTRUCTUREID,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTSTRUCTUREID,
					args);
			}
		}

		entityCache.putResult(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDomainImpl.class, projectDomain.getPrimaryKey(),
			projectDomain, false);

		clearUniqueFindersCache(projectDomainModelImpl);
		cacheUniqueFindersCache(projectDomainModelImpl, isNew);

		projectDomain.resetOriginalValues();

		return projectDomain;
	}

	protected ProjectDomain toUnwrappedModel(ProjectDomain projectDomain) {
		if (projectDomain instanceof ProjectDomainImpl) {
			return projectDomain;
		}

		ProjectDomainImpl projectDomainImpl = new ProjectDomainImpl();

		projectDomainImpl.setNew(projectDomain.isNew());
		projectDomainImpl.setPrimaryKey(projectDomain.getPrimaryKey());

		projectDomainImpl.setUuid(projectDomain.getUuid());
		projectDomainImpl.setProjectDomainId(projectDomain.getProjectDomainId());
		projectDomainImpl.setProjectStructureId(projectDomain.getProjectStructureId());
		projectDomainImpl.setCompanyId(projectDomain.getCompanyId());
		projectDomainImpl.setUserId(projectDomain.getUserId());
		projectDomainImpl.setUserName(projectDomain.getUserName());
		projectDomainImpl.setCreateDate(projectDomain.getCreateDate());
		projectDomainImpl.setModifiedDate(projectDomain.getModifiedDate());
		projectDomainImpl.setIdentifier(projectDomain.getIdentifier());
		projectDomainImpl.setName(projectDomain.getName());
		projectDomainImpl.setDescription(projectDomain.getDescription());
		projectDomainImpl.setPriority(projectDomain.getPriority());

		return projectDomainImpl;
	}

	/**
	 * Returns the project domain with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the project domain
	 * @return the project domain
	 * @throws NoSuchProjectDomainException if a project domain with the primary key could not be found
	 */
	@Override
	public ProjectDomain findByPrimaryKey(Serializable primaryKey)
		throws NoSuchProjectDomainException {
		ProjectDomain projectDomain = fetchByPrimaryKey(primaryKey);

		if (projectDomain == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchProjectDomainException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return projectDomain;
	}

	/**
	 * Returns the project domain with the primary key or throws a {@link NoSuchProjectDomainException} if it could not be found.
	 *
	 * @param projectDomainId the primary key of the project domain
	 * @return the project domain
	 * @throws NoSuchProjectDomainException if a project domain with the primary key could not be found
	 */
	@Override
	public ProjectDomain findByPrimaryKey(long projectDomainId)
		throws NoSuchProjectDomainException {
		return findByPrimaryKey((Serializable)projectDomainId);
	}

	/**
	 * Returns the project domain with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the project domain
	 * @return the project domain, or <code>null</code> if a project domain with the primary key could not be found
	 */
	@Override
	public ProjectDomain fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
				ProjectDomainImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		ProjectDomain projectDomain = (ProjectDomain)serializable;

		if (projectDomain == null) {
			Session session = null;

			try {
				session = openSession();

				projectDomain = (ProjectDomain)session.get(ProjectDomainImpl.class,
						primaryKey);

				if (projectDomain != null) {
					cacheResult(projectDomain);
				}
				else {
					entityCache.putResult(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
						ProjectDomainImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
					ProjectDomainImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return projectDomain;
	}

	/**
	 * Returns the project domain with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param projectDomainId the primary key of the project domain
	 * @return the project domain, or <code>null</code> if a project domain with the primary key could not be found
	 */
	@Override
	public ProjectDomain fetchByPrimaryKey(long projectDomainId) {
		return fetchByPrimaryKey((Serializable)projectDomainId);
	}

	@Override
	public Map<Serializable, ProjectDomain> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, ProjectDomain> map = new HashMap<Serializable, ProjectDomain>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			ProjectDomain projectDomain = fetchByPrimaryKey(primaryKey);

			if (projectDomain != null) {
				map.put(primaryKey, projectDomain);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
					ProjectDomainImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (ProjectDomain)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_PROJECTDOMAIN_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(String.valueOf(primaryKey));

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (ProjectDomain projectDomain : (List<ProjectDomain>)q.list()) {
				map.put(projectDomain.getPrimaryKeyObj(), projectDomain);

				cacheResult(projectDomain);

				uncachedPrimaryKeys.remove(projectDomain.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(ProjectDomainModelImpl.ENTITY_CACHE_ENABLED,
					ProjectDomainImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the project domains.
	 *
	 * @return the project domains
	 */
	@Override
	public List<ProjectDomain> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the project domains.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of project domains
	 * @param end the upper bound of the range of project domains (not inclusive)
	 * @return the range of project domains
	 */
	@Override
	public List<ProjectDomain> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the project domains.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of project domains
	 * @param end the upper bound of the range of project domains (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of project domains
	 */
	@Override
	public List<ProjectDomain> findAll(int start, int end,
		OrderByComparator<ProjectDomain> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the project domains.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of project domains
	 * @param end the upper bound of the range of project domains (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of project domains
	 */
	@Override
	public List<ProjectDomain> findAll(int start, int end,
		OrderByComparator<ProjectDomain> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ProjectDomain> list = null;

		if (retrieveFromCache) {
			list = (List<ProjectDomain>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_PROJECTDOMAIN);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PROJECTDOMAIN;

				if (pagination) {
					sql = sql.concat(ProjectDomainModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ProjectDomain>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ProjectDomain>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the project domains from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (ProjectDomain projectDomain : findAll()) {
			remove(projectDomain);
		}
	}

	/**
	 * Returns the number of project domains.
	 *
	 * @return the number of project domains
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PROJECTDOMAIN);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return ProjectDomainModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the project domain persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(ProjectDomainImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = CompanyProviderWrapper.class)
	protected CompanyProvider companyProvider;
	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_PROJECTDOMAIN = "SELECT projectDomain FROM ProjectDomain projectDomain";
	private static final String _SQL_SELECT_PROJECTDOMAIN_WHERE_PKS_IN = "SELECT projectDomain FROM ProjectDomain projectDomain WHERE projectDomainId IN (";
	private static final String _SQL_SELECT_PROJECTDOMAIN_WHERE = "SELECT projectDomain FROM ProjectDomain projectDomain WHERE ";
	private static final String _SQL_COUNT_PROJECTDOMAIN = "SELECT COUNT(projectDomain) FROM ProjectDomain projectDomain";
	private static final String _SQL_COUNT_PROJECTDOMAIN_WHERE = "SELECT COUNT(projectDomain) FROM ProjectDomain projectDomain WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "projectDomain.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ProjectDomain exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No ProjectDomain exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(ProjectDomainPersistenceImpl.class);
	private static final Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"uuid"
			});
}