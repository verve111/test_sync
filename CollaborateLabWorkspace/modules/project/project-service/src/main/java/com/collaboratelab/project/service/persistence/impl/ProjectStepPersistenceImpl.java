/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.project.exception.NoSuchProjectStepException;
import com.collaboratelab.project.model.ProjectStep;
import com.collaboratelab.project.model.impl.ProjectStepImpl;
import com.collaboratelab.project.model.impl.ProjectStepModelImpl;
import com.collaboratelab.project.service.persistence.ProjectStepPersistence;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.CompanyProvider;
import com.liferay.portal.kernel.service.persistence.CompanyProviderWrapper;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the project step service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Claudio Carlenzoli
 * @see ProjectStepPersistence
 * @see com.collaboratelab.project.service.persistence.ProjectStepUtil
 * @generated
 */
@ProviderType
public class ProjectStepPersistenceImpl extends BasePersistenceImpl<ProjectStep>
	implements ProjectStepPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ProjectStepUtil} to access the project step persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ProjectStepImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ProjectStepModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStepModelImpl.FINDER_CACHE_ENABLED, ProjectStepImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ProjectStepModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStepModelImpl.FINDER_CACHE_ENABLED, ProjectStepImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ProjectStepModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStepModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID = new FinderPath(ProjectStepModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStepModelImpl.FINDER_CACHE_ENABLED, ProjectStepImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID = new FinderPath(ProjectStepModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStepModelImpl.FINDER_CACHE_ENABLED, ProjectStepImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] { String.class.getName() },
			ProjectStepModelImpl.UUID_COLUMN_BITMASK |
			ProjectStepModelImpl.PROJECTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID = new FinderPath(ProjectStepModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStepModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] { String.class.getName() });

	/**
	 * Returns all the project steps where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching project steps
	 */
	@Override
	public List<ProjectStep> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the project steps where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of project steps
	 * @param end the upper bound of the range of project steps (not inclusive)
	 * @return the range of matching project steps
	 */
	@Override
	public List<ProjectStep> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the project steps where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of project steps
	 * @param end the upper bound of the range of project steps (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching project steps
	 */
	@Override
	public List<ProjectStep> findByUuid(String uuid, int start, int end,
		OrderByComparator<ProjectStep> orderByComparator) {
		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the project steps where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of project steps
	 * @param end the upper bound of the range of project steps (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching project steps
	 */
	@Override
	public List<ProjectStep> findByUuid(String uuid, int start, int end,
		OrderByComparator<ProjectStep> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid, start, end, orderByComparator };
		}

		List<ProjectStep> list = null;

		if (retrieveFromCache) {
			list = (List<ProjectStep>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ProjectStep projectStep : list) {
					if (!Objects.equals(uuid, projectStep.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PROJECTSTEP_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProjectStepModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<ProjectStep>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ProjectStep>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first project step in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project step
	 * @throws NoSuchProjectStepException if a matching project step could not be found
	 */
	@Override
	public ProjectStep findByUuid_First(String uuid,
		OrderByComparator<ProjectStep> orderByComparator)
		throws NoSuchProjectStepException {
		ProjectStep projectStep = fetchByUuid_First(uuid, orderByComparator);

		if (projectStep != null) {
			return projectStep;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectStepException(msg.toString());
	}

	/**
	 * Returns the first project step in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project step, or <code>null</code> if a matching project step could not be found
	 */
	@Override
	public ProjectStep fetchByUuid_First(String uuid,
		OrderByComparator<ProjectStep> orderByComparator) {
		List<ProjectStep> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last project step in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project step
	 * @throws NoSuchProjectStepException if a matching project step could not be found
	 */
	@Override
	public ProjectStep findByUuid_Last(String uuid,
		OrderByComparator<ProjectStep> orderByComparator)
		throws NoSuchProjectStepException {
		ProjectStep projectStep = fetchByUuid_Last(uuid, orderByComparator);

		if (projectStep != null) {
			return projectStep;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectStepException(msg.toString());
	}

	/**
	 * Returns the last project step in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project step, or <code>null</code> if a matching project step could not be found
	 */
	@Override
	public ProjectStep fetchByUuid_Last(String uuid,
		OrderByComparator<ProjectStep> orderByComparator) {
		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<ProjectStep> list = findByUuid(uuid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the project steps before and after the current project step in the ordered set where uuid = &#63;.
	 *
	 * @param projectStepId the primary key of the current project step
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next project step
	 * @throws NoSuchProjectStepException if a project step with the primary key could not be found
	 */
	@Override
	public ProjectStep[] findByUuid_PrevAndNext(long projectStepId,
		String uuid, OrderByComparator<ProjectStep> orderByComparator)
		throws NoSuchProjectStepException {
		ProjectStep projectStep = findByPrimaryKey(projectStepId);

		Session session = null;

		try {
			session = openSession();

			ProjectStep[] array = new ProjectStepImpl[3];

			array[0] = getByUuid_PrevAndNext(session, projectStep, uuid,
					orderByComparator, true);

			array[1] = projectStep;

			array[2] = getByUuid_PrevAndNext(session, projectStep, uuid,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ProjectStep getByUuid_PrevAndNext(Session session,
		ProjectStep projectStep, String uuid,
		OrderByComparator<ProjectStep> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PROJECTSTEP_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProjectStepModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(projectStep);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ProjectStep> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the project steps where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (ProjectStep projectStep : findByUuid(uuid, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(projectStep);
		}
	}

	/**
	 * Returns the number of project steps where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching project steps
	 */
	@Override
	public int countByUuid(String uuid) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID;

		Object[] finderArgs = new Object[] { uuid };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PROJECTSTEP_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_1 = "projectStep.uuid IS NULL";
	private static final String _FINDER_COLUMN_UUID_UUID_2 = "projectStep.uuid = ?";
	private static final String _FINDER_COLUMN_UUID_UUID_3 = "(projectStep.uuid IS NULL OR projectStep.uuid = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C = new FinderPath(ProjectStepModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStepModelImpl.FINDER_CACHE_ENABLED, ProjectStepImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C =
		new FinderPath(ProjectStepModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStepModelImpl.FINDER_CACHE_ENABLED, ProjectStepImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() },
			ProjectStepModelImpl.UUID_COLUMN_BITMASK |
			ProjectStepModelImpl.COMPANYID_COLUMN_BITMASK |
			ProjectStepModelImpl.PROJECTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_C = new FinderPath(ProjectStepModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStepModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns all the project steps where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching project steps
	 */
	@Override
	public List<ProjectStep> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(uuid, companyId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the project steps where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of project steps
	 * @param end the upper bound of the range of project steps (not inclusive)
	 * @return the range of matching project steps
	 */
	@Override
	public List<ProjectStep> findByUuid_C(String uuid, long companyId,
		int start, int end) {
		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the project steps where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of project steps
	 * @param end the upper bound of the range of project steps (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching project steps
	 */
	@Override
	public List<ProjectStep> findByUuid_C(String uuid, long companyId,
		int start, int end, OrderByComparator<ProjectStep> orderByComparator) {
		return findByUuid_C(uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the project steps where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of project steps
	 * @param end the upper bound of the range of project steps (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching project steps
	 */
	@Override
	public List<ProjectStep> findByUuid_C(String uuid, long companyId,
		int start, int end, OrderByComparator<ProjectStep> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] { uuid, companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] {
					uuid, companyId,
					
					start, end, orderByComparator
				};
		}

		List<ProjectStep> list = null;

		if (retrieveFromCache) {
			list = (List<ProjectStep>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ProjectStep projectStep : list) {
					if (!Objects.equals(uuid, projectStep.getUuid()) ||
							(companyId != projectStep.getCompanyId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_PROJECTSTEP_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProjectStepModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				if (!pagination) {
					list = (List<ProjectStep>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ProjectStep>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first project step in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project step
	 * @throws NoSuchProjectStepException if a matching project step could not be found
	 */
	@Override
	public ProjectStep findByUuid_C_First(String uuid, long companyId,
		OrderByComparator<ProjectStep> orderByComparator)
		throws NoSuchProjectStepException {
		ProjectStep projectStep = fetchByUuid_C_First(uuid, companyId,
				orderByComparator);

		if (projectStep != null) {
			return projectStep;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectStepException(msg.toString());
	}

	/**
	 * Returns the first project step in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project step, or <code>null</code> if a matching project step could not be found
	 */
	@Override
	public ProjectStep fetchByUuid_C_First(String uuid, long companyId,
		OrderByComparator<ProjectStep> orderByComparator) {
		List<ProjectStep> list = findByUuid_C(uuid, companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last project step in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project step
	 * @throws NoSuchProjectStepException if a matching project step could not be found
	 */
	@Override
	public ProjectStep findByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<ProjectStep> orderByComparator)
		throws NoSuchProjectStepException {
		ProjectStep projectStep = fetchByUuid_C_Last(uuid, companyId,
				orderByComparator);

		if (projectStep != null) {
			return projectStep;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectStepException(msg.toString());
	}

	/**
	 * Returns the last project step in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project step, or <code>null</code> if a matching project step could not be found
	 */
	@Override
	public ProjectStep fetchByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<ProjectStep> orderByComparator) {
		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<ProjectStep> list = findByUuid_C(uuid, companyId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the project steps before and after the current project step in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param projectStepId the primary key of the current project step
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next project step
	 * @throws NoSuchProjectStepException if a project step with the primary key could not be found
	 */
	@Override
	public ProjectStep[] findByUuid_C_PrevAndNext(long projectStepId,
		String uuid, long companyId,
		OrderByComparator<ProjectStep> orderByComparator)
		throws NoSuchProjectStepException {
		ProjectStep projectStep = findByPrimaryKey(projectStepId);

		Session session = null;

		try {
			session = openSession();

			ProjectStep[] array = new ProjectStepImpl[3];

			array[0] = getByUuid_C_PrevAndNext(session, projectStep, uuid,
					companyId, orderByComparator, true);

			array[1] = projectStep;

			array[2] = getByUuid_C_PrevAndNext(session, projectStep, uuid,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ProjectStep getByUuid_C_PrevAndNext(Session session,
		ProjectStep projectStep, String uuid, long companyId,
		OrderByComparator<ProjectStep> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_PROJECTSTEP_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProjectStepModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(projectStep);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ProjectStep> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the project steps where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (ProjectStep projectStep : findByUuid_C(uuid, companyId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(projectStep);
		}
	}

	/**
	 * Returns the number of project steps where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching project steps
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_C;

		Object[] finderArgs = new Object[] { uuid, companyId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_PROJECTSTEP_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_1 = "projectStep.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_2 = "projectStep.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_3 = "(projectStep.uuid IS NULL OR projectStep.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 = "projectStep.companyId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PROJECTID =
		new FinderPath(ProjectStepModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStepModelImpl.FINDER_CACHE_ENABLED, ProjectStepImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByProjectId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTID =
		new FinderPath(ProjectStepModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStepModelImpl.FINDER_CACHE_ENABLED, ProjectStepImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByProjectId",
			new String[] { Long.class.getName() },
			ProjectStepModelImpl.PROJECTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PROJECTID = new FinderPath(ProjectStepModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStepModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByProjectId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the project steps where projectId = &#63;.
	 *
	 * @param projectId the project ID
	 * @return the matching project steps
	 */
	@Override
	public List<ProjectStep> findByProjectId(long projectId) {
		return findByProjectId(projectId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the project steps where projectId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param projectId the project ID
	 * @param start the lower bound of the range of project steps
	 * @param end the upper bound of the range of project steps (not inclusive)
	 * @return the range of matching project steps
	 */
	@Override
	public List<ProjectStep> findByProjectId(long projectId, int start, int end) {
		return findByProjectId(projectId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the project steps where projectId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param projectId the project ID
	 * @param start the lower bound of the range of project steps
	 * @param end the upper bound of the range of project steps (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching project steps
	 */
	@Override
	public List<ProjectStep> findByProjectId(long projectId, int start,
		int end, OrderByComparator<ProjectStep> orderByComparator) {
		return findByProjectId(projectId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the project steps where projectId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param projectId the project ID
	 * @param start the lower bound of the range of project steps
	 * @param end the upper bound of the range of project steps (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching project steps
	 */
	@Override
	public List<ProjectStep> findByProjectId(long projectId, int start,
		int end, OrderByComparator<ProjectStep> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTID;
			finderArgs = new Object[] { projectId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PROJECTID;
			finderArgs = new Object[] { projectId, start, end, orderByComparator };
		}

		List<ProjectStep> list = null;

		if (retrieveFromCache) {
			list = (List<ProjectStep>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ProjectStep projectStep : list) {
					if ((projectId != projectStep.getProjectId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PROJECTSTEP_WHERE);

			query.append(_FINDER_COLUMN_PROJECTID_PROJECTID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProjectStepModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(projectId);

				if (!pagination) {
					list = (List<ProjectStep>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ProjectStep>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first project step in the ordered set where projectId = &#63;.
	 *
	 * @param projectId the project ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project step
	 * @throws NoSuchProjectStepException if a matching project step could not be found
	 */
	@Override
	public ProjectStep findByProjectId_First(long projectId,
		OrderByComparator<ProjectStep> orderByComparator)
		throws NoSuchProjectStepException {
		ProjectStep projectStep = fetchByProjectId_First(projectId,
				orderByComparator);

		if (projectStep != null) {
			return projectStep;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("projectId=");
		msg.append(projectId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectStepException(msg.toString());
	}

	/**
	 * Returns the first project step in the ordered set where projectId = &#63;.
	 *
	 * @param projectId the project ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project step, or <code>null</code> if a matching project step could not be found
	 */
	@Override
	public ProjectStep fetchByProjectId_First(long projectId,
		OrderByComparator<ProjectStep> orderByComparator) {
		List<ProjectStep> list = findByProjectId(projectId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last project step in the ordered set where projectId = &#63;.
	 *
	 * @param projectId the project ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project step
	 * @throws NoSuchProjectStepException if a matching project step could not be found
	 */
	@Override
	public ProjectStep findByProjectId_Last(long projectId,
		OrderByComparator<ProjectStep> orderByComparator)
		throws NoSuchProjectStepException {
		ProjectStep projectStep = fetchByProjectId_Last(projectId,
				orderByComparator);

		if (projectStep != null) {
			return projectStep;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("projectId=");
		msg.append(projectId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectStepException(msg.toString());
	}

	/**
	 * Returns the last project step in the ordered set where projectId = &#63;.
	 *
	 * @param projectId the project ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project step, or <code>null</code> if a matching project step could not be found
	 */
	@Override
	public ProjectStep fetchByProjectId_Last(long projectId,
		OrderByComparator<ProjectStep> orderByComparator) {
		int count = countByProjectId(projectId);

		if (count == 0) {
			return null;
		}

		List<ProjectStep> list = findByProjectId(projectId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the project steps before and after the current project step in the ordered set where projectId = &#63;.
	 *
	 * @param projectStepId the primary key of the current project step
	 * @param projectId the project ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next project step
	 * @throws NoSuchProjectStepException if a project step with the primary key could not be found
	 */
	@Override
	public ProjectStep[] findByProjectId_PrevAndNext(long projectStepId,
		long projectId, OrderByComparator<ProjectStep> orderByComparator)
		throws NoSuchProjectStepException {
		ProjectStep projectStep = findByPrimaryKey(projectStepId);

		Session session = null;

		try {
			session = openSession();

			ProjectStep[] array = new ProjectStepImpl[3];

			array[0] = getByProjectId_PrevAndNext(session, projectStep,
					projectId, orderByComparator, true);

			array[1] = projectStep;

			array[2] = getByProjectId_PrevAndNext(session, projectStep,
					projectId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ProjectStep getByProjectId_PrevAndNext(Session session,
		ProjectStep projectStep, long projectId,
		OrderByComparator<ProjectStep> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PROJECTSTEP_WHERE);

		query.append(_FINDER_COLUMN_PROJECTID_PROJECTID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProjectStepModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(projectId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(projectStep);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ProjectStep> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the project steps where projectId = &#63; from the database.
	 *
	 * @param projectId the project ID
	 */
	@Override
	public void removeByProjectId(long projectId) {
		for (ProjectStep projectStep : findByProjectId(projectId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(projectStep);
		}
	}

	/**
	 * Returns the number of project steps where projectId = &#63;.
	 *
	 * @param projectId the project ID
	 * @return the number of matching project steps
	 */
	@Override
	public int countByProjectId(long projectId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PROJECTID;

		Object[] finderArgs = new Object[] { projectId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PROJECTSTEP_WHERE);

			query.append(_FINDER_COLUMN_PROJECTID_PROJECTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(projectId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PROJECTID_PROJECTID_2 = "projectStep.projectId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PROJECTSTRUCTUREIDPROJECTDOMAINID =
		new FinderPath(ProjectStepModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStepModelImpl.FINDER_CACHE_ENABLED, ProjectStepImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByProjectStructureIdProjectDomainId",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTSTRUCTUREIDPROJECTDOMAINID =
		new FinderPath(ProjectStepModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStepModelImpl.FINDER_CACHE_ENABLED, ProjectStepImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByProjectStructureIdProjectDomainId",
			new String[] { Long.class.getName(), Long.class.getName() },
			ProjectStepModelImpl.PROJECTID_COLUMN_BITMASK |
			ProjectStepModelImpl.PROJECTDOMAINID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PROJECTSTRUCTUREIDPROJECTDOMAINID =
		new FinderPath(ProjectStepModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStepModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByProjectStructureIdProjectDomainId",
			new String[] { Long.class.getName(), Long.class.getName() });

	/**
	 * Returns all the project steps where projectId = &#63; and projectDomainId = &#63;.
	 *
	 * @param projectId the project ID
	 * @param projectDomainId the project domain ID
	 * @return the matching project steps
	 */
	@Override
	public List<ProjectStep> findByProjectStructureIdProjectDomainId(
		long projectId, long projectDomainId) {
		return findByProjectStructureIdProjectDomainId(projectId,
			projectDomainId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the project steps where projectId = &#63; and projectDomainId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param projectId the project ID
	 * @param projectDomainId the project domain ID
	 * @param start the lower bound of the range of project steps
	 * @param end the upper bound of the range of project steps (not inclusive)
	 * @return the range of matching project steps
	 */
	@Override
	public List<ProjectStep> findByProjectStructureIdProjectDomainId(
		long projectId, long projectDomainId, int start, int end) {
		return findByProjectStructureIdProjectDomainId(projectId,
			projectDomainId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the project steps where projectId = &#63; and projectDomainId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param projectId the project ID
	 * @param projectDomainId the project domain ID
	 * @param start the lower bound of the range of project steps
	 * @param end the upper bound of the range of project steps (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching project steps
	 */
	@Override
	public List<ProjectStep> findByProjectStructureIdProjectDomainId(
		long projectId, long projectDomainId, int start, int end,
		OrderByComparator<ProjectStep> orderByComparator) {
		return findByProjectStructureIdProjectDomainId(projectId,
			projectDomainId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the project steps where projectId = &#63; and projectDomainId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param projectId the project ID
	 * @param projectDomainId the project domain ID
	 * @param start the lower bound of the range of project steps
	 * @param end the upper bound of the range of project steps (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching project steps
	 */
	@Override
	public List<ProjectStep> findByProjectStructureIdProjectDomainId(
		long projectId, long projectDomainId, int start, int end,
		OrderByComparator<ProjectStep> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTSTRUCTUREIDPROJECTDOMAINID;
			finderArgs = new Object[] { projectId, projectDomainId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PROJECTSTRUCTUREIDPROJECTDOMAINID;
			finderArgs = new Object[] {
					projectId, projectDomainId,
					
					start, end, orderByComparator
				};
		}

		List<ProjectStep> list = null;

		if (retrieveFromCache) {
			list = (List<ProjectStep>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ProjectStep projectStep : list) {
					if ((projectId != projectStep.getProjectId()) ||
							(projectDomainId != projectStep.getProjectDomainId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_PROJECTSTEP_WHERE);

			query.append(_FINDER_COLUMN_PROJECTSTRUCTUREIDPROJECTDOMAINID_PROJECTID_2);

			query.append(_FINDER_COLUMN_PROJECTSTRUCTUREIDPROJECTDOMAINID_PROJECTDOMAINID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProjectStepModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(projectId);

				qPos.add(projectDomainId);

				if (!pagination) {
					list = (List<ProjectStep>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ProjectStep>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first project step in the ordered set where projectId = &#63; and projectDomainId = &#63;.
	 *
	 * @param projectId the project ID
	 * @param projectDomainId the project domain ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project step
	 * @throws NoSuchProjectStepException if a matching project step could not be found
	 */
	@Override
	public ProjectStep findByProjectStructureIdProjectDomainId_First(
		long projectId, long projectDomainId,
		OrderByComparator<ProjectStep> orderByComparator)
		throws NoSuchProjectStepException {
		ProjectStep projectStep = fetchByProjectStructureIdProjectDomainId_First(projectId,
				projectDomainId, orderByComparator);

		if (projectStep != null) {
			return projectStep;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("projectId=");
		msg.append(projectId);

		msg.append(", projectDomainId=");
		msg.append(projectDomainId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectStepException(msg.toString());
	}

	/**
	 * Returns the first project step in the ordered set where projectId = &#63; and projectDomainId = &#63;.
	 *
	 * @param projectId the project ID
	 * @param projectDomainId the project domain ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project step, or <code>null</code> if a matching project step could not be found
	 */
	@Override
	public ProjectStep fetchByProjectStructureIdProjectDomainId_First(
		long projectId, long projectDomainId,
		OrderByComparator<ProjectStep> orderByComparator) {
		List<ProjectStep> list = findByProjectStructureIdProjectDomainId(projectId,
				projectDomainId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last project step in the ordered set where projectId = &#63; and projectDomainId = &#63;.
	 *
	 * @param projectId the project ID
	 * @param projectDomainId the project domain ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project step
	 * @throws NoSuchProjectStepException if a matching project step could not be found
	 */
	@Override
	public ProjectStep findByProjectStructureIdProjectDomainId_Last(
		long projectId, long projectDomainId,
		OrderByComparator<ProjectStep> orderByComparator)
		throws NoSuchProjectStepException {
		ProjectStep projectStep = fetchByProjectStructureIdProjectDomainId_Last(projectId,
				projectDomainId, orderByComparator);

		if (projectStep != null) {
			return projectStep;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("projectId=");
		msg.append(projectId);

		msg.append(", projectDomainId=");
		msg.append(projectDomainId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectStepException(msg.toString());
	}

	/**
	 * Returns the last project step in the ordered set where projectId = &#63; and projectDomainId = &#63;.
	 *
	 * @param projectId the project ID
	 * @param projectDomainId the project domain ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project step, or <code>null</code> if a matching project step could not be found
	 */
	@Override
	public ProjectStep fetchByProjectStructureIdProjectDomainId_Last(
		long projectId, long projectDomainId,
		OrderByComparator<ProjectStep> orderByComparator) {
		int count = countByProjectStructureIdProjectDomainId(projectId,
				projectDomainId);

		if (count == 0) {
			return null;
		}

		List<ProjectStep> list = findByProjectStructureIdProjectDomainId(projectId,
				projectDomainId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the project steps before and after the current project step in the ordered set where projectId = &#63; and projectDomainId = &#63;.
	 *
	 * @param projectStepId the primary key of the current project step
	 * @param projectId the project ID
	 * @param projectDomainId the project domain ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next project step
	 * @throws NoSuchProjectStepException if a project step with the primary key could not be found
	 */
	@Override
	public ProjectStep[] findByProjectStructureIdProjectDomainId_PrevAndNext(
		long projectStepId, long projectId, long projectDomainId,
		OrderByComparator<ProjectStep> orderByComparator)
		throws NoSuchProjectStepException {
		ProjectStep projectStep = findByPrimaryKey(projectStepId);

		Session session = null;

		try {
			session = openSession();

			ProjectStep[] array = new ProjectStepImpl[3];

			array[0] = getByProjectStructureIdProjectDomainId_PrevAndNext(session,
					projectStep, projectId, projectDomainId, orderByComparator,
					true);

			array[1] = projectStep;

			array[2] = getByProjectStructureIdProjectDomainId_PrevAndNext(session,
					projectStep, projectId, projectDomainId, orderByComparator,
					false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ProjectStep getByProjectStructureIdProjectDomainId_PrevAndNext(
		Session session, ProjectStep projectStep, long projectId,
		long projectDomainId, OrderByComparator<ProjectStep> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_PROJECTSTEP_WHERE);

		query.append(_FINDER_COLUMN_PROJECTSTRUCTUREIDPROJECTDOMAINID_PROJECTID_2);

		query.append(_FINDER_COLUMN_PROJECTSTRUCTUREIDPROJECTDOMAINID_PROJECTDOMAINID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProjectStepModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(projectId);

		qPos.add(projectDomainId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(projectStep);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ProjectStep> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the project steps where projectId = &#63; and projectDomainId = &#63; from the database.
	 *
	 * @param projectId the project ID
	 * @param projectDomainId the project domain ID
	 */
	@Override
	public void removeByProjectStructureIdProjectDomainId(long projectId,
		long projectDomainId) {
		for (ProjectStep projectStep : findByProjectStructureIdProjectDomainId(
				projectId, projectDomainId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(projectStep);
		}
	}

	/**
	 * Returns the number of project steps where projectId = &#63; and projectDomainId = &#63;.
	 *
	 * @param projectId the project ID
	 * @param projectDomainId the project domain ID
	 * @return the number of matching project steps
	 */
	@Override
	public int countByProjectStructureIdProjectDomainId(long projectId,
		long projectDomainId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PROJECTSTRUCTUREIDPROJECTDOMAINID;

		Object[] finderArgs = new Object[] { projectId, projectDomainId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_PROJECTSTEP_WHERE);

			query.append(_FINDER_COLUMN_PROJECTSTRUCTUREIDPROJECTDOMAINID_PROJECTID_2);

			query.append(_FINDER_COLUMN_PROJECTSTRUCTUREIDPROJECTDOMAINID_PROJECTDOMAINID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(projectId);

				qPos.add(projectDomainId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PROJECTSTRUCTUREIDPROJECTDOMAINID_PROJECTID_2 =
		"projectStep.projectId = ? AND ";
	private static final String _FINDER_COLUMN_PROJECTSTRUCTUREIDPROJECTDOMAINID_PROJECTDOMAINID_2 =
		"projectStep.projectDomainId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PROJECTIDPROJECTPHASEID =
		new FinderPath(ProjectStepModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStepModelImpl.FINDER_CACHE_ENABLED, ProjectStepImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByProjectIdProjectPhaseId",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTIDPROJECTPHASEID =
		new FinderPath(ProjectStepModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStepModelImpl.FINDER_CACHE_ENABLED, ProjectStepImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByProjectIdProjectPhaseId",
			new String[] { Long.class.getName(), Long.class.getName() },
			ProjectStepModelImpl.PROJECTID_COLUMN_BITMASK |
			ProjectStepModelImpl.PROJECTPHASEID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PROJECTIDPROJECTPHASEID = new FinderPath(ProjectStepModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStepModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByProjectIdProjectPhaseId",
			new String[] { Long.class.getName(), Long.class.getName() });

	/**
	 * Returns all the project steps where projectId = &#63; and projectPhaseId = &#63;.
	 *
	 * @param projectId the project ID
	 * @param projectPhaseId the project phase ID
	 * @return the matching project steps
	 */
	@Override
	public List<ProjectStep> findByProjectIdProjectPhaseId(long projectId,
		long projectPhaseId) {
		return findByProjectIdProjectPhaseId(projectId, projectPhaseId,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the project steps where projectId = &#63; and projectPhaseId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param projectId the project ID
	 * @param projectPhaseId the project phase ID
	 * @param start the lower bound of the range of project steps
	 * @param end the upper bound of the range of project steps (not inclusive)
	 * @return the range of matching project steps
	 */
	@Override
	public List<ProjectStep> findByProjectIdProjectPhaseId(long projectId,
		long projectPhaseId, int start, int end) {
		return findByProjectIdProjectPhaseId(projectId, projectPhaseId, start,
			end, null);
	}

	/**
	 * Returns an ordered range of all the project steps where projectId = &#63; and projectPhaseId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param projectId the project ID
	 * @param projectPhaseId the project phase ID
	 * @param start the lower bound of the range of project steps
	 * @param end the upper bound of the range of project steps (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching project steps
	 */
	@Override
	public List<ProjectStep> findByProjectIdProjectPhaseId(long projectId,
		long projectPhaseId, int start, int end,
		OrderByComparator<ProjectStep> orderByComparator) {
		return findByProjectIdProjectPhaseId(projectId, projectPhaseId, start,
			end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the project steps where projectId = &#63; and projectPhaseId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param projectId the project ID
	 * @param projectPhaseId the project phase ID
	 * @param start the lower bound of the range of project steps
	 * @param end the upper bound of the range of project steps (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching project steps
	 */
	@Override
	public List<ProjectStep> findByProjectIdProjectPhaseId(long projectId,
		long projectPhaseId, int start, int end,
		OrderByComparator<ProjectStep> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTIDPROJECTPHASEID;
			finderArgs = new Object[] { projectId, projectPhaseId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PROJECTIDPROJECTPHASEID;
			finderArgs = new Object[] {
					projectId, projectPhaseId,
					
					start, end, orderByComparator
				};
		}

		List<ProjectStep> list = null;

		if (retrieveFromCache) {
			list = (List<ProjectStep>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ProjectStep projectStep : list) {
					if ((projectId != projectStep.getProjectId()) ||
							(projectPhaseId != projectStep.getProjectPhaseId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_PROJECTSTEP_WHERE);

			query.append(_FINDER_COLUMN_PROJECTIDPROJECTPHASEID_PROJECTID_2);

			query.append(_FINDER_COLUMN_PROJECTIDPROJECTPHASEID_PROJECTPHASEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProjectStepModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(projectId);

				qPos.add(projectPhaseId);

				if (!pagination) {
					list = (List<ProjectStep>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ProjectStep>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first project step in the ordered set where projectId = &#63; and projectPhaseId = &#63;.
	 *
	 * @param projectId the project ID
	 * @param projectPhaseId the project phase ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project step
	 * @throws NoSuchProjectStepException if a matching project step could not be found
	 */
	@Override
	public ProjectStep findByProjectIdProjectPhaseId_First(long projectId,
		long projectPhaseId, OrderByComparator<ProjectStep> orderByComparator)
		throws NoSuchProjectStepException {
		ProjectStep projectStep = fetchByProjectIdProjectPhaseId_First(projectId,
				projectPhaseId, orderByComparator);

		if (projectStep != null) {
			return projectStep;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("projectId=");
		msg.append(projectId);

		msg.append(", projectPhaseId=");
		msg.append(projectPhaseId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectStepException(msg.toString());
	}

	/**
	 * Returns the first project step in the ordered set where projectId = &#63; and projectPhaseId = &#63;.
	 *
	 * @param projectId the project ID
	 * @param projectPhaseId the project phase ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project step, or <code>null</code> if a matching project step could not be found
	 */
	@Override
	public ProjectStep fetchByProjectIdProjectPhaseId_First(long projectId,
		long projectPhaseId, OrderByComparator<ProjectStep> orderByComparator) {
		List<ProjectStep> list = findByProjectIdProjectPhaseId(projectId,
				projectPhaseId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last project step in the ordered set where projectId = &#63; and projectPhaseId = &#63;.
	 *
	 * @param projectId the project ID
	 * @param projectPhaseId the project phase ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project step
	 * @throws NoSuchProjectStepException if a matching project step could not be found
	 */
	@Override
	public ProjectStep findByProjectIdProjectPhaseId_Last(long projectId,
		long projectPhaseId, OrderByComparator<ProjectStep> orderByComparator)
		throws NoSuchProjectStepException {
		ProjectStep projectStep = fetchByProjectIdProjectPhaseId_Last(projectId,
				projectPhaseId, orderByComparator);

		if (projectStep != null) {
			return projectStep;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("projectId=");
		msg.append(projectId);

		msg.append(", projectPhaseId=");
		msg.append(projectPhaseId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectStepException(msg.toString());
	}

	/**
	 * Returns the last project step in the ordered set where projectId = &#63; and projectPhaseId = &#63;.
	 *
	 * @param projectId the project ID
	 * @param projectPhaseId the project phase ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project step, or <code>null</code> if a matching project step could not be found
	 */
	@Override
	public ProjectStep fetchByProjectIdProjectPhaseId_Last(long projectId,
		long projectPhaseId, OrderByComparator<ProjectStep> orderByComparator) {
		int count = countByProjectIdProjectPhaseId(projectId, projectPhaseId);

		if (count == 0) {
			return null;
		}

		List<ProjectStep> list = findByProjectIdProjectPhaseId(projectId,
				projectPhaseId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the project steps before and after the current project step in the ordered set where projectId = &#63; and projectPhaseId = &#63;.
	 *
	 * @param projectStepId the primary key of the current project step
	 * @param projectId the project ID
	 * @param projectPhaseId the project phase ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next project step
	 * @throws NoSuchProjectStepException if a project step with the primary key could not be found
	 */
	@Override
	public ProjectStep[] findByProjectIdProjectPhaseId_PrevAndNext(
		long projectStepId, long projectId, long projectPhaseId,
		OrderByComparator<ProjectStep> orderByComparator)
		throws NoSuchProjectStepException {
		ProjectStep projectStep = findByPrimaryKey(projectStepId);

		Session session = null;

		try {
			session = openSession();

			ProjectStep[] array = new ProjectStepImpl[3];

			array[0] = getByProjectIdProjectPhaseId_PrevAndNext(session,
					projectStep, projectId, projectPhaseId, orderByComparator,
					true);

			array[1] = projectStep;

			array[2] = getByProjectIdProjectPhaseId_PrevAndNext(session,
					projectStep, projectId, projectPhaseId, orderByComparator,
					false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ProjectStep getByProjectIdProjectPhaseId_PrevAndNext(
		Session session, ProjectStep projectStep, long projectId,
		long projectPhaseId, OrderByComparator<ProjectStep> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_PROJECTSTEP_WHERE);

		query.append(_FINDER_COLUMN_PROJECTIDPROJECTPHASEID_PROJECTID_2);

		query.append(_FINDER_COLUMN_PROJECTIDPROJECTPHASEID_PROJECTPHASEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProjectStepModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(projectId);

		qPos.add(projectPhaseId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(projectStep);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ProjectStep> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the project steps where projectId = &#63; and projectPhaseId = &#63; from the database.
	 *
	 * @param projectId the project ID
	 * @param projectPhaseId the project phase ID
	 */
	@Override
	public void removeByProjectIdProjectPhaseId(long projectId,
		long projectPhaseId) {
		for (ProjectStep projectStep : findByProjectIdProjectPhaseId(
				projectId, projectPhaseId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(projectStep);
		}
	}

	/**
	 * Returns the number of project steps where projectId = &#63; and projectPhaseId = &#63;.
	 *
	 * @param projectId the project ID
	 * @param projectPhaseId the project phase ID
	 * @return the number of matching project steps
	 */
	@Override
	public int countByProjectIdProjectPhaseId(long projectId,
		long projectPhaseId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PROJECTIDPROJECTPHASEID;

		Object[] finderArgs = new Object[] { projectId, projectPhaseId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_PROJECTSTEP_WHERE);

			query.append(_FINDER_COLUMN_PROJECTIDPROJECTPHASEID_PROJECTID_2);

			query.append(_FINDER_COLUMN_PROJECTIDPROJECTPHASEID_PROJECTPHASEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(projectId);

				qPos.add(projectPhaseId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PROJECTIDPROJECTPHASEID_PROJECTID_2 =
		"projectStep.projectId = ? AND ";
	private static final String _FINDER_COLUMN_PROJECTIDPROJECTPHASEID_PROJECTPHASEID_2 =
		"projectStep.projectPhaseId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_PROJECTIDPROJECTDOMAINIDPROJECTPHASEID =
		new FinderPath(ProjectStepModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStepModelImpl.FINDER_CACHE_ENABLED, ProjectStepImpl.class,
			FINDER_CLASS_NAME_ENTITY,
			"fetchByProjectIdProjectDomainIdProjectPhaseId",
			new String[] {
				Long.class.getName(), Long.class.getName(), Long.class.getName()
			},
			ProjectStepModelImpl.PROJECTID_COLUMN_BITMASK |
			ProjectStepModelImpl.PROJECTDOMAINID_COLUMN_BITMASK |
			ProjectStepModelImpl.PROJECTPHASEID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PROJECTIDPROJECTDOMAINIDPROJECTPHASEID =
		new FinderPath(ProjectStepModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStepModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByProjectIdProjectDomainIdProjectPhaseId",
			new String[] {
				Long.class.getName(), Long.class.getName(), Long.class.getName()
			});

	/**
	 * Returns the project step where projectId = &#63; and projectDomainId = &#63; and projectPhaseId = &#63; or throws a {@link NoSuchProjectStepException} if it could not be found.
	 *
	 * @param projectId the project ID
	 * @param projectDomainId the project domain ID
	 * @param projectPhaseId the project phase ID
	 * @return the matching project step
	 * @throws NoSuchProjectStepException if a matching project step could not be found
	 */
	@Override
	public ProjectStep findByProjectIdProjectDomainIdProjectPhaseId(
		long projectId, long projectDomainId, long projectPhaseId)
		throws NoSuchProjectStepException {
		ProjectStep projectStep = fetchByProjectIdProjectDomainIdProjectPhaseId(projectId,
				projectDomainId, projectPhaseId);

		if (projectStep == null) {
			StringBundler msg = new StringBundler(8);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("projectId=");
			msg.append(projectId);

			msg.append(", projectDomainId=");
			msg.append(projectDomainId);

			msg.append(", projectPhaseId=");
			msg.append(projectPhaseId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchProjectStepException(msg.toString());
		}

		return projectStep;
	}

	/**
	 * Returns the project step where projectId = &#63; and projectDomainId = &#63; and projectPhaseId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param projectId the project ID
	 * @param projectDomainId the project domain ID
	 * @param projectPhaseId the project phase ID
	 * @return the matching project step, or <code>null</code> if a matching project step could not be found
	 */
	@Override
	public ProjectStep fetchByProjectIdProjectDomainIdProjectPhaseId(
		long projectId, long projectDomainId, long projectPhaseId) {
		return fetchByProjectIdProjectDomainIdProjectPhaseId(projectId,
			projectDomainId, projectPhaseId, true);
	}

	/**
	 * Returns the project step where projectId = &#63; and projectDomainId = &#63; and projectPhaseId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param projectId the project ID
	 * @param projectDomainId the project domain ID
	 * @param projectPhaseId the project phase ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching project step, or <code>null</code> if a matching project step could not be found
	 */
	@Override
	public ProjectStep fetchByProjectIdProjectDomainIdProjectPhaseId(
		long projectId, long projectDomainId, long projectPhaseId,
		boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] {
				projectId, projectDomainId, projectPhaseId
			};

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_PROJECTIDPROJECTDOMAINIDPROJECTPHASEID,
					finderArgs, this);
		}

		if (result instanceof ProjectStep) {
			ProjectStep projectStep = (ProjectStep)result;

			if ((projectId != projectStep.getProjectId()) ||
					(projectDomainId != projectStep.getProjectDomainId()) ||
					(projectPhaseId != projectStep.getProjectPhaseId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(5);

			query.append(_SQL_SELECT_PROJECTSTEP_WHERE);

			query.append(_FINDER_COLUMN_PROJECTIDPROJECTDOMAINIDPROJECTPHASEID_PROJECTID_2);

			query.append(_FINDER_COLUMN_PROJECTIDPROJECTDOMAINIDPROJECTPHASEID_PROJECTDOMAINID_2);

			query.append(_FINDER_COLUMN_PROJECTIDPROJECTDOMAINIDPROJECTPHASEID_PROJECTPHASEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(projectId);

				qPos.add(projectDomainId);

				qPos.add(projectPhaseId);

				List<ProjectStep> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_PROJECTIDPROJECTDOMAINIDPROJECTPHASEID,
						finderArgs, list);
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							_log.warn(
								"ProjectStepPersistenceImpl.fetchByProjectIdProjectDomainIdProjectPhaseId(long, long, long, boolean) with parameters (" +
								StringUtil.merge(finderArgs) +
								") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					ProjectStep projectStep = list.get(0);

					result = projectStep;

					cacheResult(projectStep);

					if ((projectStep.getProjectId() != projectId) ||
							(projectStep.getProjectDomainId() != projectDomainId) ||
							(projectStep.getProjectPhaseId() != projectPhaseId)) {
						finderCache.putResult(FINDER_PATH_FETCH_BY_PROJECTIDPROJECTDOMAINIDPROJECTPHASEID,
							finderArgs, projectStep);
					}
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_PROJECTIDPROJECTDOMAINIDPROJECTPHASEID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (ProjectStep)result;
		}
	}

	/**
	 * Removes the project step where projectId = &#63; and projectDomainId = &#63; and projectPhaseId = &#63; from the database.
	 *
	 * @param projectId the project ID
	 * @param projectDomainId the project domain ID
	 * @param projectPhaseId the project phase ID
	 * @return the project step that was removed
	 */
	@Override
	public ProjectStep removeByProjectIdProjectDomainIdProjectPhaseId(
		long projectId, long projectDomainId, long projectPhaseId)
		throws NoSuchProjectStepException {
		ProjectStep projectStep = findByProjectIdProjectDomainIdProjectPhaseId(projectId,
				projectDomainId, projectPhaseId);

		return remove(projectStep);
	}

	/**
	 * Returns the number of project steps where projectId = &#63; and projectDomainId = &#63; and projectPhaseId = &#63;.
	 *
	 * @param projectId the project ID
	 * @param projectDomainId the project domain ID
	 * @param projectPhaseId the project phase ID
	 * @return the number of matching project steps
	 */
	@Override
	public int countByProjectIdProjectDomainIdProjectPhaseId(long projectId,
		long projectDomainId, long projectPhaseId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PROJECTIDPROJECTDOMAINIDPROJECTPHASEID;

		Object[] finderArgs = new Object[] {
				projectId, projectDomainId, projectPhaseId
			};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_COUNT_PROJECTSTEP_WHERE);

			query.append(_FINDER_COLUMN_PROJECTIDPROJECTDOMAINIDPROJECTPHASEID_PROJECTID_2);

			query.append(_FINDER_COLUMN_PROJECTIDPROJECTDOMAINIDPROJECTPHASEID_PROJECTDOMAINID_2);

			query.append(_FINDER_COLUMN_PROJECTIDPROJECTDOMAINIDPROJECTPHASEID_PROJECTPHASEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(projectId);

				qPos.add(projectDomainId);

				qPos.add(projectPhaseId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PROJECTIDPROJECTDOMAINIDPROJECTPHASEID_PROJECTID_2 =
		"projectStep.projectId = ? AND ";
	private static final String _FINDER_COLUMN_PROJECTIDPROJECTDOMAINIDPROJECTPHASEID_PROJECTDOMAINID_2 =
		"projectStep.projectDomainId = ? AND ";
	private static final String _FINDER_COLUMN_PROJECTIDPROJECTDOMAINIDPROJECTPHASEID_PROJECTPHASEID_2 =
		"projectStep.projectPhaseId = ?";

	public ProjectStepPersistenceImpl() {
		setModelClass(ProjectStep.class);
	}

	/**
	 * Caches the project step in the entity cache if it is enabled.
	 *
	 * @param projectStep the project step
	 */
	@Override
	public void cacheResult(ProjectStep projectStep) {
		entityCache.putResult(ProjectStepModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStepImpl.class, projectStep.getPrimaryKey(), projectStep);

		finderCache.putResult(FINDER_PATH_FETCH_BY_PROJECTIDPROJECTDOMAINIDPROJECTPHASEID,
			new Object[] {
				projectStep.getProjectId(), projectStep.getProjectDomainId(),
				projectStep.getProjectPhaseId()
			}, projectStep);

		projectStep.resetOriginalValues();
	}

	/**
	 * Caches the project steps in the entity cache if it is enabled.
	 *
	 * @param projectSteps the project steps
	 */
	@Override
	public void cacheResult(List<ProjectStep> projectSteps) {
		for (ProjectStep projectStep : projectSteps) {
			if (entityCache.getResult(
						ProjectStepModelImpl.ENTITY_CACHE_ENABLED,
						ProjectStepImpl.class, projectStep.getPrimaryKey()) == null) {
				cacheResult(projectStep);
			}
			else {
				projectStep.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all project steps.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(ProjectStepImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the project step.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ProjectStep projectStep) {
		entityCache.removeResult(ProjectStepModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStepImpl.class, projectStep.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache((ProjectStepModelImpl)projectStep);
	}

	@Override
	public void clearCache(List<ProjectStep> projectSteps) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ProjectStep projectStep : projectSteps) {
			entityCache.removeResult(ProjectStepModelImpl.ENTITY_CACHE_ENABLED,
				ProjectStepImpl.class, projectStep.getPrimaryKey());

			clearUniqueFindersCache((ProjectStepModelImpl)projectStep);
		}
	}

	protected void cacheUniqueFindersCache(
		ProjectStepModelImpl projectStepModelImpl, boolean isNew) {
		if (isNew) {
			Object[] args = new Object[] {
					projectStepModelImpl.getProjectId(),
					projectStepModelImpl.getProjectDomainId(),
					projectStepModelImpl.getProjectPhaseId()
				};

			finderCache.putResult(FINDER_PATH_COUNT_BY_PROJECTIDPROJECTDOMAINIDPROJECTPHASEID,
				args, Long.valueOf(1));
			finderCache.putResult(FINDER_PATH_FETCH_BY_PROJECTIDPROJECTDOMAINIDPROJECTPHASEID,
				args, projectStepModelImpl);
		}
		else {
			if ((projectStepModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_PROJECTIDPROJECTDOMAINIDPROJECTPHASEID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						projectStepModelImpl.getProjectId(),
						projectStepModelImpl.getProjectDomainId(),
						projectStepModelImpl.getProjectPhaseId()
					};

				finderCache.putResult(FINDER_PATH_COUNT_BY_PROJECTIDPROJECTDOMAINIDPROJECTPHASEID,
					args, Long.valueOf(1));
				finderCache.putResult(FINDER_PATH_FETCH_BY_PROJECTIDPROJECTDOMAINIDPROJECTPHASEID,
					args, projectStepModelImpl);
			}
		}
	}

	protected void clearUniqueFindersCache(
		ProjectStepModelImpl projectStepModelImpl) {
		Object[] args = new Object[] {
				projectStepModelImpl.getProjectId(),
				projectStepModelImpl.getProjectDomainId(),
				projectStepModelImpl.getProjectPhaseId()
			};

		finderCache.removeResult(FINDER_PATH_COUNT_BY_PROJECTIDPROJECTDOMAINIDPROJECTPHASEID,
			args);
		finderCache.removeResult(FINDER_PATH_FETCH_BY_PROJECTIDPROJECTDOMAINIDPROJECTPHASEID,
			args);

		if ((projectStepModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_PROJECTIDPROJECTDOMAINIDPROJECTPHASEID.getColumnBitmask()) != 0) {
			args = new Object[] {
					projectStepModelImpl.getOriginalProjectId(),
					projectStepModelImpl.getOriginalProjectDomainId(),
					projectStepModelImpl.getOriginalProjectPhaseId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_PROJECTIDPROJECTDOMAINIDPROJECTPHASEID,
				args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_PROJECTIDPROJECTDOMAINIDPROJECTPHASEID,
				args);
		}
	}

	/**
	 * Creates a new project step with the primary key. Does not add the project step to the database.
	 *
	 * @param projectStepId the primary key for the new project step
	 * @return the new project step
	 */
	@Override
	public ProjectStep create(long projectStepId) {
		ProjectStep projectStep = new ProjectStepImpl();

		projectStep.setNew(true);
		projectStep.setPrimaryKey(projectStepId);

		String uuid = PortalUUIDUtil.generate();

		projectStep.setUuid(uuid);

		projectStep.setCompanyId(companyProvider.getCompanyId());

		return projectStep;
	}

	/**
	 * Removes the project step with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param projectStepId the primary key of the project step
	 * @return the project step that was removed
	 * @throws NoSuchProjectStepException if a project step with the primary key could not be found
	 */
	@Override
	public ProjectStep remove(long projectStepId)
		throws NoSuchProjectStepException {
		return remove((Serializable)projectStepId);
	}

	/**
	 * Removes the project step with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the project step
	 * @return the project step that was removed
	 * @throws NoSuchProjectStepException if a project step with the primary key could not be found
	 */
	@Override
	public ProjectStep remove(Serializable primaryKey)
		throws NoSuchProjectStepException {
		Session session = null;

		try {
			session = openSession();

			ProjectStep projectStep = (ProjectStep)session.get(ProjectStepImpl.class,
					primaryKey);

			if (projectStep == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchProjectStepException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(projectStep);
		}
		catch (NoSuchProjectStepException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ProjectStep removeImpl(ProjectStep projectStep) {
		projectStep = toUnwrappedModel(projectStep);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(projectStep)) {
				projectStep = (ProjectStep)session.get(ProjectStepImpl.class,
						projectStep.getPrimaryKeyObj());
			}

			if (projectStep != null) {
				session.delete(projectStep);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (projectStep != null) {
			clearCache(projectStep);
		}

		return projectStep;
	}

	@Override
	public ProjectStep updateImpl(ProjectStep projectStep) {
		projectStep = toUnwrappedModel(projectStep);

		boolean isNew = projectStep.isNew();

		ProjectStepModelImpl projectStepModelImpl = (ProjectStepModelImpl)projectStep;

		if (Validator.isNull(projectStep.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			projectStep.setUuid(uuid);
		}

		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();

		Date now = new Date();

		if (isNew && (projectStep.getCreateDate() == null)) {
			if (serviceContext == null) {
				projectStep.setCreateDate(now);
			}
			else {
				projectStep.setCreateDate(serviceContext.getCreateDate(now));
			}
		}

		if (!projectStepModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				projectStep.setModifiedDate(now);
			}
			else {
				projectStep.setModifiedDate(serviceContext.getModifiedDate(now));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (projectStep.isNew()) {
				session.save(projectStep);

				projectStep.setNew(false);
			}
			else {
				projectStep = (ProjectStep)session.merge(projectStep);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ProjectStepModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((projectStepModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						projectStepModelImpl.getOriginalUuid()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);

				args = new Object[] { projectStepModelImpl.getUuid() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);
			}

			if ((projectStepModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						projectStepModelImpl.getOriginalUuid(),
						projectStepModelImpl.getOriginalCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);

				args = new Object[] {
						projectStepModelImpl.getUuid(),
						projectStepModelImpl.getCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);
			}

			if ((projectStepModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						projectStepModelImpl.getOriginalProjectId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_PROJECTID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTID,
					args);

				args = new Object[] { projectStepModelImpl.getProjectId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_PROJECTID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTID,
					args);
			}

			if ((projectStepModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTSTRUCTUREIDPROJECTDOMAINID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						projectStepModelImpl.getOriginalProjectId(),
						projectStepModelImpl.getOriginalProjectDomainId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_PROJECTSTRUCTUREIDPROJECTDOMAINID,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTSTRUCTUREIDPROJECTDOMAINID,
					args);

				args = new Object[] {
						projectStepModelImpl.getProjectId(),
						projectStepModelImpl.getProjectDomainId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_PROJECTSTRUCTUREIDPROJECTDOMAINID,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTSTRUCTUREIDPROJECTDOMAINID,
					args);
			}

			if ((projectStepModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTIDPROJECTPHASEID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						projectStepModelImpl.getOriginalProjectId(),
						projectStepModelImpl.getOriginalProjectPhaseId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_PROJECTIDPROJECTPHASEID,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTIDPROJECTPHASEID,
					args);

				args = new Object[] {
						projectStepModelImpl.getProjectId(),
						projectStepModelImpl.getProjectPhaseId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_PROJECTIDPROJECTPHASEID,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTIDPROJECTPHASEID,
					args);
			}
		}

		entityCache.putResult(ProjectStepModelImpl.ENTITY_CACHE_ENABLED,
			ProjectStepImpl.class, projectStep.getPrimaryKey(), projectStep,
			false);

		clearUniqueFindersCache(projectStepModelImpl);
		cacheUniqueFindersCache(projectStepModelImpl, isNew);

		projectStep.resetOriginalValues();

		return projectStep;
	}

	protected ProjectStep toUnwrappedModel(ProjectStep projectStep) {
		if (projectStep instanceof ProjectStepImpl) {
			return projectStep;
		}

		ProjectStepImpl projectStepImpl = new ProjectStepImpl();

		projectStepImpl.setNew(projectStep.isNew());
		projectStepImpl.setPrimaryKey(projectStep.getPrimaryKey());

		projectStepImpl.setUuid(projectStep.getUuid());
		projectStepImpl.setProjectStepId(projectStep.getProjectStepId());
		projectStepImpl.setProjectId(projectStep.getProjectId());
		projectStepImpl.setProjectDomainId(projectStep.getProjectDomainId());
		projectStepImpl.setProjectPhaseId(projectStep.getProjectPhaseId());
		projectStepImpl.setCompanyId(projectStep.getCompanyId());
		projectStepImpl.setUserId(projectStep.getUserId());
		projectStepImpl.setUserName(projectStep.getUserName());
		projectStepImpl.setCreateDate(projectStep.getCreateDate());
		projectStepImpl.setModifiedDate(projectStep.getModifiedDate());
		projectStepImpl.setPercentage(projectStep.getPercentage());

		return projectStepImpl;
	}

	/**
	 * Returns the project step with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the project step
	 * @return the project step
	 * @throws NoSuchProjectStepException if a project step with the primary key could not be found
	 */
	@Override
	public ProjectStep findByPrimaryKey(Serializable primaryKey)
		throws NoSuchProjectStepException {
		ProjectStep projectStep = fetchByPrimaryKey(primaryKey);

		if (projectStep == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchProjectStepException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return projectStep;
	}

	/**
	 * Returns the project step with the primary key or throws a {@link NoSuchProjectStepException} if it could not be found.
	 *
	 * @param projectStepId the primary key of the project step
	 * @return the project step
	 * @throws NoSuchProjectStepException if a project step with the primary key could not be found
	 */
	@Override
	public ProjectStep findByPrimaryKey(long projectStepId)
		throws NoSuchProjectStepException {
		return findByPrimaryKey((Serializable)projectStepId);
	}

	/**
	 * Returns the project step with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the project step
	 * @return the project step, or <code>null</code> if a project step with the primary key could not be found
	 */
	@Override
	public ProjectStep fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(ProjectStepModelImpl.ENTITY_CACHE_ENABLED,
				ProjectStepImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		ProjectStep projectStep = (ProjectStep)serializable;

		if (projectStep == null) {
			Session session = null;

			try {
				session = openSession();

				projectStep = (ProjectStep)session.get(ProjectStepImpl.class,
						primaryKey);

				if (projectStep != null) {
					cacheResult(projectStep);
				}
				else {
					entityCache.putResult(ProjectStepModelImpl.ENTITY_CACHE_ENABLED,
						ProjectStepImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(ProjectStepModelImpl.ENTITY_CACHE_ENABLED,
					ProjectStepImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return projectStep;
	}

	/**
	 * Returns the project step with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param projectStepId the primary key of the project step
	 * @return the project step, or <code>null</code> if a project step with the primary key could not be found
	 */
	@Override
	public ProjectStep fetchByPrimaryKey(long projectStepId) {
		return fetchByPrimaryKey((Serializable)projectStepId);
	}

	@Override
	public Map<Serializable, ProjectStep> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, ProjectStep> map = new HashMap<Serializable, ProjectStep>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			ProjectStep projectStep = fetchByPrimaryKey(primaryKey);

			if (projectStep != null) {
				map.put(primaryKey, projectStep);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(ProjectStepModelImpl.ENTITY_CACHE_ENABLED,
					ProjectStepImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (ProjectStep)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_PROJECTSTEP_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(String.valueOf(primaryKey));

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (ProjectStep projectStep : (List<ProjectStep>)q.list()) {
				map.put(projectStep.getPrimaryKeyObj(), projectStep);

				cacheResult(projectStep);

				uncachedPrimaryKeys.remove(projectStep.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(ProjectStepModelImpl.ENTITY_CACHE_ENABLED,
					ProjectStepImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the project steps.
	 *
	 * @return the project steps
	 */
	@Override
	public List<ProjectStep> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the project steps.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of project steps
	 * @param end the upper bound of the range of project steps (not inclusive)
	 * @return the range of project steps
	 */
	@Override
	public List<ProjectStep> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the project steps.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of project steps
	 * @param end the upper bound of the range of project steps (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of project steps
	 */
	@Override
	public List<ProjectStep> findAll(int start, int end,
		OrderByComparator<ProjectStep> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the project steps.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of project steps
	 * @param end the upper bound of the range of project steps (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of project steps
	 */
	@Override
	public List<ProjectStep> findAll(int start, int end,
		OrderByComparator<ProjectStep> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ProjectStep> list = null;

		if (retrieveFromCache) {
			list = (List<ProjectStep>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_PROJECTSTEP);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PROJECTSTEP;

				if (pagination) {
					sql = sql.concat(ProjectStepModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ProjectStep>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ProjectStep>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the project steps from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (ProjectStep projectStep : findAll()) {
			remove(projectStep);
		}
	}

	/**
	 * Returns the number of project steps.
	 *
	 * @return the number of project steps
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PROJECTSTEP);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return ProjectStepModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the project step persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(ProjectStepImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = CompanyProviderWrapper.class)
	protected CompanyProvider companyProvider;
	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_PROJECTSTEP = "SELECT projectStep FROM ProjectStep projectStep";
	private static final String _SQL_SELECT_PROJECTSTEP_WHERE_PKS_IN = "SELECT projectStep FROM ProjectStep projectStep WHERE projectStepId IN (";
	private static final String _SQL_SELECT_PROJECTSTEP_WHERE = "SELECT projectStep FROM ProjectStep projectStep WHERE ";
	private static final String _SQL_COUNT_PROJECTSTEP = "SELECT COUNT(projectStep) FROM ProjectStep projectStep";
	private static final String _SQL_COUNT_PROJECTSTEP_WHERE = "SELECT COUNT(projectStep) FROM ProjectStep projectStep WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "projectStep.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ProjectStep exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No ProjectStep exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(ProjectStepPersistenceImpl.class);
	private static final Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"uuid"
			});
}