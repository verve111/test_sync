/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.service.impl;

import java.util.List;

import com.collaboratelab.project.exception.NoSuchProjectStepException;
import com.collaboratelab.project.model.ProjectStep;
import com.collaboratelab.project.service.base.ProjectStepLocalServiceBaseImpl;

import aQute.bnd.annotation.ProviderType;

/**
 * The implementation of the project step local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.collaboratelab.project.service.ProjectStepLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Claudio Carlenzoli
 * @see ProjectStepLocalServiceBaseImpl
 * @see com.collaboratelab.project.service.ProjectStepLocalServiceUtil
 */
@ProviderType
public class ProjectStepLocalServiceImpl extends ProjectStepLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link com.collaboratelab.project.service.ProjectStepLocalServiceUtil} to access the project step local service.
	 */
	
	public ProjectStep findByProjectIdProjectDomainIdProjectPhaseId(long projectId, long projectDomainId, long projectPhaseId) {
		
		try {
			return this.projectStepPersistence.findByProjectIdProjectDomainIdProjectPhaseId(projectId,projectDomainId,projectPhaseId);
		} catch (NoSuchProjectStepException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	

	public List<ProjectStep> findByProjectIdProjectPhaseId(long projectId, long projectPhaseId) {
		
		return this.projectStepPersistence.findByProjectIdProjectPhaseId(projectId,projectPhaseId);
		
	}
	
	public List<ProjectStep> findByProjectId(long projectId) {
		
		return this.projectStepPersistence.findByProjectId(projectId);
	}
	
}