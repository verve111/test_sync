/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.project.exception.NoSuchProjectDeliveryException;
import com.collaboratelab.project.model.ProjectDelivery;
import com.collaboratelab.project.model.impl.ProjectDeliveryImpl;
import com.collaboratelab.project.model.impl.ProjectDeliveryModelImpl;
import com.collaboratelab.project.service.persistence.ProjectDeliveryPersistence;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.CompanyProvider;
import com.liferay.portal.kernel.service.persistence.CompanyProviderWrapper;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the project delivery service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Claudio Carlenzoli
 * @see ProjectDeliveryPersistence
 * @see com.collaboratelab.project.service.persistence.ProjectDeliveryUtil
 * @generated
 */
@ProviderType
public class ProjectDeliveryPersistenceImpl extends BasePersistenceImpl<ProjectDelivery>
	implements ProjectDeliveryPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ProjectDeliveryUtil} to access the project delivery persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ProjectDeliveryImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ProjectDeliveryModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDeliveryModelImpl.FINDER_CACHE_ENABLED,
			ProjectDeliveryImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ProjectDeliveryModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDeliveryModelImpl.FINDER_CACHE_ENABLED,
			ProjectDeliveryImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ProjectDeliveryModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDeliveryModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID = new FinderPath(ProjectDeliveryModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDeliveryModelImpl.FINDER_CACHE_ENABLED,
			ProjectDeliveryImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByUuid",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID = new FinderPath(ProjectDeliveryModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDeliveryModelImpl.FINDER_CACHE_ENABLED,
			ProjectDeliveryImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] { String.class.getName() },
			ProjectDeliveryModelImpl.UUID_COLUMN_BITMASK |
			ProjectDeliveryModelImpl.NAME_COLUMN_BITMASK |
			ProjectDeliveryModelImpl.STEPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID = new FinderPath(ProjectDeliveryModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDeliveryModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] { String.class.getName() });

	/**
	 * Returns all the project deliveries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching project deliveries
	 */
	@Override
	public List<ProjectDelivery> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the project deliveries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of project deliveries
	 * @param end the upper bound of the range of project deliveries (not inclusive)
	 * @return the range of matching project deliveries
	 */
	@Override
	public List<ProjectDelivery> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the project deliveries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of project deliveries
	 * @param end the upper bound of the range of project deliveries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching project deliveries
	 */
	@Override
	public List<ProjectDelivery> findByUuid(String uuid, int start, int end,
		OrderByComparator<ProjectDelivery> orderByComparator) {
		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the project deliveries where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of project deliveries
	 * @param end the upper bound of the range of project deliveries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching project deliveries
	 */
	@Override
	public List<ProjectDelivery> findByUuid(String uuid, int start, int end,
		OrderByComparator<ProjectDelivery> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid, start, end, orderByComparator };
		}

		List<ProjectDelivery> list = null;

		if (retrieveFromCache) {
			list = (List<ProjectDelivery>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ProjectDelivery projectDelivery : list) {
					if (!Objects.equals(uuid, projectDelivery.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PROJECTDELIVERY_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProjectDeliveryModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<ProjectDelivery>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ProjectDelivery>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first project delivery in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project delivery
	 * @throws NoSuchProjectDeliveryException if a matching project delivery could not be found
	 */
	@Override
	public ProjectDelivery findByUuid_First(String uuid,
		OrderByComparator<ProjectDelivery> orderByComparator)
		throws NoSuchProjectDeliveryException {
		ProjectDelivery projectDelivery = fetchByUuid_First(uuid,
				orderByComparator);

		if (projectDelivery != null) {
			return projectDelivery;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectDeliveryException(msg.toString());
	}

	/**
	 * Returns the first project delivery in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project delivery, or <code>null</code> if a matching project delivery could not be found
	 */
	@Override
	public ProjectDelivery fetchByUuid_First(String uuid,
		OrderByComparator<ProjectDelivery> orderByComparator) {
		List<ProjectDelivery> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last project delivery in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project delivery
	 * @throws NoSuchProjectDeliveryException if a matching project delivery could not be found
	 */
	@Override
	public ProjectDelivery findByUuid_Last(String uuid,
		OrderByComparator<ProjectDelivery> orderByComparator)
		throws NoSuchProjectDeliveryException {
		ProjectDelivery projectDelivery = fetchByUuid_Last(uuid,
				orderByComparator);

		if (projectDelivery != null) {
			return projectDelivery;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectDeliveryException(msg.toString());
	}

	/**
	 * Returns the last project delivery in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project delivery, or <code>null</code> if a matching project delivery could not be found
	 */
	@Override
	public ProjectDelivery fetchByUuid_Last(String uuid,
		OrderByComparator<ProjectDelivery> orderByComparator) {
		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<ProjectDelivery> list = findByUuid(uuid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the project deliveries before and after the current project delivery in the ordered set where uuid = &#63;.
	 *
	 * @param deliveryId the primary key of the current project delivery
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next project delivery
	 * @throws NoSuchProjectDeliveryException if a project delivery with the primary key could not be found
	 */
	@Override
	public ProjectDelivery[] findByUuid_PrevAndNext(long deliveryId,
		String uuid, OrderByComparator<ProjectDelivery> orderByComparator)
		throws NoSuchProjectDeliveryException {
		ProjectDelivery projectDelivery = findByPrimaryKey(deliveryId);

		Session session = null;

		try {
			session = openSession();

			ProjectDelivery[] array = new ProjectDeliveryImpl[3];

			array[0] = getByUuid_PrevAndNext(session, projectDelivery, uuid,
					orderByComparator, true);

			array[1] = projectDelivery;

			array[2] = getByUuid_PrevAndNext(session, projectDelivery, uuid,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ProjectDelivery getByUuid_PrevAndNext(Session session,
		ProjectDelivery projectDelivery, String uuid,
		OrderByComparator<ProjectDelivery> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PROJECTDELIVERY_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProjectDeliveryModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(projectDelivery);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ProjectDelivery> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the project deliveries where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (ProjectDelivery projectDelivery : findByUuid(uuid,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(projectDelivery);
		}
	}

	/**
	 * Returns the number of project deliveries where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching project deliveries
	 */
	@Override
	public int countByUuid(String uuid) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID;

		Object[] finderArgs = new Object[] { uuid };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PROJECTDELIVERY_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_1 = "projectDelivery.uuid IS NULL";
	private static final String _FINDER_COLUMN_UUID_UUID_2 = "projectDelivery.uuid = ?";
	private static final String _FINDER_COLUMN_UUID_UUID_3 = "(projectDelivery.uuid IS NULL OR projectDelivery.uuid = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C = new FinderPath(ProjectDeliveryModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDeliveryModelImpl.FINDER_CACHE_ENABLED,
			ProjectDeliveryImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C =
		new FinderPath(ProjectDeliveryModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDeliveryModelImpl.FINDER_CACHE_ENABLED,
			ProjectDeliveryImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() },
			ProjectDeliveryModelImpl.UUID_COLUMN_BITMASK |
			ProjectDeliveryModelImpl.COMPANYID_COLUMN_BITMASK |
			ProjectDeliveryModelImpl.NAME_COLUMN_BITMASK |
			ProjectDeliveryModelImpl.STEPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_C = new FinderPath(ProjectDeliveryModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDeliveryModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns all the project deliveries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching project deliveries
	 */
	@Override
	public List<ProjectDelivery> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(uuid, companyId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the project deliveries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of project deliveries
	 * @param end the upper bound of the range of project deliveries (not inclusive)
	 * @return the range of matching project deliveries
	 */
	@Override
	public List<ProjectDelivery> findByUuid_C(String uuid, long companyId,
		int start, int end) {
		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the project deliveries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of project deliveries
	 * @param end the upper bound of the range of project deliveries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching project deliveries
	 */
	@Override
	public List<ProjectDelivery> findByUuid_C(String uuid, long companyId,
		int start, int end, OrderByComparator<ProjectDelivery> orderByComparator) {
		return findByUuid_C(uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the project deliveries where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of project deliveries
	 * @param end the upper bound of the range of project deliveries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching project deliveries
	 */
	@Override
	public List<ProjectDelivery> findByUuid_C(String uuid, long companyId,
		int start, int end,
		OrderByComparator<ProjectDelivery> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] { uuid, companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] {
					uuid, companyId,
					
					start, end, orderByComparator
				};
		}

		List<ProjectDelivery> list = null;

		if (retrieveFromCache) {
			list = (List<ProjectDelivery>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ProjectDelivery projectDelivery : list) {
					if (!Objects.equals(uuid, projectDelivery.getUuid()) ||
							(companyId != projectDelivery.getCompanyId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_PROJECTDELIVERY_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProjectDeliveryModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				if (!pagination) {
					list = (List<ProjectDelivery>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ProjectDelivery>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first project delivery in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project delivery
	 * @throws NoSuchProjectDeliveryException if a matching project delivery could not be found
	 */
	@Override
	public ProjectDelivery findByUuid_C_First(String uuid, long companyId,
		OrderByComparator<ProjectDelivery> orderByComparator)
		throws NoSuchProjectDeliveryException {
		ProjectDelivery projectDelivery = fetchByUuid_C_First(uuid, companyId,
				orderByComparator);

		if (projectDelivery != null) {
			return projectDelivery;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectDeliveryException(msg.toString());
	}

	/**
	 * Returns the first project delivery in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project delivery, or <code>null</code> if a matching project delivery could not be found
	 */
	@Override
	public ProjectDelivery fetchByUuid_C_First(String uuid, long companyId,
		OrderByComparator<ProjectDelivery> orderByComparator) {
		List<ProjectDelivery> list = findByUuid_C(uuid, companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last project delivery in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project delivery
	 * @throws NoSuchProjectDeliveryException if a matching project delivery could not be found
	 */
	@Override
	public ProjectDelivery findByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<ProjectDelivery> orderByComparator)
		throws NoSuchProjectDeliveryException {
		ProjectDelivery projectDelivery = fetchByUuid_C_Last(uuid, companyId,
				orderByComparator);

		if (projectDelivery != null) {
			return projectDelivery;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectDeliveryException(msg.toString());
	}

	/**
	 * Returns the last project delivery in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project delivery, or <code>null</code> if a matching project delivery could not be found
	 */
	@Override
	public ProjectDelivery fetchByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<ProjectDelivery> orderByComparator) {
		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<ProjectDelivery> list = findByUuid_C(uuid, companyId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the project deliveries before and after the current project delivery in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param deliveryId the primary key of the current project delivery
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next project delivery
	 * @throws NoSuchProjectDeliveryException if a project delivery with the primary key could not be found
	 */
	@Override
	public ProjectDelivery[] findByUuid_C_PrevAndNext(long deliveryId,
		String uuid, long companyId,
		OrderByComparator<ProjectDelivery> orderByComparator)
		throws NoSuchProjectDeliveryException {
		ProjectDelivery projectDelivery = findByPrimaryKey(deliveryId);

		Session session = null;

		try {
			session = openSession();

			ProjectDelivery[] array = new ProjectDeliveryImpl[3];

			array[0] = getByUuid_C_PrevAndNext(session, projectDelivery, uuid,
					companyId, orderByComparator, true);

			array[1] = projectDelivery;

			array[2] = getByUuid_C_PrevAndNext(session, projectDelivery, uuid,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ProjectDelivery getByUuid_C_PrevAndNext(Session session,
		ProjectDelivery projectDelivery, String uuid, long companyId,
		OrderByComparator<ProjectDelivery> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_PROJECTDELIVERY_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProjectDeliveryModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(projectDelivery);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ProjectDelivery> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the project deliveries where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (ProjectDelivery projectDelivery : findByUuid_C(uuid, companyId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(projectDelivery);
		}
	}

	/**
	 * Returns the number of project deliveries where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching project deliveries
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_C;

		Object[] finderArgs = new Object[] { uuid, companyId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_PROJECTDELIVERY_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_1 = "projectDelivery.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_2 = "projectDelivery.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_3 = "(projectDelivery.uuid IS NULL OR projectDelivery.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 = "projectDelivery.companyId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_STEPID = new FinderPath(ProjectDeliveryModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDeliveryModelImpl.FINDER_CACHE_ENABLED,
			ProjectDeliveryImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByStepId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STEPID =
		new FinderPath(ProjectDeliveryModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDeliveryModelImpl.FINDER_CACHE_ENABLED,
			ProjectDeliveryImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByStepId",
			new String[] { Long.class.getName() },
			ProjectDeliveryModelImpl.STEPID_COLUMN_BITMASK |
			ProjectDeliveryModelImpl.NAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_STEPID = new FinderPath(ProjectDeliveryModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDeliveryModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByStepId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the project deliveries where stepId = &#63;.
	 *
	 * @param stepId the step ID
	 * @return the matching project deliveries
	 */
	@Override
	public List<ProjectDelivery> findByStepId(long stepId) {
		return findByStepId(stepId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the project deliveries where stepId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param stepId the step ID
	 * @param start the lower bound of the range of project deliveries
	 * @param end the upper bound of the range of project deliveries (not inclusive)
	 * @return the range of matching project deliveries
	 */
	@Override
	public List<ProjectDelivery> findByStepId(long stepId, int start, int end) {
		return findByStepId(stepId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the project deliveries where stepId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param stepId the step ID
	 * @param start the lower bound of the range of project deliveries
	 * @param end the upper bound of the range of project deliveries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching project deliveries
	 */
	@Override
	public List<ProjectDelivery> findByStepId(long stepId, int start, int end,
		OrderByComparator<ProjectDelivery> orderByComparator) {
		return findByStepId(stepId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the project deliveries where stepId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param stepId the step ID
	 * @param start the lower bound of the range of project deliveries
	 * @param end the upper bound of the range of project deliveries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching project deliveries
	 */
	@Override
	public List<ProjectDelivery> findByStepId(long stepId, int start, int end,
		OrderByComparator<ProjectDelivery> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STEPID;
			finderArgs = new Object[] { stepId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_STEPID;
			finderArgs = new Object[] { stepId, start, end, orderByComparator };
		}

		List<ProjectDelivery> list = null;

		if (retrieveFromCache) {
			list = (List<ProjectDelivery>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ProjectDelivery projectDelivery : list) {
					if ((stepId != projectDelivery.getStepId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PROJECTDELIVERY_WHERE);

			query.append(_FINDER_COLUMN_STEPID_STEPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProjectDeliveryModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(stepId);

				if (!pagination) {
					list = (List<ProjectDelivery>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ProjectDelivery>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first project delivery in the ordered set where stepId = &#63;.
	 *
	 * @param stepId the step ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project delivery
	 * @throws NoSuchProjectDeliveryException if a matching project delivery could not be found
	 */
	@Override
	public ProjectDelivery findByStepId_First(long stepId,
		OrderByComparator<ProjectDelivery> orderByComparator)
		throws NoSuchProjectDeliveryException {
		ProjectDelivery projectDelivery = fetchByStepId_First(stepId,
				orderByComparator);

		if (projectDelivery != null) {
			return projectDelivery;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("stepId=");
		msg.append(stepId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectDeliveryException(msg.toString());
	}

	/**
	 * Returns the first project delivery in the ordered set where stepId = &#63;.
	 *
	 * @param stepId the step ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project delivery, or <code>null</code> if a matching project delivery could not be found
	 */
	@Override
	public ProjectDelivery fetchByStepId_First(long stepId,
		OrderByComparator<ProjectDelivery> orderByComparator) {
		List<ProjectDelivery> list = findByStepId(stepId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last project delivery in the ordered set where stepId = &#63;.
	 *
	 * @param stepId the step ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project delivery
	 * @throws NoSuchProjectDeliveryException if a matching project delivery could not be found
	 */
	@Override
	public ProjectDelivery findByStepId_Last(long stepId,
		OrderByComparator<ProjectDelivery> orderByComparator)
		throws NoSuchProjectDeliveryException {
		ProjectDelivery projectDelivery = fetchByStepId_Last(stepId,
				orderByComparator);

		if (projectDelivery != null) {
			return projectDelivery;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("stepId=");
		msg.append(stepId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectDeliveryException(msg.toString());
	}

	/**
	 * Returns the last project delivery in the ordered set where stepId = &#63;.
	 *
	 * @param stepId the step ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project delivery, or <code>null</code> if a matching project delivery could not be found
	 */
	@Override
	public ProjectDelivery fetchByStepId_Last(long stepId,
		OrderByComparator<ProjectDelivery> orderByComparator) {
		int count = countByStepId(stepId);

		if (count == 0) {
			return null;
		}

		List<ProjectDelivery> list = findByStepId(stepId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the project deliveries before and after the current project delivery in the ordered set where stepId = &#63;.
	 *
	 * @param deliveryId the primary key of the current project delivery
	 * @param stepId the step ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next project delivery
	 * @throws NoSuchProjectDeliveryException if a project delivery with the primary key could not be found
	 */
	@Override
	public ProjectDelivery[] findByStepId_PrevAndNext(long deliveryId,
		long stepId, OrderByComparator<ProjectDelivery> orderByComparator)
		throws NoSuchProjectDeliveryException {
		ProjectDelivery projectDelivery = findByPrimaryKey(deliveryId);

		Session session = null;

		try {
			session = openSession();

			ProjectDelivery[] array = new ProjectDeliveryImpl[3];

			array[0] = getByStepId_PrevAndNext(session, projectDelivery,
					stepId, orderByComparator, true);

			array[1] = projectDelivery;

			array[2] = getByStepId_PrevAndNext(session, projectDelivery,
					stepId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ProjectDelivery getByStepId_PrevAndNext(Session session,
		ProjectDelivery projectDelivery, long stepId,
		OrderByComparator<ProjectDelivery> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PROJECTDELIVERY_WHERE);

		query.append(_FINDER_COLUMN_STEPID_STEPID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProjectDeliveryModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(stepId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(projectDelivery);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ProjectDelivery> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the project deliveries where stepId = &#63; from the database.
	 *
	 * @param stepId the step ID
	 */
	@Override
	public void removeByStepId(long stepId) {
		for (ProjectDelivery projectDelivery : findByStepId(stepId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(projectDelivery);
		}
	}

	/**
	 * Returns the number of project deliveries where stepId = &#63;.
	 *
	 * @param stepId the step ID
	 * @return the number of matching project deliveries
	 */
	@Override
	public int countByStepId(long stepId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_STEPID;

		Object[] finderArgs = new Object[] { stepId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PROJECTDELIVERY_WHERE);

			query.append(_FINDER_COLUMN_STEPID_STEPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(stepId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_STEPID_STEPID_2 = "projectDelivery.stepId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYID =
		new FinderPath(ProjectDeliveryModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDeliveryModelImpl.FINDER_CACHE_ENABLED,
			ProjectDeliveryImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByCompanyId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID =
		new FinderPath(ProjectDeliveryModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDeliveryModelImpl.FINDER_CACHE_ENABLED,
			ProjectDeliveryImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCompanyId",
			new String[] { Long.class.getName() },
			ProjectDeliveryModelImpl.COMPANYID_COLUMN_BITMASK |
			ProjectDeliveryModelImpl.NAME_COLUMN_BITMASK |
			ProjectDeliveryModelImpl.STEPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANYID = new FinderPath(ProjectDeliveryModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDeliveryModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCompanyId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the project deliveries where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the matching project deliveries
	 */
	@Override
	public List<ProjectDelivery> findByCompanyId(long companyId) {
		return findByCompanyId(companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the project deliveries where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of project deliveries
	 * @param end the upper bound of the range of project deliveries (not inclusive)
	 * @return the range of matching project deliveries
	 */
	@Override
	public List<ProjectDelivery> findByCompanyId(long companyId, int start,
		int end) {
		return findByCompanyId(companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the project deliveries where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of project deliveries
	 * @param end the upper bound of the range of project deliveries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching project deliveries
	 */
	@Override
	public List<ProjectDelivery> findByCompanyId(long companyId, int start,
		int end, OrderByComparator<ProjectDelivery> orderByComparator) {
		return findByCompanyId(companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the project deliveries where companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param start the lower bound of the range of project deliveries
	 * @param end the upper bound of the range of project deliveries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching project deliveries
	 */
	@Override
	public List<ProjectDelivery> findByCompanyId(long companyId, int start,
		int end, OrderByComparator<ProjectDelivery> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID;
			finderArgs = new Object[] { companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYID;
			finderArgs = new Object[] { companyId, start, end, orderByComparator };
		}

		List<ProjectDelivery> list = null;

		if (retrieveFromCache) {
			list = (List<ProjectDelivery>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ProjectDelivery projectDelivery : list) {
					if ((companyId != projectDelivery.getCompanyId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PROJECTDELIVERY_WHERE);

			query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProjectDeliveryModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (!pagination) {
					list = (List<ProjectDelivery>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ProjectDelivery>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first project delivery in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project delivery
	 * @throws NoSuchProjectDeliveryException if a matching project delivery could not be found
	 */
	@Override
	public ProjectDelivery findByCompanyId_First(long companyId,
		OrderByComparator<ProjectDelivery> orderByComparator)
		throws NoSuchProjectDeliveryException {
		ProjectDelivery projectDelivery = fetchByCompanyId_First(companyId,
				orderByComparator);

		if (projectDelivery != null) {
			return projectDelivery;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectDeliveryException(msg.toString());
	}

	/**
	 * Returns the first project delivery in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project delivery, or <code>null</code> if a matching project delivery could not be found
	 */
	@Override
	public ProjectDelivery fetchByCompanyId_First(long companyId,
		OrderByComparator<ProjectDelivery> orderByComparator) {
		List<ProjectDelivery> list = findByCompanyId(companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last project delivery in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project delivery
	 * @throws NoSuchProjectDeliveryException if a matching project delivery could not be found
	 */
	@Override
	public ProjectDelivery findByCompanyId_Last(long companyId,
		OrderByComparator<ProjectDelivery> orderByComparator)
		throws NoSuchProjectDeliveryException {
		ProjectDelivery projectDelivery = fetchByCompanyId_Last(companyId,
				orderByComparator);

		if (projectDelivery != null) {
			return projectDelivery;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectDeliveryException(msg.toString());
	}

	/**
	 * Returns the last project delivery in the ordered set where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project delivery, or <code>null</code> if a matching project delivery could not be found
	 */
	@Override
	public ProjectDelivery fetchByCompanyId_Last(long companyId,
		OrderByComparator<ProjectDelivery> orderByComparator) {
		int count = countByCompanyId(companyId);

		if (count == 0) {
			return null;
		}

		List<ProjectDelivery> list = findByCompanyId(companyId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the project deliveries before and after the current project delivery in the ordered set where companyId = &#63;.
	 *
	 * @param deliveryId the primary key of the current project delivery
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next project delivery
	 * @throws NoSuchProjectDeliveryException if a project delivery with the primary key could not be found
	 */
	@Override
	public ProjectDelivery[] findByCompanyId_PrevAndNext(long deliveryId,
		long companyId, OrderByComparator<ProjectDelivery> orderByComparator)
		throws NoSuchProjectDeliveryException {
		ProjectDelivery projectDelivery = findByPrimaryKey(deliveryId);

		Session session = null;

		try {
			session = openSession();

			ProjectDelivery[] array = new ProjectDeliveryImpl[3];

			array[0] = getByCompanyId_PrevAndNext(session, projectDelivery,
					companyId, orderByComparator, true);

			array[1] = projectDelivery;

			array[2] = getByCompanyId_PrevAndNext(session, projectDelivery,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ProjectDelivery getByCompanyId_PrevAndNext(Session session,
		ProjectDelivery projectDelivery, long companyId,
		OrderByComparator<ProjectDelivery> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PROJECTDELIVERY_WHERE);

		query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProjectDeliveryModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(projectDelivery);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ProjectDelivery> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the project deliveries where companyId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 */
	@Override
	public void removeByCompanyId(long companyId) {
		for (ProjectDelivery projectDelivery : findByCompanyId(companyId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(projectDelivery);
		}
	}

	/**
	 * Returns the number of project deliveries where companyId = &#63;.
	 *
	 * @param companyId the company ID
	 * @return the number of matching project deliveries
	 */
	@Override
	public int countByCompanyId(long companyId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_COMPANYID;

		Object[] finderArgs = new Object[] { companyId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PROJECTDELIVERY_WHERE);

			query.append(_FINDER_COLUMN_COMPANYID_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COMPANYID_COMPANYID_2 = "projectDelivery.companyId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER = new FinderPath(ProjectDeliveryModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDeliveryModelImpl.FINDER_CACHE_ENABLED,
			ProjectDeliveryImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByCompanyIdIdentifier",
			new String[] { Long.class.getName(), Long.class.getName() },
			ProjectDeliveryModelImpl.COMPANYID_COLUMN_BITMASK |
			ProjectDeliveryModelImpl.STEPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COMPANYIDIDENTIFIER = new FinderPath(ProjectDeliveryModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDeliveryModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByCompanyIdIdentifier",
			new String[] { Long.class.getName(), Long.class.getName() });

	/**
	 * Returns the project delivery where companyId = &#63; and stepId = &#63; or throws a {@link NoSuchProjectDeliveryException} if it could not be found.
	 *
	 * @param companyId the company ID
	 * @param stepId the step ID
	 * @return the matching project delivery
	 * @throws NoSuchProjectDeliveryException if a matching project delivery could not be found
	 */
	@Override
	public ProjectDelivery findByCompanyIdIdentifier(long companyId, long stepId)
		throws NoSuchProjectDeliveryException {
		ProjectDelivery projectDelivery = fetchByCompanyIdIdentifier(companyId,
				stepId);

		if (projectDelivery == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("companyId=");
			msg.append(companyId);

			msg.append(", stepId=");
			msg.append(stepId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchProjectDeliveryException(msg.toString());
		}

		return projectDelivery;
	}

	/**
	 * Returns the project delivery where companyId = &#63; and stepId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param companyId the company ID
	 * @param stepId the step ID
	 * @return the matching project delivery, or <code>null</code> if a matching project delivery could not be found
	 */
	@Override
	public ProjectDelivery fetchByCompanyIdIdentifier(long companyId,
		long stepId) {
		return fetchByCompanyIdIdentifier(companyId, stepId, true);
	}

	/**
	 * Returns the project delivery where companyId = &#63; and stepId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param companyId the company ID
	 * @param stepId the step ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching project delivery, or <code>null</code> if a matching project delivery could not be found
	 */
	@Override
	public ProjectDelivery fetchByCompanyIdIdentifier(long companyId,
		long stepId, boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { companyId, stepId };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER,
					finderArgs, this);
		}

		if (result instanceof ProjectDelivery) {
			ProjectDelivery projectDelivery = (ProjectDelivery)result;

			if ((companyId != projectDelivery.getCompanyId()) ||
					(stepId != projectDelivery.getStepId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_PROJECTDELIVERY_WHERE);

			query.append(_FINDER_COLUMN_COMPANYIDIDENTIFIER_COMPANYID_2);

			query.append(_FINDER_COLUMN_COMPANYIDIDENTIFIER_STEPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(stepId);

				List<ProjectDelivery> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER,
						finderArgs, list);
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							_log.warn(
								"ProjectDeliveryPersistenceImpl.fetchByCompanyIdIdentifier(long, long, boolean) with parameters (" +
								StringUtil.merge(finderArgs) +
								") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					ProjectDelivery projectDelivery = list.get(0);

					result = projectDelivery;

					cacheResult(projectDelivery);

					if ((projectDelivery.getCompanyId() != companyId) ||
							(projectDelivery.getStepId() != stepId)) {
						finderCache.putResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER,
							finderArgs, projectDelivery);
					}
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (ProjectDelivery)result;
		}
	}

	/**
	 * Removes the project delivery where companyId = &#63; and stepId = &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param stepId the step ID
	 * @return the project delivery that was removed
	 */
	@Override
	public ProjectDelivery removeByCompanyIdIdentifier(long companyId,
		long stepId) throws NoSuchProjectDeliveryException {
		ProjectDelivery projectDelivery = findByCompanyIdIdentifier(companyId,
				stepId);

		return remove(projectDelivery);
	}

	/**
	 * Returns the number of project deliveries where companyId = &#63; and stepId = &#63;.
	 *
	 * @param companyId the company ID
	 * @param stepId the step ID
	 * @return the number of matching project deliveries
	 */
	@Override
	public int countByCompanyIdIdentifier(long companyId, long stepId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_COMPANYIDIDENTIFIER;

		Object[] finderArgs = new Object[] { companyId, stepId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_PROJECTDELIVERY_WHERE);

			query.append(_FINDER_COLUMN_COMPANYIDIDENTIFIER_COMPANYID_2);

			query.append(_FINDER_COLUMN_COMPANYIDIDENTIFIER_STEPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				qPos.add(stepId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COMPANYIDIDENTIFIER_COMPANYID_2 = "projectDelivery.companyId = ? AND ";
	private static final String _FINDER_COLUMN_COMPANYIDIDENTIFIER_STEPID_2 = "projectDelivery.stepId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYIDINCLUDENAME =
		new FinderPath(ProjectDeliveryModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDeliveryModelImpl.FINDER_CACHE_ENABLED,
			ProjectDeliveryImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByCompanyIdIncludeName",
			new String[] {
				Long.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_COMPANYIDINCLUDENAME =
		new FinderPath(ProjectDeliveryModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDeliveryModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"countByCompanyIdIncludeName",
			new String[] { Long.class.getName(), String.class.getName() });

	/**
	 * Returns all the project deliveries where companyId = &#63; and name LIKE &#63;.
	 *
	 * @param companyId the company ID
	 * @param name the name
	 * @return the matching project deliveries
	 */
	@Override
	public List<ProjectDelivery> findByCompanyIdIncludeName(long companyId,
		String name) {
		return findByCompanyIdIncludeName(companyId, name, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the project deliveries where companyId = &#63; and name LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param name the name
	 * @param start the lower bound of the range of project deliveries
	 * @param end the upper bound of the range of project deliveries (not inclusive)
	 * @return the range of matching project deliveries
	 */
	@Override
	public List<ProjectDelivery> findByCompanyIdIncludeName(long companyId,
		String name, int start, int end) {
		return findByCompanyIdIncludeName(companyId, name, start, end, null);
	}

	/**
	 * Returns an ordered range of all the project deliveries where companyId = &#63; and name LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param name the name
	 * @param start the lower bound of the range of project deliveries
	 * @param end the upper bound of the range of project deliveries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching project deliveries
	 */
	@Override
	public List<ProjectDelivery> findByCompanyIdIncludeName(long companyId,
		String name, int start, int end,
		OrderByComparator<ProjectDelivery> orderByComparator) {
		return findByCompanyIdIncludeName(companyId, name, start, end,
			orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the project deliveries where companyId = &#63; and name LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param companyId the company ID
	 * @param name the name
	 * @param start the lower bound of the range of project deliveries
	 * @param end the upper bound of the range of project deliveries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching project deliveries
	 */
	@Override
	public List<ProjectDelivery> findByCompanyIdIncludeName(long companyId,
		String name, int start, int end,
		OrderByComparator<ProjectDelivery> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_COMPANYIDINCLUDENAME;
		finderArgs = new Object[] { companyId, name, start, end, orderByComparator };

		List<ProjectDelivery> list = null;

		if (retrieveFromCache) {
			list = (List<ProjectDelivery>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ProjectDelivery projectDelivery : list) {
					if ((companyId != projectDelivery.getCompanyId()) ||
							!StringUtil.wildcardMatches(
								projectDelivery.getName(), name,
								CharPool.UNDERLINE, CharPool.PERCENT,
								CharPool.BACK_SLASH, true)) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_PROJECTDELIVERY_WHERE);

			query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_COMPANYID_2);

			boolean bindName = false;

			if (name == null) {
				query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_1);
			}
			else if (name.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_3);
			}
			else {
				bindName = true;

				query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(ProjectDeliveryModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (bindName) {
					qPos.add(name);
				}

				if (!pagination) {
					list = (List<ProjectDelivery>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ProjectDelivery>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first project delivery in the ordered set where companyId = &#63; and name LIKE &#63;.
	 *
	 * @param companyId the company ID
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project delivery
	 * @throws NoSuchProjectDeliveryException if a matching project delivery could not be found
	 */
	@Override
	public ProjectDelivery findByCompanyIdIncludeName_First(long companyId,
		String name, OrderByComparator<ProjectDelivery> orderByComparator)
		throws NoSuchProjectDeliveryException {
		ProjectDelivery projectDelivery = fetchByCompanyIdIncludeName_First(companyId,
				name, orderByComparator);

		if (projectDelivery != null) {
			return projectDelivery;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", name=");
		msg.append(name);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectDeliveryException(msg.toString());
	}

	/**
	 * Returns the first project delivery in the ordered set where companyId = &#63; and name LIKE &#63;.
	 *
	 * @param companyId the company ID
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching project delivery, or <code>null</code> if a matching project delivery could not be found
	 */
	@Override
	public ProjectDelivery fetchByCompanyIdIncludeName_First(long companyId,
		String name, OrderByComparator<ProjectDelivery> orderByComparator) {
		List<ProjectDelivery> list = findByCompanyIdIncludeName(companyId,
				name, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last project delivery in the ordered set where companyId = &#63; and name LIKE &#63;.
	 *
	 * @param companyId the company ID
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project delivery
	 * @throws NoSuchProjectDeliveryException if a matching project delivery could not be found
	 */
	@Override
	public ProjectDelivery findByCompanyIdIncludeName_Last(long companyId,
		String name, OrderByComparator<ProjectDelivery> orderByComparator)
		throws NoSuchProjectDeliveryException {
		ProjectDelivery projectDelivery = fetchByCompanyIdIncludeName_Last(companyId,
				name, orderByComparator);

		if (projectDelivery != null) {
			return projectDelivery;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("companyId=");
		msg.append(companyId);

		msg.append(", name=");
		msg.append(name);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchProjectDeliveryException(msg.toString());
	}

	/**
	 * Returns the last project delivery in the ordered set where companyId = &#63; and name LIKE &#63;.
	 *
	 * @param companyId the company ID
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching project delivery, or <code>null</code> if a matching project delivery could not be found
	 */
	@Override
	public ProjectDelivery fetchByCompanyIdIncludeName_Last(long companyId,
		String name, OrderByComparator<ProjectDelivery> orderByComparator) {
		int count = countByCompanyIdIncludeName(companyId, name);

		if (count == 0) {
			return null;
		}

		List<ProjectDelivery> list = findByCompanyIdIncludeName(companyId,
				name, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the project deliveries before and after the current project delivery in the ordered set where companyId = &#63; and name LIKE &#63;.
	 *
	 * @param deliveryId the primary key of the current project delivery
	 * @param companyId the company ID
	 * @param name the name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next project delivery
	 * @throws NoSuchProjectDeliveryException if a project delivery with the primary key could not be found
	 */
	@Override
	public ProjectDelivery[] findByCompanyIdIncludeName_PrevAndNext(
		long deliveryId, long companyId, String name,
		OrderByComparator<ProjectDelivery> orderByComparator)
		throws NoSuchProjectDeliveryException {
		ProjectDelivery projectDelivery = findByPrimaryKey(deliveryId);

		Session session = null;

		try {
			session = openSession();

			ProjectDelivery[] array = new ProjectDeliveryImpl[3];

			array[0] = getByCompanyIdIncludeName_PrevAndNext(session,
					projectDelivery, companyId, name, orderByComparator, true);

			array[1] = projectDelivery;

			array[2] = getByCompanyIdIncludeName_PrevAndNext(session,
					projectDelivery, companyId, name, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ProjectDelivery getByCompanyIdIncludeName_PrevAndNext(
		Session session, ProjectDelivery projectDelivery, long companyId,
		String name, OrderByComparator<ProjectDelivery> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_PROJECTDELIVERY_WHERE);

		query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_COMPANYID_2);

		boolean bindName = false;

		if (name == null) {
			query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_1);
		}
		else if (name.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_3);
		}
		else {
			bindName = true;

			query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ProjectDeliveryModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(companyId);

		if (bindName) {
			qPos.add(name);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(projectDelivery);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<ProjectDelivery> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the project deliveries where companyId = &#63; and name LIKE &#63; from the database.
	 *
	 * @param companyId the company ID
	 * @param name the name
	 */
	@Override
	public void removeByCompanyIdIncludeName(long companyId, String name) {
		for (ProjectDelivery projectDelivery : findByCompanyIdIncludeName(
				companyId, name, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(projectDelivery);
		}
	}

	/**
	 * Returns the number of project deliveries where companyId = &#63; and name LIKE &#63;.
	 *
	 * @param companyId the company ID
	 * @param name the name
	 * @return the number of matching project deliveries
	 */
	@Override
	public int countByCompanyIdIncludeName(long companyId, String name) {
		FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_COMPANYIDINCLUDENAME;

		Object[] finderArgs = new Object[] { companyId, name };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_PROJECTDELIVERY_WHERE);

			query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_COMPANYID_2);

			boolean bindName = false;

			if (name == null) {
				query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_1);
			}
			else if (name.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_3);
			}
			else {
				bindName = true;

				query.append(_FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(companyId);

				if (bindName) {
					qPos.add(name);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COMPANYIDINCLUDENAME_COMPANYID_2 = "projectDelivery.companyId = ? AND ";
	private static final String _FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_1 = "projectDelivery.name IS NULL";
	private static final String _FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_2 = "projectDelivery.name LIKE ?";
	private static final String _FINDER_COLUMN_COMPANYIDINCLUDENAME_NAME_3 = "(projectDelivery.name IS NULL OR projectDelivery.name LIKE '')";

	public ProjectDeliveryPersistenceImpl() {
		setModelClass(ProjectDelivery.class);
	}

	/**
	 * Caches the project delivery in the entity cache if it is enabled.
	 *
	 * @param projectDelivery the project delivery
	 */
	@Override
	public void cacheResult(ProjectDelivery projectDelivery) {
		entityCache.putResult(ProjectDeliveryModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDeliveryImpl.class, projectDelivery.getPrimaryKey(),
			projectDelivery);

		finderCache.putResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER,
			new Object[] {
				projectDelivery.getCompanyId(), projectDelivery.getStepId()
			}, projectDelivery);

		projectDelivery.resetOriginalValues();
	}

	/**
	 * Caches the project deliveries in the entity cache if it is enabled.
	 *
	 * @param projectDeliveries the project deliveries
	 */
	@Override
	public void cacheResult(List<ProjectDelivery> projectDeliveries) {
		for (ProjectDelivery projectDelivery : projectDeliveries) {
			if (entityCache.getResult(
						ProjectDeliveryModelImpl.ENTITY_CACHE_ENABLED,
						ProjectDeliveryImpl.class,
						projectDelivery.getPrimaryKey()) == null) {
				cacheResult(projectDelivery);
			}
			else {
				projectDelivery.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all project deliveries.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(ProjectDeliveryImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the project delivery.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ProjectDelivery projectDelivery) {
		entityCache.removeResult(ProjectDeliveryModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDeliveryImpl.class, projectDelivery.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache((ProjectDeliveryModelImpl)projectDelivery);
	}

	@Override
	public void clearCache(List<ProjectDelivery> projectDeliveries) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ProjectDelivery projectDelivery : projectDeliveries) {
			entityCache.removeResult(ProjectDeliveryModelImpl.ENTITY_CACHE_ENABLED,
				ProjectDeliveryImpl.class, projectDelivery.getPrimaryKey());

			clearUniqueFindersCache((ProjectDeliveryModelImpl)projectDelivery);
		}
	}

	protected void cacheUniqueFindersCache(
		ProjectDeliveryModelImpl projectDeliveryModelImpl, boolean isNew) {
		if (isNew) {
			Object[] args = new Object[] {
					projectDeliveryModelImpl.getCompanyId(),
					projectDeliveryModelImpl.getStepId()
				};

			finderCache.putResult(FINDER_PATH_COUNT_BY_COMPANYIDIDENTIFIER,
				args, Long.valueOf(1));
			finderCache.putResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER,
				args, projectDeliveryModelImpl);
		}
		else {
			if ((projectDeliveryModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						projectDeliveryModelImpl.getCompanyId(),
						projectDeliveryModelImpl.getStepId()
					};

				finderCache.putResult(FINDER_PATH_COUNT_BY_COMPANYIDIDENTIFIER,
					args, Long.valueOf(1));
				finderCache.putResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER,
					args, projectDeliveryModelImpl);
			}
		}
	}

	protected void clearUniqueFindersCache(
		ProjectDeliveryModelImpl projectDeliveryModelImpl) {
		Object[] args = new Object[] {
				projectDeliveryModelImpl.getCompanyId(),
				projectDeliveryModelImpl.getStepId()
			};

		finderCache.removeResult(FINDER_PATH_COUNT_BY_COMPANYIDIDENTIFIER, args);
		finderCache.removeResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER, args);

		if ((projectDeliveryModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER.getColumnBitmask()) != 0) {
			args = new Object[] {
					projectDeliveryModelImpl.getOriginalCompanyId(),
					projectDeliveryModelImpl.getOriginalStepId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_COMPANYIDIDENTIFIER,
				args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_COMPANYIDIDENTIFIER,
				args);
		}
	}

	/**
	 * Creates a new project delivery with the primary key. Does not add the project delivery to the database.
	 *
	 * @param deliveryId the primary key for the new project delivery
	 * @return the new project delivery
	 */
	@Override
	public ProjectDelivery create(long deliveryId) {
		ProjectDelivery projectDelivery = new ProjectDeliveryImpl();

		projectDelivery.setNew(true);
		projectDelivery.setPrimaryKey(deliveryId);

		String uuid = PortalUUIDUtil.generate();

		projectDelivery.setUuid(uuid);

		projectDelivery.setCompanyId(companyProvider.getCompanyId());

		return projectDelivery;
	}

	/**
	 * Removes the project delivery with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param deliveryId the primary key of the project delivery
	 * @return the project delivery that was removed
	 * @throws NoSuchProjectDeliveryException if a project delivery with the primary key could not be found
	 */
	@Override
	public ProjectDelivery remove(long deliveryId)
		throws NoSuchProjectDeliveryException {
		return remove((Serializable)deliveryId);
	}

	/**
	 * Removes the project delivery with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the project delivery
	 * @return the project delivery that was removed
	 * @throws NoSuchProjectDeliveryException if a project delivery with the primary key could not be found
	 */
	@Override
	public ProjectDelivery remove(Serializable primaryKey)
		throws NoSuchProjectDeliveryException {
		Session session = null;

		try {
			session = openSession();

			ProjectDelivery projectDelivery = (ProjectDelivery)session.get(ProjectDeliveryImpl.class,
					primaryKey);

			if (projectDelivery == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchProjectDeliveryException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(projectDelivery);
		}
		catch (NoSuchProjectDeliveryException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ProjectDelivery removeImpl(ProjectDelivery projectDelivery) {
		projectDelivery = toUnwrappedModel(projectDelivery);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(projectDelivery)) {
				projectDelivery = (ProjectDelivery)session.get(ProjectDeliveryImpl.class,
						projectDelivery.getPrimaryKeyObj());
			}

			if (projectDelivery != null) {
				session.delete(projectDelivery);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (projectDelivery != null) {
			clearCache(projectDelivery);
		}

		return projectDelivery;
	}

	@Override
	public ProjectDelivery updateImpl(ProjectDelivery projectDelivery) {
		projectDelivery = toUnwrappedModel(projectDelivery);

		boolean isNew = projectDelivery.isNew();

		ProjectDeliveryModelImpl projectDeliveryModelImpl = (ProjectDeliveryModelImpl)projectDelivery;

		if (Validator.isNull(projectDelivery.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			projectDelivery.setUuid(uuid);
		}

		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();

		Date now = new Date();

		if (isNew && (projectDelivery.getCreateDate() == null)) {
			if (serviceContext == null) {
				projectDelivery.setCreateDate(now);
			}
			else {
				projectDelivery.setCreateDate(serviceContext.getCreateDate(now));
			}
		}

		if (!projectDeliveryModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				projectDelivery.setModifiedDate(now);
			}
			else {
				projectDelivery.setModifiedDate(serviceContext.getModifiedDate(
						now));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (projectDelivery.isNew()) {
				session.save(projectDelivery);

				projectDelivery.setNew(false);
			}
			else {
				projectDelivery = (ProjectDelivery)session.merge(projectDelivery);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !ProjectDeliveryModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((projectDeliveryModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						projectDeliveryModelImpl.getOriginalUuid()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);

				args = new Object[] { projectDeliveryModelImpl.getUuid() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);
			}

			if ((projectDeliveryModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						projectDeliveryModelImpl.getOriginalUuid(),
						projectDeliveryModelImpl.getOriginalCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);

				args = new Object[] {
						projectDeliveryModelImpl.getUuid(),
						projectDeliveryModelImpl.getCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);
			}

			if ((projectDeliveryModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STEPID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						projectDeliveryModelImpl.getOriginalStepId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_STEPID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STEPID,
					args);

				args = new Object[] { projectDeliveryModelImpl.getStepId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_STEPID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_STEPID,
					args);
			}

			if ((projectDeliveryModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						projectDeliveryModelImpl.getOriginalCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_COMPANYID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID,
					args);

				args = new Object[] { projectDeliveryModelImpl.getCompanyId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_COMPANYID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_COMPANYID,
					args);
			}
		}

		entityCache.putResult(ProjectDeliveryModelImpl.ENTITY_CACHE_ENABLED,
			ProjectDeliveryImpl.class, projectDelivery.getPrimaryKey(),
			projectDelivery, false);

		clearUniqueFindersCache(projectDeliveryModelImpl);
		cacheUniqueFindersCache(projectDeliveryModelImpl, isNew);

		projectDelivery.resetOriginalValues();

		return projectDelivery;
	}

	protected ProjectDelivery toUnwrappedModel(ProjectDelivery projectDelivery) {
		if (projectDelivery instanceof ProjectDeliveryImpl) {
			return projectDelivery;
		}

		ProjectDeliveryImpl projectDeliveryImpl = new ProjectDeliveryImpl();

		projectDeliveryImpl.setNew(projectDelivery.isNew());
		projectDeliveryImpl.setPrimaryKey(projectDelivery.getPrimaryKey());

		projectDeliveryImpl.setUuid(projectDelivery.getUuid());
		projectDeliveryImpl.setDeliveryId(projectDelivery.getDeliveryId());
		projectDeliveryImpl.setStepId(projectDelivery.getStepId());
		projectDeliveryImpl.setCompanyId(projectDelivery.getCompanyId());
		projectDeliveryImpl.setUserId(projectDelivery.getUserId());
		projectDeliveryImpl.setUserName(projectDelivery.getUserName());
		projectDeliveryImpl.setCreateDate(projectDelivery.getCreateDate());
		projectDeliveryImpl.setModifiedDate(projectDelivery.getModifiedDate());
		projectDeliveryImpl.setName(projectDelivery.getName());
		projectDeliveryImpl.setType(projectDelivery.getType());
		projectDeliveryImpl.setPhase(projectDelivery.getPhase());
		projectDeliveryImpl.setDomain(projectDelivery.getDomain());
		projectDeliveryImpl.setStatus(projectDelivery.getStatus());
		projectDeliveryImpl.setStartDate(projectDelivery.getStartDate());
		projectDeliveryImpl.setEndDate(projectDelivery.getEndDate());
		projectDeliveryImpl.setCost(projectDelivery.getCost());
		projectDeliveryImpl.setOutcomeCategory(projectDelivery.getOutcomeCategory());
		projectDeliveryImpl.setComment(projectDelivery.getComment());
		projectDeliveryImpl.setIsAvailable(projectDelivery.isIsAvailable());
		projectDeliveryImpl.setReviewerIsCompleted(projectDelivery.isReviewerIsCompleted());
		projectDeliveryImpl.setReviewerRate(projectDelivery.getReviewerRate());
		projectDeliveryImpl.setReviewerComment(projectDelivery.getReviewerComment());
		projectDeliveryImpl.setFundingSource(projectDelivery.getFundingSource());
		projectDeliveryImpl.setIsEditPlanning(projectDelivery.isIsEditPlanning());
		projectDeliveryImpl.setBudget(projectDelivery.getBudget());
		projectDeliveryImpl.setPersonResponsible(projectDelivery.getPersonResponsible());
		projectDeliveryImpl.setStartPlanningDate(projectDelivery.getStartPlanningDate());
		projectDeliveryImpl.setEndPlanningDate(projectDelivery.getEndPlanningDate());

		return projectDeliveryImpl;
	}

	/**
	 * Returns the project delivery with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the project delivery
	 * @return the project delivery
	 * @throws NoSuchProjectDeliveryException if a project delivery with the primary key could not be found
	 */
	@Override
	public ProjectDelivery findByPrimaryKey(Serializable primaryKey)
		throws NoSuchProjectDeliveryException {
		ProjectDelivery projectDelivery = fetchByPrimaryKey(primaryKey);

		if (projectDelivery == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchProjectDeliveryException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return projectDelivery;
	}

	/**
	 * Returns the project delivery with the primary key or throws a {@link NoSuchProjectDeliveryException} if it could not be found.
	 *
	 * @param deliveryId the primary key of the project delivery
	 * @return the project delivery
	 * @throws NoSuchProjectDeliveryException if a project delivery with the primary key could not be found
	 */
	@Override
	public ProjectDelivery findByPrimaryKey(long deliveryId)
		throws NoSuchProjectDeliveryException {
		return findByPrimaryKey((Serializable)deliveryId);
	}

	/**
	 * Returns the project delivery with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the project delivery
	 * @return the project delivery, or <code>null</code> if a project delivery with the primary key could not be found
	 */
	@Override
	public ProjectDelivery fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(ProjectDeliveryModelImpl.ENTITY_CACHE_ENABLED,
				ProjectDeliveryImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		ProjectDelivery projectDelivery = (ProjectDelivery)serializable;

		if (projectDelivery == null) {
			Session session = null;

			try {
				session = openSession();

				projectDelivery = (ProjectDelivery)session.get(ProjectDeliveryImpl.class,
						primaryKey);

				if (projectDelivery != null) {
					cacheResult(projectDelivery);
				}
				else {
					entityCache.putResult(ProjectDeliveryModelImpl.ENTITY_CACHE_ENABLED,
						ProjectDeliveryImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(ProjectDeliveryModelImpl.ENTITY_CACHE_ENABLED,
					ProjectDeliveryImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return projectDelivery;
	}

	/**
	 * Returns the project delivery with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param deliveryId the primary key of the project delivery
	 * @return the project delivery, or <code>null</code> if a project delivery with the primary key could not be found
	 */
	@Override
	public ProjectDelivery fetchByPrimaryKey(long deliveryId) {
		return fetchByPrimaryKey((Serializable)deliveryId);
	}

	@Override
	public Map<Serializable, ProjectDelivery> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, ProjectDelivery> map = new HashMap<Serializable, ProjectDelivery>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			ProjectDelivery projectDelivery = fetchByPrimaryKey(primaryKey);

			if (projectDelivery != null) {
				map.put(primaryKey, projectDelivery);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(ProjectDeliveryModelImpl.ENTITY_CACHE_ENABLED,
					ProjectDeliveryImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (ProjectDelivery)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_PROJECTDELIVERY_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(String.valueOf(primaryKey));

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (ProjectDelivery projectDelivery : (List<ProjectDelivery>)q.list()) {
				map.put(projectDelivery.getPrimaryKeyObj(), projectDelivery);

				cacheResult(projectDelivery);

				uncachedPrimaryKeys.remove(projectDelivery.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(ProjectDeliveryModelImpl.ENTITY_CACHE_ENABLED,
					ProjectDeliveryImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the project deliveries.
	 *
	 * @return the project deliveries
	 */
	@Override
	public List<ProjectDelivery> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the project deliveries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of project deliveries
	 * @param end the upper bound of the range of project deliveries (not inclusive)
	 * @return the range of project deliveries
	 */
	@Override
	public List<ProjectDelivery> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the project deliveries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of project deliveries
	 * @param end the upper bound of the range of project deliveries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of project deliveries
	 */
	@Override
	public List<ProjectDelivery> findAll(int start, int end,
		OrderByComparator<ProjectDelivery> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the project deliveries.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of project deliveries
	 * @param end the upper bound of the range of project deliveries (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of project deliveries
	 */
	@Override
	public List<ProjectDelivery> findAll(int start, int end,
		OrderByComparator<ProjectDelivery> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ProjectDelivery> list = null;

		if (retrieveFromCache) {
			list = (List<ProjectDelivery>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_PROJECTDELIVERY);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PROJECTDELIVERY;

				if (pagination) {
					sql = sql.concat(ProjectDeliveryModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ProjectDelivery>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ProjectDelivery>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the project deliveries from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (ProjectDelivery projectDelivery : findAll()) {
			remove(projectDelivery);
		}
	}

	/**
	 * Returns the number of project deliveries.
	 *
	 * @return the number of project deliveries
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PROJECTDELIVERY);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return ProjectDeliveryModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the project delivery persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(ProjectDeliveryImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = CompanyProviderWrapper.class)
	protected CompanyProvider companyProvider;
	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_PROJECTDELIVERY = "SELECT projectDelivery FROM ProjectDelivery projectDelivery";
	private static final String _SQL_SELECT_PROJECTDELIVERY_WHERE_PKS_IN = "SELECT projectDelivery FROM ProjectDelivery projectDelivery WHERE deliveryId IN (";
	private static final String _SQL_SELECT_PROJECTDELIVERY_WHERE = "SELECT projectDelivery FROM ProjectDelivery projectDelivery WHERE ";
	private static final String _SQL_COUNT_PROJECTDELIVERY = "SELECT COUNT(projectDelivery) FROM ProjectDelivery projectDelivery";
	private static final String _SQL_COUNT_PROJECTDELIVERY_WHERE = "SELECT COUNT(projectDelivery) FROM ProjectDelivery projectDelivery WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "projectDelivery.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ProjectDelivery exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No ProjectDelivery exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(ProjectDeliveryPersistenceImpl.class);
	private static final Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"uuid", "type", "comment"
			});
}