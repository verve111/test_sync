/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ProjectPhaseLocalService}.
 *
 * @author Claudio Carlenzoli
 * @see ProjectPhaseLocalService
 * @generated
 */
@ProviderType
public class ProjectPhaseLocalServiceWrapper implements ProjectPhaseLocalService,
	ServiceWrapper<ProjectPhaseLocalService> {
	public ProjectPhaseLocalServiceWrapper(
		ProjectPhaseLocalService projectPhaseLocalService) {
		_projectPhaseLocalService = projectPhaseLocalService;
	}

	/**
	* Adds the project phase to the database. Also notifies the appropriate model listeners.
	*
	* @param projectPhase the project phase
	* @return the project phase that was added
	*/
	@Override
	public com.collaboratelab.project.model.ProjectPhase addProjectPhase(
		com.collaboratelab.project.model.ProjectPhase projectPhase) {
		return _projectPhaseLocalService.addProjectPhase(projectPhase);
	}

	/**
	* Creates a new project phase with the primary key. Does not add the project phase to the database.
	*
	* @param projectPhaseId the primary key for the new project phase
	* @return the new project phase
	*/
	@Override
	public com.collaboratelab.project.model.ProjectPhase createProjectPhase(
		long projectPhaseId) {
		return _projectPhaseLocalService.createProjectPhase(projectPhaseId);
	}

	/**
	* Deletes the project phase from the database. Also notifies the appropriate model listeners.
	*
	* @param projectPhase the project phase
	* @return the project phase that was removed
	*/
	@Override
	public com.collaboratelab.project.model.ProjectPhase deleteProjectPhase(
		com.collaboratelab.project.model.ProjectPhase projectPhase) {
		return _projectPhaseLocalService.deleteProjectPhase(projectPhase);
	}

	/**
	* Deletes the project phase with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param projectPhaseId the primary key of the project phase
	* @return the project phase that was removed
	* @throws PortalException if a project phase with the primary key could not be found
	*/
	@Override
	public com.collaboratelab.project.model.ProjectPhase deleteProjectPhase(
		long projectPhaseId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectPhaseLocalService.deleteProjectPhase(projectPhaseId);
	}

	@Override
	public com.collaboratelab.project.model.ProjectPhase fetchProjectPhase(
		long projectPhaseId) {
		return _projectPhaseLocalService.fetchProjectPhase(projectPhaseId);
	}

	/**
	* Returns the project phase with the matching UUID and company.
	*
	* @param uuid the project phase's UUID
	* @param companyId the primary key of the company
	* @return the matching project phase, or <code>null</code> if a matching project phase could not be found
	*/
	@Override
	public com.collaboratelab.project.model.ProjectPhase fetchProjectPhaseByUuidAndCompanyId(
		java.lang.String uuid, long companyId) {
		return _projectPhaseLocalService.fetchProjectPhaseByUuidAndCompanyId(uuid,
			companyId);
	}

	@Override
	public com.collaboratelab.project.model.ProjectPhase findByProjectPhaseId(
		long projectPhaseId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _projectPhaseLocalService.findByProjectPhaseId(projectPhaseId);
	}

	@Override
	public com.collaboratelab.project.model.ProjectPhase findByProjectStructureIdIdentifier(
		long projectStructureId, java.lang.String identifier)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _projectPhaseLocalService.findByProjectStructureIdIdentifier(projectStructureId,
			identifier);
	}

	/**
	* Returns the project phase with the primary key.
	*
	* @param projectPhaseId the primary key of the project phase
	* @return the project phase
	* @throws PortalException if a project phase with the primary key could not be found
	*/
	@Override
	public com.collaboratelab.project.model.ProjectPhase getProjectPhase(
		long projectPhaseId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectPhaseLocalService.getProjectPhase(projectPhaseId);
	}

	/**
	* Returns the project phase with the matching UUID and company.
	*
	* @param uuid the project phase's UUID
	* @param companyId the primary key of the company
	* @return the matching project phase
	* @throws PortalException if a matching project phase could not be found
	*/
	@Override
	public com.collaboratelab.project.model.ProjectPhase getProjectPhaseByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectPhaseLocalService.getProjectPhaseByUuidAndCompanyId(uuid,
			companyId);
	}

	/**
	* Updates the project phase in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param projectPhase the project phase
	* @return the project phase that was updated
	*/
	@Override
	public com.collaboratelab.project.model.ProjectPhase updateProjectPhase(
		com.collaboratelab.project.model.ProjectPhase projectPhase) {
		return _projectPhaseLocalService.updateProjectPhase(projectPhase);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _projectPhaseLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _projectPhaseLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery getExportActionableDynamicQuery(
		com.liferay.exportimport.kernel.lar.PortletDataContext portletDataContext) {
		return _projectPhaseLocalService.getExportActionableDynamicQuery(portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _projectPhaseLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectPhaseLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectPhaseLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of project phases.
	*
	* @return the number of project phases
	*/
	@Override
	public int getProjectPhasesCount() {
		return _projectPhaseLocalService.getProjectPhasesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _projectPhaseLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _projectPhaseLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.project.model.impl.ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _projectPhaseLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.project.model.impl.ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _projectPhaseLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	@Override
	public java.util.List<com.collaboratelab.project.model.ProjectPhase> findByProjectStructureId(
		long projectStructureId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _projectPhaseLocalService.findByProjectStructureId(projectStructureId);
	}

	/**
	* Returns a range of all the project phases.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.project.model.impl.ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project phases
	* @param end the upper bound of the range of project phases (not inclusive)
	* @return the range of project phases
	*/
	@Override
	public java.util.List<com.collaboratelab.project.model.ProjectPhase> getProjectPhases(
		int start, int end) {
		return _projectPhaseLocalService.getProjectPhases(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _projectPhaseLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _projectPhaseLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public ProjectPhaseLocalService getWrappedService() {
		return _projectPhaseLocalService;
	}

	@Override
	public void setWrappedService(
		ProjectPhaseLocalService projectPhaseLocalService) {
		_projectPhaseLocalService = projectPhaseLocalService;
	}

	private ProjectPhaseLocalService _projectPhaseLocalService;
}