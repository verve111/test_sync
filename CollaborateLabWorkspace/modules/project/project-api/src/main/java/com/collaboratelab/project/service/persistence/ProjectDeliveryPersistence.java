/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.project.exception.NoSuchProjectDeliveryException;
import com.collaboratelab.project.model.ProjectDelivery;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the project delivery service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Claudio Carlenzoli
 * @see com.collaboratelab.project.service.persistence.impl.ProjectDeliveryPersistenceImpl
 * @see ProjectDeliveryUtil
 * @generated
 */
@ProviderType
public interface ProjectDeliveryPersistence extends BasePersistence<ProjectDelivery> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ProjectDeliveryUtil} to access the project delivery persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the project deliveries where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching project deliveries
	*/
	public java.util.List<ProjectDelivery> findByUuid(java.lang.String uuid);

	/**
	* Returns a range of all the project deliveries where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @return the range of matching project deliveries
	*/
	public java.util.List<ProjectDelivery> findByUuid(java.lang.String uuid,
		int start, int end);

	/**
	* Returns an ordered range of all the project deliveries where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project deliveries
	*/
	public java.util.List<ProjectDelivery> findByUuid(java.lang.String uuid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator);

	/**
	* Returns an ordered range of all the project deliveries where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project deliveries
	*/
	public java.util.List<ProjectDelivery> findByUuid(java.lang.String uuid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first project delivery in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project delivery
	* @throws NoSuchProjectDeliveryException if a matching project delivery could not be found
	*/
	public ProjectDelivery findByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator)
		throws NoSuchProjectDeliveryException;

	/**
	* Returns the first project delivery in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project delivery, or <code>null</code> if a matching project delivery could not be found
	*/
	public ProjectDelivery fetchByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator);

	/**
	* Returns the last project delivery in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project delivery
	* @throws NoSuchProjectDeliveryException if a matching project delivery could not be found
	*/
	public ProjectDelivery findByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator)
		throws NoSuchProjectDeliveryException;

	/**
	* Returns the last project delivery in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project delivery, or <code>null</code> if a matching project delivery could not be found
	*/
	public ProjectDelivery fetchByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator);

	/**
	* Returns the project deliveries before and after the current project delivery in the ordered set where uuid = &#63;.
	*
	* @param deliveryId the primary key of the current project delivery
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project delivery
	* @throws NoSuchProjectDeliveryException if a project delivery with the primary key could not be found
	*/
	public ProjectDelivery[] findByUuid_PrevAndNext(long deliveryId,
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator)
		throws NoSuchProjectDeliveryException;

	/**
	* Removes all the project deliveries where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public void removeByUuid(java.lang.String uuid);

	/**
	* Returns the number of project deliveries where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching project deliveries
	*/
	public int countByUuid(java.lang.String uuid);

	/**
	* Returns all the project deliveries where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching project deliveries
	*/
	public java.util.List<ProjectDelivery> findByUuid_C(java.lang.String uuid,
		long companyId);

	/**
	* Returns a range of all the project deliveries where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @return the range of matching project deliveries
	*/
	public java.util.List<ProjectDelivery> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end);

	/**
	* Returns an ordered range of all the project deliveries where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project deliveries
	*/
	public java.util.List<ProjectDelivery> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator);

	/**
	* Returns an ordered range of all the project deliveries where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project deliveries
	*/
	public java.util.List<ProjectDelivery> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first project delivery in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project delivery
	* @throws NoSuchProjectDeliveryException if a matching project delivery could not be found
	*/
	public ProjectDelivery findByUuid_C_First(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator)
		throws NoSuchProjectDeliveryException;

	/**
	* Returns the first project delivery in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project delivery, or <code>null</code> if a matching project delivery could not be found
	*/
	public ProjectDelivery fetchByUuid_C_First(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator);

	/**
	* Returns the last project delivery in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project delivery
	* @throws NoSuchProjectDeliveryException if a matching project delivery could not be found
	*/
	public ProjectDelivery findByUuid_C_Last(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator)
		throws NoSuchProjectDeliveryException;

	/**
	* Returns the last project delivery in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project delivery, or <code>null</code> if a matching project delivery could not be found
	*/
	public ProjectDelivery fetchByUuid_C_Last(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator);

	/**
	* Returns the project deliveries before and after the current project delivery in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param deliveryId the primary key of the current project delivery
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project delivery
	* @throws NoSuchProjectDeliveryException if a project delivery with the primary key could not be found
	*/
	public ProjectDelivery[] findByUuid_C_PrevAndNext(long deliveryId,
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator)
		throws NoSuchProjectDeliveryException;

	/**
	* Removes all the project deliveries where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public void removeByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the number of project deliveries where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching project deliveries
	*/
	public int countByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns all the project deliveries where stepId = &#63;.
	*
	* @param stepId the step ID
	* @return the matching project deliveries
	*/
	public java.util.List<ProjectDelivery> findByStepId(long stepId);

	/**
	* Returns a range of all the project deliveries where stepId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param stepId the step ID
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @return the range of matching project deliveries
	*/
	public java.util.List<ProjectDelivery> findByStepId(long stepId, int start,
		int end);

	/**
	* Returns an ordered range of all the project deliveries where stepId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param stepId the step ID
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project deliveries
	*/
	public java.util.List<ProjectDelivery> findByStepId(long stepId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator);

	/**
	* Returns an ordered range of all the project deliveries where stepId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param stepId the step ID
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project deliveries
	*/
	public java.util.List<ProjectDelivery> findByStepId(long stepId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first project delivery in the ordered set where stepId = &#63;.
	*
	* @param stepId the step ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project delivery
	* @throws NoSuchProjectDeliveryException if a matching project delivery could not be found
	*/
	public ProjectDelivery findByStepId_First(long stepId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator)
		throws NoSuchProjectDeliveryException;

	/**
	* Returns the first project delivery in the ordered set where stepId = &#63;.
	*
	* @param stepId the step ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project delivery, or <code>null</code> if a matching project delivery could not be found
	*/
	public ProjectDelivery fetchByStepId_First(long stepId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator);

	/**
	* Returns the last project delivery in the ordered set where stepId = &#63;.
	*
	* @param stepId the step ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project delivery
	* @throws NoSuchProjectDeliveryException if a matching project delivery could not be found
	*/
	public ProjectDelivery findByStepId_Last(long stepId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator)
		throws NoSuchProjectDeliveryException;

	/**
	* Returns the last project delivery in the ordered set where stepId = &#63;.
	*
	* @param stepId the step ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project delivery, or <code>null</code> if a matching project delivery could not be found
	*/
	public ProjectDelivery fetchByStepId_Last(long stepId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator);

	/**
	* Returns the project deliveries before and after the current project delivery in the ordered set where stepId = &#63;.
	*
	* @param deliveryId the primary key of the current project delivery
	* @param stepId the step ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project delivery
	* @throws NoSuchProjectDeliveryException if a project delivery with the primary key could not be found
	*/
	public ProjectDelivery[] findByStepId_PrevAndNext(long deliveryId,
		long stepId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator)
		throws NoSuchProjectDeliveryException;

	/**
	* Removes all the project deliveries where stepId = &#63; from the database.
	*
	* @param stepId the step ID
	*/
	public void removeByStepId(long stepId);

	/**
	* Returns the number of project deliveries where stepId = &#63;.
	*
	* @param stepId the step ID
	* @return the number of matching project deliveries
	*/
	public int countByStepId(long stepId);

	/**
	* Returns all the project deliveries where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching project deliveries
	*/
	public java.util.List<ProjectDelivery> findByCompanyId(long companyId);

	/**
	* Returns a range of all the project deliveries where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @return the range of matching project deliveries
	*/
	public java.util.List<ProjectDelivery> findByCompanyId(long companyId,
		int start, int end);

	/**
	* Returns an ordered range of all the project deliveries where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project deliveries
	*/
	public java.util.List<ProjectDelivery> findByCompanyId(long companyId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator);

	/**
	* Returns an ordered range of all the project deliveries where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project deliveries
	*/
	public java.util.List<ProjectDelivery> findByCompanyId(long companyId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first project delivery in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project delivery
	* @throws NoSuchProjectDeliveryException if a matching project delivery could not be found
	*/
	public ProjectDelivery findByCompanyId_First(long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator)
		throws NoSuchProjectDeliveryException;

	/**
	* Returns the first project delivery in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project delivery, or <code>null</code> if a matching project delivery could not be found
	*/
	public ProjectDelivery fetchByCompanyId_First(long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator);

	/**
	* Returns the last project delivery in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project delivery
	* @throws NoSuchProjectDeliveryException if a matching project delivery could not be found
	*/
	public ProjectDelivery findByCompanyId_Last(long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator)
		throws NoSuchProjectDeliveryException;

	/**
	* Returns the last project delivery in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project delivery, or <code>null</code> if a matching project delivery could not be found
	*/
	public ProjectDelivery fetchByCompanyId_Last(long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator);

	/**
	* Returns the project deliveries before and after the current project delivery in the ordered set where companyId = &#63;.
	*
	* @param deliveryId the primary key of the current project delivery
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project delivery
	* @throws NoSuchProjectDeliveryException if a project delivery with the primary key could not be found
	*/
	public ProjectDelivery[] findByCompanyId_PrevAndNext(long deliveryId,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator)
		throws NoSuchProjectDeliveryException;

	/**
	* Removes all the project deliveries where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	*/
	public void removeByCompanyId(long companyId);

	/**
	* Returns the number of project deliveries where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching project deliveries
	*/
	public int countByCompanyId(long companyId);

	/**
	* Returns the project delivery where companyId = &#63; and stepId = &#63; or throws a {@link NoSuchProjectDeliveryException} if it could not be found.
	*
	* @param companyId the company ID
	* @param stepId the step ID
	* @return the matching project delivery
	* @throws NoSuchProjectDeliveryException if a matching project delivery could not be found
	*/
	public ProjectDelivery findByCompanyIdIdentifier(long companyId, long stepId)
		throws NoSuchProjectDeliveryException;

	/**
	* Returns the project delivery where companyId = &#63; and stepId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param companyId the company ID
	* @param stepId the step ID
	* @return the matching project delivery, or <code>null</code> if a matching project delivery could not be found
	*/
	public ProjectDelivery fetchByCompanyIdIdentifier(long companyId,
		long stepId);

	/**
	* Returns the project delivery where companyId = &#63; and stepId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param companyId the company ID
	* @param stepId the step ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project delivery, or <code>null</code> if a matching project delivery could not be found
	*/
	public ProjectDelivery fetchByCompanyIdIdentifier(long companyId,
		long stepId, boolean retrieveFromCache);

	/**
	* Removes the project delivery where companyId = &#63; and stepId = &#63; from the database.
	*
	* @param companyId the company ID
	* @param stepId the step ID
	* @return the project delivery that was removed
	*/
	public ProjectDelivery removeByCompanyIdIdentifier(long companyId,
		long stepId) throws NoSuchProjectDeliveryException;

	/**
	* Returns the number of project deliveries where companyId = &#63; and stepId = &#63;.
	*
	* @param companyId the company ID
	* @param stepId the step ID
	* @return the number of matching project deliveries
	*/
	public int countByCompanyIdIdentifier(long companyId, long stepId);

	/**
	* Returns all the project deliveries where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @return the matching project deliveries
	*/
	public java.util.List<ProjectDelivery> findByCompanyIdIncludeName(
		long companyId, java.lang.String name);

	/**
	* Returns a range of all the project deliveries where companyId = &#63; and name LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param name the name
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @return the range of matching project deliveries
	*/
	public java.util.List<ProjectDelivery> findByCompanyIdIncludeName(
		long companyId, java.lang.String name, int start, int end);

	/**
	* Returns an ordered range of all the project deliveries where companyId = &#63; and name LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param name the name
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project deliveries
	*/
	public java.util.List<ProjectDelivery> findByCompanyIdIncludeName(
		long companyId, java.lang.String name, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator);

	/**
	* Returns an ordered range of all the project deliveries where companyId = &#63; and name LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param name the name
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project deliveries
	*/
	public java.util.List<ProjectDelivery> findByCompanyIdIncludeName(
		long companyId, java.lang.String name, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first project delivery in the ordered set where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project delivery
	* @throws NoSuchProjectDeliveryException if a matching project delivery could not be found
	*/
	public ProjectDelivery findByCompanyIdIncludeName_First(long companyId,
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator)
		throws NoSuchProjectDeliveryException;

	/**
	* Returns the first project delivery in the ordered set where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project delivery, or <code>null</code> if a matching project delivery could not be found
	*/
	public ProjectDelivery fetchByCompanyIdIncludeName_First(long companyId,
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator);

	/**
	* Returns the last project delivery in the ordered set where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project delivery
	* @throws NoSuchProjectDeliveryException if a matching project delivery could not be found
	*/
	public ProjectDelivery findByCompanyIdIncludeName_Last(long companyId,
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator)
		throws NoSuchProjectDeliveryException;

	/**
	* Returns the last project delivery in the ordered set where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project delivery, or <code>null</code> if a matching project delivery could not be found
	*/
	public ProjectDelivery fetchByCompanyIdIncludeName_Last(long companyId,
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator);

	/**
	* Returns the project deliveries before and after the current project delivery in the ordered set where companyId = &#63; and name LIKE &#63;.
	*
	* @param deliveryId the primary key of the current project delivery
	* @param companyId the company ID
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project delivery
	* @throws NoSuchProjectDeliveryException if a project delivery with the primary key could not be found
	*/
	public ProjectDelivery[] findByCompanyIdIncludeName_PrevAndNext(
		long deliveryId, long companyId, java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator)
		throws NoSuchProjectDeliveryException;

	/**
	* Removes all the project deliveries where companyId = &#63; and name LIKE &#63; from the database.
	*
	* @param companyId the company ID
	* @param name the name
	*/
	public void removeByCompanyIdIncludeName(long companyId,
		java.lang.String name);

	/**
	* Returns the number of project deliveries where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @return the number of matching project deliveries
	*/
	public int countByCompanyIdIncludeName(long companyId, java.lang.String name);

	/**
	* Caches the project delivery in the entity cache if it is enabled.
	*
	* @param projectDelivery the project delivery
	*/
	public void cacheResult(ProjectDelivery projectDelivery);

	/**
	* Caches the project deliveries in the entity cache if it is enabled.
	*
	* @param projectDeliveries the project deliveries
	*/
	public void cacheResult(java.util.List<ProjectDelivery> projectDeliveries);

	/**
	* Creates a new project delivery with the primary key. Does not add the project delivery to the database.
	*
	* @param deliveryId the primary key for the new project delivery
	* @return the new project delivery
	*/
	public ProjectDelivery create(long deliveryId);

	/**
	* Removes the project delivery with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param deliveryId the primary key of the project delivery
	* @return the project delivery that was removed
	* @throws NoSuchProjectDeliveryException if a project delivery with the primary key could not be found
	*/
	public ProjectDelivery remove(long deliveryId)
		throws NoSuchProjectDeliveryException;

	public ProjectDelivery updateImpl(ProjectDelivery projectDelivery);

	/**
	* Returns the project delivery with the primary key or throws a {@link NoSuchProjectDeliveryException} if it could not be found.
	*
	* @param deliveryId the primary key of the project delivery
	* @return the project delivery
	* @throws NoSuchProjectDeliveryException if a project delivery with the primary key could not be found
	*/
	public ProjectDelivery findByPrimaryKey(long deliveryId)
		throws NoSuchProjectDeliveryException;

	/**
	* Returns the project delivery with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param deliveryId the primary key of the project delivery
	* @return the project delivery, or <code>null</code> if a project delivery with the primary key could not be found
	*/
	public ProjectDelivery fetchByPrimaryKey(long deliveryId);

	@Override
	public java.util.Map<java.io.Serializable, ProjectDelivery> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the project deliveries.
	*
	* @return the project deliveries
	*/
	public java.util.List<ProjectDelivery> findAll();

	/**
	* Returns a range of all the project deliveries.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @return the range of project deliveries
	*/
	public java.util.List<ProjectDelivery> findAll(int start, int end);

	/**
	* Returns an ordered range of all the project deliveries.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of project deliveries
	*/
	public java.util.List<ProjectDelivery> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator);

	/**
	* Returns an ordered range of all the project deliveries.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of project deliveries
	*/
	public java.util.List<ProjectDelivery> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDelivery> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the project deliveries from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of project deliveries.
	*
	* @return the number of project deliveries
	*/
	public int countAll();

	@Override
	public java.util.Set<java.lang.String> getBadColumnNames();
}