/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for ProjectStructure. This utility wraps
 * {@link com.collaboratelab.project.service.impl.ProjectStructureLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Claudio Carlenzoli
 * @see ProjectStructureLocalService
 * @see com.collaboratelab.project.service.base.ProjectStructureLocalServiceBaseImpl
 * @see com.collaboratelab.project.service.impl.ProjectStructureLocalServiceImpl
 * @generated
 */
@ProviderType
public class ProjectStructureLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.collaboratelab.project.service.impl.ProjectStructureLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the project structure to the database. Also notifies the appropriate model listeners.
	*
	* @param projectStructure the project structure
	* @return the project structure that was added
	*/
	public static com.collaboratelab.project.model.ProjectStructure addProjectStructure(
		com.collaboratelab.project.model.ProjectStructure projectStructure) {
		return getService().addProjectStructure(projectStructure);
	}

	/**
	* Creates a new project structure with the primary key. Does not add the project structure to the database.
	*
	* @param projectStructureId the primary key for the new project structure
	* @return the new project structure
	*/
	public static com.collaboratelab.project.model.ProjectStructure createProjectStructure(
		long projectStructureId) {
		return getService().createProjectStructure(projectStructureId);
	}

	/**
	* Deletes the project structure from the database. Also notifies the appropriate model listeners.
	*
	* @param projectStructure the project structure
	* @return the project structure that was removed
	*/
	public static com.collaboratelab.project.model.ProjectStructure deleteProjectStructure(
		com.collaboratelab.project.model.ProjectStructure projectStructure) {
		return getService().deleteProjectStructure(projectStructure);
	}

	/**
	* Deletes the project structure with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param projectStructureId the primary key of the project structure
	* @return the project structure that was removed
	* @throws PortalException if a project structure with the primary key could not be found
	*/
	public static com.collaboratelab.project.model.ProjectStructure deleteProjectStructure(
		long projectStructureId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteProjectStructure(projectStructureId);
	}

	public static com.collaboratelab.project.model.ProjectStructure fetchProjectStructure(
		long projectStructureId) {
		return getService().fetchProjectStructure(projectStructureId);
	}

	/**
	* Returns the project structure with the matching UUID and company.
	*
	* @param uuid the project structure's UUID
	* @param companyId the primary key of the company
	* @return the matching project structure, or <code>null</code> if a matching project structure could not be found
	*/
	public static com.collaboratelab.project.model.ProjectStructure fetchProjectStructureByUuidAndCompanyId(
		java.lang.String uuid, long companyId) {
		return getService()
				   .fetchProjectStructureByUuidAndCompanyId(uuid, companyId);
	}

	public static com.collaboratelab.project.model.ProjectStructure findByProjectId(
		long projectId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByProjectId(projectId);
	}

	/**
	* Returns the project structure with the primary key.
	*
	* @param projectStructureId the primary key of the project structure
	* @return the project structure
	* @throws PortalException if a project structure with the primary key could not be found
	*/
	public static com.collaboratelab.project.model.ProjectStructure getProjectStructure(
		long projectStructureId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getProjectStructure(projectStructureId);
	}

	/**
	* Returns the project structure with the matching UUID and company.
	*
	* @param uuid the project structure's UUID
	* @param companyId the primary key of the company
	* @return the matching project structure
	* @throws PortalException if a matching project structure could not be found
	*/
	public static com.collaboratelab.project.model.ProjectStructure getProjectStructureByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService()
				   .getProjectStructureByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Updates the project structure in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param projectStructure the project structure
	* @return the project structure that was updated
	*/
	public static com.collaboratelab.project.model.ProjectStructure updateProjectStructure(
		com.collaboratelab.project.model.ProjectStructure projectStructure) {
		return getService().updateProjectStructure(projectStructure);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery getExportActionableDynamicQuery(
		com.liferay.exportimport.kernel.lar.PortletDataContext portletDataContext) {
		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of project structures.
	*
	* @return the number of project structures
	*/
	public static int getProjectStructuresCount() {
		return getService().getProjectStructuresCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.project.model.impl.ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.project.model.impl.ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns a range of all the project structures.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.project.model.impl.ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project structures
	* @param end the upper bound of the range of project structures (not inclusive)
	* @return the range of project structures
	*/
	public static java.util.List<com.collaboratelab.project.model.ProjectStructure> getProjectStructures(
		int start, int end) {
		return getService().getProjectStructures(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static ProjectStructureLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ProjectStructureLocalService, ProjectStructureLocalService> _serviceTracker =
		ServiceTrackerFactory.open(ProjectStructureLocalService.class);
}