/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.project.model.ProjectStructure;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the project structure service. This utility wraps {@link com.collaboratelab.project.service.persistence.impl.ProjectStructurePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Claudio Carlenzoli
 * @see ProjectStructurePersistence
 * @see com.collaboratelab.project.service.persistence.impl.ProjectStructurePersistenceImpl
 * @generated
 */
@ProviderType
public class ProjectStructureUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(ProjectStructure projectStructure) {
		getPersistence().clearCache(projectStructure);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<ProjectStructure> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<ProjectStructure> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<ProjectStructure> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<ProjectStructure> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static ProjectStructure update(ProjectStructure projectStructure) {
		return getPersistence().update(projectStructure);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static ProjectStructure update(ProjectStructure projectStructure,
		ServiceContext serviceContext) {
		return getPersistence().update(projectStructure, serviceContext);
	}

	/**
	* Returns all the project structures where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching project structures
	*/
	public static List<ProjectStructure> findByUuid(java.lang.String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	* Returns a range of all the project structures where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of project structures
	* @param end the upper bound of the range of project structures (not inclusive)
	* @return the range of matching project structures
	*/
	public static List<ProjectStructure> findByUuid(java.lang.String uuid,
		int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	* Returns an ordered range of all the project structures where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of project structures
	* @param end the upper bound of the range of project structures (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project structures
	*/
	public static List<ProjectStructure> findByUuid(java.lang.String uuid,
		int start, int end,
		OrderByComparator<ProjectStructure> orderByComparator) {
		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the project structures where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of project structures
	* @param end the upper bound of the range of project structures (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project structures
	*/
	public static List<ProjectStructure> findByUuid(java.lang.String uuid,
		int start, int end,
		OrderByComparator<ProjectStructure> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid(uuid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first project structure in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project structure
	* @throws NoSuchProjectStructureException if a matching project structure could not be found
	*/
	public static ProjectStructure findByUuid_First(java.lang.String uuid,
		OrderByComparator<ProjectStructure> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectStructureException {
		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the first project structure in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project structure, or <code>null</code> if a matching project structure could not be found
	*/
	public static ProjectStructure fetchByUuid_First(java.lang.String uuid,
		OrderByComparator<ProjectStructure> orderByComparator) {
		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the last project structure in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project structure
	* @throws NoSuchProjectStructureException if a matching project structure could not be found
	*/
	public static ProjectStructure findByUuid_Last(java.lang.String uuid,
		OrderByComparator<ProjectStructure> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectStructureException {
		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the last project structure in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project structure, or <code>null</code> if a matching project structure could not be found
	*/
	public static ProjectStructure fetchByUuid_Last(java.lang.String uuid,
		OrderByComparator<ProjectStructure> orderByComparator) {
		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the project structures before and after the current project structure in the ordered set where uuid = &#63;.
	*
	* @param projectStructureId the primary key of the current project structure
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project structure
	* @throws NoSuchProjectStructureException if a project structure with the primary key could not be found
	*/
	public static ProjectStructure[] findByUuid_PrevAndNext(
		long projectStructureId, java.lang.String uuid,
		OrderByComparator<ProjectStructure> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectStructureException {
		return getPersistence()
				   .findByUuid_PrevAndNext(projectStructureId, uuid,
			orderByComparator);
	}

	/**
	* Removes all the project structures where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public static void removeByUuid(java.lang.String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	* Returns the number of project structures where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching project structures
	*/
	public static int countByUuid(java.lang.String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	* Returns all the project structures where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching project structures
	*/
	public static List<ProjectStructure> findByUuid_C(java.lang.String uuid,
		long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	* Returns a range of all the project structures where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of project structures
	* @param end the upper bound of the range of project structures (not inclusive)
	* @return the range of matching project structures
	*/
	public static List<ProjectStructure> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end) {
		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	* Returns an ordered range of all the project structures where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of project structures
	* @param end the upper bound of the range of project structures (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project structures
	*/
	public static List<ProjectStructure> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<ProjectStructure> orderByComparator) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the project structures where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of project structures
	* @param end the upper bound of the range of project structures (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project structures
	*/
	public static List<ProjectStructure> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<ProjectStructure> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first project structure in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project structure
	* @throws NoSuchProjectStructureException if a matching project structure could not be found
	*/
	public static ProjectStructure findByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<ProjectStructure> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectStructureException {
		return getPersistence()
				   .findByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the first project structure in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project structure, or <code>null</code> if a matching project structure could not be found
	*/
	public static ProjectStructure fetchByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<ProjectStructure> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last project structure in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project structure
	* @throws NoSuchProjectStructureException if a matching project structure could not be found
	*/
	public static ProjectStructure findByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<ProjectStructure> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectStructureException {
		return getPersistence()
				   .findByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last project structure in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project structure, or <code>null</code> if a matching project structure could not be found
	*/
	public static ProjectStructure fetchByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<ProjectStructure> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the project structures before and after the current project structure in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param projectStructureId the primary key of the current project structure
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project structure
	* @throws NoSuchProjectStructureException if a project structure with the primary key could not be found
	*/
	public static ProjectStructure[] findByUuid_C_PrevAndNext(
		long projectStructureId, java.lang.String uuid, long companyId,
		OrderByComparator<ProjectStructure> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectStructureException {
		return getPersistence()
				   .findByUuid_C_PrevAndNext(projectStructureId, uuid,
			companyId, orderByComparator);
	}

	/**
	* Removes all the project structures where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public static void removeByUuid_C(java.lang.String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	* Returns the number of project structures where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching project structures
	*/
	public static int countByUuid_C(java.lang.String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	* Returns all the project structures where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching project structures
	*/
	public static List<ProjectStructure> findByCompanyId(long companyId) {
		return getPersistence().findByCompanyId(companyId);
	}

	/**
	* Returns a range of all the project structures where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of project structures
	* @param end the upper bound of the range of project structures (not inclusive)
	* @return the range of matching project structures
	*/
	public static List<ProjectStructure> findByCompanyId(long companyId,
		int start, int end) {
		return getPersistence().findByCompanyId(companyId, start, end);
	}

	/**
	* Returns an ordered range of all the project structures where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of project structures
	* @param end the upper bound of the range of project structures (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project structures
	*/
	public static List<ProjectStructure> findByCompanyId(long companyId,
		int start, int end,
		OrderByComparator<ProjectStructure> orderByComparator) {
		return getPersistence()
				   .findByCompanyId(companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the project structures where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of project structures
	* @param end the upper bound of the range of project structures (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project structures
	*/
	public static List<ProjectStructure> findByCompanyId(long companyId,
		int start, int end,
		OrderByComparator<ProjectStructure> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByCompanyId(companyId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first project structure in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project structure
	* @throws NoSuchProjectStructureException if a matching project structure could not be found
	*/
	public static ProjectStructure findByCompanyId_First(long companyId,
		OrderByComparator<ProjectStructure> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectStructureException {
		return getPersistence()
				   .findByCompanyId_First(companyId, orderByComparator);
	}

	/**
	* Returns the first project structure in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project structure, or <code>null</code> if a matching project structure could not be found
	*/
	public static ProjectStructure fetchByCompanyId_First(long companyId,
		OrderByComparator<ProjectStructure> orderByComparator) {
		return getPersistence()
				   .fetchByCompanyId_First(companyId, orderByComparator);
	}

	/**
	* Returns the last project structure in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project structure
	* @throws NoSuchProjectStructureException if a matching project structure could not be found
	*/
	public static ProjectStructure findByCompanyId_Last(long companyId,
		OrderByComparator<ProjectStructure> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectStructureException {
		return getPersistence()
				   .findByCompanyId_Last(companyId, orderByComparator);
	}

	/**
	* Returns the last project structure in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project structure, or <code>null</code> if a matching project structure could not be found
	*/
	public static ProjectStructure fetchByCompanyId_Last(long companyId,
		OrderByComparator<ProjectStructure> orderByComparator) {
		return getPersistence()
				   .fetchByCompanyId_Last(companyId, orderByComparator);
	}

	/**
	* Returns the project structures before and after the current project structure in the ordered set where companyId = &#63;.
	*
	* @param projectStructureId the primary key of the current project structure
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project structure
	* @throws NoSuchProjectStructureException if a project structure with the primary key could not be found
	*/
	public static ProjectStructure[] findByCompanyId_PrevAndNext(
		long projectStructureId, long companyId,
		OrderByComparator<ProjectStructure> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectStructureException {
		return getPersistence()
				   .findByCompanyId_PrevAndNext(projectStructureId, companyId,
			orderByComparator);
	}

	/**
	* Removes all the project structures where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	*/
	public static void removeByCompanyId(long companyId) {
		getPersistence().removeByCompanyId(companyId);
	}

	/**
	* Returns the number of project structures where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching project structures
	*/
	public static int countByCompanyId(long companyId) {
		return getPersistence().countByCompanyId(companyId);
	}

	/**
	* Returns the project structure where companyId = &#63; and identifier = &#63; or throws a {@link NoSuchProjectStructureException} if it could not be found.
	*
	* @param companyId the company ID
	* @param identifier the identifier
	* @return the matching project structure
	* @throws NoSuchProjectStructureException if a matching project structure could not be found
	*/
	public static ProjectStructure findByCompanyIdIdentifier(long companyId,
		java.lang.String identifier)
		throws com.collaboratelab.project.exception.NoSuchProjectStructureException {
		return getPersistence().findByCompanyIdIdentifier(companyId, identifier);
	}

	/**
	* Returns the project structure where companyId = &#63; and identifier = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param companyId the company ID
	* @param identifier the identifier
	* @return the matching project structure, or <code>null</code> if a matching project structure could not be found
	*/
	public static ProjectStructure fetchByCompanyIdIdentifier(long companyId,
		java.lang.String identifier) {
		return getPersistence().fetchByCompanyIdIdentifier(companyId, identifier);
	}

	/**
	* Returns the project structure where companyId = &#63; and identifier = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param companyId the company ID
	* @param identifier the identifier
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project structure, or <code>null</code> if a matching project structure could not be found
	*/
	public static ProjectStructure fetchByCompanyIdIdentifier(long companyId,
		java.lang.String identifier, boolean retrieveFromCache) {
		return getPersistence()
				   .fetchByCompanyIdIdentifier(companyId, identifier,
			retrieveFromCache);
	}

	/**
	* Removes the project structure where companyId = &#63; and identifier = &#63; from the database.
	*
	* @param companyId the company ID
	* @param identifier the identifier
	* @return the project structure that was removed
	*/
	public static ProjectStructure removeByCompanyIdIdentifier(long companyId,
		java.lang.String identifier)
		throws com.collaboratelab.project.exception.NoSuchProjectStructureException {
		return getPersistence()
				   .removeByCompanyIdIdentifier(companyId, identifier);
	}

	/**
	* Returns the number of project structures where companyId = &#63; and identifier = &#63;.
	*
	* @param companyId the company ID
	* @param identifier the identifier
	* @return the number of matching project structures
	*/
	public static int countByCompanyIdIdentifier(long companyId,
		java.lang.String identifier) {
		return getPersistence().countByCompanyIdIdentifier(companyId, identifier);
	}

	/**
	* Returns all the project structures where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @return the matching project structures
	*/
	public static List<ProjectStructure> findByCompanyIdIncludeName(
		long companyId, java.lang.String name) {
		return getPersistence().findByCompanyIdIncludeName(companyId, name);
	}

	/**
	* Returns a range of all the project structures where companyId = &#63; and name LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param name the name
	* @param start the lower bound of the range of project structures
	* @param end the upper bound of the range of project structures (not inclusive)
	* @return the range of matching project structures
	*/
	public static List<ProjectStructure> findByCompanyIdIncludeName(
		long companyId, java.lang.String name, int start, int end) {
		return getPersistence()
				   .findByCompanyIdIncludeName(companyId, name, start, end);
	}

	/**
	* Returns an ordered range of all the project structures where companyId = &#63; and name LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param name the name
	* @param start the lower bound of the range of project structures
	* @param end the upper bound of the range of project structures (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project structures
	*/
	public static List<ProjectStructure> findByCompanyIdIncludeName(
		long companyId, java.lang.String name, int start, int end,
		OrderByComparator<ProjectStructure> orderByComparator) {
		return getPersistence()
				   .findByCompanyIdIncludeName(companyId, name, start, end,
			orderByComparator);
	}

	/**
	* Returns an ordered range of all the project structures where companyId = &#63; and name LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param name the name
	* @param start the lower bound of the range of project structures
	* @param end the upper bound of the range of project structures (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project structures
	*/
	public static List<ProjectStructure> findByCompanyIdIncludeName(
		long companyId, java.lang.String name, int start, int end,
		OrderByComparator<ProjectStructure> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByCompanyIdIncludeName(companyId, name, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first project structure in the ordered set where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project structure
	* @throws NoSuchProjectStructureException if a matching project structure could not be found
	*/
	public static ProjectStructure findByCompanyIdIncludeName_First(
		long companyId, java.lang.String name,
		OrderByComparator<ProjectStructure> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectStructureException {
		return getPersistence()
				   .findByCompanyIdIncludeName_First(companyId, name,
			orderByComparator);
	}

	/**
	* Returns the first project structure in the ordered set where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project structure, or <code>null</code> if a matching project structure could not be found
	*/
	public static ProjectStructure fetchByCompanyIdIncludeName_First(
		long companyId, java.lang.String name,
		OrderByComparator<ProjectStructure> orderByComparator) {
		return getPersistence()
				   .fetchByCompanyIdIncludeName_First(companyId, name,
			orderByComparator);
	}

	/**
	* Returns the last project structure in the ordered set where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project structure
	* @throws NoSuchProjectStructureException if a matching project structure could not be found
	*/
	public static ProjectStructure findByCompanyIdIncludeName_Last(
		long companyId, java.lang.String name,
		OrderByComparator<ProjectStructure> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectStructureException {
		return getPersistence()
				   .findByCompanyIdIncludeName_Last(companyId, name,
			orderByComparator);
	}

	/**
	* Returns the last project structure in the ordered set where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project structure, or <code>null</code> if a matching project structure could not be found
	*/
	public static ProjectStructure fetchByCompanyIdIncludeName_Last(
		long companyId, java.lang.String name,
		OrderByComparator<ProjectStructure> orderByComparator) {
		return getPersistence()
				   .fetchByCompanyIdIncludeName_Last(companyId, name,
			orderByComparator);
	}

	/**
	* Returns the project structures before and after the current project structure in the ordered set where companyId = &#63; and name LIKE &#63;.
	*
	* @param projectStructureId the primary key of the current project structure
	* @param companyId the company ID
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project structure
	* @throws NoSuchProjectStructureException if a project structure with the primary key could not be found
	*/
	public static ProjectStructure[] findByCompanyIdIncludeName_PrevAndNext(
		long projectStructureId, long companyId, java.lang.String name,
		OrderByComparator<ProjectStructure> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectStructureException {
		return getPersistence()
				   .findByCompanyIdIncludeName_PrevAndNext(projectStructureId,
			companyId, name, orderByComparator);
	}

	/**
	* Removes all the project structures where companyId = &#63; and name LIKE &#63; from the database.
	*
	* @param companyId the company ID
	* @param name the name
	*/
	public static void removeByCompanyIdIncludeName(long companyId,
		java.lang.String name) {
		getPersistence().removeByCompanyIdIncludeName(companyId, name);
	}

	/**
	* Returns the number of project structures where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @return the number of matching project structures
	*/
	public static int countByCompanyIdIncludeName(long companyId,
		java.lang.String name) {
		return getPersistence().countByCompanyIdIncludeName(companyId, name);
	}

	/**
	* Returns the project structure where projectId = &#63; or throws a {@link NoSuchProjectStructureException} if it could not be found.
	*
	* @param projectId the project ID
	* @return the matching project structure
	* @throws NoSuchProjectStructureException if a matching project structure could not be found
	*/
	public static ProjectStructure findByProjectId(long projectId)
		throws com.collaboratelab.project.exception.NoSuchProjectStructureException {
		return getPersistence().findByProjectId(projectId);
	}

	/**
	* Returns the project structure where projectId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param projectId the project ID
	* @return the matching project structure, or <code>null</code> if a matching project structure could not be found
	*/
	public static ProjectStructure fetchByProjectId(long projectId) {
		return getPersistence().fetchByProjectId(projectId);
	}

	/**
	* Returns the project structure where projectId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param projectId the project ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project structure, or <code>null</code> if a matching project structure could not be found
	*/
	public static ProjectStructure fetchByProjectId(long projectId,
		boolean retrieveFromCache) {
		return getPersistence().fetchByProjectId(projectId, retrieveFromCache);
	}

	/**
	* Removes the project structure where projectId = &#63; from the database.
	*
	* @param projectId the project ID
	* @return the project structure that was removed
	*/
	public static ProjectStructure removeByProjectId(long projectId)
		throws com.collaboratelab.project.exception.NoSuchProjectStructureException {
		return getPersistence().removeByProjectId(projectId);
	}

	/**
	* Returns the number of project structures where projectId = &#63;.
	*
	* @param projectId the project ID
	* @return the number of matching project structures
	*/
	public static int countByProjectId(long projectId) {
		return getPersistence().countByProjectId(projectId);
	}

	/**
	* Caches the project structure in the entity cache if it is enabled.
	*
	* @param projectStructure the project structure
	*/
	public static void cacheResult(ProjectStructure projectStructure) {
		getPersistence().cacheResult(projectStructure);
	}

	/**
	* Caches the project structures in the entity cache if it is enabled.
	*
	* @param projectStructures the project structures
	*/
	public static void cacheResult(List<ProjectStructure> projectStructures) {
		getPersistence().cacheResult(projectStructures);
	}

	/**
	* Creates a new project structure with the primary key. Does not add the project structure to the database.
	*
	* @param projectStructureId the primary key for the new project structure
	* @return the new project structure
	*/
	public static ProjectStructure create(long projectStructureId) {
		return getPersistence().create(projectStructureId);
	}

	/**
	* Removes the project structure with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param projectStructureId the primary key of the project structure
	* @return the project structure that was removed
	* @throws NoSuchProjectStructureException if a project structure with the primary key could not be found
	*/
	public static ProjectStructure remove(long projectStructureId)
		throws com.collaboratelab.project.exception.NoSuchProjectStructureException {
		return getPersistence().remove(projectStructureId);
	}

	public static ProjectStructure updateImpl(ProjectStructure projectStructure) {
		return getPersistence().updateImpl(projectStructure);
	}

	/**
	* Returns the project structure with the primary key or throws a {@link NoSuchProjectStructureException} if it could not be found.
	*
	* @param projectStructureId the primary key of the project structure
	* @return the project structure
	* @throws NoSuchProjectStructureException if a project structure with the primary key could not be found
	*/
	public static ProjectStructure findByPrimaryKey(long projectStructureId)
		throws com.collaboratelab.project.exception.NoSuchProjectStructureException {
		return getPersistence().findByPrimaryKey(projectStructureId);
	}

	/**
	* Returns the project structure with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param projectStructureId the primary key of the project structure
	* @return the project structure, or <code>null</code> if a project structure with the primary key could not be found
	*/
	public static ProjectStructure fetchByPrimaryKey(long projectStructureId) {
		return getPersistence().fetchByPrimaryKey(projectStructureId);
	}

	public static java.util.Map<java.io.Serializable, ProjectStructure> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the project structures.
	*
	* @return the project structures
	*/
	public static List<ProjectStructure> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the project structures.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project structures
	* @param end the upper bound of the range of project structures (not inclusive)
	* @return the range of project structures
	*/
	public static List<ProjectStructure> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the project structures.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project structures
	* @param end the upper bound of the range of project structures (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of project structures
	*/
	public static List<ProjectStructure> findAll(int start, int end,
		OrderByComparator<ProjectStructure> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the project structures.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project structures
	* @param end the upper bound of the range of project structures (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of project structures
	*/
	public static List<ProjectStructure> findAll(int start, int end,
		OrderByComparator<ProjectStructure> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the project structures from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of project structures.
	*
	* @return the number of project structures
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static java.util.Set<java.lang.String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static ProjectStructurePersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ProjectStructurePersistence, ProjectStructurePersistence> _serviceTracker =
		ServiceTrackerFactory.open(ProjectStructurePersistence.class);
}