/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.project.model.ProjectStep;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the project step service. This utility wraps {@link com.collaboratelab.project.service.persistence.impl.ProjectStepPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Claudio Carlenzoli
 * @see ProjectStepPersistence
 * @see com.collaboratelab.project.service.persistence.impl.ProjectStepPersistenceImpl
 * @generated
 */
@ProviderType
public class ProjectStepUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(ProjectStep projectStep) {
		getPersistence().clearCache(projectStep);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<ProjectStep> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<ProjectStep> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<ProjectStep> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<ProjectStep> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static ProjectStep update(ProjectStep projectStep) {
		return getPersistence().update(projectStep);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static ProjectStep update(ProjectStep projectStep,
		ServiceContext serviceContext) {
		return getPersistence().update(projectStep, serviceContext);
	}

	/**
	* Returns all the project steps where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching project steps
	*/
	public static List<ProjectStep> findByUuid(java.lang.String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	* Returns a range of all the project steps where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @return the range of matching project steps
	*/
	public static List<ProjectStep> findByUuid(java.lang.String uuid,
		int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	* Returns an ordered range of all the project steps where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project steps
	*/
	public static List<ProjectStep> findByUuid(java.lang.String uuid,
		int start, int end, OrderByComparator<ProjectStep> orderByComparator) {
		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the project steps where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project steps
	*/
	public static List<ProjectStep> findByUuid(java.lang.String uuid,
		int start, int end, OrderByComparator<ProjectStep> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid(uuid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first project step in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project step
	* @throws NoSuchProjectStepException if a matching project step could not be found
	*/
	public static ProjectStep findByUuid_First(java.lang.String uuid,
		OrderByComparator<ProjectStep> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectStepException {
		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the first project step in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project step, or <code>null</code> if a matching project step could not be found
	*/
	public static ProjectStep fetchByUuid_First(java.lang.String uuid,
		OrderByComparator<ProjectStep> orderByComparator) {
		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the last project step in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project step
	* @throws NoSuchProjectStepException if a matching project step could not be found
	*/
	public static ProjectStep findByUuid_Last(java.lang.String uuid,
		OrderByComparator<ProjectStep> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectStepException {
		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the last project step in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project step, or <code>null</code> if a matching project step could not be found
	*/
	public static ProjectStep fetchByUuid_Last(java.lang.String uuid,
		OrderByComparator<ProjectStep> orderByComparator) {
		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the project steps before and after the current project step in the ordered set where uuid = &#63;.
	*
	* @param projectStepId the primary key of the current project step
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project step
	* @throws NoSuchProjectStepException if a project step with the primary key could not be found
	*/
	public static ProjectStep[] findByUuid_PrevAndNext(long projectStepId,
		java.lang.String uuid, OrderByComparator<ProjectStep> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectStepException {
		return getPersistence()
				   .findByUuid_PrevAndNext(projectStepId, uuid,
			orderByComparator);
	}

	/**
	* Removes all the project steps where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public static void removeByUuid(java.lang.String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	* Returns the number of project steps where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching project steps
	*/
	public static int countByUuid(java.lang.String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	* Returns all the project steps where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching project steps
	*/
	public static List<ProjectStep> findByUuid_C(java.lang.String uuid,
		long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	* Returns a range of all the project steps where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @return the range of matching project steps
	*/
	public static List<ProjectStep> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end) {
		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	* Returns an ordered range of all the project steps where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project steps
	*/
	public static List<ProjectStep> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<ProjectStep> orderByComparator) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the project steps where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project steps
	*/
	public static List<ProjectStep> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<ProjectStep> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first project step in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project step
	* @throws NoSuchProjectStepException if a matching project step could not be found
	*/
	public static ProjectStep findByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<ProjectStep> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectStepException {
		return getPersistence()
				   .findByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the first project step in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project step, or <code>null</code> if a matching project step could not be found
	*/
	public static ProjectStep fetchByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<ProjectStep> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last project step in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project step
	* @throws NoSuchProjectStepException if a matching project step could not be found
	*/
	public static ProjectStep findByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<ProjectStep> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectStepException {
		return getPersistence()
				   .findByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last project step in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project step, or <code>null</code> if a matching project step could not be found
	*/
	public static ProjectStep fetchByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<ProjectStep> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the project steps before and after the current project step in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param projectStepId the primary key of the current project step
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project step
	* @throws NoSuchProjectStepException if a project step with the primary key could not be found
	*/
	public static ProjectStep[] findByUuid_C_PrevAndNext(long projectStepId,
		java.lang.String uuid, long companyId,
		OrderByComparator<ProjectStep> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectStepException {
		return getPersistence()
				   .findByUuid_C_PrevAndNext(projectStepId, uuid, companyId,
			orderByComparator);
	}

	/**
	* Removes all the project steps where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public static void removeByUuid_C(java.lang.String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	* Returns the number of project steps where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching project steps
	*/
	public static int countByUuid_C(java.lang.String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	* Returns all the project steps where projectId = &#63;.
	*
	* @param projectId the project ID
	* @return the matching project steps
	*/
	public static List<ProjectStep> findByProjectId(long projectId) {
		return getPersistence().findByProjectId(projectId);
	}

	/**
	* Returns a range of all the project steps where projectId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectId the project ID
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @return the range of matching project steps
	*/
	public static List<ProjectStep> findByProjectId(long projectId, int start,
		int end) {
		return getPersistence().findByProjectId(projectId, start, end);
	}

	/**
	* Returns an ordered range of all the project steps where projectId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectId the project ID
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project steps
	*/
	public static List<ProjectStep> findByProjectId(long projectId, int start,
		int end, OrderByComparator<ProjectStep> orderByComparator) {
		return getPersistence()
				   .findByProjectId(projectId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the project steps where projectId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectId the project ID
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project steps
	*/
	public static List<ProjectStep> findByProjectId(long projectId, int start,
		int end, OrderByComparator<ProjectStep> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByProjectId(projectId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first project step in the ordered set where projectId = &#63;.
	*
	* @param projectId the project ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project step
	* @throws NoSuchProjectStepException if a matching project step could not be found
	*/
	public static ProjectStep findByProjectId_First(long projectId,
		OrderByComparator<ProjectStep> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectStepException {
		return getPersistence()
				   .findByProjectId_First(projectId, orderByComparator);
	}

	/**
	* Returns the first project step in the ordered set where projectId = &#63;.
	*
	* @param projectId the project ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project step, or <code>null</code> if a matching project step could not be found
	*/
	public static ProjectStep fetchByProjectId_First(long projectId,
		OrderByComparator<ProjectStep> orderByComparator) {
		return getPersistence()
				   .fetchByProjectId_First(projectId, orderByComparator);
	}

	/**
	* Returns the last project step in the ordered set where projectId = &#63;.
	*
	* @param projectId the project ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project step
	* @throws NoSuchProjectStepException if a matching project step could not be found
	*/
	public static ProjectStep findByProjectId_Last(long projectId,
		OrderByComparator<ProjectStep> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectStepException {
		return getPersistence()
				   .findByProjectId_Last(projectId, orderByComparator);
	}

	/**
	* Returns the last project step in the ordered set where projectId = &#63;.
	*
	* @param projectId the project ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project step, or <code>null</code> if a matching project step could not be found
	*/
	public static ProjectStep fetchByProjectId_Last(long projectId,
		OrderByComparator<ProjectStep> orderByComparator) {
		return getPersistence()
				   .fetchByProjectId_Last(projectId, orderByComparator);
	}

	/**
	* Returns the project steps before and after the current project step in the ordered set where projectId = &#63;.
	*
	* @param projectStepId the primary key of the current project step
	* @param projectId the project ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project step
	* @throws NoSuchProjectStepException if a project step with the primary key could not be found
	*/
	public static ProjectStep[] findByProjectId_PrevAndNext(
		long projectStepId, long projectId,
		OrderByComparator<ProjectStep> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectStepException {
		return getPersistence()
				   .findByProjectId_PrevAndNext(projectStepId, projectId,
			orderByComparator);
	}

	/**
	* Removes all the project steps where projectId = &#63; from the database.
	*
	* @param projectId the project ID
	*/
	public static void removeByProjectId(long projectId) {
		getPersistence().removeByProjectId(projectId);
	}

	/**
	* Returns the number of project steps where projectId = &#63;.
	*
	* @param projectId the project ID
	* @return the number of matching project steps
	*/
	public static int countByProjectId(long projectId) {
		return getPersistence().countByProjectId(projectId);
	}

	/**
	* Returns all the project steps where projectId = &#63; and projectDomainId = &#63;.
	*
	* @param projectId the project ID
	* @param projectDomainId the project domain ID
	* @return the matching project steps
	*/
	public static List<ProjectStep> findByProjectStructureIdProjectDomainId(
		long projectId, long projectDomainId) {
		return getPersistence()
				   .findByProjectStructureIdProjectDomainId(projectId,
			projectDomainId);
	}

	/**
	* Returns a range of all the project steps where projectId = &#63; and projectDomainId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectId the project ID
	* @param projectDomainId the project domain ID
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @return the range of matching project steps
	*/
	public static List<ProjectStep> findByProjectStructureIdProjectDomainId(
		long projectId, long projectDomainId, int start, int end) {
		return getPersistence()
				   .findByProjectStructureIdProjectDomainId(projectId,
			projectDomainId, start, end);
	}

	/**
	* Returns an ordered range of all the project steps where projectId = &#63; and projectDomainId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectId the project ID
	* @param projectDomainId the project domain ID
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project steps
	*/
	public static List<ProjectStep> findByProjectStructureIdProjectDomainId(
		long projectId, long projectDomainId, int start, int end,
		OrderByComparator<ProjectStep> orderByComparator) {
		return getPersistence()
				   .findByProjectStructureIdProjectDomainId(projectId,
			projectDomainId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the project steps where projectId = &#63; and projectDomainId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectId the project ID
	* @param projectDomainId the project domain ID
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project steps
	*/
	public static List<ProjectStep> findByProjectStructureIdProjectDomainId(
		long projectId, long projectDomainId, int start, int end,
		OrderByComparator<ProjectStep> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByProjectStructureIdProjectDomainId(projectId,
			projectDomainId, start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first project step in the ordered set where projectId = &#63; and projectDomainId = &#63;.
	*
	* @param projectId the project ID
	* @param projectDomainId the project domain ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project step
	* @throws NoSuchProjectStepException if a matching project step could not be found
	*/
	public static ProjectStep findByProjectStructureIdProjectDomainId_First(
		long projectId, long projectDomainId,
		OrderByComparator<ProjectStep> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectStepException {
		return getPersistence()
				   .findByProjectStructureIdProjectDomainId_First(projectId,
			projectDomainId, orderByComparator);
	}

	/**
	* Returns the first project step in the ordered set where projectId = &#63; and projectDomainId = &#63;.
	*
	* @param projectId the project ID
	* @param projectDomainId the project domain ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project step, or <code>null</code> if a matching project step could not be found
	*/
	public static ProjectStep fetchByProjectStructureIdProjectDomainId_First(
		long projectId, long projectDomainId,
		OrderByComparator<ProjectStep> orderByComparator) {
		return getPersistence()
				   .fetchByProjectStructureIdProjectDomainId_First(projectId,
			projectDomainId, orderByComparator);
	}

	/**
	* Returns the last project step in the ordered set where projectId = &#63; and projectDomainId = &#63;.
	*
	* @param projectId the project ID
	* @param projectDomainId the project domain ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project step
	* @throws NoSuchProjectStepException if a matching project step could not be found
	*/
	public static ProjectStep findByProjectStructureIdProjectDomainId_Last(
		long projectId, long projectDomainId,
		OrderByComparator<ProjectStep> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectStepException {
		return getPersistence()
				   .findByProjectStructureIdProjectDomainId_Last(projectId,
			projectDomainId, orderByComparator);
	}

	/**
	* Returns the last project step in the ordered set where projectId = &#63; and projectDomainId = &#63;.
	*
	* @param projectId the project ID
	* @param projectDomainId the project domain ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project step, or <code>null</code> if a matching project step could not be found
	*/
	public static ProjectStep fetchByProjectStructureIdProjectDomainId_Last(
		long projectId, long projectDomainId,
		OrderByComparator<ProjectStep> orderByComparator) {
		return getPersistence()
				   .fetchByProjectStructureIdProjectDomainId_Last(projectId,
			projectDomainId, orderByComparator);
	}

	/**
	* Returns the project steps before and after the current project step in the ordered set where projectId = &#63; and projectDomainId = &#63;.
	*
	* @param projectStepId the primary key of the current project step
	* @param projectId the project ID
	* @param projectDomainId the project domain ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project step
	* @throws NoSuchProjectStepException if a project step with the primary key could not be found
	*/
	public static ProjectStep[] findByProjectStructureIdProjectDomainId_PrevAndNext(
		long projectStepId, long projectId, long projectDomainId,
		OrderByComparator<ProjectStep> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectStepException {
		return getPersistence()
				   .findByProjectStructureIdProjectDomainId_PrevAndNext(projectStepId,
			projectId, projectDomainId, orderByComparator);
	}

	/**
	* Removes all the project steps where projectId = &#63; and projectDomainId = &#63; from the database.
	*
	* @param projectId the project ID
	* @param projectDomainId the project domain ID
	*/
	public static void removeByProjectStructureIdProjectDomainId(
		long projectId, long projectDomainId) {
		getPersistence()
			.removeByProjectStructureIdProjectDomainId(projectId,
			projectDomainId);
	}

	/**
	* Returns the number of project steps where projectId = &#63; and projectDomainId = &#63;.
	*
	* @param projectId the project ID
	* @param projectDomainId the project domain ID
	* @return the number of matching project steps
	*/
	public static int countByProjectStructureIdProjectDomainId(long projectId,
		long projectDomainId) {
		return getPersistence()
				   .countByProjectStructureIdProjectDomainId(projectId,
			projectDomainId);
	}

	/**
	* Returns all the project steps where projectId = &#63; and projectPhaseId = &#63;.
	*
	* @param projectId the project ID
	* @param projectPhaseId the project phase ID
	* @return the matching project steps
	*/
	public static List<ProjectStep> findByProjectIdProjectPhaseId(
		long projectId, long projectPhaseId) {
		return getPersistence()
				   .findByProjectIdProjectPhaseId(projectId, projectPhaseId);
	}

	/**
	* Returns a range of all the project steps where projectId = &#63; and projectPhaseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectId the project ID
	* @param projectPhaseId the project phase ID
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @return the range of matching project steps
	*/
	public static List<ProjectStep> findByProjectIdProjectPhaseId(
		long projectId, long projectPhaseId, int start, int end) {
		return getPersistence()
				   .findByProjectIdProjectPhaseId(projectId, projectPhaseId,
			start, end);
	}

	/**
	* Returns an ordered range of all the project steps where projectId = &#63; and projectPhaseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectId the project ID
	* @param projectPhaseId the project phase ID
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project steps
	*/
	public static List<ProjectStep> findByProjectIdProjectPhaseId(
		long projectId, long projectPhaseId, int start, int end,
		OrderByComparator<ProjectStep> orderByComparator) {
		return getPersistence()
				   .findByProjectIdProjectPhaseId(projectId, projectPhaseId,
			start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the project steps where projectId = &#63; and projectPhaseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectId the project ID
	* @param projectPhaseId the project phase ID
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project steps
	*/
	public static List<ProjectStep> findByProjectIdProjectPhaseId(
		long projectId, long projectPhaseId, int start, int end,
		OrderByComparator<ProjectStep> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByProjectIdProjectPhaseId(projectId, projectPhaseId,
			start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first project step in the ordered set where projectId = &#63; and projectPhaseId = &#63;.
	*
	* @param projectId the project ID
	* @param projectPhaseId the project phase ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project step
	* @throws NoSuchProjectStepException if a matching project step could not be found
	*/
	public static ProjectStep findByProjectIdProjectPhaseId_First(
		long projectId, long projectPhaseId,
		OrderByComparator<ProjectStep> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectStepException {
		return getPersistence()
				   .findByProjectIdProjectPhaseId_First(projectId,
			projectPhaseId, orderByComparator);
	}

	/**
	* Returns the first project step in the ordered set where projectId = &#63; and projectPhaseId = &#63;.
	*
	* @param projectId the project ID
	* @param projectPhaseId the project phase ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project step, or <code>null</code> if a matching project step could not be found
	*/
	public static ProjectStep fetchByProjectIdProjectPhaseId_First(
		long projectId, long projectPhaseId,
		OrderByComparator<ProjectStep> orderByComparator) {
		return getPersistence()
				   .fetchByProjectIdProjectPhaseId_First(projectId,
			projectPhaseId, orderByComparator);
	}

	/**
	* Returns the last project step in the ordered set where projectId = &#63; and projectPhaseId = &#63;.
	*
	* @param projectId the project ID
	* @param projectPhaseId the project phase ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project step
	* @throws NoSuchProjectStepException if a matching project step could not be found
	*/
	public static ProjectStep findByProjectIdProjectPhaseId_Last(
		long projectId, long projectPhaseId,
		OrderByComparator<ProjectStep> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectStepException {
		return getPersistence()
				   .findByProjectIdProjectPhaseId_Last(projectId,
			projectPhaseId, orderByComparator);
	}

	/**
	* Returns the last project step in the ordered set where projectId = &#63; and projectPhaseId = &#63;.
	*
	* @param projectId the project ID
	* @param projectPhaseId the project phase ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project step, or <code>null</code> if a matching project step could not be found
	*/
	public static ProjectStep fetchByProjectIdProjectPhaseId_Last(
		long projectId, long projectPhaseId,
		OrderByComparator<ProjectStep> orderByComparator) {
		return getPersistence()
				   .fetchByProjectIdProjectPhaseId_Last(projectId,
			projectPhaseId, orderByComparator);
	}

	/**
	* Returns the project steps before and after the current project step in the ordered set where projectId = &#63; and projectPhaseId = &#63;.
	*
	* @param projectStepId the primary key of the current project step
	* @param projectId the project ID
	* @param projectPhaseId the project phase ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project step
	* @throws NoSuchProjectStepException if a project step with the primary key could not be found
	*/
	public static ProjectStep[] findByProjectIdProjectPhaseId_PrevAndNext(
		long projectStepId, long projectId, long projectPhaseId,
		OrderByComparator<ProjectStep> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectStepException {
		return getPersistence()
				   .findByProjectIdProjectPhaseId_PrevAndNext(projectStepId,
			projectId, projectPhaseId, orderByComparator);
	}

	/**
	* Removes all the project steps where projectId = &#63; and projectPhaseId = &#63; from the database.
	*
	* @param projectId the project ID
	* @param projectPhaseId the project phase ID
	*/
	public static void removeByProjectIdProjectPhaseId(long projectId,
		long projectPhaseId) {
		getPersistence()
			.removeByProjectIdProjectPhaseId(projectId, projectPhaseId);
	}

	/**
	* Returns the number of project steps where projectId = &#63; and projectPhaseId = &#63;.
	*
	* @param projectId the project ID
	* @param projectPhaseId the project phase ID
	* @return the number of matching project steps
	*/
	public static int countByProjectIdProjectPhaseId(long projectId,
		long projectPhaseId) {
		return getPersistence()
				   .countByProjectIdProjectPhaseId(projectId, projectPhaseId);
	}

	/**
	* Returns the project step where projectId = &#63; and projectDomainId = &#63; and projectPhaseId = &#63; or throws a {@link NoSuchProjectStepException} if it could not be found.
	*
	* @param projectId the project ID
	* @param projectDomainId the project domain ID
	* @param projectPhaseId the project phase ID
	* @return the matching project step
	* @throws NoSuchProjectStepException if a matching project step could not be found
	*/
	public static ProjectStep findByProjectIdProjectDomainIdProjectPhaseId(
		long projectId, long projectDomainId, long projectPhaseId)
		throws com.collaboratelab.project.exception.NoSuchProjectStepException {
		return getPersistence()
				   .findByProjectIdProjectDomainIdProjectPhaseId(projectId,
			projectDomainId, projectPhaseId);
	}

	/**
	* Returns the project step where projectId = &#63; and projectDomainId = &#63; and projectPhaseId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param projectId the project ID
	* @param projectDomainId the project domain ID
	* @param projectPhaseId the project phase ID
	* @return the matching project step, or <code>null</code> if a matching project step could not be found
	*/
	public static ProjectStep fetchByProjectIdProjectDomainIdProjectPhaseId(
		long projectId, long projectDomainId, long projectPhaseId) {
		return getPersistence()
				   .fetchByProjectIdProjectDomainIdProjectPhaseId(projectId,
			projectDomainId, projectPhaseId);
	}

	/**
	* Returns the project step where projectId = &#63; and projectDomainId = &#63; and projectPhaseId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param projectId the project ID
	* @param projectDomainId the project domain ID
	* @param projectPhaseId the project phase ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project step, or <code>null</code> if a matching project step could not be found
	*/
	public static ProjectStep fetchByProjectIdProjectDomainIdProjectPhaseId(
		long projectId, long projectDomainId, long projectPhaseId,
		boolean retrieveFromCache) {
		return getPersistence()
				   .fetchByProjectIdProjectDomainIdProjectPhaseId(projectId,
			projectDomainId, projectPhaseId, retrieveFromCache);
	}

	/**
	* Removes the project step where projectId = &#63; and projectDomainId = &#63; and projectPhaseId = &#63; from the database.
	*
	* @param projectId the project ID
	* @param projectDomainId the project domain ID
	* @param projectPhaseId the project phase ID
	* @return the project step that was removed
	*/
	public static ProjectStep removeByProjectIdProjectDomainIdProjectPhaseId(
		long projectId, long projectDomainId, long projectPhaseId)
		throws com.collaboratelab.project.exception.NoSuchProjectStepException {
		return getPersistence()
				   .removeByProjectIdProjectDomainIdProjectPhaseId(projectId,
			projectDomainId, projectPhaseId);
	}

	/**
	* Returns the number of project steps where projectId = &#63; and projectDomainId = &#63; and projectPhaseId = &#63;.
	*
	* @param projectId the project ID
	* @param projectDomainId the project domain ID
	* @param projectPhaseId the project phase ID
	* @return the number of matching project steps
	*/
	public static int countByProjectIdProjectDomainIdProjectPhaseId(
		long projectId, long projectDomainId, long projectPhaseId) {
		return getPersistence()
				   .countByProjectIdProjectDomainIdProjectPhaseId(projectId,
			projectDomainId, projectPhaseId);
	}

	/**
	* Caches the project step in the entity cache if it is enabled.
	*
	* @param projectStep the project step
	*/
	public static void cacheResult(ProjectStep projectStep) {
		getPersistence().cacheResult(projectStep);
	}

	/**
	* Caches the project steps in the entity cache if it is enabled.
	*
	* @param projectSteps the project steps
	*/
	public static void cacheResult(List<ProjectStep> projectSteps) {
		getPersistence().cacheResult(projectSteps);
	}

	/**
	* Creates a new project step with the primary key. Does not add the project step to the database.
	*
	* @param projectStepId the primary key for the new project step
	* @return the new project step
	*/
	public static ProjectStep create(long projectStepId) {
		return getPersistence().create(projectStepId);
	}

	/**
	* Removes the project step with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param projectStepId the primary key of the project step
	* @return the project step that was removed
	* @throws NoSuchProjectStepException if a project step with the primary key could not be found
	*/
	public static ProjectStep remove(long projectStepId)
		throws com.collaboratelab.project.exception.NoSuchProjectStepException {
		return getPersistence().remove(projectStepId);
	}

	public static ProjectStep updateImpl(ProjectStep projectStep) {
		return getPersistence().updateImpl(projectStep);
	}

	/**
	* Returns the project step with the primary key or throws a {@link NoSuchProjectStepException} if it could not be found.
	*
	* @param projectStepId the primary key of the project step
	* @return the project step
	* @throws NoSuchProjectStepException if a project step with the primary key could not be found
	*/
	public static ProjectStep findByPrimaryKey(long projectStepId)
		throws com.collaboratelab.project.exception.NoSuchProjectStepException {
		return getPersistence().findByPrimaryKey(projectStepId);
	}

	/**
	* Returns the project step with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param projectStepId the primary key of the project step
	* @return the project step, or <code>null</code> if a project step with the primary key could not be found
	*/
	public static ProjectStep fetchByPrimaryKey(long projectStepId) {
		return getPersistence().fetchByPrimaryKey(projectStepId);
	}

	public static java.util.Map<java.io.Serializable, ProjectStep> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the project steps.
	*
	* @return the project steps
	*/
	public static List<ProjectStep> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the project steps.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @return the range of project steps
	*/
	public static List<ProjectStep> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the project steps.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of project steps
	*/
	public static List<ProjectStep> findAll(int start, int end,
		OrderByComparator<ProjectStep> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the project steps.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of project steps
	*/
	public static List<ProjectStep> findAll(int start, int end,
		OrderByComparator<ProjectStep> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the project steps from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of project steps.
	*
	* @return the number of project steps
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static java.util.Set<java.lang.String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static ProjectStepPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ProjectStepPersistence, ProjectStepPersistence> _serviceTracker =
		ServiceTrackerFactory.open(ProjectStepPersistence.class);
}