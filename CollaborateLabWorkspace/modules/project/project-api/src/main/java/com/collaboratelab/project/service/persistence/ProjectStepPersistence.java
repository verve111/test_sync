/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.project.exception.NoSuchProjectStepException;
import com.collaboratelab.project.model.ProjectStep;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the project step service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Claudio Carlenzoli
 * @see com.collaboratelab.project.service.persistence.impl.ProjectStepPersistenceImpl
 * @see ProjectStepUtil
 * @generated
 */
@ProviderType
public interface ProjectStepPersistence extends BasePersistence<ProjectStep> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ProjectStepUtil} to access the project step persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the project steps where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching project steps
	*/
	public java.util.List<ProjectStep> findByUuid(java.lang.String uuid);

	/**
	* Returns a range of all the project steps where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @return the range of matching project steps
	*/
	public java.util.List<ProjectStep> findByUuid(java.lang.String uuid,
		int start, int end);

	/**
	* Returns an ordered range of all the project steps where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project steps
	*/
	public java.util.List<ProjectStep> findByUuid(java.lang.String uuid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator);

	/**
	* Returns an ordered range of all the project steps where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project steps
	*/
	public java.util.List<ProjectStep> findByUuid(java.lang.String uuid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first project step in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project step
	* @throws NoSuchProjectStepException if a matching project step could not be found
	*/
	public ProjectStep findByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator)
		throws NoSuchProjectStepException;

	/**
	* Returns the first project step in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project step, or <code>null</code> if a matching project step could not be found
	*/
	public ProjectStep fetchByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator);

	/**
	* Returns the last project step in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project step
	* @throws NoSuchProjectStepException if a matching project step could not be found
	*/
	public ProjectStep findByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator)
		throws NoSuchProjectStepException;

	/**
	* Returns the last project step in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project step, or <code>null</code> if a matching project step could not be found
	*/
	public ProjectStep fetchByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator);

	/**
	* Returns the project steps before and after the current project step in the ordered set where uuid = &#63;.
	*
	* @param projectStepId the primary key of the current project step
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project step
	* @throws NoSuchProjectStepException if a project step with the primary key could not be found
	*/
	public ProjectStep[] findByUuid_PrevAndNext(long projectStepId,
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator)
		throws NoSuchProjectStepException;

	/**
	* Removes all the project steps where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public void removeByUuid(java.lang.String uuid);

	/**
	* Returns the number of project steps where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching project steps
	*/
	public int countByUuid(java.lang.String uuid);

	/**
	* Returns all the project steps where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching project steps
	*/
	public java.util.List<ProjectStep> findByUuid_C(java.lang.String uuid,
		long companyId);

	/**
	* Returns a range of all the project steps where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @return the range of matching project steps
	*/
	public java.util.List<ProjectStep> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end);

	/**
	* Returns an ordered range of all the project steps where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project steps
	*/
	public java.util.List<ProjectStep> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator);

	/**
	* Returns an ordered range of all the project steps where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project steps
	*/
	public java.util.List<ProjectStep> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first project step in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project step
	* @throws NoSuchProjectStepException if a matching project step could not be found
	*/
	public ProjectStep findByUuid_C_First(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator)
		throws NoSuchProjectStepException;

	/**
	* Returns the first project step in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project step, or <code>null</code> if a matching project step could not be found
	*/
	public ProjectStep fetchByUuid_C_First(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator);

	/**
	* Returns the last project step in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project step
	* @throws NoSuchProjectStepException if a matching project step could not be found
	*/
	public ProjectStep findByUuid_C_Last(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator)
		throws NoSuchProjectStepException;

	/**
	* Returns the last project step in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project step, or <code>null</code> if a matching project step could not be found
	*/
	public ProjectStep fetchByUuid_C_Last(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator);

	/**
	* Returns the project steps before and after the current project step in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param projectStepId the primary key of the current project step
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project step
	* @throws NoSuchProjectStepException if a project step with the primary key could not be found
	*/
	public ProjectStep[] findByUuid_C_PrevAndNext(long projectStepId,
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator)
		throws NoSuchProjectStepException;

	/**
	* Removes all the project steps where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public void removeByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the number of project steps where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching project steps
	*/
	public int countByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns all the project steps where projectId = &#63;.
	*
	* @param projectId the project ID
	* @return the matching project steps
	*/
	public java.util.List<ProjectStep> findByProjectId(long projectId);

	/**
	* Returns a range of all the project steps where projectId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectId the project ID
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @return the range of matching project steps
	*/
	public java.util.List<ProjectStep> findByProjectId(long projectId,
		int start, int end);

	/**
	* Returns an ordered range of all the project steps where projectId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectId the project ID
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project steps
	*/
	public java.util.List<ProjectStep> findByProjectId(long projectId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator);

	/**
	* Returns an ordered range of all the project steps where projectId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectId the project ID
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project steps
	*/
	public java.util.List<ProjectStep> findByProjectId(long projectId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first project step in the ordered set where projectId = &#63;.
	*
	* @param projectId the project ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project step
	* @throws NoSuchProjectStepException if a matching project step could not be found
	*/
	public ProjectStep findByProjectId_First(long projectId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator)
		throws NoSuchProjectStepException;

	/**
	* Returns the first project step in the ordered set where projectId = &#63;.
	*
	* @param projectId the project ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project step, or <code>null</code> if a matching project step could not be found
	*/
	public ProjectStep fetchByProjectId_First(long projectId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator);

	/**
	* Returns the last project step in the ordered set where projectId = &#63;.
	*
	* @param projectId the project ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project step
	* @throws NoSuchProjectStepException if a matching project step could not be found
	*/
	public ProjectStep findByProjectId_Last(long projectId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator)
		throws NoSuchProjectStepException;

	/**
	* Returns the last project step in the ordered set where projectId = &#63;.
	*
	* @param projectId the project ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project step, or <code>null</code> if a matching project step could not be found
	*/
	public ProjectStep fetchByProjectId_Last(long projectId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator);

	/**
	* Returns the project steps before and after the current project step in the ordered set where projectId = &#63;.
	*
	* @param projectStepId the primary key of the current project step
	* @param projectId the project ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project step
	* @throws NoSuchProjectStepException if a project step with the primary key could not be found
	*/
	public ProjectStep[] findByProjectId_PrevAndNext(long projectStepId,
		long projectId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator)
		throws NoSuchProjectStepException;

	/**
	* Removes all the project steps where projectId = &#63; from the database.
	*
	* @param projectId the project ID
	*/
	public void removeByProjectId(long projectId);

	/**
	* Returns the number of project steps where projectId = &#63;.
	*
	* @param projectId the project ID
	* @return the number of matching project steps
	*/
	public int countByProjectId(long projectId);

	/**
	* Returns all the project steps where projectId = &#63; and projectDomainId = &#63;.
	*
	* @param projectId the project ID
	* @param projectDomainId the project domain ID
	* @return the matching project steps
	*/
	public java.util.List<ProjectStep> findByProjectStructureIdProjectDomainId(
		long projectId, long projectDomainId);

	/**
	* Returns a range of all the project steps where projectId = &#63; and projectDomainId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectId the project ID
	* @param projectDomainId the project domain ID
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @return the range of matching project steps
	*/
	public java.util.List<ProjectStep> findByProjectStructureIdProjectDomainId(
		long projectId, long projectDomainId, int start, int end);

	/**
	* Returns an ordered range of all the project steps where projectId = &#63; and projectDomainId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectId the project ID
	* @param projectDomainId the project domain ID
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project steps
	*/
	public java.util.List<ProjectStep> findByProjectStructureIdProjectDomainId(
		long projectId, long projectDomainId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator);

	/**
	* Returns an ordered range of all the project steps where projectId = &#63; and projectDomainId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectId the project ID
	* @param projectDomainId the project domain ID
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project steps
	*/
	public java.util.List<ProjectStep> findByProjectStructureIdProjectDomainId(
		long projectId, long projectDomainId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first project step in the ordered set where projectId = &#63; and projectDomainId = &#63;.
	*
	* @param projectId the project ID
	* @param projectDomainId the project domain ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project step
	* @throws NoSuchProjectStepException if a matching project step could not be found
	*/
	public ProjectStep findByProjectStructureIdProjectDomainId_First(
		long projectId, long projectDomainId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator)
		throws NoSuchProjectStepException;

	/**
	* Returns the first project step in the ordered set where projectId = &#63; and projectDomainId = &#63;.
	*
	* @param projectId the project ID
	* @param projectDomainId the project domain ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project step, or <code>null</code> if a matching project step could not be found
	*/
	public ProjectStep fetchByProjectStructureIdProjectDomainId_First(
		long projectId, long projectDomainId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator);

	/**
	* Returns the last project step in the ordered set where projectId = &#63; and projectDomainId = &#63;.
	*
	* @param projectId the project ID
	* @param projectDomainId the project domain ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project step
	* @throws NoSuchProjectStepException if a matching project step could not be found
	*/
	public ProjectStep findByProjectStructureIdProjectDomainId_Last(
		long projectId, long projectDomainId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator)
		throws NoSuchProjectStepException;

	/**
	* Returns the last project step in the ordered set where projectId = &#63; and projectDomainId = &#63;.
	*
	* @param projectId the project ID
	* @param projectDomainId the project domain ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project step, or <code>null</code> if a matching project step could not be found
	*/
	public ProjectStep fetchByProjectStructureIdProjectDomainId_Last(
		long projectId, long projectDomainId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator);

	/**
	* Returns the project steps before and after the current project step in the ordered set where projectId = &#63; and projectDomainId = &#63;.
	*
	* @param projectStepId the primary key of the current project step
	* @param projectId the project ID
	* @param projectDomainId the project domain ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project step
	* @throws NoSuchProjectStepException if a project step with the primary key could not be found
	*/
	public ProjectStep[] findByProjectStructureIdProjectDomainId_PrevAndNext(
		long projectStepId, long projectId, long projectDomainId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator)
		throws NoSuchProjectStepException;

	/**
	* Removes all the project steps where projectId = &#63; and projectDomainId = &#63; from the database.
	*
	* @param projectId the project ID
	* @param projectDomainId the project domain ID
	*/
	public void removeByProjectStructureIdProjectDomainId(long projectId,
		long projectDomainId);

	/**
	* Returns the number of project steps where projectId = &#63; and projectDomainId = &#63;.
	*
	* @param projectId the project ID
	* @param projectDomainId the project domain ID
	* @return the number of matching project steps
	*/
	public int countByProjectStructureIdProjectDomainId(long projectId,
		long projectDomainId);

	/**
	* Returns all the project steps where projectId = &#63; and projectPhaseId = &#63;.
	*
	* @param projectId the project ID
	* @param projectPhaseId the project phase ID
	* @return the matching project steps
	*/
	public java.util.List<ProjectStep> findByProjectIdProjectPhaseId(
		long projectId, long projectPhaseId);

	/**
	* Returns a range of all the project steps where projectId = &#63; and projectPhaseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectId the project ID
	* @param projectPhaseId the project phase ID
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @return the range of matching project steps
	*/
	public java.util.List<ProjectStep> findByProjectIdProjectPhaseId(
		long projectId, long projectPhaseId, int start, int end);

	/**
	* Returns an ordered range of all the project steps where projectId = &#63; and projectPhaseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectId the project ID
	* @param projectPhaseId the project phase ID
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project steps
	*/
	public java.util.List<ProjectStep> findByProjectIdProjectPhaseId(
		long projectId, long projectPhaseId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator);

	/**
	* Returns an ordered range of all the project steps where projectId = &#63; and projectPhaseId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectId the project ID
	* @param projectPhaseId the project phase ID
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project steps
	*/
	public java.util.List<ProjectStep> findByProjectIdProjectPhaseId(
		long projectId, long projectPhaseId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first project step in the ordered set where projectId = &#63; and projectPhaseId = &#63;.
	*
	* @param projectId the project ID
	* @param projectPhaseId the project phase ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project step
	* @throws NoSuchProjectStepException if a matching project step could not be found
	*/
	public ProjectStep findByProjectIdProjectPhaseId_First(long projectId,
		long projectPhaseId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator)
		throws NoSuchProjectStepException;

	/**
	* Returns the first project step in the ordered set where projectId = &#63; and projectPhaseId = &#63;.
	*
	* @param projectId the project ID
	* @param projectPhaseId the project phase ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project step, or <code>null</code> if a matching project step could not be found
	*/
	public ProjectStep fetchByProjectIdProjectPhaseId_First(long projectId,
		long projectPhaseId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator);

	/**
	* Returns the last project step in the ordered set where projectId = &#63; and projectPhaseId = &#63;.
	*
	* @param projectId the project ID
	* @param projectPhaseId the project phase ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project step
	* @throws NoSuchProjectStepException if a matching project step could not be found
	*/
	public ProjectStep findByProjectIdProjectPhaseId_Last(long projectId,
		long projectPhaseId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator)
		throws NoSuchProjectStepException;

	/**
	* Returns the last project step in the ordered set where projectId = &#63; and projectPhaseId = &#63;.
	*
	* @param projectId the project ID
	* @param projectPhaseId the project phase ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project step, or <code>null</code> if a matching project step could not be found
	*/
	public ProjectStep fetchByProjectIdProjectPhaseId_Last(long projectId,
		long projectPhaseId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator);

	/**
	* Returns the project steps before and after the current project step in the ordered set where projectId = &#63; and projectPhaseId = &#63;.
	*
	* @param projectStepId the primary key of the current project step
	* @param projectId the project ID
	* @param projectPhaseId the project phase ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project step
	* @throws NoSuchProjectStepException if a project step with the primary key could not be found
	*/
	public ProjectStep[] findByProjectIdProjectPhaseId_PrevAndNext(
		long projectStepId, long projectId, long projectPhaseId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator)
		throws NoSuchProjectStepException;

	/**
	* Removes all the project steps where projectId = &#63; and projectPhaseId = &#63; from the database.
	*
	* @param projectId the project ID
	* @param projectPhaseId the project phase ID
	*/
	public void removeByProjectIdProjectPhaseId(long projectId,
		long projectPhaseId);

	/**
	* Returns the number of project steps where projectId = &#63; and projectPhaseId = &#63;.
	*
	* @param projectId the project ID
	* @param projectPhaseId the project phase ID
	* @return the number of matching project steps
	*/
	public int countByProjectIdProjectPhaseId(long projectId,
		long projectPhaseId);

	/**
	* Returns the project step where projectId = &#63; and projectDomainId = &#63; and projectPhaseId = &#63; or throws a {@link NoSuchProjectStepException} if it could not be found.
	*
	* @param projectId the project ID
	* @param projectDomainId the project domain ID
	* @param projectPhaseId the project phase ID
	* @return the matching project step
	* @throws NoSuchProjectStepException if a matching project step could not be found
	*/
	public ProjectStep findByProjectIdProjectDomainIdProjectPhaseId(
		long projectId, long projectDomainId, long projectPhaseId)
		throws NoSuchProjectStepException;

	/**
	* Returns the project step where projectId = &#63; and projectDomainId = &#63; and projectPhaseId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param projectId the project ID
	* @param projectDomainId the project domain ID
	* @param projectPhaseId the project phase ID
	* @return the matching project step, or <code>null</code> if a matching project step could not be found
	*/
	public ProjectStep fetchByProjectIdProjectDomainIdProjectPhaseId(
		long projectId, long projectDomainId, long projectPhaseId);

	/**
	* Returns the project step where projectId = &#63; and projectDomainId = &#63; and projectPhaseId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param projectId the project ID
	* @param projectDomainId the project domain ID
	* @param projectPhaseId the project phase ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project step, or <code>null</code> if a matching project step could not be found
	*/
	public ProjectStep fetchByProjectIdProjectDomainIdProjectPhaseId(
		long projectId, long projectDomainId, long projectPhaseId,
		boolean retrieveFromCache);

	/**
	* Removes the project step where projectId = &#63; and projectDomainId = &#63; and projectPhaseId = &#63; from the database.
	*
	* @param projectId the project ID
	* @param projectDomainId the project domain ID
	* @param projectPhaseId the project phase ID
	* @return the project step that was removed
	*/
	public ProjectStep removeByProjectIdProjectDomainIdProjectPhaseId(
		long projectId, long projectDomainId, long projectPhaseId)
		throws NoSuchProjectStepException;

	/**
	* Returns the number of project steps where projectId = &#63; and projectDomainId = &#63; and projectPhaseId = &#63;.
	*
	* @param projectId the project ID
	* @param projectDomainId the project domain ID
	* @param projectPhaseId the project phase ID
	* @return the number of matching project steps
	*/
	public int countByProjectIdProjectDomainIdProjectPhaseId(long projectId,
		long projectDomainId, long projectPhaseId);

	/**
	* Caches the project step in the entity cache if it is enabled.
	*
	* @param projectStep the project step
	*/
	public void cacheResult(ProjectStep projectStep);

	/**
	* Caches the project steps in the entity cache if it is enabled.
	*
	* @param projectSteps the project steps
	*/
	public void cacheResult(java.util.List<ProjectStep> projectSteps);

	/**
	* Creates a new project step with the primary key. Does not add the project step to the database.
	*
	* @param projectStepId the primary key for the new project step
	* @return the new project step
	*/
	public ProjectStep create(long projectStepId);

	/**
	* Removes the project step with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param projectStepId the primary key of the project step
	* @return the project step that was removed
	* @throws NoSuchProjectStepException if a project step with the primary key could not be found
	*/
	public ProjectStep remove(long projectStepId)
		throws NoSuchProjectStepException;

	public ProjectStep updateImpl(ProjectStep projectStep);

	/**
	* Returns the project step with the primary key or throws a {@link NoSuchProjectStepException} if it could not be found.
	*
	* @param projectStepId the primary key of the project step
	* @return the project step
	* @throws NoSuchProjectStepException if a project step with the primary key could not be found
	*/
	public ProjectStep findByPrimaryKey(long projectStepId)
		throws NoSuchProjectStepException;

	/**
	* Returns the project step with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param projectStepId the primary key of the project step
	* @return the project step, or <code>null</code> if a project step with the primary key could not be found
	*/
	public ProjectStep fetchByPrimaryKey(long projectStepId);

	@Override
	public java.util.Map<java.io.Serializable, ProjectStep> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the project steps.
	*
	* @return the project steps
	*/
	public java.util.List<ProjectStep> findAll();

	/**
	* Returns a range of all the project steps.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @return the range of project steps
	*/
	public java.util.List<ProjectStep> findAll(int start, int end);

	/**
	* Returns an ordered range of all the project steps.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of project steps
	*/
	public java.util.List<ProjectStep> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator);

	/**
	* Returns an ordered range of all the project steps.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStepModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project steps
	* @param end the upper bound of the range of project steps (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of project steps
	*/
	public java.util.List<ProjectStep> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStep> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the project steps from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of project steps.
	*
	* @return the number of project steps
	*/
	public int countAll();

	@Override
	public java.util.Set<java.lang.String> getBadColumnNames();
}