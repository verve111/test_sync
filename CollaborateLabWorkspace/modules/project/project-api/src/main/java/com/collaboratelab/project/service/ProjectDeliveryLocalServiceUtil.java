/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for ProjectDelivery. This utility wraps
 * {@link com.collaboratelab.project.service.impl.ProjectDeliveryLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Claudio Carlenzoli
 * @see ProjectDeliveryLocalService
 * @see com.collaboratelab.project.service.base.ProjectDeliveryLocalServiceBaseImpl
 * @see com.collaboratelab.project.service.impl.ProjectDeliveryLocalServiceImpl
 * @generated
 */
@ProviderType
public class ProjectDeliveryLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.collaboratelab.project.service.impl.ProjectDeliveryLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the project delivery to the database. Also notifies the appropriate model listeners.
	*
	* @param projectDelivery the project delivery
	* @return the project delivery that was added
	*/
	public static com.collaboratelab.project.model.ProjectDelivery addProjectDelivery(
		com.collaboratelab.project.model.ProjectDelivery projectDelivery) {
		return getService().addProjectDelivery(projectDelivery);
	}

	/**
	* Creates a new project delivery with the primary key. Does not add the project delivery to the database.
	*
	* @param deliveryId the primary key for the new project delivery
	* @return the new project delivery
	*/
	public static com.collaboratelab.project.model.ProjectDelivery createProjectDelivery(
		long deliveryId) {
		return getService().createProjectDelivery(deliveryId);
	}

	/**
	* Deletes the project delivery from the database. Also notifies the appropriate model listeners.
	*
	* @param projectDelivery the project delivery
	* @return the project delivery that was removed
	*/
	public static com.collaboratelab.project.model.ProjectDelivery deleteProjectDelivery(
		com.collaboratelab.project.model.ProjectDelivery projectDelivery) {
		return getService().deleteProjectDelivery(projectDelivery);
	}

	/**
	* Deletes the project delivery with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param deliveryId the primary key of the project delivery
	* @return the project delivery that was removed
	* @throws PortalException if a project delivery with the primary key could not be found
	*/
	public static com.collaboratelab.project.model.ProjectDelivery deleteProjectDelivery(
		long deliveryId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteProjectDelivery(deliveryId);
	}

	public static com.collaboratelab.project.model.ProjectDelivery fetchProjectDelivery(
		long deliveryId) {
		return getService().fetchProjectDelivery(deliveryId);
	}

	/**
	* Returns the project delivery with the matching UUID and company.
	*
	* @param uuid the project delivery's UUID
	* @param companyId the primary key of the company
	* @return the matching project delivery, or <code>null</code> if a matching project delivery could not be found
	*/
	public static com.collaboratelab.project.model.ProjectDelivery fetchProjectDeliveryByUuidAndCompanyId(
		java.lang.String uuid, long companyId) {
		return getService()
				   .fetchProjectDeliveryByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Returns the project delivery with the primary key.
	*
	* @param deliveryId the primary key of the project delivery
	* @return the project delivery
	* @throws PortalException if a project delivery with the primary key could not be found
	*/
	public static com.collaboratelab.project.model.ProjectDelivery getProjectDelivery(
		long deliveryId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getProjectDelivery(deliveryId);
	}

	/**
	* Returns the project delivery with the matching UUID and company.
	*
	* @param uuid the project delivery's UUID
	* @param companyId the primary key of the company
	* @return the matching project delivery
	* @throws PortalException if a matching project delivery could not be found
	*/
	public static com.collaboratelab.project.model.ProjectDelivery getProjectDeliveryByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getProjectDeliveryByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Updates the project delivery in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param projectDelivery the project delivery
	* @return the project delivery that was updated
	*/
	public static com.collaboratelab.project.model.ProjectDelivery updateProjectDelivery(
		com.collaboratelab.project.model.ProjectDelivery projectDelivery) {
		return getService().updateProjectDelivery(projectDelivery);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery getExportActionableDynamicQuery(
		com.liferay.exportimport.kernel.lar.PortletDataContext portletDataContext) {
		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of project deliveries.
	*
	* @return the number of project deliveries
	*/
	public static int getProjectDeliveriesCount() {
		return getService().getProjectDeliveriesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.project.model.impl.ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.project.model.impl.ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	public static java.util.List<com.collaboratelab.project.model.ProjectDelivery> findByStepId(
		long projectStepId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByStepId(projectStepId);
	}

	/**
	* Returns a range of all the project deliveries.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.project.model.impl.ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @return the range of project deliveries
	*/
	public static java.util.List<com.collaboratelab.project.model.ProjectDelivery> getProjectDeliveries(
		int start, int end) {
		return getService().getProjectDeliveries(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static ProjectDeliveryLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ProjectDeliveryLocalService, ProjectDeliveryLocalService> _serviceTracker =
		ServiceTrackerFactory.open(ProjectDeliveryLocalService.class);
}