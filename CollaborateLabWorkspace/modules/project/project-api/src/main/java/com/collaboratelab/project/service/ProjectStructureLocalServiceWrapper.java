/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ProjectStructureLocalService}.
 *
 * @author Claudio Carlenzoli
 * @see ProjectStructureLocalService
 * @generated
 */
@ProviderType
public class ProjectStructureLocalServiceWrapper
	implements ProjectStructureLocalService,
		ServiceWrapper<ProjectStructureLocalService> {
	public ProjectStructureLocalServiceWrapper(
		ProjectStructureLocalService projectStructureLocalService) {
		_projectStructureLocalService = projectStructureLocalService;
	}

	/**
	* Adds the project structure to the database. Also notifies the appropriate model listeners.
	*
	* @param projectStructure the project structure
	* @return the project structure that was added
	*/
	@Override
	public com.collaboratelab.project.model.ProjectStructure addProjectStructure(
		com.collaboratelab.project.model.ProjectStructure projectStructure) {
		return _projectStructureLocalService.addProjectStructure(projectStructure);
	}

	/**
	* Creates a new project structure with the primary key. Does not add the project structure to the database.
	*
	* @param projectStructureId the primary key for the new project structure
	* @return the new project structure
	*/
	@Override
	public com.collaboratelab.project.model.ProjectStructure createProjectStructure(
		long projectStructureId) {
		return _projectStructureLocalService.createProjectStructure(projectStructureId);
	}

	/**
	* Deletes the project structure from the database. Also notifies the appropriate model listeners.
	*
	* @param projectStructure the project structure
	* @return the project structure that was removed
	*/
	@Override
	public com.collaboratelab.project.model.ProjectStructure deleteProjectStructure(
		com.collaboratelab.project.model.ProjectStructure projectStructure) {
		return _projectStructureLocalService.deleteProjectStructure(projectStructure);
	}

	/**
	* Deletes the project structure with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param projectStructureId the primary key of the project structure
	* @return the project structure that was removed
	* @throws PortalException if a project structure with the primary key could not be found
	*/
	@Override
	public com.collaboratelab.project.model.ProjectStructure deleteProjectStructure(
		long projectStructureId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectStructureLocalService.deleteProjectStructure(projectStructureId);
	}

	@Override
	public com.collaboratelab.project.model.ProjectStructure fetchProjectStructure(
		long projectStructureId) {
		return _projectStructureLocalService.fetchProjectStructure(projectStructureId);
	}

	/**
	* Returns the project structure with the matching UUID and company.
	*
	* @param uuid the project structure's UUID
	* @param companyId the primary key of the company
	* @return the matching project structure, or <code>null</code> if a matching project structure could not be found
	*/
	@Override
	public com.collaboratelab.project.model.ProjectStructure fetchProjectStructureByUuidAndCompanyId(
		java.lang.String uuid, long companyId) {
		return _projectStructureLocalService.fetchProjectStructureByUuidAndCompanyId(uuid,
			companyId);
	}

	@Override
	public com.collaboratelab.project.model.ProjectStructure findByProjectId(
		long projectId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _projectStructureLocalService.findByProjectId(projectId);
	}

	/**
	* Returns the project structure with the primary key.
	*
	* @param projectStructureId the primary key of the project structure
	* @return the project structure
	* @throws PortalException if a project structure with the primary key could not be found
	*/
	@Override
	public com.collaboratelab.project.model.ProjectStructure getProjectStructure(
		long projectStructureId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectStructureLocalService.getProjectStructure(projectStructureId);
	}

	/**
	* Returns the project structure with the matching UUID and company.
	*
	* @param uuid the project structure's UUID
	* @param companyId the primary key of the company
	* @return the matching project structure
	* @throws PortalException if a matching project structure could not be found
	*/
	@Override
	public com.collaboratelab.project.model.ProjectStructure getProjectStructureByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectStructureLocalService.getProjectStructureByUuidAndCompanyId(uuid,
			companyId);
	}

	/**
	* Updates the project structure in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param projectStructure the project structure
	* @return the project structure that was updated
	*/
	@Override
	public com.collaboratelab.project.model.ProjectStructure updateProjectStructure(
		com.collaboratelab.project.model.ProjectStructure projectStructure) {
		return _projectStructureLocalService.updateProjectStructure(projectStructure);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _projectStructureLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _projectStructureLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery getExportActionableDynamicQuery(
		com.liferay.exportimport.kernel.lar.PortletDataContext portletDataContext) {
		return _projectStructureLocalService.getExportActionableDynamicQuery(portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _projectStructureLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectStructureLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectStructureLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of project structures.
	*
	* @return the number of project structures
	*/
	@Override
	public int getProjectStructuresCount() {
		return _projectStructureLocalService.getProjectStructuresCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _projectStructureLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _projectStructureLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.project.model.impl.ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _projectStructureLocalService.dynamicQuery(dynamicQuery, start,
			end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.project.model.impl.ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _projectStructureLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns a range of all the project structures.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.project.model.impl.ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project structures
	* @param end the upper bound of the range of project structures (not inclusive)
	* @return the range of project structures
	*/
	@Override
	public java.util.List<com.collaboratelab.project.model.ProjectStructure> getProjectStructures(
		int start, int end) {
		return _projectStructureLocalService.getProjectStructures(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _projectStructureLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _projectStructureLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public ProjectStructureLocalService getWrappedService() {
		return _projectStructureLocalService;
	}

	@Override
	public void setWrappedService(
		ProjectStructureLocalService projectStructureLocalService) {
		_projectStructureLocalService = projectStructureLocalService;
	}

	private ProjectStructureLocalService _projectStructureLocalService;
}