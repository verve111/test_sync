/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.collaboratelab.project.service.http.ProjectStructureServiceSoap}.
 *
 * @author Claudio Carlenzoli
 * @see com.collaboratelab.project.service.http.ProjectStructureServiceSoap
 * @generated
 */
@ProviderType
public class ProjectStructureSoap implements Serializable {
	public static ProjectStructureSoap toSoapModel(ProjectStructure model) {
		ProjectStructureSoap soapModel = new ProjectStructureSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setProjectStructureId(model.getProjectStructureId());
		soapModel.setProjectId(model.getProjectId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setIdentifier(model.getIdentifier());
		soapModel.setName(model.getName());
		soapModel.setDescription(model.getDescription());

		return soapModel;
	}

	public static ProjectStructureSoap[] toSoapModels(ProjectStructure[] models) {
		ProjectStructureSoap[] soapModels = new ProjectStructureSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ProjectStructureSoap[][] toSoapModels(
		ProjectStructure[][] models) {
		ProjectStructureSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ProjectStructureSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ProjectStructureSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ProjectStructureSoap[] toSoapModels(
		List<ProjectStructure> models) {
		List<ProjectStructureSoap> soapModels = new ArrayList<ProjectStructureSoap>(models.size());

		for (ProjectStructure model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ProjectStructureSoap[soapModels.size()]);
	}

	public ProjectStructureSoap() {
	}

	public long getPrimaryKey() {
		return _projectStructureId;
	}

	public void setPrimaryKey(long pk) {
		setProjectStructureId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getProjectStructureId() {
		return _projectStructureId;
	}

	public void setProjectStructureId(long projectStructureId) {
		_projectStructureId = projectStructureId;
	}

	public long getProjectId() {
		return _projectId;
	}

	public void setProjectId(long projectId) {
		_projectId = projectId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getIdentifier() {
		return _identifier;
	}

	public void setIdentifier(String identifier) {
		_identifier = identifier;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String description) {
		_description = description;
	}

	private String _uuid;
	private long _projectStructureId;
	private long _projectId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _identifier;
	private String _name;
	private String _description;
}