/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.collaboratelab.project.service.http.ProjectDeliveryServiceSoap}.
 *
 * @author Claudio Carlenzoli
 * @see com.collaboratelab.project.service.http.ProjectDeliveryServiceSoap
 * @generated
 */
@ProviderType
public class ProjectDeliverySoap implements Serializable {
	public static ProjectDeliverySoap toSoapModel(ProjectDelivery model) {
		ProjectDeliverySoap soapModel = new ProjectDeliverySoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setDeliveryId(model.getDeliveryId());
		soapModel.setStepId(model.getStepId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setName(model.getName());
		soapModel.setType(model.getType());
		soapModel.setPhase(model.getPhase());
		soapModel.setDomain(model.getDomain());
		soapModel.setStatus(model.getStatus());
		soapModel.setStartDate(model.getStartDate());
		soapModel.setEndDate(model.getEndDate());
		soapModel.setCost(model.getCost());
		soapModel.setOutcomeCategory(model.getOutcomeCategory());
		soapModel.setComment(model.getComment());
		soapModel.setIsAvailable(model.getIsAvailable());
		soapModel.setReviewerIsCompleted(model.getReviewerIsCompleted());
		soapModel.setReviewerRate(model.getReviewerRate());
		soapModel.setReviewerComment(model.getReviewerComment());
		soapModel.setFundingSource(model.getFundingSource());
		soapModel.setIsEditPlanning(model.getIsEditPlanning());
		soapModel.setBudget(model.getBudget());
		soapModel.setPersonResponsible(model.getPersonResponsible());
		soapModel.setStartPlanningDate(model.getStartPlanningDate());
		soapModel.setEndPlanningDate(model.getEndPlanningDate());

		return soapModel;
	}

	public static ProjectDeliverySoap[] toSoapModels(ProjectDelivery[] models) {
		ProjectDeliverySoap[] soapModels = new ProjectDeliverySoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ProjectDeliverySoap[][] toSoapModels(
		ProjectDelivery[][] models) {
		ProjectDeliverySoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ProjectDeliverySoap[models.length][models[0].length];
		}
		else {
			soapModels = new ProjectDeliverySoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ProjectDeliverySoap[] toSoapModels(
		List<ProjectDelivery> models) {
		List<ProjectDeliverySoap> soapModels = new ArrayList<ProjectDeliverySoap>(models.size());

		for (ProjectDelivery model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ProjectDeliverySoap[soapModels.size()]);
	}

	public ProjectDeliverySoap() {
	}

	public long getPrimaryKey() {
		return _deliveryId;
	}

	public void setPrimaryKey(long pk) {
		setDeliveryId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getDeliveryId() {
		return _deliveryId;
	}

	public void setDeliveryId(long deliveryId) {
		_deliveryId = deliveryId;
	}

	public long getStepId() {
		return _stepId;
	}

	public void setStepId(long stepId) {
		_stepId = stepId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public int getType() {
		return _type;
	}

	public void setType(int type) {
		_type = type;
	}

	public String getPhase() {
		return _phase;
	}

	public void setPhase(String phase) {
		_phase = phase;
	}

	public String getDomain() {
		return _domain;
	}

	public void setDomain(String domain) {
		_domain = domain;
	}

	public int getStatus() {
		return _status;
	}

	public void setStatus(int status) {
		_status = status;
	}

	public Date getStartDate() {
		return _startDate;
	}

	public void setStartDate(Date startDate) {
		_startDate = startDate;
	}

	public Date getEndDate() {
		return _endDate;
	}

	public void setEndDate(Date endDate) {
		_endDate = endDate;
	}

	public int getCost() {
		return _cost;
	}

	public void setCost(int cost) {
		_cost = cost;
	}

	public int getOutcomeCategory() {
		return _outcomeCategory;
	}

	public void setOutcomeCategory(int outcomeCategory) {
		_outcomeCategory = outcomeCategory;
	}

	public String getComment() {
		return _comment;
	}

	public void setComment(String comment) {
		_comment = comment;
	}

	public boolean getIsAvailable() {
		return _isAvailable;
	}

	public boolean isIsAvailable() {
		return _isAvailable;
	}

	public void setIsAvailable(boolean isAvailable) {
		_isAvailable = isAvailable;
	}

	public boolean getReviewerIsCompleted() {
		return _reviewerIsCompleted;
	}

	public boolean isReviewerIsCompleted() {
		return _reviewerIsCompleted;
	}

	public void setReviewerIsCompleted(boolean reviewerIsCompleted) {
		_reviewerIsCompleted = reviewerIsCompleted;
	}

	public int getReviewerRate() {
		return _reviewerRate;
	}

	public void setReviewerRate(int reviewerRate) {
		_reviewerRate = reviewerRate;
	}

	public String getReviewerComment() {
		return _reviewerComment;
	}

	public void setReviewerComment(String reviewerComment) {
		_reviewerComment = reviewerComment;
	}

	public int getFundingSource() {
		return _fundingSource;
	}

	public void setFundingSource(int fundingSource) {
		_fundingSource = fundingSource;
	}

	public boolean getIsEditPlanning() {
		return _isEditPlanning;
	}

	public boolean isIsEditPlanning() {
		return _isEditPlanning;
	}

	public void setIsEditPlanning(boolean isEditPlanning) {
		_isEditPlanning = isEditPlanning;
	}

	public double getBudget() {
		return _budget;
	}

	public void setBudget(double budget) {
		_budget = budget;
	}

	public String getPersonResponsible() {
		return _personResponsible;
	}

	public void setPersonResponsible(String personResponsible) {
		_personResponsible = personResponsible;
	}

	public Date getStartPlanningDate() {
		return _startPlanningDate;
	}

	public void setStartPlanningDate(Date startPlanningDate) {
		_startPlanningDate = startPlanningDate;
	}

	public Date getEndPlanningDate() {
		return _endPlanningDate;
	}

	public void setEndPlanningDate(Date endPlanningDate) {
		_endPlanningDate = endPlanningDate;
	}

	private String _uuid;
	private long _deliveryId;
	private long _stepId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _name;
	private int _type;
	private String _phase;
	private String _domain;
	private int _status;
	private Date _startDate;
	private Date _endDate;
	private int _cost;
	private int _outcomeCategory;
	private String _comment;
	private boolean _isAvailable;
	private boolean _reviewerIsCompleted;
	private int _reviewerRate;
	private String _reviewerComment;
	private int _fundingSource;
	private boolean _isEditPlanning;
	private double _budget;
	private String _personResponsible;
	private Date _startPlanningDate;
	private Date _endPlanningDate;
}