/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.project.exception.NoSuchProjectStructureException;
import com.collaboratelab.project.model.ProjectStructure;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the project structure service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Claudio Carlenzoli
 * @see com.collaboratelab.project.service.persistence.impl.ProjectStructurePersistenceImpl
 * @see ProjectStructureUtil
 * @generated
 */
@ProviderType
public interface ProjectStructurePersistence extends BasePersistence<ProjectStructure> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ProjectStructureUtil} to access the project structure persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the project structures where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching project structures
	*/
	public java.util.List<ProjectStructure> findByUuid(java.lang.String uuid);

	/**
	* Returns a range of all the project structures where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of project structures
	* @param end the upper bound of the range of project structures (not inclusive)
	* @return the range of matching project structures
	*/
	public java.util.List<ProjectStructure> findByUuid(java.lang.String uuid,
		int start, int end);

	/**
	* Returns an ordered range of all the project structures where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of project structures
	* @param end the upper bound of the range of project structures (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project structures
	*/
	public java.util.List<ProjectStructure> findByUuid(java.lang.String uuid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStructure> orderByComparator);

	/**
	* Returns an ordered range of all the project structures where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of project structures
	* @param end the upper bound of the range of project structures (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project structures
	*/
	public java.util.List<ProjectStructure> findByUuid(java.lang.String uuid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStructure> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first project structure in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project structure
	* @throws NoSuchProjectStructureException if a matching project structure could not be found
	*/
	public ProjectStructure findByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStructure> orderByComparator)
		throws NoSuchProjectStructureException;

	/**
	* Returns the first project structure in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project structure, or <code>null</code> if a matching project structure could not be found
	*/
	public ProjectStructure fetchByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStructure> orderByComparator);

	/**
	* Returns the last project structure in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project structure
	* @throws NoSuchProjectStructureException if a matching project structure could not be found
	*/
	public ProjectStructure findByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStructure> orderByComparator)
		throws NoSuchProjectStructureException;

	/**
	* Returns the last project structure in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project structure, or <code>null</code> if a matching project structure could not be found
	*/
	public ProjectStructure fetchByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStructure> orderByComparator);

	/**
	* Returns the project structures before and after the current project structure in the ordered set where uuid = &#63;.
	*
	* @param projectStructureId the primary key of the current project structure
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project structure
	* @throws NoSuchProjectStructureException if a project structure with the primary key could not be found
	*/
	public ProjectStructure[] findByUuid_PrevAndNext(long projectStructureId,
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStructure> orderByComparator)
		throws NoSuchProjectStructureException;

	/**
	* Removes all the project structures where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public void removeByUuid(java.lang.String uuid);

	/**
	* Returns the number of project structures where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching project structures
	*/
	public int countByUuid(java.lang.String uuid);

	/**
	* Returns all the project structures where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching project structures
	*/
	public java.util.List<ProjectStructure> findByUuid_C(
		java.lang.String uuid, long companyId);

	/**
	* Returns a range of all the project structures where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of project structures
	* @param end the upper bound of the range of project structures (not inclusive)
	* @return the range of matching project structures
	*/
	public java.util.List<ProjectStructure> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end);

	/**
	* Returns an ordered range of all the project structures where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of project structures
	* @param end the upper bound of the range of project structures (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project structures
	*/
	public java.util.List<ProjectStructure> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStructure> orderByComparator);

	/**
	* Returns an ordered range of all the project structures where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of project structures
	* @param end the upper bound of the range of project structures (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project structures
	*/
	public java.util.List<ProjectStructure> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStructure> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first project structure in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project structure
	* @throws NoSuchProjectStructureException if a matching project structure could not be found
	*/
	public ProjectStructure findByUuid_C_First(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStructure> orderByComparator)
		throws NoSuchProjectStructureException;

	/**
	* Returns the first project structure in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project structure, or <code>null</code> if a matching project structure could not be found
	*/
	public ProjectStructure fetchByUuid_C_First(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStructure> orderByComparator);

	/**
	* Returns the last project structure in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project structure
	* @throws NoSuchProjectStructureException if a matching project structure could not be found
	*/
	public ProjectStructure findByUuid_C_Last(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStructure> orderByComparator)
		throws NoSuchProjectStructureException;

	/**
	* Returns the last project structure in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project structure, or <code>null</code> if a matching project structure could not be found
	*/
	public ProjectStructure fetchByUuid_C_Last(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStructure> orderByComparator);

	/**
	* Returns the project structures before and after the current project structure in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param projectStructureId the primary key of the current project structure
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project structure
	* @throws NoSuchProjectStructureException if a project structure with the primary key could not be found
	*/
	public ProjectStructure[] findByUuid_C_PrevAndNext(
		long projectStructureId, java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStructure> orderByComparator)
		throws NoSuchProjectStructureException;

	/**
	* Removes all the project structures where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public void removeByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the number of project structures where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching project structures
	*/
	public int countByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns all the project structures where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching project structures
	*/
	public java.util.List<ProjectStructure> findByCompanyId(long companyId);

	/**
	* Returns a range of all the project structures where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of project structures
	* @param end the upper bound of the range of project structures (not inclusive)
	* @return the range of matching project structures
	*/
	public java.util.List<ProjectStructure> findByCompanyId(long companyId,
		int start, int end);

	/**
	* Returns an ordered range of all the project structures where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of project structures
	* @param end the upper bound of the range of project structures (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project structures
	*/
	public java.util.List<ProjectStructure> findByCompanyId(long companyId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStructure> orderByComparator);

	/**
	* Returns an ordered range of all the project structures where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of project structures
	* @param end the upper bound of the range of project structures (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project structures
	*/
	public java.util.List<ProjectStructure> findByCompanyId(long companyId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStructure> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first project structure in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project structure
	* @throws NoSuchProjectStructureException if a matching project structure could not be found
	*/
	public ProjectStructure findByCompanyId_First(long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStructure> orderByComparator)
		throws NoSuchProjectStructureException;

	/**
	* Returns the first project structure in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project structure, or <code>null</code> if a matching project structure could not be found
	*/
	public ProjectStructure fetchByCompanyId_First(long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStructure> orderByComparator);

	/**
	* Returns the last project structure in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project structure
	* @throws NoSuchProjectStructureException if a matching project structure could not be found
	*/
	public ProjectStructure findByCompanyId_Last(long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStructure> orderByComparator)
		throws NoSuchProjectStructureException;

	/**
	* Returns the last project structure in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project structure, or <code>null</code> if a matching project structure could not be found
	*/
	public ProjectStructure fetchByCompanyId_Last(long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStructure> orderByComparator);

	/**
	* Returns the project structures before and after the current project structure in the ordered set where companyId = &#63;.
	*
	* @param projectStructureId the primary key of the current project structure
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project structure
	* @throws NoSuchProjectStructureException if a project structure with the primary key could not be found
	*/
	public ProjectStructure[] findByCompanyId_PrevAndNext(
		long projectStructureId, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStructure> orderByComparator)
		throws NoSuchProjectStructureException;

	/**
	* Removes all the project structures where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	*/
	public void removeByCompanyId(long companyId);

	/**
	* Returns the number of project structures where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching project structures
	*/
	public int countByCompanyId(long companyId);

	/**
	* Returns the project structure where companyId = &#63; and identifier = &#63; or throws a {@link NoSuchProjectStructureException} if it could not be found.
	*
	* @param companyId the company ID
	* @param identifier the identifier
	* @return the matching project structure
	* @throws NoSuchProjectStructureException if a matching project structure could not be found
	*/
	public ProjectStructure findByCompanyIdIdentifier(long companyId,
		java.lang.String identifier) throws NoSuchProjectStructureException;

	/**
	* Returns the project structure where companyId = &#63; and identifier = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param companyId the company ID
	* @param identifier the identifier
	* @return the matching project structure, or <code>null</code> if a matching project structure could not be found
	*/
	public ProjectStructure fetchByCompanyIdIdentifier(long companyId,
		java.lang.String identifier);

	/**
	* Returns the project structure where companyId = &#63; and identifier = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param companyId the company ID
	* @param identifier the identifier
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project structure, or <code>null</code> if a matching project structure could not be found
	*/
	public ProjectStructure fetchByCompanyIdIdentifier(long companyId,
		java.lang.String identifier, boolean retrieveFromCache);

	/**
	* Removes the project structure where companyId = &#63; and identifier = &#63; from the database.
	*
	* @param companyId the company ID
	* @param identifier the identifier
	* @return the project structure that was removed
	*/
	public ProjectStructure removeByCompanyIdIdentifier(long companyId,
		java.lang.String identifier) throws NoSuchProjectStructureException;

	/**
	* Returns the number of project structures where companyId = &#63; and identifier = &#63;.
	*
	* @param companyId the company ID
	* @param identifier the identifier
	* @return the number of matching project structures
	*/
	public int countByCompanyIdIdentifier(long companyId,
		java.lang.String identifier);

	/**
	* Returns all the project structures where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @return the matching project structures
	*/
	public java.util.List<ProjectStructure> findByCompanyIdIncludeName(
		long companyId, java.lang.String name);

	/**
	* Returns a range of all the project structures where companyId = &#63; and name LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param name the name
	* @param start the lower bound of the range of project structures
	* @param end the upper bound of the range of project structures (not inclusive)
	* @return the range of matching project structures
	*/
	public java.util.List<ProjectStructure> findByCompanyIdIncludeName(
		long companyId, java.lang.String name, int start, int end);

	/**
	* Returns an ordered range of all the project structures where companyId = &#63; and name LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param name the name
	* @param start the lower bound of the range of project structures
	* @param end the upper bound of the range of project structures (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project structures
	*/
	public java.util.List<ProjectStructure> findByCompanyIdIncludeName(
		long companyId, java.lang.String name, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStructure> orderByComparator);

	/**
	* Returns an ordered range of all the project structures where companyId = &#63; and name LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param name the name
	* @param start the lower bound of the range of project structures
	* @param end the upper bound of the range of project structures (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project structures
	*/
	public java.util.List<ProjectStructure> findByCompanyIdIncludeName(
		long companyId, java.lang.String name, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStructure> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first project structure in the ordered set where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project structure
	* @throws NoSuchProjectStructureException if a matching project structure could not be found
	*/
	public ProjectStructure findByCompanyIdIncludeName_First(long companyId,
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStructure> orderByComparator)
		throws NoSuchProjectStructureException;

	/**
	* Returns the first project structure in the ordered set where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project structure, or <code>null</code> if a matching project structure could not be found
	*/
	public ProjectStructure fetchByCompanyIdIncludeName_First(long companyId,
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStructure> orderByComparator);

	/**
	* Returns the last project structure in the ordered set where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project structure
	* @throws NoSuchProjectStructureException if a matching project structure could not be found
	*/
	public ProjectStructure findByCompanyIdIncludeName_Last(long companyId,
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStructure> orderByComparator)
		throws NoSuchProjectStructureException;

	/**
	* Returns the last project structure in the ordered set where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project structure, or <code>null</code> if a matching project structure could not be found
	*/
	public ProjectStructure fetchByCompanyIdIncludeName_Last(long companyId,
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStructure> orderByComparator);

	/**
	* Returns the project structures before and after the current project structure in the ordered set where companyId = &#63; and name LIKE &#63;.
	*
	* @param projectStructureId the primary key of the current project structure
	* @param companyId the company ID
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project structure
	* @throws NoSuchProjectStructureException if a project structure with the primary key could not be found
	*/
	public ProjectStructure[] findByCompanyIdIncludeName_PrevAndNext(
		long projectStructureId, long companyId, java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStructure> orderByComparator)
		throws NoSuchProjectStructureException;

	/**
	* Removes all the project structures where companyId = &#63; and name LIKE &#63; from the database.
	*
	* @param companyId the company ID
	* @param name the name
	*/
	public void removeByCompanyIdIncludeName(long companyId,
		java.lang.String name);

	/**
	* Returns the number of project structures where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @return the number of matching project structures
	*/
	public int countByCompanyIdIncludeName(long companyId, java.lang.String name);

	/**
	* Returns the project structure where projectId = &#63; or throws a {@link NoSuchProjectStructureException} if it could not be found.
	*
	* @param projectId the project ID
	* @return the matching project structure
	* @throws NoSuchProjectStructureException if a matching project structure could not be found
	*/
	public ProjectStructure findByProjectId(long projectId)
		throws NoSuchProjectStructureException;

	/**
	* Returns the project structure where projectId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param projectId the project ID
	* @return the matching project structure, or <code>null</code> if a matching project structure could not be found
	*/
	public ProjectStructure fetchByProjectId(long projectId);

	/**
	* Returns the project structure where projectId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param projectId the project ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project structure, or <code>null</code> if a matching project structure could not be found
	*/
	public ProjectStructure fetchByProjectId(long projectId,
		boolean retrieveFromCache);

	/**
	* Removes the project structure where projectId = &#63; from the database.
	*
	* @param projectId the project ID
	* @return the project structure that was removed
	*/
	public ProjectStructure removeByProjectId(long projectId)
		throws NoSuchProjectStructureException;

	/**
	* Returns the number of project structures where projectId = &#63;.
	*
	* @param projectId the project ID
	* @return the number of matching project structures
	*/
	public int countByProjectId(long projectId);

	/**
	* Caches the project structure in the entity cache if it is enabled.
	*
	* @param projectStructure the project structure
	*/
	public void cacheResult(ProjectStructure projectStructure);

	/**
	* Caches the project structures in the entity cache if it is enabled.
	*
	* @param projectStructures the project structures
	*/
	public void cacheResult(java.util.List<ProjectStructure> projectStructures);

	/**
	* Creates a new project structure with the primary key. Does not add the project structure to the database.
	*
	* @param projectStructureId the primary key for the new project structure
	* @return the new project structure
	*/
	public ProjectStructure create(long projectStructureId);

	/**
	* Removes the project structure with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param projectStructureId the primary key of the project structure
	* @return the project structure that was removed
	* @throws NoSuchProjectStructureException if a project structure with the primary key could not be found
	*/
	public ProjectStructure remove(long projectStructureId)
		throws NoSuchProjectStructureException;

	public ProjectStructure updateImpl(ProjectStructure projectStructure);

	/**
	* Returns the project structure with the primary key or throws a {@link NoSuchProjectStructureException} if it could not be found.
	*
	* @param projectStructureId the primary key of the project structure
	* @return the project structure
	* @throws NoSuchProjectStructureException if a project structure with the primary key could not be found
	*/
	public ProjectStructure findByPrimaryKey(long projectStructureId)
		throws NoSuchProjectStructureException;

	/**
	* Returns the project structure with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param projectStructureId the primary key of the project structure
	* @return the project structure, or <code>null</code> if a project structure with the primary key could not be found
	*/
	public ProjectStructure fetchByPrimaryKey(long projectStructureId);

	@Override
	public java.util.Map<java.io.Serializable, ProjectStructure> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the project structures.
	*
	* @return the project structures
	*/
	public java.util.List<ProjectStructure> findAll();

	/**
	* Returns a range of all the project structures.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project structures
	* @param end the upper bound of the range of project structures (not inclusive)
	* @return the range of project structures
	*/
	public java.util.List<ProjectStructure> findAll(int start, int end);

	/**
	* Returns an ordered range of all the project structures.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project structures
	* @param end the upper bound of the range of project structures (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of project structures
	*/
	public java.util.List<ProjectStructure> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStructure> orderByComparator);

	/**
	* Returns an ordered range of all the project structures.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectStructureModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project structures
	* @param end the upper bound of the range of project structures (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of project structures
	*/
	public java.util.List<ProjectStructure> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectStructure> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the project structures from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of project structures.
	*
	* @return the number of project structures
	*/
	public int countAll();

	@Override
	public java.util.Set<java.lang.String> getBadColumnNames();
}