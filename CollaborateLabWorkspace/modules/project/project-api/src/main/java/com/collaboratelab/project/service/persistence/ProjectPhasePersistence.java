/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.project.exception.NoSuchProjectPhaseException;
import com.collaboratelab.project.model.ProjectPhase;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the project phase service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Claudio Carlenzoli
 * @see com.collaboratelab.project.service.persistence.impl.ProjectPhasePersistenceImpl
 * @see ProjectPhaseUtil
 * @generated
 */
@ProviderType
public interface ProjectPhasePersistence extends BasePersistence<ProjectPhase> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ProjectPhaseUtil} to access the project phase persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the project phases where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching project phases
	*/
	public java.util.List<ProjectPhase> findByUuid(java.lang.String uuid);

	/**
	* Returns a range of all the project phases where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of project phases
	* @param end the upper bound of the range of project phases (not inclusive)
	* @return the range of matching project phases
	*/
	public java.util.List<ProjectPhase> findByUuid(java.lang.String uuid,
		int start, int end);

	/**
	* Returns an ordered range of all the project phases where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of project phases
	* @param end the upper bound of the range of project phases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project phases
	*/
	public java.util.List<ProjectPhase> findByUuid(java.lang.String uuid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectPhase> orderByComparator);

	/**
	* Returns an ordered range of all the project phases where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of project phases
	* @param end the upper bound of the range of project phases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project phases
	*/
	public java.util.List<ProjectPhase> findByUuid(java.lang.String uuid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectPhase> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first project phase in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project phase
	* @throws NoSuchProjectPhaseException if a matching project phase could not be found
	*/
	public ProjectPhase findByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectPhase> orderByComparator)
		throws NoSuchProjectPhaseException;

	/**
	* Returns the first project phase in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project phase, or <code>null</code> if a matching project phase could not be found
	*/
	public ProjectPhase fetchByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectPhase> orderByComparator);

	/**
	* Returns the last project phase in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project phase
	* @throws NoSuchProjectPhaseException if a matching project phase could not be found
	*/
	public ProjectPhase findByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectPhase> orderByComparator)
		throws NoSuchProjectPhaseException;

	/**
	* Returns the last project phase in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project phase, or <code>null</code> if a matching project phase could not be found
	*/
	public ProjectPhase fetchByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectPhase> orderByComparator);

	/**
	* Returns the project phases before and after the current project phase in the ordered set where uuid = &#63;.
	*
	* @param projectPhaseId the primary key of the current project phase
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project phase
	* @throws NoSuchProjectPhaseException if a project phase with the primary key could not be found
	*/
	public ProjectPhase[] findByUuid_PrevAndNext(long projectPhaseId,
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectPhase> orderByComparator)
		throws NoSuchProjectPhaseException;

	/**
	* Removes all the project phases where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public void removeByUuid(java.lang.String uuid);

	/**
	* Returns the number of project phases where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching project phases
	*/
	public int countByUuid(java.lang.String uuid);

	/**
	* Returns all the project phases where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching project phases
	*/
	public java.util.List<ProjectPhase> findByUuid_C(java.lang.String uuid,
		long companyId);

	/**
	* Returns a range of all the project phases where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of project phases
	* @param end the upper bound of the range of project phases (not inclusive)
	* @return the range of matching project phases
	*/
	public java.util.List<ProjectPhase> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end);

	/**
	* Returns an ordered range of all the project phases where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of project phases
	* @param end the upper bound of the range of project phases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project phases
	*/
	public java.util.List<ProjectPhase> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectPhase> orderByComparator);

	/**
	* Returns an ordered range of all the project phases where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of project phases
	* @param end the upper bound of the range of project phases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project phases
	*/
	public java.util.List<ProjectPhase> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectPhase> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first project phase in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project phase
	* @throws NoSuchProjectPhaseException if a matching project phase could not be found
	*/
	public ProjectPhase findByUuid_C_First(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectPhase> orderByComparator)
		throws NoSuchProjectPhaseException;

	/**
	* Returns the first project phase in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project phase, or <code>null</code> if a matching project phase could not be found
	*/
	public ProjectPhase fetchByUuid_C_First(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectPhase> orderByComparator);

	/**
	* Returns the last project phase in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project phase
	* @throws NoSuchProjectPhaseException if a matching project phase could not be found
	*/
	public ProjectPhase findByUuid_C_Last(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectPhase> orderByComparator)
		throws NoSuchProjectPhaseException;

	/**
	* Returns the last project phase in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project phase, or <code>null</code> if a matching project phase could not be found
	*/
	public ProjectPhase fetchByUuid_C_Last(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectPhase> orderByComparator);

	/**
	* Returns the project phases before and after the current project phase in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param projectPhaseId the primary key of the current project phase
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project phase
	* @throws NoSuchProjectPhaseException if a project phase with the primary key could not be found
	*/
	public ProjectPhase[] findByUuid_C_PrevAndNext(long projectPhaseId,
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectPhase> orderByComparator)
		throws NoSuchProjectPhaseException;

	/**
	* Removes all the project phases where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public void removeByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the number of project phases where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching project phases
	*/
	public int countByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns all the project phases where projectStructureId = &#63;.
	*
	* @param projectStructureId the project structure ID
	* @return the matching project phases
	*/
	public java.util.List<ProjectPhase> findByProjectStructureId(
		long projectStructureId);

	/**
	* Returns a range of all the project phases where projectStructureId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectStructureId the project structure ID
	* @param start the lower bound of the range of project phases
	* @param end the upper bound of the range of project phases (not inclusive)
	* @return the range of matching project phases
	*/
	public java.util.List<ProjectPhase> findByProjectStructureId(
		long projectStructureId, int start, int end);

	/**
	* Returns an ordered range of all the project phases where projectStructureId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectStructureId the project structure ID
	* @param start the lower bound of the range of project phases
	* @param end the upper bound of the range of project phases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project phases
	*/
	public java.util.List<ProjectPhase> findByProjectStructureId(
		long projectStructureId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectPhase> orderByComparator);

	/**
	* Returns an ordered range of all the project phases where projectStructureId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectStructureId the project structure ID
	* @param start the lower bound of the range of project phases
	* @param end the upper bound of the range of project phases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project phases
	*/
	public java.util.List<ProjectPhase> findByProjectStructureId(
		long projectStructureId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectPhase> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first project phase in the ordered set where projectStructureId = &#63;.
	*
	* @param projectStructureId the project structure ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project phase
	* @throws NoSuchProjectPhaseException if a matching project phase could not be found
	*/
	public ProjectPhase findByProjectStructureId_First(
		long projectStructureId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectPhase> orderByComparator)
		throws NoSuchProjectPhaseException;

	/**
	* Returns the first project phase in the ordered set where projectStructureId = &#63;.
	*
	* @param projectStructureId the project structure ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project phase, or <code>null</code> if a matching project phase could not be found
	*/
	public ProjectPhase fetchByProjectStructureId_First(
		long projectStructureId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectPhase> orderByComparator);

	/**
	* Returns the last project phase in the ordered set where projectStructureId = &#63;.
	*
	* @param projectStructureId the project structure ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project phase
	* @throws NoSuchProjectPhaseException if a matching project phase could not be found
	*/
	public ProjectPhase findByProjectStructureId_Last(long projectStructureId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectPhase> orderByComparator)
		throws NoSuchProjectPhaseException;

	/**
	* Returns the last project phase in the ordered set where projectStructureId = &#63;.
	*
	* @param projectStructureId the project structure ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project phase, or <code>null</code> if a matching project phase could not be found
	*/
	public ProjectPhase fetchByProjectStructureId_Last(
		long projectStructureId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectPhase> orderByComparator);

	/**
	* Returns the project phases before and after the current project phase in the ordered set where projectStructureId = &#63;.
	*
	* @param projectPhaseId the primary key of the current project phase
	* @param projectStructureId the project structure ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project phase
	* @throws NoSuchProjectPhaseException if a project phase with the primary key could not be found
	*/
	public ProjectPhase[] findByProjectStructureId_PrevAndNext(
		long projectPhaseId, long projectStructureId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectPhase> orderByComparator)
		throws NoSuchProjectPhaseException;

	/**
	* Removes all the project phases where projectStructureId = &#63; from the database.
	*
	* @param projectStructureId the project structure ID
	*/
	public void removeByProjectStructureId(long projectStructureId);

	/**
	* Returns the number of project phases where projectStructureId = &#63;.
	*
	* @param projectStructureId the project structure ID
	* @return the number of matching project phases
	*/
	public int countByProjectStructureId(long projectStructureId);

	/**
	* Returns the project phase where companyId = &#63; and identifier = &#63; or throws a {@link NoSuchProjectPhaseException} if it could not be found.
	*
	* @param companyId the company ID
	* @param identifier the identifier
	* @return the matching project phase
	* @throws NoSuchProjectPhaseException if a matching project phase could not be found
	*/
	public ProjectPhase findByCompanyIdIdentifier(long companyId,
		java.lang.String identifier) throws NoSuchProjectPhaseException;

	/**
	* Returns the project phase where companyId = &#63; and identifier = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param companyId the company ID
	* @param identifier the identifier
	* @return the matching project phase, or <code>null</code> if a matching project phase could not be found
	*/
	public ProjectPhase fetchByCompanyIdIdentifier(long companyId,
		java.lang.String identifier);

	/**
	* Returns the project phase where companyId = &#63; and identifier = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param companyId the company ID
	* @param identifier the identifier
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project phase, or <code>null</code> if a matching project phase could not be found
	*/
	public ProjectPhase fetchByCompanyIdIdentifier(long companyId,
		java.lang.String identifier, boolean retrieveFromCache);

	/**
	* Removes the project phase where companyId = &#63; and identifier = &#63; from the database.
	*
	* @param companyId the company ID
	* @param identifier the identifier
	* @return the project phase that was removed
	*/
	public ProjectPhase removeByCompanyIdIdentifier(long companyId,
		java.lang.String identifier) throws NoSuchProjectPhaseException;

	/**
	* Returns the number of project phases where companyId = &#63; and identifier = &#63;.
	*
	* @param companyId the company ID
	* @param identifier the identifier
	* @return the number of matching project phases
	*/
	public int countByCompanyIdIdentifier(long companyId,
		java.lang.String identifier);

	/**
	* Returns the project phase where projectStructureId = &#63; and identifier = &#63; or throws a {@link NoSuchProjectPhaseException} if it could not be found.
	*
	* @param projectStructureId the project structure ID
	* @param identifier the identifier
	* @return the matching project phase
	* @throws NoSuchProjectPhaseException if a matching project phase could not be found
	*/
	public ProjectPhase findByProjectStructureIdIdentifier(
		long projectStructureId, java.lang.String identifier)
		throws NoSuchProjectPhaseException;

	/**
	* Returns the project phase where projectStructureId = &#63; and identifier = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param projectStructureId the project structure ID
	* @param identifier the identifier
	* @return the matching project phase, or <code>null</code> if a matching project phase could not be found
	*/
	public ProjectPhase fetchByProjectStructureIdIdentifier(
		long projectStructureId, java.lang.String identifier);

	/**
	* Returns the project phase where projectStructureId = &#63; and identifier = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param projectStructureId the project structure ID
	* @param identifier the identifier
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project phase, or <code>null</code> if a matching project phase could not be found
	*/
	public ProjectPhase fetchByProjectStructureIdIdentifier(
		long projectStructureId, java.lang.String identifier,
		boolean retrieveFromCache);

	/**
	* Removes the project phase where projectStructureId = &#63; and identifier = &#63; from the database.
	*
	* @param projectStructureId the project structure ID
	* @param identifier the identifier
	* @return the project phase that was removed
	*/
	public ProjectPhase removeByProjectStructureIdIdentifier(
		long projectStructureId, java.lang.String identifier)
		throws NoSuchProjectPhaseException;

	/**
	* Returns the number of project phases where projectStructureId = &#63; and identifier = &#63;.
	*
	* @param projectStructureId the project structure ID
	* @param identifier the identifier
	* @return the number of matching project phases
	*/
	public int countByProjectStructureIdIdentifier(long projectStructureId,
		java.lang.String identifier);

	/**
	* Returns the project phase where projectPhaseId = &#63; or throws a {@link NoSuchProjectPhaseException} if it could not be found.
	*
	* @param projectPhaseId the project phase ID
	* @return the matching project phase
	* @throws NoSuchProjectPhaseException if a matching project phase could not be found
	*/
	public ProjectPhase findByProjectPhaseId(long projectPhaseId)
		throws NoSuchProjectPhaseException;

	/**
	* Returns the project phase where projectPhaseId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param projectPhaseId the project phase ID
	* @return the matching project phase, or <code>null</code> if a matching project phase could not be found
	*/
	public ProjectPhase fetchByProjectPhaseId(long projectPhaseId);

	/**
	* Returns the project phase where projectPhaseId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param projectPhaseId the project phase ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project phase, or <code>null</code> if a matching project phase could not be found
	*/
	public ProjectPhase fetchByProjectPhaseId(long projectPhaseId,
		boolean retrieveFromCache);

	/**
	* Removes the project phase where projectPhaseId = &#63; from the database.
	*
	* @param projectPhaseId the project phase ID
	* @return the project phase that was removed
	*/
	public ProjectPhase removeByProjectPhaseId(long projectPhaseId)
		throws NoSuchProjectPhaseException;

	/**
	* Returns the number of project phases where projectPhaseId = &#63;.
	*
	* @param projectPhaseId the project phase ID
	* @return the number of matching project phases
	*/
	public int countByProjectPhaseId(long projectPhaseId);

	/**
	* Caches the project phase in the entity cache if it is enabled.
	*
	* @param projectPhase the project phase
	*/
	public void cacheResult(ProjectPhase projectPhase);

	/**
	* Caches the project phases in the entity cache if it is enabled.
	*
	* @param projectPhases the project phases
	*/
	public void cacheResult(java.util.List<ProjectPhase> projectPhases);

	/**
	* Creates a new project phase with the primary key. Does not add the project phase to the database.
	*
	* @param projectPhaseId the primary key for the new project phase
	* @return the new project phase
	*/
	public ProjectPhase create(long projectPhaseId);

	/**
	* Removes the project phase with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param projectPhaseId the primary key of the project phase
	* @return the project phase that was removed
	* @throws NoSuchProjectPhaseException if a project phase with the primary key could not be found
	*/
	public ProjectPhase remove(long projectPhaseId)
		throws NoSuchProjectPhaseException;

	public ProjectPhase updateImpl(ProjectPhase projectPhase);

	/**
	* Returns the project phase with the primary key or throws a {@link NoSuchProjectPhaseException} if it could not be found.
	*
	* @param projectPhaseId the primary key of the project phase
	* @return the project phase
	* @throws NoSuchProjectPhaseException if a project phase with the primary key could not be found
	*/
	public ProjectPhase findByPrimaryKey(long projectPhaseId)
		throws NoSuchProjectPhaseException;

	/**
	* Returns the project phase with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param projectPhaseId the primary key of the project phase
	* @return the project phase, or <code>null</code> if a project phase with the primary key could not be found
	*/
	public ProjectPhase fetchByPrimaryKey(long projectPhaseId);

	@Override
	public java.util.Map<java.io.Serializable, ProjectPhase> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the project phases.
	*
	* @return the project phases
	*/
	public java.util.List<ProjectPhase> findAll();

	/**
	* Returns a range of all the project phases.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project phases
	* @param end the upper bound of the range of project phases (not inclusive)
	* @return the range of project phases
	*/
	public java.util.List<ProjectPhase> findAll(int start, int end);

	/**
	* Returns an ordered range of all the project phases.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project phases
	* @param end the upper bound of the range of project phases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of project phases
	*/
	public java.util.List<ProjectPhase> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectPhase> orderByComparator);

	/**
	* Returns an ordered range of all the project phases.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project phases
	* @param end the upper bound of the range of project phases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of project phases
	*/
	public java.util.List<ProjectPhase> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectPhase> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the project phases from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of project phases.
	*
	* @return the number of project phases
	*/
	public int countAll();

	@Override
	public java.util.Set<java.lang.String> getBadColumnNames();
}