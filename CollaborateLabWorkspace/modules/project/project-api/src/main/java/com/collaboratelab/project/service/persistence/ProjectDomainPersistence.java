/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.project.exception.NoSuchProjectDomainException;
import com.collaboratelab.project.model.ProjectDomain;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the project domain service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Claudio Carlenzoli
 * @see com.collaboratelab.project.service.persistence.impl.ProjectDomainPersistenceImpl
 * @see ProjectDomainUtil
 * @generated
 */
@ProviderType
public interface ProjectDomainPersistence extends BasePersistence<ProjectDomain> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ProjectDomainUtil} to access the project domain persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the project domains where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching project domains
	*/
	public java.util.List<ProjectDomain> findByUuid(java.lang.String uuid);

	/**
	* Returns a range of all the project domains where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @return the range of matching project domains
	*/
	public java.util.List<ProjectDomain> findByUuid(java.lang.String uuid,
		int start, int end);

	/**
	* Returns an ordered range of all the project domains where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project domains
	*/
	public java.util.List<ProjectDomain> findByUuid(java.lang.String uuid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator);

	/**
	* Returns an ordered range of all the project domains where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project domains
	*/
	public java.util.List<ProjectDomain> findByUuid(java.lang.String uuid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first project domain in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project domain
	* @throws NoSuchProjectDomainException if a matching project domain could not be found
	*/
	public ProjectDomain findByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException;

	/**
	* Returns the first project domain in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public ProjectDomain fetchByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator);

	/**
	* Returns the last project domain in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project domain
	* @throws NoSuchProjectDomainException if a matching project domain could not be found
	*/
	public ProjectDomain findByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException;

	/**
	* Returns the last project domain in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public ProjectDomain fetchByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator);

	/**
	* Returns the project domains before and after the current project domain in the ordered set where uuid = &#63;.
	*
	* @param projectDomainId the primary key of the current project domain
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project domain
	* @throws NoSuchProjectDomainException if a project domain with the primary key could not be found
	*/
	public ProjectDomain[] findByUuid_PrevAndNext(long projectDomainId,
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException;

	/**
	* Removes all the project domains where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public void removeByUuid(java.lang.String uuid);

	/**
	* Returns the number of project domains where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching project domains
	*/
	public int countByUuid(java.lang.String uuid);

	/**
	* Returns all the project domains where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching project domains
	*/
	public java.util.List<ProjectDomain> findByUuid_C(java.lang.String uuid,
		long companyId);

	/**
	* Returns a range of all the project domains where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @return the range of matching project domains
	*/
	public java.util.List<ProjectDomain> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end);

	/**
	* Returns an ordered range of all the project domains where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project domains
	*/
	public java.util.List<ProjectDomain> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator);

	/**
	* Returns an ordered range of all the project domains where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project domains
	*/
	public java.util.List<ProjectDomain> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first project domain in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project domain
	* @throws NoSuchProjectDomainException if a matching project domain could not be found
	*/
	public ProjectDomain findByUuid_C_First(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException;

	/**
	* Returns the first project domain in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public ProjectDomain fetchByUuid_C_First(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator);

	/**
	* Returns the last project domain in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project domain
	* @throws NoSuchProjectDomainException if a matching project domain could not be found
	*/
	public ProjectDomain findByUuid_C_Last(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException;

	/**
	* Returns the last project domain in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public ProjectDomain fetchByUuid_C_Last(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator);

	/**
	* Returns the project domains before and after the current project domain in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param projectDomainId the primary key of the current project domain
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project domain
	* @throws NoSuchProjectDomainException if a project domain with the primary key could not be found
	*/
	public ProjectDomain[] findByUuid_C_PrevAndNext(long projectDomainId,
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException;

	/**
	* Removes all the project domains where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public void removeByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the number of project domains where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching project domains
	*/
	public int countByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns all the project domains where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching project domains
	*/
	public java.util.List<ProjectDomain> findByCompanyId(long companyId);

	/**
	* Returns a range of all the project domains where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @return the range of matching project domains
	*/
	public java.util.List<ProjectDomain> findByCompanyId(long companyId,
		int start, int end);

	/**
	* Returns an ordered range of all the project domains where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project domains
	*/
	public java.util.List<ProjectDomain> findByCompanyId(long companyId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator);

	/**
	* Returns an ordered range of all the project domains where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project domains
	*/
	public java.util.List<ProjectDomain> findByCompanyId(long companyId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first project domain in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project domain
	* @throws NoSuchProjectDomainException if a matching project domain could not be found
	*/
	public ProjectDomain findByCompanyId_First(long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException;

	/**
	* Returns the first project domain in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public ProjectDomain fetchByCompanyId_First(long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator);

	/**
	* Returns the last project domain in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project domain
	* @throws NoSuchProjectDomainException if a matching project domain could not be found
	*/
	public ProjectDomain findByCompanyId_Last(long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException;

	/**
	* Returns the last project domain in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public ProjectDomain fetchByCompanyId_Last(long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator);

	/**
	* Returns the project domains before and after the current project domain in the ordered set where companyId = &#63;.
	*
	* @param projectDomainId the primary key of the current project domain
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project domain
	* @throws NoSuchProjectDomainException if a project domain with the primary key could not be found
	*/
	public ProjectDomain[] findByCompanyId_PrevAndNext(long projectDomainId,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException;

	/**
	* Removes all the project domains where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	*/
	public void removeByCompanyId(long companyId);

	/**
	* Returns the number of project domains where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching project domains
	*/
	public int countByCompanyId(long companyId);

	/**
	* Returns the project domain where companyId = &#63; and identifier = &#63; or throws a {@link NoSuchProjectDomainException} if it could not be found.
	*
	* @param companyId the company ID
	* @param identifier the identifier
	* @return the matching project domain
	* @throws NoSuchProjectDomainException if a matching project domain could not be found
	*/
	public ProjectDomain findByCompanyIdIdentifier(long companyId,
		java.lang.String identifier) throws NoSuchProjectDomainException;

	/**
	* Returns the project domain where companyId = &#63; and identifier = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param companyId the company ID
	* @param identifier the identifier
	* @return the matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public ProjectDomain fetchByCompanyIdIdentifier(long companyId,
		java.lang.String identifier);

	/**
	* Returns the project domain where companyId = &#63; and identifier = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param companyId the company ID
	* @param identifier the identifier
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public ProjectDomain fetchByCompanyIdIdentifier(long companyId,
		java.lang.String identifier, boolean retrieveFromCache);

	/**
	* Removes the project domain where companyId = &#63; and identifier = &#63; from the database.
	*
	* @param companyId the company ID
	* @param identifier the identifier
	* @return the project domain that was removed
	*/
	public ProjectDomain removeByCompanyIdIdentifier(long companyId,
		java.lang.String identifier) throws NoSuchProjectDomainException;

	/**
	* Returns the number of project domains where companyId = &#63; and identifier = &#63;.
	*
	* @param companyId the company ID
	* @param identifier the identifier
	* @return the number of matching project domains
	*/
	public int countByCompanyIdIdentifier(long companyId,
		java.lang.String identifier);

	/**
	* Returns all the project domains where companyId = &#63; and projectStructureId = &#63;.
	*
	* @param companyId the company ID
	* @param projectStructureId the project structure ID
	* @return the matching project domains
	*/
	public java.util.List<ProjectDomain> findByCompanyIdProjectStructureId(
		long companyId, long projectStructureId);

	/**
	* Returns a range of all the project domains where companyId = &#63; and projectStructureId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param projectStructureId the project structure ID
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @return the range of matching project domains
	*/
	public java.util.List<ProjectDomain> findByCompanyIdProjectStructureId(
		long companyId, long projectStructureId, int start, int end);

	/**
	* Returns an ordered range of all the project domains where companyId = &#63; and projectStructureId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param projectStructureId the project structure ID
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project domains
	*/
	public java.util.List<ProjectDomain> findByCompanyIdProjectStructureId(
		long companyId, long projectStructureId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator);

	/**
	* Returns an ordered range of all the project domains where companyId = &#63; and projectStructureId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param projectStructureId the project structure ID
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project domains
	*/
	public java.util.List<ProjectDomain> findByCompanyIdProjectStructureId(
		long companyId, long projectStructureId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first project domain in the ordered set where companyId = &#63; and projectStructureId = &#63;.
	*
	* @param companyId the company ID
	* @param projectStructureId the project structure ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project domain
	* @throws NoSuchProjectDomainException if a matching project domain could not be found
	*/
	public ProjectDomain findByCompanyIdProjectStructureId_First(
		long companyId, long projectStructureId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException;

	/**
	* Returns the first project domain in the ordered set where companyId = &#63; and projectStructureId = &#63;.
	*
	* @param companyId the company ID
	* @param projectStructureId the project structure ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public ProjectDomain fetchByCompanyIdProjectStructureId_First(
		long companyId, long projectStructureId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator);

	/**
	* Returns the last project domain in the ordered set where companyId = &#63; and projectStructureId = &#63;.
	*
	* @param companyId the company ID
	* @param projectStructureId the project structure ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project domain
	* @throws NoSuchProjectDomainException if a matching project domain could not be found
	*/
	public ProjectDomain findByCompanyIdProjectStructureId_Last(
		long companyId, long projectStructureId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException;

	/**
	* Returns the last project domain in the ordered set where companyId = &#63; and projectStructureId = &#63;.
	*
	* @param companyId the company ID
	* @param projectStructureId the project structure ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public ProjectDomain fetchByCompanyIdProjectStructureId_Last(
		long companyId, long projectStructureId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator);

	/**
	* Returns the project domains before and after the current project domain in the ordered set where companyId = &#63; and projectStructureId = &#63;.
	*
	* @param projectDomainId the primary key of the current project domain
	* @param companyId the company ID
	* @param projectStructureId the project structure ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project domain
	* @throws NoSuchProjectDomainException if a project domain with the primary key could not be found
	*/
	public ProjectDomain[] findByCompanyIdProjectStructureId_PrevAndNext(
		long projectDomainId, long companyId, long projectStructureId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException;

	/**
	* Removes all the project domains where companyId = &#63; and projectStructureId = &#63; from the database.
	*
	* @param companyId the company ID
	* @param projectStructureId the project structure ID
	*/
	public void removeByCompanyIdProjectStructureId(long companyId,
		long projectStructureId);

	/**
	* Returns the number of project domains where companyId = &#63; and projectStructureId = &#63;.
	*
	* @param companyId the company ID
	* @param projectStructureId the project structure ID
	* @return the number of matching project domains
	*/
	public int countByCompanyIdProjectStructureId(long companyId,
		long projectStructureId);

	/**
	* Returns all the project domains where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @return the matching project domains
	*/
	public java.util.List<ProjectDomain> findByCompanyIdIncludeName(
		long companyId, java.lang.String name);

	/**
	* Returns a range of all the project domains where companyId = &#63; and name LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param name the name
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @return the range of matching project domains
	*/
	public java.util.List<ProjectDomain> findByCompanyIdIncludeName(
		long companyId, java.lang.String name, int start, int end);

	/**
	* Returns an ordered range of all the project domains where companyId = &#63; and name LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param name the name
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project domains
	*/
	public java.util.List<ProjectDomain> findByCompanyIdIncludeName(
		long companyId, java.lang.String name, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator);

	/**
	* Returns an ordered range of all the project domains where companyId = &#63; and name LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param name the name
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project domains
	*/
	public java.util.List<ProjectDomain> findByCompanyIdIncludeName(
		long companyId, java.lang.String name, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first project domain in the ordered set where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project domain
	* @throws NoSuchProjectDomainException if a matching project domain could not be found
	*/
	public ProjectDomain findByCompanyIdIncludeName_First(long companyId,
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException;

	/**
	* Returns the first project domain in the ordered set where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public ProjectDomain fetchByCompanyIdIncludeName_First(long companyId,
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator);

	/**
	* Returns the last project domain in the ordered set where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project domain
	* @throws NoSuchProjectDomainException if a matching project domain could not be found
	*/
	public ProjectDomain findByCompanyIdIncludeName_Last(long companyId,
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException;

	/**
	* Returns the last project domain in the ordered set where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public ProjectDomain fetchByCompanyIdIncludeName_Last(long companyId,
		java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator);

	/**
	* Returns the project domains before and after the current project domain in the ordered set where companyId = &#63; and name LIKE &#63;.
	*
	* @param projectDomainId the primary key of the current project domain
	* @param companyId the company ID
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project domain
	* @throws NoSuchProjectDomainException if a project domain with the primary key could not be found
	*/
	public ProjectDomain[] findByCompanyIdIncludeName_PrevAndNext(
		long projectDomainId, long companyId, java.lang.String name,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException;

	/**
	* Removes all the project domains where companyId = &#63; and name LIKE &#63; from the database.
	*
	* @param companyId the company ID
	* @param name the name
	*/
	public void removeByCompanyIdIncludeName(long companyId,
		java.lang.String name);

	/**
	* Returns the number of project domains where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @return the number of matching project domains
	*/
	public int countByCompanyIdIncludeName(long companyId, java.lang.String name);

	/**
	* Returns all the project domains where projectStructureId = &#63;.
	*
	* @param projectStructureId the project structure ID
	* @return the matching project domains
	*/
	public java.util.List<ProjectDomain> findByProjectStructureId(
		long projectStructureId);

	/**
	* Returns a range of all the project domains where projectStructureId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectStructureId the project structure ID
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @return the range of matching project domains
	*/
	public java.util.List<ProjectDomain> findByProjectStructureId(
		long projectStructureId, int start, int end);

	/**
	* Returns an ordered range of all the project domains where projectStructureId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectStructureId the project structure ID
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project domains
	*/
	public java.util.List<ProjectDomain> findByProjectStructureId(
		long projectStructureId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator);

	/**
	* Returns an ordered range of all the project domains where projectStructureId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectStructureId the project structure ID
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project domains
	*/
	public java.util.List<ProjectDomain> findByProjectStructureId(
		long projectStructureId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first project domain in the ordered set where projectStructureId = &#63;.
	*
	* @param projectStructureId the project structure ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project domain
	* @throws NoSuchProjectDomainException if a matching project domain could not be found
	*/
	public ProjectDomain findByProjectStructureId_First(
		long projectStructureId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException;

	/**
	* Returns the first project domain in the ordered set where projectStructureId = &#63;.
	*
	* @param projectStructureId the project structure ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public ProjectDomain fetchByProjectStructureId_First(
		long projectStructureId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator);

	/**
	* Returns the last project domain in the ordered set where projectStructureId = &#63;.
	*
	* @param projectStructureId the project structure ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project domain
	* @throws NoSuchProjectDomainException if a matching project domain could not be found
	*/
	public ProjectDomain findByProjectStructureId_Last(
		long projectStructureId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException;

	/**
	* Returns the last project domain in the ordered set where projectStructureId = &#63;.
	*
	* @param projectStructureId the project structure ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public ProjectDomain fetchByProjectStructureId_Last(
		long projectStructureId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator);

	/**
	* Returns the project domains before and after the current project domain in the ordered set where projectStructureId = &#63;.
	*
	* @param projectDomainId the primary key of the current project domain
	* @param projectStructureId the project structure ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project domain
	* @throws NoSuchProjectDomainException if a project domain with the primary key could not be found
	*/
	public ProjectDomain[] findByProjectStructureId_PrevAndNext(
		long projectDomainId, long projectStructureId,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator)
		throws NoSuchProjectDomainException;

	/**
	* Removes all the project domains where projectStructureId = &#63; from the database.
	*
	* @param projectStructureId the project structure ID
	*/
	public void removeByProjectStructureId(long projectStructureId);

	/**
	* Returns the number of project domains where projectStructureId = &#63;.
	*
	* @param projectStructureId the project structure ID
	* @return the number of matching project domains
	*/
	public int countByProjectStructureId(long projectStructureId);

	/**
	* Returns the project domain where projectStructureId = &#63; and identifier = &#63; or throws a {@link NoSuchProjectDomainException} if it could not be found.
	*
	* @param projectStructureId the project structure ID
	* @param identifier the identifier
	* @return the matching project domain
	* @throws NoSuchProjectDomainException if a matching project domain could not be found
	*/
	public ProjectDomain findByProjectStructureIdIdentifier(
		long projectStructureId, java.lang.String identifier)
		throws NoSuchProjectDomainException;

	/**
	* Returns the project domain where projectStructureId = &#63; and identifier = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param projectStructureId the project structure ID
	* @param identifier the identifier
	* @return the matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public ProjectDomain fetchByProjectStructureIdIdentifier(
		long projectStructureId, java.lang.String identifier);

	/**
	* Returns the project domain where projectStructureId = &#63; and identifier = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param projectStructureId the project structure ID
	* @param identifier the identifier
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public ProjectDomain fetchByProjectStructureIdIdentifier(
		long projectStructureId, java.lang.String identifier,
		boolean retrieveFromCache);

	/**
	* Removes the project domain where projectStructureId = &#63; and identifier = &#63; from the database.
	*
	* @param projectStructureId the project structure ID
	* @param identifier the identifier
	* @return the project domain that was removed
	*/
	public ProjectDomain removeByProjectStructureIdIdentifier(
		long projectStructureId, java.lang.String identifier)
		throws NoSuchProjectDomainException;

	/**
	* Returns the number of project domains where projectStructureId = &#63; and identifier = &#63;.
	*
	* @param projectStructureId the project structure ID
	* @param identifier the identifier
	* @return the number of matching project domains
	*/
	public int countByProjectStructureIdIdentifier(long projectStructureId,
		java.lang.String identifier);

	/**
	* Returns the project domain where projectDomainId = &#63; or throws a {@link NoSuchProjectDomainException} if it could not be found.
	*
	* @param projectDomainId the project domain ID
	* @return the matching project domain
	* @throws NoSuchProjectDomainException if a matching project domain could not be found
	*/
	public ProjectDomain findByProjectDomainId(long projectDomainId)
		throws NoSuchProjectDomainException;

	/**
	* Returns the project domain where projectDomainId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param projectDomainId the project domain ID
	* @return the matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public ProjectDomain fetchByProjectDomainId(long projectDomainId);

	/**
	* Returns the project domain where projectDomainId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param projectDomainId the project domain ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public ProjectDomain fetchByProjectDomainId(long projectDomainId,
		boolean retrieveFromCache);

	/**
	* Removes the project domain where projectDomainId = &#63; from the database.
	*
	* @param projectDomainId the project domain ID
	* @return the project domain that was removed
	*/
	public ProjectDomain removeByProjectDomainId(long projectDomainId)
		throws NoSuchProjectDomainException;

	/**
	* Returns the number of project domains where projectDomainId = &#63;.
	*
	* @param projectDomainId the project domain ID
	* @return the number of matching project domains
	*/
	public int countByProjectDomainId(long projectDomainId);

	/**
	* Caches the project domain in the entity cache if it is enabled.
	*
	* @param projectDomain the project domain
	*/
	public void cacheResult(ProjectDomain projectDomain);

	/**
	* Caches the project domains in the entity cache if it is enabled.
	*
	* @param projectDomains the project domains
	*/
	public void cacheResult(java.util.List<ProjectDomain> projectDomains);

	/**
	* Creates a new project domain with the primary key. Does not add the project domain to the database.
	*
	* @param projectDomainId the primary key for the new project domain
	* @return the new project domain
	*/
	public ProjectDomain create(long projectDomainId);

	/**
	* Removes the project domain with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param projectDomainId the primary key of the project domain
	* @return the project domain that was removed
	* @throws NoSuchProjectDomainException if a project domain with the primary key could not be found
	*/
	public ProjectDomain remove(long projectDomainId)
		throws NoSuchProjectDomainException;

	public ProjectDomain updateImpl(ProjectDomain projectDomain);

	/**
	* Returns the project domain with the primary key or throws a {@link NoSuchProjectDomainException} if it could not be found.
	*
	* @param projectDomainId the primary key of the project domain
	* @return the project domain
	* @throws NoSuchProjectDomainException if a project domain with the primary key could not be found
	*/
	public ProjectDomain findByPrimaryKey(long projectDomainId)
		throws NoSuchProjectDomainException;

	/**
	* Returns the project domain with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param projectDomainId the primary key of the project domain
	* @return the project domain, or <code>null</code> if a project domain with the primary key could not be found
	*/
	public ProjectDomain fetchByPrimaryKey(long projectDomainId);

	@Override
	public java.util.Map<java.io.Serializable, ProjectDomain> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the project domains.
	*
	* @return the project domains
	*/
	public java.util.List<ProjectDomain> findAll();

	/**
	* Returns a range of all the project domains.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @return the range of project domains
	*/
	public java.util.List<ProjectDomain> findAll(int start, int end);

	/**
	* Returns an ordered range of all the project domains.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of project domains
	*/
	public java.util.List<ProjectDomain> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator);

	/**
	* Returns an ordered range of all the project domains.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of project domains
	*/
	public java.util.List<ProjectDomain> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ProjectDomain> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the project domains from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of project domains.
	*
	* @return the number of project domains
	*/
	public int countAll();

	@Override
	public java.util.Set<java.lang.String> getBadColumnNames();
}