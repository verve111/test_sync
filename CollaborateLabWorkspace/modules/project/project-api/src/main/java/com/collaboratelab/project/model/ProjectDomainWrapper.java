/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.exportimport.kernel.lar.StagedModelType;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link ProjectDomain}.
 * </p>
 *
 * @author Claudio Carlenzoli
 * @see ProjectDomain
 * @generated
 */
@ProviderType
public class ProjectDomainWrapper implements ProjectDomain,
	ModelWrapper<ProjectDomain> {
	public ProjectDomainWrapper(ProjectDomain projectDomain) {
		_projectDomain = projectDomain;
	}

	@Override
	public Class<?> getModelClass() {
		return ProjectDomain.class;
	}

	@Override
	public String getModelClassName() {
		return ProjectDomain.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("projectDomainId", getProjectDomainId());
		attributes.put("projectStructureId", getProjectStructureId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("identifier", getIdentifier());
		attributes.put("name", getName());
		attributes.put("description", getDescription());
		attributes.put("priority", getPriority());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long projectDomainId = (Long)attributes.get("projectDomainId");

		if (projectDomainId != null) {
			setProjectDomainId(projectDomainId);
		}

		Long projectStructureId = (Long)attributes.get("projectStructureId");

		if (projectStructureId != null) {
			setProjectStructureId(projectStructureId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String identifier = (String)attributes.get("identifier");

		if (identifier != null) {
			setIdentifier(identifier);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		Long priority = (Long)attributes.get("priority");

		if (priority != null) {
			setPriority(priority);
		}
	}

	@Override
	public ProjectDomain toEscapedModel() {
		return new ProjectDomainWrapper(_projectDomain.toEscapedModel());
	}

	@Override
	public ProjectDomain toUnescapedModel() {
		return new ProjectDomainWrapper(_projectDomain.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _projectDomain.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _projectDomain.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _projectDomain.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _projectDomain.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<ProjectDomain> toCacheModel() {
		return _projectDomain.toCacheModel();
	}

	@Override
	public int compareTo(ProjectDomain projectDomain) {
		return _projectDomain.compareTo(projectDomain);
	}

	@Override
	public int hashCode() {
		return _projectDomain.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _projectDomain.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new ProjectDomainWrapper((ProjectDomain)_projectDomain.clone());
	}

	/**
	* Returns the description of this project domain.
	*
	* @return the description of this project domain
	*/
	@Override
	public java.lang.String getDescription() {
		return _projectDomain.getDescription();
	}

	/**
	* Returns the identifier of this project domain.
	*
	* @return the identifier of this project domain
	*/
	@Override
	public java.lang.String getIdentifier() {
		return _projectDomain.getIdentifier();
	}

	/**
	* Returns the name of this project domain.
	*
	* @return the name of this project domain
	*/
	@Override
	public java.lang.String getName() {
		return _projectDomain.getName();
	}

	/**
	* Returns the user name of this project domain.
	*
	* @return the user name of this project domain
	*/
	@Override
	public java.lang.String getUserName() {
		return _projectDomain.getUserName();
	}

	/**
	* Returns the user uuid of this project domain.
	*
	* @return the user uuid of this project domain
	*/
	@Override
	public java.lang.String getUserUuid() {
		return _projectDomain.getUserUuid();
	}

	/**
	* Returns the uuid of this project domain.
	*
	* @return the uuid of this project domain
	*/
	@Override
	public java.lang.String getUuid() {
		return _projectDomain.getUuid();
	}

	@Override
	public java.lang.String toString() {
		return _projectDomain.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _projectDomain.toXmlString();
	}

	/**
	* Returns the create date of this project domain.
	*
	* @return the create date of this project domain
	*/
	@Override
	public Date getCreateDate() {
		return _projectDomain.getCreateDate();
	}

	/**
	* Returns the modified date of this project domain.
	*
	* @return the modified date of this project domain
	*/
	@Override
	public Date getModifiedDate() {
		return _projectDomain.getModifiedDate();
	}

	/**
	* Returns the company ID of this project domain.
	*
	* @return the company ID of this project domain
	*/
	@Override
	public long getCompanyId() {
		return _projectDomain.getCompanyId();
	}

	/**
	* Returns the primary key of this project domain.
	*
	* @return the primary key of this project domain
	*/
	@Override
	public long getPrimaryKey() {
		return _projectDomain.getPrimaryKey();
	}

	/**
	* Returns the priority of this project domain.
	*
	* @return the priority of this project domain
	*/
	@Override
	public long getPriority() {
		return _projectDomain.getPriority();
	}

	/**
	* Returns the project domain ID of this project domain.
	*
	* @return the project domain ID of this project domain
	*/
	@Override
	public long getProjectDomainId() {
		return _projectDomain.getProjectDomainId();
	}

	/**
	* Returns the project structure ID of this project domain.
	*
	* @return the project structure ID of this project domain
	*/
	@Override
	public long getProjectStructureId() {
		return _projectDomain.getProjectStructureId();
	}

	/**
	* Returns the user ID of this project domain.
	*
	* @return the user ID of this project domain
	*/
	@Override
	public long getUserId() {
		return _projectDomain.getUserId();
	}

	@Override
	public void persist() {
		_projectDomain.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_projectDomain.setCachedModel(cachedModel);
	}

	/**
	* Sets the company ID of this project domain.
	*
	* @param companyId the company ID of this project domain
	*/
	@Override
	public void setCompanyId(long companyId) {
		_projectDomain.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this project domain.
	*
	* @param createDate the create date of this project domain
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_projectDomain.setCreateDate(createDate);
	}

	/**
	* Sets the description of this project domain.
	*
	* @param description the description of this project domain
	*/
	@Override
	public void setDescription(java.lang.String description) {
		_projectDomain.setDescription(description);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_projectDomain.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_projectDomain.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_projectDomain.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the identifier of this project domain.
	*
	* @param identifier the identifier of this project domain
	*/
	@Override
	public void setIdentifier(java.lang.String identifier) {
		_projectDomain.setIdentifier(identifier);
	}

	/**
	* Sets the modified date of this project domain.
	*
	* @param modifiedDate the modified date of this project domain
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_projectDomain.setModifiedDate(modifiedDate);
	}

	/**
	* Sets the name of this project domain.
	*
	* @param name the name of this project domain
	*/
	@Override
	public void setName(java.lang.String name) {
		_projectDomain.setName(name);
	}

	@Override
	public void setNew(boolean n) {
		_projectDomain.setNew(n);
	}

	/**
	* Sets the primary key of this project domain.
	*
	* @param primaryKey the primary key of this project domain
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_projectDomain.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_projectDomain.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the priority of this project domain.
	*
	* @param priority the priority of this project domain
	*/
	@Override
	public void setPriority(long priority) {
		_projectDomain.setPriority(priority);
	}

	/**
	* Sets the project domain ID of this project domain.
	*
	* @param projectDomainId the project domain ID of this project domain
	*/
	@Override
	public void setProjectDomainId(long projectDomainId) {
		_projectDomain.setProjectDomainId(projectDomainId);
	}

	/**
	* Sets the project structure ID of this project domain.
	*
	* @param projectStructureId the project structure ID of this project domain
	*/
	@Override
	public void setProjectStructureId(long projectStructureId) {
		_projectDomain.setProjectStructureId(projectStructureId);
	}

	/**
	* Sets the user ID of this project domain.
	*
	* @param userId the user ID of this project domain
	*/
	@Override
	public void setUserId(long userId) {
		_projectDomain.setUserId(userId);
	}

	/**
	* Sets the user name of this project domain.
	*
	* @param userName the user name of this project domain
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_projectDomain.setUserName(userName);
	}

	/**
	* Sets the user uuid of this project domain.
	*
	* @param userUuid the user uuid of this project domain
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_projectDomain.setUserUuid(userUuid);
	}

	/**
	* Sets the uuid of this project domain.
	*
	* @param uuid the uuid of this project domain
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_projectDomain.setUuid(uuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ProjectDomainWrapper)) {
			return false;
		}

		ProjectDomainWrapper projectDomainWrapper = (ProjectDomainWrapper)obj;

		if (Objects.equals(_projectDomain, projectDomainWrapper._projectDomain)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _projectDomain.getStagedModelType();
	}

	@Override
	public ProjectDomain getWrappedModel() {
		return _projectDomain;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _projectDomain.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _projectDomain.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_projectDomain.resetOriginalValues();
	}

	private final ProjectDomain _projectDomain;
}