/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.project.model.ProjectPhase;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the project phase service. This utility wraps {@link com.collaboratelab.project.service.persistence.impl.ProjectPhasePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Claudio Carlenzoli
 * @see ProjectPhasePersistence
 * @see com.collaboratelab.project.service.persistence.impl.ProjectPhasePersistenceImpl
 * @generated
 */
@ProviderType
public class ProjectPhaseUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(ProjectPhase projectPhase) {
		getPersistence().clearCache(projectPhase);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<ProjectPhase> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<ProjectPhase> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<ProjectPhase> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<ProjectPhase> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static ProjectPhase update(ProjectPhase projectPhase) {
		return getPersistence().update(projectPhase);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static ProjectPhase update(ProjectPhase projectPhase,
		ServiceContext serviceContext) {
		return getPersistence().update(projectPhase, serviceContext);
	}

	/**
	* Returns all the project phases where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching project phases
	*/
	public static List<ProjectPhase> findByUuid(java.lang.String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	* Returns a range of all the project phases where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of project phases
	* @param end the upper bound of the range of project phases (not inclusive)
	* @return the range of matching project phases
	*/
	public static List<ProjectPhase> findByUuid(java.lang.String uuid,
		int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	* Returns an ordered range of all the project phases where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of project phases
	* @param end the upper bound of the range of project phases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project phases
	*/
	public static List<ProjectPhase> findByUuid(java.lang.String uuid,
		int start, int end, OrderByComparator<ProjectPhase> orderByComparator) {
		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the project phases where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of project phases
	* @param end the upper bound of the range of project phases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project phases
	*/
	public static List<ProjectPhase> findByUuid(java.lang.String uuid,
		int start, int end, OrderByComparator<ProjectPhase> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid(uuid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first project phase in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project phase
	* @throws NoSuchProjectPhaseException if a matching project phase could not be found
	*/
	public static ProjectPhase findByUuid_First(java.lang.String uuid,
		OrderByComparator<ProjectPhase> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectPhaseException {
		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the first project phase in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project phase, or <code>null</code> if a matching project phase could not be found
	*/
	public static ProjectPhase fetchByUuid_First(java.lang.String uuid,
		OrderByComparator<ProjectPhase> orderByComparator) {
		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the last project phase in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project phase
	* @throws NoSuchProjectPhaseException if a matching project phase could not be found
	*/
	public static ProjectPhase findByUuid_Last(java.lang.String uuid,
		OrderByComparator<ProjectPhase> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectPhaseException {
		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the last project phase in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project phase, or <code>null</code> if a matching project phase could not be found
	*/
	public static ProjectPhase fetchByUuid_Last(java.lang.String uuid,
		OrderByComparator<ProjectPhase> orderByComparator) {
		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the project phases before and after the current project phase in the ordered set where uuid = &#63;.
	*
	* @param projectPhaseId the primary key of the current project phase
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project phase
	* @throws NoSuchProjectPhaseException if a project phase with the primary key could not be found
	*/
	public static ProjectPhase[] findByUuid_PrevAndNext(long projectPhaseId,
		java.lang.String uuid, OrderByComparator<ProjectPhase> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectPhaseException {
		return getPersistence()
				   .findByUuid_PrevAndNext(projectPhaseId, uuid,
			orderByComparator);
	}

	/**
	* Removes all the project phases where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public static void removeByUuid(java.lang.String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	* Returns the number of project phases where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching project phases
	*/
	public static int countByUuid(java.lang.String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	* Returns all the project phases where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching project phases
	*/
	public static List<ProjectPhase> findByUuid_C(java.lang.String uuid,
		long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	* Returns a range of all the project phases where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of project phases
	* @param end the upper bound of the range of project phases (not inclusive)
	* @return the range of matching project phases
	*/
	public static List<ProjectPhase> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end) {
		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	* Returns an ordered range of all the project phases where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of project phases
	* @param end the upper bound of the range of project phases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project phases
	*/
	public static List<ProjectPhase> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<ProjectPhase> orderByComparator) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the project phases where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of project phases
	* @param end the upper bound of the range of project phases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project phases
	*/
	public static List<ProjectPhase> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<ProjectPhase> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first project phase in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project phase
	* @throws NoSuchProjectPhaseException if a matching project phase could not be found
	*/
	public static ProjectPhase findByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<ProjectPhase> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectPhaseException {
		return getPersistence()
				   .findByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the first project phase in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project phase, or <code>null</code> if a matching project phase could not be found
	*/
	public static ProjectPhase fetchByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<ProjectPhase> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last project phase in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project phase
	* @throws NoSuchProjectPhaseException if a matching project phase could not be found
	*/
	public static ProjectPhase findByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<ProjectPhase> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectPhaseException {
		return getPersistence()
				   .findByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last project phase in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project phase, or <code>null</code> if a matching project phase could not be found
	*/
	public static ProjectPhase fetchByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<ProjectPhase> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the project phases before and after the current project phase in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param projectPhaseId the primary key of the current project phase
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project phase
	* @throws NoSuchProjectPhaseException if a project phase with the primary key could not be found
	*/
	public static ProjectPhase[] findByUuid_C_PrevAndNext(long projectPhaseId,
		java.lang.String uuid, long companyId,
		OrderByComparator<ProjectPhase> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectPhaseException {
		return getPersistence()
				   .findByUuid_C_PrevAndNext(projectPhaseId, uuid, companyId,
			orderByComparator);
	}

	/**
	* Removes all the project phases where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public static void removeByUuid_C(java.lang.String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	* Returns the number of project phases where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching project phases
	*/
	public static int countByUuid_C(java.lang.String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	* Returns all the project phases where projectStructureId = &#63;.
	*
	* @param projectStructureId the project structure ID
	* @return the matching project phases
	*/
	public static List<ProjectPhase> findByProjectStructureId(
		long projectStructureId) {
		return getPersistence().findByProjectStructureId(projectStructureId);
	}

	/**
	* Returns a range of all the project phases where projectStructureId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectStructureId the project structure ID
	* @param start the lower bound of the range of project phases
	* @param end the upper bound of the range of project phases (not inclusive)
	* @return the range of matching project phases
	*/
	public static List<ProjectPhase> findByProjectStructureId(
		long projectStructureId, int start, int end) {
		return getPersistence()
				   .findByProjectStructureId(projectStructureId, start, end);
	}

	/**
	* Returns an ordered range of all the project phases where projectStructureId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectStructureId the project structure ID
	* @param start the lower bound of the range of project phases
	* @param end the upper bound of the range of project phases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project phases
	*/
	public static List<ProjectPhase> findByProjectStructureId(
		long projectStructureId, int start, int end,
		OrderByComparator<ProjectPhase> orderByComparator) {
		return getPersistence()
				   .findByProjectStructureId(projectStructureId, start, end,
			orderByComparator);
	}

	/**
	* Returns an ordered range of all the project phases where projectStructureId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectStructureId the project structure ID
	* @param start the lower bound of the range of project phases
	* @param end the upper bound of the range of project phases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project phases
	*/
	public static List<ProjectPhase> findByProjectStructureId(
		long projectStructureId, int start, int end,
		OrderByComparator<ProjectPhase> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByProjectStructureId(projectStructureId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first project phase in the ordered set where projectStructureId = &#63;.
	*
	* @param projectStructureId the project structure ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project phase
	* @throws NoSuchProjectPhaseException if a matching project phase could not be found
	*/
	public static ProjectPhase findByProjectStructureId_First(
		long projectStructureId,
		OrderByComparator<ProjectPhase> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectPhaseException {
		return getPersistence()
				   .findByProjectStructureId_First(projectStructureId,
			orderByComparator);
	}

	/**
	* Returns the first project phase in the ordered set where projectStructureId = &#63;.
	*
	* @param projectStructureId the project structure ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project phase, or <code>null</code> if a matching project phase could not be found
	*/
	public static ProjectPhase fetchByProjectStructureId_First(
		long projectStructureId,
		OrderByComparator<ProjectPhase> orderByComparator) {
		return getPersistence()
				   .fetchByProjectStructureId_First(projectStructureId,
			orderByComparator);
	}

	/**
	* Returns the last project phase in the ordered set where projectStructureId = &#63;.
	*
	* @param projectStructureId the project structure ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project phase
	* @throws NoSuchProjectPhaseException if a matching project phase could not be found
	*/
	public static ProjectPhase findByProjectStructureId_Last(
		long projectStructureId,
		OrderByComparator<ProjectPhase> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectPhaseException {
		return getPersistence()
				   .findByProjectStructureId_Last(projectStructureId,
			orderByComparator);
	}

	/**
	* Returns the last project phase in the ordered set where projectStructureId = &#63;.
	*
	* @param projectStructureId the project structure ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project phase, or <code>null</code> if a matching project phase could not be found
	*/
	public static ProjectPhase fetchByProjectStructureId_Last(
		long projectStructureId,
		OrderByComparator<ProjectPhase> orderByComparator) {
		return getPersistence()
				   .fetchByProjectStructureId_Last(projectStructureId,
			orderByComparator);
	}

	/**
	* Returns the project phases before and after the current project phase in the ordered set where projectStructureId = &#63;.
	*
	* @param projectPhaseId the primary key of the current project phase
	* @param projectStructureId the project structure ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project phase
	* @throws NoSuchProjectPhaseException if a project phase with the primary key could not be found
	*/
	public static ProjectPhase[] findByProjectStructureId_PrevAndNext(
		long projectPhaseId, long projectStructureId,
		OrderByComparator<ProjectPhase> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectPhaseException {
		return getPersistence()
				   .findByProjectStructureId_PrevAndNext(projectPhaseId,
			projectStructureId, orderByComparator);
	}

	/**
	* Removes all the project phases where projectStructureId = &#63; from the database.
	*
	* @param projectStructureId the project structure ID
	*/
	public static void removeByProjectStructureId(long projectStructureId) {
		getPersistence().removeByProjectStructureId(projectStructureId);
	}

	/**
	* Returns the number of project phases where projectStructureId = &#63;.
	*
	* @param projectStructureId the project structure ID
	* @return the number of matching project phases
	*/
	public static int countByProjectStructureId(long projectStructureId) {
		return getPersistence().countByProjectStructureId(projectStructureId);
	}

	/**
	* Returns the project phase where companyId = &#63; and identifier = &#63; or throws a {@link NoSuchProjectPhaseException} if it could not be found.
	*
	* @param companyId the company ID
	* @param identifier the identifier
	* @return the matching project phase
	* @throws NoSuchProjectPhaseException if a matching project phase could not be found
	*/
	public static ProjectPhase findByCompanyIdIdentifier(long companyId,
		java.lang.String identifier)
		throws com.collaboratelab.project.exception.NoSuchProjectPhaseException {
		return getPersistence().findByCompanyIdIdentifier(companyId, identifier);
	}

	/**
	* Returns the project phase where companyId = &#63; and identifier = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param companyId the company ID
	* @param identifier the identifier
	* @return the matching project phase, or <code>null</code> if a matching project phase could not be found
	*/
	public static ProjectPhase fetchByCompanyIdIdentifier(long companyId,
		java.lang.String identifier) {
		return getPersistence().fetchByCompanyIdIdentifier(companyId, identifier);
	}

	/**
	* Returns the project phase where companyId = &#63; and identifier = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param companyId the company ID
	* @param identifier the identifier
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project phase, or <code>null</code> if a matching project phase could not be found
	*/
	public static ProjectPhase fetchByCompanyIdIdentifier(long companyId,
		java.lang.String identifier, boolean retrieveFromCache) {
		return getPersistence()
				   .fetchByCompanyIdIdentifier(companyId, identifier,
			retrieveFromCache);
	}

	/**
	* Removes the project phase where companyId = &#63; and identifier = &#63; from the database.
	*
	* @param companyId the company ID
	* @param identifier the identifier
	* @return the project phase that was removed
	*/
	public static ProjectPhase removeByCompanyIdIdentifier(long companyId,
		java.lang.String identifier)
		throws com.collaboratelab.project.exception.NoSuchProjectPhaseException {
		return getPersistence()
				   .removeByCompanyIdIdentifier(companyId, identifier);
	}

	/**
	* Returns the number of project phases where companyId = &#63; and identifier = &#63;.
	*
	* @param companyId the company ID
	* @param identifier the identifier
	* @return the number of matching project phases
	*/
	public static int countByCompanyIdIdentifier(long companyId,
		java.lang.String identifier) {
		return getPersistence().countByCompanyIdIdentifier(companyId, identifier);
	}

	/**
	* Returns the project phase where projectStructureId = &#63; and identifier = &#63; or throws a {@link NoSuchProjectPhaseException} if it could not be found.
	*
	* @param projectStructureId the project structure ID
	* @param identifier the identifier
	* @return the matching project phase
	* @throws NoSuchProjectPhaseException if a matching project phase could not be found
	*/
	public static ProjectPhase findByProjectStructureIdIdentifier(
		long projectStructureId, java.lang.String identifier)
		throws com.collaboratelab.project.exception.NoSuchProjectPhaseException {
		return getPersistence()
				   .findByProjectStructureIdIdentifier(projectStructureId,
			identifier);
	}

	/**
	* Returns the project phase where projectStructureId = &#63; and identifier = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param projectStructureId the project structure ID
	* @param identifier the identifier
	* @return the matching project phase, or <code>null</code> if a matching project phase could not be found
	*/
	public static ProjectPhase fetchByProjectStructureIdIdentifier(
		long projectStructureId, java.lang.String identifier) {
		return getPersistence()
				   .fetchByProjectStructureIdIdentifier(projectStructureId,
			identifier);
	}

	/**
	* Returns the project phase where projectStructureId = &#63; and identifier = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param projectStructureId the project structure ID
	* @param identifier the identifier
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project phase, or <code>null</code> if a matching project phase could not be found
	*/
	public static ProjectPhase fetchByProjectStructureIdIdentifier(
		long projectStructureId, java.lang.String identifier,
		boolean retrieveFromCache) {
		return getPersistence()
				   .fetchByProjectStructureIdIdentifier(projectStructureId,
			identifier, retrieveFromCache);
	}

	/**
	* Removes the project phase where projectStructureId = &#63; and identifier = &#63; from the database.
	*
	* @param projectStructureId the project structure ID
	* @param identifier the identifier
	* @return the project phase that was removed
	*/
	public static ProjectPhase removeByProjectStructureIdIdentifier(
		long projectStructureId, java.lang.String identifier)
		throws com.collaboratelab.project.exception.NoSuchProjectPhaseException {
		return getPersistence()
				   .removeByProjectStructureIdIdentifier(projectStructureId,
			identifier);
	}

	/**
	* Returns the number of project phases where projectStructureId = &#63; and identifier = &#63;.
	*
	* @param projectStructureId the project structure ID
	* @param identifier the identifier
	* @return the number of matching project phases
	*/
	public static int countByProjectStructureIdIdentifier(
		long projectStructureId, java.lang.String identifier) {
		return getPersistence()
				   .countByProjectStructureIdIdentifier(projectStructureId,
			identifier);
	}

	/**
	* Returns the project phase where projectPhaseId = &#63; or throws a {@link NoSuchProjectPhaseException} if it could not be found.
	*
	* @param projectPhaseId the project phase ID
	* @return the matching project phase
	* @throws NoSuchProjectPhaseException if a matching project phase could not be found
	*/
	public static ProjectPhase findByProjectPhaseId(long projectPhaseId)
		throws com.collaboratelab.project.exception.NoSuchProjectPhaseException {
		return getPersistence().findByProjectPhaseId(projectPhaseId);
	}

	/**
	* Returns the project phase where projectPhaseId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param projectPhaseId the project phase ID
	* @return the matching project phase, or <code>null</code> if a matching project phase could not be found
	*/
	public static ProjectPhase fetchByProjectPhaseId(long projectPhaseId) {
		return getPersistence().fetchByProjectPhaseId(projectPhaseId);
	}

	/**
	* Returns the project phase where projectPhaseId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param projectPhaseId the project phase ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project phase, or <code>null</code> if a matching project phase could not be found
	*/
	public static ProjectPhase fetchByProjectPhaseId(long projectPhaseId,
		boolean retrieveFromCache) {
		return getPersistence()
				   .fetchByProjectPhaseId(projectPhaseId, retrieveFromCache);
	}

	/**
	* Removes the project phase where projectPhaseId = &#63; from the database.
	*
	* @param projectPhaseId the project phase ID
	* @return the project phase that was removed
	*/
	public static ProjectPhase removeByProjectPhaseId(long projectPhaseId)
		throws com.collaboratelab.project.exception.NoSuchProjectPhaseException {
		return getPersistence().removeByProjectPhaseId(projectPhaseId);
	}

	/**
	* Returns the number of project phases where projectPhaseId = &#63;.
	*
	* @param projectPhaseId the project phase ID
	* @return the number of matching project phases
	*/
	public static int countByProjectPhaseId(long projectPhaseId) {
		return getPersistence().countByProjectPhaseId(projectPhaseId);
	}

	/**
	* Caches the project phase in the entity cache if it is enabled.
	*
	* @param projectPhase the project phase
	*/
	public static void cacheResult(ProjectPhase projectPhase) {
		getPersistence().cacheResult(projectPhase);
	}

	/**
	* Caches the project phases in the entity cache if it is enabled.
	*
	* @param projectPhases the project phases
	*/
	public static void cacheResult(List<ProjectPhase> projectPhases) {
		getPersistence().cacheResult(projectPhases);
	}

	/**
	* Creates a new project phase with the primary key. Does not add the project phase to the database.
	*
	* @param projectPhaseId the primary key for the new project phase
	* @return the new project phase
	*/
	public static ProjectPhase create(long projectPhaseId) {
		return getPersistence().create(projectPhaseId);
	}

	/**
	* Removes the project phase with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param projectPhaseId the primary key of the project phase
	* @return the project phase that was removed
	* @throws NoSuchProjectPhaseException if a project phase with the primary key could not be found
	*/
	public static ProjectPhase remove(long projectPhaseId)
		throws com.collaboratelab.project.exception.NoSuchProjectPhaseException {
		return getPersistence().remove(projectPhaseId);
	}

	public static ProjectPhase updateImpl(ProjectPhase projectPhase) {
		return getPersistence().updateImpl(projectPhase);
	}

	/**
	* Returns the project phase with the primary key or throws a {@link NoSuchProjectPhaseException} if it could not be found.
	*
	* @param projectPhaseId the primary key of the project phase
	* @return the project phase
	* @throws NoSuchProjectPhaseException if a project phase with the primary key could not be found
	*/
	public static ProjectPhase findByPrimaryKey(long projectPhaseId)
		throws com.collaboratelab.project.exception.NoSuchProjectPhaseException {
		return getPersistence().findByPrimaryKey(projectPhaseId);
	}

	/**
	* Returns the project phase with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param projectPhaseId the primary key of the project phase
	* @return the project phase, or <code>null</code> if a project phase with the primary key could not be found
	*/
	public static ProjectPhase fetchByPrimaryKey(long projectPhaseId) {
		return getPersistence().fetchByPrimaryKey(projectPhaseId);
	}

	public static java.util.Map<java.io.Serializable, ProjectPhase> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the project phases.
	*
	* @return the project phases
	*/
	public static List<ProjectPhase> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the project phases.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project phases
	* @param end the upper bound of the range of project phases (not inclusive)
	* @return the range of project phases
	*/
	public static List<ProjectPhase> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the project phases.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project phases
	* @param end the upper bound of the range of project phases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of project phases
	*/
	public static List<ProjectPhase> findAll(int start, int end,
		OrderByComparator<ProjectPhase> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the project phases.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project phases
	* @param end the upper bound of the range of project phases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of project phases
	*/
	public static List<ProjectPhase> findAll(int start, int end,
		OrderByComparator<ProjectPhase> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the project phases from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of project phases.
	*
	* @return the number of project phases
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static java.util.Set<java.lang.String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static ProjectPhasePersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ProjectPhasePersistence, ProjectPhasePersistence> _serviceTracker =
		ServiceTrackerFactory.open(ProjectPhasePersistence.class);
}