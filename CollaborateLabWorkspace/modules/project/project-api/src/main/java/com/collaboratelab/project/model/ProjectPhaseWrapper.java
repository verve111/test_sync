/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.exportimport.kernel.lar.StagedModelType;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link ProjectPhase}.
 * </p>
 *
 * @author Claudio Carlenzoli
 * @see ProjectPhase
 * @generated
 */
@ProviderType
public class ProjectPhaseWrapper implements ProjectPhase,
	ModelWrapper<ProjectPhase> {
	public ProjectPhaseWrapper(ProjectPhase projectPhase) {
		_projectPhase = projectPhase;
	}

	@Override
	public Class<?> getModelClass() {
		return ProjectPhase.class;
	}

	@Override
	public String getModelClassName() {
		return ProjectPhase.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("projectPhaseId", getProjectPhaseId());
		attributes.put("projectStructureId", getProjectStructureId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("identifier", getIdentifier());
		attributes.put("name", getName());
		attributes.put("description", getDescription());
		attributes.put("priority", getPriority());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long projectPhaseId = (Long)attributes.get("projectPhaseId");

		if (projectPhaseId != null) {
			setProjectPhaseId(projectPhaseId);
		}

		Long projectStructureId = (Long)attributes.get("projectStructureId");

		if (projectStructureId != null) {
			setProjectStructureId(projectStructureId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String identifier = (String)attributes.get("identifier");

		if (identifier != null) {
			setIdentifier(identifier);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		Long priority = (Long)attributes.get("priority");

		if (priority != null) {
			setPriority(priority);
		}
	}

	@Override
	public ProjectPhase toEscapedModel() {
		return new ProjectPhaseWrapper(_projectPhase.toEscapedModel());
	}

	@Override
	public ProjectPhase toUnescapedModel() {
		return new ProjectPhaseWrapper(_projectPhase.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _projectPhase.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _projectPhase.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _projectPhase.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _projectPhase.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<ProjectPhase> toCacheModel() {
		return _projectPhase.toCacheModel();
	}

	@Override
	public int compareTo(ProjectPhase projectPhase) {
		return _projectPhase.compareTo(projectPhase);
	}

	@Override
	public int hashCode() {
		return _projectPhase.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _projectPhase.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new ProjectPhaseWrapper((ProjectPhase)_projectPhase.clone());
	}

	/**
	* Returns the description of this project phase.
	*
	* @return the description of this project phase
	*/
	@Override
	public java.lang.String getDescription() {
		return _projectPhase.getDescription();
	}

	/**
	* Returns the identifier of this project phase.
	*
	* @return the identifier of this project phase
	*/
	@Override
	public java.lang.String getIdentifier() {
		return _projectPhase.getIdentifier();
	}

	/**
	* Returns the name of this project phase.
	*
	* @return the name of this project phase
	*/
	@Override
	public java.lang.String getName() {
		return _projectPhase.getName();
	}

	/**
	* Returns the user name of this project phase.
	*
	* @return the user name of this project phase
	*/
	@Override
	public java.lang.String getUserName() {
		return _projectPhase.getUserName();
	}

	/**
	* Returns the user uuid of this project phase.
	*
	* @return the user uuid of this project phase
	*/
	@Override
	public java.lang.String getUserUuid() {
		return _projectPhase.getUserUuid();
	}

	/**
	* Returns the uuid of this project phase.
	*
	* @return the uuid of this project phase
	*/
	@Override
	public java.lang.String getUuid() {
		return _projectPhase.getUuid();
	}

	@Override
	public java.lang.String toString() {
		return _projectPhase.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _projectPhase.toXmlString();
	}

	/**
	* Returns the create date of this project phase.
	*
	* @return the create date of this project phase
	*/
	@Override
	public Date getCreateDate() {
		return _projectPhase.getCreateDate();
	}

	/**
	* Returns the modified date of this project phase.
	*
	* @return the modified date of this project phase
	*/
	@Override
	public Date getModifiedDate() {
		return _projectPhase.getModifiedDate();
	}

	/**
	* Returns the company ID of this project phase.
	*
	* @return the company ID of this project phase
	*/
	@Override
	public long getCompanyId() {
		return _projectPhase.getCompanyId();
	}

	/**
	* Returns the primary key of this project phase.
	*
	* @return the primary key of this project phase
	*/
	@Override
	public long getPrimaryKey() {
		return _projectPhase.getPrimaryKey();
	}

	/**
	* Returns the priority of this project phase.
	*
	* @return the priority of this project phase
	*/
	@Override
	public long getPriority() {
		return _projectPhase.getPriority();
	}

	/**
	* Returns the project phase ID of this project phase.
	*
	* @return the project phase ID of this project phase
	*/
	@Override
	public long getProjectPhaseId() {
		return _projectPhase.getProjectPhaseId();
	}

	/**
	* Returns the project structure ID of this project phase.
	*
	* @return the project structure ID of this project phase
	*/
	@Override
	public long getProjectStructureId() {
		return _projectPhase.getProjectStructureId();
	}

	/**
	* Returns the user ID of this project phase.
	*
	* @return the user ID of this project phase
	*/
	@Override
	public long getUserId() {
		return _projectPhase.getUserId();
	}

	@Override
	public void persist() {
		_projectPhase.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_projectPhase.setCachedModel(cachedModel);
	}

	/**
	* Sets the company ID of this project phase.
	*
	* @param companyId the company ID of this project phase
	*/
	@Override
	public void setCompanyId(long companyId) {
		_projectPhase.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this project phase.
	*
	* @param createDate the create date of this project phase
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_projectPhase.setCreateDate(createDate);
	}

	/**
	* Sets the description of this project phase.
	*
	* @param description the description of this project phase
	*/
	@Override
	public void setDescription(java.lang.String description) {
		_projectPhase.setDescription(description);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_projectPhase.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_projectPhase.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_projectPhase.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the identifier of this project phase.
	*
	* @param identifier the identifier of this project phase
	*/
	@Override
	public void setIdentifier(java.lang.String identifier) {
		_projectPhase.setIdentifier(identifier);
	}

	/**
	* Sets the modified date of this project phase.
	*
	* @param modifiedDate the modified date of this project phase
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_projectPhase.setModifiedDate(modifiedDate);
	}

	/**
	* Sets the name of this project phase.
	*
	* @param name the name of this project phase
	*/
	@Override
	public void setName(java.lang.String name) {
		_projectPhase.setName(name);
	}

	@Override
	public void setNew(boolean n) {
		_projectPhase.setNew(n);
	}

	/**
	* Sets the primary key of this project phase.
	*
	* @param primaryKey the primary key of this project phase
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_projectPhase.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_projectPhase.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the priority of this project phase.
	*
	* @param priority the priority of this project phase
	*/
	@Override
	public void setPriority(long priority) {
		_projectPhase.setPriority(priority);
	}

	/**
	* Sets the project phase ID of this project phase.
	*
	* @param projectPhaseId the project phase ID of this project phase
	*/
	@Override
	public void setProjectPhaseId(long projectPhaseId) {
		_projectPhase.setProjectPhaseId(projectPhaseId);
	}

	/**
	* Sets the project structure ID of this project phase.
	*
	* @param projectStructureId the project structure ID of this project phase
	*/
	@Override
	public void setProjectStructureId(long projectStructureId) {
		_projectPhase.setProjectStructureId(projectStructureId);
	}

	/**
	* Sets the user ID of this project phase.
	*
	* @param userId the user ID of this project phase
	*/
	@Override
	public void setUserId(long userId) {
		_projectPhase.setUserId(userId);
	}

	/**
	* Sets the user name of this project phase.
	*
	* @param userName the user name of this project phase
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_projectPhase.setUserName(userName);
	}

	/**
	* Sets the user uuid of this project phase.
	*
	* @param userUuid the user uuid of this project phase
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_projectPhase.setUserUuid(userUuid);
	}

	/**
	* Sets the uuid of this project phase.
	*
	* @param uuid the uuid of this project phase
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_projectPhase.setUuid(uuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ProjectPhaseWrapper)) {
			return false;
		}

		ProjectPhaseWrapper projectPhaseWrapper = (ProjectPhaseWrapper)obj;

		if (Objects.equals(_projectPhase, projectPhaseWrapper._projectPhase)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _projectPhase.getStagedModelType();
	}

	@Override
	public ProjectPhase getWrappedModel() {
		return _projectPhase;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _projectPhase.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _projectPhase.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_projectPhase.resetOriginalValues();
	}

	private final ProjectPhase _projectPhase;
}