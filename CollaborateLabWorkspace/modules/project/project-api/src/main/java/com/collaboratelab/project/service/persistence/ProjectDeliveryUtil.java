/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.project.model.ProjectDelivery;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the project delivery service. This utility wraps {@link com.collaboratelab.project.service.persistence.impl.ProjectDeliveryPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Claudio Carlenzoli
 * @see ProjectDeliveryPersistence
 * @see com.collaboratelab.project.service.persistence.impl.ProjectDeliveryPersistenceImpl
 * @generated
 */
@ProviderType
public class ProjectDeliveryUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(ProjectDelivery projectDelivery) {
		getPersistence().clearCache(projectDelivery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<ProjectDelivery> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<ProjectDelivery> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<ProjectDelivery> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<ProjectDelivery> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static ProjectDelivery update(ProjectDelivery projectDelivery) {
		return getPersistence().update(projectDelivery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static ProjectDelivery update(ProjectDelivery projectDelivery,
		ServiceContext serviceContext) {
		return getPersistence().update(projectDelivery, serviceContext);
	}

	/**
	* Returns all the project deliveries where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching project deliveries
	*/
	public static List<ProjectDelivery> findByUuid(java.lang.String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	* Returns a range of all the project deliveries where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @return the range of matching project deliveries
	*/
	public static List<ProjectDelivery> findByUuid(java.lang.String uuid,
		int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	* Returns an ordered range of all the project deliveries where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project deliveries
	*/
	public static List<ProjectDelivery> findByUuid(java.lang.String uuid,
		int start, int end, OrderByComparator<ProjectDelivery> orderByComparator) {
		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the project deliveries where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project deliveries
	*/
	public static List<ProjectDelivery> findByUuid(java.lang.String uuid,
		int start, int end,
		OrderByComparator<ProjectDelivery> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid(uuid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first project delivery in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project delivery
	* @throws NoSuchProjectDeliveryException if a matching project delivery could not be found
	*/
	public static ProjectDelivery findByUuid_First(java.lang.String uuid,
		OrderByComparator<ProjectDelivery> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectDeliveryException {
		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the first project delivery in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project delivery, or <code>null</code> if a matching project delivery could not be found
	*/
	public static ProjectDelivery fetchByUuid_First(java.lang.String uuid,
		OrderByComparator<ProjectDelivery> orderByComparator) {
		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the last project delivery in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project delivery
	* @throws NoSuchProjectDeliveryException if a matching project delivery could not be found
	*/
	public static ProjectDelivery findByUuid_Last(java.lang.String uuid,
		OrderByComparator<ProjectDelivery> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectDeliveryException {
		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the last project delivery in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project delivery, or <code>null</code> if a matching project delivery could not be found
	*/
	public static ProjectDelivery fetchByUuid_Last(java.lang.String uuid,
		OrderByComparator<ProjectDelivery> orderByComparator) {
		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the project deliveries before and after the current project delivery in the ordered set where uuid = &#63;.
	*
	* @param deliveryId the primary key of the current project delivery
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project delivery
	* @throws NoSuchProjectDeliveryException if a project delivery with the primary key could not be found
	*/
	public static ProjectDelivery[] findByUuid_PrevAndNext(long deliveryId,
		java.lang.String uuid,
		OrderByComparator<ProjectDelivery> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectDeliveryException {
		return getPersistence()
				   .findByUuid_PrevAndNext(deliveryId, uuid, orderByComparator);
	}

	/**
	* Removes all the project deliveries where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public static void removeByUuid(java.lang.String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	* Returns the number of project deliveries where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching project deliveries
	*/
	public static int countByUuid(java.lang.String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	* Returns all the project deliveries where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching project deliveries
	*/
	public static List<ProjectDelivery> findByUuid_C(java.lang.String uuid,
		long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	* Returns a range of all the project deliveries where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @return the range of matching project deliveries
	*/
	public static List<ProjectDelivery> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end) {
		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	* Returns an ordered range of all the project deliveries where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project deliveries
	*/
	public static List<ProjectDelivery> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<ProjectDelivery> orderByComparator) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the project deliveries where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project deliveries
	*/
	public static List<ProjectDelivery> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<ProjectDelivery> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first project delivery in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project delivery
	* @throws NoSuchProjectDeliveryException if a matching project delivery could not be found
	*/
	public static ProjectDelivery findByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<ProjectDelivery> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectDeliveryException {
		return getPersistence()
				   .findByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the first project delivery in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project delivery, or <code>null</code> if a matching project delivery could not be found
	*/
	public static ProjectDelivery fetchByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<ProjectDelivery> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last project delivery in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project delivery
	* @throws NoSuchProjectDeliveryException if a matching project delivery could not be found
	*/
	public static ProjectDelivery findByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<ProjectDelivery> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectDeliveryException {
		return getPersistence()
				   .findByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last project delivery in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project delivery, or <code>null</code> if a matching project delivery could not be found
	*/
	public static ProjectDelivery fetchByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<ProjectDelivery> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the project deliveries before and after the current project delivery in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param deliveryId the primary key of the current project delivery
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project delivery
	* @throws NoSuchProjectDeliveryException if a project delivery with the primary key could not be found
	*/
	public static ProjectDelivery[] findByUuid_C_PrevAndNext(long deliveryId,
		java.lang.String uuid, long companyId,
		OrderByComparator<ProjectDelivery> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectDeliveryException {
		return getPersistence()
				   .findByUuid_C_PrevAndNext(deliveryId, uuid, companyId,
			orderByComparator);
	}

	/**
	* Removes all the project deliveries where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public static void removeByUuid_C(java.lang.String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	* Returns the number of project deliveries where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching project deliveries
	*/
	public static int countByUuid_C(java.lang.String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	* Returns all the project deliveries where stepId = &#63;.
	*
	* @param stepId the step ID
	* @return the matching project deliveries
	*/
	public static List<ProjectDelivery> findByStepId(long stepId) {
		return getPersistence().findByStepId(stepId);
	}

	/**
	* Returns a range of all the project deliveries where stepId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param stepId the step ID
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @return the range of matching project deliveries
	*/
	public static List<ProjectDelivery> findByStepId(long stepId, int start,
		int end) {
		return getPersistence().findByStepId(stepId, start, end);
	}

	/**
	* Returns an ordered range of all the project deliveries where stepId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param stepId the step ID
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project deliveries
	*/
	public static List<ProjectDelivery> findByStepId(long stepId, int start,
		int end, OrderByComparator<ProjectDelivery> orderByComparator) {
		return getPersistence()
				   .findByStepId(stepId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the project deliveries where stepId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param stepId the step ID
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project deliveries
	*/
	public static List<ProjectDelivery> findByStepId(long stepId, int start,
		int end, OrderByComparator<ProjectDelivery> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByStepId(stepId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first project delivery in the ordered set where stepId = &#63;.
	*
	* @param stepId the step ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project delivery
	* @throws NoSuchProjectDeliveryException if a matching project delivery could not be found
	*/
	public static ProjectDelivery findByStepId_First(long stepId,
		OrderByComparator<ProjectDelivery> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectDeliveryException {
		return getPersistence().findByStepId_First(stepId, orderByComparator);
	}

	/**
	* Returns the first project delivery in the ordered set where stepId = &#63;.
	*
	* @param stepId the step ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project delivery, or <code>null</code> if a matching project delivery could not be found
	*/
	public static ProjectDelivery fetchByStepId_First(long stepId,
		OrderByComparator<ProjectDelivery> orderByComparator) {
		return getPersistence().fetchByStepId_First(stepId, orderByComparator);
	}

	/**
	* Returns the last project delivery in the ordered set where stepId = &#63;.
	*
	* @param stepId the step ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project delivery
	* @throws NoSuchProjectDeliveryException if a matching project delivery could not be found
	*/
	public static ProjectDelivery findByStepId_Last(long stepId,
		OrderByComparator<ProjectDelivery> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectDeliveryException {
		return getPersistence().findByStepId_Last(stepId, orderByComparator);
	}

	/**
	* Returns the last project delivery in the ordered set where stepId = &#63;.
	*
	* @param stepId the step ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project delivery, or <code>null</code> if a matching project delivery could not be found
	*/
	public static ProjectDelivery fetchByStepId_Last(long stepId,
		OrderByComparator<ProjectDelivery> orderByComparator) {
		return getPersistence().fetchByStepId_Last(stepId, orderByComparator);
	}

	/**
	* Returns the project deliveries before and after the current project delivery in the ordered set where stepId = &#63;.
	*
	* @param deliveryId the primary key of the current project delivery
	* @param stepId the step ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project delivery
	* @throws NoSuchProjectDeliveryException if a project delivery with the primary key could not be found
	*/
	public static ProjectDelivery[] findByStepId_PrevAndNext(long deliveryId,
		long stepId, OrderByComparator<ProjectDelivery> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectDeliveryException {
		return getPersistence()
				   .findByStepId_PrevAndNext(deliveryId, stepId,
			orderByComparator);
	}

	/**
	* Removes all the project deliveries where stepId = &#63; from the database.
	*
	* @param stepId the step ID
	*/
	public static void removeByStepId(long stepId) {
		getPersistence().removeByStepId(stepId);
	}

	/**
	* Returns the number of project deliveries where stepId = &#63;.
	*
	* @param stepId the step ID
	* @return the number of matching project deliveries
	*/
	public static int countByStepId(long stepId) {
		return getPersistence().countByStepId(stepId);
	}

	/**
	* Returns all the project deliveries where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching project deliveries
	*/
	public static List<ProjectDelivery> findByCompanyId(long companyId) {
		return getPersistence().findByCompanyId(companyId);
	}

	/**
	* Returns a range of all the project deliveries where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @return the range of matching project deliveries
	*/
	public static List<ProjectDelivery> findByCompanyId(long companyId,
		int start, int end) {
		return getPersistence().findByCompanyId(companyId, start, end);
	}

	/**
	* Returns an ordered range of all the project deliveries where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project deliveries
	*/
	public static List<ProjectDelivery> findByCompanyId(long companyId,
		int start, int end, OrderByComparator<ProjectDelivery> orderByComparator) {
		return getPersistence()
				   .findByCompanyId(companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the project deliveries where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project deliveries
	*/
	public static List<ProjectDelivery> findByCompanyId(long companyId,
		int start, int end,
		OrderByComparator<ProjectDelivery> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByCompanyId(companyId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first project delivery in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project delivery
	* @throws NoSuchProjectDeliveryException if a matching project delivery could not be found
	*/
	public static ProjectDelivery findByCompanyId_First(long companyId,
		OrderByComparator<ProjectDelivery> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectDeliveryException {
		return getPersistence()
				   .findByCompanyId_First(companyId, orderByComparator);
	}

	/**
	* Returns the first project delivery in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project delivery, or <code>null</code> if a matching project delivery could not be found
	*/
	public static ProjectDelivery fetchByCompanyId_First(long companyId,
		OrderByComparator<ProjectDelivery> orderByComparator) {
		return getPersistence()
				   .fetchByCompanyId_First(companyId, orderByComparator);
	}

	/**
	* Returns the last project delivery in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project delivery
	* @throws NoSuchProjectDeliveryException if a matching project delivery could not be found
	*/
	public static ProjectDelivery findByCompanyId_Last(long companyId,
		OrderByComparator<ProjectDelivery> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectDeliveryException {
		return getPersistence()
				   .findByCompanyId_Last(companyId, orderByComparator);
	}

	/**
	* Returns the last project delivery in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project delivery, or <code>null</code> if a matching project delivery could not be found
	*/
	public static ProjectDelivery fetchByCompanyId_Last(long companyId,
		OrderByComparator<ProjectDelivery> orderByComparator) {
		return getPersistence()
				   .fetchByCompanyId_Last(companyId, orderByComparator);
	}

	/**
	* Returns the project deliveries before and after the current project delivery in the ordered set where companyId = &#63;.
	*
	* @param deliveryId the primary key of the current project delivery
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project delivery
	* @throws NoSuchProjectDeliveryException if a project delivery with the primary key could not be found
	*/
	public static ProjectDelivery[] findByCompanyId_PrevAndNext(
		long deliveryId, long companyId,
		OrderByComparator<ProjectDelivery> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectDeliveryException {
		return getPersistence()
				   .findByCompanyId_PrevAndNext(deliveryId, companyId,
			orderByComparator);
	}

	/**
	* Removes all the project deliveries where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	*/
	public static void removeByCompanyId(long companyId) {
		getPersistence().removeByCompanyId(companyId);
	}

	/**
	* Returns the number of project deliveries where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching project deliveries
	*/
	public static int countByCompanyId(long companyId) {
		return getPersistence().countByCompanyId(companyId);
	}

	/**
	* Returns the project delivery where companyId = &#63; and stepId = &#63; or throws a {@link NoSuchProjectDeliveryException} if it could not be found.
	*
	* @param companyId the company ID
	* @param stepId the step ID
	* @return the matching project delivery
	* @throws NoSuchProjectDeliveryException if a matching project delivery could not be found
	*/
	public static ProjectDelivery findByCompanyIdIdentifier(long companyId,
		long stepId)
		throws com.collaboratelab.project.exception.NoSuchProjectDeliveryException {
		return getPersistence().findByCompanyIdIdentifier(companyId, stepId);
	}

	/**
	* Returns the project delivery where companyId = &#63; and stepId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param companyId the company ID
	* @param stepId the step ID
	* @return the matching project delivery, or <code>null</code> if a matching project delivery could not be found
	*/
	public static ProjectDelivery fetchByCompanyIdIdentifier(long companyId,
		long stepId) {
		return getPersistence().fetchByCompanyIdIdentifier(companyId, stepId);
	}

	/**
	* Returns the project delivery where companyId = &#63; and stepId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param companyId the company ID
	* @param stepId the step ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project delivery, or <code>null</code> if a matching project delivery could not be found
	*/
	public static ProjectDelivery fetchByCompanyIdIdentifier(long companyId,
		long stepId, boolean retrieveFromCache) {
		return getPersistence()
				   .fetchByCompanyIdIdentifier(companyId, stepId,
			retrieveFromCache);
	}

	/**
	* Removes the project delivery where companyId = &#63; and stepId = &#63; from the database.
	*
	* @param companyId the company ID
	* @param stepId the step ID
	* @return the project delivery that was removed
	*/
	public static ProjectDelivery removeByCompanyIdIdentifier(long companyId,
		long stepId)
		throws com.collaboratelab.project.exception.NoSuchProjectDeliveryException {
		return getPersistence().removeByCompanyIdIdentifier(companyId, stepId);
	}

	/**
	* Returns the number of project deliveries where companyId = &#63; and stepId = &#63;.
	*
	* @param companyId the company ID
	* @param stepId the step ID
	* @return the number of matching project deliveries
	*/
	public static int countByCompanyIdIdentifier(long companyId, long stepId) {
		return getPersistence().countByCompanyIdIdentifier(companyId, stepId);
	}

	/**
	* Returns all the project deliveries where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @return the matching project deliveries
	*/
	public static List<ProjectDelivery> findByCompanyIdIncludeName(
		long companyId, java.lang.String name) {
		return getPersistence().findByCompanyIdIncludeName(companyId, name);
	}

	/**
	* Returns a range of all the project deliveries where companyId = &#63; and name LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param name the name
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @return the range of matching project deliveries
	*/
	public static List<ProjectDelivery> findByCompanyIdIncludeName(
		long companyId, java.lang.String name, int start, int end) {
		return getPersistence()
				   .findByCompanyIdIncludeName(companyId, name, start, end);
	}

	/**
	* Returns an ordered range of all the project deliveries where companyId = &#63; and name LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param name the name
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project deliveries
	*/
	public static List<ProjectDelivery> findByCompanyIdIncludeName(
		long companyId, java.lang.String name, int start, int end,
		OrderByComparator<ProjectDelivery> orderByComparator) {
		return getPersistence()
				   .findByCompanyIdIncludeName(companyId, name, start, end,
			orderByComparator);
	}

	/**
	* Returns an ordered range of all the project deliveries where companyId = &#63; and name LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param name the name
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project deliveries
	*/
	public static List<ProjectDelivery> findByCompanyIdIncludeName(
		long companyId, java.lang.String name, int start, int end,
		OrderByComparator<ProjectDelivery> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByCompanyIdIncludeName(companyId, name, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first project delivery in the ordered set where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project delivery
	* @throws NoSuchProjectDeliveryException if a matching project delivery could not be found
	*/
	public static ProjectDelivery findByCompanyIdIncludeName_First(
		long companyId, java.lang.String name,
		OrderByComparator<ProjectDelivery> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectDeliveryException {
		return getPersistence()
				   .findByCompanyIdIncludeName_First(companyId, name,
			orderByComparator);
	}

	/**
	* Returns the first project delivery in the ordered set where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project delivery, or <code>null</code> if a matching project delivery could not be found
	*/
	public static ProjectDelivery fetchByCompanyIdIncludeName_First(
		long companyId, java.lang.String name,
		OrderByComparator<ProjectDelivery> orderByComparator) {
		return getPersistence()
				   .fetchByCompanyIdIncludeName_First(companyId, name,
			orderByComparator);
	}

	/**
	* Returns the last project delivery in the ordered set where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project delivery
	* @throws NoSuchProjectDeliveryException if a matching project delivery could not be found
	*/
	public static ProjectDelivery findByCompanyIdIncludeName_Last(
		long companyId, java.lang.String name,
		OrderByComparator<ProjectDelivery> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectDeliveryException {
		return getPersistence()
				   .findByCompanyIdIncludeName_Last(companyId, name,
			orderByComparator);
	}

	/**
	* Returns the last project delivery in the ordered set where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project delivery, or <code>null</code> if a matching project delivery could not be found
	*/
	public static ProjectDelivery fetchByCompanyIdIncludeName_Last(
		long companyId, java.lang.String name,
		OrderByComparator<ProjectDelivery> orderByComparator) {
		return getPersistence()
				   .fetchByCompanyIdIncludeName_Last(companyId, name,
			orderByComparator);
	}

	/**
	* Returns the project deliveries before and after the current project delivery in the ordered set where companyId = &#63; and name LIKE &#63;.
	*
	* @param deliveryId the primary key of the current project delivery
	* @param companyId the company ID
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project delivery
	* @throws NoSuchProjectDeliveryException if a project delivery with the primary key could not be found
	*/
	public static ProjectDelivery[] findByCompanyIdIncludeName_PrevAndNext(
		long deliveryId, long companyId, java.lang.String name,
		OrderByComparator<ProjectDelivery> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectDeliveryException {
		return getPersistence()
				   .findByCompanyIdIncludeName_PrevAndNext(deliveryId,
			companyId, name, orderByComparator);
	}

	/**
	* Removes all the project deliveries where companyId = &#63; and name LIKE &#63; from the database.
	*
	* @param companyId the company ID
	* @param name the name
	*/
	public static void removeByCompanyIdIncludeName(long companyId,
		java.lang.String name) {
		getPersistence().removeByCompanyIdIncludeName(companyId, name);
	}

	/**
	* Returns the number of project deliveries where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @return the number of matching project deliveries
	*/
	public static int countByCompanyIdIncludeName(long companyId,
		java.lang.String name) {
		return getPersistence().countByCompanyIdIncludeName(companyId, name);
	}

	/**
	* Caches the project delivery in the entity cache if it is enabled.
	*
	* @param projectDelivery the project delivery
	*/
	public static void cacheResult(ProjectDelivery projectDelivery) {
		getPersistence().cacheResult(projectDelivery);
	}

	/**
	* Caches the project deliveries in the entity cache if it is enabled.
	*
	* @param projectDeliveries the project deliveries
	*/
	public static void cacheResult(List<ProjectDelivery> projectDeliveries) {
		getPersistence().cacheResult(projectDeliveries);
	}

	/**
	* Creates a new project delivery with the primary key. Does not add the project delivery to the database.
	*
	* @param deliveryId the primary key for the new project delivery
	* @return the new project delivery
	*/
	public static ProjectDelivery create(long deliveryId) {
		return getPersistence().create(deliveryId);
	}

	/**
	* Removes the project delivery with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param deliveryId the primary key of the project delivery
	* @return the project delivery that was removed
	* @throws NoSuchProjectDeliveryException if a project delivery with the primary key could not be found
	*/
	public static ProjectDelivery remove(long deliveryId)
		throws com.collaboratelab.project.exception.NoSuchProjectDeliveryException {
		return getPersistence().remove(deliveryId);
	}

	public static ProjectDelivery updateImpl(ProjectDelivery projectDelivery) {
		return getPersistence().updateImpl(projectDelivery);
	}

	/**
	* Returns the project delivery with the primary key or throws a {@link NoSuchProjectDeliveryException} if it could not be found.
	*
	* @param deliveryId the primary key of the project delivery
	* @return the project delivery
	* @throws NoSuchProjectDeliveryException if a project delivery with the primary key could not be found
	*/
	public static ProjectDelivery findByPrimaryKey(long deliveryId)
		throws com.collaboratelab.project.exception.NoSuchProjectDeliveryException {
		return getPersistence().findByPrimaryKey(deliveryId);
	}

	/**
	* Returns the project delivery with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param deliveryId the primary key of the project delivery
	* @return the project delivery, or <code>null</code> if a project delivery with the primary key could not be found
	*/
	public static ProjectDelivery fetchByPrimaryKey(long deliveryId) {
		return getPersistence().fetchByPrimaryKey(deliveryId);
	}

	public static java.util.Map<java.io.Serializable, ProjectDelivery> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the project deliveries.
	*
	* @return the project deliveries
	*/
	public static List<ProjectDelivery> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the project deliveries.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @return the range of project deliveries
	*/
	public static List<ProjectDelivery> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the project deliveries.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of project deliveries
	*/
	public static List<ProjectDelivery> findAll(int start, int end,
		OrderByComparator<ProjectDelivery> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the project deliveries.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of project deliveries
	*/
	public static List<ProjectDelivery> findAll(int start, int end,
		OrderByComparator<ProjectDelivery> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the project deliveries from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of project deliveries.
	*
	* @return the number of project deliveries
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static java.util.Set<java.lang.String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static ProjectDeliveryPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ProjectDeliveryPersistence, ProjectDeliveryPersistence> _serviceTracker =
		ServiceTrackerFactory.open(ProjectDeliveryPersistence.class);
}