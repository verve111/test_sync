/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

/**
 * The extended model interface for the ProjectPhase service. Represents a row in the &quot;CLab_Prj_ProjectPhase&quot; database table, with each column mapped to a property of this class.
 *
 * @author Claudio Carlenzoli
 * @see ProjectPhaseModel
 * @see com.collaboratelab.project.model.impl.ProjectPhaseImpl
 * @see com.collaboratelab.project.model.impl.ProjectPhaseModelImpl
 * @generated
 */
@ImplementationClassName("com.collaboratelab.project.model.impl.ProjectPhaseImpl")
@ProviderType
public interface ProjectPhase extends ProjectPhaseModel, PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link com.collaboratelab.project.model.impl.ProjectPhaseImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<ProjectPhase, Long> PROJECT_PHASE_ID_ACCESSOR = new Accessor<ProjectPhase, Long>() {
			@Override
			public Long get(ProjectPhase projectPhase) {
				return projectPhase.getProjectPhaseId();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<ProjectPhase> getTypeClass() {
				return ProjectPhase.class;
			}
		};
}