/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ProjectDomainLocalService}.
 *
 * @author Claudio Carlenzoli
 * @see ProjectDomainLocalService
 * @generated
 */
@ProviderType
public class ProjectDomainLocalServiceWrapper
	implements ProjectDomainLocalService,
		ServiceWrapper<ProjectDomainLocalService> {
	public ProjectDomainLocalServiceWrapper(
		ProjectDomainLocalService projectDomainLocalService) {
		_projectDomainLocalService = projectDomainLocalService;
	}

	/**
	* Adds the project domain to the database. Also notifies the appropriate model listeners.
	*
	* @param projectDomain the project domain
	* @return the project domain that was added
	*/
	@Override
	public com.collaboratelab.project.model.ProjectDomain addProjectDomain(
		com.collaboratelab.project.model.ProjectDomain projectDomain) {
		return _projectDomainLocalService.addProjectDomain(projectDomain);
	}

	/**
	* Creates a new project domain with the primary key. Does not add the project domain to the database.
	*
	* @param projectDomainId the primary key for the new project domain
	* @return the new project domain
	*/
	@Override
	public com.collaboratelab.project.model.ProjectDomain createProjectDomain(
		long projectDomainId) {
		return _projectDomainLocalService.createProjectDomain(projectDomainId);
	}

	/**
	* Deletes the project domain from the database. Also notifies the appropriate model listeners.
	*
	* @param projectDomain the project domain
	* @return the project domain that was removed
	*/
	@Override
	public com.collaboratelab.project.model.ProjectDomain deleteProjectDomain(
		com.collaboratelab.project.model.ProjectDomain projectDomain) {
		return _projectDomainLocalService.deleteProjectDomain(projectDomain);
	}

	/**
	* Deletes the project domain with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param projectDomainId the primary key of the project domain
	* @return the project domain that was removed
	* @throws PortalException if a project domain with the primary key could not be found
	*/
	@Override
	public com.collaboratelab.project.model.ProjectDomain deleteProjectDomain(
		long projectDomainId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectDomainLocalService.deleteProjectDomain(projectDomainId);
	}

	@Override
	public com.collaboratelab.project.model.ProjectDomain fetchProjectDomain(
		long projectDomainId) {
		return _projectDomainLocalService.fetchProjectDomain(projectDomainId);
	}

	/**
	* Returns the project domain with the matching UUID and company.
	*
	* @param uuid the project domain's UUID
	* @param companyId the primary key of the company
	* @return the matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	@Override
	public com.collaboratelab.project.model.ProjectDomain fetchProjectDomainByUuidAndCompanyId(
		java.lang.String uuid, long companyId) {
		return _projectDomainLocalService.fetchProjectDomainByUuidAndCompanyId(uuid,
			companyId);
	}

	@Override
	public com.collaboratelab.project.model.ProjectDomain findByProjectDomainId(
		long projectDomainId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _projectDomainLocalService.findByProjectDomainId(projectDomainId);
	}

	@Override
	public com.collaboratelab.project.model.ProjectDomain findByProjectStructureIdIdentifier(
		long projectStructureId, java.lang.String identifier)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _projectDomainLocalService.findByProjectStructureIdIdentifier(projectStructureId,
			identifier);
	}

	/**
	* Returns the project domain with the primary key.
	*
	* @param projectDomainId the primary key of the project domain
	* @return the project domain
	* @throws PortalException if a project domain with the primary key could not be found
	*/
	@Override
	public com.collaboratelab.project.model.ProjectDomain getProjectDomain(
		long projectDomainId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectDomainLocalService.getProjectDomain(projectDomainId);
	}

	/**
	* Returns the project domain with the matching UUID and company.
	*
	* @param uuid the project domain's UUID
	* @param companyId the primary key of the company
	* @return the matching project domain
	* @throws PortalException if a matching project domain could not be found
	*/
	@Override
	public com.collaboratelab.project.model.ProjectDomain getProjectDomainByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectDomainLocalService.getProjectDomainByUuidAndCompanyId(uuid,
			companyId);
	}

	/**
	* Updates the project domain in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param projectDomain the project domain
	* @return the project domain that was updated
	*/
	@Override
	public com.collaboratelab.project.model.ProjectDomain updateProjectDomain(
		com.collaboratelab.project.model.ProjectDomain projectDomain) {
		return _projectDomainLocalService.updateProjectDomain(projectDomain);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _projectDomainLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _projectDomainLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery getExportActionableDynamicQuery(
		com.liferay.exportimport.kernel.lar.PortletDataContext portletDataContext) {
		return _projectDomainLocalService.getExportActionableDynamicQuery(portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _projectDomainLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectDomainLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectDomainLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of project domains.
	*
	* @return the number of project domains
	*/
	@Override
	public int getProjectDomainsCount() {
		return _projectDomainLocalService.getProjectDomainsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _projectDomainLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _projectDomainLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.project.model.impl.ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _projectDomainLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.project.model.impl.ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _projectDomainLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	@Override
	public java.util.List<com.collaboratelab.project.model.ProjectDomain> findByProjectStructureId(
		long projectStructureId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _projectDomainLocalService.findByProjectStructureId(projectStructureId);
	}

	/**
	* Returns a range of all the project domains.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.project.model.impl.ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @return the range of project domains
	*/
	@Override
	public java.util.List<com.collaboratelab.project.model.ProjectDomain> getProjectDomains(
		int start, int end) {
		return _projectDomainLocalService.getProjectDomains(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _projectDomainLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _projectDomainLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public ProjectDomainLocalService getWrappedService() {
		return _projectDomainLocalService;
	}

	@Override
	public void setWrappedService(
		ProjectDomainLocalService projectDomainLocalService) {
		_projectDomainLocalService = projectDomainLocalService;
	}

	private ProjectDomainLocalService _projectDomainLocalService;
}