/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ProjectDeliveryLocalService}.
 *
 * @author Claudio Carlenzoli
 * @see ProjectDeliveryLocalService
 * @generated
 */
@ProviderType
public class ProjectDeliveryLocalServiceWrapper
	implements ProjectDeliveryLocalService,
		ServiceWrapper<ProjectDeliveryLocalService> {
	public ProjectDeliveryLocalServiceWrapper(
		ProjectDeliveryLocalService projectDeliveryLocalService) {
		_projectDeliveryLocalService = projectDeliveryLocalService;
	}

	/**
	* Adds the project delivery to the database. Also notifies the appropriate model listeners.
	*
	* @param projectDelivery the project delivery
	* @return the project delivery that was added
	*/
	@Override
	public com.collaboratelab.project.model.ProjectDelivery addProjectDelivery(
		com.collaboratelab.project.model.ProjectDelivery projectDelivery) {
		return _projectDeliveryLocalService.addProjectDelivery(projectDelivery);
	}

	/**
	* Creates a new project delivery with the primary key. Does not add the project delivery to the database.
	*
	* @param deliveryId the primary key for the new project delivery
	* @return the new project delivery
	*/
	@Override
	public com.collaboratelab.project.model.ProjectDelivery createProjectDelivery(
		long deliveryId) {
		return _projectDeliveryLocalService.createProjectDelivery(deliveryId);
	}

	/**
	* Deletes the project delivery from the database. Also notifies the appropriate model listeners.
	*
	* @param projectDelivery the project delivery
	* @return the project delivery that was removed
	*/
	@Override
	public com.collaboratelab.project.model.ProjectDelivery deleteProjectDelivery(
		com.collaboratelab.project.model.ProjectDelivery projectDelivery) {
		return _projectDeliveryLocalService.deleteProjectDelivery(projectDelivery);
	}

	/**
	* Deletes the project delivery with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param deliveryId the primary key of the project delivery
	* @return the project delivery that was removed
	* @throws PortalException if a project delivery with the primary key could not be found
	*/
	@Override
	public com.collaboratelab.project.model.ProjectDelivery deleteProjectDelivery(
		long deliveryId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectDeliveryLocalService.deleteProjectDelivery(deliveryId);
	}

	@Override
	public com.collaboratelab.project.model.ProjectDelivery fetchProjectDelivery(
		long deliveryId) {
		return _projectDeliveryLocalService.fetchProjectDelivery(deliveryId);
	}

	/**
	* Returns the project delivery with the matching UUID and company.
	*
	* @param uuid the project delivery's UUID
	* @param companyId the primary key of the company
	* @return the matching project delivery, or <code>null</code> if a matching project delivery could not be found
	*/
	@Override
	public com.collaboratelab.project.model.ProjectDelivery fetchProjectDeliveryByUuidAndCompanyId(
		java.lang.String uuid, long companyId) {
		return _projectDeliveryLocalService.fetchProjectDeliveryByUuidAndCompanyId(uuid,
			companyId);
	}

	/**
	* Returns the project delivery with the primary key.
	*
	* @param deliveryId the primary key of the project delivery
	* @return the project delivery
	* @throws PortalException if a project delivery with the primary key could not be found
	*/
	@Override
	public com.collaboratelab.project.model.ProjectDelivery getProjectDelivery(
		long deliveryId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectDeliveryLocalService.getProjectDelivery(deliveryId);
	}

	/**
	* Returns the project delivery with the matching UUID and company.
	*
	* @param uuid the project delivery's UUID
	* @param companyId the primary key of the company
	* @return the matching project delivery
	* @throws PortalException if a matching project delivery could not be found
	*/
	@Override
	public com.collaboratelab.project.model.ProjectDelivery getProjectDeliveryByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectDeliveryLocalService.getProjectDeliveryByUuidAndCompanyId(uuid,
			companyId);
	}

	/**
	* Updates the project delivery in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param projectDelivery the project delivery
	* @return the project delivery that was updated
	*/
	@Override
	public com.collaboratelab.project.model.ProjectDelivery updateProjectDelivery(
		com.collaboratelab.project.model.ProjectDelivery projectDelivery) {
		return _projectDeliveryLocalService.updateProjectDelivery(projectDelivery);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _projectDeliveryLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _projectDeliveryLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery getExportActionableDynamicQuery(
		com.liferay.exportimport.kernel.lar.PortletDataContext portletDataContext) {
		return _projectDeliveryLocalService.getExportActionableDynamicQuery(portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _projectDeliveryLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectDeliveryLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _projectDeliveryLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of project deliveries.
	*
	* @return the number of project deliveries
	*/
	@Override
	public int getProjectDeliveriesCount() {
		return _projectDeliveryLocalService.getProjectDeliveriesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _projectDeliveryLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _projectDeliveryLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.project.model.impl.ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _projectDeliveryLocalService.dynamicQuery(dynamicQuery, start,
			end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.project.model.impl.ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _projectDeliveryLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	@Override
	public java.util.List<com.collaboratelab.project.model.ProjectDelivery> findByStepId(
		long projectStepId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _projectDeliveryLocalService.findByStepId(projectStepId);
	}

	/**
	* Returns a range of all the project deliveries.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.project.model.impl.ProjectDeliveryModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project deliveries
	* @param end the upper bound of the range of project deliveries (not inclusive)
	* @return the range of project deliveries
	*/
	@Override
	public java.util.List<com.collaboratelab.project.model.ProjectDelivery> getProjectDeliveries(
		int start, int end) {
		return _projectDeliveryLocalService.getProjectDeliveries(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _projectDeliveryLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _projectDeliveryLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public ProjectDeliveryLocalService getWrappedService() {
		return _projectDeliveryLocalService;
	}

	@Override
	public void setWrappedService(
		ProjectDeliveryLocalService projectDeliveryLocalService) {
		_projectDeliveryLocalService = projectDeliveryLocalService;
	}

	private ProjectDeliveryLocalService _projectDeliveryLocalService;
}