/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.exportimport.kernel.lar.StagedModelType;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link ProjectStructure}.
 * </p>
 *
 * @author Claudio Carlenzoli
 * @see ProjectStructure
 * @generated
 */
@ProviderType
public class ProjectStructureWrapper implements ProjectStructure,
	ModelWrapper<ProjectStructure> {
	public ProjectStructureWrapper(ProjectStructure projectStructure) {
		_projectStructure = projectStructure;
	}

	@Override
	public Class<?> getModelClass() {
		return ProjectStructure.class;
	}

	@Override
	public String getModelClassName() {
		return ProjectStructure.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("projectStructureId", getProjectStructureId());
		attributes.put("projectId", getProjectId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("identifier", getIdentifier());
		attributes.put("name", getName());
		attributes.put("description", getDescription());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long projectStructureId = (Long)attributes.get("projectStructureId");

		if (projectStructureId != null) {
			setProjectStructureId(projectStructureId);
		}

		Long projectId = (Long)attributes.get("projectId");

		if (projectId != null) {
			setProjectId(projectId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String identifier = (String)attributes.get("identifier");

		if (identifier != null) {
			setIdentifier(identifier);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}
	}

	@Override
	public ProjectStructure toEscapedModel() {
		return new ProjectStructureWrapper(_projectStructure.toEscapedModel());
	}

	@Override
	public ProjectStructure toUnescapedModel() {
		return new ProjectStructureWrapper(_projectStructure.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _projectStructure.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _projectStructure.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _projectStructure.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _projectStructure.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<ProjectStructure> toCacheModel() {
		return _projectStructure.toCacheModel();
	}

	@Override
	public int compareTo(ProjectStructure projectStructure) {
		return _projectStructure.compareTo(projectStructure);
	}

	@Override
	public int hashCode() {
		return _projectStructure.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _projectStructure.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new ProjectStructureWrapper((ProjectStructure)_projectStructure.clone());
	}

	/**
	* Returns the description of this project structure.
	*
	* @return the description of this project structure
	*/
	@Override
	public java.lang.String getDescription() {
		return _projectStructure.getDescription();
	}

	/**
	* Returns the identifier of this project structure.
	*
	* @return the identifier of this project structure
	*/
	@Override
	public java.lang.String getIdentifier() {
		return _projectStructure.getIdentifier();
	}

	/**
	* Returns the name of this project structure.
	*
	* @return the name of this project structure
	*/
	@Override
	public java.lang.String getName() {
		return _projectStructure.getName();
	}

	/**
	* Returns the user name of this project structure.
	*
	* @return the user name of this project structure
	*/
	@Override
	public java.lang.String getUserName() {
		return _projectStructure.getUserName();
	}

	/**
	* Returns the user uuid of this project structure.
	*
	* @return the user uuid of this project structure
	*/
	@Override
	public java.lang.String getUserUuid() {
		return _projectStructure.getUserUuid();
	}

	/**
	* Returns the uuid of this project structure.
	*
	* @return the uuid of this project structure
	*/
	@Override
	public java.lang.String getUuid() {
		return _projectStructure.getUuid();
	}

	@Override
	public java.lang.String toString() {
		return _projectStructure.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _projectStructure.toXmlString();
	}

	/**
	* Returns the create date of this project structure.
	*
	* @return the create date of this project structure
	*/
	@Override
	public Date getCreateDate() {
		return _projectStructure.getCreateDate();
	}

	/**
	* Returns the modified date of this project structure.
	*
	* @return the modified date of this project structure
	*/
	@Override
	public Date getModifiedDate() {
		return _projectStructure.getModifiedDate();
	}

	/**
	* Returns the company ID of this project structure.
	*
	* @return the company ID of this project structure
	*/
	@Override
	public long getCompanyId() {
		return _projectStructure.getCompanyId();
	}

	/**
	* Returns the primary key of this project structure.
	*
	* @return the primary key of this project structure
	*/
	@Override
	public long getPrimaryKey() {
		return _projectStructure.getPrimaryKey();
	}

	/**
	* Returns the project ID of this project structure.
	*
	* @return the project ID of this project structure
	*/
	@Override
	public long getProjectId() {
		return _projectStructure.getProjectId();
	}

	/**
	* Returns the project structure ID of this project structure.
	*
	* @return the project structure ID of this project structure
	*/
	@Override
	public long getProjectStructureId() {
		return _projectStructure.getProjectStructureId();
	}

	/**
	* Returns the user ID of this project structure.
	*
	* @return the user ID of this project structure
	*/
	@Override
	public long getUserId() {
		return _projectStructure.getUserId();
	}

	@Override
	public void persist() {
		_projectStructure.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_projectStructure.setCachedModel(cachedModel);
	}

	/**
	* Sets the company ID of this project structure.
	*
	* @param companyId the company ID of this project structure
	*/
	@Override
	public void setCompanyId(long companyId) {
		_projectStructure.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this project structure.
	*
	* @param createDate the create date of this project structure
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_projectStructure.setCreateDate(createDate);
	}

	/**
	* Sets the description of this project structure.
	*
	* @param description the description of this project structure
	*/
	@Override
	public void setDescription(java.lang.String description) {
		_projectStructure.setDescription(description);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_projectStructure.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_projectStructure.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_projectStructure.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the identifier of this project structure.
	*
	* @param identifier the identifier of this project structure
	*/
	@Override
	public void setIdentifier(java.lang.String identifier) {
		_projectStructure.setIdentifier(identifier);
	}

	/**
	* Sets the modified date of this project structure.
	*
	* @param modifiedDate the modified date of this project structure
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_projectStructure.setModifiedDate(modifiedDate);
	}

	/**
	* Sets the name of this project structure.
	*
	* @param name the name of this project structure
	*/
	@Override
	public void setName(java.lang.String name) {
		_projectStructure.setName(name);
	}

	@Override
	public void setNew(boolean n) {
		_projectStructure.setNew(n);
	}

	/**
	* Sets the primary key of this project structure.
	*
	* @param primaryKey the primary key of this project structure
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_projectStructure.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_projectStructure.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the project ID of this project structure.
	*
	* @param projectId the project ID of this project structure
	*/
	@Override
	public void setProjectId(long projectId) {
		_projectStructure.setProjectId(projectId);
	}

	/**
	* Sets the project structure ID of this project structure.
	*
	* @param projectStructureId the project structure ID of this project structure
	*/
	@Override
	public void setProjectStructureId(long projectStructureId) {
		_projectStructure.setProjectStructureId(projectStructureId);
	}

	/**
	* Sets the user ID of this project structure.
	*
	* @param userId the user ID of this project structure
	*/
	@Override
	public void setUserId(long userId) {
		_projectStructure.setUserId(userId);
	}

	/**
	* Sets the user name of this project structure.
	*
	* @param userName the user name of this project structure
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_projectStructure.setUserName(userName);
	}

	/**
	* Sets the user uuid of this project structure.
	*
	* @param userUuid the user uuid of this project structure
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_projectStructure.setUserUuid(userUuid);
	}

	/**
	* Sets the uuid of this project structure.
	*
	* @param uuid the uuid of this project structure
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_projectStructure.setUuid(uuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ProjectStructureWrapper)) {
			return false;
		}

		ProjectStructureWrapper projectStructureWrapper = (ProjectStructureWrapper)obj;

		if (Objects.equals(_projectStructure,
					projectStructureWrapper._projectStructure)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _projectStructure.getStagedModelType();
	}

	@Override
	public ProjectStructure getWrappedModel() {
		return _projectStructure;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _projectStructure.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _projectStructure.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_projectStructure.resetOriginalValues();
	}

	private final ProjectStructure _projectStructure;
}