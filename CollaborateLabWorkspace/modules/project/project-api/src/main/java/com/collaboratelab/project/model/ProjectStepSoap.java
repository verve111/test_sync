/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.collaboratelab.project.service.http.ProjectStepServiceSoap}.
 *
 * @author Claudio Carlenzoli
 * @see com.collaboratelab.project.service.http.ProjectStepServiceSoap
 * @generated
 */
@ProviderType
public class ProjectStepSoap implements Serializable {
	public static ProjectStepSoap toSoapModel(ProjectStep model) {
		ProjectStepSoap soapModel = new ProjectStepSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setProjectStepId(model.getProjectStepId());
		soapModel.setProjectId(model.getProjectId());
		soapModel.setProjectDomainId(model.getProjectDomainId());
		soapModel.setProjectPhaseId(model.getProjectPhaseId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setPercentage(model.getPercentage());

		return soapModel;
	}

	public static ProjectStepSoap[] toSoapModels(ProjectStep[] models) {
		ProjectStepSoap[] soapModels = new ProjectStepSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ProjectStepSoap[][] toSoapModels(ProjectStep[][] models) {
		ProjectStepSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ProjectStepSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ProjectStepSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ProjectStepSoap[] toSoapModels(List<ProjectStep> models) {
		List<ProjectStepSoap> soapModels = new ArrayList<ProjectStepSoap>(models.size());

		for (ProjectStep model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ProjectStepSoap[soapModels.size()]);
	}

	public ProjectStepSoap() {
	}

	public long getPrimaryKey() {
		return _projectStepId;
	}

	public void setPrimaryKey(long pk) {
		setProjectStepId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getProjectStepId() {
		return _projectStepId;
	}

	public void setProjectStepId(long projectStepId) {
		_projectStepId = projectStepId;
	}

	public long getProjectId() {
		return _projectId;
	}

	public void setProjectId(long projectId) {
		_projectId = projectId;
	}

	public long getProjectDomainId() {
		return _projectDomainId;
	}

	public void setProjectDomainId(long projectDomainId) {
		_projectDomainId = projectDomainId;
	}

	public long getProjectPhaseId() {
		return _projectPhaseId;
	}

	public void setProjectPhaseId(long projectPhaseId) {
		_projectPhaseId = projectPhaseId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getPercentage() {
		return _percentage;
	}

	public void setPercentage(long percentage) {
		_percentage = percentage;
	}

	private String _uuid;
	private long _projectStepId;
	private long _projectId;
	private long _projectDomainId;
	private long _projectPhaseId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _percentage;
}