/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.collaboratelab.project.exception;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.exception.NoSuchModelException;

/**
 * @author Claudio Carlenzoli
 */
@ProviderType
public class NoSuchProjectStepException extends NoSuchModelException {

	public NoSuchProjectStepException() {
	}

	public NoSuchProjectStepException(String msg) {
		super(msg);
	}

	public NoSuchProjectStepException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public NoSuchProjectStepException(Throwable cause) {
		super(cause);
	}

}