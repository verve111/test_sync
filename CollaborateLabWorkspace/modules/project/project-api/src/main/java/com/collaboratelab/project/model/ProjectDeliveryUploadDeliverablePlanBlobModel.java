/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.model;

import aQute.bnd.annotation.ProviderType;

import java.sql.Blob;

/**
 * The Blob model class for lazy loading the uploadDeliverablePlan column in ProjectDelivery.
 *
 * @author Claudio Carlenzoli
 * @see ProjectDelivery
 * @generated
 */
@ProviderType
public class ProjectDeliveryUploadDeliverablePlanBlobModel {
	public ProjectDeliveryUploadDeliverablePlanBlobModel() {
	}

	public ProjectDeliveryUploadDeliverablePlanBlobModel(long deliveryId) {
		_deliveryId = deliveryId;
	}

	public ProjectDeliveryUploadDeliverablePlanBlobModel(long deliveryId,
		Blob uploadDeliverablePlanBlob) {
		_deliveryId = deliveryId;
		_uploadDeliverablePlanBlob = uploadDeliverablePlanBlob;
	}

	public long getDeliveryId() {
		return _deliveryId;
	}

	public void setDeliveryId(long deliveryId) {
		_deliveryId = deliveryId;
	}

	public Blob getUploadDeliverablePlanBlob() {
		return _uploadDeliverablePlanBlob;
	}

	public void setUploadDeliverablePlanBlob(Blob uploadDeliverablePlanBlob) {
		_uploadDeliverablePlanBlob = uploadDeliverablePlanBlob;
	}

	private long _deliveryId;
	private Blob _uploadDeliverablePlanBlob;
}