/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.collaboratelab.project.service.http.ProjectPhaseServiceSoap}.
 *
 * @author Claudio Carlenzoli
 * @see com.collaboratelab.project.service.http.ProjectPhaseServiceSoap
 * @generated
 */
@ProviderType
public class ProjectPhaseSoap implements Serializable {
	public static ProjectPhaseSoap toSoapModel(ProjectPhase model) {
		ProjectPhaseSoap soapModel = new ProjectPhaseSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setProjectPhaseId(model.getProjectPhaseId());
		soapModel.setProjectStructureId(model.getProjectStructureId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setIdentifier(model.getIdentifier());
		soapModel.setName(model.getName());
		soapModel.setDescription(model.getDescription());
		soapModel.setPriority(model.getPriority());

		return soapModel;
	}

	public static ProjectPhaseSoap[] toSoapModels(ProjectPhase[] models) {
		ProjectPhaseSoap[] soapModels = new ProjectPhaseSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ProjectPhaseSoap[][] toSoapModels(ProjectPhase[][] models) {
		ProjectPhaseSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ProjectPhaseSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ProjectPhaseSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ProjectPhaseSoap[] toSoapModels(List<ProjectPhase> models) {
		List<ProjectPhaseSoap> soapModels = new ArrayList<ProjectPhaseSoap>(models.size());

		for (ProjectPhase model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ProjectPhaseSoap[soapModels.size()]);
	}

	public ProjectPhaseSoap() {
	}

	public long getPrimaryKey() {
		return _projectPhaseId;
	}

	public void setPrimaryKey(long pk) {
		setProjectPhaseId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getProjectPhaseId() {
		return _projectPhaseId;
	}

	public void setProjectPhaseId(long projectPhaseId) {
		_projectPhaseId = projectPhaseId;
	}

	public long getProjectStructureId() {
		return _projectStructureId;
	}

	public void setProjectStructureId(long projectStructureId) {
		_projectStructureId = projectStructureId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getIdentifier() {
		return _identifier;
	}

	public void setIdentifier(String identifier) {
		_identifier = identifier;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String description) {
		_description = description;
	}

	public long getPriority() {
		return _priority;
	}

	public void setPriority(long priority) {
		_priority = priority;
	}

	private String _uuid;
	private long _projectPhaseId;
	private long _projectStructureId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _identifier;
	private String _name;
	private String _description;
	private long _priority;
}