/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.project.model.ProjectDomain;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the project domain service. This utility wraps {@link com.collaboratelab.project.service.persistence.impl.ProjectDomainPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Claudio Carlenzoli
 * @see ProjectDomainPersistence
 * @see com.collaboratelab.project.service.persistence.impl.ProjectDomainPersistenceImpl
 * @generated
 */
@ProviderType
public class ProjectDomainUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(ProjectDomain projectDomain) {
		getPersistence().clearCache(projectDomain);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<ProjectDomain> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<ProjectDomain> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<ProjectDomain> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<ProjectDomain> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static ProjectDomain update(ProjectDomain projectDomain) {
		return getPersistence().update(projectDomain);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static ProjectDomain update(ProjectDomain projectDomain,
		ServiceContext serviceContext) {
		return getPersistence().update(projectDomain, serviceContext);
	}

	/**
	* Returns all the project domains where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching project domains
	*/
	public static List<ProjectDomain> findByUuid(java.lang.String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	* Returns a range of all the project domains where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @return the range of matching project domains
	*/
	public static List<ProjectDomain> findByUuid(java.lang.String uuid,
		int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	* Returns an ordered range of all the project domains where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project domains
	*/
	public static List<ProjectDomain> findByUuid(java.lang.String uuid,
		int start, int end, OrderByComparator<ProjectDomain> orderByComparator) {
		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the project domains where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project domains
	*/
	public static List<ProjectDomain> findByUuid(java.lang.String uuid,
		int start, int end, OrderByComparator<ProjectDomain> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid(uuid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first project domain in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project domain
	* @throws NoSuchProjectDomainException if a matching project domain could not be found
	*/
	public static ProjectDomain findByUuid_First(java.lang.String uuid,
		OrderByComparator<ProjectDomain> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectDomainException {
		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the first project domain in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public static ProjectDomain fetchByUuid_First(java.lang.String uuid,
		OrderByComparator<ProjectDomain> orderByComparator) {
		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the last project domain in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project domain
	* @throws NoSuchProjectDomainException if a matching project domain could not be found
	*/
	public static ProjectDomain findByUuid_Last(java.lang.String uuid,
		OrderByComparator<ProjectDomain> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectDomainException {
		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the last project domain in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public static ProjectDomain fetchByUuid_Last(java.lang.String uuid,
		OrderByComparator<ProjectDomain> orderByComparator) {
		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the project domains before and after the current project domain in the ordered set where uuid = &#63;.
	*
	* @param projectDomainId the primary key of the current project domain
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project domain
	* @throws NoSuchProjectDomainException if a project domain with the primary key could not be found
	*/
	public static ProjectDomain[] findByUuid_PrevAndNext(long projectDomainId,
		java.lang.String uuid,
		OrderByComparator<ProjectDomain> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectDomainException {
		return getPersistence()
				   .findByUuid_PrevAndNext(projectDomainId, uuid,
			orderByComparator);
	}

	/**
	* Removes all the project domains where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public static void removeByUuid(java.lang.String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	* Returns the number of project domains where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching project domains
	*/
	public static int countByUuid(java.lang.String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	* Returns all the project domains where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching project domains
	*/
	public static List<ProjectDomain> findByUuid_C(java.lang.String uuid,
		long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	* Returns a range of all the project domains where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @return the range of matching project domains
	*/
	public static List<ProjectDomain> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end) {
		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	* Returns an ordered range of all the project domains where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project domains
	*/
	public static List<ProjectDomain> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<ProjectDomain> orderByComparator) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the project domains where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project domains
	*/
	public static List<ProjectDomain> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<ProjectDomain> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first project domain in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project domain
	* @throws NoSuchProjectDomainException if a matching project domain could not be found
	*/
	public static ProjectDomain findByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<ProjectDomain> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectDomainException {
		return getPersistence()
				   .findByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the first project domain in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public static ProjectDomain fetchByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<ProjectDomain> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last project domain in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project domain
	* @throws NoSuchProjectDomainException if a matching project domain could not be found
	*/
	public static ProjectDomain findByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<ProjectDomain> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectDomainException {
		return getPersistence()
				   .findByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last project domain in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public static ProjectDomain fetchByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<ProjectDomain> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the project domains before and after the current project domain in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param projectDomainId the primary key of the current project domain
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project domain
	* @throws NoSuchProjectDomainException if a project domain with the primary key could not be found
	*/
	public static ProjectDomain[] findByUuid_C_PrevAndNext(
		long projectDomainId, java.lang.String uuid, long companyId,
		OrderByComparator<ProjectDomain> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectDomainException {
		return getPersistence()
				   .findByUuid_C_PrevAndNext(projectDomainId, uuid, companyId,
			orderByComparator);
	}

	/**
	* Removes all the project domains where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public static void removeByUuid_C(java.lang.String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	* Returns the number of project domains where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching project domains
	*/
	public static int countByUuid_C(java.lang.String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	* Returns all the project domains where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the matching project domains
	*/
	public static List<ProjectDomain> findByCompanyId(long companyId) {
		return getPersistence().findByCompanyId(companyId);
	}

	/**
	* Returns a range of all the project domains where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @return the range of matching project domains
	*/
	public static List<ProjectDomain> findByCompanyId(long companyId,
		int start, int end) {
		return getPersistence().findByCompanyId(companyId, start, end);
	}

	/**
	* Returns an ordered range of all the project domains where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project domains
	*/
	public static List<ProjectDomain> findByCompanyId(long companyId,
		int start, int end, OrderByComparator<ProjectDomain> orderByComparator) {
		return getPersistence()
				   .findByCompanyId(companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the project domains where companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project domains
	*/
	public static List<ProjectDomain> findByCompanyId(long companyId,
		int start, int end, OrderByComparator<ProjectDomain> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByCompanyId(companyId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first project domain in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project domain
	* @throws NoSuchProjectDomainException if a matching project domain could not be found
	*/
	public static ProjectDomain findByCompanyId_First(long companyId,
		OrderByComparator<ProjectDomain> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectDomainException {
		return getPersistence()
				   .findByCompanyId_First(companyId, orderByComparator);
	}

	/**
	* Returns the first project domain in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public static ProjectDomain fetchByCompanyId_First(long companyId,
		OrderByComparator<ProjectDomain> orderByComparator) {
		return getPersistence()
				   .fetchByCompanyId_First(companyId, orderByComparator);
	}

	/**
	* Returns the last project domain in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project domain
	* @throws NoSuchProjectDomainException if a matching project domain could not be found
	*/
	public static ProjectDomain findByCompanyId_Last(long companyId,
		OrderByComparator<ProjectDomain> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectDomainException {
		return getPersistence()
				   .findByCompanyId_Last(companyId, orderByComparator);
	}

	/**
	* Returns the last project domain in the ordered set where companyId = &#63;.
	*
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public static ProjectDomain fetchByCompanyId_Last(long companyId,
		OrderByComparator<ProjectDomain> orderByComparator) {
		return getPersistence()
				   .fetchByCompanyId_Last(companyId, orderByComparator);
	}

	/**
	* Returns the project domains before and after the current project domain in the ordered set where companyId = &#63;.
	*
	* @param projectDomainId the primary key of the current project domain
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project domain
	* @throws NoSuchProjectDomainException if a project domain with the primary key could not be found
	*/
	public static ProjectDomain[] findByCompanyId_PrevAndNext(
		long projectDomainId, long companyId,
		OrderByComparator<ProjectDomain> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectDomainException {
		return getPersistence()
				   .findByCompanyId_PrevAndNext(projectDomainId, companyId,
			orderByComparator);
	}

	/**
	* Removes all the project domains where companyId = &#63; from the database.
	*
	* @param companyId the company ID
	*/
	public static void removeByCompanyId(long companyId) {
		getPersistence().removeByCompanyId(companyId);
	}

	/**
	* Returns the number of project domains where companyId = &#63;.
	*
	* @param companyId the company ID
	* @return the number of matching project domains
	*/
	public static int countByCompanyId(long companyId) {
		return getPersistence().countByCompanyId(companyId);
	}

	/**
	* Returns the project domain where companyId = &#63; and identifier = &#63; or throws a {@link NoSuchProjectDomainException} if it could not be found.
	*
	* @param companyId the company ID
	* @param identifier the identifier
	* @return the matching project domain
	* @throws NoSuchProjectDomainException if a matching project domain could not be found
	*/
	public static ProjectDomain findByCompanyIdIdentifier(long companyId,
		java.lang.String identifier)
		throws com.collaboratelab.project.exception.NoSuchProjectDomainException {
		return getPersistence().findByCompanyIdIdentifier(companyId, identifier);
	}

	/**
	* Returns the project domain where companyId = &#63; and identifier = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param companyId the company ID
	* @param identifier the identifier
	* @return the matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public static ProjectDomain fetchByCompanyIdIdentifier(long companyId,
		java.lang.String identifier) {
		return getPersistence().fetchByCompanyIdIdentifier(companyId, identifier);
	}

	/**
	* Returns the project domain where companyId = &#63; and identifier = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param companyId the company ID
	* @param identifier the identifier
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public static ProjectDomain fetchByCompanyIdIdentifier(long companyId,
		java.lang.String identifier, boolean retrieveFromCache) {
		return getPersistence()
				   .fetchByCompanyIdIdentifier(companyId, identifier,
			retrieveFromCache);
	}

	/**
	* Removes the project domain where companyId = &#63; and identifier = &#63; from the database.
	*
	* @param companyId the company ID
	* @param identifier the identifier
	* @return the project domain that was removed
	*/
	public static ProjectDomain removeByCompanyIdIdentifier(long companyId,
		java.lang.String identifier)
		throws com.collaboratelab.project.exception.NoSuchProjectDomainException {
		return getPersistence()
				   .removeByCompanyIdIdentifier(companyId, identifier);
	}

	/**
	* Returns the number of project domains where companyId = &#63; and identifier = &#63;.
	*
	* @param companyId the company ID
	* @param identifier the identifier
	* @return the number of matching project domains
	*/
	public static int countByCompanyIdIdentifier(long companyId,
		java.lang.String identifier) {
		return getPersistence().countByCompanyIdIdentifier(companyId, identifier);
	}

	/**
	* Returns all the project domains where companyId = &#63; and projectStructureId = &#63;.
	*
	* @param companyId the company ID
	* @param projectStructureId the project structure ID
	* @return the matching project domains
	*/
	public static List<ProjectDomain> findByCompanyIdProjectStructureId(
		long companyId, long projectStructureId) {
		return getPersistence()
				   .findByCompanyIdProjectStructureId(companyId,
			projectStructureId);
	}

	/**
	* Returns a range of all the project domains where companyId = &#63; and projectStructureId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param projectStructureId the project structure ID
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @return the range of matching project domains
	*/
	public static List<ProjectDomain> findByCompanyIdProjectStructureId(
		long companyId, long projectStructureId, int start, int end) {
		return getPersistence()
				   .findByCompanyIdProjectStructureId(companyId,
			projectStructureId, start, end);
	}

	/**
	* Returns an ordered range of all the project domains where companyId = &#63; and projectStructureId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param projectStructureId the project structure ID
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project domains
	*/
	public static List<ProjectDomain> findByCompanyIdProjectStructureId(
		long companyId, long projectStructureId, int start, int end,
		OrderByComparator<ProjectDomain> orderByComparator) {
		return getPersistence()
				   .findByCompanyIdProjectStructureId(companyId,
			projectStructureId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the project domains where companyId = &#63; and projectStructureId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param projectStructureId the project structure ID
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project domains
	*/
	public static List<ProjectDomain> findByCompanyIdProjectStructureId(
		long companyId, long projectStructureId, int start, int end,
		OrderByComparator<ProjectDomain> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByCompanyIdProjectStructureId(companyId,
			projectStructureId, start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first project domain in the ordered set where companyId = &#63; and projectStructureId = &#63;.
	*
	* @param companyId the company ID
	* @param projectStructureId the project structure ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project domain
	* @throws NoSuchProjectDomainException if a matching project domain could not be found
	*/
	public static ProjectDomain findByCompanyIdProjectStructureId_First(
		long companyId, long projectStructureId,
		OrderByComparator<ProjectDomain> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectDomainException {
		return getPersistence()
				   .findByCompanyIdProjectStructureId_First(companyId,
			projectStructureId, orderByComparator);
	}

	/**
	* Returns the first project domain in the ordered set where companyId = &#63; and projectStructureId = &#63;.
	*
	* @param companyId the company ID
	* @param projectStructureId the project structure ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public static ProjectDomain fetchByCompanyIdProjectStructureId_First(
		long companyId, long projectStructureId,
		OrderByComparator<ProjectDomain> orderByComparator) {
		return getPersistence()
				   .fetchByCompanyIdProjectStructureId_First(companyId,
			projectStructureId, orderByComparator);
	}

	/**
	* Returns the last project domain in the ordered set where companyId = &#63; and projectStructureId = &#63;.
	*
	* @param companyId the company ID
	* @param projectStructureId the project structure ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project domain
	* @throws NoSuchProjectDomainException if a matching project domain could not be found
	*/
	public static ProjectDomain findByCompanyIdProjectStructureId_Last(
		long companyId, long projectStructureId,
		OrderByComparator<ProjectDomain> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectDomainException {
		return getPersistence()
				   .findByCompanyIdProjectStructureId_Last(companyId,
			projectStructureId, orderByComparator);
	}

	/**
	* Returns the last project domain in the ordered set where companyId = &#63; and projectStructureId = &#63;.
	*
	* @param companyId the company ID
	* @param projectStructureId the project structure ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public static ProjectDomain fetchByCompanyIdProjectStructureId_Last(
		long companyId, long projectStructureId,
		OrderByComparator<ProjectDomain> orderByComparator) {
		return getPersistence()
				   .fetchByCompanyIdProjectStructureId_Last(companyId,
			projectStructureId, orderByComparator);
	}

	/**
	* Returns the project domains before and after the current project domain in the ordered set where companyId = &#63; and projectStructureId = &#63;.
	*
	* @param projectDomainId the primary key of the current project domain
	* @param companyId the company ID
	* @param projectStructureId the project structure ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project domain
	* @throws NoSuchProjectDomainException if a project domain with the primary key could not be found
	*/
	public static ProjectDomain[] findByCompanyIdProjectStructureId_PrevAndNext(
		long projectDomainId, long companyId, long projectStructureId,
		OrderByComparator<ProjectDomain> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectDomainException {
		return getPersistence()
				   .findByCompanyIdProjectStructureId_PrevAndNext(projectDomainId,
			companyId, projectStructureId, orderByComparator);
	}

	/**
	* Removes all the project domains where companyId = &#63; and projectStructureId = &#63; from the database.
	*
	* @param companyId the company ID
	* @param projectStructureId the project structure ID
	*/
	public static void removeByCompanyIdProjectStructureId(long companyId,
		long projectStructureId) {
		getPersistence()
			.removeByCompanyIdProjectStructureId(companyId, projectStructureId);
	}

	/**
	* Returns the number of project domains where companyId = &#63; and projectStructureId = &#63;.
	*
	* @param companyId the company ID
	* @param projectStructureId the project structure ID
	* @return the number of matching project domains
	*/
	public static int countByCompanyIdProjectStructureId(long companyId,
		long projectStructureId) {
		return getPersistence()
				   .countByCompanyIdProjectStructureId(companyId,
			projectStructureId);
	}

	/**
	* Returns all the project domains where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @return the matching project domains
	*/
	public static List<ProjectDomain> findByCompanyIdIncludeName(
		long companyId, java.lang.String name) {
		return getPersistence().findByCompanyIdIncludeName(companyId, name);
	}

	/**
	* Returns a range of all the project domains where companyId = &#63; and name LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param name the name
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @return the range of matching project domains
	*/
	public static List<ProjectDomain> findByCompanyIdIncludeName(
		long companyId, java.lang.String name, int start, int end) {
		return getPersistence()
				   .findByCompanyIdIncludeName(companyId, name, start, end);
	}

	/**
	* Returns an ordered range of all the project domains where companyId = &#63; and name LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param name the name
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project domains
	*/
	public static List<ProjectDomain> findByCompanyIdIncludeName(
		long companyId, java.lang.String name, int start, int end,
		OrderByComparator<ProjectDomain> orderByComparator) {
		return getPersistence()
				   .findByCompanyIdIncludeName(companyId, name, start, end,
			orderByComparator);
	}

	/**
	* Returns an ordered range of all the project domains where companyId = &#63; and name LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param companyId the company ID
	* @param name the name
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project domains
	*/
	public static List<ProjectDomain> findByCompanyIdIncludeName(
		long companyId, java.lang.String name, int start, int end,
		OrderByComparator<ProjectDomain> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByCompanyIdIncludeName(companyId, name, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first project domain in the ordered set where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project domain
	* @throws NoSuchProjectDomainException if a matching project domain could not be found
	*/
	public static ProjectDomain findByCompanyIdIncludeName_First(
		long companyId, java.lang.String name,
		OrderByComparator<ProjectDomain> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectDomainException {
		return getPersistence()
				   .findByCompanyIdIncludeName_First(companyId, name,
			orderByComparator);
	}

	/**
	* Returns the first project domain in the ordered set where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public static ProjectDomain fetchByCompanyIdIncludeName_First(
		long companyId, java.lang.String name,
		OrderByComparator<ProjectDomain> orderByComparator) {
		return getPersistence()
				   .fetchByCompanyIdIncludeName_First(companyId, name,
			orderByComparator);
	}

	/**
	* Returns the last project domain in the ordered set where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project domain
	* @throws NoSuchProjectDomainException if a matching project domain could not be found
	*/
	public static ProjectDomain findByCompanyIdIncludeName_Last(
		long companyId, java.lang.String name,
		OrderByComparator<ProjectDomain> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectDomainException {
		return getPersistence()
				   .findByCompanyIdIncludeName_Last(companyId, name,
			orderByComparator);
	}

	/**
	* Returns the last project domain in the ordered set where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public static ProjectDomain fetchByCompanyIdIncludeName_Last(
		long companyId, java.lang.String name,
		OrderByComparator<ProjectDomain> orderByComparator) {
		return getPersistence()
				   .fetchByCompanyIdIncludeName_Last(companyId, name,
			orderByComparator);
	}

	/**
	* Returns the project domains before and after the current project domain in the ordered set where companyId = &#63; and name LIKE &#63;.
	*
	* @param projectDomainId the primary key of the current project domain
	* @param companyId the company ID
	* @param name the name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project domain
	* @throws NoSuchProjectDomainException if a project domain with the primary key could not be found
	*/
	public static ProjectDomain[] findByCompanyIdIncludeName_PrevAndNext(
		long projectDomainId, long companyId, java.lang.String name,
		OrderByComparator<ProjectDomain> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectDomainException {
		return getPersistence()
				   .findByCompanyIdIncludeName_PrevAndNext(projectDomainId,
			companyId, name, orderByComparator);
	}

	/**
	* Removes all the project domains where companyId = &#63; and name LIKE &#63; from the database.
	*
	* @param companyId the company ID
	* @param name the name
	*/
	public static void removeByCompanyIdIncludeName(long companyId,
		java.lang.String name) {
		getPersistence().removeByCompanyIdIncludeName(companyId, name);
	}

	/**
	* Returns the number of project domains where companyId = &#63; and name LIKE &#63;.
	*
	* @param companyId the company ID
	* @param name the name
	* @return the number of matching project domains
	*/
	public static int countByCompanyIdIncludeName(long companyId,
		java.lang.String name) {
		return getPersistence().countByCompanyIdIncludeName(companyId, name);
	}

	/**
	* Returns all the project domains where projectStructureId = &#63;.
	*
	* @param projectStructureId the project structure ID
	* @return the matching project domains
	*/
	public static List<ProjectDomain> findByProjectStructureId(
		long projectStructureId) {
		return getPersistence().findByProjectStructureId(projectStructureId);
	}

	/**
	* Returns a range of all the project domains where projectStructureId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectStructureId the project structure ID
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @return the range of matching project domains
	*/
	public static List<ProjectDomain> findByProjectStructureId(
		long projectStructureId, int start, int end) {
		return getPersistence()
				   .findByProjectStructureId(projectStructureId, start, end);
	}

	/**
	* Returns an ordered range of all the project domains where projectStructureId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectStructureId the project structure ID
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching project domains
	*/
	public static List<ProjectDomain> findByProjectStructureId(
		long projectStructureId, int start, int end,
		OrderByComparator<ProjectDomain> orderByComparator) {
		return getPersistence()
				   .findByProjectStructureId(projectStructureId, start, end,
			orderByComparator);
	}

	/**
	* Returns an ordered range of all the project domains where projectStructureId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param projectStructureId the project structure ID
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching project domains
	*/
	public static List<ProjectDomain> findByProjectStructureId(
		long projectStructureId, int start, int end,
		OrderByComparator<ProjectDomain> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByProjectStructureId(projectStructureId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first project domain in the ordered set where projectStructureId = &#63;.
	*
	* @param projectStructureId the project structure ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project domain
	* @throws NoSuchProjectDomainException if a matching project domain could not be found
	*/
	public static ProjectDomain findByProjectStructureId_First(
		long projectStructureId,
		OrderByComparator<ProjectDomain> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectDomainException {
		return getPersistence()
				   .findByProjectStructureId_First(projectStructureId,
			orderByComparator);
	}

	/**
	* Returns the first project domain in the ordered set where projectStructureId = &#63;.
	*
	* @param projectStructureId the project structure ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public static ProjectDomain fetchByProjectStructureId_First(
		long projectStructureId,
		OrderByComparator<ProjectDomain> orderByComparator) {
		return getPersistence()
				   .fetchByProjectStructureId_First(projectStructureId,
			orderByComparator);
	}

	/**
	* Returns the last project domain in the ordered set where projectStructureId = &#63;.
	*
	* @param projectStructureId the project structure ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project domain
	* @throws NoSuchProjectDomainException if a matching project domain could not be found
	*/
	public static ProjectDomain findByProjectStructureId_Last(
		long projectStructureId,
		OrderByComparator<ProjectDomain> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectDomainException {
		return getPersistence()
				   .findByProjectStructureId_Last(projectStructureId,
			orderByComparator);
	}

	/**
	* Returns the last project domain in the ordered set where projectStructureId = &#63;.
	*
	* @param projectStructureId the project structure ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public static ProjectDomain fetchByProjectStructureId_Last(
		long projectStructureId,
		OrderByComparator<ProjectDomain> orderByComparator) {
		return getPersistence()
				   .fetchByProjectStructureId_Last(projectStructureId,
			orderByComparator);
	}

	/**
	* Returns the project domains before and after the current project domain in the ordered set where projectStructureId = &#63;.
	*
	* @param projectDomainId the primary key of the current project domain
	* @param projectStructureId the project structure ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next project domain
	* @throws NoSuchProjectDomainException if a project domain with the primary key could not be found
	*/
	public static ProjectDomain[] findByProjectStructureId_PrevAndNext(
		long projectDomainId, long projectStructureId,
		OrderByComparator<ProjectDomain> orderByComparator)
		throws com.collaboratelab.project.exception.NoSuchProjectDomainException {
		return getPersistence()
				   .findByProjectStructureId_PrevAndNext(projectDomainId,
			projectStructureId, orderByComparator);
	}

	/**
	* Removes all the project domains where projectStructureId = &#63; from the database.
	*
	* @param projectStructureId the project structure ID
	*/
	public static void removeByProjectStructureId(long projectStructureId) {
		getPersistence().removeByProjectStructureId(projectStructureId);
	}

	/**
	* Returns the number of project domains where projectStructureId = &#63;.
	*
	* @param projectStructureId the project structure ID
	* @return the number of matching project domains
	*/
	public static int countByProjectStructureId(long projectStructureId) {
		return getPersistence().countByProjectStructureId(projectStructureId);
	}

	/**
	* Returns the project domain where projectStructureId = &#63; and identifier = &#63; or throws a {@link NoSuchProjectDomainException} if it could not be found.
	*
	* @param projectStructureId the project structure ID
	* @param identifier the identifier
	* @return the matching project domain
	* @throws NoSuchProjectDomainException if a matching project domain could not be found
	*/
	public static ProjectDomain findByProjectStructureIdIdentifier(
		long projectStructureId, java.lang.String identifier)
		throws com.collaboratelab.project.exception.NoSuchProjectDomainException {
		return getPersistence()
				   .findByProjectStructureIdIdentifier(projectStructureId,
			identifier);
	}

	/**
	* Returns the project domain where projectStructureId = &#63; and identifier = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param projectStructureId the project structure ID
	* @param identifier the identifier
	* @return the matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public static ProjectDomain fetchByProjectStructureIdIdentifier(
		long projectStructureId, java.lang.String identifier) {
		return getPersistence()
				   .fetchByProjectStructureIdIdentifier(projectStructureId,
			identifier);
	}

	/**
	* Returns the project domain where projectStructureId = &#63; and identifier = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param projectStructureId the project structure ID
	* @param identifier the identifier
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public static ProjectDomain fetchByProjectStructureIdIdentifier(
		long projectStructureId, java.lang.String identifier,
		boolean retrieveFromCache) {
		return getPersistence()
				   .fetchByProjectStructureIdIdentifier(projectStructureId,
			identifier, retrieveFromCache);
	}

	/**
	* Removes the project domain where projectStructureId = &#63; and identifier = &#63; from the database.
	*
	* @param projectStructureId the project structure ID
	* @param identifier the identifier
	* @return the project domain that was removed
	*/
	public static ProjectDomain removeByProjectStructureIdIdentifier(
		long projectStructureId, java.lang.String identifier)
		throws com.collaboratelab.project.exception.NoSuchProjectDomainException {
		return getPersistence()
				   .removeByProjectStructureIdIdentifier(projectStructureId,
			identifier);
	}

	/**
	* Returns the number of project domains where projectStructureId = &#63; and identifier = &#63;.
	*
	* @param projectStructureId the project structure ID
	* @param identifier the identifier
	* @return the number of matching project domains
	*/
	public static int countByProjectStructureIdIdentifier(
		long projectStructureId, java.lang.String identifier) {
		return getPersistence()
				   .countByProjectStructureIdIdentifier(projectStructureId,
			identifier);
	}

	/**
	* Returns the project domain where projectDomainId = &#63; or throws a {@link NoSuchProjectDomainException} if it could not be found.
	*
	* @param projectDomainId the project domain ID
	* @return the matching project domain
	* @throws NoSuchProjectDomainException if a matching project domain could not be found
	*/
	public static ProjectDomain findByProjectDomainId(long projectDomainId)
		throws com.collaboratelab.project.exception.NoSuchProjectDomainException {
		return getPersistence().findByProjectDomainId(projectDomainId);
	}

	/**
	* Returns the project domain where projectDomainId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param projectDomainId the project domain ID
	* @return the matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public static ProjectDomain fetchByProjectDomainId(long projectDomainId) {
		return getPersistence().fetchByProjectDomainId(projectDomainId);
	}

	/**
	* Returns the project domain where projectDomainId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param projectDomainId the project domain ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching project domain, or <code>null</code> if a matching project domain could not be found
	*/
	public static ProjectDomain fetchByProjectDomainId(long projectDomainId,
		boolean retrieveFromCache) {
		return getPersistence()
				   .fetchByProjectDomainId(projectDomainId, retrieveFromCache);
	}

	/**
	* Removes the project domain where projectDomainId = &#63; from the database.
	*
	* @param projectDomainId the project domain ID
	* @return the project domain that was removed
	*/
	public static ProjectDomain removeByProjectDomainId(long projectDomainId)
		throws com.collaboratelab.project.exception.NoSuchProjectDomainException {
		return getPersistence().removeByProjectDomainId(projectDomainId);
	}

	/**
	* Returns the number of project domains where projectDomainId = &#63;.
	*
	* @param projectDomainId the project domain ID
	* @return the number of matching project domains
	*/
	public static int countByProjectDomainId(long projectDomainId) {
		return getPersistence().countByProjectDomainId(projectDomainId);
	}

	/**
	* Caches the project domain in the entity cache if it is enabled.
	*
	* @param projectDomain the project domain
	*/
	public static void cacheResult(ProjectDomain projectDomain) {
		getPersistence().cacheResult(projectDomain);
	}

	/**
	* Caches the project domains in the entity cache if it is enabled.
	*
	* @param projectDomains the project domains
	*/
	public static void cacheResult(List<ProjectDomain> projectDomains) {
		getPersistence().cacheResult(projectDomains);
	}

	/**
	* Creates a new project domain with the primary key. Does not add the project domain to the database.
	*
	* @param projectDomainId the primary key for the new project domain
	* @return the new project domain
	*/
	public static ProjectDomain create(long projectDomainId) {
		return getPersistence().create(projectDomainId);
	}

	/**
	* Removes the project domain with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param projectDomainId the primary key of the project domain
	* @return the project domain that was removed
	* @throws NoSuchProjectDomainException if a project domain with the primary key could not be found
	*/
	public static ProjectDomain remove(long projectDomainId)
		throws com.collaboratelab.project.exception.NoSuchProjectDomainException {
		return getPersistence().remove(projectDomainId);
	}

	public static ProjectDomain updateImpl(ProjectDomain projectDomain) {
		return getPersistence().updateImpl(projectDomain);
	}

	/**
	* Returns the project domain with the primary key or throws a {@link NoSuchProjectDomainException} if it could not be found.
	*
	* @param projectDomainId the primary key of the project domain
	* @return the project domain
	* @throws NoSuchProjectDomainException if a project domain with the primary key could not be found
	*/
	public static ProjectDomain findByPrimaryKey(long projectDomainId)
		throws com.collaboratelab.project.exception.NoSuchProjectDomainException {
		return getPersistence().findByPrimaryKey(projectDomainId);
	}

	/**
	* Returns the project domain with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param projectDomainId the primary key of the project domain
	* @return the project domain, or <code>null</code> if a project domain with the primary key could not be found
	*/
	public static ProjectDomain fetchByPrimaryKey(long projectDomainId) {
		return getPersistence().fetchByPrimaryKey(projectDomainId);
	}

	public static java.util.Map<java.io.Serializable, ProjectDomain> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the project domains.
	*
	* @return the project domains
	*/
	public static List<ProjectDomain> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the project domains.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @return the range of project domains
	*/
	public static List<ProjectDomain> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the project domains.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of project domains
	*/
	public static List<ProjectDomain> findAll(int start, int end,
		OrderByComparator<ProjectDomain> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the project domains.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ProjectDomainModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project domains
	* @param end the upper bound of the range of project domains (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of project domains
	*/
	public static List<ProjectDomain> findAll(int start, int end,
		OrderByComparator<ProjectDomain> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the project domains from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of project domains.
	*
	* @return the number of project domains
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static java.util.Set<java.lang.String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static ProjectDomainPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ProjectDomainPersistence, ProjectDomainPersistence> _serviceTracker =
		ServiceTrackerFactory.open(ProjectDomainPersistence.class);
}