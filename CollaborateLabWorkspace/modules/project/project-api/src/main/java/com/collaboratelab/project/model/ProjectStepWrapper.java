/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.exportimport.kernel.lar.StagedModelType;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link ProjectStep}.
 * </p>
 *
 * @author Claudio Carlenzoli
 * @see ProjectStep
 * @generated
 */
@ProviderType
public class ProjectStepWrapper implements ProjectStep,
	ModelWrapper<ProjectStep> {
	public ProjectStepWrapper(ProjectStep projectStep) {
		_projectStep = projectStep;
	}

	@Override
	public Class<?> getModelClass() {
		return ProjectStep.class;
	}

	@Override
	public String getModelClassName() {
		return ProjectStep.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("projectStepId", getProjectStepId());
		attributes.put("projectId", getProjectId());
		attributes.put("projectDomainId", getProjectDomainId());
		attributes.put("projectPhaseId", getProjectPhaseId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("percentage", getPercentage());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long projectStepId = (Long)attributes.get("projectStepId");

		if (projectStepId != null) {
			setProjectStepId(projectStepId);
		}

		Long projectId = (Long)attributes.get("projectId");

		if (projectId != null) {
			setProjectId(projectId);
		}

		Long projectDomainId = (Long)attributes.get("projectDomainId");

		if (projectDomainId != null) {
			setProjectDomainId(projectDomainId);
		}

		Long projectPhaseId = (Long)attributes.get("projectPhaseId");

		if (projectPhaseId != null) {
			setProjectPhaseId(projectPhaseId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long percentage = (Long)attributes.get("percentage");

		if (percentage != null) {
			setPercentage(percentage);
		}
	}

	@Override
	public ProjectStep toEscapedModel() {
		return new ProjectStepWrapper(_projectStep.toEscapedModel());
	}

	@Override
	public ProjectStep toUnescapedModel() {
		return new ProjectStepWrapper(_projectStep.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _projectStep.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _projectStep.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _projectStep.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _projectStep.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<ProjectStep> toCacheModel() {
		return _projectStep.toCacheModel();
	}

	@Override
	public int compareTo(ProjectStep projectStep) {
		return _projectStep.compareTo(projectStep);
	}

	@Override
	public int hashCode() {
		return _projectStep.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _projectStep.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new ProjectStepWrapper((ProjectStep)_projectStep.clone());
	}

	/**
	* Returns the user name of this project step.
	*
	* @return the user name of this project step
	*/
	@Override
	public java.lang.String getUserName() {
		return _projectStep.getUserName();
	}

	/**
	* Returns the user uuid of this project step.
	*
	* @return the user uuid of this project step
	*/
	@Override
	public java.lang.String getUserUuid() {
		return _projectStep.getUserUuid();
	}

	/**
	* Returns the uuid of this project step.
	*
	* @return the uuid of this project step
	*/
	@Override
	public java.lang.String getUuid() {
		return _projectStep.getUuid();
	}

	@Override
	public java.lang.String toString() {
		return _projectStep.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _projectStep.toXmlString();
	}

	/**
	* Returns the create date of this project step.
	*
	* @return the create date of this project step
	*/
	@Override
	public Date getCreateDate() {
		return _projectStep.getCreateDate();
	}

	/**
	* Returns the modified date of this project step.
	*
	* @return the modified date of this project step
	*/
	@Override
	public Date getModifiedDate() {
		return _projectStep.getModifiedDate();
	}

	/**
	* Returns the company ID of this project step.
	*
	* @return the company ID of this project step
	*/
	@Override
	public long getCompanyId() {
		return _projectStep.getCompanyId();
	}

	/**
	* Returns the percentage of this project step.
	*
	* @return the percentage of this project step
	*/
	@Override
	public long getPercentage() {
		return _projectStep.getPercentage();
	}

	/**
	* Returns the primary key of this project step.
	*
	* @return the primary key of this project step
	*/
	@Override
	public long getPrimaryKey() {
		return _projectStep.getPrimaryKey();
	}

	/**
	* Returns the project domain ID of this project step.
	*
	* @return the project domain ID of this project step
	*/
	@Override
	public long getProjectDomainId() {
		return _projectStep.getProjectDomainId();
	}

	/**
	* Returns the project ID of this project step.
	*
	* @return the project ID of this project step
	*/
	@Override
	public long getProjectId() {
		return _projectStep.getProjectId();
	}

	/**
	* Returns the project phase ID of this project step.
	*
	* @return the project phase ID of this project step
	*/
	@Override
	public long getProjectPhaseId() {
		return _projectStep.getProjectPhaseId();
	}

	/**
	* Returns the project step ID of this project step.
	*
	* @return the project step ID of this project step
	*/
	@Override
	public long getProjectStepId() {
		return _projectStep.getProjectStepId();
	}

	/**
	* Returns the user ID of this project step.
	*
	* @return the user ID of this project step
	*/
	@Override
	public long getUserId() {
		return _projectStep.getUserId();
	}

	@Override
	public void persist() {
		_projectStep.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_projectStep.setCachedModel(cachedModel);
	}

	/**
	* Sets the company ID of this project step.
	*
	* @param companyId the company ID of this project step
	*/
	@Override
	public void setCompanyId(long companyId) {
		_projectStep.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this project step.
	*
	* @param createDate the create date of this project step
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_projectStep.setCreateDate(createDate);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_projectStep.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_projectStep.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_projectStep.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the modified date of this project step.
	*
	* @param modifiedDate the modified date of this project step
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_projectStep.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_projectStep.setNew(n);
	}

	/**
	* Sets the percentage of this project step.
	*
	* @param percentage the percentage of this project step
	*/
	@Override
	public void setPercentage(long percentage) {
		_projectStep.setPercentage(percentage);
	}

	/**
	* Sets the primary key of this project step.
	*
	* @param primaryKey the primary key of this project step
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_projectStep.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_projectStep.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the project domain ID of this project step.
	*
	* @param projectDomainId the project domain ID of this project step
	*/
	@Override
	public void setProjectDomainId(long projectDomainId) {
		_projectStep.setProjectDomainId(projectDomainId);
	}

	/**
	* Sets the project ID of this project step.
	*
	* @param projectId the project ID of this project step
	*/
	@Override
	public void setProjectId(long projectId) {
		_projectStep.setProjectId(projectId);
	}

	/**
	* Sets the project phase ID of this project step.
	*
	* @param projectPhaseId the project phase ID of this project step
	*/
	@Override
	public void setProjectPhaseId(long projectPhaseId) {
		_projectStep.setProjectPhaseId(projectPhaseId);
	}

	/**
	* Sets the project step ID of this project step.
	*
	* @param projectStepId the project step ID of this project step
	*/
	@Override
	public void setProjectStepId(long projectStepId) {
		_projectStep.setProjectStepId(projectStepId);
	}

	/**
	* Sets the user ID of this project step.
	*
	* @param userId the user ID of this project step
	*/
	@Override
	public void setUserId(long userId) {
		_projectStep.setUserId(userId);
	}

	/**
	* Sets the user name of this project step.
	*
	* @param userName the user name of this project step
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_projectStep.setUserName(userName);
	}

	/**
	* Sets the user uuid of this project step.
	*
	* @param userUuid the user uuid of this project step
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_projectStep.setUserUuid(userUuid);
	}

	/**
	* Sets the uuid of this project step.
	*
	* @param uuid the uuid of this project step
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_projectStep.setUuid(uuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ProjectStepWrapper)) {
			return false;
		}

		ProjectStepWrapper projectStepWrapper = (ProjectStepWrapper)obj;

		if (Objects.equals(_projectStep, projectStepWrapper._projectStep)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _projectStep.getStagedModelType();
	}

	@Override
	public ProjectStep getWrappedModel() {
		return _projectStep;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _projectStep.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _projectStep.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_projectStep.resetOriginalValues();
	}

	private final ProjectStep _projectStep;
}