/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.exportimport.kernel.lar.StagedModelType;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link ProjectDelivery}.
 * </p>
 *
 * @author Claudio Carlenzoli
 * @see ProjectDelivery
 * @generated
 */
@ProviderType
public class ProjectDeliveryWrapper implements ProjectDelivery,
	ModelWrapper<ProjectDelivery> {
	public ProjectDeliveryWrapper(ProjectDelivery projectDelivery) {
		_projectDelivery = projectDelivery;
	}

	@Override
	public Class<?> getModelClass() {
		return ProjectDelivery.class;
	}

	@Override
	public String getModelClassName() {
		return ProjectDelivery.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("deliveryId", getDeliveryId());
		attributes.put("stepId", getStepId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("name", getName());
		attributes.put("type", getType());
		attributes.put("phase", getPhase());
		attributes.put("domain", getDomain());
		attributes.put("status", getStatus());
		attributes.put("startDate", getStartDate());
		attributes.put("endDate", getEndDate());
		attributes.put("cost", getCost());
		attributes.put("outcomeCategory", getOutcomeCategory());
		attributes.put("comment", getComment());
		attributes.put("isAvailable", getIsAvailable());
		attributes.put("reviewerIsCompleted", getReviewerIsCompleted());
		attributes.put("reviewerRate", getReviewerRate());
		attributes.put("reviewerComment", getReviewerComment());
		attributes.put("fundingSource", getFundingSource());
		attributes.put("isEditPlanning", getIsEditPlanning());
		attributes.put("budget", getBudget());
		attributes.put("personResponsible", getPersonResponsible());
		attributes.put("startPlanningDate", getStartPlanningDate());
		attributes.put("endPlanningDate", getEndPlanningDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long deliveryId = (Long)attributes.get("deliveryId");

		if (deliveryId != null) {
			setDeliveryId(deliveryId);
		}

		Long stepId = (Long)attributes.get("stepId");

		if (stepId != null) {
			setStepId(stepId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		Integer type = (Integer)attributes.get("type");

		if (type != null) {
			setType(type);
		}

		String phase = (String)attributes.get("phase");

		if (phase != null) {
			setPhase(phase);
		}

		String domain = (String)attributes.get("domain");

		if (domain != null) {
			setDomain(domain);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		Date startDate = (Date)attributes.get("startDate");

		if (startDate != null) {
			setStartDate(startDate);
		}

		Date endDate = (Date)attributes.get("endDate");

		if (endDate != null) {
			setEndDate(endDate);
		}

		Integer cost = (Integer)attributes.get("cost");

		if (cost != null) {
			setCost(cost);
		}

		Integer outcomeCategory = (Integer)attributes.get("outcomeCategory");

		if (outcomeCategory != null) {
			setOutcomeCategory(outcomeCategory);
		}

		String comment = (String)attributes.get("comment");

		if (comment != null) {
			setComment(comment);
		}

		Boolean isAvailable = (Boolean)attributes.get("isAvailable");

		if (isAvailable != null) {
			setIsAvailable(isAvailable);
		}

		Boolean reviewerIsCompleted = (Boolean)attributes.get(
				"reviewerIsCompleted");

		if (reviewerIsCompleted != null) {
			setReviewerIsCompleted(reviewerIsCompleted);
		}

		Integer reviewerRate = (Integer)attributes.get("reviewerRate");

		if (reviewerRate != null) {
			setReviewerRate(reviewerRate);
		}

		String reviewerComment = (String)attributes.get("reviewerComment");

		if (reviewerComment != null) {
			setReviewerComment(reviewerComment);
		}

		Integer fundingSource = (Integer)attributes.get("fundingSource");

		if (fundingSource != null) {
			setFundingSource(fundingSource);
		}

		Boolean isEditPlanning = (Boolean)attributes.get("isEditPlanning");

		if (isEditPlanning != null) {
			setIsEditPlanning(isEditPlanning);
		}

		Double budget = (Double)attributes.get("budget");

		if (budget != null) {
			setBudget(budget);
		}

		String personResponsible = (String)attributes.get("personResponsible");

		if (personResponsible != null) {
			setPersonResponsible(personResponsible);
		}

		Date startPlanningDate = (Date)attributes.get("startPlanningDate");

		if (startPlanningDate != null) {
			setStartPlanningDate(startPlanningDate);
		}

		Date endPlanningDate = (Date)attributes.get("endPlanningDate");

		if (endPlanningDate != null) {
			setEndPlanningDate(endPlanningDate);
		}
	}

	@Override
	public ProjectDelivery toEscapedModel() {
		return new ProjectDeliveryWrapper(_projectDelivery.toEscapedModel());
	}

	@Override
	public ProjectDelivery toUnescapedModel() {
		return new ProjectDeliveryWrapper(_projectDelivery.toUnescapedModel());
	}

	/**
	* Returns the is available of this project delivery.
	*
	* @return the is available of this project delivery
	*/
	@Override
	public boolean getIsAvailable() {
		return _projectDelivery.getIsAvailable();
	}

	/**
	* Returns the is edit planning of this project delivery.
	*
	* @return the is edit planning of this project delivery
	*/
	@Override
	public boolean getIsEditPlanning() {
		return _projectDelivery.getIsEditPlanning();
	}

	/**
	* Returns the reviewer is completed of this project delivery.
	*
	* @return the reviewer is completed of this project delivery
	*/
	@Override
	public boolean getReviewerIsCompleted() {
		return _projectDelivery.getReviewerIsCompleted();
	}

	@Override
	public boolean isCachedModel() {
		return _projectDelivery.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _projectDelivery.isEscapedModel();
	}

	/**
	* Returns <code>true</code> if this project delivery is is available.
	*
	* @return <code>true</code> if this project delivery is is available; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsAvailable() {
		return _projectDelivery.isIsAvailable();
	}

	/**
	* Returns <code>true</code> if this project delivery is is edit planning.
	*
	* @return <code>true</code> if this project delivery is is edit planning; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsEditPlanning() {
		return _projectDelivery.isIsEditPlanning();
	}

	@Override
	public boolean isNew() {
		return _projectDelivery.isNew();
	}

	/**
	* Returns <code>true</code> if this project delivery is reviewer is completed.
	*
	* @return <code>true</code> if this project delivery is reviewer is completed; <code>false</code> otherwise
	*/
	@Override
	public boolean isReviewerIsCompleted() {
		return _projectDelivery.isReviewerIsCompleted();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _projectDelivery.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<ProjectDelivery> toCacheModel() {
		return _projectDelivery.toCacheModel();
	}

	/**
	* Returns the budget of this project delivery.
	*
	* @return the budget of this project delivery
	*/
	@Override
	public double getBudget() {
		return _projectDelivery.getBudget();
	}

	@Override
	public int compareTo(ProjectDelivery projectDelivery) {
		return _projectDelivery.compareTo(projectDelivery);
	}

	/**
	* Returns the cost of this project delivery.
	*
	* @return the cost of this project delivery
	*/
	@Override
	public int getCost() {
		return _projectDelivery.getCost();
	}

	/**
	* Returns the funding source of this project delivery.
	*
	* @return the funding source of this project delivery
	*/
	@Override
	public int getFundingSource() {
		return _projectDelivery.getFundingSource();
	}

	/**
	* Returns the outcome category of this project delivery.
	*
	* @return the outcome category of this project delivery
	*/
	@Override
	public int getOutcomeCategory() {
		return _projectDelivery.getOutcomeCategory();
	}

	/**
	* Returns the reviewer rate of this project delivery.
	*
	* @return the reviewer rate of this project delivery
	*/
	@Override
	public int getReviewerRate() {
		return _projectDelivery.getReviewerRate();
	}

	/**
	* Returns the status of this project delivery.
	*
	* @return the status of this project delivery
	*/
	@Override
	public int getStatus() {
		return _projectDelivery.getStatus();
	}

	/**
	* Returns the type of this project delivery.
	*
	* @return the type of this project delivery
	*/
	@Override
	public int getType() {
		return _projectDelivery.getType();
	}

	@Override
	public int hashCode() {
		return _projectDelivery.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _projectDelivery.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new ProjectDeliveryWrapper((ProjectDelivery)_projectDelivery.clone());
	}

	/**
	* Returns the comment of this project delivery.
	*
	* @return the comment of this project delivery
	*/
	@Override
	public java.lang.String getComment() {
		return _projectDelivery.getComment();
	}

	/**
	* Returns the domain of this project delivery.
	*
	* @return the domain of this project delivery
	*/
	@Override
	public java.lang.String getDomain() {
		return _projectDelivery.getDomain();
	}

	/**
	* Returns the name of this project delivery.
	*
	* @return the name of this project delivery
	*/
	@Override
	public java.lang.String getName() {
		return _projectDelivery.getName();
	}

	/**
	* Returns the person responsible of this project delivery.
	*
	* @return the person responsible of this project delivery
	*/
	@Override
	public java.lang.String getPersonResponsible() {
		return _projectDelivery.getPersonResponsible();
	}

	/**
	* Returns the phase of this project delivery.
	*
	* @return the phase of this project delivery
	*/
	@Override
	public java.lang.String getPhase() {
		return _projectDelivery.getPhase();
	}

	/**
	* Returns the reviewer comment of this project delivery.
	*
	* @return the reviewer comment of this project delivery
	*/
	@Override
	public java.lang.String getReviewerComment() {
		return _projectDelivery.getReviewerComment();
	}

	/**
	* Returns the user name of this project delivery.
	*
	* @return the user name of this project delivery
	*/
	@Override
	public java.lang.String getUserName() {
		return _projectDelivery.getUserName();
	}

	/**
	* Returns the user uuid of this project delivery.
	*
	* @return the user uuid of this project delivery
	*/
	@Override
	public java.lang.String getUserUuid() {
		return _projectDelivery.getUserUuid();
	}

	/**
	* Returns the uuid of this project delivery.
	*
	* @return the uuid of this project delivery
	*/
	@Override
	public java.lang.String getUuid() {
		return _projectDelivery.getUuid();
	}

	@Override
	public java.lang.String toString() {
		return _projectDelivery.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _projectDelivery.toXmlString();
	}

	/**
	* Returns the create date of this project delivery.
	*
	* @return the create date of this project delivery
	*/
	@Override
	public Date getCreateDate() {
		return _projectDelivery.getCreateDate();
	}

	/**
	* Returns the end date of this project delivery.
	*
	* @return the end date of this project delivery
	*/
	@Override
	public Date getEndDate() {
		return _projectDelivery.getEndDate();
	}

	/**
	* Returns the end planning date of this project delivery.
	*
	* @return the end planning date of this project delivery
	*/
	@Override
	public Date getEndPlanningDate() {
		return _projectDelivery.getEndPlanningDate();
	}

	/**
	* Returns the modified date of this project delivery.
	*
	* @return the modified date of this project delivery
	*/
	@Override
	public Date getModifiedDate() {
		return _projectDelivery.getModifiedDate();
	}

	/**
	* Returns the start date of this project delivery.
	*
	* @return the start date of this project delivery
	*/
	@Override
	public Date getStartDate() {
		return _projectDelivery.getStartDate();
	}

	/**
	* Returns the start planning date of this project delivery.
	*
	* @return the start planning date of this project delivery
	*/
	@Override
	public Date getStartPlanningDate() {
		return _projectDelivery.getStartPlanningDate();
	}

	/**
	* Returns the company ID of this project delivery.
	*
	* @return the company ID of this project delivery
	*/
	@Override
	public long getCompanyId() {
		return _projectDelivery.getCompanyId();
	}

	/**
	* Returns the delivery ID of this project delivery.
	*
	* @return the delivery ID of this project delivery
	*/
	@Override
	public long getDeliveryId() {
		return _projectDelivery.getDeliveryId();
	}

	/**
	* Returns the primary key of this project delivery.
	*
	* @return the primary key of this project delivery
	*/
	@Override
	public long getPrimaryKey() {
		return _projectDelivery.getPrimaryKey();
	}

	/**
	* Returns the step ID of this project delivery.
	*
	* @return the step ID of this project delivery
	*/
	@Override
	public long getStepId() {
		return _projectDelivery.getStepId();
	}

	/**
	* Returns the user ID of this project delivery.
	*
	* @return the user ID of this project delivery
	*/
	@Override
	public long getUserId() {
		return _projectDelivery.getUserId();
	}

	@Override
	public void persist() {
		_projectDelivery.persist();
	}

	/**
	* Sets the budget of this project delivery.
	*
	* @param budget the budget of this project delivery
	*/
	@Override
	public void setBudget(double budget) {
		_projectDelivery.setBudget(budget);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_projectDelivery.setCachedModel(cachedModel);
	}

	/**
	* Sets the comment of this project delivery.
	*
	* @param comment the comment of this project delivery
	*/
	@Override
	public void setComment(java.lang.String comment) {
		_projectDelivery.setComment(comment);
	}

	/**
	* Sets the company ID of this project delivery.
	*
	* @param companyId the company ID of this project delivery
	*/
	@Override
	public void setCompanyId(long companyId) {
		_projectDelivery.setCompanyId(companyId);
	}

	/**
	* Sets the cost of this project delivery.
	*
	* @param cost the cost of this project delivery
	*/
	@Override
	public void setCost(int cost) {
		_projectDelivery.setCost(cost);
	}

	/**
	* Sets the create date of this project delivery.
	*
	* @param createDate the create date of this project delivery
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_projectDelivery.setCreateDate(createDate);
	}

	/**
	* Sets the delivery ID of this project delivery.
	*
	* @param deliveryId the delivery ID of this project delivery
	*/
	@Override
	public void setDeliveryId(long deliveryId) {
		_projectDelivery.setDeliveryId(deliveryId);
	}

	/**
	* Sets the domain of this project delivery.
	*
	* @param domain the domain of this project delivery
	*/
	@Override
	public void setDomain(java.lang.String domain) {
		_projectDelivery.setDomain(domain);
	}

	/**
	* Sets the end date of this project delivery.
	*
	* @param endDate the end date of this project delivery
	*/
	@Override
	public void setEndDate(Date endDate) {
		_projectDelivery.setEndDate(endDate);
	}

	/**
	* Sets the end planning date of this project delivery.
	*
	* @param endPlanningDate the end planning date of this project delivery
	*/
	@Override
	public void setEndPlanningDate(Date endPlanningDate) {
		_projectDelivery.setEndPlanningDate(endPlanningDate);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_projectDelivery.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_projectDelivery.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_projectDelivery.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the funding source of this project delivery.
	*
	* @param fundingSource the funding source of this project delivery
	*/
	@Override
	public void setFundingSource(int fundingSource) {
		_projectDelivery.setFundingSource(fundingSource);
	}

	/**
	* Sets whether this project delivery is is available.
	*
	* @param isAvailable the is available of this project delivery
	*/
	@Override
	public void setIsAvailable(boolean isAvailable) {
		_projectDelivery.setIsAvailable(isAvailable);
	}

	/**
	* Sets whether this project delivery is is edit planning.
	*
	* @param isEditPlanning the is edit planning of this project delivery
	*/
	@Override
	public void setIsEditPlanning(boolean isEditPlanning) {
		_projectDelivery.setIsEditPlanning(isEditPlanning);
	}

	/**
	* Sets the modified date of this project delivery.
	*
	* @param modifiedDate the modified date of this project delivery
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_projectDelivery.setModifiedDate(modifiedDate);
	}

	/**
	* Sets the name of this project delivery.
	*
	* @param name the name of this project delivery
	*/
	@Override
	public void setName(java.lang.String name) {
		_projectDelivery.setName(name);
	}

	@Override
	public void setNew(boolean n) {
		_projectDelivery.setNew(n);
	}

	/**
	* Sets the outcome category of this project delivery.
	*
	* @param outcomeCategory the outcome category of this project delivery
	*/
	@Override
	public void setOutcomeCategory(int outcomeCategory) {
		_projectDelivery.setOutcomeCategory(outcomeCategory);
	}

	/**
	* Sets the person responsible of this project delivery.
	*
	* @param personResponsible the person responsible of this project delivery
	*/
	@Override
	public void setPersonResponsible(java.lang.String personResponsible) {
		_projectDelivery.setPersonResponsible(personResponsible);
	}

	/**
	* Sets the phase of this project delivery.
	*
	* @param phase the phase of this project delivery
	*/
	@Override
	public void setPhase(java.lang.String phase) {
		_projectDelivery.setPhase(phase);
	}

	/**
	* Sets the primary key of this project delivery.
	*
	* @param primaryKey the primary key of this project delivery
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_projectDelivery.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_projectDelivery.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the reviewer comment of this project delivery.
	*
	* @param reviewerComment the reviewer comment of this project delivery
	*/
	@Override
	public void setReviewerComment(java.lang.String reviewerComment) {
		_projectDelivery.setReviewerComment(reviewerComment);
	}

	/**
	* Sets whether this project delivery is reviewer is completed.
	*
	* @param reviewerIsCompleted the reviewer is completed of this project delivery
	*/
	@Override
	public void setReviewerIsCompleted(boolean reviewerIsCompleted) {
		_projectDelivery.setReviewerIsCompleted(reviewerIsCompleted);
	}

	/**
	* Sets the reviewer rate of this project delivery.
	*
	* @param reviewerRate the reviewer rate of this project delivery
	*/
	@Override
	public void setReviewerRate(int reviewerRate) {
		_projectDelivery.setReviewerRate(reviewerRate);
	}

	/**
	* Sets the start date of this project delivery.
	*
	* @param startDate the start date of this project delivery
	*/
	@Override
	public void setStartDate(Date startDate) {
		_projectDelivery.setStartDate(startDate);
	}

	/**
	* Sets the start planning date of this project delivery.
	*
	* @param startPlanningDate the start planning date of this project delivery
	*/
	@Override
	public void setStartPlanningDate(Date startPlanningDate) {
		_projectDelivery.setStartPlanningDate(startPlanningDate);
	}

	/**
	* Sets the status of this project delivery.
	*
	* @param status the status of this project delivery
	*/
	@Override
	public void setStatus(int status) {
		_projectDelivery.setStatus(status);
	}

	/**
	* Sets the step ID of this project delivery.
	*
	* @param stepId the step ID of this project delivery
	*/
	@Override
	public void setStepId(long stepId) {
		_projectDelivery.setStepId(stepId);
	}

	/**
	* Sets the type of this project delivery.
	*
	* @param type the type of this project delivery
	*/
	@Override
	public void setType(int type) {
		_projectDelivery.setType(type);
	}

	/**
	* Sets the user ID of this project delivery.
	*
	* @param userId the user ID of this project delivery
	*/
	@Override
	public void setUserId(long userId) {
		_projectDelivery.setUserId(userId);
	}

	/**
	* Sets the user name of this project delivery.
	*
	* @param userName the user name of this project delivery
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_projectDelivery.setUserName(userName);
	}

	/**
	* Sets the user uuid of this project delivery.
	*
	* @param userUuid the user uuid of this project delivery
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_projectDelivery.setUserUuid(userUuid);
	}

	/**
	* Sets the uuid of this project delivery.
	*
	* @param uuid the uuid of this project delivery
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_projectDelivery.setUuid(uuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ProjectDeliveryWrapper)) {
			return false;
		}

		ProjectDeliveryWrapper projectDeliveryWrapper = (ProjectDeliveryWrapper)obj;

		if (Objects.equals(_projectDelivery,
					projectDeliveryWrapper._projectDelivery)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _projectDelivery.getStagedModelType();
	}

	@Override
	public ProjectDelivery getWrappedModel() {
		return _projectDelivery;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _projectDelivery.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _projectDelivery.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_projectDelivery.resetOriginalValues();
	}

	private final ProjectDelivery _projectDelivery;
}