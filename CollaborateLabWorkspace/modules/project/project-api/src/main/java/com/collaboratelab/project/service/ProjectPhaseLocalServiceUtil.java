/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.project.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for ProjectPhase. This utility wraps
 * {@link com.collaboratelab.project.service.impl.ProjectPhaseLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Claudio Carlenzoli
 * @see ProjectPhaseLocalService
 * @see com.collaboratelab.project.service.base.ProjectPhaseLocalServiceBaseImpl
 * @see com.collaboratelab.project.service.impl.ProjectPhaseLocalServiceImpl
 * @generated
 */
@ProviderType
public class ProjectPhaseLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.collaboratelab.project.service.impl.ProjectPhaseLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the project phase to the database. Also notifies the appropriate model listeners.
	*
	* @param projectPhase the project phase
	* @return the project phase that was added
	*/
	public static com.collaboratelab.project.model.ProjectPhase addProjectPhase(
		com.collaboratelab.project.model.ProjectPhase projectPhase) {
		return getService().addProjectPhase(projectPhase);
	}

	/**
	* Creates a new project phase with the primary key. Does not add the project phase to the database.
	*
	* @param projectPhaseId the primary key for the new project phase
	* @return the new project phase
	*/
	public static com.collaboratelab.project.model.ProjectPhase createProjectPhase(
		long projectPhaseId) {
		return getService().createProjectPhase(projectPhaseId);
	}

	/**
	* Deletes the project phase from the database. Also notifies the appropriate model listeners.
	*
	* @param projectPhase the project phase
	* @return the project phase that was removed
	*/
	public static com.collaboratelab.project.model.ProjectPhase deleteProjectPhase(
		com.collaboratelab.project.model.ProjectPhase projectPhase) {
		return getService().deleteProjectPhase(projectPhase);
	}

	/**
	* Deletes the project phase with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param projectPhaseId the primary key of the project phase
	* @return the project phase that was removed
	* @throws PortalException if a project phase with the primary key could not be found
	*/
	public static com.collaboratelab.project.model.ProjectPhase deleteProjectPhase(
		long projectPhaseId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteProjectPhase(projectPhaseId);
	}

	public static com.collaboratelab.project.model.ProjectPhase fetchProjectPhase(
		long projectPhaseId) {
		return getService().fetchProjectPhase(projectPhaseId);
	}

	/**
	* Returns the project phase with the matching UUID and company.
	*
	* @param uuid the project phase's UUID
	* @param companyId the primary key of the company
	* @return the matching project phase, or <code>null</code> if a matching project phase could not be found
	*/
	public static com.collaboratelab.project.model.ProjectPhase fetchProjectPhaseByUuidAndCompanyId(
		java.lang.String uuid, long companyId) {
		return getService().fetchProjectPhaseByUuidAndCompanyId(uuid, companyId);
	}

	public static com.collaboratelab.project.model.ProjectPhase findByProjectPhaseId(
		long projectPhaseId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByProjectPhaseId(projectPhaseId);
	}

	public static com.collaboratelab.project.model.ProjectPhase findByProjectStructureIdIdentifier(
		long projectStructureId, java.lang.String identifier)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .findByProjectStructureIdIdentifier(projectStructureId,
			identifier);
	}

	/**
	* Returns the project phase with the primary key.
	*
	* @param projectPhaseId the primary key of the project phase
	* @return the project phase
	* @throws PortalException if a project phase with the primary key could not be found
	*/
	public static com.collaboratelab.project.model.ProjectPhase getProjectPhase(
		long projectPhaseId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getProjectPhase(projectPhaseId);
	}

	/**
	* Returns the project phase with the matching UUID and company.
	*
	* @param uuid the project phase's UUID
	* @param companyId the primary key of the company
	* @return the matching project phase
	* @throws PortalException if a matching project phase could not be found
	*/
	public static com.collaboratelab.project.model.ProjectPhase getProjectPhaseByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getProjectPhaseByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Updates the project phase in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param projectPhase the project phase
	* @return the project phase that was updated
	*/
	public static com.collaboratelab.project.model.ProjectPhase updateProjectPhase(
		com.collaboratelab.project.model.ProjectPhase projectPhase) {
		return getService().updateProjectPhase(projectPhase);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery getExportActionableDynamicQuery(
		com.liferay.exportimport.kernel.lar.PortletDataContext portletDataContext) {
		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of project phases.
	*
	* @return the number of project phases
	*/
	public static int getProjectPhasesCount() {
		return getService().getProjectPhasesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.project.model.impl.ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.project.model.impl.ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	public static java.util.List<com.collaboratelab.project.model.ProjectPhase> findByProjectStructureId(
		long projectStructureId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByProjectStructureId(projectStructureId);
	}

	/**
	* Returns a range of all the project phases.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.project.model.impl.ProjectPhaseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of project phases
	* @param end the upper bound of the range of project phases (not inclusive)
	* @return the range of project phases
	*/
	public static java.util.List<com.collaboratelab.project.model.ProjectPhase> getProjectPhases(
		int start, int end) {
		return getService().getProjectPhases(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static ProjectPhaseLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ProjectPhaseLocalService, ProjectPhaseLocalService> _serviceTracker =
		ServiceTrackerFactory.open(ProjectPhaseLocalService.class);
}