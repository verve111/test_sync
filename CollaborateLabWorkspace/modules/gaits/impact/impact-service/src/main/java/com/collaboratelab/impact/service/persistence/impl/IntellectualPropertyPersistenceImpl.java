/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.impact.exception.NoSuchIntellectualPropertyException;
import com.collaboratelab.impact.model.IntellectualProperty;
import com.collaboratelab.impact.model.impl.IntellectualPropertyImpl;
import com.collaboratelab.impact.model.impl.IntellectualPropertyModelImpl;
import com.collaboratelab.impact.service.persistence.IntellectualPropertyPersistence;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.CompanyProvider;
import com.liferay.portal.kernel.service.persistence.CompanyProviderWrapper;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the intellectual property service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see IntellectualPropertyPersistence
 * @see com.collaboratelab.impact.service.persistence.IntellectualPropertyUtil
 * @generated
 */
@ProviderType
public class IntellectualPropertyPersistenceImpl extends BasePersistenceImpl<IntellectualProperty>
	implements IntellectualPropertyPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link IntellectualPropertyUtil} to access the intellectual property persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = IntellectualPropertyImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(IntellectualPropertyModelImpl.ENTITY_CACHE_ENABLED,
			IntellectualPropertyModelImpl.FINDER_CACHE_ENABLED,
			IntellectualPropertyImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(IntellectualPropertyModelImpl.ENTITY_CACHE_ENABLED,
			IntellectualPropertyModelImpl.FINDER_CACHE_ENABLED,
			IntellectualPropertyImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(IntellectualPropertyModelImpl.ENTITY_CACHE_ENABLED,
			IntellectualPropertyModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID = new FinderPath(IntellectualPropertyModelImpl.ENTITY_CACHE_ENABLED,
			IntellectualPropertyModelImpl.FINDER_CACHE_ENABLED,
			IntellectualPropertyImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID = new FinderPath(IntellectualPropertyModelImpl.ENTITY_CACHE_ENABLED,
			IntellectualPropertyModelImpl.FINDER_CACHE_ENABLED,
			IntellectualPropertyImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] { String.class.getName() },
			IntellectualPropertyModelImpl.UUID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID = new FinderPath(IntellectualPropertyModelImpl.ENTITY_CACHE_ENABLED,
			IntellectualPropertyModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] { String.class.getName() });

	/**
	 * Returns all the intellectual properties where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching intellectual properties
	 */
	@Override
	public List<IntellectualProperty> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the intellectual properties where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of intellectual properties
	 * @param end the upper bound of the range of intellectual properties (not inclusive)
	 * @return the range of matching intellectual properties
	 */
	@Override
	public List<IntellectualProperty> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the intellectual properties where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of intellectual properties
	 * @param end the upper bound of the range of intellectual properties (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching intellectual properties
	 */
	@Override
	public List<IntellectualProperty> findByUuid(String uuid, int start,
		int end, OrderByComparator<IntellectualProperty> orderByComparator) {
		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the intellectual properties where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of intellectual properties
	 * @param end the upper bound of the range of intellectual properties (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching intellectual properties
	 */
	@Override
	public List<IntellectualProperty> findByUuid(String uuid, int start,
		int end, OrderByComparator<IntellectualProperty> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid, start, end, orderByComparator };
		}

		List<IntellectualProperty> list = null;

		if (retrieveFromCache) {
			list = (List<IntellectualProperty>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (IntellectualProperty intellectualProperty : list) {
					if (!Objects.equals(uuid, intellectualProperty.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_INTELLECTUALPROPERTY_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(IntellectualPropertyModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<IntellectualProperty>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<IntellectualProperty>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first intellectual property in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching intellectual property
	 * @throws NoSuchIntellectualPropertyException if a matching intellectual property could not be found
	 */
	@Override
	public IntellectualProperty findByUuid_First(String uuid,
		OrderByComparator<IntellectualProperty> orderByComparator)
		throws NoSuchIntellectualPropertyException {
		IntellectualProperty intellectualProperty = fetchByUuid_First(uuid,
				orderByComparator);

		if (intellectualProperty != null) {
			return intellectualProperty;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchIntellectualPropertyException(msg.toString());
	}

	/**
	 * Returns the first intellectual property in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching intellectual property, or <code>null</code> if a matching intellectual property could not be found
	 */
	@Override
	public IntellectualProperty fetchByUuid_First(String uuid,
		OrderByComparator<IntellectualProperty> orderByComparator) {
		List<IntellectualProperty> list = findByUuid(uuid, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last intellectual property in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching intellectual property
	 * @throws NoSuchIntellectualPropertyException if a matching intellectual property could not be found
	 */
	@Override
	public IntellectualProperty findByUuid_Last(String uuid,
		OrderByComparator<IntellectualProperty> orderByComparator)
		throws NoSuchIntellectualPropertyException {
		IntellectualProperty intellectualProperty = fetchByUuid_Last(uuid,
				orderByComparator);

		if (intellectualProperty != null) {
			return intellectualProperty;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchIntellectualPropertyException(msg.toString());
	}

	/**
	 * Returns the last intellectual property in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching intellectual property, or <code>null</code> if a matching intellectual property could not be found
	 */
	@Override
	public IntellectualProperty fetchByUuid_Last(String uuid,
		OrderByComparator<IntellectualProperty> orderByComparator) {
		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<IntellectualProperty> list = findByUuid(uuid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the intellectual properties before and after the current intellectual property in the ordered set where uuid = &#63;.
	 *
	 * @param intellectualPropertyId the primary key of the current intellectual property
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next intellectual property
	 * @throws NoSuchIntellectualPropertyException if a intellectual property with the primary key could not be found
	 */
	@Override
	public IntellectualProperty[] findByUuid_PrevAndNext(
		long intellectualPropertyId, String uuid,
		OrderByComparator<IntellectualProperty> orderByComparator)
		throws NoSuchIntellectualPropertyException {
		IntellectualProperty intellectualProperty = findByPrimaryKey(intellectualPropertyId);

		Session session = null;

		try {
			session = openSession();

			IntellectualProperty[] array = new IntellectualPropertyImpl[3];

			array[0] = getByUuid_PrevAndNext(session, intellectualProperty,
					uuid, orderByComparator, true);

			array[1] = intellectualProperty;

			array[2] = getByUuid_PrevAndNext(session, intellectualProperty,
					uuid, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected IntellectualProperty getByUuid_PrevAndNext(Session session,
		IntellectualProperty intellectualProperty, String uuid,
		OrderByComparator<IntellectualProperty> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_INTELLECTUALPROPERTY_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(IntellectualPropertyModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(intellectualProperty);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<IntellectualProperty> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the intellectual properties where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (IntellectualProperty intellectualProperty : findByUuid(uuid,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(intellectualProperty);
		}
	}

	/**
	 * Returns the number of intellectual properties where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching intellectual properties
	 */
	@Override
	public int countByUuid(String uuid) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID;

		Object[] finderArgs = new Object[] { uuid };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_INTELLECTUALPROPERTY_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_1 = "intellectualProperty.uuid IS NULL";
	private static final String _FINDER_COLUMN_UUID_UUID_2 = "intellectualProperty.uuid = ?";
	private static final String _FINDER_COLUMN_UUID_UUID_3 = "(intellectualProperty.uuid IS NULL OR intellectualProperty.uuid = '')";
	public static final FinderPath FINDER_PATH_FETCH_BY_UUID_G = new FinderPath(IntellectualPropertyModelImpl.ENTITY_CACHE_ENABLED,
			IntellectualPropertyModelImpl.FINDER_CACHE_ENABLED,
			IntellectualPropertyImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByUUID_G",
			new String[] { String.class.getName(), Long.class.getName() },
			IntellectualPropertyModelImpl.UUID_COLUMN_BITMASK |
			IntellectualPropertyModelImpl.GROUPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_G = new FinderPath(IntellectualPropertyModelImpl.ENTITY_CACHE_ENABLED,
			IntellectualPropertyModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns the intellectual property where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchIntellectualPropertyException} if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching intellectual property
	 * @throws NoSuchIntellectualPropertyException if a matching intellectual property could not be found
	 */
	@Override
	public IntellectualProperty findByUUID_G(String uuid, long groupId)
		throws NoSuchIntellectualPropertyException {
		IntellectualProperty intellectualProperty = fetchByUUID_G(uuid, groupId);

		if (intellectualProperty == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("uuid=");
			msg.append(uuid);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchIntellectualPropertyException(msg.toString());
		}

		return intellectualProperty;
	}

	/**
	 * Returns the intellectual property where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching intellectual property, or <code>null</code> if a matching intellectual property could not be found
	 */
	@Override
	public IntellectualProperty fetchByUUID_G(String uuid, long groupId) {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the intellectual property where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching intellectual property, or <code>null</code> if a matching intellectual property could not be found
	 */
	@Override
	public IntellectualProperty fetchByUUID_G(String uuid, long groupId,
		boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { uuid, groupId };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_UUID_G,
					finderArgs, this);
		}

		if (result instanceof IntellectualProperty) {
			IntellectualProperty intellectualProperty = (IntellectualProperty)result;

			if (!Objects.equals(uuid, intellectualProperty.getUuid()) ||
					(groupId != intellectualProperty.getGroupId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_INTELLECTUALPROPERTY_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				List<IntellectualProperty> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G,
						finderArgs, list);
				}
				else {
					IntellectualProperty intellectualProperty = list.get(0);

					result = intellectualProperty;

					cacheResult(intellectualProperty);

					if ((intellectualProperty.getUuid() == null) ||
							!intellectualProperty.getUuid().equals(uuid) ||
							(intellectualProperty.getGroupId() != groupId)) {
						finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G,
							finderArgs, intellectualProperty);
					}
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_UUID_G, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (IntellectualProperty)result;
		}
	}

	/**
	 * Removes the intellectual property where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the intellectual property that was removed
	 */
	@Override
	public IntellectualProperty removeByUUID_G(String uuid, long groupId)
		throws NoSuchIntellectualPropertyException {
		IntellectualProperty intellectualProperty = findByUUID_G(uuid, groupId);

		return remove(intellectualProperty);
	}

	/**
	 * Returns the number of intellectual properties where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching intellectual properties
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_G;

		Object[] finderArgs = new Object[] { uuid, groupId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_INTELLECTUALPROPERTY_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_1 = "intellectualProperty.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_2 = "intellectualProperty.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_3 = "(intellectualProperty.uuid IS NULL OR intellectualProperty.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 = "intellectualProperty.groupId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C = new FinderPath(IntellectualPropertyModelImpl.ENTITY_CACHE_ENABLED,
			IntellectualPropertyModelImpl.FINDER_CACHE_ENABLED,
			IntellectualPropertyImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C =
		new FinderPath(IntellectualPropertyModelImpl.ENTITY_CACHE_ENABLED,
			IntellectualPropertyModelImpl.FINDER_CACHE_ENABLED,
			IntellectualPropertyImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() },
			IntellectualPropertyModelImpl.UUID_COLUMN_BITMASK |
			IntellectualPropertyModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_C = new FinderPath(IntellectualPropertyModelImpl.ENTITY_CACHE_ENABLED,
			IntellectualPropertyModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns all the intellectual properties where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching intellectual properties
	 */
	@Override
	public List<IntellectualProperty> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(uuid, companyId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the intellectual properties where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of intellectual properties
	 * @param end the upper bound of the range of intellectual properties (not inclusive)
	 * @return the range of matching intellectual properties
	 */
	@Override
	public List<IntellectualProperty> findByUuid_C(String uuid, long companyId,
		int start, int end) {
		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the intellectual properties where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of intellectual properties
	 * @param end the upper bound of the range of intellectual properties (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching intellectual properties
	 */
	@Override
	public List<IntellectualProperty> findByUuid_C(String uuid, long companyId,
		int start, int end,
		OrderByComparator<IntellectualProperty> orderByComparator) {
		return findByUuid_C(uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the intellectual properties where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of intellectual properties
	 * @param end the upper bound of the range of intellectual properties (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching intellectual properties
	 */
	@Override
	public List<IntellectualProperty> findByUuid_C(String uuid, long companyId,
		int start, int end,
		OrderByComparator<IntellectualProperty> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] { uuid, companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] {
					uuid, companyId,
					
					start, end, orderByComparator
				};
		}

		List<IntellectualProperty> list = null;

		if (retrieveFromCache) {
			list = (List<IntellectualProperty>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (IntellectualProperty intellectualProperty : list) {
					if (!Objects.equals(uuid, intellectualProperty.getUuid()) ||
							(companyId != intellectualProperty.getCompanyId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_INTELLECTUALPROPERTY_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(IntellectualPropertyModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				if (!pagination) {
					list = (List<IntellectualProperty>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<IntellectualProperty>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first intellectual property in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching intellectual property
	 * @throws NoSuchIntellectualPropertyException if a matching intellectual property could not be found
	 */
	@Override
	public IntellectualProperty findByUuid_C_First(String uuid, long companyId,
		OrderByComparator<IntellectualProperty> orderByComparator)
		throws NoSuchIntellectualPropertyException {
		IntellectualProperty intellectualProperty = fetchByUuid_C_First(uuid,
				companyId, orderByComparator);

		if (intellectualProperty != null) {
			return intellectualProperty;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchIntellectualPropertyException(msg.toString());
	}

	/**
	 * Returns the first intellectual property in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching intellectual property, or <code>null</code> if a matching intellectual property could not be found
	 */
	@Override
	public IntellectualProperty fetchByUuid_C_First(String uuid,
		long companyId,
		OrderByComparator<IntellectualProperty> orderByComparator) {
		List<IntellectualProperty> list = findByUuid_C(uuid, companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last intellectual property in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching intellectual property
	 * @throws NoSuchIntellectualPropertyException if a matching intellectual property could not be found
	 */
	@Override
	public IntellectualProperty findByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<IntellectualProperty> orderByComparator)
		throws NoSuchIntellectualPropertyException {
		IntellectualProperty intellectualProperty = fetchByUuid_C_Last(uuid,
				companyId, orderByComparator);

		if (intellectualProperty != null) {
			return intellectualProperty;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchIntellectualPropertyException(msg.toString());
	}

	/**
	 * Returns the last intellectual property in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching intellectual property, or <code>null</code> if a matching intellectual property could not be found
	 */
	@Override
	public IntellectualProperty fetchByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<IntellectualProperty> orderByComparator) {
		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<IntellectualProperty> list = findByUuid_C(uuid, companyId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the intellectual properties before and after the current intellectual property in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param intellectualPropertyId the primary key of the current intellectual property
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next intellectual property
	 * @throws NoSuchIntellectualPropertyException if a intellectual property with the primary key could not be found
	 */
	@Override
	public IntellectualProperty[] findByUuid_C_PrevAndNext(
		long intellectualPropertyId, String uuid, long companyId,
		OrderByComparator<IntellectualProperty> orderByComparator)
		throws NoSuchIntellectualPropertyException {
		IntellectualProperty intellectualProperty = findByPrimaryKey(intellectualPropertyId);

		Session session = null;

		try {
			session = openSession();

			IntellectualProperty[] array = new IntellectualPropertyImpl[3];

			array[0] = getByUuid_C_PrevAndNext(session, intellectualProperty,
					uuid, companyId, orderByComparator, true);

			array[1] = intellectualProperty;

			array[2] = getByUuid_C_PrevAndNext(session, intellectualProperty,
					uuid, companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected IntellectualProperty getByUuid_C_PrevAndNext(Session session,
		IntellectualProperty intellectualProperty, String uuid, long companyId,
		OrderByComparator<IntellectualProperty> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_INTELLECTUALPROPERTY_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(IntellectualPropertyModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(intellectualProperty);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<IntellectualProperty> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the intellectual properties where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (IntellectualProperty intellectualProperty : findByUuid_C(uuid,
				companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(intellectualProperty);
		}
	}

	/**
	 * Returns the number of intellectual properties where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching intellectual properties
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_C;

		Object[] finderArgs = new Object[] { uuid, companyId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_INTELLECTUALPROPERTY_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_1 = "intellectualProperty.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_2 = "intellectualProperty.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_3 = "(intellectualProperty.uuid IS NULL OR intellectualProperty.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 = "intellectualProperty.companyId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_INTELLECTUALPROPERTYID = new FinderPath(IntellectualPropertyModelImpl.ENTITY_CACHE_ENABLED,
			IntellectualPropertyModelImpl.FINDER_CACHE_ENABLED,
			IntellectualPropertyImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByIntellectualPropertyId",
			new String[] { Long.class.getName() },
			IntellectualPropertyModelImpl.INTELLECTUALPROPERTYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_INTELLECTUALPROPERTYID = new FinderPath(IntellectualPropertyModelImpl.ENTITY_CACHE_ENABLED,
			IntellectualPropertyModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByIntellectualPropertyId",
			new String[] { Long.class.getName() });

	/**
	 * Returns the intellectual property where intellectualPropertyId = &#63; or throws a {@link NoSuchIntellectualPropertyException} if it could not be found.
	 *
	 * @param intellectualPropertyId the intellectual property ID
	 * @return the matching intellectual property
	 * @throws NoSuchIntellectualPropertyException if a matching intellectual property could not be found
	 */
	@Override
	public IntellectualProperty findByIntellectualPropertyId(
		long intellectualPropertyId) throws NoSuchIntellectualPropertyException {
		IntellectualProperty intellectualProperty = fetchByIntellectualPropertyId(intellectualPropertyId);

		if (intellectualProperty == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("intellectualPropertyId=");
			msg.append(intellectualPropertyId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchIntellectualPropertyException(msg.toString());
		}

		return intellectualProperty;
	}

	/**
	 * Returns the intellectual property where intellectualPropertyId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param intellectualPropertyId the intellectual property ID
	 * @return the matching intellectual property, or <code>null</code> if a matching intellectual property could not be found
	 */
	@Override
	public IntellectualProperty fetchByIntellectualPropertyId(
		long intellectualPropertyId) {
		return fetchByIntellectualPropertyId(intellectualPropertyId, true);
	}

	/**
	 * Returns the intellectual property where intellectualPropertyId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param intellectualPropertyId the intellectual property ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching intellectual property, or <code>null</code> if a matching intellectual property could not be found
	 */
	@Override
	public IntellectualProperty fetchByIntellectualPropertyId(
		long intellectualPropertyId, boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { intellectualPropertyId };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_INTELLECTUALPROPERTYID,
					finderArgs, this);
		}

		if (result instanceof IntellectualProperty) {
			IntellectualProperty intellectualProperty = (IntellectualProperty)result;

			if ((intellectualPropertyId != intellectualProperty.getIntellectualPropertyId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_INTELLECTUALPROPERTY_WHERE);

			query.append(_FINDER_COLUMN_INTELLECTUALPROPERTYID_INTELLECTUALPROPERTYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(intellectualPropertyId);

				List<IntellectualProperty> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_INTELLECTUALPROPERTYID,
						finderArgs, list);
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							_log.warn(
								"IntellectualPropertyPersistenceImpl.fetchByIntellectualPropertyId(long, boolean) with parameters (" +
								StringUtil.merge(finderArgs) +
								") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					IntellectualProperty intellectualProperty = list.get(0);

					result = intellectualProperty;

					cacheResult(intellectualProperty);

					if ((intellectualProperty.getIntellectualPropertyId() != intellectualPropertyId)) {
						finderCache.putResult(FINDER_PATH_FETCH_BY_INTELLECTUALPROPERTYID,
							finderArgs, intellectualProperty);
					}
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_INTELLECTUALPROPERTYID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (IntellectualProperty)result;
		}
	}

	/**
	 * Removes the intellectual property where intellectualPropertyId = &#63; from the database.
	 *
	 * @param intellectualPropertyId the intellectual property ID
	 * @return the intellectual property that was removed
	 */
	@Override
	public IntellectualProperty removeByIntellectualPropertyId(
		long intellectualPropertyId) throws NoSuchIntellectualPropertyException {
		IntellectualProperty intellectualProperty = findByIntellectualPropertyId(intellectualPropertyId);

		return remove(intellectualProperty);
	}

	/**
	 * Returns the number of intellectual properties where intellectualPropertyId = &#63;.
	 *
	 * @param intellectualPropertyId the intellectual property ID
	 * @return the number of matching intellectual properties
	 */
	@Override
	public int countByIntellectualPropertyId(long intellectualPropertyId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_INTELLECTUALPROPERTYID;

		Object[] finderArgs = new Object[] { intellectualPropertyId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_INTELLECTUALPROPERTY_WHERE);

			query.append(_FINDER_COLUMN_INTELLECTUALPROPERTYID_INTELLECTUALPROPERTYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(intellectualPropertyId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_INTELLECTUALPROPERTYID_INTELLECTUALPROPERTYID_2 =
		"intellectualProperty.intellectualPropertyId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_GROUPID = new FinderPath(IntellectualPropertyModelImpl.ENTITY_CACHE_ENABLED,
			IntellectualPropertyModelImpl.FINDER_CACHE_ENABLED,
			IntellectualPropertyImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByGroupId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID =
		new FinderPath(IntellectualPropertyModelImpl.ENTITY_CACHE_ENABLED,
			IntellectualPropertyModelImpl.FINDER_CACHE_ENABLED,
			IntellectualPropertyImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByGroupId",
			new String[] { Long.class.getName() },
			IntellectualPropertyModelImpl.GROUPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_GROUPID = new FinderPath(IntellectualPropertyModelImpl.ENTITY_CACHE_ENABLED,
			IntellectualPropertyModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByGroupId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the intellectual properties where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching intellectual properties
	 */
	@Override
	public List<IntellectualProperty> findByGroupId(long groupId) {
		return findByGroupId(groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the intellectual properties where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of intellectual properties
	 * @param end the upper bound of the range of intellectual properties (not inclusive)
	 * @return the range of matching intellectual properties
	 */
	@Override
	public List<IntellectualProperty> findByGroupId(long groupId, int start,
		int end) {
		return findByGroupId(groupId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the intellectual properties where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of intellectual properties
	 * @param end the upper bound of the range of intellectual properties (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching intellectual properties
	 */
	@Override
	public List<IntellectualProperty> findByGroupId(long groupId, int start,
		int end, OrderByComparator<IntellectualProperty> orderByComparator) {
		return findByGroupId(groupId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the intellectual properties where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of intellectual properties
	 * @param end the upper bound of the range of intellectual properties (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching intellectual properties
	 */
	@Override
	public List<IntellectualProperty> findByGroupId(long groupId, int start,
		int end, OrderByComparator<IntellectualProperty> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID;
			finderArgs = new Object[] { groupId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_GROUPID;
			finderArgs = new Object[] { groupId, start, end, orderByComparator };
		}

		List<IntellectualProperty> list = null;

		if (retrieveFromCache) {
			list = (List<IntellectualProperty>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (IntellectualProperty intellectualProperty : list) {
					if ((groupId != intellectualProperty.getGroupId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_INTELLECTUALPROPERTY_WHERE);

			query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(IntellectualPropertyModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				if (!pagination) {
					list = (List<IntellectualProperty>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<IntellectualProperty>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first intellectual property in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching intellectual property
	 * @throws NoSuchIntellectualPropertyException if a matching intellectual property could not be found
	 */
	@Override
	public IntellectualProperty findByGroupId_First(long groupId,
		OrderByComparator<IntellectualProperty> orderByComparator)
		throws NoSuchIntellectualPropertyException {
		IntellectualProperty intellectualProperty = fetchByGroupId_First(groupId,
				orderByComparator);

		if (intellectualProperty != null) {
			return intellectualProperty;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("groupId=");
		msg.append(groupId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchIntellectualPropertyException(msg.toString());
	}

	/**
	 * Returns the first intellectual property in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching intellectual property, or <code>null</code> if a matching intellectual property could not be found
	 */
	@Override
	public IntellectualProperty fetchByGroupId_First(long groupId,
		OrderByComparator<IntellectualProperty> orderByComparator) {
		List<IntellectualProperty> list = findByGroupId(groupId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last intellectual property in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching intellectual property
	 * @throws NoSuchIntellectualPropertyException if a matching intellectual property could not be found
	 */
	@Override
	public IntellectualProperty findByGroupId_Last(long groupId,
		OrderByComparator<IntellectualProperty> orderByComparator)
		throws NoSuchIntellectualPropertyException {
		IntellectualProperty intellectualProperty = fetchByGroupId_Last(groupId,
				orderByComparator);

		if (intellectualProperty != null) {
			return intellectualProperty;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("groupId=");
		msg.append(groupId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchIntellectualPropertyException(msg.toString());
	}

	/**
	 * Returns the last intellectual property in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching intellectual property, or <code>null</code> if a matching intellectual property could not be found
	 */
	@Override
	public IntellectualProperty fetchByGroupId_Last(long groupId,
		OrderByComparator<IntellectualProperty> orderByComparator) {
		int count = countByGroupId(groupId);

		if (count == 0) {
			return null;
		}

		List<IntellectualProperty> list = findByGroupId(groupId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the intellectual properties before and after the current intellectual property in the ordered set where groupId = &#63;.
	 *
	 * @param intellectualPropertyId the primary key of the current intellectual property
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next intellectual property
	 * @throws NoSuchIntellectualPropertyException if a intellectual property with the primary key could not be found
	 */
	@Override
	public IntellectualProperty[] findByGroupId_PrevAndNext(
		long intellectualPropertyId, long groupId,
		OrderByComparator<IntellectualProperty> orderByComparator)
		throws NoSuchIntellectualPropertyException {
		IntellectualProperty intellectualProperty = findByPrimaryKey(intellectualPropertyId);

		Session session = null;

		try {
			session = openSession();

			IntellectualProperty[] array = new IntellectualPropertyImpl[3];

			array[0] = getByGroupId_PrevAndNext(session, intellectualProperty,
					groupId, orderByComparator, true);

			array[1] = intellectualProperty;

			array[2] = getByGroupId_PrevAndNext(session, intellectualProperty,
					groupId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected IntellectualProperty getByGroupId_PrevAndNext(Session session,
		IntellectualProperty intellectualProperty, long groupId,
		OrderByComparator<IntellectualProperty> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_INTELLECTUALPROPERTY_WHERE);

		query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(IntellectualPropertyModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(groupId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(intellectualProperty);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<IntellectualProperty> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the intellectual properties where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	@Override
	public void removeByGroupId(long groupId) {
		for (IntellectualProperty intellectualProperty : findByGroupId(
				groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(intellectualProperty);
		}
	}

	/**
	 * Returns the number of intellectual properties where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching intellectual properties
	 */
	@Override
	public int countByGroupId(long groupId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_GROUPID;

		Object[] finderArgs = new Object[] { groupId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_INTELLECTUALPROPERTY_WHERE);

			query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_GROUPID_GROUPID_2 = "intellectualProperty.groupId = ?";

	public IntellectualPropertyPersistenceImpl() {
		setModelClass(IntellectualProperty.class);
	}

	/**
	 * Caches the intellectual property in the entity cache if it is enabled.
	 *
	 * @param intellectualProperty the intellectual property
	 */
	@Override
	public void cacheResult(IntellectualProperty intellectualProperty) {
		entityCache.putResult(IntellectualPropertyModelImpl.ENTITY_CACHE_ENABLED,
			IntellectualPropertyImpl.class,
			intellectualProperty.getPrimaryKey(), intellectualProperty);

		finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G,
			new Object[] {
				intellectualProperty.getUuid(),
				intellectualProperty.getGroupId()
			}, intellectualProperty);

		finderCache.putResult(FINDER_PATH_FETCH_BY_INTELLECTUALPROPERTYID,
			new Object[] { intellectualProperty.getIntellectualPropertyId() },
			intellectualProperty);

		intellectualProperty.resetOriginalValues();
	}

	/**
	 * Caches the intellectual properties in the entity cache if it is enabled.
	 *
	 * @param intellectualProperties the intellectual properties
	 */
	@Override
	public void cacheResult(List<IntellectualProperty> intellectualProperties) {
		for (IntellectualProperty intellectualProperty : intellectualProperties) {
			if (entityCache.getResult(
						IntellectualPropertyModelImpl.ENTITY_CACHE_ENABLED,
						IntellectualPropertyImpl.class,
						intellectualProperty.getPrimaryKey()) == null) {
				cacheResult(intellectualProperty);
			}
			else {
				intellectualProperty.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all intellectual properties.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(IntellectualPropertyImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the intellectual property.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(IntellectualProperty intellectualProperty) {
		entityCache.removeResult(IntellectualPropertyModelImpl.ENTITY_CACHE_ENABLED,
			IntellectualPropertyImpl.class, intellectualProperty.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache((IntellectualPropertyModelImpl)intellectualProperty);
	}

	@Override
	public void clearCache(List<IntellectualProperty> intellectualProperties) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (IntellectualProperty intellectualProperty : intellectualProperties) {
			entityCache.removeResult(IntellectualPropertyModelImpl.ENTITY_CACHE_ENABLED,
				IntellectualPropertyImpl.class,
				intellectualProperty.getPrimaryKey());

			clearUniqueFindersCache((IntellectualPropertyModelImpl)intellectualProperty);
		}
	}

	protected void cacheUniqueFindersCache(
		IntellectualPropertyModelImpl intellectualPropertyModelImpl,
		boolean isNew) {
		if (isNew) {
			Object[] args = new Object[] {
					intellectualPropertyModelImpl.getUuid(),
					intellectualPropertyModelImpl.getGroupId()
				};

			finderCache.putResult(FINDER_PATH_COUNT_BY_UUID_G, args,
				Long.valueOf(1));
			finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G, args,
				intellectualPropertyModelImpl);

			args = new Object[] {
					intellectualPropertyModelImpl.getIntellectualPropertyId()
				};

			finderCache.putResult(FINDER_PATH_COUNT_BY_INTELLECTUALPROPERTYID,
				args, Long.valueOf(1));
			finderCache.putResult(FINDER_PATH_FETCH_BY_INTELLECTUALPROPERTYID,
				args, intellectualPropertyModelImpl);
		}
		else {
			if ((intellectualPropertyModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_UUID_G.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						intellectualPropertyModelImpl.getUuid(),
						intellectualPropertyModelImpl.getGroupId()
					};

				finderCache.putResult(FINDER_PATH_COUNT_BY_UUID_G, args,
					Long.valueOf(1));
				finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G, args,
					intellectualPropertyModelImpl);
			}

			if ((intellectualPropertyModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_INTELLECTUALPROPERTYID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						intellectualPropertyModelImpl.getIntellectualPropertyId()
					};

				finderCache.putResult(FINDER_PATH_COUNT_BY_INTELLECTUALPROPERTYID,
					args, Long.valueOf(1));
				finderCache.putResult(FINDER_PATH_FETCH_BY_INTELLECTUALPROPERTYID,
					args, intellectualPropertyModelImpl);
			}
		}
	}

	protected void clearUniqueFindersCache(
		IntellectualPropertyModelImpl intellectualPropertyModelImpl) {
		Object[] args = new Object[] {
				intellectualPropertyModelImpl.getUuid(),
				intellectualPropertyModelImpl.getGroupId()
			};

		finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
		finderCache.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);

		if ((intellectualPropertyModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_UUID_G.getColumnBitmask()) != 0) {
			args = new Object[] {
					intellectualPropertyModelImpl.getOriginalUuid(),
					intellectualPropertyModelImpl.getOriginalGroupId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);
		}

		args = new Object[] {
				intellectualPropertyModelImpl.getIntellectualPropertyId()
			};

		finderCache.removeResult(FINDER_PATH_COUNT_BY_INTELLECTUALPROPERTYID,
			args);
		finderCache.removeResult(FINDER_PATH_FETCH_BY_INTELLECTUALPROPERTYID,
			args);

		if ((intellectualPropertyModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_INTELLECTUALPROPERTYID.getColumnBitmask()) != 0) {
			args = new Object[] {
					intellectualPropertyModelImpl.getOriginalIntellectualPropertyId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_INTELLECTUALPROPERTYID,
				args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_INTELLECTUALPROPERTYID,
				args);
		}
	}

	/**
	 * Creates a new intellectual property with the primary key. Does not add the intellectual property to the database.
	 *
	 * @param intellectualPropertyId the primary key for the new intellectual property
	 * @return the new intellectual property
	 */
	@Override
	public IntellectualProperty create(long intellectualPropertyId) {
		IntellectualProperty intellectualProperty = new IntellectualPropertyImpl();

		intellectualProperty.setNew(true);
		intellectualProperty.setPrimaryKey(intellectualPropertyId);

		String uuid = PortalUUIDUtil.generate();

		intellectualProperty.setUuid(uuid);

		intellectualProperty.setCompanyId(companyProvider.getCompanyId());

		return intellectualProperty;
	}

	/**
	 * Removes the intellectual property with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param intellectualPropertyId the primary key of the intellectual property
	 * @return the intellectual property that was removed
	 * @throws NoSuchIntellectualPropertyException if a intellectual property with the primary key could not be found
	 */
	@Override
	public IntellectualProperty remove(long intellectualPropertyId)
		throws NoSuchIntellectualPropertyException {
		return remove((Serializable)intellectualPropertyId);
	}

	/**
	 * Removes the intellectual property with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the intellectual property
	 * @return the intellectual property that was removed
	 * @throws NoSuchIntellectualPropertyException if a intellectual property with the primary key could not be found
	 */
	@Override
	public IntellectualProperty remove(Serializable primaryKey)
		throws NoSuchIntellectualPropertyException {
		Session session = null;

		try {
			session = openSession();

			IntellectualProperty intellectualProperty = (IntellectualProperty)session.get(IntellectualPropertyImpl.class,
					primaryKey);

			if (intellectualProperty == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchIntellectualPropertyException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(intellectualProperty);
		}
		catch (NoSuchIntellectualPropertyException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected IntellectualProperty removeImpl(
		IntellectualProperty intellectualProperty) {
		intellectualProperty = toUnwrappedModel(intellectualProperty);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(intellectualProperty)) {
				intellectualProperty = (IntellectualProperty)session.get(IntellectualPropertyImpl.class,
						intellectualProperty.getPrimaryKeyObj());
			}

			if (intellectualProperty != null) {
				session.delete(intellectualProperty);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (intellectualProperty != null) {
			clearCache(intellectualProperty);
		}

		return intellectualProperty;
	}

	@Override
	public IntellectualProperty updateImpl(
		IntellectualProperty intellectualProperty) {
		intellectualProperty = toUnwrappedModel(intellectualProperty);

		boolean isNew = intellectualProperty.isNew();

		IntellectualPropertyModelImpl intellectualPropertyModelImpl = (IntellectualPropertyModelImpl)intellectualProperty;

		if (Validator.isNull(intellectualProperty.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			intellectualProperty.setUuid(uuid);
		}

		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();

		Date now = new Date();

		if (isNew && (intellectualProperty.getCreateDate() == null)) {
			if (serviceContext == null) {
				intellectualProperty.setCreateDate(now);
			}
			else {
				intellectualProperty.setCreateDate(serviceContext.getCreateDate(
						now));
			}
		}

		if (!intellectualPropertyModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				intellectualProperty.setModifiedDate(now);
			}
			else {
				intellectualProperty.setModifiedDate(serviceContext.getModifiedDate(
						now));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (intellectualProperty.isNew()) {
				session.save(intellectualProperty);

				intellectualProperty.setNew(false);
			}
			else {
				intellectualProperty = (IntellectualProperty)session.merge(intellectualProperty);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !IntellectualPropertyModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((intellectualPropertyModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						intellectualPropertyModelImpl.getOriginalUuid()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);

				args = new Object[] { intellectualPropertyModelImpl.getUuid() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);
			}

			if ((intellectualPropertyModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						intellectualPropertyModelImpl.getOriginalUuid(),
						intellectualPropertyModelImpl.getOriginalCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);

				args = new Object[] {
						intellectualPropertyModelImpl.getUuid(),
						intellectualPropertyModelImpl.getCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);
			}

			if ((intellectualPropertyModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						intellectualPropertyModelImpl.getOriginalGroupId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_GROUPID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID,
					args);

				args = new Object[] { intellectualPropertyModelImpl.getGroupId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_GROUPID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID,
					args);
			}
		}

		entityCache.putResult(IntellectualPropertyModelImpl.ENTITY_CACHE_ENABLED,
			IntellectualPropertyImpl.class,
			intellectualProperty.getPrimaryKey(), intellectualProperty, false);

		clearUniqueFindersCache(intellectualPropertyModelImpl);
		cacheUniqueFindersCache(intellectualPropertyModelImpl, isNew);

		intellectualProperty.resetOriginalValues();

		return intellectualProperty;
	}

	protected IntellectualProperty toUnwrappedModel(
		IntellectualProperty intellectualProperty) {
		if (intellectualProperty instanceof IntellectualPropertyImpl) {
			return intellectualProperty;
		}

		IntellectualPropertyImpl intellectualPropertyImpl = new IntellectualPropertyImpl();

		intellectualPropertyImpl.setNew(intellectualProperty.isNew());
		intellectualPropertyImpl.setPrimaryKey(intellectualProperty.getPrimaryKey());

		intellectualPropertyImpl.setUuid(intellectualProperty.getUuid());
		intellectualPropertyImpl.setIntellectualPropertyId(intellectualProperty.getIntellectualPropertyId());
		intellectualPropertyImpl.setGroupId(intellectualProperty.getGroupId());
		intellectualPropertyImpl.setCompanyId(intellectualProperty.getCompanyId());
		intellectualPropertyImpl.setUserId(intellectualProperty.getUserId());
		intellectualPropertyImpl.setUserName(intellectualProperty.getUserName());
		intellectualPropertyImpl.setCreateDate(intellectualProperty.getCreateDate());
		intellectualPropertyImpl.setModifiedDate(intellectualProperty.getModifiedDate());
		intellectualPropertyImpl.setOneStFileDate(intellectualProperty.getOneStFileDate());
		intellectualPropertyImpl.setAsOfDate(intellectualProperty.getAsOfDate());
		intellectualPropertyImpl.setTitle(intellectualProperty.getTitle());
		intellectualPropertyImpl.setStage(intellectualProperty.getStage());
		intellectualPropertyImpl.setLicenseStatus(intellectualProperty.getLicenseStatus());

		return intellectualPropertyImpl;
	}

	/**
	 * Returns the intellectual property with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the intellectual property
	 * @return the intellectual property
	 * @throws NoSuchIntellectualPropertyException if a intellectual property with the primary key could not be found
	 */
	@Override
	public IntellectualProperty findByPrimaryKey(Serializable primaryKey)
		throws NoSuchIntellectualPropertyException {
		IntellectualProperty intellectualProperty = fetchByPrimaryKey(primaryKey);

		if (intellectualProperty == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchIntellectualPropertyException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return intellectualProperty;
	}

	/**
	 * Returns the intellectual property with the primary key or throws a {@link NoSuchIntellectualPropertyException} if it could not be found.
	 *
	 * @param intellectualPropertyId the primary key of the intellectual property
	 * @return the intellectual property
	 * @throws NoSuchIntellectualPropertyException if a intellectual property with the primary key could not be found
	 */
	@Override
	public IntellectualProperty findByPrimaryKey(long intellectualPropertyId)
		throws NoSuchIntellectualPropertyException {
		return findByPrimaryKey((Serializable)intellectualPropertyId);
	}

	/**
	 * Returns the intellectual property with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the intellectual property
	 * @return the intellectual property, or <code>null</code> if a intellectual property with the primary key could not be found
	 */
	@Override
	public IntellectualProperty fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(IntellectualPropertyModelImpl.ENTITY_CACHE_ENABLED,
				IntellectualPropertyImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		IntellectualProperty intellectualProperty = (IntellectualProperty)serializable;

		if (intellectualProperty == null) {
			Session session = null;

			try {
				session = openSession();

				intellectualProperty = (IntellectualProperty)session.get(IntellectualPropertyImpl.class,
						primaryKey);

				if (intellectualProperty != null) {
					cacheResult(intellectualProperty);
				}
				else {
					entityCache.putResult(IntellectualPropertyModelImpl.ENTITY_CACHE_ENABLED,
						IntellectualPropertyImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(IntellectualPropertyModelImpl.ENTITY_CACHE_ENABLED,
					IntellectualPropertyImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return intellectualProperty;
	}

	/**
	 * Returns the intellectual property with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param intellectualPropertyId the primary key of the intellectual property
	 * @return the intellectual property, or <code>null</code> if a intellectual property with the primary key could not be found
	 */
	@Override
	public IntellectualProperty fetchByPrimaryKey(long intellectualPropertyId) {
		return fetchByPrimaryKey((Serializable)intellectualPropertyId);
	}

	@Override
	public Map<Serializable, IntellectualProperty> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, IntellectualProperty> map = new HashMap<Serializable, IntellectualProperty>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			IntellectualProperty intellectualProperty = fetchByPrimaryKey(primaryKey);

			if (intellectualProperty != null) {
				map.put(primaryKey, intellectualProperty);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(IntellectualPropertyModelImpl.ENTITY_CACHE_ENABLED,
					IntellectualPropertyImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (IntellectualProperty)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_INTELLECTUALPROPERTY_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(String.valueOf(primaryKey));

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (IntellectualProperty intellectualProperty : (List<IntellectualProperty>)q.list()) {
				map.put(intellectualProperty.getPrimaryKeyObj(),
					intellectualProperty);

				cacheResult(intellectualProperty);

				uncachedPrimaryKeys.remove(intellectualProperty.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(IntellectualPropertyModelImpl.ENTITY_CACHE_ENABLED,
					IntellectualPropertyImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the intellectual properties.
	 *
	 * @return the intellectual properties
	 */
	@Override
	public List<IntellectualProperty> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the intellectual properties.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of intellectual properties
	 * @param end the upper bound of the range of intellectual properties (not inclusive)
	 * @return the range of intellectual properties
	 */
	@Override
	public List<IntellectualProperty> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the intellectual properties.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of intellectual properties
	 * @param end the upper bound of the range of intellectual properties (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of intellectual properties
	 */
	@Override
	public List<IntellectualProperty> findAll(int start, int end,
		OrderByComparator<IntellectualProperty> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the intellectual properties.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of intellectual properties
	 * @param end the upper bound of the range of intellectual properties (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of intellectual properties
	 */
	@Override
	public List<IntellectualProperty> findAll(int start, int end,
		OrderByComparator<IntellectualProperty> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<IntellectualProperty> list = null;

		if (retrieveFromCache) {
			list = (List<IntellectualProperty>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_INTELLECTUALPROPERTY);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_INTELLECTUALPROPERTY;

				if (pagination) {
					sql = sql.concat(IntellectualPropertyModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<IntellectualProperty>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<IntellectualProperty>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the intellectual properties from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (IntellectualProperty intellectualProperty : findAll()) {
			remove(intellectualProperty);
		}
	}

	/**
	 * Returns the number of intellectual properties.
	 *
	 * @return the number of intellectual properties
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_INTELLECTUALPROPERTY);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return IntellectualPropertyModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the intellectual property persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(IntellectualPropertyImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = CompanyProviderWrapper.class)
	protected CompanyProvider companyProvider;
	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_INTELLECTUALPROPERTY = "SELECT intellectualProperty FROM IntellectualProperty intellectualProperty";
	private static final String _SQL_SELECT_INTELLECTUALPROPERTY_WHERE_PKS_IN = "SELECT intellectualProperty FROM IntellectualProperty intellectualProperty WHERE intellectualPropertyId IN (";
	private static final String _SQL_SELECT_INTELLECTUALPROPERTY_WHERE = "SELECT intellectualProperty FROM IntellectualProperty intellectualProperty WHERE ";
	private static final String _SQL_COUNT_INTELLECTUALPROPERTY = "SELECT COUNT(intellectualProperty) FROM IntellectualProperty intellectualProperty";
	private static final String _SQL_COUNT_INTELLECTUALPROPERTY_WHERE = "SELECT COUNT(intellectualProperty) FROM IntellectualProperty intellectualProperty WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "intellectualProperty.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No IntellectualProperty exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No IntellectualProperty exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(IntellectualPropertyPersistenceImpl.class);
	private static final Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"uuid"
			});
}