/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.impact.model.Publications;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Publications in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Publications
 * @generated
 */
@ProviderType
public class PublicationsCacheModel implements CacheModel<Publications>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PublicationsCacheModel)) {
			return false;
		}

		PublicationsCacheModel publicationsCacheModel = (PublicationsCacheModel)obj;

		if (publicationsId == publicationsCacheModel.publicationsId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, publicationsId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", publicationsId=");
		sb.append(publicationsId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", titleOfPublications=");
		sb.append(titleOfPublications);
		sb.append(", date=");
		sb.append(date);
		sb.append(", URL=");
		sb.append(URL);
		sb.append(", comments=");
		sb.append(comments);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Publications toEntityModel() {
		PublicationsImpl publicationsImpl = new PublicationsImpl();

		if (uuid == null) {
			publicationsImpl.setUuid(StringPool.BLANK);
		}
		else {
			publicationsImpl.setUuid(uuid);
		}

		publicationsImpl.setPublicationsId(publicationsId);
		publicationsImpl.setGroupId(groupId);
		publicationsImpl.setCompanyId(companyId);
		publicationsImpl.setUserId(userId);

		if (userName == null) {
			publicationsImpl.setUserName(StringPool.BLANK);
		}
		else {
			publicationsImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			publicationsImpl.setCreateDate(null);
		}
		else {
			publicationsImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			publicationsImpl.setModifiedDate(null);
		}
		else {
			publicationsImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (titleOfPublications == null) {
			publicationsImpl.setTitleOfPublications(StringPool.BLANK);
		}
		else {
			publicationsImpl.setTitleOfPublications(titleOfPublications);
		}

		if (date == Long.MIN_VALUE) {
			publicationsImpl.setDate(null);
		}
		else {
			publicationsImpl.setDate(new Date(date));
		}

		if (URL == null) {
			publicationsImpl.setURL(StringPool.BLANK);
		}
		else {
			publicationsImpl.setURL(URL);
		}

		if (comments == null) {
			publicationsImpl.setComments(StringPool.BLANK);
		}
		else {
			publicationsImpl.setComments(comments);
		}

		publicationsImpl.resetOriginalValues();

		return publicationsImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		publicationsId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		titleOfPublications = objectInput.readUTF();
		date = objectInput.readLong();
		URL = objectInput.readUTF();
		comments = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(publicationsId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (titleOfPublications == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(titleOfPublications);
		}

		objectOutput.writeLong(date);

		if (URL == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(URL);
		}

		if (comments == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(comments);
		}
	}

	public String uuid;
	public long publicationsId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String titleOfPublications;
	public long date;
	public String URL;
	public String comments;
}