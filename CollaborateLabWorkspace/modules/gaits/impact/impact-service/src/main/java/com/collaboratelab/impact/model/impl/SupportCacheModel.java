/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.impact.model.Support;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Support in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Support
 * @generated
 */
@ProviderType
public class SupportCacheModel implements CacheModel<Support>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof SupportCacheModel)) {
			return false;
		}

		SupportCacheModel supportCacheModel = (SupportCacheModel)obj;

		if (supportId == supportCacheModel.supportId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, supportId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(27);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", supportId=");
		sb.append(supportId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", startDate=");
		sb.append(startDate);
		sb.append(", duration=");
		sb.append(duration);
		sb.append(", totalValue=");
		sb.append(totalValue);
		sb.append(", sponsorType=");
		sb.append(sponsorType);
		sb.append(", supportType=");
		sb.append(supportType);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Support toEntityModel() {
		SupportImpl supportImpl = new SupportImpl();

		if (uuid == null) {
			supportImpl.setUuid(StringPool.BLANK);
		}
		else {
			supportImpl.setUuid(uuid);
		}

		supportImpl.setSupportId(supportId);
		supportImpl.setGroupId(groupId);
		supportImpl.setCompanyId(companyId);
		supportImpl.setUserId(userId);

		if (userName == null) {
			supportImpl.setUserName(StringPool.BLANK);
		}
		else {
			supportImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			supportImpl.setCreateDate(null);
		}
		else {
			supportImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			supportImpl.setModifiedDate(null);
		}
		else {
			supportImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (startDate == Long.MIN_VALUE) {
			supportImpl.setStartDate(null);
		}
		else {
			supportImpl.setStartDate(new Date(startDate));
		}

		supportImpl.setDuration(duration);
		supportImpl.setTotalValue(totalValue);
		supportImpl.setSponsorType(sponsorType);
		supportImpl.setSupportType(supportType);

		supportImpl.resetOriginalValues();

		return supportImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		supportId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		startDate = objectInput.readLong();

		duration = objectInput.readInt();

		totalValue = objectInput.readDouble();

		sponsorType = objectInput.readInt();

		supportType = objectInput.readInt();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(supportId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);
		objectOutput.writeLong(startDate);

		objectOutput.writeInt(duration);

		objectOutput.writeDouble(totalValue);

		objectOutput.writeInt(sponsorType);

		objectOutput.writeInt(supportType);
	}

	public String uuid;
	public long supportId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long startDate;
	public int duration;
	public double totalValue;
	public int sponsorType;
	public int supportType;
}