/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.impact.model.PatientsImpacted;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing PatientsImpacted in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see PatientsImpacted
 * @generated
 */
@ProviderType
public class PatientsImpactedCacheModel implements CacheModel<PatientsImpacted>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PatientsImpactedCacheModel)) {
			return false;
		}

		PatientsImpactedCacheModel patientsImpactedCacheModel = (PatientsImpactedCacheModel)obj;

		if (patientImpactedId == patientsImpactedCacheModel.patientImpactedId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, patientImpactedId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", patientImpactedId=");
		sb.append(patientImpactedId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", asOfDate=");
		sb.append(asOfDate);
		sb.append(", rangeEstimate=");
		sb.append(rangeEstimate);
		sb.append(", population=");
		sb.append(population);
		sb.append(", optionalComments=");
		sb.append(optionalComments);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public PatientsImpacted toEntityModel() {
		PatientsImpactedImpl patientsImpactedImpl = new PatientsImpactedImpl();

		if (uuid == null) {
			patientsImpactedImpl.setUuid(StringPool.BLANK);
		}
		else {
			patientsImpactedImpl.setUuid(uuid);
		}

		patientsImpactedImpl.setPatientImpactedId(patientImpactedId);
		patientsImpactedImpl.setGroupId(groupId);
		patientsImpactedImpl.setCompanyId(companyId);
		patientsImpactedImpl.setUserId(userId);

		if (userName == null) {
			patientsImpactedImpl.setUserName(StringPool.BLANK);
		}
		else {
			patientsImpactedImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			patientsImpactedImpl.setCreateDate(null);
		}
		else {
			patientsImpactedImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			patientsImpactedImpl.setModifiedDate(null);
		}
		else {
			patientsImpactedImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (asOfDate == Long.MIN_VALUE) {
			patientsImpactedImpl.setAsOfDate(null);
		}
		else {
			patientsImpactedImpl.setAsOfDate(new Date(asOfDate));
		}

		patientsImpactedImpl.setRangeEstimate(rangeEstimate);
		patientsImpactedImpl.setPopulation(population);

		if (optionalComments == null) {
			patientsImpactedImpl.setOptionalComments(StringPool.BLANK);
		}
		else {
			patientsImpactedImpl.setOptionalComments(optionalComments);
		}

		patientsImpactedImpl.resetOriginalValues();

		return patientsImpactedImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		patientImpactedId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		asOfDate = objectInput.readLong();

		rangeEstimate = objectInput.readInt();

		population = objectInput.readInt();
		optionalComments = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(patientImpactedId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);
		objectOutput.writeLong(asOfDate);

		objectOutput.writeInt(rangeEstimate);

		objectOutput.writeInt(population);

		if (optionalComments == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(optionalComments);
		}
	}

	public String uuid;
	public long patientImpactedId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long asOfDate;
	public int rangeEstimate;
	public int population;
	public String optionalComments;
}