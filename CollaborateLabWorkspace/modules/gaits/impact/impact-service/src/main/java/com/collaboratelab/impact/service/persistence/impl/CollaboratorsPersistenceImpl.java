/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.impact.exception.NoSuchCollaboratorsException;
import com.collaboratelab.impact.model.Collaborators;
import com.collaboratelab.impact.model.impl.CollaboratorsImpl;
import com.collaboratelab.impact.model.impl.CollaboratorsModelImpl;
import com.collaboratelab.impact.service.persistence.CollaboratorsPersistence;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.CompanyProvider;
import com.liferay.portal.kernel.service.persistence.CompanyProviderWrapper;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the collaborators service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CollaboratorsPersistence
 * @see com.collaboratelab.impact.service.persistence.CollaboratorsUtil
 * @generated
 */
@ProviderType
public class CollaboratorsPersistenceImpl extends BasePersistenceImpl<Collaborators>
	implements CollaboratorsPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CollaboratorsUtil} to access the collaborators persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CollaboratorsImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CollaboratorsModelImpl.ENTITY_CACHE_ENABLED,
			CollaboratorsModelImpl.FINDER_CACHE_ENABLED,
			CollaboratorsImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CollaboratorsModelImpl.ENTITY_CACHE_ENABLED,
			CollaboratorsModelImpl.FINDER_CACHE_ENABLED,
			CollaboratorsImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CollaboratorsModelImpl.ENTITY_CACHE_ENABLED,
			CollaboratorsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID = new FinderPath(CollaboratorsModelImpl.ENTITY_CACHE_ENABLED,
			CollaboratorsModelImpl.FINDER_CACHE_ENABLED,
			CollaboratorsImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByUuid",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID = new FinderPath(CollaboratorsModelImpl.ENTITY_CACHE_ENABLED,
			CollaboratorsModelImpl.FINDER_CACHE_ENABLED,
			CollaboratorsImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByUuid", new String[] { String.class.getName() },
			CollaboratorsModelImpl.UUID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID = new FinderPath(CollaboratorsModelImpl.ENTITY_CACHE_ENABLED,
			CollaboratorsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] { String.class.getName() });

	/**
	 * Returns all the collaboratorses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching collaboratorses
	 */
	@Override
	public List<Collaborators> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the collaboratorses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of collaboratorses
	 * @param end the upper bound of the range of collaboratorses (not inclusive)
	 * @return the range of matching collaboratorses
	 */
	@Override
	public List<Collaborators> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the collaboratorses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of collaboratorses
	 * @param end the upper bound of the range of collaboratorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching collaboratorses
	 */
	@Override
	public List<Collaborators> findByUuid(String uuid, int start, int end,
		OrderByComparator<Collaborators> orderByComparator) {
		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the collaboratorses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of collaboratorses
	 * @param end the upper bound of the range of collaboratorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching collaboratorses
	 */
	@Override
	public List<Collaborators> findByUuid(String uuid, int start, int end,
		OrderByComparator<Collaborators> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid, start, end, orderByComparator };
		}

		List<Collaborators> list = null;

		if (retrieveFromCache) {
			list = (List<Collaborators>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Collaborators collaborators : list) {
					if (!Objects.equals(uuid, collaborators.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_COLLABORATORS_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CollaboratorsModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<Collaborators>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Collaborators>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first collaborators in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching collaborators
	 * @throws NoSuchCollaboratorsException if a matching collaborators could not be found
	 */
	@Override
	public Collaborators findByUuid_First(String uuid,
		OrderByComparator<Collaborators> orderByComparator)
		throws NoSuchCollaboratorsException {
		Collaborators collaborators = fetchByUuid_First(uuid, orderByComparator);

		if (collaborators != null) {
			return collaborators;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCollaboratorsException(msg.toString());
	}

	/**
	 * Returns the first collaborators in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching collaborators, or <code>null</code> if a matching collaborators could not be found
	 */
	@Override
	public Collaborators fetchByUuid_First(String uuid,
		OrderByComparator<Collaborators> orderByComparator) {
		List<Collaborators> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last collaborators in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching collaborators
	 * @throws NoSuchCollaboratorsException if a matching collaborators could not be found
	 */
	@Override
	public Collaborators findByUuid_Last(String uuid,
		OrderByComparator<Collaborators> orderByComparator)
		throws NoSuchCollaboratorsException {
		Collaborators collaborators = fetchByUuid_Last(uuid, orderByComparator);

		if (collaborators != null) {
			return collaborators;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCollaboratorsException(msg.toString());
	}

	/**
	 * Returns the last collaborators in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching collaborators, or <code>null</code> if a matching collaborators could not be found
	 */
	@Override
	public Collaborators fetchByUuid_Last(String uuid,
		OrderByComparator<Collaborators> orderByComparator) {
		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<Collaborators> list = findByUuid(uuid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the collaboratorses before and after the current collaborators in the ordered set where uuid = &#63;.
	 *
	 * @param collaboratorId the primary key of the current collaborators
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next collaborators
	 * @throws NoSuchCollaboratorsException if a collaborators with the primary key could not be found
	 */
	@Override
	public Collaborators[] findByUuid_PrevAndNext(long collaboratorId,
		String uuid, OrderByComparator<Collaborators> orderByComparator)
		throws NoSuchCollaboratorsException {
		Collaborators collaborators = findByPrimaryKey(collaboratorId);

		Session session = null;

		try {
			session = openSession();

			Collaborators[] array = new CollaboratorsImpl[3];

			array[0] = getByUuid_PrevAndNext(session, collaborators, uuid,
					orderByComparator, true);

			array[1] = collaborators;

			array[2] = getByUuid_PrevAndNext(session, collaborators, uuid,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Collaborators getByUuid_PrevAndNext(Session session,
		Collaborators collaborators, String uuid,
		OrderByComparator<Collaborators> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_COLLABORATORS_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CollaboratorsModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(collaborators);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Collaborators> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the collaboratorses where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (Collaborators collaborators : findByUuid(uuid, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(collaborators);
		}
	}

	/**
	 * Returns the number of collaboratorses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching collaboratorses
	 */
	@Override
	public int countByUuid(String uuid) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID;

		Object[] finderArgs = new Object[] { uuid };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_COLLABORATORS_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_1 = "collaborators.uuid IS NULL";
	private static final String _FINDER_COLUMN_UUID_UUID_2 = "collaborators.uuid = ?";
	private static final String _FINDER_COLUMN_UUID_UUID_3 = "(collaborators.uuid IS NULL OR collaborators.uuid = '')";
	public static final FinderPath FINDER_PATH_FETCH_BY_UUID_G = new FinderPath(CollaboratorsModelImpl.ENTITY_CACHE_ENABLED,
			CollaboratorsModelImpl.FINDER_CACHE_ENABLED,
			CollaboratorsImpl.class, FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G",
			new String[] { String.class.getName(), Long.class.getName() },
			CollaboratorsModelImpl.UUID_COLUMN_BITMASK |
			CollaboratorsModelImpl.GROUPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_G = new FinderPath(CollaboratorsModelImpl.ENTITY_CACHE_ENABLED,
			CollaboratorsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns the collaborators where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchCollaboratorsException} if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching collaborators
	 * @throws NoSuchCollaboratorsException if a matching collaborators could not be found
	 */
	@Override
	public Collaborators findByUUID_G(String uuid, long groupId)
		throws NoSuchCollaboratorsException {
		Collaborators collaborators = fetchByUUID_G(uuid, groupId);

		if (collaborators == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("uuid=");
			msg.append(uuid);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchCollaboratorsException(msg.toString());
		}

		return collaborators;
	}

	/**
	 * Returns the collaborators where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching collaborators, or <code>null</code> if a matching collaborators could not be found
	 */
	@Override
	public Collaborators fetchByUUID_G(String uuid, long groupId) {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the collaborators where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching collaborators, or <code>null</code> if a matching collaborators could not be found
	 */
	@Override
	public Collaborators fetchByUUID_G(String uuid, long groupId,
		boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { uuid, groupId };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_UUID_G,
					finderArgs, this);
		}

		if (result instanceof Collaborators) {
			Collaborators collaborators = (Collaborators)result;

			if (!Objects.equals(uuid, collaborators.getUuid()) ||
					(groupId != collaborators.getGroupId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_COLLABORATORS_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				List<Collaborators> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G,
						finderArgs, list);
				}
				else {
					Collaborators collaborators = list.get(0);

					result = collaborators;

					cacheResult(collaborators);

					if ((collaborators.getUuid() == null) ||
							!collaborators.getUuid().equals(uuid) ||
							(collaborators.getGroupId() != groupId)) {
						finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G,
							finderArgs, collaborators);
					}
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_UUID_G, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Collaborators)result;
		}
	}

	/**
	 * Removes the collaborators where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the collaborators that was removed
	 */
	@Override
	public Collaborators removeByUUID_G(String uuid, long groupId)
		throws NoSuchCollaboratorsException {
		Collaborators collaborators = findByUUID_G(uuid, groupId);

		return remove(collaborators);
	}

	/**
	 * Returns the number of collaboratorses where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching collaboratorses
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_G;

		Object[] finderArgs = new Object[] { uuid, groupId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_COLLABORATORS_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_1 = "collaborators.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_2 = "collaborators.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_3 = "(collaborators.uuid IS NULL OR collaborators.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 = "collaborators.groupId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C = new FinderPath(CollaboratorsModelImpl.ENTITY_CACHE_ENABLED,
			CollaboratorsModelImpl.FINDER_CACHE_ENABLED,
			CollaboratorsImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C =
		new FinderPath(CollaboratorsModelImpl.ENTITY_CACHE_ENABLED,
			CollaboratorsModelImpl.FINDER_CACHE_ENABLED,
			CollaboratorsImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() },
			CollaboratorsModelImpl.UUID_COLUMN_BITMASK |
			CollaboratorsModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_C = new FinderPath(CollaboratorsModelImpl.ENTITY_CACHE_ENABLED,
			CollaboratorsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns all the collaboratorses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching collaboratorses
	 */
	@Override
	public List<Collaborators> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(uuid, companyId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the collaboratorses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of collaboratorses
	 * @param end the upper bound of the range of collaboratorses (not inclusive)
	 * @return the range of matching collaboratorses
	 */
	@Override
	public List<Collaborators> findByUuid_C(String uuid, long companyId,
		int start, int end) {
		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the collaboratorses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of collaboratorses
	 * @param end the upper bound of the range of collaboratorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching collaboratorses
	 */
	@Override
	public List<Collaborators> findByUuid_C(String uuid, long companyId,
		int start, int end, OrderByComparator<Collaborators> orderByComparator) {
		return findByUuid_C(uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the collaboratorses where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of collaboratorses
	 * @param end the upper bound of the range of collaboratorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching collaboratorses
	 */
	@Override
	public List<Collaborators> findByUuid_C(String uuid, long companyId,
		int start, int end, OrderByComparator<Collaborators> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] { uuid, companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] {
					uuid, companyId,
					
					start, end, orderByComparator
				};
		}

		List<Collaborators> list = null;

		if (retrieveFromCache) {
			list = (List<Collaborators>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Collaborators collaborators : list) {
					if (!Objects.equals(uuid, collaborators.getUuid()) ||
							(companyId != collaborators.getCompanyId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_COLLABORATORS_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CollaboratorsModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				if (!pagination) {
					list = (List<Collaborators>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Collaborators>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first collaborators in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching collaborators
	 * @throws NoSuchCollaboratorsException if a matching collaborators could not be found
	 */
	@Override
	public Collaborators findByUuid_C_First(String uuid, long companyId,
		OrderByComparator<Collaborators> orderByComparator)
		throws NoSuchCollaboratorsException {
		Collaborators collaborators = fetchByUuid_C_First(uuid, companyId,
				orderByComparator);

		if (collaborators != null) {
			return collaborators;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCollaboratorsException(msg.toString());
	}

	/**
	 * Returns the first collaborators in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching collaborators, or <code>null</code> if a matching collaborators could not be found
	 */
	@Override
	public Collaborators fetchByUuid_C_First(String uuid, long companyId,
		OrderByComparator<Collaborators> orderByComparator) {
		List<Collaborators> list = findByUuid_C(uuid, companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last collaborators in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching collaborators
	 * @throws NoSuchCollaboratorsException if a matching collaborators could not be found
	 */
	@Override
	public Collaborators findByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<Collaborators> orderByComparator)
		throws NoSuchCollaboratorsException {
		Collaborators collaborators = fetchByUuid_C_Last(uuid, companyId,
				orderByComparator);

		if (collaborators != null) {
			return collaborators;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCollaboratorsException(msg.toString());
	}

	/**
	 * Returns the last collaborators in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching collaborators, or <code>null</code> if a matching collaborators could not be found
	 */
	@Override
	public Collaborators fetchByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<Collaborators> orderByComparator) {
		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<Collaborators> list = findByUuid_C(uuid, companyId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the collaboratorses before and after the current collaborators in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param collaboratorId the primary key of the current collaborators
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next collaborators
	 * @throws NoSuchCollaboratorsException if a collaborators with the primary key could not be found
	 */
	@Override
	public Collaborators[] findByUuid_C_PrevAndNext(long collaboratorId,
		String uuid, long companyId,
		OrderByComparator<Collaborators> orderByComparator)
		throws NoSuchCollaboratorsException {
		Collaborators collaborators = findByPrimaryKey(collaboratorId);

		Session session = null;

		try {
			session = openSession();

			Collaborators[] array = new CollaboratorsImpl[3];

			array[0] = getByUuid_C_PrevAndNext(session, collaborators, uuid,
					companyId, orderByComparator, true);

			array[1] = collaborators;

			array[2] = getByUuid_C_PrevAndNext(session, collaborators, uuid,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Collaborators getByUuid_C_PrevAndNext(Session session,
		Collaborators collaborators, String uuid, long companyId,
		OrderByComparator<Collaborators> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_COLLABORATORS_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CollaboratorsModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(collaborators);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Collaborators> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the collaboratorses where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (Collaborators collaborators : findByUuid_C(uuid, companyId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(collaborators);
		}
	}

	/**
	 * Returns the number of collaboratorses where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching collaboratorses
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_C;

		Object[] finderArgs = new Object[] { uuid, companyId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_COLLABORATORS_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_1 = "collaborators.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_2 = "collaborators.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_3 = "(collaborators.uuid IS NULL OR collaborators.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 = "collaborators.companyId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_COLLABORATORSID = new FinderPath(CollaboratorsModelImpl.ENTITY_CACHE_ENABLED,
			CollaboratorsModelImpl.FINDER_CACHE_ENABLED,
			CollaboratorsImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByCollaboratorsId", new String[] { Long.class.getName() },
			CollaboratorsModelImpl.COLLABORATORID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_COLLABORATORSID = new FinderPath(CollaboratorsModelImpl.ENTITY_CACHE_ENABLED,
			CollaboratorsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByCollaboratorsId", new String[] { Long.class.getName() });

	/**
	 * Returns the collaborators where collaboratorId = &#63; or throws a {@link NoSuchCollaboratorsException} if it could not be found.
	 *
	 * @param collaboratorId the collaborator ID
	 * @return the matching collaborators
	 * @throws NoSuchCollaboratorsException if a matching collaborators could not be found
	 */
	@Override
	public Collaborators findByCollaboratorsId(long collaboratorId)
		throws NoSuchCollaboratorsException {
		Collaborators collaborators = fetchByCollaboratorsId(collaboratorId);

		if (collaborators == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("collaboratorId=");
			msg.append(collaboratorId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchCollaboratorsException(msg.toString());
		}

		return collaborators;
	}

	/**
	 * Returns the collaborators where collaboratorId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param collaboratorId the collaborator ID
	 * @return the matching collaborators, or <code>null</code> if a matching collaborators could not be found
	 */
	@Override
	public Collaborators fetchByCollaboratorsId(long collaboratorId) {
		return fetchByCollaboratorsId(collaboratorId, true);
	}

	/**
	 * Returns the collaborators where collaboratorId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param collaboratorId the collaborator ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching collaborators, or <code>null</code> if a matching collaborators could not be found
	 */
	@Override
	public Collaborators fetchByCollaboratorsId(long collaboratorId,
		boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { collaboratorId };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_COLLABORATORSID,
					finderArgs, this);
		}

		if (result instanceof Collaborators) {
			Collaborators collaborators = (Collaborators)result;

			if ((collaboratorId != collaborators.getCollaboratorId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_COLLABORATORS_WHERE);

			query.append(_FINDER_COLUMN_COLLABORATORSID_COLLABORATORID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(collaboratorId);

				List<Collaborators> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_COLLABORATORSID,
						finderArgs, list);
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							_log.warn(
								"CollaboratorsPersistenceImpl.fetchByCollaboratorsId(long, boolean) with parameters (" +
								StringUtil.merge(finderArgs) +
								") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					Collaborators collaborators = list.get(0);

					result = collaborators;

					cacheResult(collaborators);

					if ((collaborators.getCollaboratorId() != collaboratorId)) {
						finderCache.putResult(FINDER_PATH_FETCH_BY_COLLABORATORSID,
							finderArgs, collaborators);
					}
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_COLLABORATORSID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Collaborators)result;
		}
	}

	/**
	 * Removes the collaborators where collaboratorId = &#63; from the database.
	 *
	 * @param collaboratorId the collaborator ID
	 * @return the collaborators that was removed
	 */
	@Override
	public Collaborators removeByCollaboratorsId(long collaboratorId)
		throws NoSuchCollaboratorsException {
		Collaborators collaborators = findByCollaboratorsId(collaboratorId);

		return remove(collaborators);
	}

	/**
	 * Returns the number of collaboratorses where collaboratorId = &#63;.
	 *
	 * @param collaboratorId the collaborator ID
	 * @return the number of matching collaboratorses
	 */
	@Override
	public int countByCollaboratorsId(long collaboratorId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_COLLABORATORSID;

		Object[] finderArgs = new Object[] { collaboratorId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_COLLABORATORS_WHERE);

			query.append(_FINDER_COLUMN_COLLABORATORSID_COLLABORATORID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(collaboratorId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_COLLABORATORSID_COLLABORATORID_2 = "collaborators.collaboratorId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_GROUPID = new FinderPath(CollaboratorsModelImpl.ENTITY_CACHE_ENABLED,
			CollaboratorsModelImpl.FINDER_CACHE_ENABLED,
			CollaboratorsImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByGroupId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID =
		new FinderPath(CollaboratorsModelImpl.ENTITY_CACHE_ENABLED,
			CollaboratorsModelImpl.FINDER_CACHE_ENABLED,
			CollaboratorsImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByGroupId", new String[] { Long.class.getName() },
			CollaboratorsModelImpl.GROUPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_GROUPID = new FinderPath(CollaboratorsModelImpl.ENTITY_CACHE_ENABLED,
			CollaboratorsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByGroupId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the collaboratorses where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching collaboratorses
	 */
	@Override
	public List<Collaborators> findByGroupId(long groupId) {
		return findByGroupId(groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the collaboratorses where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of collaboratorses
	 * @param end the upper bound of the range of collaboratorses (not inclusive)
	 * @return the range of matching collaboratorses
	 */
	@Override
	public List<Collaborators> findByGroupId(long groupId, int start, int end) {
		return findByGroupId(groupId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the collaboratorses where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of collaboratorses
	 * @param end the upper bound of the range of collaboratorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching collaboratorses
	 */
	@Override
	public List<Collaborators> findByGroupId(long groupId, int start, int end,
		OrderByComparator<Collaborators> orderByComparator) {
		return findByGroupId(groupId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the collaboratorses where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of collaboratorses
	 * @param end the upper bound of the range of collaboratorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching collaboratorses
	 */
	@Override
	public List<Collaborators> findByGroupId(long groupId, int start, int end,
		OrderByComparator<Collaborators> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID;
			finderArgs = new Object[] { groupId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_GROUPID;
			finderArgs = new Object[] { groupId, start, end, orderByComparator };
		}

		List<Collaborators> list = null;

		if (retrieveFromCache) {
			list = (List<Collaborators>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Collaborators collaborators : list) {
					if ((groupId != collaborators.getGroupId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_COLLABORATORS_WHERE);

			query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CollaboratorsModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				if (!pagination) {
					list = (List<Collaborators>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Collaborators>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first collaborators in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching collaborators
	 * @throws NoSuchCollaboratorsException if a matching collaborators could not be found
	 */
	@Override
	public Collaborators findByGroupId_First(long groupId,
		OrderByComparator<Collaborators> orderByComparator)
		throws NoSuchCollaboratorsException {
		Collaborators collaborators = fetchByGroupId_First(groupId,
				orderByComparator);

		if (collaborators != null) {
			return collaborators;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("groupId=");
		msg.append(groupId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCollaboratorsException(msg.toString());
	}

	/**
	 * Returns the first collaborators in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching collaborators, or <code>null</code> if a matching collaborators could not be found
	 */
	@Override
	public Collaborators fetchByGroupId_First(long groupId,
		OrderByComparator<Collaborators> orderByComparator) {
		List<Collaborators> list = findByGroupId(groupId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last collaborators in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching collaborators
	 * @throws NoSuchCollaboratorsException if a matching collaborators could not be found
	 */
	@Override
	public Collaborators findByGroupId_Last(long groupId,
		OrderByComparator<Collaborators> orderByComparator)
		throws NoSuchCollaboratorsException {
		Collaborators collaborators = fetchByGroupId_Last(groupId,
				orderByComparator);

		if (collaborators != null) {
			return collaborators;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("groupId=");
		msg.append(groupId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCollaboratorsException(msg.toString());
	}

	/**
	 * Returns the last collaborators in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching collaborators, or <code>null</code> if a matching collaborators could not be found
	 */
	@Override
	public Collaborators fetchByGroupId_Last(long groupId,
		OrderByComparator<Collaborators> orderByComparator) {
		int count = countByGroupId(groupId);

		if (count == 0) {
			return null;
		}

		List<Collaborators> list = findByGroupId(groupId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the collaboratorses before and after the current collaborators in the ordered set where groupId = &#63;.
	 *
	 * @param collaboratorId the primary key of the current collaborators
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next collaborators
	 * @throws NoSuchCollaboratorsException if a collaborators with the primary key could not be found
	 */
	@Override
	public Collaborators[] findByGroupId_PrevAndNext(long collaboratorId,
		long groupId, OrderByComparator<Collaborators> orderByComparator)
		throws NoSuchCollaboratorsException {
		Collaborators collaborators = findByPrimaryKey(collaboratorId);

		Session session = null;

		try {
			session = openSession();

			Collaborators[] array = new CollaboratorsImpl[3];

			array[0] = getByGroupId_PrevAndNext(session, collaborators,
					groupId, orderByComparator, true);

			array[1] = collaborators;

			array[2] = getByGroupId_PrevAndNext(session, collaborators,
					groupId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Collaborators getByGroupId_PrevAndNext(Session session,
		Collaborators collaborators, long groupId,
		OrderByComparator<Collaborators> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_COLLABORATORS_WHERE);

		query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CollaboratorsModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(groupId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(collaborators);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Collaborators> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the collaboratorses where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	@Override
	public void removeByGroupId(long groupId) {
		for (Collaborators collaborators : findByGroupId(groupId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(collaborators);
		}
	}

	/**
	 * Returns the number of collaboratorses where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching collaboratorses
	 */
	@Override
	public int countByGroupId(long groupId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_GROUPID;

		Object[] finderArgs = new Object[] { groupId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_COLLABORATORS_WHERE);

			query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_GROUPID_GROUPID_2 = "collaborators.groupId = ?";

	public CollaboratorsPersistenceImpl() {
		setModelClass(Collaborators.class);
	}

	/**
	 * Caches the collaborators in the entity cache if it is enabled.
	 *
	 * @param collaborators the collaborators
	 */
	@Override
	public void cacheResult(Collaborators collaborators) {
		entityCache.putResult(CollaboratorsModelImpl.ENTITY_CACHE_ENABLED,
			CollaboratorsImpl.class, collaborators.getPrimaryKey(),
			collaborators);

		finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G,
			new Object[] { collaborators.getUuid(), collaborators.getGroupId() },
			collaborators);

		finderCache.putResult(FINDER_PATH_FETCH_BY_COLLABORATORSID,
			new Object[] { collaborators.getCollaboratorId() }, collaborators);

		collaborators.resetOriginalValues();
	}

	/**
	 * Caches the collaboratorses in the entity cache if it is enabled.
	 *
	 * @param collaboratorses the collaboratorses
	 */
	@Override
	public void cacheResult(List<Collaborators> collaboratorses) {
		for (Collaborators collaborators : collaboratorses) {
			if (entityCache.getResult(
						CollaboratorsModelImpl.ENTITY_CACHE_ENABLED,
						CollaboratorsImpl.class, collaborators.getPrimaryKey()) == null) {
				cacheResult(collaborators);
			}
			else {
				collaborators.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all collaboratorses.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(CollaboratorsImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the collaborators.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Collaborators collaborators) {
		entityCache.removeResult(CollaboratorsModelImpl.ENTITY_CACHE_ENABLED,
			CollaboratorsImpl.class, collaborators.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache((CollaboratorsModelImpl)collaborators);
	}

	@Override
	public void clearCache(List<Collaborators> collaboratorses) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Collaborators collaborators : collaboratorses) {
			entityCache.removeResult(CollaboratorsModelImpl.ENTITY_CACHE_ENABLED,
				CollaboratorsImpl.class, collaborators.getPrimaryKey());

			clearUniqueFindersCache((CollaboratorsModelImpl)collaborators);
		}
	}

	protected void cacheUniqueFindersCache(
		CollaboratorsModelImpl collaboratorsModelImpl, boolean isNew) {
		if (isNew) {
			Object[] args = new Object[] {
					collaboratorsModelImpl.getUuid(),
					collaboratorsModelImpl.getGroupId()
				};

			finderCache.putResult(FINDER_PATH_COUNT_BY_UUID_G, args,
				Long.valueOf(1));
			finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G, args,
				collaboratorsModelImpl);

			args = new Object[] { collaboratorsModelImpl.getCollaboratorId() };

			finderCache.putResult(FINDER_PATH_COUNT_BY_COLLABORATORSID, args,
				Long.valueOf(1));
			finderCache.putResult(FINDER_PATH_FETCH_BY_COLLABORATORSID, args,
				collaboratorsModelImpl);
		}
		else {
			if ((collaboratorsModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_UUID_G.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						collaboratorsModelImpl.getUuid(),
						collaboratorsModelImpl.getGroupId()
					};

				finderCache.putResult(FINDER_PATH_COUNT_BY_UUID_G, args,
					Long.valueOf(1));
				finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G, args,
					collaboratorsModelImpl);
			}

			if ((collaboratorsModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_COLLABORATORSID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						collaboratorsModelImpl.getCollaboratorId()
					};

				finderCache.putResult(FINDER_PATH_COUNT_BY_COLLABORATORSID,
					args, Long.valueOf(1));
				finderCache.putResult(FINDER_PATH_FETCH_BY_COLLABORATORSID,
					args, collaboratorsModelImpl);
			}
		}
	}

	protected void clearUniqueFindersCache(
		CollaboratorsModelImpl collaboratorsModelImpl) {
		Object[] args = new Object[] {
				collaboratorsModelImpl.getUuid(),
				collaboratorsModelImpl.getGroupId()
			};

		finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
		finderCache.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);

		if ((collaboratorsModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_UUID_G.getColumnBitmask()) != 0) {
			args = new Object[] {
					collaboratorsModelImpl.getOriginalUuid(),
					collaboratorsModelImpl.getOriginalGroupId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);
		}

		args = new Object[] { collaboratorsModelImpl.getCollaboratorId() };

		finderCache.removeResult(FINDER_PATH_COUNT_BY_COLLABORATORSID, args);
		finderCache.removeResult(FINDER_PATH_FETCH_BY_COLLABORATORSID, args);

		if ((collaboratorsModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_COLLABORATORSID.getColumnBitmask()) != 0) {
			args = new Object[] {
					collaboratorsModelImpl.getOriginalCollaboratorId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_COLLABORATORSID, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_COLLABORATORSID, args);
		}
	}

	/**
	 * Creates a new collaborators with the primary key. Does not add the collaborators to the database.
	 *
	 * @param collaboratorId the primary key for the new collaborators
	 * @return the new collaborators
	 */
	@Override
	public Collaborators create(long collaboratorId) {
		Collaborators collaborators = new CollaboratorsImpl();

		collaborators.setNew(true);
		collaborators.setPrimaryKey(collaboratorId);

		String uuid = PortalUUIDUtil.generate();

		collaborators.setUuid(uuid);

		collaborators.setCompanyId(companyProvider.getCompanyId());

		return collaborators;
	}

	/**
	 * Removes the collaborators with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param collaboratorId the primary key of the collaborators
	 * @return the collaborators that was removed
	 * @throws NoSuchCollaboratorsException if a collaborators with the primary key could not be found
	 */
	@Override
	public Collaborators remove(long collaboratorId)
		throws NoSuchCollaboratorsException {
		return remove((Serializable)collaboratorId);
	}

	/**
	 * Removes the collaborators with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the collaborators
	 * @return the collaborators that was removed
	 * @throws NoSuchCollaboratorsException if a collaborators with the primary key could not be found
	 */
	@Override
	public Collaborators remove(Serializable primaryKey)
		throws NoSuchCollaboratorsException {
		Session session = null;

		try {
			session = openSession();

			Collaborators collaborators = (Collaborators)session.get(CollaboratorsImpl.class,
					primaryKey);

			if (collaborators == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCollaboratorsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(collaborators);
		}
		catch (NoSuchCollaboratorsException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Collaborators removeImpl(Collaborators collaborators) {
		collaborators = toUnwrappedModel(collaborators);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(collaborators)) {
				collaborators = (Collaborators)session.get(CollaboratorsImpl.class,
						collaborators.getPrimaryKeyObj());
			}

			if (collaborators != null) {
				session.delete(collaborators);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (collaborators != null) {
			clearCache(collaborators);
		}

		return collaborators;
	}

	@Override
	public Collaborators updateImpl(Collaborators collaborators) {
		collaborators = toUnwrappedModel(collaborators);

		boolean isNew = collaborators.isNew();

		CollaboratorsModelImpl collaboratorsModelImpl = (CollaboratorsModelImpl)collaborators;

		if (Validator.isNull(collaborators.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			collaborators.setUuid(uuid);
		}

		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();

		Date now = new Date();

		if (isNew && (collaborators.getCreateDate() == null)) {
			if (serviceContext == null) {
				collaborators.setCreateDate(now);
			}
			else {
				collaborators.setCreateDate(serviceContext.getCreateDate(now));
			}
		}

		if (!collaboratorsModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				collaborators.setModifiedDate(now);
			}
			else {
				collaborators.setModifiedDate(serviceContext.getModifiedDate(
						now));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (collaborators.isNew()) {
				session.save(collaborators);

				collaborators.setNew(false);
			}
			else {
				collaborators = (Collaborators)session.merge(collaborators);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !CollaboratorsModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((collaboratorsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						collaboratorsModelImpl.getOriginalUuid()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);

				args = new Object[] { collaboratorsModelImpl.getUuid() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);
			}

			if ((collaboratorsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						collaboratorsModelImpl.getOriginalUuid(),
						collaboratorsModelImpl.getOriginalCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);

				args = new Object[] {
						collaboratorsModelImpl.getUuid(),
						collaboratorsModelImpl.getCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);
			}

			if ((collaboratorsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						collaboratorsModelImpl.getOriginalGroupId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_GROUPID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID,
					args);

				args = new Object[] { collaboratorsModelImpl.getGroupId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_GROUPID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID,
					args);
			}
		}

		entityCache.putResult(CollaboratorsModelImpl.ENTITY_CACHE_ENABLED,
			CollaboratorsImpl.class, collaborators.getPrimaryKey(),
			collaborators, false);

		clearUniqueFindersCache(collaboratorsModelImpl);
		cacheUniqueFindersCache(collaboratorsModelImpl, isNew);

		collaborators.resetOriginalValues();

		return collaborators;
	}

	protected Collaborators toUnwrappedModel(Collaborators collaborators) {
		if (collaborators instanceof CollaboratorsImpl) {
			return collaborators;
		}

		CollaboratorsImpl collaboratorsImpl = new CollaboratorsImpl();

		collaboratorsImpl.setNew(collaborators.isNew());
		collaboratorsImpl.setPrimaryKey(collaborators.getPrimaryKey());

		collaboratorsImpl.setUuid(collaborators.getUuid());
		collaboratorsImpl.setCollaboratorId(collaborators.getCollaboratorId());
		collaboratorsImpl.setGroupId(collaborators.getGroupId());
		collaboratorsImpl.setCompanyId(collaborators.getCompanyId());
		collaboratorsImpl.setUserId(collaborators.getUserId());
		collaboratorsImpl.setUserName(collaborators.getUserName());
		collaboratorsImpl.setCreateDate(collaborators.getCreateDate());
		collaboratorsImpl.setModifiedDate(collaborators.getModifiedDate());
		collaboratorsImpl.setStartDate(collaborators.getStartDate());
		collaboratorsImpl.setEndDate(collaborators.getEndDate());
		collaboratorsImpl.setName(collaborators.getName());
		collaboratorsImpl.setURL(collaborators.getURL());
		collaboratorsImpl.setType(collaborators.getType());
		collaboratorsImpl.setOptionalComments(collaborators.getOptionalComments());

		return collaboratorsImpl;
	}

	/**
	 * Returns the collaborators with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the collaborators
	 * @return the collaborators
	 * @throws NoSuchCollaboratorsException if a collaborators with the primary key could not be found
	 */
	@Override
	public Collaborators findByPrimaryKey(Serializable primaryKey)
		throws NoSuchCollaboratorsException {
		Collaborators collaborators = fetchByPrimaryKey(primaryKey);

		if (collaborators == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchCollaboratorsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return collaborators;
	}

	/**
	 * Returns the collaborators with the primary key or throws a {@link NoSuchCollaboratorsException} if it could not be found.
	 *
	 * @param collaboratorId the primary key of the collaborators
	 * @return the collaborators
	 * @throws NoSuchCollaboratorsException if a collaborators with the primary key could not be found
	 */
	@Override
	public Collaborators findByPrimaryKey(long collaboratorId)
		throws NoSuchCollaboratorsException {
		return findByPrimaryKey((Serializable)collaboratorId);
	}

	/**
	 * Returns the collaborators with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the collaborators
	 * @return the collaborators, or <code>null</code> if a collaborators with the primary key could not be found
	 */
	@Override
	public Collaborators fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(CollaboratorsModelImpl.ENTITY_CACHE_ENABLED,
				CollaboratorsImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Collaborators collaborators = (Collaborators)serializable;

		if (collaborators == null) {
			Session session = null;

			try {
				session = openSession();

				collaborators = (Collaborators)session.get(CollaboratorsImpl.class,
						primaryKey);

				if (collaborators != null) {
					cacheResult(collaborators);
				}
				else {
					entityCache.putResult(CollaboratorsModelImpl.ENTITY_CACHE_ENABLED,
						CollaboratorsImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(CollaboratorsModelImpl.ENTITY_CACHE_ENABLED,
					CollaboratorsImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return collaborators;
	}

	/**
	 * Returns the collaborators with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param collaboratorId the primary key of the collaborators
	 * @return the collaborators, or <code>null</code> if a collaborators with the primary key could not be found
	 */
	@Override
	public Collaborators fetchByPrimaryKey(long collaboratorId) {
		return fetchByPrimaryKey((Serializable)collaboratorId);
	}

	@Override
	public Map<Serializable, Collaborators> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Collaborators> map = new HashMap<Serializable, Collaborators>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Collaborators collaborators = fetchByPrimaryKey(primaryKey);

			if (collaborators != null) {
				map.put(primaryKey, collaborators);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(CollaboratorsModelImpl.ENTITY_CACHE_ENABLED,
					CollaboratorsImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Collaborators)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_COLLABORATORS_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(String.valueOf(primaryKey));

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (Collaborators collaborators : (List<Collaborators>)q.list()) {
				map.put(collaborators.getPrimaryKeyObj(), collaborators);

				cacheResult(collaborators);

				uncachedPrimaryKeys.remove(collaborators.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(CollaboratorsModelImpl.ENTITY_CACHE_ENABLED,
					CollaboratorsImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the collaboratorses.
	 *
	 * @return the collaboratorses
	 */
	@Override
	public List<Collaborators> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the collaboratorses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of collaboratorses
	 * @param end the upper bound of the range of collaboratorses (not inclusive)
	 * @return the range of collaboratorses
	 */
	@Override
	public List<Collaborators> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the collaboratorses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of collaboratorses
	 * @param end the upper bound of the range of collaboratorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of collaboratorses
	 */
	@Override
	public List<Collaborators> findAll(int start, int end,
		OrderByComparator<Collaborators> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the collaboratorses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of collaboratorses
	 * @param end the upper bound of the range of collaboratorses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of collaboratorses
	 */
	@Override
	public List<Collaborators> findAll(int start, int end,
		OrderByComparator<Collaborators> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Collaborators> list = null;

		if (retrieveFromCache) {
			list = (List<Collaborators>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_COLLABORATORS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_COLLABORATORS;

				if (pagination) {
					sql = sql.concat(CollaboratorsModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Collaborators>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Collaborators>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the collaboratorses from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Collaborators collaborators : findAll()) {
			remove(collaborators);
		}
	}

	/**
	 * Returns the number of collaboratorses.
	 *
	 * @return the number of collaboratorses
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_COLLABORATORS);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return CollaboratorsModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the collaborators persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(CollaboratorsImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = CompanyProviderWrapper.class)
	protected CompanyProvider companyProvider;
	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_COLLABORATORS = "SELECT collaborators FROM Collaborators collaborators";
	private static final String _SQL_SELECT_COLLABORATORS_WHERE_PKS_IN = "SELECT collaborators FROM Collaborators collaborators WHERE collaboratorId IN (";
	private static final String _SQL_SELECT_COLLABORATORS_WHERE = "SELECT collaborators FROM Collaborators collaborators WHERE ";
	private static final String _SQL_COUNT_COLLABORATORS = "SELECT COUNT(collaborators) FROM Collaborators collaborators";
	private static final String _SQL_COUNT_COLLABORATORS_WHERE = "SELECT COUNT(collaborators) FROM Collaborators collaborators WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "collaborators.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Collaborators exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Collaborators exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(CollaboratorsPersistenceImpl.class);
	private static final Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"uuid", "type"
			});
}