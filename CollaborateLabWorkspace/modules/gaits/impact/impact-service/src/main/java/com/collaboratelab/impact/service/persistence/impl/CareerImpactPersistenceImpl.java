/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.impact.exception.NoSuchCareerImpactException;
import com.collaboratelab.impact.model.CareerImpact;
import com.collaboratelab.impact.model.impl.CareerImpactImpl;
import com.collaboratelab.impact.model.impl.CareerImpactModelImpl;
import com.collaboratelab.impact.service.persistence.CareerImpactPersistence;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.CompanyProvider;
import com.liferay.portal.kernel.service.persistence.CompanyProviderWrapper;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the career impact service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CareerImpactPersistence
 * @see com.collaboratelab.impact.service.persistence.CareerImpactUtil
 * @generated
 */
@ProviderType
public class CareerImpactPersistenceImpl extends BasePersistenceImpl<CareerImpact>
	implements CareerImpactPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CareerImpactUtil} to access the career impact persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CareerImpactImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CareerImpactModelImpl.ENTITY_CACHE_ENABLED,
			CareerImpactModelImpl.FINDER_CACHE_ENABLED, CareerImpactImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CareerImpactModelImpl.ENTITY_CACHE_ENABLED,
			CareerImpactModelImpl.FINDER_CACHE_ENABLED, CareerImpactImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CareerImpactModelImpl.ENTITY_CACHE_ENABLED,
			CareerImpactModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID = new FinderPath(CareerImpactModelImpl.ENTITY_CACHE_ENABLED,
			CareerImpactModelImpl.FINDER_CACHE_ENABLED, CareerImpactImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID = new FinderPath(CareerImpactModelImpl.ENTITY_CACHE_ENABLED,
			CareerImpactModelImpl.FINDER_CACHE_ENABLED, CareerImpactImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] { String.class.getName() },
			CareerImpactModelImpl.UUID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID = new FinderPath(CareerImpactModelImpl.ENTITY_CACHE_ENABLED,
			CareerImpactModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] { String.class.getName() });

	/**
	 * Returns all the career impacts where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching career impacts
	 */
	@Override
	public List<CareerImpact> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the career impacts where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of career impacts
	 * @param end the upper bound of the range of career impacts (not inclusive)
	 * @return the range of matching career impacts
	 */
	@Override
	public List<CareerImpact> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the career impacts where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of career impacts
	 * @param end the upper bound of the range of career impacts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching career impacts
	 */
	@Override
	public List<CareerImpact> findByUuid(String uuid, int start, int end,
		OrderByComparator<CareerImpact> orderByComparator) {
		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the career impacts where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of career impacts
	 * @param end the upper bound of the range of career impacts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching career impacts
	 */
	@Override
	public List<CareerImpact> findByUuid(String uuid, int start, int end,
		OrderByComparator<CareerImpact> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid, start, end, orderByComparator };
		}

		List<CareerImpact> list = null;

		if (retrieveFromCache) {
			list = (List<CareerImpact>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (CareerImpact careerImpact : list) {
					if (!Objects.equals(uuid, careerImpact.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CAREERIMPACT_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CareerImpactModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<CareerImpact>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<CareerImpact>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first career impact in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching career impact
	 * @throws NoSuchCareerImpactException if a matching career impact could not be found
	 */
	@Override
	public CareerImpact findByUuid_First(String uuid,
		OrderByComparator<CareerImpact> orderByComparator)
		throws NoSuchCareerImpactException {
		CareerImpact careerImpact = fetchByUuid_First(uuid, orderByComparator);

		if (careerImpact != null) {
			return careerImpact;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCareerImpactException(msg.toString());
	}

	/**
	 * Returns the first career impact in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching career impact, or <code>null</code> if a matching career impact could not be found
	 */
	@Override
	public CareerImpact fetchByUuid_First(String uuid,
		OrderByComparator<CareerImpact> orderByComparator) {
		List<CareerImpact> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last career impact in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching career impact
	 * @throws NoSuchCareerImpactException if a matching career impact could not be found
	 */
	@Override
	public CareerImpact findByUuid_Last(String uuid,
		OrderByComparator<CareerImpact> orderByComparator)
		throws NoSuchCareerImpactException {
		CareerImpact careerImpact = fetchByUuid_Last(uuid, orderByComparator);

		if (careerImpact != null) {
			return careerImpact;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCareerImpactException(msg.toString());
	}

	/**
	 * Returns the last career impact in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching career impact, or <code>null</code> if a matching career impact could not be found
	 */
	@Override
	public CareerImpact fetchByUuid_Last(String uuid,
		OrderByComparator<CareerImpact> orderByComparator) {
		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<CareerImpact> list = findByUuid(uuid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the career impacts before and after the current career impact in the ordered set where uuid = &#63;.
	 *
	 * @param careerImpactId the primary key of the current career impact
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next career impact
	 * @throws NoSuchCareerImpactException if a career impact with the primary key could not be found
	 */
	@Override
	public CareerImpact[] findByUuid_PrevAndNext(long careerImpactId,
		String uuid, OrderByComparator<CareerImpact> orderByComparator)
		throws NoSuchCareerImpactException {
		CareerImpact careerImpact = findByPrimaryKey(careerImpactId);

		Session session = null;

		try {
			session = openSession();

			CareerImpact[] array = new CareerImpactImpl[3];

			array[0] = getByUuid_PrevAndNext(session, careerImpact, uuid,
					orderByComparator, true);

			array[1] = careerImpact;

			array[2] = getByUuid_PrevAndNext(session, careerImpact, uuid,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CareerImpact getByUuid_PrevAndNext(Session session,
		CareerImpact careerImpact, String uuid,
		OrderByComparator<CareerImpact> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CAREERIMPACT_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CareerImpactModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(careerImpact);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CareerImpact> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the career impacts where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (CareerImpact careerImpact : findByUuid(uuid, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(careerImpact);
		}
	}

	/**
	 * Returns the number of career impacts where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching career impacts
	 */
	@Override
	public int countByUuid(String uuid) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID;

		Object[] finderArgs = new Object[] { uuid };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CAREERIMPACT_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_1 = "careerImpact.uuid IS NULL";
	private static final String _FINDER_COLUMN_UUID_UUID_2 = "careerImpact.uuid = ?";
	private static final String _FINDER_COLUMN_UUID_UUID_3 = "(careerImpact.uuid IS NULL OR careerImpact.uuid = '')";
	public static final FinderPath FINDER_PATH_FETCH_BY_UUID_G = new FinderPath(CareerImpactModelImpl.ENTITY_CACHE_ENABLED,
			CareerImpactModelImpl.FINDER_CACHE_ENABLED, CareerImpactImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G",
			new String[] { String.class.getName(), Long.class.getName() },
			CareerImpactModelImpl.UUID_COLUMN_BITMASK |
			CareerImpactModelImpl.GROUPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_G = new FinderPath(CareerImpactModelImpl.ENTITY_CACHE_ENABLED,
			CareerImpactModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns the career impact where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchCareerImpactException} if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching career impact
	 * @throws NoSuchCareerImpactException if a matching career impact could not be found
	 */
	@Override
	public CareerImpact findByUUID_G(String uuid, long groupId)
		throws NoSuchCareerImpactException {
		CareerImpact careerImpact = fetchByUUID_G(uuid, groupId);

		if (careerImpact == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("uuid=");
			msg.append(uuid);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchCareerImpactException(msg.toString());
		}

		return careerImpact;
	}

	/**
	 * Returns the career impact where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching career impact, or <code>null</code> if a matching career impact could not be found
	 */
	@Override
	public CareerImpact fetchByUUID_G(String uuid, long groupId) {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the career impact where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching career impact, or <code>null</code> if a matching career impact could not be found
	 */
	@Override
	public CareerImpact fetchByUUID_G(String uuid, long groupId,
		boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { uuid, groupId };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_UUID_G,
					finderArgs, this);
		}

		if (result instanceof CareerImpact) {
			CareerImpact careerImpact = (CareerImpact)result;

			if (!Objects.equals(uuid, careerImpact.getUuid()) ||
					(groupId != careerImpact.getGroupId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_CAREERIMPACT_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				List<CareerImpact> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G,
						finderArgs, list);
				}
				else {
					CareerImpact careerImpact = list.get(0);

					result = careerImpact;

					cacheResult(careerImpact);

					if ((careerImpact.getUuid() == null) ||
							!careerImpact.getUuid().equals(uuid) ||
							(careerImpact.getGroupId() != groupId)) {
						finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G,
							finderArgs, careerImpact);
					}
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_UUID_G, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (CareerImpact)result;
		}
	}

	/**
	 * Removes the career impact where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the career impact that was removed
	 */
	@Override
	public CareerImpact removeByUUID_G(String uuid, long groupId)
		throws NoSuchCareerImpactException {
		CareerImpact careerImpact = findByUUID_G(uuid, groupId);

		return remove(careerImpact);
	}

	/**
	 * Returns the number of career impacts where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching career impacts
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_G;

		Object[] finderArgs = new Object[] { uuid, groupId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_CAREERIMPACT_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_1 = "careerImpact.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_2 = "careerImpact.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_3 = "(careerImpact.uuid IS NULL OR careerImpact.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 = "careerImpact.groupId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C = new FinderPath(CareerImpactModelImpl.ENTITY_CACHE_ENABLED,
			CareerImpactModelImpl.FINDER_CACHE_ENABLED, CareerImpactImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C =
		new FinderPath(CareerImpactModelImpl.ENTITY_CACHE_ENABLED,
			CareerImpactModelImpl.FINDER_CACHE_ENABLED, CareerImpactImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() },
			CareerImpactModelImpl.UUID_COLUMN_BITMASK |
			CareerImpactModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_C = new FinderPath(CareerImpactModelImpl.ENTITY_CACHE_ENABLED,
			CareerImpactModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns all the career impacts where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching career impacts
	 */
	@Override
	public List<CareerImpact> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(uuid, companyId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the career impacts where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of career impacts
	 * @param end the upper bound of the range of career impacts (not inclusive)
	 * @return the range of matching career impacts
	 */
	@Override
	public List<CareerImpact> findByUuid_C(String uuid, long companyId,
		int start, int end) {
		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the career impacts where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of career impacts
	 * @param end the upper bound of the range of career impacts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching career impacts
	 */
	@Override
	public List<CareerImpact> findByUuid_C(String uuid, long companyId,
		int start, int end, OrderByComparator<CareerImpact> orderByComparator) {
		return findByUuid_C(uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the career impacts where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of career impacts
	 * @param end the upper bound of the range of career impacts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching career impacts
	 */
	@Override
	public List<CareerImpact> findByUuid_C(String uuid, long companyId,
		int start, int end, OrderByComparator<CareerImpact> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] { uuid, companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] {
					uuid, companyId,
					
					start, end, orderByComparator
				};
		}

		List<CareerImpact> list = null;

		if (retrieveFromCache) {
			list = (List<CareerImpact>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (CareerImpact careerImpact : list) {
					if (!Objects.equals(uuid, careerImpact.getUuid()) ||
							(companyId != careerImpact.getCompanyId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_CAREERIMPACT_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CareerImpactModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				if (!pagination) {
					list = (List<CareerImpact>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<CareerImpact>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first career impact in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching career impact
	 * @throws NoSuchCareerImpactException if a matching career impact could not be found
	 */
	@Override
	public CareerImpact findByUuid_C_First(String uuid, long companyId,
		OrderByComparator<CareerImpact> orderByComparator)
		throws NoSuchCareerImpactException {
		CareerImpact careerImpact = fetchByUuid_C_First(uuid, companyId,
				orderByComparator);

		if (careerImpact != null) {
			return careerImpact;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCareerImpactException(msg.toString());
	}

	/**
	 * Returns the first career impact in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching career impact, or <code>null</code> if a matching career impact could not be found
	 */
	@Override
	public CareerImpact fetchByUuid_C_First(String uuid, long companyId,
		OrderByComparator<CareerImpact> orderByComparator) {
		List<CareerImpact> list = findByUuid_C(uuid, companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last career impact in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching career impact
	 * @throws NoSuchCareerImpactException if a matching career impact could not be found
	 */
	@Override
	public CareerImpact findByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<CareerImpact> orderByComparator)
		throws NoSuchCareerImpactException {
		CareerImpact careerImpact = fetchByUuid_C_Last(uuid, companyId,
				orderByComparator);

		if (careerImpact != null) {
			return careerImpact;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCareerImpactException(msg.toString());
	}

	/**
	 * Returns the last career impact in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching career impact, or <code>null</code> if a matching career impact could not be found
	 */
	@Override
	public CareerImpact fetchByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<CareerImpact> orderByComparator) {
		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<CareerImpact> list = findByUuid_C(uuid, companyId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the career impacts before and after the current career impact in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param careerImpactId the primary key of the current career impact
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next career impact
	 * @throws NoSuchCareerImpactException if a career impact with the primary key could not be found
	 */
	@Override
	public CareerImpact[] findByUuid_C_PrevAndNext(long careerImpactId,
		String uuid, long companyId,
		OrderByComparator<CareerImpact> orderByComparator)
		throws NoSuchCareerImpactException {
		CareerImpact careerImpact = findByPrimaryKey(careerImpactId);

		Session session = null;

		try {
			session = openSession();

			CareerImpact[] array = new CareerImpactImpl[3];

			array[0] = getByUuid_C_PrevAndNext(session, careerImpact, uuid,
					companyId, orderByComparator, true);

			array[1] = careerImpact;

			array[2] = getByUuid_C_PrevAndNext(session, careerImpact, uuid,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CareerImpact getByUuid_C_PrevAndNext(Session session,
		CareerImpact careerImpact, String uuid, long companyId,
		OrderByComparator<CareerImpact> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_CAREERIMPACT_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CareerImpactModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(careerImpact);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CareerImpact> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the career impacts where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (CareerImpact careerImpact : findByUuid_C(uuid, companyId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(careerImpact);
		}
	}

	/**
	 * Returns the number of career impacts where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching career impacts
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_C;

		Object[] finderArgs = new Object[] { uuid, companyId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_CAREERIMPACT_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_1 = "careerImpact.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_2 = "careerImpact.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_3 = "(careerImpact.uuid IS NULL OR careerImpact.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 = "careerImpact.companyId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_CAREERIMPACTID = new FinderPath(CareerImpactModelImpl.ENTITY_CACHE_ENABLED,
			CareerImpactModelImpl.FINDER_CACHE_ENABLED, CareerImpactImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByCareerImpactId",
			new String[] { Long.class.getName() },
			CareerImpactModelImpl.CAREERIMPACTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CAREERIMPACTID = new FinderPath(CareerImpactModelImpl.ENTITY_CACHE_ENABLED,
			CareerImpactModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCareerImpactId",
			new String[] { Long.class.getName() });

	/**
	 * Returns the career impact where careerImpactId = &#63; or throws a {@link NoSuchCareerImpactException} if it could not be found.
	 *
	 * @param careerImpactId the career impact ID
	 * @return the matching career impact
	 * @throws NoSuchCareerImpactException if a matching career impact could not be found
	 */
	@Override
	public CareerImpact findByCareerImpactId(long careerImpactId)
		throws NoSuchCareerImpactException {
		CareerImpact careerImpact = fetchByCareerImpactId(careerImpactId);

		if (careerImpact == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("careerImpactId=");
			msg.append(careerImpactId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchCareerImpactException(msg.toString());
		}

		return careerImpact;
	}

	/**
	 * Returns the career impact where careerImpactId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param careerImpactId the career impact ID
	 * @return the matching career impact, or <code>null</code> if a matching career impact could not be found
	 */
	@Override
	public CareerImpact fetchByCareerImpactId(long careerImpactId) {
		return fetchByCareerImpactId(careerImpactId, true);
	}

	/**
	 * Returns the career impact where careerImpactId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param careerImpactId the career impact ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching career impact, or <code>null</code> if a matching career impact could not be found
	 */
	@Override
	public CareerImpact fetchByCareerImpactId(long careerImpactId,
		boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { careerImpactId };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_CAREERIMPACTID,
					finderArgs, this);
		}

		if (result instanceof CareerImpact) {
			CareerImpact careerImpact = (CareerImpact)result;

			if ((careerImpactId != careerImpact.getCareerImpactId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_CAREERIMPACT_WHERE);

			query.append(_FINDER_COLUMN_CAREERIMPACTID_CAREERIMPACTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(careerImpactId);

				List<CareerImpact> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_CAREERIMPACTID,
						finderArgs, list);
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							_log.warn(
								"CareerImpactPersistenceImpl.fetchByCareerImpactId(long, boolean) with parameters (" +
								StringUtil.merge(finderArgs) +
								") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					CareerImpact careerImpact = list.get(0);

					result = careerImpact;

					cacheResult(careerImpact);

					if ((careerImpact.getCareerImpactId() != careerImpactId)) {
						finderCache.putResult(FINDER_PATH_FETCH_BY_CAREERIMPACTID,
							finderArgs, careerImpact);
					}
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_CAREERIMPACTID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (CareerImpact)result;
		}
	}

	/**
	 * Removes the career impact where careerImpactId = &#63; from the database.
	 *
	 * @param careerImpactId the career impact ID
	 * @return the career impact that was removed
	 */
	@Override
	public CareerImpact removeByCareerImpactId(long careerImpactId)
		throws NoSuchCareerImpactException {
		CareerImpact careerImpact = findByCareerImpactId(careerImpactId);

		return remove(careerImpact);
	}

	/**
	 * Returns the number of career impacts where careerImpactId = &#63;.
	 *
	 * @param careerImpactId the career impact ID
	 * @return the number of matching career impacts
	 */
	@Override
	public int countByCareerImpactId(long careerImpactId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CAREERIMPACTID;

		Object[] finderArgs = new Object[] { careerImpactId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CAREERIMPACT_WHERE);

			query.append(_FINDER_COLUMN_CAREERIMPACTID_CAREERIMPACTID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(careerImpactId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CAREERIMPACTID_CAREERIMPACTID_2 = "careerImpact.careerImpactId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_GROUPID = new FinderPath(CareerImpactModelImpl.ENTITY_CACHE_ENABLED,
			CareerImpactModelImpl.FINDER_CACHE_ENABLED, CareerImpactImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByGroupId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID =
		new FinderPath(CareerImpactModelImpl.ENTITY_CACHE_ENABLED,
			CareerImpactModelImpl.FINDER_CACHE_ENABLED, CareerImpactImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByGroupId",
			new String[] { Long.class.getName() },
			CareerImpactModelImpl.GROUPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_GROUPID = new FinderPath(CareerImpactModelImpl.ENTITY_CACHE_ENABLED,
			CareerImpactModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByGroupId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the career impacts where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching career impacts
	 */
	@Override
	public List<CareerImpact> findByGroupId(long groupId) {
		return findByGroupId(groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the career impacts where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of career impacts
	 * @param end the upper bound of the range of career impacts (not inclusive)
	 * @return the range of matching career impacts
	 */
	@Override
	public List<CareerImpact> findByGroupId(long groupId, int start, int end) {
		return findByGroupId(groupId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the career impacts where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of career impacts
	 * @param end the upper bound of the range of career impacts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching career impacts
	 */
	@Override
	public List<CareerImpact> findByGroupId(long groupId, int start, int end,
		OrderByComparator<CareerImpact> orderByComparator) {
		return findByGroupId(groupId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the career impacts where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of career impacts
	 * @param end the upper bound of the range of career impacts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching career impacts
	 */
	@Override
	public List<CareerImpact> findByGroupId(long groupId, int start, int end,
		OrderByComparator<CareerImpact> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID;
			finderArgs = new Object[] { groupId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_GROUPID;
			finderArgs = new Object[] { groupId, start, end, orderByComparator };
		}

		List<CareerImpact> list = null;

		if (retrieveFromCache) {
			list = (List<CareerImpact>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (CareerImpact careerImpact : list) {
					if ((groupId != careerImpact.getGroupId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CAREERIMPACT_WHERE);

			query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CareerImpactModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				if (!pagination) {
					list = (List<CareerImpact>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<CareerImpact>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first career impact in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching career impact
	 * @throws NoSuchCareerImpactException if a matching career impact could not be found
	 */
	@Override
	public CareerImpact findByGroupId_First(long groupId,
		OrderByComparator<CareerImpact> orderByComparator)
		throws NoSuchCareerImpactException {
		CareerImpact careerImpact = fetchByGroupId_First(groupId,
				orderByComparator);

		if (careerImpact != null) {
			return careerImpact;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("groupId=");
		msg.append(groupId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCareerImpactException(msg.toString());
	}

	/**
	 * Returns the first career impact in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching career impact, or <code>null</code> if a matching career impact could not be found
	 */
	@Override
	public CareerImpact fetchByGroupId_First(long groupId,
		OrderByComparator<CareerImpact> orderByComparator) {
		List<CareerImpact> list = findByGroupId(groupId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last career impact in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching career impact
	 * @throws NoSuchCareerImpactException if a matching career impact could not be found
	 */
	@Override
	public CareerImpact findByGroupId_Last(long groupId,
		OrderByComparator<CareerImpact> orderByComparator)
		throws NoSuchCareerImpactException {
		CareerImpact careerImpact = fetchByGroupId_Last(groupId,
				orderByComparator);

		if (careerImpact != null) {
			return careerImpact;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("groupId=");
		msg.append(groupId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCareerImpactException(msg.toString());
	}

	/**
	 * Returns the last career impact in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching career impact, or <code>null</code> if a matching career impact could not be found
	 */
	@Override
	public CareerImpact fetchByGroupId_Last(long groupId,
		OrderByComparator<CareerImpact> orderByComparator) {
		int count = countByGroupId(groupId);

		if (count == 0) {
			return null;
		}

		List<CareerImpact> list = findByGroupId(groupId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the career impacts before and after the current career impact in the ordered set where groupId = &#63;.
	 *
	 * @param careerImpactId the primary key of the current career impact
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next career impact
	 * @throws NoSuchCareerImpactException if a career impact with the primary key could not be found
	 */
	@Override
	public CareerImpact[] findByGroupId_PrevAndNext(long careerImpactId,
		long groupId, OrderByComparator<CareerImpact> orderByComparator)
		throws NoSuchCareerImpactException {
		CareerImpact careerImpact = findByPrimaryKey(careerImpactId);

		Session session = null;

		try {
			session = openSession();

			CareerImpact[] array = new CareerImpactImpl[3];

			array[0] = getByGroupId_PrevAndNext(session, careerImpact, groupId,
					orderByComparator, true);

			array[1] = careerImpact;

			array[2] = getByGroupId_PrevAndNext(session, careerImpact, groupId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CareerImpact getByGroupId_PrevAndNext(Session session,
		CareerImpact careerImpact, long groupId,
		OrderByComparator<CareerImpact> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CAREERIMPACT_WHERE);

		query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CareerImpactModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(groupId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(careerImpact);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CareerImpact> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the career impacts where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	@Override
	public void removeByGroupId(long groupId) {
		for (CareerImpact careerImpact : findByGroupId(groupId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(careerImpact);
		}
	}

	/**
	 * Returns the number of career impacts where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching career impacts
	 */
	@Override
	public int countByGroupId(long groupId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_GROUPID;

		Object[] finderArgs = new Object[] { groupId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CAREERIMPACT_WHERE);

			query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_GROUPID_GROUPID_2 = "careerImpact.groupId = ?";

	public CareerImpactPersistenceImpl() {
		setModelClass(CareerImpact.class);
	}

	/**
	 * Caches the career impact in the entity cache if it is enabled.
	 *
	 * @param careerImpact the career impact
	 */
	@Override
	public void cacheResult(CareerImpact careerImpact) {
		entityCache.putResult(CareerImpactModelImpl.ENTITY_CACHE_ENABLED,
			CareerImpactImpl.class, careerImpact.getPrimaryKey(), careerImpact);

		finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G,
			new Object[] { careerImpact.getUuid(), careerImpact.getGroupId() },
			careerImpact);

		finderCache.putResult(FINDER_PATH_FETCH_BY_CAREERIMPACTID,
			new Object[] { careerImpact.getCareerImpactId() }, careerImpact);

		careerImpact.resetOriginalValues();
	}

	/**
	 * Caches the career impacts in the entity cache if it is enabled.
	 *
	 * @param careerImpacts the career impacts
	 */
	@Override
	public void cacheResult(List<CareerImpact> careerImpacts) {
		for (CareerImpact careerImpact : careerImpacts) {
			if (entityCache.getResult(
						CareerImpactModelImpl.ENTITY_CACHE_ENABLED,
						CareerImpactImpl.class, careerImpact.getPrimaryKey()) == null) {
				cacheResult(careerImpact);
			}
			else {
				careerImpact.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all career impacts.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(CareerImpactImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the career impact.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(CareerImpact careerImpact) {
		entityCache.removeResult(CareerImpactModelImpl.ENTITY_CACHE_ENABLED,
			CareerImpactImpl.class, careerImpact.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache((CareerImpactModelImpl)careerImpact);
	}

	@Override
	public void clearCache(List<CareerImpact> careerImpacts) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (CareerImpact careerImpact : careerImpacts) {
			entityCache.removeResult(CareerImpactModelImpl.ENTITY_CACHE_ENABLED,
				CareerImpactImpl.class, careerImpact.getPrimaryKey());

			clearUniqueFindersCache((CareerImpactModelImpl)careerImpact);
		}
	}

	protected void cacheUniqueFindersCache(
		CareerImpactModelImpl careerImpactModelImpl, boolean isNew) {
		if (isNew) {
			Object[] args = new Object[] {
					careerImpactModelImpl.getUuid(),
					careerImpactModelImpl.getGroupId()
				};

			finderCache.putResult(FINDER_PATH_COUNT_BY_UUID_G, args,
				Long.valueOf(1));
			finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G, args,
				careerImpactModelImpl);

			args = new Object[] { careerImpactModelImpl.getCareerImpactId() };

			finderCache.putResult(FINDER_PATH_COUNT_BY_CAREERIMPACTID, args,
				Long.valueOf(1));
			finderCache.putResult(FINDER_PATH_FETCH_BY_CAREERIMPACTID, args,
				careerImpactModelImpl);
		}
		else {
			if ((careerImpactModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_UUID_G.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						careerImpactModelImpl.getUuid(),
						careerImpactModelImpl.getGroupId()
					};

				finderCache.putResult(FINDER_PATH_COUNT_BY_UUID_G, args,
					Long.valueOf(1));
				finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G, args,
					careerImpactModelImpl);
			}

			if ((careerImpactModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_CAREERIMPACTID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						careerImpactModelImpl.getCareerImpactId()
					};

				finderCache.putResult(FINDER_PATH_COUNT_BY_CAREERIMPACTID,
					args, Long.valueOf(1));
				finderCache.putResult(FINDER_PATH_FETCH_BY_CAREERIMPACTID,
					args, careerImpactModelImpl);
			}
		}
	}

	protected void clearUniqueFindersCache(
		CareerImpactModelImpl careerImpactModelImpl) {
		Object[] args = new Object[] {
				careerImpactModelImpl.getUuid(),
				careerImpactModelImpl.getGroupId()
			};

		finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
		finderCache.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);

		if ((careerImpactModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_UUID_G.getColumnBitmask()) != 0) {
			args = new Object[] {
					careerImpactModelImpl.getOriginalUuid(),
					careerImpactModelImpl.getOriginalGroupId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);
		}

		args = new Object[] { careerImpactModelImpl.getCareerImpactId() };

		finderCache.removeResult(FINDER_PATH_COUNT_BY_CAREERIMPACTID, args);
		finderCache.removeResult(FINDER_PATH_FETCH_BY_CAREERIMPACTID, args);

		if ((careerImpactModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_CAREERIMPACTID.getColumnBitmask()) != 0) {
			args = new Object[] {
					careerImpactModelImpl.getOriginalCareerImpactId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_CAREERIMPACTID, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_CAREERIMPACTID, args);
		}
	}

	/**
	 * Creates a new career impact with the primary key. Does not add the career impact to the database.
	 *
	 * @param careerImpactId the primary key for the new career impact
	 * @return the new career impact
	 */
	@Override
	public CareerImpact create(long careerImpactId) {
		CareerImpact careerImpact = new CareerImpactImpl();

		careerImpact.setNew(true);
		careerImpact.setPrimaryKey(careerImpactId);

		String uuid = PortalUUIDUtil.generate();

		careerImpact.setUuid(uuid);

		careerImpact.setCompanyId(companyProvider.getCompanyId());

		return careerImpact;
	}

	/**
	 * Removes the career impact with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param careerImpactId the primary key of the career impact
	 * @return the career impact that was removed
	 * @throws NoSuchCareerImpactException if a career impact with the primary key could not be found
	 */
	@Override
	public CareerImpact remove(long careerImpactId)
		throws NoSuchCareerImpactException {
		return remove((Serializable)careerImpactId);
	}

	/**
	 * Removes the career impact with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the career impact
	 * @return the career impact that was removed
	 * @throws NoSuchCareerImpactException if a career impact with the primary key could not be found
	 */
	@Override
	public CareerImpact remove(Serializable primaryKey)
		throws NoSuchCareerImpactException {
		Session session = null;

		try {
			session = openSession();

			CareerImpact careerImpact = (CareerImpact)session.get(CareerImpactImpl.class,
					primaryKey);

			if (careerImpact == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCareerImpactException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(careerImpact);
		}
		catch (NoSuchCareerImpactException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected CareerImpact removeImpl(CareerImpact careerImpact) {
		careerImpact = toUnwrappedModel(careerImpact);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(careerImpact)) {
				careerImpact = (CareerImpact)session.get(CareerImpactImpl.class,
						careerImpact.getPrimaryKeyObj());
			}

			if (careerImpact != null) {
				session.delete(careerImpact);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (careerImpact != null) {
			clearCache(careerImpact);
		}

		return careerImpact;
	}

	@Override
	public CareerImpact updateImpl(CareerImpact careerImpact) {
		careerImpact = toUnwrappedModel(careerImpact);

		boolean isNew = careerImpact.isNew();

		CareerImpactModelImpl careerImpactModelImpl = (CareerImpactModelImpl)careerImpact;

		if (Validator.isNull(careerImpact.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			careerImpact.setUuid(uuid);
		}

		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();

		Date now = new Date();

		if (isNew && (careerImpact.getCreateDate() == null)) {
			if (serviceContext == null) {
				careerImpact.setCreateDate(now);
			}
			else {
				careerImpact.setCreateDate(serviceContext.getCreateDate(now));
			}
		}

		if (!careerImpactModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				careerImpact.setModifiedDate(now);
			}
			else {
				careerImpact.setModifiedDate(serviceContext.getModifiedDate(now));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (careerImpact.isNew()) {
				session.save(careerImpact);

				careerImpact.setNew(false);
			}
			else {
				careerImpact = (CareerImpact)session.merge(careerImpact);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !CareerImpactModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((careerImpactModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						careerImpactModelImpl.getOriginalUuid()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);

				args = new Object[] { careerImpactModelImpl.getUuid() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);
			}

			if ((careerImpactModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						careerImpactModelImpl.getOriginalUuid(),
						careerImpactModelImpl.getOriginalCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);

				args = new Object[] {
						careerImpactModelImpl.getUuid(),
						careerImpactModelImpl.getCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);
			}

			if ((careerImpactModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						careerImpactModelImpl.getOriginalGroupId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_GROUPID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID,
					args);

				args = new Object[] { careerImpactModelImpl.getGroupId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_GROUPID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID,
					args);
			}
		}

		entityCache.putResult(CareerImpactModelImpl.ENTITY_CACHE_ENABLED,
			CareerImpactImpl.class, careerImpact.getPrimaryKey(), careerImpact,
			false);

		clearUniqueFindersCache(careerImpactModelImpl);
		cacheUniqueFindersCache(careerImpactModelImpl, isNew);

		careerImpact.resetOriginalValues();

		return careerImpact;
	}

	protected CareerImpact toUnwrappedModel(CareerImpact careerImpact) {
		if (careerImpact instanceof CareerImpactImpl) {
			return careerImpact;
		}

		CareerImpactImpl careerImpactImpl = new CareerImpactImpl();

		careerImpactImpl.setNew(careerImpact.isNew());
		careerImpactImpl.setPrimaryKey(careerImpact.getPrimaryKey());

		careerImpactImpl.setUuid(careerImpact.getUuid());
		careerImpactImpl.setCareerImpactId(careerImpact.getCareerImpactId());
		careerImpactImpl.setGroupId(careerImpact.getGroupId());
		careerImpactImpl.setCompanyId(careerImpact.getCompanyId());
		careerImpactImpl.setUserId(careerImpact.getUserId());
		careerImpactImpl.setUserName(careerImpact.getUserName());
		careerImpactImpl.setCreateDate(careerImpact.getCreateDate());
		careerImpactImpl.setModifiedDate(careerImpact.getModifiedDate());
		careerImpactImpl.setAsOfDate(careerImpact.getAsOfDate());
		careerImpactImpl.setCategoryRaing(careerImpact.getCategoryRaing());
		careerImpactImpl.setOptionalComments(careerImpact.getOptionalComments());

		return careerImpactImpl;
	}

	/**
	 * Returns the career impact with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the career impact
	 * @return the career impact
	 * @throws NoSuchCareerImpactException if a career impact with the primary key could not be found
	 */
	@Override
	public CareerImpact findByPrimaryKey(Serializable primaryKey)
		throws NoSuchCareerImpactException {
		CareerImpact careerImpact = fetchByPrimaryKey(primaryKey);

		if (careerImpact == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchCareerImpactException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return careerImpact;
	}

	/**
	 * Returns the career impact with the primary key or throws a {@link NoSuchCareerImpactException} if it could not be found.
	 *
	 * @param careerImpactId the primary key of the career impact
	 * @return the career impact
	 * @throws NoSuchCareerImpactException if a career impact with the primary key could not be found
	 */
	@Override
	public CareerImpact findByPrimaryKey(long careerImpactId)
		throws NoSuchCareerImpactException {
		return findByPrimaryKey((Serializable)careerImpactId);
	}

	/**
	 * Returns the career impact with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the career impact
	 * @return the career impact, or <code>null</code> if a career impact with the primary key could not be found
	 */
	@Override
	public CareerImpact fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(CareerImpactModelImpl.ENTITY_CACHE_ENABLED,
				CareerImpactImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		CareerImpact careerImpact = (CareerImpact)serializable;

		if (careerImpact == null) {
			Session session = null;

			try {
				session = openSession();

				careerImpact = (CareerImpact)session.get(CareerImpactImpl.class,
						primaryKey);

				if (careerImpact != null) {
					cacheResult(careerImpact);
				}
				else {
					entityCache.putResult(CareerImpactModelImpl.ENTITY_CACHE_ENABLED,
						CareerImpactImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(CareerImpactModelImpl.ENTITY_CACHE_ENABLED,
					CareerImpactImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return careerImpact;
	}

	/**
	 * Returns the career impact with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param careerImpactId the primary key of the career impact
	 * @return the career impact, or <code>null</code> if a career impact with the primary key could not be found
	 */
	@Override
	public CareerImpact fetchByPrimaryKey(long careerImpactId) {
		return fetchByPrimaryKey((Serializable)careerImpactId);
	}

	@Override
	public Map<Serializable, CareerImpact> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, CareerImpact> map = new HashMap<Serializable, CareerImpact>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			CareerImpact careerImpact = fetchByPrimaryKey(primaryKey);

			if (careerImpact != null) {
				map.put(primaryKey, careerImpact);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(CareerImpactModelImpl.ENTITY_CACHE_ENABLED,
					CareerImpactImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (CareerImpact)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_CAREERIMPACT_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(String.valueOf(primaryKey));

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (CareerImpact careerImpact : (List<CareerImpact>)q.list()) {
				map.put(careerImpact.getPrimaryKeyObj(), careerImpact);

				cacheResult(careerImpact);

				uncachedPrimaryKeys.remove(careerImpact.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(CareerImpactModelImpl.ENTITY_CACHE_ENABLED,
					CareerImpactImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the career impacts.
	 *
	 * @return the career impacts
	 */
	@Override
	public List<CareerImpact> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the career impacts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of career impacts
	 * @param end the upper bound of the range of career impacts (not inclusive)
	 * @return the range of career impacts
	 */
	@Override
	public List<CareerImpact> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the career impacts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of career impacts
	 * @param end the upper bound of the range of career impacts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of career impacts
	 */
	@Override
	public List<CareerImpact> findAll(int start, int end,
		OrderByComparator<CareerImpact> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the career impacts.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of career impacts
	 * @param end the upper bound of the range of career impacts (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of career impacts
	 */
	@Override
	public List<CareerImpact> findAll(int start, int end,
		OrderByComparator<CareerImpact> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<CareerImpact> list = null;

		if (retrieveFromCache) {
			list = (List<CareerImpact>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_CAREERIMPACT);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CAREERIMPACT;

				if (pagination) {
					sql = sql.concat(CareerImpactModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<CareerImpact>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<CareerImpact>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the career impacts from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (CareerImpact careerImpact : findAll()) {
			remove(careerImpact);
		}
	}

	/**
	 * Returns the number of career impacts.
	 *
	 * @return the number of career impacts
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CAREERIMPACT);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return CareerImpactModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the career impact persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(CareerImpactImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = CompanyProviderWrapper.class)
	protected CompanyProvider companyProvider;
	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_CAREERIMPACT = "SELECT careerImpact FROM CareerImpact careerImpact";
	private static final String _SQL_SELECT_CAREERIMPACT_WHERE_PKS_IN = "SELECT careerImpact FROM CareerImpact careerImpact WHERE careerImpactId IN (";
	private static final String _SQL_SELECT_CAREERIMPACT_WHERE = "SELECT careerImpact FROM CareerImpact careerImpact WHERE ";
	private static final String _SQL_COUNT_CAREERIMPACT = "SELECT COUNT(careerImpact) FROM CareerImpact careerImpact";
	private static final String _SQL_COUNT_CAREERIMPACT_WHERE = "SELECT COUNT(careerImpact) FROM CareerImpact careerImpact WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "careerImpact.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CareerImpact exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No CareerImpact exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(CareerImpactPersistenceImpl.class);
	private static final Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"uuid"
			});
}