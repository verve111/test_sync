/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.impact.exception.NoSuchSitesUsingSolutionException;
import com.collaboratelab.impact.model.SitesUsingSolution;
import com.collaboratelab.impact.model.impl.SitesUsingSolutionImpl;
import com.collaboratelab.impact.model.impl.SitesUsingSolutionModelImpl;
import com.collaboratelab.impact.service.persistence.SitesUsingSolutionPersistence;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.CompanyProvider;
import com.liferay.portal.kernel.service.persistence.CompanyProviderWrapper;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the sites using solution service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see SitesUsingSolutionPersistence
 * @see com.collaboratelab.impact.service.persistence.SitesUsingSolutionUtil
 * @generated
 */
@ProviderType
public class SitesUsingSolutionPersistenceImpl extends BasePersistenceImpl<SitesUsingSolution>
	implements SitesUsingSolutionPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link SitesUsingSolutionUtil} to access the sites using solution persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = SitesUsingSolutionImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(SitesUsingSolutionModelImpl.ENTITY_CACHE_ENABLED,
			SitesUsingSolutionModelImpl.FINDER_CACHE_ENABLED,
			SitesUsingSolutionImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(SitesUsingSolutionModelImpl.ENTITY_CACHE_ENABLED,
			SitesUsingSolutionModelImpl.FINDER_CACHE_ENABLED,
			SitesUsingSolutionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(SitesUsingSolutionModelImpl.ENTITY_CACHE_ENABLED,
			SitesUsingSolutionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID = new FinderPath(SitesUsingSolutionModelImpl.ENTITY_CACHE_ENABLED,
			SitesUsingSolutionModelImpl.FINDER_CACHE_ENABLED,
			SitesUsingSolutionImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID = new FinderPath(SitesUsingSolutionModelImpl.ENTITY_CACHE_ENABLED,
			SitesUsingSolutionModelImpl.FINDER_CACHE_ENABLED,
			SitesUsingSolutionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] { String.class.getName() },
			SitesUsingSolutionModelImpl.UUID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID = new FinderPath(SitesUsingSolutionModelImpl.ENTITY_CACHE_ENABLED,
			SitesUsingSolutionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] { String.class.getName() });

	/**
	 * Returns all the sites using solutions where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching sites using solutions
	 */
	@Override
	public List<SitesUsingSolution> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the sites using solutions where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of sites using solutions
	 * @param end the upper bound of the range of sites using solutions (not inclusive)
	 * @return the range of matching sites using solutions
	 */
	@Override
	public List<SitesUsingSolution> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the sites using solutions where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of sites using solutions
	 * @param end the upper bound of the range of sites using solutions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching sites using solutions
	 */
	@Override
	public List<SitesUsingSolution> findByUuid(String uuid, int start, int end,
		OrderByComparator<SitesUsingSolution> orderByComparator) {
		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the sites using solutions where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of sites using solutions
	 * @param end the upper bound of the range of sites using solutions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching sites using solutions
	 */
	@Override
	public List<SitesUsingSolution> findByUuid(String uuid, int start, int end,
		OrderByComparator<SitesUsingSolution> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid, start, end, orderByComparator };
		}

		List<SitesUsingSolution> list = null;

		if (retrieveFromCache) {
			list = (List<SitesUsingSolution>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (SitesUsingSolution sitesUsingSolution : list) {
					if (!Objects.equals(uuid, sitesUsingSolution.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_SITESUSINGSOLUTION_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(SitesUsingSolutionModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<SitesUsingSolution>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<SitesUsingSolution>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first sites using solution in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching sites using solution
	 * @throws NoSuchSitesUsingSolutionException if a matching sites using solution could not be found
	 */
	@Override
	public SitesUsingSolution findByUuid_First(String uuid,
		OrderByComparator<SitesUsingSolution> orderByComparator)
		throws NoSuchSitesUsingSolutionException {
		SitesUsingSolution sitesUsingSolution = fetchByUuid_First(uuid,
				orderByComparator);

		if (sitesUsingSolution != null) {
			return sitesUsingSolution;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchSitesUsingSolutionException(msg.toString());
	}

	/**
	 * Returns the first sites using solution in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching sites using solution, or <code>null</code> if a matching sites using solution could not be found
	 */
	@Override
	public SitesUsingSolution fetchByUuid_First(String uuid,
		OrderByComparator<SitesUsingSolution> orderByComparator) {
		List<SitesUsingSolution> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last sites using solution in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching sites using solution
	 * @throws NoSuchSitesUsingSolutionException if a matching sites using solution could not be found
	 */
	@Override
	public SitesUsingSolution findByUuid_Last(String uuid,
		OrderByComparator<SitesUsingSolution> orderByComparator)
		throws NoSuchSitesUsingSolutionException {
		SitesUsingSolution sitesUsingSolution = fetchByUuid_Last(uuid,
				orderByComparator);

		if (sitesUsingSolution != null) {
			return sitesUsingSolution;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchSitesUsingSolutionException(msg.toString());
	}

	/**
	 * Returns the last sites using solution in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching sites using solution, or <code>null</code> if a matching sites using solution could not be found
	 */
	@Override
	public SitesUsingSolution fetchByUuid_Last(String uuid,
		OrderByComparator<SitesUsingSolution> orderByComparator) {
		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<SitesUsingSolution> list = findByUuid(uuid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the sites using solutions before and after the current sites using solution in the ordered set where uuid = &#63;.
	 *
	 * @param sitesUsingSolutionId the primary key of the current sites using solution
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next sites using solution
	 * @throws NoSuchSitesUsingSolutionException if a sites using solution with the primary key could not be found
	 */
	@Override
	public SitesUsingSolution[] findByUuid_PrevAndNext(
		long sitesUsingSolutionId, String uuid,
		OrderByComparator<SitesUsingSolution> orderByComparator)
		throws NoSuchSitesUsingSolutionException {
		SitesUsingSolution sitesUsingSolution = findByPrimaryKey(sitesUsingSolutionId);

		Session session = null;

		try {
			session = openSession();

			SitesUsingSolution[] array = new SitesUsingSolutionImpl[3];

			array[0] = getByUuid_PrevAndNext(session, sitesUsingSolution, uuid,
					orderByComparator, true);

			array[1] = sitesUsingSolution;

			array[2] = getByUuid_PrevAndNext(session, sitesUsingSolution, uuid,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected SitesUsingSolution getByUuid_PrevAndNext(Session session,
		SitesUsingSolution sitesUsingSolution, String uuid,
		OrderByComparator<SitesUsingSolution> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_SITESUSINGSOLUTION_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(SitesUsingSolutionModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(sitesUsingSolution);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<SitesUsingSolution> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the sites using solutions where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (SitesUsingSolution sitesUsingSolution : findByUuid(uuid,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(sitesUsingSolution);
		}
	}

	/**
	 * Returns the number of sites using solutions where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching sites using solutions
	 */
	@Override
	public int countByUuid(String uuid) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID;

		Object[] finderArgs = new Object[] { uuid };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_SITESUSINGSOLUTION_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_1 = "sitesUsingSolution.uuid IS NULL";
	private static final String _FINDER_COLUMN_UUID_UUID_2 = "sitesUsingSolution.uuid = ?";
	private static final String _FINDER_COLUMN_UUID_UUID_3 = "(sitesUsingSolution.uuid IS NULL OR sitesUsingSolution.uuid = '')";
	public static final FinderPath FINDER_PATH_FETCH_BY_UUID_G = new FinderPath(SitesUsingSolutionModelImpl.ENTITY_CACHE_ENABLED,
			SitesUsingSolutionModelImpl.FINDER_CACHE_ENABLED,
			SitesUsingSolutionImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByUUID_G",
			new String[] { String.class.getName(), Long.class.getName() },
			SitesUsingSolutionModelImpl.UUID_COLUMN_BITMASK |
			SitesUsingSolutionModelImpl.GROUPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_G = new FinderPath(SitesUsingSolutionModelImpl.ENTITY_CACHE_ENABLED,
			SitesUsingSolutionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns the sites using solution where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchSitesUsingSolutionException} if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching sites using solution
	 * @throws NoSuchSitesUsingSolutionException if a matching sites using solution could not be found
	 */
	@Override
	public SitesUsingSolution findByUUID_G(String uuid, long groupId)
		throws NoSuchSitesUsingSolutionException {
		SitesUsingSolution sitesUsingSolution = fetchByUUID_G(uuid, groupId);

		if (sitesUsingSolution == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("uuid=");
			msg.append(uuid);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchSitesUsingSolutionException(msg.toString());
		}

		return sitesUsingSolution;
	}

	/**
	 * Returns the sites using solution where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching sites using solution, or <code>null</code> if a matching sites using solution could not be found
	 */
	@Override
	public SitesUsingSolution fetchByUUID_G(String uuid, long groupId) {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the sites using solution where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching sites using solution, or <code>null</code> if a matching sites using solution could not be found
	 */
	@Override
	public SitesUsingSolution fetchByUUID_G(String uuid, long groupId,
		boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { uuid, groupId };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_UUID_G,
					finderArgs, this);
		}

		if (result instanceof SitesUsingSolution) {
			SitesUsingSolution sitesUsingSolution = (SitesUsingSolution)result;

			if (!Objects.equals(uuid, sitesUsingSolution.getUuid()) ||
					(groupId != sitesUsingSolution.getGroupId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_SITESUSINGSOLUTION_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				List<SitesUsingSolution> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G,
						finderArgs, list);
				}
				else {
					SitesUsingSolution sitesUsingSolution = list.get(0);

					result = sitesUsingSolution;

					cacheResult(sitesUsingSolution);

					if ((sitesUsingSolution.getUuid() == null) ||
							!sitesUsingSolution.getUuid().equals(uuid) ||
							(sitesUsingSolution.getGroupId() != groupId)) {
						finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G,
							finderArgs, sitesUsingSolution);
					}
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_UUID_G, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (SitesUsingSolution)result;
		}
	}

	/**
	 * Removes the sites using solution where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the sites using solution that was removed
	 */
	@Override
	public SitesUsingSolution removeByUUID_G(String uuid, long groupId)
		throws NoSuchSitesUsingSolutionException {
		SitesUsingSolution sitesUsingSolution = findByUUID_G(uuid, groupId);

		return remove(sitesUsingSolution);
	}

	/**
	 * Returns the number of sites using solutions where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching sites using solutions
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_G;

		Object[] finderArgs = new Object[] { uuid, groupId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_SITESUSINGSOLUTION_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_1 = "sitesUsingSolution.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_2 = "sitesUsingSolution.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_3 = "(sitesUsingSolution.uuid IS NULL OR sitesUsingSolution.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 = "sitesUsingSolution.groupId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C = new FinderPath(SitesUsingSolutionModelImpl.ENTITY_CACHE_ENABLED,
			SitesUsingSolutionModelImpl.FINDER_CACHE_ENABLED,
			SitesUsingSolutionImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C =
		new FinderPath(SitesUsingSolutionModelImpl.ENTITY_CACHE_ENABLED,
			SitesUsingSolutionModelImpl.FINDER_CACHE_ENABLED,
			SitesUsingSolutionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() },
			SitesUsingSolutionModelImpl.UUID_COLUMN_BITMASK |
			SitesUsingSolutionModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_C = new FinderPath(SitesUsingSolutionModelImpl.ENTITY_CACHE_ENABLED,
			SitesUsingSolutionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns all the sites using solutions where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching sites using solutions
	 */
	@Override
	public List<SitesUsingSolution> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(uuid, companyId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the sites using solutions where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of sites using solutions
	 * @param end the upper bound of the range of sites using solutions (not inclusive)
	 * @return the range of matching sites using solutions
	 */
	@Override
	public List<SitesUsingSolution> findByUuid_C(String uuid, long companyId,
		int start, int end) {
		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the sites using solutions where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of sites using solutions
	 * @param end the upper bound of the range of sites using solutions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching sites using solutions
	 */
	@Override
	public List<SitesUsingSolution> findByUuid_C(String uuid, long companyId,
		int start, int end,
		OrderByComparator<SitesUsingSolution> orderByComparator) {
		return findByUuid_C(uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the sites using solutions where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of sites using solutions
	 * @param end the upper bound of the range of sites using solutions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching sites using solutions
	 */
	@Override
	public List<SitesUsingSolution> findByUuid_C(String uuid, long companyId,
		int start, int end,
		OrderByComparator<SitesUsingSolution> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] { uuid, companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] {
					uuid, companyId,
					
					start, end, orderByComparator
				};
		}

		List<SitesUsingSolution> list = null;

		if (retrieveFromCache) {
			list = (List<SitesUsingSolution>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (SitesUsingSolution sitesUsingSolution : list) {
					if (!Objects.equals(uuid, sitesUsingSolution.getUuid()) ||
							(companyId != sitesUsingSolution.getCompanyId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_SITESUSINGSOLUTION_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(SitesUsingSolutionModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				if (!pagination) {
					list = (List<SitesUsingSolution>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<SitesUsingSolution>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first sites using solution in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching sites using solution
	 * @throws NoSuchSitesUsingSolutionException if a matching sites using solution could not be found
	 */
	@Override
	public SitesUsingSolution findByUuid_C_First(String uuid, long companyId,
		OrderByComparator<SitesUsingSolution> orderByComparator)
		throws NoSuchSitesUsingSolutionException {
		SitesUsingSolution sitesUsingSolution = fetchByUuid_C_First(uuid,
				companyId, orderByComparator);

		if (sitesUsingSolution != null) {
			return sitesUsingSolution;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchSitesUsingSolutionException(msg.toString());
	}

	/**
	 * Returns the first sites using solution in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching sites using solution, or <code>null</code> if a matching sites using solution could not be found
	 */
	@Override
	public SitesUsingSolution fetchByUuid_C_First(String uuid, long companyId,
		OrderByComparator<SitesUsingSolution> orderByComparator) {
		List<SitesUsingSolution> list = findByUuid_C(uuid, companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last sites using solution in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching sites using solution
	 * @throws NoSuchSitesUsingSolutionException if a matching sites using solution could not be found
	 */
	@Override
	public SitesUsingSolution findByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<SitesUsingSolution> orderByComparator)
		throws NoSuchSitesUsingSolutionException {
		SitesUsingSolution sitesUsingSolution = fetchByUuid_C_Last(uuid,
				companyId, orderByComparator);

		if (sitesUsingSolution != null) {
			return sitesUsingSolution;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchSitesUsingSolutionException(msg.toString());
	}

	/**
	 * Returns the last sites using solution in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching sites using solution, or <code>null</code> if a matching sites using solution could not be found
	 */
	@Override
	public SitesUsingSolution fetchByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<SitesUsingSolution> orderByComparator) {
		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<SitesUsingSolution> list = findByUuid_C(uuid, companyId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the sites using solutions before and after the current sites using solution in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param sitesUsingSolutionId the primary key of the current sites using solution
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next sites using solution
	 * @throws NoSuchSitesUsingSolutionException if a sites using solution with the primary key could not be found
	 */
	@Override
	public SitesUsingSolution[] findByUuid_C_PrevAndNext(
		long sitesUsingSolutionId, String uuid, long companyId,
		OrderByComparator<SitesUsingSolution> orderByComparator)
		throws NoSuchSitesUsingSolutionException {
		SitesUsingSolution sitesUsingSolution = findByPrimaryKey(sitesUsingSolutionId);

		Session session = null;

		try {
			session = openSession();

			SitesUsingSolution[] array = new SitesUsingSolutionImpl[3];

			array[0] = getByUuid_C_PrevAndNext(session, sitesUsingSolution,
					uuid, companyId, orderByComparator, true);

			array[1] = sitesUsingSolution;

			array[2] = getByUuid_C_PrevAndNext(session, sitesUsingSolution,
					uuid, companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected SitesUsingSolution getByUuid_C_PrevAndNext(Session session,
		SitesUsingSolution sitesUsingSolution, String uuid, long companyId,
		OrderByComparator<SitesUsingSolution> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_SITESUSINGSOLUTION_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(SitesUsingSolutionModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(sitesUsingSolution);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<SitesUsingSolution> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the sites using solutions where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (SitesUsingSolution sitesUsingSolution : findByUuid_C(uuid,
				companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(sitesUsingSolution);
		}
	}

	/**
	 * Returns the number of sites using solutions where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching sites using solutions
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_C;

		Object[] finderArgs = new Object[] { uuid, companyId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_SITESUSINGSOLUTION_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_1 = "sitesUsingSolution.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_2 = "sitesUsingSolution.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_3 = "(sitesUsingSolution.uuid IS NULL OR sitesUsingSolution.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 = "sitesUsingSolution.companyId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_SITESUSINGSOLUTIONID = new FinderPath(SitesUsingSolutionModelImpl.ENTITY_CACHE_ENABLED,
			SitesUsingSolutionModelImpl.FINDER_CACHE_ENABLED,
			SitesUsingSolutionImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchBySitesUsingSolutionId",
			new String[] { Long.class.getName() },
			SitesUsingSolutionModelImpl.SITESUSINGSOLUTIONID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_SITESUSINGSOLUTIONID = new FinderPath(SitesUsingSolutionModelImpl.ENTITY_CACHE_ENABLED,
			SitesUsingSolutionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countBySitesUsingSolutionId", new String[] { Long.class.getName() });

	/**
	 * Returns the sites using solution where sitesUsingSolutionId = &#63; or throws a {@link NoSuchSitesUsingSolutionException} if it could not be found.
	 *
	 * @param sitesUsingSolutionId the sites using solution ID
	 * @return the matching sites using solution
	 * @throws NoSuchSitesUsingSolutionException if a matching sites using solution could not be found
	 */
	@Override
	public SitesUsingSolution findBySitesUsingSolutionId(
		long sitesUsingSolutionId) throws NoSuchSitesUsingSolutionException {
		SitesUsingSolution sitesUsingSolution = fetchBySitesUsingSolutionId(sitesUsingSolutionId);

		if (sitesUsingSolution == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("sitesUsingSolutionId=");
			msg.append(sitesUsingSolutionId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchSitesUsingSolutionException(msg.toString());
		}

		return sitesUsingSolution;
	}

	/**
	 * Returns the sites using solution where sitesUsingSolutionId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param sitesUsingSolutionId the sites using solution ID
	 * @return the matching sites using solution, or <code>null</code> if a matching sites using solution could not be found
	 */
	@Override
	public SitesUsingSolution fetchBySitesUsingSolutionId(
		long sitesUsingSolutionId) {
		return fetchBySitesUsingSolutionId(sitesUsingSolutionId, true);
	}

	/**
	 * Returns the sites using solution where sitesUsingSolutionId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param sitesUsingSolutionId the sites using solution ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching sites using solution, or <code>null</code> if a matching sites using solution could not be found
	 */
	@Override
	public SitesUsingSolution fetchBySitesUsingSolutionId(
		long sitesUsingSolutionId, boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { sitesUsingSolutionId };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_SITESUSINGSOLUTIONID,
					finderArgs, this);
		}

		if (result instanceof SitesUsingSolution) {
			SitesUsingSolution sitesUsingSolution = (SitesUsingSolution)result;

			if ((sitesUsingSolutionId != sitesUsingSolution.getSitesUsingSolutionId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_SITESUSINGSOLUTION_WHERE);

			query.append(_FINDER_COLUMN_SITESUSINGSOLUTIONID_SITESUSINGSOLUTIONID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(sitesUsingSolutionId);

				List<SitesUsingSolution> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_SITESUSINGSOLUTIONID,
						finderArgs, list);
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							_log.warn(
								"SitesUsingSolutionPersistenceImpl.fetchBySitesUsingSolutionId(long, boolean) with parameters (" +
								StringUtil.merge(finderArgs) +
								") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					SitesUsingSolution sitesUsingSolution = list.get(0);

					result = sitesUsingSolution;

					cacheResult(sitesUsingSolution);

					if ((sitesUsingSolution.getSitesUsingSolutionId() != sitesUsingSolutionId)) {
						finderCache.putResult(FINDER_PATH_FETCH_BY_SITESUSINGSOLUTIONID,
							finderArgs, sitesUsingSolution);
					}
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_SITESUSINGSOLUTIONID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (SitesUsingSolution)result;
		}
	}

	/**
	 * Removes the sites using solution where sitesUsingSolutionId = &#63; from the database.
	 *
	 * @param sitesUsingSolutionId the sites using solution ID
	 * @return the sites using solution that was removed
	 */
	@Override
	public SitesUsingSolution removeBySitesUsingSolutionId(
		long sitesUsingSolutionId) throws NoSuchSitesUsingSolutionException {
		SitesUsingSolution sitesUsingSolution = findBySitesUsingSolutionId(sitesUsingSolutionId);

		return remove(sitesUsingSolution);
	}

	/**
	 * Returns the number of sites using solutions where sitesUsingSolutionId = &#63;.
	 *
	 * @param sitesUsingSolutionId the sites using solution ID
	 * @return the number of matching sites using solutions
	 */
	@Override
	public int countBySitesUsingSolutionId(long sitesUsingSolutionId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_SITESUSINGSOLUTIONID;

		Object[] finderArgs = new Object[] { sitesUsingSolutionId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_SITESUSINGSOLUTION_WHERE);

			query.append(_FINDER_COLUMN_SITESUSINGSOLUTIONID_SITESUSINGSOLUTIONID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(sitesUsingSolutionId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_SITESUSINGSOLUTIONID_SITESUSINGSOLUTIONID_2 =
		"sitesUsingSolution.sitesUsingSolutionId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_GROUPID = new FinderPath(SitesUsingSolutionModelImpl.ENTITY_CACHE_ENABLED,
			SitesUsingSolutionModelImpl.FINDER_CACHE_ENABLED,
			SitesUsingSolutionImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByGroupId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID =
		new FinderPath(SitesUsingSolutionModelImpl.ENTITY_CACHE_ENABLED,
			SitesUsingSolutionModelImpl.FINDER_CACHE_ENABLED,
			SitesUsingSolutionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByGroupId",
			new String[] { Long.class.getName() },
			SitesUsingSolutionModelImpl.GROUPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_GROUPID = new FinderPath(SitesUsingSolutionModelImpl.ENTITY_CACHE_ENABLED,
			SitesUsingSolutionModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByGroupId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the sites using solutions where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching sites using solutions
	 */
	@Override
	public List<SitesUsingSolution> findByGroupId(long groupId) {
		return findByGroupId(groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the sites using solutions where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of sites using solutions
	 * @param end the upper bound of the range of sites using solutions (not inclusive)
	 * @return the range of matching sites using solutions
	 */
	@Override
	public List<SitesUsingSolution> findByGroupId(long groupId, int start,
		int end) {
		return findByGroupId(groupId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the sites using solutions where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of sites using solutions
	 * @param end the upper bound of the range of sites using solutions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching sites using solutions
	 */
	@Override
	public List<SitesUsingSolution> findByGroupId(long groupId, int start,
		int end, OrderByComparator<SitesUsingSolution> orderByComparator) {
		return findByGroupId(groupId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the sites using solutions where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of sites using solutions
	 * @param end the upper bound of the range of sites using solutions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching sites using solutions
	 */
	@Override
	public List<SitesUsingSolution> findByGroupId(long groupId, int start,
		int end, OrderByComparator<SitesUsingSolution> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID;
			finderArgs = new Object[] { groupId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_GROUPID;
			finderArgs = new Object[] { groupId, start, end, orderByComparator };
		}

		List<SitesUsingSolution> list = null;

		if (retrieveFromCache) {
			list = (List<SitesUsingSolution>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (SitesUsingSolution sitesUsingSolution : list) {
					if ((groupId != sitesUsingSolution.getGroupId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_SITESUSINGSOLUTION_WHERE);

			query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(SitesUsingSolutionModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				if (!pagination) {
					list = (List<SitesUsingSolution>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<SitesUsingSolution>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first sites using solution in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching sites using solution
	 * @throws NoSuchSitesUsingSolutionException if a matching sites using solution could not be found
	 */
	@Override
	public SitesUsingSolution findByGroupId_First(long groupId,
		OrderByComparator<SitesUsingSolution> orderByComparator)
		throws NoSuchSitesUsingSolutionException {
		SitesUsingSolution sitesUsingSolution = fetchByGroupId_First(groupId,
				orderByComparator);

		if (sitesUsingSolution != null) {
			return sitesUsingSolution;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("groupId=");
		msg.append(groupId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchSitesUsingSolutionException(msg.toString());
	}

	/**
	 * Returns the first sites using solution in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching sites using solution, or <code>null</code> if a matching sites using solution could not be found
	 */
	@Override
	public SitesUsingSolution fetchByGroupId_First(long groupId,
		OrderByComparator<SitesUsingSolution> orderByComparator) {
		List<SitesUsingSolution> list = findByGroupId(groupId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last sites using solution in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching sites using solution
	 * @throws NoSuchSitesUsingSolutionException if a matching sites using solution could not be found
	 */
	@Override
	public SitesUsingSolution findByGroupId_Last(long groupId,
		OrderByComparator<SitesUsingSolution> orderByComparator)
		throws NoSuchSitesUsingSolutionException {
		SitesUsingSolution sitesUsingSolution = fetchByGroupId_Last(groupId,
				orderByComparator);

		if (sitesUsingSolution != null) {
			return sitesUsingSolution;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("groupId=");
		msg.append(groupId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchSitesUsingSolutionException(msg.toString());
	}

	/**
	 * Returns the last sites using solution in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching sites using solution, or <code>null</code> if a matching sites using solution could not be found
	 */
	@Override
	public SitesUsingSolution fetchByGroupId_Last(long groupId,
		OrderByComparator<SitesUsingSolution> orderByComparator) {
		int count = countByGroupId(groupId);

		if (count == 0) {
			return null;
		}

		List<SitesUsingSolution> list = findByGroupId(groupId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the sites using solutions before and after the current sites using solution in the ordered set where groupId = &#63;.
	 *
	 * @param sitesUsingSolutionId the primary key of the current sites using solution
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next sites using solution
	 * @throws NoSuchSitesUsingSolutionException if a sites using solution with the primary key could not be found
	 */
	@Override
	public SitesUsingSolution[] findByGroupId_PrevAndNext(
		long sitesUsingSolutionId, long groupId,
		OrderByComparator<SitesUsingSolution> orderByComparator)
		throws NoSuchSitesUsingSolutionException {
		SitesUsingSolution sitesUsingSolution = findByPrimaryKey(sitesUsingSolutionId);

		Session session = null;

		try {
			session = openSession();

			SitesUsingSolution[] array = new SitesUsingSolutionImpl[3];

			array[0] = getByGroupId_PrevAndNext(session, sitesUsingSolution,
					groupId, orderByComparator, true);

			array[1] = sitesUsingSolution;

			array[2] = getByGroupId_PrevAndNext(session, sitesUsingSolution,
					groupId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected SitesUsingSolution getByGroupId_PrevAndNext(Session session,
		SitesUsingSolution sitesUsingSolution, long groupId,
		OrderByComparator<SitesUsingSolution> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_SITESUSINGSOLUTION_WHERE);

		query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(SitesUsingSolutionModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(groupId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(sitesUsingSolution);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<SitesUsingSolution> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the sites using solutions where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	@Override
	public void removeByGroupId(long groupId) {
		for (SitesUsingSolution sitesUsingSolution : findByGroupId(groupId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(sitesUsingSolution);
		}
	}

	/**
	 * Returns the number of sites using solutions where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching sites using solutions
	 */
	@Override
	public int countByGroupId(long groupId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_GROUPID;

		Object[] finderArgs = new Object[] { groupId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_SITESUSINGSOLUTION_WHERE);

			query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_GROUPID_GROUPID_2 = "sitesUsingSolution.groupId = ?";

	public SitesUsingSolutionPersistenceImpl() {
		setModelClass(SitesUsingSolution.class);
	}

	/**
	 * Caches the sites using solution in the entity cache if it is enabled.
	 *
	 * @param sitesUsingSolution the sites using solution
	 */
	@Override
	public void cacheResult(SitesUsingSolution sitesUsingSolution) {
		entityCache.putResult(SitesUsingSolutionModelImpl.ENTITY_CACHE_ENABLED,
			SitesUsingSolutionImpl.class, sitesUsingSolution.getPrimaryKey(),
			sitesUsingSolution);

		finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G,
			new Object[] {
				sitesUsingSolution.getUuid(), sitesUsingSolution.getGroupId()
			}, sitesUsingSolution);

		finderCache.putResult(FINDER_PATH_FETCH_BY_SITESUSINGSOLUTIONID,
			new Object[] { sitesUsingSolution.getSitesUsingSolutionId() },
			sitesUsingSolution);

		sitesUsingSolution.resetOriginalValues();
	}

	/**
	 * Caches the sites using solutions in the entity cache if it is enabled.
	 *
	 * @param sitesUsingSolutions the sites using solutions
	 */
	@Override
	public void cacheResult(List<SitesUsingSolution> sitesUsingSolutions) {
		for (SitesUsingSolution sitesUsingSolution : sitesUsingSolutions) {
			if (entityCache.getResult(
						SitesUsingSolutionModelImpl.ENTITY_CACHE_ENABLED,
						SitesUsingSolutionImpl.class,
						sitesUsingSolution.getPrimaryKey()) == null) {
				cacheResult(sitesUsingSolution);
			}
			else {
				sitesUsingSolution.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all sites using solutions.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(SitesUsingSolutionImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the sites using solution.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(SitesUsingSolution sitesUsingSolution) {
		entityCache.removeResult(SitesUsingSolutionModelImpl.ENTITY_CACHE_ENABLED,
			SitesUsingSolutionImpl.class, sitesUsingSolution.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache((SitesUsingSolutionModelImpl)sitesUsingSolution);
	}

	@Override
	public void clearCache(List<SitesUsingSolution> sitesUsingSolutions) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (SitesUsingSolution sitesUsingSolution : sitesUsingSolutions) {
			entityCache.removeResult(SitesUsingSolutionModelImpl.ENTITY_CACHE_ENABLED,
				SitesUsingSolutionImpl.class, sitesUsingSolution.getPrimaryKey());

			clearUniqueFindersCache((SitesUsingSolutionModelImpl)sitesUsingSolution);
		}
	}

	protected void cacheUniqueFindersCache(
		SitesUsingSolutionModelImpl sitesUsingSolutionModelImpl, boolean isNew) {
		if (isNew) {
			Object[] args = new Object[] {
					sitesUsingSolutionModelImpl.getUuid(),
					sitesUsingSolutionModelImpl.getGroupId()
				};

			finderCache.putResult(FINDER_PATH_COUNT_BY_UUID_G, args,
				Long.valueOf(1));
			finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G, args,
				sitesUsingSolutionModelImpl);

			args = new Object[] {
					sitesUsingSolutionModelImpl.getSitesUsingSolutionId()
				};

			finderCache.putResult(FINDER_PATH_COUNT_BY_SITESUSINGSOLUTIONID,
				args, Long.valueOf(1));
			finderCache.putResult(FINDER_PATH_FETCH_BY_SITESUSINGSOLUTIONID,
				args, sitesUsingSolutionModelImpl);
		}
		else {
			if ((sitesUsingSolutionModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_UUID_G.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						sitesUsingSolutionModelImpl.getUuid(),
						sitesUsingSolutionModelImpl.getGroupId()
					};

				finderCache.putResult(FINDER_PATH_COUNT_BY_UUID_G, args,
					Long.valueOf(1));
				finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G, args,
					sitesUsingSolutionModelImpl);
			}

			if ((sitesUsingSolutionModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_SITESUSINGSOLUTIONID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						sitesUsingSolutionModelImpl.getSitesUsingSolutionId()
					};

				finderCache.putResult(FINDER_PATH_COUNT_BY_SITESUSINGSOLUTIONID,
					args, Long.valueOf(1));
				finderCache.putResult(FINDER_PATH_FETCH_BY_SITESUSINGSOLUTIONID,
					args, sitesUsingSolutionModelImpl);
			}
		}
	}

	protected void clearUniqueFindersCache(
		SitesUsingSolutionModelImpl sitesUsingSolutionModelImpl) {
		Object[] args = new Object[] {
				sitesUsingSolutionModelImpl.getUuid(),
				sitesUsingSolutionModelImpl.getGroupId()
			};

		finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
		finderCache.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);

		if ((sitesUsingSolutionModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_UUID_G.getColumnBitmask()) != 0) {
			args = new Object[] {
					sitesUsingSolutionModelImpl.getOriginalUuid(),
					sitesUsingSolutionModelImpl.getOriginalGroupId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);
		}

		args = new Object[] {
				sitesUsingSolutionModelImpl.getSitesUsingSolutionId()
			};

		finderCache.removeResult(FINDER_PATH_COUNT_BY_SITESUSINGSOLUTIONID, args);
		finderCache.removeResult(FINDER_PATH_FETCH_BY_SITESUSINGSOLUTIONID, args);

		if ((sitesUsingSolutionModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_SITESUSINGSOLUTIONID.getColumnBitmask()) != 0) {
			args = new Object[] {
					sitesUsingSolutionModelImpl.getOriginalSitesUsingSolutionId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_SITESUSINGSOLUTIONID,
				args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_SITESUSINGSOLUTIONID,
				args);
		}
	}

	/**
	 * Creates a new sites using solution with the primary key. Does not add the sites using solution to the database.
	 *
	 * @param sitesUsingSolutionId the primary key for the new sites using solution
	 * @return the new sites using solution
	 */
	@Override
	public SitesUsingSolution create(long sitesUsingSolutionId) {
		SitesUsingSolution sitesUsingSolution = new SitesUsingSolutionImpl();

		sitesUsingSolution.setNew(true);
		sitesUsingSolution.setPrimaryKey(sitesUsingSolutionId);

		String uuid = PortalUUIDUtil.generate();

		sitesUsingSolution.setUuid(uuid);

		sitesUsingSolution.setCompanyId(companyProvider.getCompanyId());

		return sitesUsingSolution;
	}

	/**
	 * Removes the sites using solution with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param sitesUsingSolutionId the primary key of the sites using solution
	 * @return the sites using solution that was removed
	 * @throws NoSuchSitesUsingSolutionException if a sites using solution with the primary key could not be found
	 */
	@Override
	public SitesUsingSolution remove(long sitesUsingSolutionId)
		throws NoSuchSitesUsingSolutionException {
		return remove((Serializable)sitesUsingSolutionId);
	}

	/**
	 * Removes the sites using solution with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the sites using solution
	 * @return the sites using solution that was removed
	 * @throws NoSuchSitesUsingSolutionException if a sites using solution with the primary key could not be found
	 */
	@Override
	public SitesUsingSolution remove(Serializable primaryKey)
		throws NoSuchSitesUsingSolutionException {
		Session session = null;

		try {
			session = openSession();

			SitesUsingSolution sitesUsingSolution = (SitesUsingSolution)session.get(SitesUsingSolutionImpl.class,
					primaryKey);

			if (sitesUsingSolution == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchSitesUsingSolutionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(sitesUsingSolution);
		}
		catch (NoSuchSitesUsingSolutionException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected SitesUsingSolution removeImpl(
		SitesUsingSolution sitesUsingSolution) {
		sitesUsingSolution = toUnwrappedModel(sitesUsingSolution);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(sitesUsingSolution)) {
				sitesUsingSolution = (SitesUsingSolution)session.get(SitesUsingSolutionImpl.class,
						sitesUsingSolution.getPrimaryKeyObj());
			}

			if (sitesUsingSolution != null) {
				session.delete(sitesUsingSolution);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (sitesUsingSolution != null) {
			clearCache(sitesUsingSolution);
		}

		return sitesUsingSolution;
	}

	@Override
	public SitesUsingSolution updateImpl(SitesUsingSolution sitesUsingSolution) {
		sitesUsingSolution = toUnwrappedModel(sitesUsingSolution);

		boolean isNew = sitesUsingSolution.isNew();

		SitesUsingSolutionModelImpl sitesUsingSolutionModelImpl = (SitesUsingSolutionModelImpl)sitesUsingSolution;

		if (Validator.isNull(sitesUsingSolution.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			sitesUsingSolution.setUuid(uuid);
		}

		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();

		Date now = new Date();

		if (isNew && (sitesUsingSolution.getCreateDate() == null)) {
			if (serviceContext == null) {
				sitesUsingSolution.setCreateDate(now);
			}
			else {
				sitesUsingSolution.setCreateDate(serviceContext.getCreateDate(
						now));
			}
		}

		if (!sitesUsingSolutionModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				sitesUsingSolution.setModifiedDate(now);
			}
			else {
				sitesUsingSolution.setModifiedDate(serviceContext.getModifiedDate(
						now));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (sitesUsingSolution.isNew()) {
				session.save(sitesUsingSolution);

				sitesUsingSolution.setNew(false);
			}
			else {
				sitesUsingSolution = (SitesUsingSolution)session.merge(sitesUsingSolution);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !SitesUsingSolutionModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((sitesUsingSolutionModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						sitesUsingSolutionModelImpl.getOriginalUuid()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);

				args = new Object[] { sitesUsingSolutionModelImpl.getUuid() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);
			}

			if ((sitesUsingSolutionModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						sitesUsingSolutionModelImpl.getOriginalUuid(),
						sitesUsingSolutionModelImpl.getOriginalCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);

				args = new Object[] {
						sitesUsingSolutionModelImpl.getUuid(),
						sitesUsingSolutionModelImpl.getCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);
			}

			if ((sitesUsingSolutionModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						sitesUsingSolutionModelImpl.getOriginalGroupId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_GROUPID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID,
					args);

				args = new Object[] { sitesUsingSolutionModelImpl.getGroupId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_GROUPID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID,
					args);
			}
		}

		entityCache.putResult(SitesUsingSolutionModelImpl.ENTITY_CACHE_ENABLED,
			SitesUsingSolutionImpl.class, sitesUsingSolution.getPrimaryKey(),
			sitesUsingSolution, false);

		clearUniqueFindersCache(sitesUsingSolutionModelImpl);
		cacheUniqueFindersCache(sitesUsingSolutionModelImpl, isNew);

		sitesUsingSolution.resetOriginalValues();

		return sitesUsingSolution;
	}

	protected SitesUsingSolution toUnwrappedModel(
		SitesUsingSolution sitesUsingSolution) {
		if (sitesUsingSolution instanceof SitesUsingSolutionImpl) {
			return sitesUsingSolution;
		}

		SitesUsingSolutionImpl sitesUsingSolutionImpl = new SitesUsingSolutionImpl();

		sitesUsingSolutionImpl.setNew(sitesUsingSolution.isNew());
		sitesUsingSolutionImpl.setPrimaryKey(sitesUsingSolution.getPrimaryKey());

		sitesUsingSolutionImpl.setUuid(sitesUsingSolution.getUuid());
		sitesUsingSolutionImpl.setSitesUsingSolutionId(sitesUsingSolution.getSitesUsingSolutionId());
		sitesUsingSolutionImpl.setGroupId(sitesUsingSolution.getGroupId());
		sitesUsingSolutionImpl.setCompanyId(sitesUsingSolution.getCompanyId());
		sitesUsingSolutionImpl.setUserId(sitesUsingSolution.getUserId());
		sitesUsingSolutionImpl.setUserName(sitesUsingSolution.getUserName());
		sitesUsingSolutionImpl.setCreateDate(sitesUsingSolution.getCreateDate());
		sitesUsingSolutionImpl.setModifiedDate(sitesUsingSolution.getModifiedDate());
		sitesUsingSolutionImpl.setAsOfDate(sitesUsingSolution.getAsOfDate());
		sitesUsingSolutionImpl.setNumber(sitesUsingSolution.getNumber());
		sitesUsingSolutionImpl.setOptionalComments(sitesUsingSolution.getOptionalComments());

		return sitesUsingSolutionImpl;
	}

	/**
	 * Returns the sites using solution with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the sites using solution
	 * @return the sites using solution
	 * @throws NoSuchSitesUsingSolutionException if a sites using solution with the primary key could not be found
	 */
	@Override
	public SitesUsingSolution findByPrimaryKey(Serializable primaryKey)
		throws NoSuchSitesUsingSolutionException {
		SitesUsingSolution sitesUsingSolution = fetchByPrimaryKey(primaryKey);

		if (sitesUsingSolution == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchSitesUsingSolutionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return sitesUsingSolution;
	}

	/**
	 * Returns the sites using solution with the primary key or throws a {@link NoSuchSitesUsingSolutionException} if it could not be found.
	 *
	 * @param sitesUsingSolutionId the primary key of the sites using solution
	 * @return the sites using solution
	 * @throws NoSuchSitesUsingSolutionException if a sites using solution with the primary key could not be found
	 */
	@Override
	public SitesUsingSolution findByPrimaryKey(long sitesUsingSolutionId)
		throws NoSuchSitesUsingSolutionException {
		return findByPrimaryKey((Serializable)sitesUsingSolutionId);
	}

	/**
	 * Returns the sites using solution with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the sites using solution
	 * @return the sites using solution, or <code>null</code> if a sites using solution with the primary key could not be found
	 */
	@Override
	public SitesUsingSolution fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(SitesUsingSolutionModelImpl.ENTITY_CACHE_ENABLED,
				SitesUsingSolutionImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		SitesUsingSolution sitesUsingSolution = (SitesUsingSolution)serializable;

		if (sitesUsingSolution == null) {
			Session session = null;

			try {
				session = openSession();

				sitesUsingSolution = (SitesUsingSolution)session.get(SitesUsingSolutionImpl.class,
						primaryKey);

				if (sitesUsingSolution != null) {
					cacheResult(sitesUsingSolution);
				}
				else {
					entityCache.putResult(SitesUsingSolutionModelImpl.ENTITY_CACHE_ENABLED,
						SitesUsingSolutionImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(SitesUsingSolutionModelImpl.ENTITY_CACHE_ENABLED,
					SitesUsingSolutionImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return sitesUsingSolution;
	}

	/**
	 * Returns the sites using solution with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param sitesUsingSolutionId the primary key of the sites using solution
	 * @return the sites using solution, or <code>null</code> if a sites using solution with the primary key could not be found
	 */
	@Override
	public SitesUsingSolution fetchByPrimaryKey(long sitesUsingSolutionId) {
		return fetchByPrimaryKey((Serializable)sitesUsingSolutionId);
	}

	@Override
	public Map<Serializable, SitesUsingSolution> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, SitesUsingSolution> map = new HashMap<Serializable, SitesUsingSolution>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			SitesUsingSolution sitesUsingSolution = fetchByPrimaryKey(primaryKey);

			if (sitesUsingSolution != null) {
				map.put(primaryKey, sitesUsingSolution);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(SitesUsingSolutionModelImpl.ENTITY_CACHE_ENABLED,
					SitesUsingSolutionImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (SitesUsingSolution)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_SITESUSINGSOLUTION_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(String.valueOf(primaryKey));

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (SitesUsingSolution sitesUsingSolution : (List<SitesUsingSolution>)q.list()) {
				map.put(sitesUsingSolution.getPrimaryKeyObj(),
					sitesUsingSolution);

				cacheResult(sitesUsingSolution);

				uncachedPrimaryKeys.remove(sitesUsingSolution.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(SitesUsingSolutionModelImpl.ENTITY_CACHE_ENABLED,
					SitesUsingSolutionImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the sites using solutions.
	 *
	 * @return the sites using solutions
	 */
	@Override
	public List<SitesUsingSolution> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the sites using solutions.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of sites using solutions
	 * @param end the upper bound of the range of sites using solutions (not inclusive)
	 * @return the range of sites using solutions
	 */
	@Override
	public List<SitesUsingSolution> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the sites using solutions.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of sites using solutions
	 * @param end the upper bound of the range of sites using solutions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of sites using solutions
	 */
	@Override
	public List<SitesUsingSolution> findAll(int start, int end,
		OrderByComparator<SitesUsingSolution> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the sites using solutions.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of sites using solutions
	 * @param end the upper bound of the range of sites using solutions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of sites using solutions
	 */
	@Override
	public List<SitesUsingSolution> findAll(int start, int end,
		OrderByComparator<SitesUsingSolution> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<SitesUsingSolution> list = null;

		if (retrieveFromCache) {
			list = (List<SitesUsingSolution>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_SITESUSINGSOLUTION);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_SITESUSINGSOLUTION;

				if (pagination) {
					sql = sql.concat(SitesUsingSolutionModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<SitesUsingSolution>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<SitesUsingSolution>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the sites using solutions from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (SitesUsingSolution sitesUsingSolution : findAll()) {
			remove(sitesUsingSolution);
		}
	}

	/**
	 * Returns the number of sites using solutions.
	 *
	 * @return the number of sites using solutions
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_SITESUSINGSOLUTION);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return SitesUsingSolutionModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the sites using solution persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(SitesUsingSolutionImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = CompanyProviderWrapper.class)
	protected CompanyProvider companyProvider;
	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_SITESUSINGSOLUTION = "SELECT sitesUsingSolution FROM SitesUsingSolution sitesUsingSolution";
	private static final String _SQL_SELECT_SITESUSINGSOLUTION_WHERE_PKS_IN = "SELECT sitesUsingSolution FROM SitesUsingSolution sitesUsingSolution WHERE sitesUsingSolutionId IN (";
	private static final String _SQL_SELECT_SITESUSINGSOLUTION_WHERE = "SELECT sitesUsingSolution FROM SitesUsingSolution sitesUsingSolution WHERE ";
	private static final String _SQL_COUNT_SITESUSINGSOLUTION = "SELECT COUNT(sitesUsingSolution) FROM SitesUsingSolution sitesUsingSolution";
	private static final String _SQL_COUNT_SITESUSINGSOLUTION_WHERE = "SELECT COUNT(sitesUsingSolution) FROM SitesUsingSolution sitesUsingSolution WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "sitesUsingSolution.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No SitesUsingSolution exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No SitesUsingSolution exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(SitesUsingSolutionPersistenceImpl.class);
	private static final Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"uuid", "number"
			});
}