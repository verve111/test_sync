/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.service.impl;

import java.util.List;

import com.collaboratelab.impact.model.Publications;
import com.collaboratelab.impact.service.base.PublicationsLocalServiceBaseImpl;
import com.liferay.portal.kernel.exception.SystemException;

import aQute.bnd.annotation.ProviderType;

/**
 * The implementation of the publications local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.collaboratelab.impact.service.PublicationsLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see PublicationsLocalServiceBaseImpl
 * @see com.collaboratelab.impact.service.PublicationsLocalServiceUtil
 */
@ProviderType
public class PublicationsLocalServiceImpl
	extends PublicationsLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link com.collaboratelab.impact.service.PublicationsLocalServiceUtil} to access the publications local service.
	 */
	
	public List<Publications> findByGroupId(long groupId) throws SystemException
	{
	    return this.publicationsPersistence.findByGroupId(groupId);
	}
}