/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.impact.model.Collaborators;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Collaborators in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Collaborators
 * @generated
 */
@ProviderType
public class CollaboratorsCacheModel implements CacheModel<Collaborators>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CollaboratorsCacheModel)) {
			return false;
		}

		CollaboratorsCacheModel collaboratorsCacheModel = (CollaboratorsCacheModel)obj;

		if (collaboratorId == collaboratorsCacheModel.collaboratorId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, collaboratorId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(29);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", collaboratorId=");
		sb.append(collaboratorId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", startDate=");
		sb.append(startDate);
		sb.append(", endDate=");
		sb.append(endDate);
		sb.append(", name=");
		sb.append(name);
		sb.append(", URL=");
		sb.append(URL);
		sb.append(", type=");
		sb.append(type);
		sb.append(", optionalComments=");
		sb.append(optionalComments);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Collaborators toEntityModel() {
		CollaboratorsImpl collaboratorsImpl = new CollaboratorsImpl();

		if (uuid == null) {
			collaboratorsImpl.setUuid(StringPool.BLANK);
		}
		else {
			collaboratorsImpl.setUuid(uuid);
		}

		collaboratorsImpl.setCollaboratorId(collaboratorId);
		collaboratorsImpl.setGroupId(groupId);
		collaboratorsImpl.setCompanyId(companyId);
		collaboratorsImpl.setUserId(userId);

		if (userName == null) {
			collaboratorsImpl.setUserName(StringPool.BLANK);
		}
		else {
			collaboratorsImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			collaboratorsImpl.setCreateDate(null);
		}
		else {
			collaboratorsImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			collaboratorsImpl.setModifiedDate(null);
		}
		else {
			collaboratorsImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (startDate == Long.MIN_VALUE) {
			collaboratorsImpl.setStartDate(null);
		}
		else {
			collaboratorsImpl.setStartDate(new Date(startDate));
		}

		if (endDate == Long.MIN_VALUE) {
			collaboratorsImpl.setEndDate(null);
		}
		else {
			collaboratorsImpl.setEndDate(new Date(endDate));
		}

		if (name == null) {
			collaboratorsImpl.setName(StringPool.BLANK);
		}
		else {
			collaboratorsImpl.setName(name);
		}

		if (URL == null) {
			collaboratorsImpl.setURL(StringPool.BLANK);
		}
		else {
			collaboratorsImpl.setURL(URL);
		}

		collaboratorsImpl.setType(type);

		if (optionalComments == null) {
			collaboratorsImpl.setOptionalComments(StringPool.BLANK);
		}
		else {
			collaboratorsImpl.setOptionalComments(optionalComments);
		}

		collaboratorsImpl.resetOriginalValues();

		return collaboratorsImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		collaboratorId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		startDate = objectInput.readLong();
		endDate = objectInput.readLong();
		name = objectInput.readUTF();
		URL = objectInput.readUTF();

		type = objectInput.readInt();
		optionalComments = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(collaboratorId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);
		objectOutput.writeLong(startDate);
		objectOutput.writeLong(endDate);

		if (name == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(name);
		}

		if (URL == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(URL);
		}

		objectOutput.writeInt(type);

		if (optionalComments == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(optionalComments);
		}
	}

	public String uuid;
	public long collaboratorId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long startDate;
	public long endDate;
	public String name;
	public String URL;
	public int type;
	public String optionalComments;
}