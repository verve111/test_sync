/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.impact.model.TeamMemberProjectRecognition;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing TeamMemberProjectRecognition in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see TeamMemberProjectRecognition
 * @generated
 */
@ProviderType
public class TeamMemberProjectRecognitionCacheModel implements CacheModel<TeamMemberProjectRecognition>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof TeamMemberProjectRecognitionCacheModel)) {
			return false;
		}

		TeamMemberProjectRecognitionCacheModel teamMemberProjectRecognitionCacheModel =
			(TeamMemberProjectRecognitionCacheModel)obj;

		if (teamMemberProjectRecognitionId == teamMemberProjectRecognitionCacheModel.teamMemberProjectRecognitionId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, teamMemberProjectRecognitionId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(27);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", teamMemberProjectRecognitionId=");
		sb.append(teamMemberProjectRecognitionId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", dateOfRecognition=");
		sb.append(dateOfRecognition);
		sb.append(", Name=");
		sb.append(Name);
		sb.append(", CatRating=");
		sb.append(CatRating);
		sb.append(", URL=");
		sb.append(URL);
		sb.append(", optionalComments=");
		sb.append(optionalComments);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public TeamMemberProjectRecognition toEntityModel() {
		TeamMemberProjectRecognitionImpl teamMemberProjectRecognitionImpl = new TeamMemberProjectRecognitionImpl();

		if (uuid == null) {
			teamMemberProjectRecognitionImpl.setUuid(StringPool.BLANK);
		}
		else {
			teamMemberProjectRecognitionImpl.setUuid(uuid);
		}

		teamMemberProjectRecognitionImpl.setTeamMemberProjectRecognitionId(teamMemberProjectRecognitionId);
		teamMemberProjectRecognitionImpl.setGroupId(groupId);
		teamMemberProjectRecognitionImpl.setCompanyId(companyId);
		teamMemberProjectRecognitionImpl.setUserId(userId);

		if (userName == null) {
			teamMemberProjectRecognitionImpl.setUserName(StringPool.BLANK);
		}
		else {
			teamMemberProjectRecognitionImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			teamMemberProjectRecognitionImpl.setCreateDate(null);
		}
		else {
			teamMemberProjectRecognitionImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			teamMemberProjectRecognitionImpl.setModifiedDate(null);
		}
		else {
			teamMemberProjectRecognitionImpl.setModifiedDate(new Date(
					modifiedDate));
		}

		if (dateOfRecognition == Long.MIN_VALUE) {
			teamMemberProjectRecognitionImpl.setDateOfRecognition(null);
		}
		else {
			teamMemberProjectRecognitionImpl.setDateOfRecognition(new Date(
					dateOfRecognition));
		}

		if (Name == null) {
			teamMemberProjectRecognitionImpl.setName(StringPool.BLANK);
		}
		else {
			teamMemberProjectRecognitionImpl.setName(Name);
		}

		teamMemberProjectRecognitionImpl.setCatRating(CatRating);

		if (URL == null) {
			teamMemberProjectRecognitionImpl.setURL(StringPool.BLANK);
		}
		else {
			teamMemberProjectRecognitionImpl.setURL(URL);
		}

		if (optionalComments == null) {
			teamMemberProjectRecognitionImpl.setOptionalComments(StringPool.BLANK);
		}
		else {
			teamMemberProjectRecognitionImpl.setOptionalComments(optionalComments);
		}

		teamMemberProjectRecognitionImpl.resetOriginalValues();

		return teamMemberProjectRecognitionImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		teamMemberProjectRecognitionId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		dateOfRecognition = objectInput.readLong();
		Name = objectInput.readUTF();

		CatRating = objectInput.readInt();
		URL = objectInput.readUTF();
		optionalComments = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(teamMemberProjectRecognitionId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);
		objectOutput.writeLong(dateOfRecognition);

		if (Name == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(Name);
		}

		objectOutput.writeInt(CatRating);

		if (URL == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(URL);
		}

		if (optionalComments == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(optionalComments);
		}
	}

	public String uuid;
	public long teamMemberProjectRecognitionId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long dateOfRecognition;
	public String Name;
	public int CatRating;
	public String URL;
	public String optionalComments;
}