/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.service.base;

import com.collaboratelab.impact.model.CareerImpact;
import com.collaboratelab.impact.service.CareerImpactService;
import com.collaboratelab.impact.service.persistence.CareerImpactPersistence;
import com.collaboratelab.impact.service.persistence.CollaboratorsPersistence;
import com.collaboratelab.impact.service.persistence.IntellectualPropertyPersistence;
import com.collaboratelab.impact.service.persistence.JobsPersistence;
import com.collaboratelab.impact.service.persistence.PatientsImpactedPersistence;
import com.collaboratelab.impact.service.persistence.PublicationsPersistence;
import com.collaboratelab.impact.service.persistence.SitesUsingSolutionPersistence;
import com.collaboratelab.impact.service.persistence.SupportPersistence;
import com.collaboratelab.impact.service.persistence.TeamMemberProjectRecognitionPersistence;

import com.liferay.asset.kernel.service.persistence.AssetEntryPersistence;
import com.liferay.asset.kernel.service.persistence.AssetTagPersistence;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.dao.db.DB;
import com.liferay.portal.kernel.dao.db.DBManagerUtil;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdate;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdateFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.module.framework.service.IdentifiableOSGiService;
import com.liferay.portal.kernel.service.BaseServiceImpl;
import com.liferay.portal.kernel.service.persistence.ClassNamePersistence;
import com.liferay.portal.kernel.service.persistence.UserPersistence;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import javax.sql.DataSource;

/**
 * Provides the base implementation for the career impact remote service.
 *
 * <p>
 * This implementation exists only as a container for the default service methods generated by ServiceBuilder. All custom service methods should be put in {@link com.collaboratelab.impact.service.impl.CareerImpactServiceImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.collaboratelab.impact.service.impl.CareerImpactServiceImpl
 * @see com.collaboratelab.impact.service.CareerImpactServiceUtil
 * @generated
 */
public abstract class CareerImpactServiceBaseImpl extends BaseServiceImpl
	implements CareerImpactService, IdentifiableOSGiService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link com.collaboratelab.impact.service.CareerImpactServiceUtil} to access the career impact remote service.
	 */

	/**
	 * Returns the career impact local service.
	 *
	 * @return the career impact local service
	 */
	public com.collaboratelab.impact.service.CareerImpactLocalService getCareerImpactLocalService() {
		return careerImpactLocalService;
	}

	/**
	 * Sets the career impact local service.
	 *
	 * @param careerImpactLocalService the career impact local service
	 */
	public void setCareerImpactLocalService(
		com.collaboratelab.impact.service.CareerImpactLocalService careerImpactLocalService) {
		this.careerImpactLocalService = careerImpactLocalService;
	}

	/**
	 * Returns the career impact remote service.
	 *
	 * @return the career impact remote service
	 */
	public CareerImpactService getCareerImpactService() {
		return careerImpactService;
	}

	/**
	 * Sets the career impact remote service.
	 *
	 * @param careerImpactService the career impact remote service
	 */
	public void setCareerImpactService(CareerImpactService careerImpactService) {
		this.careerImpactService = careerImpactService;
	}

	/**
	 * Returns the career impact persistence.
	 *
	 * @return the career impact persistence
	 */
	public CareerImpactPersistence getCareerImpactPersistence() {
		return careerImpactPersistence;
	}

	/**
	 * Sets the career impact persistence.
	 *
	 * @param careerImpactPersistence the career impact persistence
	 */
	public void setCareerImpactPersistence(
		CareerImpactPersistence careerImpactPersistence) {
		this.careerImpactPersistence = careerImpactPersistence;
	}

	/**
	 * Returns the collaborators local service.
	 *
	 * @return the collaborators local service
	 */
	public com.collaboratelab.impact.service.CollaboratorsLocalService getCollaboratorsLocalService() {
		return collaboratorsLocalService;
	}

	/**
	 * Sets the collaborators local service.
	 *
	 * @param collaboratorsLocalService the collaborators local service
	 */
	public void setCollaboratorsLocalService(
		com.collaboratelab.impact.service.CollaboratorsLocalService collaboratorsLocalService) {
		this.collaboratorsLocalService = collaboratorsLocalService;
	}

	/**
	 * Returns the collaborators remote service.
	 *
	 * @return the collaborators remote service
	 */
	public com.collaboratelab.impact.service.CollaboratorsService getCollaboratorsService() {
		return collaboratorsService;
	}

	/**
	 * Sets the collaborators remote service.
	 *
	 * @param collaboratorsService the collaborators remote service
	 */
	public void setCollaboratorsService(
		com.collaboratelab.impact.service.CollaboratorsService collaboratorsService) {
		this.collaboratorsService = collaboratorsService;
	}

	/**
	 * Returns the collaborators persistence.
	 *
	 * @return the collaborators persistence
	 */
	public CollaboratorsPersistence getCollaboratorsPersistence() {
		return collaboratorsPersistence;
	}

	/**
	 * Sets the collaborators persistence.
	 *
	 * @param collaboratorsPersistence the collaborators persistence
	 */
	public void setCollaboratorsPersistence(
		CollaboratorsPersistence collaboratorsPersistence) {
		this.collaboratorsPersistence = collaboratorsPersistence;
	}

	/**
	 * Returns the intellectual property local service.
	 *
	 * @return the intellectual property local service
	 */
	public com.collaboratelab.impact.service.IntellectualPropertyLocalService getIntellectualPropertyLocalService() {
		return intellectualPropertyLocalService;
	}

	/**
	 * Sets the intellectual property local service.
	 *
	 * @param intellectualPropertyLocalService the intellectual property local service
	 */
	public void setIntellectualPropertyLocalService(
		com.collaboratelab.impact.service.IntellectualPropertyLocalService intellectualPropertyLocalService) {
		this.intellectualPropertyLocalService = intellectualPropertyLocalService;
	}

	/**
	 * Returns the intellectual property remote service.
	 *
	 * @return the intellectual property remote service
	 */
	public com.collaboratelab.impact.service.IntellectualPropertyService getIntellectualPropertyService() {
		return intellectualPropertyService;
	}

	/**
	 * Sets the intellectual property remote service.
	 *
	 * @param intellectualPropertyService the intellectual property remote service
	 */
	public void setIntellectualPropertyService(
		com.collaboratelab.impact.service.IntellectualPropertyService intellectualPropertyService) {
		this.intellectualPropertyService = intellectualPropertyService;
	}

	/**
	 * Returns the intellectual property persistence.
	 *
	 * @return the intellectual property persistence
	 */
	public IntellectualPropertyPersistence getIntellectualPropertyPersistence() {
		return intellectualPropertyPersistence;
	}

	/**
	 * Sets the intellectual property persistence.
	 *
	 * @param intellectualPropertyPersistence the intellectual property persistence
	 */
	public void setIntellectualPropertyPersistence(
		IntellectualPropertyPersistence intellectualPropertyPersistence) {
		this.intellectualPropertyPersistence = intellectualPropertyPersistence;
	}

	/**
	 * Returns the jobs local service.
	 *
	 * @return the jobs local service
	 */
	public com.collaboratelab.impact.service.JobsLocalService getJobsLocalService() {
		return jobsLocalService;
	}

	/**
	 * Sets the jobs local service.
	 *
	 * @param jobsLocalService the jobs local service
	 */
	public void setJobsLocalService(
		com.collaboratelab.impact.service.JobsLocalService jobsLocalService) {
		this.jobsLocalService = jobsLocalService;
	}

	/**
	 * Returns the jobs remote service.
	 *
	 * @return the jobs remote service
	 */
	public com.collaboratelab.impact.service.JobsService getJobsService() {
		return jobsService;
	}

	/**
	 * Sets the jobs remote service.
	 *
	 * @param jobsService the jobs remote service
	 */
	public void setJobsService(
		com.collaboratelab.impact.service.JobsService jobsService) {
		this.jobsService = jobsService;
	}

	/**
	 * Returns the jobs persistence.
	 *
	 * @return the jobs persistence
	 */
	public JobsPersistence getJobsPersistence() {
		return jobsPersistence;
	}

	/**
	 * Sets the jobs persistence.
	 *
	 * @param jobsPersistence the jobs persistence
	 */
	public void setJobsPersistence(JobsPersistence jobsPersistence) {
		this.jobsPersistence = jobsPersistence;
	}

	/**
	 * Returns the patients impacted local service.
	 *
	 * @return the patients impacted local service
	 */
	public com.collaboratelab.impact.service.PatientsImpactedLocalService getPatientsImpactedLocalService() {
		return patientsImpactedLocalService;
	}

	/**
	 * Sets the patients impacted local service.
	 *
	 * @param patientsImpactedLocalService the patients impacted local service
	 */
	public void setPatientsImpactedLocalService(
		com.collaboratelab.impact.service.PatientsImpactedLocalService patientsImpactedLocalService) {
		this.patientsImpactedLocalService = patientsImpactedLocalService;
	}

	/**
	 * Returns the patients impacted remote service.
	 *
	 * @return the patients impacted remote service
	 */
	public com.collaboratelab.impact.service.PatientsImpactedService getPatientsImpactedService() {
		return patientsImpactedService;
	}

	/**
	 * Sets the patients impacted remote service.
	 *
	 * @param patientsImpactedService the patients impacted remote service
	 */
	public void setPatientsImpactedService(
		com.collaboratelab.impact.service.PatientsImpactedService patientsImpactedService) {
		this.patientsImpactedService = patientsImpactedService;
	}

	/**
	 * Returns the patients impacted persistence.
	 *
	 * @return the patients impacted persistence
	 */
	public PatientsImpactedPersistence getPatientsImpactedPersistence() {
		return patientsImpactedPersistence;
	}

	/**
	 * Sets the patients impacted persistence.
	 *
	 * @param patientsImpactedPersistence the patients impacted persistence
	 */
	public void setPatientsImpactedPersistence(
		PatientsImpactedPersistence patientsImpactedPersistence) {
		this.patientsImpactedPersistence = patientsImpactedPersistence;
	}

	/**
	 * Returns the publications local service.
	 *
	 * @return the publications local service
	 */
	public com.collaboratelab.impact.service.PublicationsLocalService getPublicationsLocalService() {
		return publicationsLocalService;
	}

	/**
	 * Sets the publications local service.
	 *
	 * @param publicationsLocalService the publications local service
	 */
	public void setPublicationsLocalService(
		com.collaboratelab.impact.service.PublicationsLocalService publicationsLocalService) {
		this.publicationsLocalService = publicationsLocalService;
	}

	/**
	 * Returns the publications remote service.
	 *
	 * @return the publications remote service
	 */
	public com.collaboratelab.impact.service.PublicationsService getPublicationsService() {
		return publicationsService;
	}

	/**
	 * Sets the publications remote service.
	 *
	 * @param publicationsService the publications remote service
	 */
	public void setPublicationsService(
		com.collaboratelab.impact.service.PublicationsService publicationsService) {
		this.publicationsService = publicationsService;
	}

	/**
	 * Returns the publications persistence.
	 *
	 * @return the publications persistence
	 */
	public PublicationsPersistence getPublicationsPersistence() {
		return publicationsPersistence;
	}

	/**
	 * Sets the publications persistence.
	 *
	 * @param publicationsPersistence the publications persistence
	 */
	public void setPublicationsPersistence(
		PublicationsPersistence publicationsPersistence) {
		this.publicationsPersistence = publicationsPersistence;
	}

	/**
	 * Returns the sites using solution local service.
	 *
	 * @return the sites using solution local service
	 */
	public com.collaboratelab.impact.service.SitesUsingSolutionLocalService getSitesUsingSolutionLocalService() {
		return sitesUsingSolutionLocalService;
	}

	/**
	 * Sets the sites using solution local service.
	 *
	 * @param sitesUsingSolutionLocalService the sites using solution local service
	 */
	public void setSitesUsingSolutionLocalService(
		com.collaboratelab.impact.service.SitesUsingSolutionLocalService sitesUsingSolutionLocalService) {
		this.sitesUsingSolutionLocalService = sitesUsingSolutionLocalService;
	}

	/**
	 * Returns the sites using solution remote service.
	 *
	 * @return the sites using solution remote service
	 */
	public com.collaboratelab.impact.service.SitesUsingSolutionService getSitesUsingSolutionService() {
		return sitesUsingSolutionService;
	}

	/**
	 * Sets the sites using solution remote service.
	 *
	 * @param sitesUsingSolutionService the sites using solution remote service
	 */
	public void setSitesUsingSolutionService(
		com.collaboratelab.impact.service.SitesUsingSolutionService sitesUsingSolutionService) {
		this.sitesUsingSolutionService = sitesUsingSolutionService;
	}

	/**
	 * Returns the sites using solution persistence.
	 *
	 * @return the sites using solution persistence
	 */
	public SitesUsingSolutionPersistence getSitesUsingSolutionPersistence() {
		return sitesUsingSolutionPersistence;
	}

	/**
	 * Sets the sites using solution persistence.
	 *
	 * @param sitesUsingSolutionPersistence the sites using solution persistence
	 */
	public void setSitesUsingSolutionPersistence(
		SitesUsingSolutionPersistence sitesUsingSolutionPersistence) {
		this.sitesUsingSolutionPersistence = sitesUsingSolutionPersistence;
	}

	/**
	 * Returns the support local service.
	 *
	 * @return the support local service
	 */
	public com.collaboratelab.impact.service.SupportLocalService getSupportLocalService() {
		return supportLocalService;
	}

	/**
	 * Sets the support local service.
	 *
	 * @param supportLocalService the support local service
	 */
	public void setSupportLocalService(
		com.collaboratelab.impact.service.SupportLocalService supportLocalService) {
		this.supportLocalService = supportLocalService;
	}

	/**
	 * Returns the support remote service.
	 *
	 * @return the support remote service
	 */
	public com.collaboratelab.impact.service.SupportService getSupportService() {
		return supportService;
	}

	/**
	 * Sets the support remote service.
	 *
	 * @param supportService the support remote service
	 */
	public void setSupportService(
		com.collaboratelab.impact.service.SupportService supportService) {
		this.supportService = supportService;
	}

	/**
	 * Returns the support persistence.
	 *
	 * @return the support persistence
	 */
	public SupportPersistence getSupportPersistence() {
		return supportPersistence;
	}

	/**
	 * Sets the support persistence.
	 *
	 * @param supportPersistence the support persistence
	 */
	public void setSupportPersistence(SupportPersistence supportPersistence) {
		this.supportPersistence = supportPersistence;
	}

	/**
	 * Returns the team member project recognition local service.
	 *
	 * @return the team member project recognition local service
	 */
	public com.collaboratelab.impact.service.TeamMemberProjectRecognitionLocalService getTeamMemberProjectRecognitionLocalService() {
		return teamMemberProjectRecognitionLocalService;
	}

	/**
	 * Sets the team member project recognition local service.
	 *
	 * @param teamMemberProjectRecognitionLocalService the team member project recognition local service
	 */
	public void setTeamMemberProjectRecognitionLocalService(
		com.collaboratelab.impact.service.TeamMemberProjectRecognitionLocalService teamMemberProjectRecognitionLocalService) {
		this.teamMemberProjectRecognitionLocalService = teamMemberProjectRecognitionLocalService;
	}

	/**
	 * Returns the team member project recognition remote service.
	 *
	 * @return the team member project recognition remote service
	 */
	public com.collaboratelab.impact.service.TeamMemberProjectRecognitionService getTeamMemberProjectRecognitionService() {
		return teamMemberProjectRecognitionService;
	}

	/**
	 * Sets the team member project recognition remote service.
	 *
	 * @param teamMemberProjectRecognitionService the team member project recognition remote service
	 */
	public void setTeamMemberProjectRecognitionService(
		com.collaboratelab.impact.service.TeamMemberProjectRecognitionService teamMemberProjectRecognitionService) {
		this.teamMemberProjectRecognitionService = teamMemberProjectRecognitionService;
	}

	/**
	 * Returns the team member project recognition persistence.
	 *
	 * @return the team member project recognition persistence
	 */
	public TeamMemberProjectRecognitionPersistence getTeamMemberProjectRecognitionPersistence() {
		return teamMemberProjectRecognitionPersistence;
	}

	/**
	 * Sets the team member project recognition persistence.
	 *
	 * @param teamMemberProjectRecognitionPersistence the team member project recognition persistence
	 */
	public void setTeamMemberProjectRecognitionPersistence(
		TeamMemberProjectRecognitionPersistence teamMemberProjectRecognitionPersistence) {
		this.teamMemberProjectRecognitionPersistence = teamMemberProjectRecognitionPersistence;
	}

	/**
	 * Returns the counter local service.
	 *
	 * @return the counter local service
	 */
	public com.liferay.counter.kernel.service.CounterLocalService getCounterLocalService() {
		return counterLocalService;
	}

	/**
	 * Sets the counter local service.
	 *
	 * @param counterLocalService the counter local service
	 */
	public void setCounterLocalService(
		com.liferay.counter.kernel.service.CounterLocalService counterLocalService) {
		this.counterLocalService = counterLocalService;
	}

	/**
	 * Returns the class name local service.
	 *
	 * @return the class name local service
	 */
	public com.liferay.portal.kernel.service.ClassNameLocalService getClassNameLocalService() {
		return classNameLocalService;
	}

	/**
	 * Sets the class name local service.
	 *
	 * @param classNameLocalService the class name local service
	 */
	public void setClassNameLocalService(
		com.liferay.portal.kernel.service.ClassNameLocalService classNameLocalService) {
		this.classNameLocalService = classNameLocalService;
	}

	/**
	 * Returns the class name remote service.
	 *
	 * @return the class name remote service
	 */
	public com.liferay.portal.kernel.service.ClassNameService getClassNameService() {
		return classNameService;
	}

	/**
	 * Sets the class name remote service.
	 *
	 * @param classNameService the class name remote service
	 */
	public void setClassNameService(
		com.liferay.portal.kernel.service.ClassNameService classNameService) {
		this.classNameService = classNameService;
	}

	/**
	 * Returns the class name persistence.
	 *
	 * @return the class name persistence
	 */
	public ClassNamePersistence getClassNamePersistence() {
		return classNamePersistence;
	}

	/**
	 * Sets the class name persistence.
	 *
	 * @param classNamePersistence the class name persistence
	 */
	public void setClassNamePersistence(
		ClassNamePersistence classNamePersistence) {
		this.classNamePersistence = classNamePersistence;
	}

	/**
	 * Returns the resource local service.
	 *
	 * @return the resource local service
	 */
	public com.liferay.portal.kernel.service.ResourceLocalService getResourceLocalService() {
		return resourceLocalService;
	}

	/**
	 * Sets the resource local service.
	 *
	 * @param resourceLocalService the resource local service
	 */
	public void setResourceLocalService(
		com.liferay.portal.kernel.service.ResourceLocalService resourceLocalService) {
		this.resourceLocalService = resourceLocalService;
	}

	/**
	 * Returns the user local service.
	 *
	 * @return the user local service
	 */
	public com.liferay.portal.kernel.service.UserLocalService getUserLocalService() {
		return userLocalService;
	}

	/**
	 * Sets the user local service.
	 *
	 * @param userLocalService the user local service
	 */
	public void setUserLocalService(
		com.liferay.portal.kernel.service.UserLocalService userLocalService) {
		this.userLocalService = userLocalService;
	}

	/**
	 * Returns the user remote service.
	 *
	 * @return the user remote service
	 */
	public com.liferay.portal.kernel.service.UserService getUserService() {
		return userService;
	}

	/**
	 * Sets the user remote service.
	 *
	 * @param userService the user remote service
	 */
	public void setUserService(
		com.liferay.portal.kernel.service.UserService userService) {
		this.userService = userService;
	}

	/**
	 * Returns the user persistence.
	 *
	 * @return the user persistence
	 */
	public UserPersistence getUserPersistence() {
		return userPersistence;
	}

	/**
	 * Sets the user persistence.
	 *
	 * @param userPersistence the user persistence
	 */
	public void setUserPersistence(UserPersistence userPersistence) {
		this.userPersistence = userPersistence;
	}

	/**
	 * Returns the asset entry local service.
	 *
	 * @return the asset entry local service
	 */
	public com.liferay.asset.kernel.service.AssetEntryLocalService getAssetEntryLocalService() {
		return assetEntryLocalService;
	}

	/**
	 * Sets the asset entry local service.
	 *
	 * @param assetEntryLocalService the asset entry local service
	 */
	public void setAssetEntryLocalService(
		com.liferay.asset.kernel.service.AssetEntryLocalService assetEntryLocalService) {
		this.assetEntryLocalService = assetEntryLocalService;
	}

	/**
	 * Returns the asset entry remote service.
	 *
	 * @return the asset entry remote service
	 */
	public com.liferay.asset.kernel.service.AssetEntryService getAssetEntryService() {
		return assetEntryService;
	}

	/**
	 * Sets the asset entry remote service.
	 *
	 * @param assetEntryService the asset entry remote service
	 */
	public void setAssetEntryService(
		com.liferay.asset.kernel.service.AssetEntryService assetEntryService) {
		this.assetEntryService = assetEntryService;
	}

	/**
	 * Returns the asset entry persistence.
	 *
	 * @return the asset entry persistence
	 */
	public AssetEntryPersistence getAssetEntryPersistence() {
		return assetEntryPersistence;
	}

	/**
	 * Sets the asset entry persistence.
	 *
	 * @param assetEntryPersistence the asset entry persistence
	 */
	public void setAssetEntryPersistence(
		AssetEntryPersistence assetEntryPersistence) {
		this.assetEntryPersistence = assetEntryPersistence;
	}

	/**
	 * Returns the asset tag local service.
	 *
	 * @return the asset tag local service
	 */
	public com.liferay.asset.kernel.service.AssetTagLocalService getAssetTagLocalService() {
		return assetTagLocalService;
	}

	/**
	 * Sets the asset tag local service.
	 *
	 * @param assetTagLocalService the asset tag local service
	 */
	public void setAssetTagLocalService(
		com.liferay.asset.kernel.service.AssetTagLocalService assetTagLocalService) {
		this.assetTagLocalService = assetTagLocalService;
	}

	/**
	 * Returns the asset tag remote service.
	 *
	 * @return the asset tag remote service
	 */
	public com.liferay.asset.kernel.service.AssetTagService getAssetTagService() {
		return assetTagService;
	}

	/**
	 * Sets the asset tag remote service.
	 *
	 * @param assetTagService the asset tag remote service
	 */
	public void setAssetTagService(
		com.liferay.asset.kernel.service.AssetTagService assetTagService) {
		this.assetTagService = assetTagService;
	}

	/**
	 * Returns the asset tag persistence.
	 *
	 * @return the asset tag persistence
	 */
	public AssetTagPersistence getAssetTagPersistence() {
		return assetTagPersistence;
	}

	/**
	 * Sets the asset tag persistence.
	 *
	 * @param assetTagPersistence the asset tag persistence
	 */
	public void setAssetTagPersistence(AssetTagPersistence assetTagPersistence) {
		this.assetTagPersistence = assetTagPersistence;
	}

	public void afterPropertiesSet() {
	}

	public void destroy() {
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return CareerImpactService.class.getName();
	}

	protected Class<?> getModelClass() {
		return CareerImpact.class;
	}

	protected String getModelClassName() {
		return CareerImpact.class.getName();
	}

	/**
	 * Performs a SQL query.
	 *
	 * @param sql the sql query
	 */
	protected void runSQL(String sql) {
		try {
			DataSource dataSource = careerImpactPersistence.getDataSource();

			DB db = DBManagerUtil.getDB();

			sql = db.buildSQL(sql);
			sql = PortalUtil.transformSQL(sql);

			SqlUpdate sqlUpdate = SqlUpdateFactoryUtil.getSqlUpdate(dataSource,
					sql);

			sqlUpdate.update();
		}
		catch (Exception e) {
			throw new SystemException(e);
		}
	}

	@BeanReference(type = com.collaboratelab.impact.service.CareerImpactLocalService.class)
	protected com.collaboratelab.impact.service.CareerImpactLocalService careerImpactLocalService;
	@BeanReference(type = CareerImpactService.class)
	protected CareerImpactService careerImpactService;
	@BeanReference(type = CareerImpactPersistence.class)
	protected CareerImpactPersistence careerImpactPersistence;
	@BeanReference(type = com.collaboratelab.impact.service.CollaboratorsLocalService.class)
	protected com.collaboratelab.impact.service.CollaboratorsLocalService collaboratorsLocalService;
	@BeanReference(type = com.collaboratelab.impact.service.CollaboratorsService.class)
	protected com.collaboratelab.impact.service.CollaboratorsService collaboratorsService;
	@BeanReference(type = CollaboratorsPersistence.class)
	protected CollaboratorsPersistence collaboratorsPersistence;
	@BeanReference(type = com.collaboratelab.impact.service.IntellectualPropertyLocalService.class)
	protected com.collaboratelab.impact.service.IntellectualPropertyLocalService intellectualPropertyLocalService;
	@BeanReference(type = com.collaboratelab.impact.service.IntellectualPropertyService.class)
	protected com.collaboratelab.impact.service.IntellectualPropertyService intellectualPropertyService;
	@BeanReference(type = IntellectualPropertyPersistence.class)
	protected IntellectualPropertyPersistence intellectualPropertyPersistence;
	@BeanReference(type = com.collaboratelab.impact.service.JobsLocalService.class)
	protected com.collaboratelab.impact.service.JobsLocalService jobsLocalService;
	@BeanReference(type = com.collaboratelab.impact.service.JobsService.class)
	protected com.collaboratelab.impact.service.JobsService jobsService;
	@BeanReference(type = JobsPersistence.class)
	protected JobsPersistence jobsPersistence;
	@BeanReference(type = com.collaboratelab.impact.service.PatientsImpactedLocalService.class)
	protected com.collaboratelab.impact.service.PatientsImpactedLocalService patientsImpactedLocalService;
	@BeanReference(type = com.collaboratelab.impact.service.PatientsImpactedService.class)
	protected com.collaboratelab.impact.service.PatientsImpactedService patientsImpactedService;
	@BeanReference(type = PatientsImpactedPersistence.class)
	protected PatientsImpactedPersistence patientsImpactedPersistence;
	@BeanReference(type = com.collaboratelab.impact.service.PublicationsLocalService.class)
	protected com.collaboratelab.impact.service.PublicationsLocalService publicationsLocalService;
	@BeanReference(type = com.collaboratelab.impact.service.PublicationsService.class)
	protected com.collaboratelab.impact.service.PublicationsService publicationsService;
	@BeanReference(type = PublicationsPersistence.class)
	protected PublicationsPersistence publicationsPersistence;
	@BeanReference(type = com.collaboratelab.impact.service.SitesUsingSolutionLocalService.class)
	protected com.collaboratelab.impact.service.SitesUsingSolutionLocalService sitesUsingSolutionLocalService;
	@BeanReference(type = com.collaboratelab.impact.service.SitesUsingSolutionService.class)
	protected com.collaboratelab.impact.service.SitesUsingSolutionService sitesUsingSolutionService;
	@BeanReference(type = SitesUsingSolutionPersistence.class)
	protected SitesUsingSolutionPersistence sitesUsingSolutionPersistence;
	@BeanReference(type = com.collaboratelab.impact.service.SupportLocalService.class)
	protected com.collaboratelab.impact.service.SupportLocalService supportLocalService;
	@BeanReference(type = com.collaboratelab.impact.service.SupportService.class)
	protected com.collaboratelab.impact.service.SupportService supportService;
	@BeanReference(type = SupportPersistence.class)
	protected SupportPersistence supportPersistence;
	@BeanReference(type = com.collaboratelab.impact.service.TeamMemberProjectRecognitionLocalService.class)
	protected com.collaboratelab.impact.service.TeamMemberProjectRecognitionLocalService teamMemberProjectRecognitionLocalService;
	@BeanReference(type = com.collaboratelab.impact.service.TeamMemberProjectRecognitionService.class)
	protected com.collaboratelab.impact.service.TeamMemberProjectRecognitionService teamMemberProjectRecognitionService;
	@BeanReference(type = TeamMemberProjectRecognitionPersistence.class)
	protected TeamMemberProjectRecognitionPersistence teamMemberProjectRecognitionPersistence;
	@ServiceReference(type = com.liferay.counter.kernel.service.CounterLocalService.class)
	protected com.liferay.counter.kernel.service.CounterLocalService counterLocalService;
	@ServiceReference(type = com.liferay.portal.kernel.service.ClassNameLocalService.class)
	protected com.liferay.portal.kernel.service.ClassNameLocalService classNameLocalService;
	@ServiceReference(type = com.liferay.portal.kernel.service.ClassNameService.class)
	protected com.liferay.portal.kernel.service.ClassNameService classNameService;
	@ServiceReference(type = ClassNamePersistence.class)
	protected ClassNamePersistence classNamePersistence;
	@ServiceReference(type = com.liferay.portal.kernel.service.ResourceLocalService.class)
	protected com.liferay.portal.kernel.service.ResourceLocalService resourceLocalService;
	@ServiceReference(type = com.liferay.portal.kernel.service.UserLocalService.class)
	protected com.liferay.portal.kernel.service.UserLocalService userLocalService;
	@ServiceReference(type = com.liferay.portal.kernel.service.UserService.class)
	protected com.liferay.portal.kernel.service.UserService userService;
	@ServiceReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	@ServiceReference(type = com.liferay.asset.kernel.service.AssetEntryLocalService.class)
	protected com.liferay.asset.kernel.service.AssetEntryLocalService assetEntryLocalService;
	@ServiceReference(type = com.liferay.asset.kernel.service.AssetEntryService.class)
	protected com.liferay.asset.kernel.service.AssetEntryService assetEntryService;
	@ServiceReference(type = AssetEntryPersistence.class)
	protected AssetEntryPersistence assetEntryPersistence;
	@ServiceReference(type = com.liferay.asset.kernel.service.AssetTagLocalService.class)
	protected com.liferay.asset.kernel.service.AssetTagLocalService assetTagLocalService;
	@ServiceReference(type = com.liferay.asset.kernel.service.AssetTagService.class)
	protected com.liferay.asset.kernel.service.AssetTagService assetTagService;
	@ServiceReference(type = AssetTagPersistence.class)
	protected AssetTagPersistence assetTagPersistence;
}