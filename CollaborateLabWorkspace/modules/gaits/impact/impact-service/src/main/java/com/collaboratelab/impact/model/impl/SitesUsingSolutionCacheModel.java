/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.impact.model.SitesUsingSolution;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing SitesUsingSolution in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see SitesUsingSolution
 * @generated
 */
@ProviderType
public class SitesUsingSolutionCacheModel implements CacheModel<SitesUsingSolution>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof SitesUsingSolutionCacheModel)) {
			return false;
		}

		SitesUsingSolutionCacheModel sitesUsingSolutionCacheModel = (SitesUsingSolutionCacheModel)obj;

		if (sitesUsingSolutionId == sitesUsingSolutionCacheModel.sitesUsingSolutionId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, sitesUsingSolutionId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", sitesUsingSolutionId=");
		sb.append(sitesUsingSolutionId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", asOfDate=");
		sb.append(asOfDate);
		sb.append(", number=");
		sb.append(number);
		sb.append(", optionalComments=");
		sb.append(optionalComments);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public SitesUsingSolution toEntityModel() {
		SitesUsingSolutionImpl sitesUsingSolutionImpl = new SitesUsingSolutionImpl();

		if (uuid == null) {
			sitesUsingSolutionImpl.setUuid(StringPool.BLANK);
		}
		else {
			sitesUsingSolutionImpl.setUuid(uuid);
		}

		sitesUsingSolutionImpl.setSitesUsingSolutionId(sitesUsingSolutionId);
		sitesUsingSolutionImpl.setGroupId(groupId);
		sitesUsingSolutionImpl.setCompanyId(companyId);
		sitesUsingSolutionImpl.setUserId(userId);

		if (userName == null) {
			sitesUsingSolutionImpl.setUserName(StringPool.BLANK);
		}
		else {
			sitesUsingSolutionImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			sitesUsingSolutionImpl.setCreateDate(null);
		}
		else {
			sitesUsingSolutionImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			sitesUsingSolutionImpl.setModifiedDate(null);
		}
		else {
			sitesUsingSolutionImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (asOfDate == Long.MIN_VALUE) {
			sitesUsingSolutionImpl.setAsOfDate(null);
		}
		else {
			sitesUsingSolutionImpl.setAsOfDate(new Date(asOfDate));
		}

		sitesUsingSolutionImpl.setNumber(number);

		if (optionalComments == null) {
			sitesUsingSolutionImpl.setOptionalComments(StringPool.BLANK);
		}
		else {
			sitesUsingSolutionImpl.setOptionalComments(optionalComments);
		}

		sitesUsingSolutionImpl.resetOriginalValues();

		return sitesUsingSolutionImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		sitesUsingSolutionId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		asOfDate = objectInput.readLong();

		number = objectInput.readInt();
		optionalComments = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(sitesUsingSolutionId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);
		objectOutput.writeLong(asOfDate);

		objectOutput.writeInt(number);

		if (optionalComments == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(optionalComments);
		}
	}

	public String uuid;
	public long sitesUsingSolutionId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long asOfDate;
	public int number;
	public String optionalComments;
}