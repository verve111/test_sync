/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.impact.model.IntellectualProperty;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing IntellectualProperty in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see IntellectualProperty
 * @generated
 */
@ProviderType
public class IntellectualPropertyCacheModel implements CacheModel<IntellectualProperty>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof IntellectualPropertyCacheModel)) {
			return false;
		}

		IntellectualPropertyCacheModel intellectualPropertyCacheModel = (IntellectualPropertyCacheModel)obj;

		if (intellectualPropertyId == intellectualPropertyCacheModel.intellectualPropertyId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, intellectualPropertyId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(27);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", intellectualPropertyId=");
		sb.append(intellectualPropertyId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", oneStFileDate=");
		sb.append(oneStFileDate);
		sb.append(", asOfDate=");
		sb.append(asOfDate);
		sb.append(", title=");
		sb.append(title);
		sb.append(", stage=");
		sb.append(stage);
		sb.append(", licenseStatus=");
		sb.append(licenseStatus);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public IntellectualProperty toEntityModel() {
		IntellectualPropertyImpl intellectualPropertyImpl = new IntellectualPropertyImpl();

		if (uuid == null) {
			intellectualPropertyImpl.setUuid(StringPool.BLANK);
		}
		else {
			intellectualPropertyImpl.setUuid(uuid);
		}

		intellectualPropertyImpl.setIntellectualPropertyId(intellectualPropertyId);
		intellectualPropertyImpl.setGroupId(groupId);
		intellectualPropertyImpl.setCompanyId(companyId);
		intellectualPropertyImpl.setUserId(userId);

		if (userName == null) {
			intellectualPropertyImpl.setUserName(StringPool.BLANK);
		}
		else {
			intellectualPropertyImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			intellectualPropertyImpl.setCreateDate(null);
		}
		else {
			intellectualPropertyImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			intellectualPropertyImpl.setModifiedDate(null);
		}
		else {
			intellectualPropertyImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (oneStFileDate == Long.MIN_VALUE) {
			intellectualPropertyImpl.setOneStFileDate(null);
		}
		else {
			intellectualPropertyImpl.setOneStFileDate(new Date(oneStFileDate));
		}

		if (asOfDate == Long.MIN_VALUE) {
			intellectualPropertyImpl.setAsOfDate(null);
		}
		else {
			intellectualPropertyImpl.setAsOfDate(new Date(asOfDate));
		}

		if (title == null) {
			intellectualPropertyImpl.setTitle(StringPool.BLANK);
		}
		else {
			intellectualPropertyImpl.setTitle(title);
		}

		intellectualPropertyImpl.setStage(stage);
		intellectualPropertyImpl.setLicenseStatus(licenseStatus);

		intellectualPropertyImpl.resetOriginalValues();

		return intellectualPropertyImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		intellectualPropertyId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		oneStFileDate = objectInput.readLong();
		asOfDate = objectInput.readLong();
		title = objectInput.readUTF();

		stage = objectInput.readInt();

		licenseStatus = objectInput.readInt();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(intellectualPropertyId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);
		objectOutput.writeLong(oneStFileDate);
		objectOutput.writeLong(asOfDate);

		if (title == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(title);
		}

		objectOutput.writeInt(stage);

		objectOutput.writeInt(licenseStatus);
	}

	public String uuid;
	public long intellectualPropertyId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long oneStFileDate;
	public long asOfDate;
	public String title;
	public int stage;
	public int licenseStatus;
}