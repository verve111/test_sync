/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.impact.model.CareerImpact;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing CareerImpact in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see CareerImpact
 * @generated
 */
@ProviderType
public class CareerImpactCacheModel implements CacheModel<CareerImpact>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CareerImpactCacheModel)) {
			return false;
		}

		CareerImpactCacheModel careerImpactCacheModel = (CareerImpactCacheModel)obj;

		if (careerImpactId == careerImpactCacheModel.careerImpactId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, careerImpactId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", careerImpactId=");
		sb.append(careerImpactId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", asOfDate=");
		sb.append(asOfDate);
		sb.append(", categoryRaing=");
		sb.append(categoryRaing);
		sb.append(", optionalComments=");
		sb.append(optionalComments);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public CareerImpact toEntityModel() {
		CareerImpactImpl careerImpactImpl = new CareerImpactImpl();

		if (uuid == null) {
			careerImpactImpl.setUuid(StringPool.BLANK);
		}
		else {
			careerImpactImpl.setUuid(uuid);
		}

		careerImpactImpl.setCareerImpactId(careerImpactId);
		careerImpactImpl.setGroupId(groupId);
		careerImpactImpl.setCompanyId(companyId);
		careerImpactImpl.setUserId(userId);

		if (userName == null) {
			careerImpactImpl.setUserName(StringPool.BLANK);
		}
		else {
			careerImpactImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			careerImpactImpl.setCreateDate(null);
		}
		else {
			careerImpactImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			careerImpactImpl.setModifiedDate(null);
		}
		else {
			careerImpactImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (asOfDate == Long.MIN_VALUE) {
			careerImpactImpl.setAsOfDate(null);
		}
		else {
			careerImpactImpl.setAsOfDate(new Date(asOfDate));
		}

		careerImpactImpl.setCategoryRaing(categoryRaing);

		if (optionalComments == null) {
			careerImpactImpl.setOptionalComments(StringPool.BLANK);
		}
		else {
			careerImpactImpl.setOptionalComments(optionalComments);
		}

		careerImpactImpl.resetOriginalValues();

		return careerImpactImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		careerImpactId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		asOfDate = objectInput.readLong();

		categoryRaing = objectInput.readInt();
		optionalComments = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(careerImpactId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);
		objectOutput.writeLong(asOfDate);

		objectOutput.writeInt(categoryRaing);

		if (optionalComments == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(optionalComments);
		}
	}

	public String uuid;
	public long careerImpactId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long asOfDate;
	public int categoryRaing;
	public String optionalComments;
}