/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.impact.exception.NoSuchTeamMemberProjectRecognitionException;
import com.collaboratelab.impact.model.TeamMemberProjectRecognition;
import com.collaboratelab.impact.model.impl.TeamMemberProjectRecognitionImpl;
import com.collaboratelab.impact.model.impl.TeamMemberProjectRecognitionModelImpl;
import com.collaboratelab.impact.service.persistence.TeamMemberProjectRecognitionPersistence;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.CompanyProvider;
import com.liferay.portal.kernel.service.persistence.CompanyProviderWrapper;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the team member project recognition service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see TeamMemberProjectRecognitionPersistence
 * @see com.collaboratelab.impact.service.persistence.TeamMemberProjectRecognitionUtil
 * @generated
 */
@ProviderType
public class TeamMemberProjectRecognitionPersistenceImpl
	extends BasePersistenceImpl<TeamMemberProjectRecognition>
	implements TeamMemberProjectRecognitionPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link TeamMemberProjectRecognitionUtil} to access the team member project recognition persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = TeamMemberProjectRecognitionImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(TeamMemberProjectRecognitionModelImpl.ENTITY_CACHE_ENABLED,
			TeamMemberProjectRecognitionModelImpl.FINDER_CACHE_ENABLED,
			TeamMemberProjectRecognitionImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(TeamMemberProjectRecognitionModelImpl.ENTITY_CACHE_ENABLED,
			TeamMemberProjectRecognitionModelImpl.FINDER_CACHE_ENABLED,
			TeamMemberProjectRecognitionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(TeamMemberProjectRecognitionModelImpl.ENTITY_CACHE_ENABLED,
			TeamMemberProjectRecognitionModelImpl.FINDER_CACHE_ENABLED,
			Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID = new FinderPath(TeamMemberProjectRecognitionModelImpl.ENTITY_CACHE_ENABLED,
			TeamMemberProjectRecognitionModelImpl.FINDER_CACHE_ENABLED,
			TeamMemberProjectRecognitionImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID = new FinderPath(TeamMemberProjectRecognitionModelImpl.ENTITY_CACHE_ENABLED,
			TeamMemberProjectRecognitionModelImpl.FINDER_CACHE_ENABLED,
			TeamMemberProjectRecognitionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] { String.class.getName() },
			TeamMemberProjectRecognitionModelImpl.UUID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID = new FinderPath(TeamMemberProjectRecognitionModelImpl.ENTITY_CACHE_ENABLED,
			TeamMemberProjectRecognitionModelImpl.FINDER_CACHE_ENABLED,
			Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByUuid", new String[] { String.class.getName() });

	/**
	 * Returns all the team member project recognitions where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching team member project recognitions
	 */
	@Override
	public List<TeamMemberProjectRecognition> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the team member project recognitions where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of team member project recognitions
	 * @param end the upper bound of the range of team member project recognitions (not inclusive)
	 * @return the range of matching team member project recognitions
	 */
	@Override
	public List<TeamMemberProjectRecognition> findByUuid(String uuid,
		int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the team member project recognitions where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of team member project recognitions
	 * @param end the upper bound of the range of team member project recognitions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching team member project recognitions
	 */
	@Override
	public List<TeamMemberProjectRecognition> findByUuid(String uuid,
		int start, int end,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator) {
		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the team member project recognitions where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of team member project recognitions
	 * @param end the upper bound of the range of team member project recognitions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching team member project recognitions
	 */
	@Override
	public List<TeamMemberProjectRecognition> findByUuid(String uuid,
		int start, int end,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid, start, end, orderByComparator };
		}

		List<TeamMemberProjectRecognition> list = null;

		if (retrieveFromCache) {
			list = (List<TeamMemberProjectRecognition>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (TeamMemberProjectRecognition teamMemberProjectRecognition : list) {
					if (!Objects.equals(uuid,
								teamMemberProjectRecognition.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_TEAMMEMBERPROJECTRECOGNITION_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(TeamMemberProjectRecognitionModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<TeamMemberProjectRecognition>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<TeamMemberProjectRecognition>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first team member project recognition in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching team member project recognition
	 * @throws NoSuchTeamMemberProjectRecognitionException if a matching team member project recognition could not be found
	 */
	@Override
	public TeamMemberProjectRecognition findByUuid_First(String uuid,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator)
		throws NoSuchTeamMemberProjectRecognitionException {
		TeamMemberProjectRecognition teamMemberProjectRecognition = fetchByUuid_First(uuid,
				orderByComparator);

		if (teamMemberProjectRecognition != null) {
			return teamMemberProjectRecognition;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTeamMemberProjectRecognitionException(msg.toString());
	}

	/**
	 * Returns the first team member project recognition in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching team member project recognition, or <code>null</code> if a matching team member project recognition could not be found
	 */
	@Override
	public TeamMemberProjectRecognition fetchByUuid_First(String uuid,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator) {
		List<TeamMemberProjectRecognition> list = findByUuid(uuid, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last team member project recognition in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching team member project recognition
	 * @throws NoSuchTeamMemberProjectRecognitionException if a matching team member project recognition could not be found
	 */
	@Override
	public TeamMemberProjectRecognition findByUuid_Last(String uuid,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator)
		throws NoSuchTeamMemberProjectRecognitionException {
		TeamMemberProjectRecognition teamMemberProjectRecognition = fetchByUuid_Last(uuid,
				orderByComparator);

		if (teamMemberProjectRecognition != null) {
			return teamMemberProjectRecognition;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTeamMemberProjectRecognitionException(msg.toString());
	}

	/**
	 * Returns the last team member project recognition in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching team member project recognition, or <code>null</code> if a matching team member project recognition could not be found
	 */
	@Override
	public TeamMemberProjectRecognition fetchByUuid_Last(String uuid,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator) {
		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<TeamMemberProjectRecognition> list = findByUuid(uuid, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the team member project recognitions before and after the current team member project recognition in the ordered set where uuid = &#63;.
	 *
	 * @param teamMemberProjectRecognitionId the primary key of the current team member project recognition
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next team member project recognition
	 * @throws NoSuchTeamMemberProjectRecognitionException if a team member project recognition with the primary key could not be found
	 */
	@Override
	public TeamMemberProjectRecognition[] findByUuid_PrevAndNext(
		long teamMemberProjectRecognitionId, String uuid,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator)
		throws NoSuchTeamMemberProjectRecognitionException {
		TeamMemberProjectRecognition teamMemberProjectRecognition = findByPrimaryKey(teamMemberProjectRecognitionId);

		Session session = null;

		try {
			session = openSession();

			TeamMemberProjectRecognition[] array = new TeamMemberProjectRecognitionImpl[3];

			array[0] = getByUuid_PrevAndNext(session,
					teamMemberProjectRecognition, uuid, orderByComparator, true);

			array[1] = teamMemberProjectRecognition;

			array[2] = getByUuid_PrevAndNext(session,
					teamMemberProjectRecognition, uuid, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected TeamMemberProjectRecognition getByUuid_PrevAndNext(
		Session session,
		TeamMemberProjectRecognition teamMemberProjectRecognition, String uuid,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_TEAMMEMBERPROJECTRECOGNITION_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(TeamMemberProjectRecognitionModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(teamMemberProjectRecognition);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<TeamMemberProjectRecognition> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the team member project recognitions where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (TeamMemberProjectRecognition teamMemberProjectRecognition : findByUuid(
				uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(teamMemberProjectRecognition);
		}
	}

	/**
	 * Returns the number of team member project recognitions where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching team member project recognitions
	 */
	@Override
	public int countByUuid(String uuid) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID;

		Object[] finderArgs = new Object[] { uuid };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_TEAMMEMBERPROJECTRECOGNITION_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_1 = "teamMemberProjectRecognition.uuid IS NULL";
	private static final String _FINDER_COLUMN_UUID_UUID_2 = "teamMemberProjectRecognition.uuid = ?";
	private static final String _FINDER_COLUMN_UUID_UUID_3 = "(teamMemberProjectRecognition.uuid IS NULL OR teamMemberProjectRecognition.uuid = '')";
	public static final FinderPath FINDER_PATH_FETCH_BY_UUID_G = new FinderPath(TeamMemberProjectRecognitionModelImpl.ENTITY_CACHE_ENABLED,
			TeamMemberProjectRecognitionModelImpl.FINDER_CACHE_ENABLED,
			TeamMemberProjectRecognitionImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByUUID_G",
			new String[] { String.class.getName(), Long.class.getName() },
			TeamMemberProjectRecognitionModelImpl.UUID_COLUMN_BITMASK |
			TeamMemberProjectRecognitionModelImpl.GROUPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_G = new FinderPath(TeamMemberProjectRecognitionModelImpl.ENTITY_CACHE_ENABLED,
			TeamMemberProjectRecognitionModelImpl.FINDER_CACHE_ENABLED,
			Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByUUID_G",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns the team member project recognition where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchTeamMemberProjectRecognitionException} if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching team member project recognition
	 * @throws NoSuchTeamMemberProjectRecognitionException if a matching team member project recognition could not be found
	 */
	@Override
	public TeamMemberProjectRecognition findByUUID_G(String uuid, long groupId)
		throws NoSuchTeamMemberProjectRecognitionException {
		TeamMemberProjectRecognition teamMemberProjectRecognition = fetchByUUID_G(uuid,
				groupId);

		if (teamMemberProjectRecognition == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("uuid=");
			msg.append(uuid);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchTeamMemberProjectRecognitionException(msg.toString());
		}

		return teamMemberProjectRecognition;
	}

	/**
	 * Returns the team member project recognition where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching team member project recognition, or <code>null</code> if a matching team member project recognition could not be found
	 */
	@Override
	public TeamMemberProjectRecognition fetchByUUID_G(String uuid, long groupId) {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the team member project recognition where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching team member project recognition, or <code>null</code> if a matching team member project recognition could not be found
	 */
	@Override
	public TeamMemberProjectRecognition fetchByUUID_G(String uuid,
		long groupId, boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { uuid, groupId };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_UUID_G,
					finderArgs, this);
		}

		if (result instanceof TeamMemberProjectRecognition) {
			TeamMemberProjectRecognition teamMemberProjectRecognition = (TeamMemberProjectRecognition)result;

			if (!Objects.equals(uuid, teamMemberProjectRecognition.getUuid()) ||
					(groupId != teamMemberProjectRecognition.getGroupId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_TEAMMEMBERPROJECTRECOGNITION_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				List<TeamMemberProjectRecognition> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G,
						finderArgs, list);
				}
				else {
					TeamMemberProjectRecognition teamMemberProjectRecognition = list.get(0);

					result = teamMemberProjectRecognition;

					cacheResult(teamMemberProjectRecognition);

					if ((teamMemberProjectRecognition.getUuid() == null) ||
							!teamMemberProjectRecognition.getUuid().equals(uuid) ||
							(teamMemberProjectRecognition.getGroupId() != groupId)) {
						finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G,
							finderArgs, teamMemberProjectRecognition);
					}
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_UUID_G, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (TeamMemberProjectRecognition)result;
		}
	}

	/**
	 * Removes the team member project recognition where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the team member project recognition that was removed
	 */
	@Override
	public TeamMemberProjectRecognition removeByUUID_G(String uuid, long groupId)
		throws NoSuchTeamMemberProjectRecognitionException {
		TeamMemberProjectRecognition teamMemberProjectRecognition = findByUUID_G(uuid,
				groupId);

		return remove(teamMemberProjectRecognition);
	}

	/**
	 * Returns the number of team member project recognitions where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching team member project recognitions
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_G;

		Object[] finderArgs = new Object[] { uuid, groupId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_TEAMMEMBERPROJECTRECOGNITION_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_1 = "teamMemberProjectRecognition.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_2 = "teamMemberProjectRecognition.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_3 = "(teamMemberProjectRecognition.uuid IS NULL OR teamMemberProjectRecognition.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 = "teamMemberProjectRecognition.groupId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C = new FinderPath(TeamMemberProjectRecognitionModelImpl.ENTITY_CACHE_ENABLED,
			TeamMemberProjectRecognitionModelImpl.FINDER_CACHE_ENABLED,
			TeamMemberProjectRecognitionImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C =
		new FinderPath(TeamMemberProjectRecognitionModelImpl.ENTITY_CACHE_ENABLED,
			TeamMemberProjectRecognitionModelImpl.FINDER_CACHE_ENABLED,
			TeamMemberProjectRecognitionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() },
			TeamMemberProjectRecognitionModelImpl.UUID_COLUMN_BITMASK |
			TeamMemberProjectRecognitionModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_C = new FinderPath(TeamMemberProjectRecognitionModelImpl.ENTITY_CACHE_ENABLED,
			TeamMemberProjectRecognitionModelImpl.FINDER_CACHE_ENABLED,
			Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns all the team member project recognitions where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching team member project recognitions
	 */
	@Override
	public List<TeamMemberProjectRecognition> findByUuid_C(String uuid,
		long companyId) {
		return findByUuid_C(uuid, companyId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the team member project recognitions where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of team member project recognitions
	 * @param end the upper bound of the range of team member project recognitions (not inclusive)
	 * @return the range of matching team member project recognitions
	 */
	@Override
	public List<TeamMemberProjectRecognition> findByUuid_C(String uuid,
		long companyId, int start, int end) {
		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the team member project recognitions where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of team member project recognitions
	 * @param end the upper bound of the range of team member project recognitions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching team member project recognitions
	 */
	@Override
	public List<TeamMemberProjectRecognition> findByUuid_C(String uuid,
		long companyId, int start, int end,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator) {
		return findByUuid_C(uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the team member project recognitions where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of team member project recognitions
	 * @param end the upper bound of the range of team member project recognitions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching team member project recognitions
	 */
	@Override
	public List<TeamMemberProjectRecognition> findByUuid_C(String uuid,
		long companyId, int start, int end,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] { uuid, companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] {
					uuid, companyId,
					
					start, end, orderByComparator
				};
		}

		List<TeamMemberProjectRecognition> list = null;

		if (retrieveFromCache) {
			list = (List<TeamMemberProjectRecognition>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (TeamMemberProjectRecognition teamMemberProjectRecognition : list) {
					if (!Objects.equals(uuid,
								teamMemberProjectRecognition.getUuid()) ||
							(companyId != teamMemberProjectRecognition.getCompanyId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_TEAMMEMBERPROJECTRECOGNITION_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(TeamMemberProjectRecognitionModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				if (!pagination) {
					list = (List<TeamMemberProjectRecognition>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<TeamMemberProjectRecognition>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first team member project recognition in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching team member project recognition
	 * @throws NoSuchTeamMemberProjectRecognitionException if a matching team member project recognition could not be found
	 */
	@Override
	public TeamMemberProjectRecognition findByUuid_C_First(String uuid,
		long companyId,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator)
		throws NoSuchTeamMemberProjectRecognitionException {
		TeamMemberProjectRecognition teamMemberProjectRecognition = fetchByUuid_C_First(uuid,
				companyId, orderByComparator);

		if (teamMemberProjectRecognition != null) {
			return teamMemberProjectRecognition;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTeamMemberProjectRecognitionException(msg.toString());
	}

	/**
	 * Returns the first team member project recognition in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching team member project recognition, or <code>null</code> if a matching team member project recognition could not be found
	 */
	@Override
	public TeamMemberProjectRecognition fetchByUuid_C_First(String uuid,
		long companyId,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator) {
		List<TeamMemberProjectRecognition> list = findByUuid_C(uuid, companyId,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last team member project recognition in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching team member project recognition
	 * @throws NoSuchTeamMemberProjectRecognitionException if a matching team member project recognition could not be found
	 */
	@Override
	public TeamMemberProjectRecognition findByUuid_C_Last(String uuid,
		long companyId,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator)
		throws NoSuchTeamMemberProjectRecognitionException {
		TeamMemberProjectRecognition teamMemberProjectRecognition = fetchByUuid_C_Last(uuid,
				companyId, orderByComparator);

		if (teamMemberProjectRecognition != null) {
			return teamMemberProjectRecognition;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTeamMemberProjectRecognitionException(msg.toString());
	}

	/**
	 * Returns the last team member project recognition in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching team member project recognition, or <code>null</code> if a matching team member project recognition could not be found
	 */
	@Override
	public TeamMemberProjectRecognition fetchByUuid_C_Last(String uuid,
		long companyId,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator) {
		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<TeamMemberProjectRecognition> list = findByUuid_C(uuid, companyId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the team member project recognitions before and after the current team member project recognition in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param teamMemberProjectRecognitionId the primary key of the current team member project recognition
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next team member project recognition
	 * @throws NoSuchTeamMemberProjectRecognitionException if a team member project recognition with the primary key could not be found
	 */
	@Override
	public TeamMemberProjectRecognition[] findByUuid_C_PrevAndNext(
		long teamMemberProjectRecognitionId, String uuid, long companyId,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator)
		throws NoSuchTeamMemberProjectRecognitionException {
		TeamMemberProjectRecognition teamMemberProjectRecognition = findByPrimaryKey(teamMemberProjectRecognitionId);

		Session session = null;

		try {
			session = openSession();

			TeamMemberProjectRecognition[] array = new TeamMemberProjectRecognitionImpl[3];

			array[0] = getByUuid_C_PrevAndNext(session,
					teamMemberProjectRecognition, uuid, companyId,
					orderByComparator, true);

			array[1] = teamMemberProjectRecognition;

			array[2] = getByUuid_C_PrevAndNext(session,
					teamMemberProjectRecognition, uuid, companyId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected TeamMemberProjectRecognition getByUuid_C_PrevAndNext(
		Session session,
		TeamMemberProjectRecognition teamMemberProjectRecognition, String uuid,
		long companyId,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_TEAMMEMBERPROJECTRECOGNITION_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(TeamMemberProjectRecognitionModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(teamMemberProjectRecognition);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<TeamMemberProjectRecognition> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the team member project recognitions where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (TeamMemberProjectRecognition teamMemberProjectRecognition : findByUuid_C(
				uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(teamMemberProjectRecognition);
		}
	}

	/**
	 * Returns the number of team member project recognitions where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching team member project recognitions
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_C;

		Object[] finderArgs = new Object[] { uuid, companyId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_TEAMMEMBERPROJECTRECOGNITION_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_1 = "teamMemberProjectRecognition.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_2 = "teamMemberProjectRecognition.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_3 = "(teamMemberProjectRecognition.uuid IS NULL OR teamMemberProjectRecognition.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 = "teamMemberProjectRecognition.companyId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_TEAMMEMBERPROJECTRECOGNITIONID =
		new FinderPath(TeamMemberProjectRecognitionModelImpl.ENTITY_CACHE_ENABLED,
			TeamMemberProjectRecognitionModelImpl.FINDER_CACHE_ENABLED,
			TeamMemberProjectRecognitionImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByTeamMemberProjectRecognitionId",
			new String[] { Long.class.getName() },
			TeamMemberProjectRecognitionModelImpl.TEAMMEMBERPROJECTRECOGNITIONID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_TEAMMEMBERPROJECTRECOGNITIONID =
		new FinderPath(TeamMemberProjectRecognitionModelImpl.ENTITY_CACHE_ENABLED,
			TeamMemberProjectRecognitionModelImpl.FINDER_CACHE_ENABLED,
			Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByTeamMemberProjectRecognitionId",
			new String[] { Long.class.getName() });

	/**
	 * Returns the team member project recognition where teamMemberProjectRecognitionId = &#63; or throws a {@link NoSuchTeamMemberProjectRecognitionException} if it could not be found.
	 *
	 * @param teamMemberProjectRecognitionId the team member project recognition ID
	 * @return the matching team member project recognition
	 * @throws NoSuchTeamMemberProjectRecognitionException if a matching team member project recognition could not be found
	 */
	@Override
	public TeamMemberProjectRecognition findByTeamMemberProjectRecognitionId(
		long teamMemberProjectRecognitionId)
		throws NoSuchTeamMemberProjectRecognitionException {
		TeamMemberProjectRecognition teamMemberProjectRecognition = fetchByTeamMemberProjectRecognitionId(teamMemberProjectRecognitionId);

		if (teamMemberProjectRecognition == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("teamMemberProjectRecognitionId=");
			msg.append(teamMemberProjectRecognitionId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchTeamMemberProjectRecognitionException(msg.toString());
		}

		return teamMemberProjectRecognition;
	}

	/**
	 * Returns the team member project recognition where teamMemberProjectRecognitionId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param teamMemberProjectRecognitionId the team member project recognition ID
	 * @return the matching team member project recognition, or <code>null</code> if a matching team member project recognition could not be found
	 */
	@Override
	public TeamMemberProjectRecognition fetchByTeamMemberProjectRecognitionId(
		long teamMemberProjectRecognitionId) {
		return fetchByTeamMemberProjectRecognitionId(teamMemberProjectRecognitionId,
			true);
	}

	/**
	 * Returns the team member project recognition where teamMemberProjectRecognitionId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param teamMemberProjectRecognitionId the team member project recognition ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching team member project recognition, or <code>null</code> if a matching team member project recognition could not be found
	 */
	@Override
	public TeamMemberProjectRecognition fetchByTeamMemberProjectRecognitionId(
		long teamMemberProjectRecognitionId, boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { teamMemberProjectRecognitionId };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_TEAMMEMBERPROJECTRECOGNITIONID,
					finderArgs, this);
		}

		if (result instanceof TeamMemberProjectRecognition) {
			TeamMemberProjectRecognition teamMemberProjectRecognition = (TeamMemberProjectRecognition)result;

			if ((teamMemberProjectRecognitionId != teamMemberProjectRecognition.getTeamMemberProjectRecognitionId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_TEAMMEMBERPROJECTRECOGNITION_WHERE);

			query.append(_FINDER_COLUMN_TEAMMEMBERPROJECTRECOGNITIONID_TEAMMEMBERPROJECTRECOGNITIONID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(teamMemberProjectRecognitionId);

				List<TeamMemberProjectRecognition> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_TEAMMEMBERPROJECTRECOGNITIONID,
						finderArgs, list);
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							_log.warn(
								"TeamMemberProjectRecognitionPersistenceImpl.fetchByTeamMemberProjectRecognitionId(long, boolean) with parameters (" +
								StringUtil.merge(finderArgs) +
								") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					TeamMemberProjectRecognition teamMemberProjectRecognition = list.get(0);

					result = teamMemberProjectRecognition;

					cacheResult(teamMemberProjectRecognition);

					if ((teamMemberProjectRecognition.getTeamMemberProjectRecognitionId() != teamMemberProjectRecognitionId)) {
						finderCache.putResult(FINDER_PATH_FETCH_BY_TEAMMEMBERPROJECTRECOGNITIONID,
							finderArgs, teamMemberProjectRecognition);
					}
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_TEAMMEMBERPROJECTRECOGNITIONID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (TeamMemberProjectRecognition)result;
		}
	}

	/**
	 * Removes the team member project recognition where teamMemberProjectRecognitionId = &#63; from the database.
	 *
	 * @param teamMemberProjectRecognitionId the team member project recognition ID
	 * @return the team member project recognition that was removed
	 */
	@Override
	public TeamMemberProjectRecognition removeByTeamMemberProjectRecognitionId(
		long teamMemberProjectRecognitionId)
		throws NoSuchTeamMemberProjectRecognitionException {
		TeamMemberProjectRecognition teamMemberProjectRecognition = findByTeamMemberProjectRecognitionId(teamMemberProjectRecognitionId);

		return remove(teamMemberProjectRecognition);
	}

	/**
	 * Returns the number of team member project recognitions where teamMemberProjectRecognitionId = &#63;.
	 *
	 * @param teamMemberProjectRecognitionId the team member project recognition ID
	 * @return the number of matching team member project recognitions
	 */
	@Override
	public int countByTeamMemberProjectRecognitionId(
		long teamMemberProjectRecognitionId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_TEAMMEMBERPROJECTRECOGNITIONID;

		Object[] finderArgs = new Object[] { teamMemberProjectRecognitionId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_TEAMMEMBERPROJECTRECOGNITION_WHERE);

			query.append(_FINDER_COLUMN_TEAMMEMBERPROJECTRECOGNITIONID_TEAMMEMBERPROJECTRECOGNITIONID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(teamMemberProjectRecognitionId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_TEAMMEMBERPROJECTRECOGNITIONID_TEAMMEMBERPROJECTRECOGNITIONID_2 =
		"teamMemberProjectRecognition.teamMemberProjectRecognitionId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_GROUPID = new FinderPath(TeamMemberProjectRecognitionModelImpl.ENTITY_CACHE_ENABLED,
			TeamMemberProjectRecognitionModelImpl.FINDER_CACHE_ENABLED,
			TeamMemberProjectRecognitionImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByGroupId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID =
		new FinderPath(TeamMemberProjectRecognitionModelImpl.ENTITY_CACHE_ENABLED,
			TeamMemberProjectRecognitionModelImpl.FINDER_CACHE_ENABLED,
			TeamMemberProjectRecognitionImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByGroupId",
			new String[] { Long.class.getName() },
			TeamMemberProjectRecognitionModelImpl.GROUPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_GROUPID = new FinderPath(TeamMemberProjectRecognitionModelImpl.ENTITY_CACHE_ENABLED,
			TeamMemberProjectRecognitionModelImpl.FINDER_CACHE_ENABLED,
			Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByGroupId", new String[] { Long.class.getName() });

	/**
	 * Returns all the team member project recognitions where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching team member project recognitions
	 */
	@Override
	public List<TeamMemberProjectRecognition> findByGroupId(long groupId) {
		return findByGroupId(groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the team member project recognitions where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of team member project recognitions
	 * @param end the upper bound of the range of team member project recognitions (not inclusive)
	 * @return the range of matching team member project recognitions
	 */
	@Override
	public List<TeamMemberProjectRecognition> findByGroupId(long groupId,
		int start, int end) {
		return findByGroupId(groupId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the team member project recognitions where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of team member project recognitions
	 * @param end the upper bound of the range of team member project recognitions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching team member project recognitions
	 */
	@Override
	public List<TeamMemberProjectRecognition> findByGroupId(long groupId,
		int start, int end,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator) {
		return findByGroupId(groupId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the team member project recognitions where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of team member project recognitions
	 * @param end the upper bound of the range of team member project recognitions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching team member project recognitions
	 */
	@Override
	public List<TeamMemberProjectRecognition> findByGroupId(long groupId,
		int start, int end,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID;
			finderArgs = new Object[] { groupId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_GROUPID;
			finderArgs = new Object[] { groupId, start, end, orderByComparator };
		}

		List<TeamMemberProjectRecognition> list = null;

		if (retrieveFromCache) {
			list = (List<TeamMemberProjectRecognition>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (TeamMemberProjectRecognition teamMemberProjectRecognition : list) {
					if ((groupId != teamMemberProjectRecognition.getGroupId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_TEAMMEMBERPROJECTRECOGNITION_WHERE);

			query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(TeamMemberProjectRecognitionModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				if (!pagination) {
					list = (List<TeamMemberProjectRecognition>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<TeamMemberProjectRecognition>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first team member project recognition in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching team member project recognition
	 * @throws NoSuchTeamMemberProjectRecognitionException if a matching team member project recognition could not be found
	 */
	@Override
	public TeamMemberProjectRecognition findByGroupId_First(long groupId,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator)
		throws NoSuchTeamMemberProjectRecognitionException {
		TeamMemberProjectRecognition teamMemberProjectRecognition = fetchByGroupId_First(groupId,
				orderByComparator);

		if (teamMemberProjectRecognition != null) {
			return teamMemberProjectRecognition;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("groupId=");
		msg.append(groupId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTeamMemberProjectRecognitionException(msg.toString());
	}

	/**
	 * Returns the first team member project recognition in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching team member project recognition, or <code>null</code> if a matching team member project recognition could not be found
	 */
	@Override
	public TeamMemberProjectRecognition fetchByGroupId_First(long groupId,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator) {
		List<TeamMemberProjectRecognition> list = findByGroupId(groupId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last team member project recognition in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching team member project recognition
	 * @throws NoSuchTeamMemberProjectRecognitionException if a matching team member project recognition could not be found
	 */
	@Override
	public TeamMemberProjectRecognition findByGroupId_Last(long groupId,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator)
		throws NoSuchTeamMemberProjectRecognitionException {
		TeamMemberProjectRecognition teamMemberProjectRecognition = fetchByGroupId_Last(groupId,
				orderByComparator);

		if (teamMemberProjectRecognition != null) {
			return teamMemberProjectRecognition;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("groupId=");
		msg.append(groupId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchTeamMemberProjectRecognitionException(msg.toString());
	}

	/**
	 * Returns the last team member project recognition in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching team member project recognition, or <code>null</code> if a matching team member project recognition could not be found
	 */
	@Override
	public TeamMemberProjectRecognition fetchByGroupId_Last(long groupId,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator) {
		int count = countByGroupId(groupId);

		if (count == 0) {
			return null;
		}

		List<TeamMemberProjectRecognition> list = findByGroupId(groupId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the team member project recognitions before and after the current team member project recognition in the ordered set where groupId = &#63;.
	 *
	 * @param teamMemberProjectRecognitionId the primary key of the current team member project recognition
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next team member project recognition
	 * @throws NoSuchTeamMemberProjectRecognitionException if a team member project recognition with the primary key could not be found
	 */
	@Override
	public TeamMemberProjectRecognition[] findByGroupId_PrevAndNext(
		long teamMemberProjectRecognitionId, long groupId,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator)
		throws NoSuchTeamMemberProjectRecognitionException {
		TeamMemberProjectRecognition teamMemberProjectRecognition = findByPrimaryKey(teamMemberProjectRecognitionId);

		Session session = null;

		try {
			session = openSession();

			TeamMemberProjectRecognition[] array = new TeamMemberProjectRecognitionImpl[3];

			array[0] = getByGroupId_PrevAndNext(session,
					teamMemberProjectRecognition, groupId, orderByComparator,
					true);

			array[1] = teamMemberProjectRecognition;

			array[2] = getByGroupId_PrevAndNext(session,
					teamMemberProjectRecognition, groupId, orderByComparator,
					false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected TeamMemberProjectRecognition getByGroupId_PrevAndNext(
		Session session,
		TeamMemberProjectRecognition teamMemberProjectRecognition,
		long groupId,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_TEAMMEMBERPROJECTRECOGNITION_WHERE);

		query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(TeamMemberProjectRecognitionModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(groupId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(teamMemberProjectRecognition);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<TeamMemberProjectRecognition> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the team member project recognitions where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	@Override
	public void removeByGroupId(long groupId) {
		for (TeamMemberProjectRecognition teamMemberProjectRecognition : findByGroupId(
				groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(teamMemberProjectRecognition);
		}
	}

	/**
	 * Returns the number of team member project recognitions where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching team member project recognitions
	 */
	@Override
	public int countByGroupId(long groupId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_GROUPID;

		Object[] finderArgs = new Object[] { groupId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_TEAMMEMBERPROJECTRECOGNITION_WHERE);

			query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_GROUPID_GROUPID_2 = "teamMemberProjectRecognition.groupId = ?";

	public TeamMemberProjectRecognitionPersistenceImpl() {
		setModelClass(TeamMemberProjectRecognition.class);
	}

	/**
	 * Caches the team member project recognition in the entity cache if it is enabled.
	 *
	 * @param teamMemberProjectRecognition the team member project recognition
	 */
	@Override
	public void cacheResult(
		TeamMemberProjectRecognition teamMemberProjectRecognition) {
		entityCache.putResult(TeamMemberProjectRecognitionModelImpl.ENTITY_CACHE_ENABLED,
			TeamMemberProjectRecognitionImpl.class,
			teamMemberProjectRecognition.getPrimaryKey(),
			teamMemberProjectRecognition);

		finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G,
			new Object[] {
				teamMemberProjectRecognition.getUuid(),
				teamMemberProjectRecognition.getGroupId()
			}, teamMemberProjectRecognition);

		finderCache.putResult(FINDER_PATH_FETCH_BY_TEAMMEMBERPROJECTRECOGNITIONID,
			new Object[] {
				teamMemberProjectRecognition.getTeamMemberProjectRecognitionId()
			}, teamMemberProjectRecognition);

		teamMemberProjectRecognition.resetOriginalValues();
	}

	/**
	 * Caches the team member project recognitions in the entity cache if it is enabled.
	 *
	 * @param teamMemberProjectRecognitions the team member project recognitions
	 */
	@Override
	public void cacheResult(
		List<TeamMemberProjectRecognition> teamMemberProjectRecognitions) {
		for (TeamMemberProjectRecognition teamMemberProjectRecognition : teamMemberProjectRecognitions) {
			if (entityCache.getResult(
						TeamMemberProjectRecognitionModelImpl.ENTITY_CACHE_ENABLED,
						TeamMemberProjectRecognitionImpl.class,
						teamMemberProjectRecognition.getPrimaryKey()) == null) {
				cacheResult(teamMemberProjectRecognition);
			}
			else {
				teamMemberProjectRecognition.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all team member project recognitions.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(TeamMemberProjectRecognitionImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the team member project recognition.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(
		TeamMemberProjectRecognition teamMemberProjectRecognition) {
		entityCache.removeResult(TeamMemberProjectRecognitionModelImpl.ENTITY_CACHE_ENABLED,
			TeamMemberProjectRecognitionImpl.class,
			teamMemberProjectRecognition.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache((TeamMemberProjectRecognitionModelImpl)teamMemberProjectRecognition);
	}

	@Override
	public void clearCache(
		List<TeamMemberProjectRecognition> teamMemberProjectRecognitions) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (TeamMemberProjectRecognition teamMemberProjectRecognition : teamMemberProjectRecognitions) {
			entityCache.removeResult(TeamMemberProjectRecognitionModelImpl.ENTITY_CACHE_ENABLED,
				TeamMemberProjectRecognitionImpl.class,
				teamMemberProjectRecognition.getPrimaryKey());

			clearUniqueFindersCache((TeamMemberProjectRecognitionModelImpl)teamMemberProjectRecognition);
		}
	}

	protected void cacheUniqueFindersCache(
		TeamMemberProjectRecognitionModelImpl teamMemberProjectRecognitionModelImpl,
		boolean isNew) {
		if (isNew) {
			Object[] args = new Object[] {
					teamMemberProjectRecognitionModelImpl.getUuid(),
					teamMemberProjectRecognitionModelImpl.getGroupId()
				};

			finderCache.putResult(FINDER_PATH_COUNT_BY_UUID_G, args,
				Long.valueOf(1));
			finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G, args,
				teamMemberProjectRecognitionModelImpl);

			args = new Object[] {
					teamMemberProjectRecognitionModelImpl.getTeamMemberProjectRecognitionId()
				};

			finderCache.putResult(FINDER_PATH_COUNT_BY_TEAMMEMBERPROJECTRECOGNITIONID,
				args, Long.valueOf(1));
			finderCache.putResult(FINDER_PATH_FETCH_BY_TEAMMEMBERPROJECTRECOGNITIONID,
				args, teamMemberProjectRecognitionModelImpl);
		}
		else {
			if ((teamMemberProjectRecognitionModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_UUID_G.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						teamMemberProjectRecognitionModelImpl.getUuid(),
						teamMemberProjectRecognitionModelImpl.getGroupId()
					};

				finderCache.putResult(FINDER_PATH_COUNT_BY_UUID_G, args,
					Long.valueOf(1));
				finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G, args,
					teamMemberProjectRecognitionModelImpl);
			}

			if ((teamMemberProjectRecognitionModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_TEAMMEMBERPROJECTRECOGNITIONID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						teamMemberProjectRecognitionModelImpl.getTeamMemberProjectRecognitionId()
					};

				finderCache.putResult(FINDER_PATH_COUNT_BY_TEAMMEMBERPROJECTRECOGNITIONID,
					args, Long.valueOf(1));
				finderCache.putResult(FINDER_PATH_FETCH_BY_TEAMMEMBERPROJECTRECOGNITIONID,
					args, teamMemberProjectRecognitionModelImpl);
			}
		}
	}

	protected void clearUniqueFindersCache(
		TeamMemberProjectRecognitionModelImpl teamMemberProjectRecognitionModelImpl) {
		Object[] args = new Object[] {
				teamMemberProjectRecognitionModelImpl.getUuid(),
				teamMemberProjectRecognitionModelImpl.getGroupId()
			};

		finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
		finderCache.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);

		if ((teamMemberProjectRecognitionModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_UUID_G.getColumnBitmask()) != 0) {
			args = new Object[] {
					teamMemberProjectRecognitionModelImpl.getOriginalUuid(),
					teamMemberProjectRecognitionModelImpl.getOriginalGroupId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);
		}

		args = new Object[] {
				teamMemberProjectRecognitionModelImpl.getTeamMemberProjectRecognitionId()
			};

		finderCache.removeResult(FINDER_PATH_COUNT_BY_TEAMMEMBERPROJECTRECOGNITIONID,
			args);
		finderCache.removeResult(FINDER_PATH_FETCH_BY_TEAMMEMBERPROJECTRECOGNITIONID,
			args);

		if ((teamMemberProjectRecognitionModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_TEAMMEMBERPROJECTRECOGNITIONID.getColumnBitmask()) != 0) {
			args = new Object[] {
					teamMemberProjectRecognitionModelImpl.getOriginalTeamMemberProjectRecognitionId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_TEAMMEMBERPROJECTRECOGNITIONID,
				args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_TEAMMEMBERPROJECTRECOGNITIONID,
				args);
		}
	}

	/**
	 * Creates a new team member project recognition with the primary key. Does not add the team member project recognition to the database.
	 *
	 * @param teamMemberProjectRecognitionId the primary key for the new team member project recognition
	 * @return the new team member project recognition
	 */
	@Override
	public TeamMemberProjectRecognition create(
		long teamMemberProjectRecognitionId) {
		TeamMemberProjectRecognition teamMemberProjectRecognition = new TeamMemberProjectRecognitionImpl();

		teamMemberProjectRecognition.setNew(true);
		teamMemberProjectRecognition.setPrimaryKey(teamMemberProjectRecognitionId);

		String uuid = PortalUUIDUtil.generate();

		teamMemberProjectRecognition.setUuid(uuid);

		teamMemberProjectRecognition.setCompanyId(companyProvider.getCompanyId());

		return teamMemberProjectRecognition;
	}

	/**
	 * Removes the team member project recognition with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param teamMemberProjectRecognitionId the primary key of the team member project recognition
	 * @return the team member project recognition that was removed
	 * @throws NoSuchTeamMemberProjectRecognitionException if a team member project recognition with the primary key could not be found
	 */
	@Override
	public TeamMemberProjectRecognition remove(
		long teamMemberProjectRecognitionId)
		throws NoSuchTeamMemberProjectRecognitionException {
		return remove((Serializable)teamMemberProjectRecognitionId);
	}

	/**
	 * Removes the team member project recognition with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the team member project recognition
	 * @return the team member project recognition that was removed
	 * @throws NoSuchTeamMemberProjectRecognitionException if a team member project recognition with the primary key could not be found
	 */
	@Override
	public TeamMemberProjectRecognition remove(Serializable primaryKey)
		throws NoSuchTeamMemberProjectRecognitionException {
		Session session = null;

		try {
			session = openSession();

			TeamMemberProjectRecognition teamMemberProjectRecognition = (TeamMemberProjectRecognition)session.get(TeamMemberProjectRecognitionImpl.class,
					primaryKey);

			if (teamMemberProjectRecognition == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchTeamMemberProjectRecognitionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(teamMemberProjectRecognition);
		}
		catch (NoSuchTeamMemberProjectRecognitionException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected TeamMemberProjectRecognition removeImpl(
		TeamMemberProjectRecognition teamMemberProjectRecognition) {
		teamMemberProjectRecognition = toUnwrappedModel(teamMemberProjectRecognition);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(teamMemberProjectRecognition)) {
				teamMemberProjectRecognition = (TeamMemberProjectRecognition)session.get(TeamMemberProjectRecognitionImpl.class,
						teamMemberProjectRecognition.getPrimaryKeyObj());
			}

			if (teamMemberProjectRecognition != null) {
				session.delete(teamMemberProjectRecognition);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (teamMemberProjectRecognition != null) {
			clearCache(teamMemberProjectRecognition);
		}

		return teamMemberProjectRecognition;
	}

	@Override
	public TeamMemberProjectRecognition updateImpl(
		TeamMemberProjectRecognition teamMemberProjectRecognition) {
		teamMemberProjectRecognition = toUnwrappedModel(teamMemberProjectRecognition);

		boolean isNew = teamMemberProjectRecognition.isNew();

		TeamMemberProjectRecognitionModelImpl teamMemberProjectRecognitionModelImpl =
			(TeamMemberProjectRecognitionModelImpl)teamMemberProjectRecognition;

		if (Validator.isNull(teamMemberProjectRecognition.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			teamMemberProjectRecognition.setUuid(uuid);
		}

		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();

		Date now = new Date();

		if (isNew && (teamMemberProjectRecognition.getCreateDate() == null)) {
			if (serviceContext == null) {
				teamMemberProjectRecognition.setCreateDate(now);
			}
			else {
				teamMemberProjectRecognition.setCreateDate(serviceContext.getCreateDate(
						now));
			}
		}

		if (!teamMemberProjectRecognitionModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				teamMemberProjectRecognition.setModifiedDate(now);
			}
			else {
				teamMemberProjectRecognition.setModifiedDate(serviceContext.getModifiedDate(
						now));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (teamMemberProjectRecognition.isNew()) {
				session.save(teamMemberProjectRecognition);

				teamMemberProjectRecognition.setNew(false);
			}
			else {
				teamMemberProjectRecognition = (TeamMemberProjectRecognition)session.merge(teamMemberProjectRecognition);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew ||
				!TeamMemberProjectRecognitionModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((teamMemberProjectRecognitionModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						teamMemberProjectRecognitionModelImpl.getOriginalUuid()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);

				args = new Object[] {
						teamMemberProjectRecognitionModelImpl.getUuid()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);
			}

			if ((teamMemberProjectRecognitionModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						teamMemberProjectRecognitionModelImpl.getOriginalUuid(),
						teamMemberProjectRecognitionModelImpl.getOriginalCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);

				args = new Object[] {
						teamMemberProjectRecognitionModelImpl.getUuid(),
						teamMemberProjectRecognitionModelImpl.getCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);
			}

			if ((teamMemberProjectRecognitionModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						teamMemberProjectRecognitionModelImpl.getOriginalGroupId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_GROUPID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID,
					args);

				args = new Object[] {
						teamMemberProjectRecognitionModelImpl.getGroupId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_GROUPID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID,
					args);
			}
		}

		entityCache.putResult(TeamMemberProjectRecognitionModelImpl.ENTITY_CACHE_ENABLED,
			TeamMemberProjectRecognitionImpl.class,
			teamMemberProjectRecognition.getPrimaryKey(),
			teamMemberProjectRecognition, false);

		clearUniqueFindersCache(teamMemberProjectRecognitionModelImpl);
		cacheUniqueFindersCache(teamMemberProjectRecognitionModelImpl, isNew);

		teamMemberProjectRecognition.resetOriginalValues();

		return teamMemberProjectRecognition;
	}

	protected TeamMemberProjectRecognition toUnwrappedModel(
		TeamMemberProjectRecognition teamMemberProjectRecognition) {
		if (teamMemberProjectRecognition instanceof TeamMemberProjectRecognitionImpl) {
			return teamMemberProjectRecognition;
		}

		TeamMemberProjectRecognitionImpl teamMemberProjectRecognitionImpl = new TeamMemberProjectRecognitionImpl();

		teamMemberProjectRecognitionImpl.setNew(teamMemberProjectRecognition.isNew());
		teamMemberProjectRecognitionImpl.setPrimaryKey(teamMemberProjectRecognition.getPrimaryKey());

		teamMemberProjectRecognitionImpl.setUuid(teamMemberProjectRecognition.getUuid());
		teamMemberProjectRecognitionImpl.setTeamMemberProjectRecognitionId(teamMemberProjectRecognition.getTeamMemberProjectRecognitionId());
		teamMemberProjectRecognitionImpl.setGroupId(teamMemberProjectRecognition.getGroupId());
		teamMemberProjectRecognitionImpl.setCompanyId(teamMemberProjectRecognition.getCompanyId());
		teamMemberProjectRecognitionImpl.setUserId(teamMemberProjectRecognition.getUserId());
		teamMemberProjectRecognitionImpl.setUserName(teamMemberProjectRecognition.getUserName());
		teamMemberProjectRecognitionImpl.setCreateDate(teamMemberProjectRecognition.getCreateDate());
		teamMemberProjectRecognitionImpl.setModifiedDate(teamMemberProjectRecognition.getModifiedDate());
		teamMemberProjectRecognitionImpl.setDateOfRecognition(teamMemberProjectRecognition.getDateOfRecognition());
		teamMemberProjectRecognitionImpl.setName(teamMemberProjectRecognition.getName());
		teamMemberProjectRecognitionImpl.setCatRating(teamMemberProjectRecognition.getCatRating());
		teamMemberProjectRecognitionImpl.setURL(teamMemberProjectRecognition.getURL());
		teamMemberProjectRecognitionImpl.setOptionalComments(teamMemberProjectRecognition.getOptionalComments());

		return teamMemberProjectRecognitionImpl;
	}

	/**
	 * Returns the team member project recognition with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the team member project recognition
	 * @return the team member project recognition
	 * @throws NoSuchTeamMemberProjectRecognitionException if a team member project recognition with the primary key could not be found
	 */
	@Override
	public TeamMemberProjectRecognition findByPrimaryKey(
		Serializable primaryKey)
		throws NoSuchTeamMemberProjectRecognitionException {
		TeamMemberProjectRecognition teamMemberProjectRecognition = fetchByPrimaryKey(primaryKey);

		if (teamMemberProjectRecognition == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchTeamMemberProjectRecognitionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return teamMemberProjectRecognition;
	}

	/**
	 * Returns the team member project recognition with the primary key or throws a {@link NoSuchTeamMemberProjectRecognitionException} if it could not be found.
	 *
	 * @param teamMemberProjectRecognitionId the primary key of the team member project recognition
	 * @return the team member project recognition
	 * @throws NoSuchTeamMemberProjectRecognitionException if a team member project recognition with the primary key could not be found
	 */
	@Override
	public TeamMemberProjectRecognition findByPrimaryKey(
		long teamMemberProjectRecognitionId)
		throws NoSuchTeamMemberProjectRecognitionException {
		return findByPrimaryKey((Serializable)teamMemberProjectRecognitionId);
	}

	/**
	 * Returns the team member project recognition with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the team member project recognition
	 * @return the team member project recognition, or <code>null</code> if a team member project recognition with the primary key could not be found
	 */
	@Override
	public TeamMemberProjectRecognition fetchByPrimaryKey(
		Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(TeamMemberProjectRecognitionModelImpl.ENTITY_CACHE_ENABLED,
				TeamMemberProjectRecognitionImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		TeamMemberProjectRecognition teamMemberProjectRecognition = (TeamMemberProjectRecognition)serializable;

		if (teamMemberProjectRecognition == null) {
			Session session = null;

			try {
				session = openSession();

				teamMemberProjectRecognition = (TeamMemberProjectRecognition)session.get(TeamMemberProjectRecognitionImpl.class,
						primaryKey);

				if (teamMemberProjectRecognition != null) {
					cacheResult(teamMemberProjectRecognition);
				}
				else {
					entityCache.putResult(TeamMemberProjectRecognitionModelImpl.ENTITY_CACHE_ENABLED,
						TeamMemberProjectRecognitionImpl.class, primaryKey,
						nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(TeamMemberProjectRecognitionModelImpl.ENTITY_CACHE_ENABLED,
					TeamMemberProjectRecognitionImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return teamMemberProjectRecognition;
	}

	/**
	 * Returns the team member project recognition with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param teamMemberProjectRecognitionId the primary key of the team member project recognition
	 * @return the team member project recognition, or <code>null</code> if a team member project recognition with the primary key could not be found
	 */
	@Override
	public TeamMemberProjectRecognition fetchByPrimaryKey(
		long teamMemberProjectRecognitionId) {
		return fetchByPrimaryKey((Serializable)teamMemberProjectRecognitionId);
	}

	@Override
	public Map<Serializable, TeamMemberProjectRecognition> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, TeamMemberProjectRecognition> map = new HashMap<Serializable, TeamMemberProjectRecognition>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			TeamMemberProjectRecognition teamMemberProjectRecognition = fetchByPrimaryKey(primaryKey);

			if (teamMemberProjectRecognition != null) {
				map.put(primaryKey, teamMemberProjectRecognition);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(TeamMemberProjectRecognitionModelImpl.ENTITY_CACHE_ENABLED,
					TeamMemberProjectRecognitionImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey,
						(TeamMemberProjectRecognition)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_TEAMMEMBERPROJECTRECOGNITION_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(String.valueOf(primaryKey));

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (TeamMemberProjectRecognition teamMemberProjectRecognition : (List<TeamMemberProjectRecognition>)q.list()) {
				map.put(teamMemberProjectRecognition.getPrimaryKeyObj(),
					teamMemberProjectRecognition);

				cacheResult(teamMemberProjectRecognition);

				uncachedPrimaryKeys.remove(teamMemberProjectRecognition.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(TeamMemberProjectRecognitionModelImpl.ENTITY_CACHE_ENABLED,
					TeamMemberProjectRecognitionImpl.class, primaryKey,
					nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the team member project recognitions.
	 *
	 * @return the team member project recognitions
	 */
	@Override
	public List<TeamMemberProjectRecognition> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the team member project recognitions.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of team member project recognitions
	 * @param end the upper bound of the range of team member project recognitions (not inclusive)
	 * @return the range of team member project recognitions
	 */
	@Override
	public List<TeamMemberProjectRecognition> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the team member project recognitions.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of team member project recognitions
	 * @param end the upper bound of the range of team member project recognitions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of team member project recognitions
	 */
	@Override
	public List<TeamMemberProjectRecognition> findAll(int start, int end,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the team member project recognitions.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of team member project recognitions
	 * @param end the upper bound of the range of team member project recognitions (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of team member project recognitions
	 */
	@Override
	public List<TeamMemberProjectRecognition> findAll(int start, int end,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<TeamMemberProjectRecognition> list = null;

		if (retrieveFromCache) {
			list = (List<TeamMemberProjectRecognition>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_TEAMMEMBERPROJECTRECOGNITION);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_TEAMMEMBERPROJECTRECOGNITION;

				if (pagination) {
					sql = sql.concat(TeamMemberProjectRecognitionModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<TeamMemberProjectRecognition>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<TeamMemberProjectRecognition>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the team member project recognitions from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (TeamMemberProjectRecognition teamMemberProjectRecognition : findAll()) {
			remove(teamMemberProjectRecognition);
		}
	}

	/**
	 * Returns the number of team member project recognitions.
	 *
	 * @return the number of team member project recognitions
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_TEAMMEMBERPROJECTRECOGNITION);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return TeamMemberProjectRecognitionModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the team member project recognition persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(TeamMemberProjectRecognitionImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = CompanyProviderWrapper.class)
	protected CompanyProvider companyProvider;
	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_TEAMMEMBERPROJECTRECOGNITION = "SELECT teamMemberProjectRecognition FROM TeamMemberProjectRecognition teamMemberProjectRecognition";
	private static final String _SQL_SELECT_TEAMMEMBERPROJECTRECOGNITION_WHERE_PKS_IN =
		"SELECT teamMemberProjectRecognition FROM TeamMemberProjectRecognition teamMemberProjectRecognition WHERE teamMemberProjectRecognitionId IN (";
	private static final String _SQL_SELECT_TEAMMEMBERPROJECTRECOGNITION_WHERE = "SELECT teamMemberProjectRecognition FROM TeamMemberProjectRecognition teamMemberProjectRecognition WHERE ";
	private static final String _SQL_COUNT_TEAMMEMBERPROJECTRECOGNITION = "SELECT COUNT(teamMemberProjectRecognition) FROM TeamMemberProjectRecognition teamMemberProjectRecognition";
	private static final String _SQL_COUNT_TEAMMEMBERPROJECTRECOGNITION_WHERE = "SELECT COUNT(teamMemberProjectRecognition) FROM TeamMemberProjectRecognition teamMemberProjectRecognition WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "teamMemberProjectRecognition.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No TeamMemberProjectRecognition exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No TeamMemberProjectRecognition exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(TeamMemberProjectRecognitionPersistenceImpl.class);
	private static final Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"uuid"
			});
}