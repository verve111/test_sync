/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.impact.model.Jobs;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Jobs in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Jobs
 * @generated
 */
@ProviderType
public class JobsCacheModel implements CacheModel<Jobs>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof JobsCacheModel)) {
			return false;
		}

		JobsCacheModel jobsCacheModel = (JobsCacheModel)obj;

		if (jobsId == jobsCacheModel.jobsId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, jobsId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", jobsId=");
		sb.append(jobsId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", asOfDate=");
		sb.append(asOfDate);
		sb.append(", sizeOfTeam=");
		sb.append(sizeOfTeam);
		sb.append(", comments=");
		sb.append(comments);
		sb.append(", primaryJob=");
		sb.append(primaryJob);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Jobs toEntityModel() {
		JobsImpl jobsImpl = new JobsImpl();

		if (uuid == null) {
			jobsImpl.setUuid(StringPool.BLANK);
		}
		else {
			jobsImpl.setUuid(uuid);
		}

		jobsImpl.setJobsId(jobsId);
		jobsImpl.setGroupId(groupId);
		jobsImpl.setCompanyId(companyId);
		jobsImpl.setUserId(userId);

		if (userName == null) {
			jobsImpl.setUserName(StringPool.BLANK);
		}
		else {
			jobsImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			jobsImpl.setCreateDate(null);
		}
		else {
			jobsImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			jobsImpl.setModifiedDate(null);
		}
		else {
			jobsImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (asOfDate == Long.MIN_VALUE) {
			jobsImpl.setAsOfDate(null);
		}
		else {
			jobsImpl.setAsOfDate(new Date(asOfDate));
		}

		jobsImpl.setSizeOfTeam(sizeOfTeam);

		if (comments == null) {
			jobsImpl.setComments(StringPool.BLANK);
		}
		else {
			jobsImpl.setComments(comments);
		}

		if (primaryJob == null) {
			jobsImpl.setPrimaryJob(StringPool.BLANK);
		}
		else {
			jobsImpl.setPrimaryJob(primaryJob);
		}

		jobsImpl.resetOriginalValues();

		return jobsImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		jobsId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		asOfDate = objectInput.readLong();

		sizeOfTeam = objectInput.readInt();
		comments = objectInput.readUTF();
		primaryJob = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(jobsId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);
		objectOutput.writeLong(asOfDate);

		objectOutput.writeInt(sizeOfTeam);

		if (comments == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(comments);
		}

		if (primaryJob == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(primaryJob);
		}
	}

	public String uuid;
	public long jobsId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long asOfDate;
	public int sizeOfTeam;
	public String comments;
	public String primaryJob;
}