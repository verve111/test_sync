/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.impact.exception.NoSuchPatientsImpactedException;
import com.collaboratelab.impact.model.PatientsImpacted;
import com.collaboratelab.impact.model.impl.PatientsImpactedImpl;
import com.collaboratelab.impact.model.impl.PatientsImpactedModelImpl;
import com.collaboratelab.impact.service.persistence.PatientsImpactedPersistence;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.CompanyProvider;
import com.liferay.portal.kernel.service.persistence.CompanyProviderWrapper;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the patients impacted service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see PatientsImpactedPersistence
 * @see com.collaboratelab.impact.service.persistence.PatientsImpactedUtil
 * @generated
 */
@ProviderType
public class PatientsImpactedPersistenceImpl extends BasePersistenceImpl<PatientsImpacted>
	implements PatientsImpactedPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link PatientsImpactedUtil} to access the patients impacted persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = PatientsImpactedImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(PatientsImpactedModelImpl.ENTITY_CACHE_ENABLED,
			PatientsImpactedModelImpl.FINDER_CACHE_ENABLED,
			PatientsImpactedImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(PatientsImpactedModelImpl.ENTITY_CACHE_ENABLED,
			PatientsImpactedModelImpl.FINDER_CACHE_ENABLED,
			PatientsImpactedImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(PatientsImpactedModelImpl.ENTITY_CACHE_ENABLED,
			PatientsImpactedModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID = new FinderPath(PatientsImpactedModelImpl.ENTITY_CACHE_ENABLED,
			PatientsImpactedModelImpl.FINDER_CACHE_ENABLED,
			PatientsImpactedImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByUuid",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID = new FinderPath(PatientsImpactedModelImpl.ENTITY_CACHE_ENABLED,
			PatientsImpactedModelImpl.FINDER_CACHE_ENABLED,
			PatientsImpactedImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] { String.class.getName() },
			PatientsImpactedModelImpl.UUID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID = new FinderPath(PatientsImpactedModelImpl.ENTITY_CACHE_ENABLED,
			PatientsImpactedModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] { String.class.getName() });

	/**
	 * Returns all the patients impacteds where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching patients impacteds
	 */
	@Override
	public List<PatientsImpacted> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the patients impacteds where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of patients impacteds
	 * @param end the upper bound of the range of patients impacteds (not inclusive)
	 * @return the range of matching patients impacteds
	 */
	@Override
	public List<PatientsImpacted> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the patients impacteds where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of patients impacteds
	 * @param end the upper bound of the range of patients impacteds (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching patients impacteds
	 */
	@Override
	public List<PatientsImpacted> findByUuid(String uuid, int start, int end,
		OrderByComparator<PatientsImpacted> orderByComparator) {
		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the patients impacteds where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of patients impacteds
	 * @param end the upper bound of the range of patients impacteds (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching patients impacteds
	 */
	@Override
	public List<PatientsImpacted> findByUuid(String uuid, int start, int end,
		OrderByComparator<PatientsImpacted> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid, start, end, orderByComparator };
		}

		List<PatientsImpacted> list = null;

		if (retrieveFromCache) {
			list = (List<PatientsImpacted>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (PatientsImpacted patientsImpacted : list) {
					if (!Objects.equals(uuid, patientsImpacted.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PATIENTSIMPACTED_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(PatientsImpactedModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<PatientsImpacted>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<PatientsImpacted>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first patients impacted in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching patients impacted
	 * @throws NoSuchPatientsImpactedException if a matching patients impacted could not be found
	 */
	@Override
	public PatientsImpacted findByUuid_First(String uuid,
		OrderByComparator<PatientsImpacted> orderByComparator)
		throws NoSuchPatientsImpactedException {
		PatientsImpacted patientsImpacted = fetchByUuid_First(uuid,
				orderByComparator);

		if (patientsImpacted != null) {
			return patientsImpacted;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPatientsImpactedException(msg.toString());
	}

	/**
	 * Returns the first patients impacted in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching patients impacted, or <code>null</code> if a matching patients impacted could not be found
	 */
	@Override
	public PatientsImpacted fetchByUuid_First(String uuid,
		OrderByComparator<PatientsImpacted> orderByComparator) {
		List<PatientsImpacted> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last patients impacted in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching patients impacted
	 * @throws NoSuchPatientsImpactedException if a matching patients impacted could not be found
	 */
	@Override
	public PatientsImpacted findByUuid_Last(String uuid,
		OrderByComparator<PatientsImpacted> orderByComparator)
		throws NoSuchPatientsImpactedException {
		PatientsImpacted patientsImpacted = fetchByUuid_Last(uuid,
				orderByComparator);

		if (patientsImpacted != null) {
			return patientsImpacted;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPatientsImpactedException(msg.toString());
	}

	/**
	 * Returns the last patients impacted in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching patients impacted, or <code>null</code> if a matching patients impacted could not be found
	 */
	@Override
	public PatientsImpacted fetchByUuid_Last(String uuid,
		OrderByComparator<PatientsImpacted> orderByComparator) {
		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<PatientsImpacted> list = findByUuid(uuid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the patients impacteds before and after the current patients impacted in the ordered set where uuid = &#63;.
	 *
	 * @param patientImpactedId the primary key of the current patients impacted
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next patients impacted
	 * @throws NoSuchPatientsImpactedException if a patients impacted with the primary key could not be found
	 */
	@Override
	public PatientsImpacted[] findByUuid_PrevAndNext(long patientImpactedId,
		String uuid, OrderByComparator<PatientsImpacted> orderByComparator)
		throws NoSuchPatientsImpactedException {
		PatientsImpacted patientsImpacted = findByPrimaryKey(patientImpactedId);

		Session session = null;

		try {
			session = openSession();

			PatientsImpacted[] array = new PatientsImpactedImpl[3];

			array[0] = getByUuid_PrevAndNext(session, patientsImpacted, uuid,
					orderByComparator, true);

			array[1] = patientsImpacted;

			array[2] = getByUuid_PrevAndNext(session, patientsImpacted, uuid,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected PatientsImpacted getByUuid_PrevAndNext(Session session,
		PatientsImpacted patientsImpacted, String uuid,
		OrderByComparator<PatientsImpacted> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PATIENTSIMPACTED_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(PatientsImpactedModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(patientsImpacted);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<PatientsImpacted> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the patients impacteds where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (PatientsImpacted patientsImpacted : findByUuid(uuid,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(patientsImpacted);
		}
	}

	/**
	 * Returns the number of patients impacteds where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching patients impacteds
	 */
	@Override
	public int countByUuid(String uuid) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID;

		Object[] finderArgs = new Object[] { uuid };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PATIENTSIMPACTED_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_1 = "patientsImpacted.uuid IS NULL";
	private static final String _FINDER_COLUMN_UUID_UUID_2 = "patientsImpacted.uuid = ?";
	private static final String _FINDER_COLUMN_UUID_UUID_3 = "(patientsImpacted.uuid IS NULL OR patientsImpacted.uuid = '')";
	public static final FinderPath FINDER_PATH_FETCH_BY_UUID_G = new FinderPath(PatientsImpactedModelImpl.ENTITY_CACHE_ENABLED,
			PatientsImpactedModelImpl.FINDER_CACHE_ENABLED,
			PatientsImpactedImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByUUID_G",
			new String[] { String.class.getName(), Long.class.getName() },
			PatientsImpactedModelImpl.UUID_COLUMN_BITMASK |
			PatientsImpactedModelImpl.GROUPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_G = new FinderPath(PatientsImpactedModelImpl.ENTITY_CACHE_ENABLED,
			PatientsImpactedModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns the patients impacted where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchPatientsImpactedException} if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching patients impacted
	 * @throws NoSuchPatientsImpactedException if a matching patients impacted could not be found
	 */
	@Override
	public PatientsImpacted findByUUID_G(String uuid, long groupId)
		throws NoSuchPatientsImpactedException {
		PatientsImpacted patientsImpacted = fetchByUUID_G(uuid, groupId);

		if (patientsImpacted == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("uuid=");
			msg.append(uuid);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchPatientsImpactedException(msg.toString());
		}

		return patientsImpacted;
	}

	/**
	 * Returns the patients impacted where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching patients impacted, or <code>null</code> if a matching patients impacted could not be found
	 */
	@Override
	public PatientsImpacted fetchByUUID_G(String uuid, long groupId) {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the patients impacted where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching patients impacted, or <code>null</code> if a matching patients impacted could not be found
	 */
	@Override
	public PatientsImpacted fetchByUUID_G(String uuid, long groupId,
		boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { uuid, groupId };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_UUID_G,
					finderArgs, this);
		}

		if (result instanceof PatientsImpacted) {
			PatientsImpacted patientsImpacted = (PatientsImpacted)result;

			if (!Objects.equals(uuid, patientsImpacted.getUuid()) ||
					(groupId != patientsImpacted.getGroupId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_PATIENTSIMPACTED_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				List<PatientsImpacted> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G,
						finderArgs, list);
				}
				else {
					PatientsImpacted patientsImpacted = list.get(0);

					result = patientsImpacted;

					cacheResult(patientsImpacted);

					if ((patientsImpacted.getUuid() == null) ||
							!patientsImpacted.getUuid().equals(uuid) ||
							(patientsImpacted.getGroupId() != groupId)) {
						finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G,
							finderArgs, patientsImpacted);
					}
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_UUID_G, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (PatientsImpacted)result;
		}
	}

	/**
	 * Removes the patients impacted where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the patients impacted that was removed
	 */
	@Override
	public PatientsImpacted removeByUUID_G(String uuid, long groupId)
		throws NoSuchPatientsImpactedException {
		PatientsImpacted patientsImpacted = findByUUID_G(uuid, groupId);

		return remove(patientsImpacted);
	}

	/**
	 * Returns the number of patients impacteds where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching patients impacteds
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_G;

		Object[] finderArgs = new Object[] { uuid, groupId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_PATIENTSIMPACTED_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_1 = "patientsImpacted.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_2 = "patientsImpacted.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_3 = "(patientsImpacted.uuid IS NULL OR patientsImpacted.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 = "patientsImpacted.groupId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C = new FinderPath(PatientsImpactedModelImpl.ENTITY_CACHE_ENABLED,
			PatientsImpactedModelImpl.FINDER_CACHE_ENABLED,
			PatientsImpactedImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C =
		new FinderPath(PatientsImpactedModelImpl.ENTITY_CACHE_ENABLED,
			PatientsImpactedModelImpl.FINDER_CACHE_ENABLED,
			PatientsImpactedImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() },
			PatientsImpactedModelImpl.UUID_COLUMN_BITMASK |
			PatientsImpactedModelImpl.COMPANYID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_C = new FinderPath(PatientsImpactedModelImpl.ENTITY_CACHE_ENABLED,
			PatientsImpactedModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns all the patients impacteds where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching patients impacteds
	 */
	@Override
	public List<PatientsImpacted> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(uuid, companyId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the patients impacteds where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of patients impacteds
	 * @param end the upper bound of the range of patients impacteds (not inclusive)
	 * @return the range of matching patients impacteds
	 */
	@Override
	public List<PatientsImpacted> findByUuid_C(String uuid, long companyId,
		int start, int end) {
		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the patients impacteds where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of patients impacteds
	 * @param end the upper bound of the range of patients impacteds (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching patients impacteds
	 */
	@Override
	public List<PatientsImpacted> findByUuid_C(String uuid, long companyId,
		int start, int end,
		OrderByComparator<PatientsImpacted> orderByComparator) {
		return findByUuid_C(uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the patients impacteds where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of patients impacteds
	 * @param end the upper bound of the range of patients impacteds (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching patients impacteds
	 */
	@Override
	public List<PatientsImpacted> findByUuid_C(String uuid, long companyId,
		int start, int end,
		OrderByComparator<PatientsImpacted> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] { uuid, companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] {
					uuid, companyId,
					
					start, end, orderByComparator
				};
		}

		List<PatientsImpacted> list = null;

		if (retrieveFromCache) {
			list = (List<PatientsImpacted>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (PatientsImpacted patientsImpacted : list) {
					if (!Objects.equals(uuid, patientsImpacted.getUuid()) ||
							(companyId != patientsImpacted.getCompanyId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_PATIENTSIMPACTED_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(PatientsImpactedModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				if (!pagination) {
					list = (List<PatientsImpacted>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<PatientsImpacted>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first patients impacted in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching patients impacted
	 * @throws NoSuchPatientsImpactedException if a matching patients impacted could not be found
	 */
	@Override
	public PatientsImpacted findByUuid_C_First(String uuid, long companyId,
		OrderByComparator<PatientsImpacted> orderByComparator)
		throws NoSuchPatientsImpactedException {
		PatientsImpacted patientsImpacted = fetchByUuid_C_First(uuid,
				companyId, orderByComparator);

		if (patientsImpacted != null) {
			return patientsImpacted;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPatientsImpactedException(msg.toString());
	}

	/**
	 * Returns the first patients impacted in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching patients impacted, or <code>null</code> if a matching patients impacted could not be found
	 */
	@Override
	public PatientsImpacted fetchByUuid_C_First(String uuid, long companyId,
		OrderByComparator<PatientsImpacted> orderByComparator) {
		List<PatientsImpacted> list = findByUuid_C(uuid, companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last patients impacted in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching patients impacted
	 * @throws NoSuchPatientsImpactedException if a matching patients impacted could not be found
	 */
	@Override
	public PatientsImpacted findByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<PatientsImpacted> orderByComparator)
		throws NoSuchPatientsImpactedException {
		PatientsImpacted patientsImpacted = fetchByUuid_C_Last(uuid, companyId,
				orderByComparator);

		if (patientsImpacted != null) {
			return patientsImpacted;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPatientsImpactedException(msg.toString());
	}

	/**
	 * Returns the last patients impacted in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching patients impacted, or <code>null</code> if a matching patients impacted could not be found
	 */
	@Override
	public PatientsImpacted fetchByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<PatientsImpacted> orderByComparator) {
		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<PatientsImpacted> list = findByUuid_C(uuid, companyId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the patients impacteds before and after the current patients impacted in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param patientImpactedId the primary key of the current patients impacted
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next patients impacted
	 * @throws NoSuchPatientsImpactedException if a patients impacted with the primary key could not be found
	 */
	@Override
	public PatientsImpacted[] findByUuid_C_PrevAndNext(long patientImpactedId,
		String uuid, long companyId,
		OrderByComparator<PatientsImpacted> orderByComparator)
		throws NoSuchPatientsImpactedException {
		PatientsImpacted patientsImpacted = findByPrimaryKey(patientImpactedId);

		Session session = null;

		try {
			session = openSession();

			PatientsImpacted[] array = new PatientsImpactedImpl[3];

			array[0] = getByUuid_C_PrevAndNext(session, patientsImpacted, uuid,
					companyId, orderByComparator, true);

			array[1] = patientsImpacted;

			array[2] = getByUuid_C_PrevAndNext(session, patientsImpacted, uuid,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected PatientsImpacted getByUuid_C_PrevAndNext(Session session,
		PatientsImpacted patientsImpacted, String uuid, long companyId,
		OrderByComparator<PatientsImpacted> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(5 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_PATIENTSIMPACTED_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(PatientsImpactedModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(patientsImpacted);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<PatientsImpacted> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the patients impacteds where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (PatientsImpacted patientsImpacted : findByUuid_C(uuid, companyId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(patientsImpacted);
		}
	}

	/**
	 * Returns the number of patients impacteds where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching patients impacteds
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_C;

		Object[] finderArgs = new Object[] { uuid, companyId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_PATIENTSIMPACTED_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_1 = "patientsImpacted.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_2 = "patientsImpacted.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_3 = "(patientsImpacted.uuid IS NULL OR patientsImpacted.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 = "patientsImpacted.companyId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_PATIENTSIMPACTEDID = new FinderPath(PatientsImpactedModelImpl.ENTITY_CACHE_ENABLED,
			PatientsImpactedModelImpl.FINDER_CACHE_ENABLED,
			PatientsImpactedImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByPatientsImpactedId", new String[] { Long.class.getName() },
			PatientsImpactedModelImpl.PATIENTIMPACTEDID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_PATIENTSIMPACTEDID = new FinderPath(PatientsImpactedModelImpl.ENTITY_CACHE_ENABLED,
			PatientsImpactedModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByPatientsImpactedId", new String[] { Long.class.getName() });

	/**
	 * Returns the patients impacted where patientImpactedId = &#63; or throws a {@link NoSuchPatientsImpactedException} if it could not be found.
	 *
	 * @param patientImpactedId the patient impacted ID
	 * @return the matching patients impacted
	 * @throws NoSuchPatientsImpactedException if a matching patients impacted could not be found
	 */
	@Override
	public PatientsImpacted findByPatientsImpactedId(long patientImpactedId)
		throws NoSuchPatientsImpactedException {
		PatientsImpacted patientsImpacted = fetchByPatientsImpactedId(patientImpactedId);

		if (patientsImpacted == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("patientImpactedId=");
			msg.append(patientImpactedId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchPatientsImpactedException(msg.toString());
		}

		return patientsImpacted;
	}

	/**
	 * Returns the patients impacted where patientImpactedId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param patientImpactedId the patient impacted ID
	 * @return the matching patients impacted, or <code>null</code> if a matching patients impacted could not be found
	 */
	@Override
	public PatientsImpacted fetchByPatientsImpactedId(long patientImpactedId) {
		return fetchByPatientsImpactedId(patientImpactedId, true);
	}

	/**
	 * Returns the patients impacted where patientImpactedId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param patientImpactedId the patient impacted ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching patients impacted, or <code>null</code> if a matching patients impacted could not be found
	 */
	@Override
	public PatientsImpacted fetchByPatientsImpactedId(long patientImpactedId,
		boolean retrieveFromCache) {
		Object[] finderArgs = new Object[] { patientImpactedId };

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(FINDER_PATH_FETCH_BY_PATIENTSIMPACTEDID,
					finderArgs, this);
		}

		if (result instanceof PatientsImpacted) {
			PatientsImpacted patientsImpacted = (PatientsImpacted)result;

			if ((patientImpactedId != patientsImpacted.getPatientImpactedId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_PATIENTSIMPACTED_WHERE);

			query.append(_FINDER_COLUMN_PATIENTSIMPACTEDID_PATIENTIMPACTEDID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(patientImpactedId);

				List<PatientsImpacted> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(FINDER_PATH_FETCH_BY_PATIENTSIMPACTEDID,
						finderArgs, list);
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							_log.warn(
								"PatientsImpactedPersistenceImpl.fetchByPatientsImpactedId(long, boolean) with parameters (" +
								StringUtil.merge(finderArgs) +
								") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					PatientsImpacted patientsImpacted = list.get(0);

					result = patientsImpacted;

					cacheResult(patientsImpacted);

					if ((patientsImpacted.getPatientImpactedId() != patientImpactedId)) {
						finderCache.putResult(FINDER_PATH_FETCH_BY_PATIENTSIMPACTEDID,
							finderArgs, patientsImpacted);
					}
				}
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_FETCH_BY_PATIENTSIMPACTEDID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (PatientsImpacted)result;
		}
	}

	/**
	 * Removes the patients impacted where patientImpactedId = &#63; from the database.
	 *
	 * @param patientImpactedId the patient impacted ID
	 * @return the patients impacted that was removed
	 */
	@Override
	public PatientsImpacted removeByPatientsImpactedId(long patientImpactedId)
		throws NoSuchPatientsImpactedException {
		PatientsImpacted patientsImpacted = findByPatientsImpactedId(patientImpactedId);

		return remove(patientsImpacted);
	}

	/**
	 * Returns the number of patients impacteds where patientImpactedId = &#63;.
	 *
	 * @param patientImpactedId the patient impacted ID
	 * @return the number of matching patients impacteds
	 */
	@Override
	public int countByPatientsImpactedId(long patientImpactedId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_PATIENTSIMPACTEDID;

		Object[] finderArgs = new Object[] { patientImpactedId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PATIENTSIMPACTED_WHERE);

			query.append(_FINDER_COLUMN_PATIENTSIMPACTEDID_PATIENTIMPACTEDID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(patientImpactedId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_PATIENTSIMPACTEDID_PATIENTIMPACTEDID_2 =
		"patientsImpacted.patientImpactedId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_GROUPID = new FinderPath(PatientsImpactedModelImpl.ENTITY_CACHE_ENABLED,
			PatientsImpactedModelImpl.FINDER_CACHE_ENABLED,
			PatientsImpactedImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByGroupId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID =
		new FinderPath(PatientsImpactedModelImpl.ENTITY_CACHE_ENABLED,
			PatientsImpactedModelImpl.FINDER_CACHE_ENABLED,
			PatientsImpactedImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByGroupId",
			new String[] { Long.class.getName() },
			PatientsImpactedModelImpl.GROUPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_GROUPID = new FinderPath(PatientsImpactedModelImpl.ENTITY_CACHE_ENABLED,
			PatientsImpactedModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByGroupId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the patients impacteds where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the matching patients impacteds
	 */
	@Override
	public List<PatientsImpacted> findByGroupId(long groupId) {
		return findByGroupId(groupId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the patients impacteds where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of patients impacteds
	 * @param end the upper bound of the range of patients impacteds (not inclusive)
	 * @return the range of matching patients impacteds
	 */
	@Override
	public List<PatientsImpacted> findByGroupId(long groupId, int start, int end) {
		return findByGroupId(groupId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the patients impacteds where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of patients impacteds
	 * @param end the upper bound of the range of patients impacteds (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching patients impacteds
	 */
	@Override
	public List<PatientsImpacted> findByGroupId(long groupId, int start,
		int end, OrderByComparator<PatientsImpacted> orderByComparator) {
		return findByGroupId(groupId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the patients impacteds where groupId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param groupId the group ID
	 * @param start the lower bound of the range of patients impacteds
	 * @param end the upper bound of the range of patients impacteds (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching patients impacteds
	 */
	@Override
	public List<PatientsImpacted> findByGroupId(long groupId, int start,
		int end, OrderByComparator<PatientsImpacted> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID;
			finderArgs = new Object[] { groupId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_GROUPID;
			finderArgs = new Object[] { groupId, start, end, orderByComparator };
		}

		List<PatientsImpacted> list = null;

		if (retrieveFromCache) {
			list = (List<PatientsImpacted>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (PatientsImpacted patientsImpacted : list) {
					if ((groupId != patientsImpacted.getGroupId())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_PATIENTSIMPACTED_WHERE);

			query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(PatientsImpactedModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				if (!pagination) {
					list = (List<PatientsImpacted>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<PatientsImpacted>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first patients impacted in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching patients impacted
	 * @throws NoSuchPatientsImpactedException if a matching patients impacted could not be found
	 */
	@Override
	public PatientsImpacted findByGroupId_First(long groupId,
		OrderByComparator<PatientsImpacted> orderByComparator)
		throws NoSuchPatientsImpactedException {
		PatientsImpacted patientsImpacted = fetchByGroupId_First(groupId,
				orderByComparator);

		if (patientsImpacted != null) {
			return patientsImpacted;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("groupId=");
		msg.append(groupId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPatientsImpactedException(msg.toString());
	}

	/**
	 * Returns the first patients impacted in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching patients impacted, or <code>null</code> if a matching patients impacted could not be found
	 */
	@Override
	public PatientsImpacted fetchByGroupId_First(long groupId,
		OrderByComparator<PatientsImpacted> orderByComparator) {
		List<PatientsImpacted> list = findByGroupId(groupId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last patients impacted in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching patients impacted
	 * @throws NoSuchPatientsImpactedException if a matching patients impacted could not be found
	 */
	@Override
	public PatientsImpacted findByGroupId_Last(long groupId,
		OrderByComparator<PatientsImpacted> orderByComparator)
		throws NoSuchPatientsImpactedException {
		PatientsImpacted patientsImpacted = fetchByGroupId_Last(groupId,
				orderByComparator);

		if (patientsImpacted != null) {
			return patientsImpacted;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("groupId=");
		msg.append(groupId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchPatientsImpactedException(msg.toString());
	}

	/**
	 * Returns the last patients impacted in the ordered set where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching patients impacted, or <code>null</code> if a matching patients impacted could not be found
	 */
	@Override
	public PatientsImpacted fetchByGroupId_Last(long groupId,
		OrderByComparator<PatientsImpacted> orderByComparator) {
		int count = countByGroupId(groupId);

		if (count == 0) {
			return null;
		}

		List<PatientsImpacted> list = findByGroupId(groupId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the patients impacteds before and after the current patients impacted in the ordered set where groupId = &#63;.
	 *
	 * @param patientImpactedId the primary key of the current patients impacted
	 * @param groupId the group ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next patients impacted
	 * @throws NoSuchPatientsImpactedException if a patients impacted with the primary key could not be found
	 */
	@Override
	public PatientsImpacted[] findByGroupId_PrevAndNext(
		long patientImpactedId, long groupId,
		OrderByComparator<PatientsImpacted> orderByComparator)
		throws NoSuchPatientsImpactedException {
		PatientsImpacted patientsImpacted = findByPrimaryKey(patientImpactedId);

		Session session = null;

		try {
			session = openSession();

			PatientsImpacted[] array = new PatientsImpactedImpl[3];

			array[0] = getByGroupId_PrevAndNext(session, patientsImpacted,
					groupId, orderByComparator, true);

			array[1] = patientsImpacted;

			array[2] = getByGroupId_PrevAndNext(session, patientsImpacted,
					groupId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected PatientsImpacted getByGroupId_PrevAndNext(Session session,
		PatientsImpacted patientsImpacted, long groupId,
		OrderByComparator<PatientsImpacted> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_PATIENTSIMPACTED_WHERE);

		query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(PatientsImpactedModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(groupId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(patientsImpacted);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<PatientsImpacted> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the patients impacteds where groupId = &#63; from the database.
	 *
	 * @param groupId the group ID
	 */
	@Override
	public void removeByGroupId(long groupId) {
		for (PatientsImpacted patientsImpacted : findByGroupId(groupId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(patientsImpacted);
		}
	}

	/**
	 * Returns the number of patients impacteds where groupId = &#63;.
	 *
	 * @param groupId the group ID
	 * @return the number of matching patients impacteds
	 */
	@Override
	public int countByGroupId(long groupId) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_GROUPID;

		Object[] finderArgs = new Object[] { groupId };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_PATIENTSIMPACTED_WHERE);

			query.append(_FINDER_COLUMN_GROUPID_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_GROUPID_GROUPID_2 = "patientsImpacted.groupId = ?";

	public PatientsImpactedPersistenceImpl() {
		setModelClass(PatientsImpacted.class);
	}

	/**
	 * Caches the patients impacted in the entity cache if it is enabled.
	 *
	 * @param patientsImpacted the patients impacted
	 */
	@Override
	public void cacheResult(PatientsImpacted patientsImpacted) {
		entityCache.putResult(PatientsImpactedModelImpl.ENTITY_CACHE_ENABLED,
			PatientsImpactedImpl.class, patientsImpacted.getPrimaryKey(),
			patientsImpacted);

		finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G,
			new Object[] {
				patientsImpacted.getUuid(), patientsImpacted.getGroupId()
			}, patientsImpacted);

		finderCache.putResult(FINDER_PATH_FETCH_BY_PATIENTSIMPACTEDID,
			new Object[] { patientsImpacted.getPatientImpactedId() },
			patientsImpacted);

		patientsImpacted.resetOriginalValues();
	}

	/**
	 * Caches the patients impacteds in the entity cache if it is enabled.
	 *
	 * @param patientsImpacteds the patients impacteds
	 */
	@Override
	public void cacheResult(List<PatientsImpacted> patientsImpacteds) {
		for (PatientsImpacted patientsImpacted : patientsImpacteds) {
			if (entityCache.getResult(
						PatientsImpactedModelImpl.ENTITY_CACHE_ENABLED,
						PatientsImpactedImpl.class,
						patientsImpacted.getPrimaryKey()) == null) {
				cacheResult(patientsImpacted);
			}
			else {
				patientsImpacted.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all patients impacteds.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(PatientsImpactedImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the patients impacted.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(PatientsImpacted patientsImpacted) {
		entityCache.removeResult(PatientsImpactedModelImpl.ENTITY_CACHE_ENABLED,
			PatientsImpactedImpl.class, patientsImpacted.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache((PatientsImpactedModelImpl)patientsImpacted);
	}

	@Override
	public void clearCache(List<PatientsImpacted> patientsImpacteds) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (PatientsImpacted patientsImpacted : patientsImpacteds) {
			entityCache.removeResult(PatientsImpactedModelImpl.ENTITY_CACHE_ENABLED,
				PatientsImpactedImpl.class, patientsImpacted.getPrimaryKey());

			clearUniqueFindersCache((PatientsImpactedModelImpl)patientsImpacted);
		}
	}

	protected void cacheUniqueFindersCache(
		PatientsImpactedModelImpl patientsImpactedModelImpl, boolean isNew) {
		if (isNew) {
			Object[] args = new Object[] {
					patientsImpactedModelImpl.getUuid(),
					patientsImpactedModelImpl.getGroupId()
				};

			finderCache.putResult(FINDER_PATH_COUNT_BY_UUID_G, args,
				Long.valueOf(1));
			finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G, args,
				patientsImpactedModelImpl);

			args = new Object[] { patientsImpactedModelImpl.getPatientImpactedId() };

			finderCache.putResult(FINDER_PATH_COUNT_BY_PATIENTSIMPACTEDID,
				args, Long.valueOf(1));
			finderCache.putResult(FINDER_PATH_FETCH_BY_PATIENTSIMPACTEDID,
				args, patientsImpactedModelImpl);
		}
		else {
			if ((patientsImpactedModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_UUID_G.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						patientsImpactedModelImpl.getUuid(),
						patientsImpactedModelImpl.getGroupId()
					};

				finderCache.putResult(FINDER_PATH_COUNT_BY_UUID_G, args,
					Long.valueOf(1));
				finderCache.putResult(FINDER_PATH_FETCH_BY_UUID_G, args,
					patientsImpactedModelImpl);
			}

			if ((patientsImpactedModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_PATIENTSIMPACTEDID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						patientsImpactedModelImpl.getPatientImpactedId()
					};

				finderCache.putResult(FINDER_PATH_COUNT_BY_PATIENTSIMPACTEDID,
					args, Long.valueOf(1));
				finderCache.putResult(FINDER_PATH_FETCH_BY_PATIENTSIMPACTEDID,
					args, patientsImpactedModelImpl);
			}
		}
	}

	protected void clearUniqueFindersCache(
		PatientsImpactedModelImpl patientsImpactedModelImpl) {
		Object[] args = new Object[] {
				patientsImpactedModelImpl.getUuid(),
				patientsImpactedModelImpl.getGroupId()
			};

		finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
		finderCache.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);

		if ((patientsImpactedModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_UUID_G.getColumnBitmask()) != 0) {
			args = new Object[] {
					patientsImpactedModelImpl.getOriginalUuid(),
					patientsImpactedModelImpl.getOriginalGroupId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);
		}

		args = new Object[] { patientsImpactedModelImpl.getPatientImpactedId() };

		finderCache.removeResult(FINDER_PATH_COUNT_BY_PATIENTSIMPACTEDID, args);
		finderCache.removeResult(FINDER_PATH_FETCH_BY_PATIENTSIMPACTEDID, args);

		if ((patientsImpactedModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_PATIENTSIMPACTEDID.getColumnBitmask()) != 0) {
			args = new Object[] {
					patientsImpactedModelImpl.getOriginalPatientImpactedId()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_PATIENTSIMPACTEDID,
				args);
			finderCache.removeResult(FINDER_PATH_FETCH_BY_PATIENTSIMPACTEDID,
				args);
		}
	}

	/**
	 * Creates a new patients impacted with the primary key. Does not add the patients impacted to the database.
	 *
	 * @param patientImpactedId the primary key for the new patients impacted
	 * @return the new patients impacted
	 */
	@Override
	public PatientsImpacted create(long patientImpactedId) {
		PatientsImpacted patientsImpacted = new PatientsImpactedImpl();

		patientsImpacted.setNew(true);
		patientsImpacted.setPrimaryKey(patientImpactedId);

		String uuid = PortalUUIDUtil.generate();

		patientsImpacted.setUuid(uuid);

		patientsImpacted.setCompanyId(companyProvider.getCompanyId());

		return patientsImpacted;
	}

	/**
	 * Removes the patients impacted with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param patientImpactedId the primary key of the patients impacted
	 * @return the patients impacted that was removed
	 * @throws NoSuchPatientsImpactedException if a patients impacted with the primary key could not be found
	 */
	@Override
	public PatientsImpacted remove(long patientImpactedId)
		throws NoSuchPatientsImpactedException {
		return remove((Serializable)patientImpactedId);
	}

	/**
	 * Removes the patients impacted with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the patients impacted
	 * @return the patients impacted that was removed
	 * @throws NoSuchPatientsImpactedException if a patients impacted with the primary key could not be found
	 */
	@Override
	public PatientsImpacted remove(Serializable primaryKey)
		throws NoSuchPatientsImpactedException {
		Session session = null;

		try {
			session = openSession();

			PatientsImpacted patientsImpacted = (PatientsImpacted)session.get(PatientsImpactedImpl.class,
					primaryKey);

			if (patientsImpacted == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchPatientsImpactedException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(patientsImpacted);
		}
		catch (NoSuchPatientsImpactedException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected PatientsImpacted removeImpl(PatientsImpacted patientsImpacted) {
		patientsImpacted = toUnwrappedModel(patientsImpacted);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(patientsImpacted)) {
				patientsImpacted = (PatientsImpacted)session.get(PatientsImpactedImpl.class,
						patientsImpacted.getPrimaryKeyObj());
			}

			if (patientsImpacted != null) {
				session.delete(patientsImpacted);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (patientsImpacted != null) {
			clearCache(patientsImpacted);
		}

		return patientsImpacted;
	}

	@Override
	public PatientsImpacted updateImpl(PatientsImpacted patientsImpacted) {
		patientsImpacted = toUnwrappedModel(patientsImpacted);

		boolean isNew = patientsImpacted.isNew();

		PatientsImpactedModelImpl patientsImpactedModelImpl = (PatientsImpactedModelImpl)patientsImpacted;

		if (Validator.isNull(patientsImpacted.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			patientsImpacted.setUuid(uuid);
		}

		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();

		Date now = new Date();

		if (isNew && (patientsImpacted.getCreateDate() == null)) {
			if (serviceContext == null) {
				patientsImpacted.setCreateDate(now);
			}
			else {
				patientsImpacted.setCreateDate(serviceContext.getCreateDate(now));
			}
		}

		if (!patientsImpactedModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				patientsImpacted.setModifiedDate(now);
			}
			else {
				patientsImpacted.setModifiedDate(serviceContext.getModifiedDate(
						now));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (patientsImpacted.isNew()) {
				session.save(patientsImpacted);

				patientsImpacted.setNew(false);
			}
			else {
				patientsImpacted = (PatientsImpacted)session.merge(patientsImpacted);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !PatientsImpactedModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((patientsImpactedModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						patientsImpactedModelImpl.getOriginalUuid()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);

				args = new Object[] { patientsImpactedModelImpl.getUuid() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);
			}

			if ((patientsImpactedModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						patientsImpactedModelImpl.getOriginalUuid(),
						patientsImpactedModelImpl.getOriginalCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);

				args = new Object[] {
						patientsImpactedModelImpl.getUuid(),
						patientsImpactedModelImpl.getCompanyId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);
			}

			if ((patientsImpactedModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						patientsImpactedModelImpl.getOriginalGroupId()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_GROUPID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID,
					args);

				args = new Object[] { patientsImpactedModelImpl.getGroupId() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_GROUPID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GROUPID,
					args);
			}
		}

		entityCache.putResult(PatientsImpactedModelImpl.ENTITY_CACHE_ENABLED,
			PatientsImpactedImpl.class, patientsImpacted.getPrimaryKey(),
			patientsImpacted, false);

		clearUniqueFindersCache(patientsImpactedModelImpl);
		cacheUniqueFindersCache(patientsImpactedModelImpl, isNew);

		patientsImpacted.resetOriginalValues();

		return patientsImpacted;
	}

	protected PatientsImpacted toUnwrappedModel(
		PatientsImpacted patientsImpacted) {
		if (patientsImpacted instanceof PatientsImpactedImpl) {
			return patientsImpacted;
		}

		PatientsImpactedImpl patientsImpactedImpl = new PatientsImpactedImpl();

		patientsImpactedImpl.setNew(patientsImpacted.isNew());
		patientsImpactedImpl.setPrimaryKey(patientsImpacted.getPrimaryKey());

		patientsImpactedImpl.setUuid(patientsImpacted.getUuid());
		patientsImpactedImpl.setPatientImpactedId(patientsImpacted.getPatientImpactedId());
		patientsImpactedImpl.setGroupId(patientsImpacted.getGroupId());
		patientsImpactedImpl.setCompanyId(patientsImpacted.getCompanyId());
		patientsImpactedImpl.setUserId(patientsImpacted.getUserId());
		patientsImpactedImpl.setUserName(patientsImpacted.getUserName());
		patientsImpactedImpl.setCreateDate(patientsImpacted.getCreateDate());
		patientsImpactedImpl.setModifiedDate(patientsImpacted.getModifiedDate());
		patientsImpactedImpl.setAsOfDate(patientsImpacted.getAsOfDate());
		patientsImpactedImpl.setRangeEstimate(patientsImpacted.getRangeEstimate());
		patientsImpactedImpl.setPopulation(patientsImpacted.getPopulation());
		patientsImpactedImpl.setOptionalComments(patientsImpacted.getOptionalComments());

		return patientsImpactedImpl;
	}

	/**
	 * Returns the patients impacted with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the patients impacted
	 * @return the patients impacted
	 * @throws NoSuchPatientsImpactedException if a patients impacted with the primary key could not be found
	 */
	@Override
	public PatientsImpacted findByPrimaryKey(Serializable primaryKey)
		throws NoSuchPatientsImpactedException {
		PatientsImpacted patientsImpacted = fetchByPrimaryKey(primaryKey);

		if (patientsImpacted == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchPatientsImpactedException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return patientsImpacted;
	}

	/**
	 * Returns the patients impacted with the primary key or throws a {@link NoSuchPatientsImpactedException} if it could not be found.
	 *
	 * @param patientImpactedId the primary key of the patients impacted
	 * @return the patients impacted
	 * @throws NoSuchPatientsImpactedException if a patients impacted with the primary key could not be found
	 */
	@Override
	public PatientsImpacted findByPrimaryKey(long patientImpactedId)
		throws NoSuchPatientsImpactedException {
		return findByPrimaryKey((Serializable)patientImpactedId);
	}

	/**
	 * Returns the patients impacted with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the patients impacted
	 * @return the patients impacted, or <code>null</code> if a patients impacted with the primary key could not be found
	 */
	@Override
	public PatientsImpacted fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(PatientsImpactedModelImpl.ENTITY_CACHE_ENABLED,
				PatientsImpactedImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		PatientsImpacted patientsImpacted = (PatientsImpacted)serializable;

		if (patientsImpacted == null) {
			Session session = null;

			try {
				session = openSession();

				patientsImpacted = (PatientsImpacted)session.get(PatientsImpactedImpl.class,
						primaryKey);

				if (patientsImpacted != null) {
					cacheResult(patientsImpacted);
				}
				else {
					entityCache.putResult(PatientsImpactedModelImpl.ENTITY_CACHE_ENABLED,
						PatientsImpactedImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(PatientsImpactedModelImpl.ENTITY_CACHE_ENABLED,
					PatientsImpactedImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return patientsImpacted;
	}

	/**
	 * Returns the patients impacted with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param patientImpactedId the primary key of the patients impacted
	 * @return the patients impacted, or <code>null</code> if a patients impacted with the primary key could not be found
	 */
	@Override
	public PatientsImpacted fetchByPrimaryKey(long patientImpactedId) {
		return fetchByPrimaryKey((Serializable)patientImpactedId);
	}

	@Override
	public Map<Serializable, PatientsImpacted> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, PatientsImpacted> map = new HashMap<Serializable, PatientsImpacted>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			PatientsImpacted patientsImpacted = fetchByPrimaryKey(primaryKey);

			if (patientsImpacted != null) {
				map.put(primaryKey, patientsImpacted);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(PatientsImpactedModelImpl.ENTITY_CACHE_ENABLED,
					PatientsImpactedImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (PatientsImpacted)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_PATIENTSIMPACTED_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(String.valueOf(primaryKey));

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (PatientsImpacted patientsImpacted : (List<PatientsImpacted>)q.list()) {
				map.put(patientsImpacted.getPrimaryKeyObj(), patientsImpacted);

				cacheResult(patientsImpacted);

				uncachedPrimaryKeys.remove(patientsImpacted.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(PatientsImpactedModelImpl.ENTITY_CACHE_ENABLED,
					PatientsImpactedImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the patients impacteds.
	 *
	 * @return the patients impacteds
	 */
	@Override
	public List<PatientsImpacted> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the patients impacteds.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of patients impacteds
	 * @param end the upper bound of the range of patients impacteds (not inclusive)
	 * @return the range of patients impacteds
	 */
	@Override
	public List<PatientsImpacted> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the patients impacteds.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of patients impacteds
	 * @param end the upper bound of the range of patients impacteds (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of patients impacteds
	 */
	@Override
	public List<PatientsImpacted> findAll(int start, int end,
		OrderByComparator<PatientsImpacted> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the patients impacteds.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of patients impacteds
	 * @param end the upper bound of the range of patients impacteds (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of patients impacteds
	 */
	@Override
	public List<PatientsImpacted> findAll(int start, int end,
		OrderByComparator<PatientsImpacted> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<PatientsImpacted> list = null;

		if (retrieveFromCache) {
			list = (List<PatientsImpacted>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_PATIENTSIMPACTED);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_PATIENTSIMPACTED;

				if (pagination) {
					sql = sql.concat(PatientsImpactedModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<PatientsImpacted>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<PatientsImpacted>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the patients impacteds from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (PatientsImpacted patientsImpacted : findAll()) {
			remove(patientsImpacted);
		}
	}

	/**
	 * Returns the number of patients impacteds.
	 *
	 * @return the number of patients impacteds
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_PATIENTSIMPACTED);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return PatientsImpactedModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the patients impacted persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(PatientsImpactedImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = CompanyProviderWrapper.class)
	protected CompanyProvider companyProvider;
	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_PATIENTSIMPACTED = "SELECT patientsImpacted FROM PatientsImpacted patientsImpacted";
	private static final String _SQL_SELECT_PATIENTSIMPACTED_WHERE_PKS_IN = "SELECT patientsImpacted FROM PatientsImpacted patientsImpacted WHERE patientImpactedId IN (";
	private static final String _SQL_SELECT_PATIENTSIMPACTED_WHERE = "SELECT patientsImpacted FROM PatientsImpacted patientsImpacted WHERE ";
	private static final String _SQL_COUNT_PATIENTSIMPACTED = "SELECT COUNT(patientsImpacted) FROM PatientsImpacted patientsImpacted";
	private static final String _SQL_COUNT_PATIENTSIMPACTED_WHERE = "SELECT COUNT(patientsImpacted) FROM PatientsImpacted patientsImpacted WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "patientsImpacted.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No PatientsImpacted exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No PatientsImpacted exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(PatientsImpactedPersistenceImpl.class);
	private static final Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"uuid"
			});
}