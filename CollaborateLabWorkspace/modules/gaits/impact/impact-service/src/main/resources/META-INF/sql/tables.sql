create table CLab_Impact_CareerImpact (
	uuid_ VARCHAR(75) null,
	careerImpactId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	asOfDate DATE null,
	categoryRaing INTEGER,
	optionalComments VARCHAR(75) null
);

create table CLab_Impact_Collaborators (
	uuid_ VARCHAR(75) null,
	collaboratorId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	startDate DATE null,
	endDate DATE null,
	name VARCHAR(75) null,
	URL VARCHAR(75) null,
	type_ INTEGER,
	optionalComments VARCHAR(75) null
);

create table CLab_Impact_IntellectualProperty (
	uuid_ VARCHAR(75) null,
	intellectualPropertyId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	oneStFileDate DATE null,
	asOfDate DATE null,
	title VARCHAR(75) null,
	stage INTEGER,
	licenseStatus INTEGER
);

create table CLab_Impact_Jobs (
	uuid_ VARCHAR(75) null,
	jobsId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	asOfDate DATE null,
	sizeOfTeam INTEGER,
	comments VARCHAR(75) null,
	primaryJob VARCHAR(75) null
);

create table CLab_Impact_PatientsImpacted (
	uuid_ VARCHAR(75) null,
	patientImpactedId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	asOfDate DATE null,
	rangeEstimate INTEGER,
	population INTEGER,
	optionalComments VARCHAR(75) null
);

create table CLab_Impact_Publications (
	uuid_ VARCHAR(75) null,
	publicationsId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	titleOfPublications VARCHAR(75) null,
	date_ DATE null,
	URL VARCHAR(75) null,
	comments VARCHAR(75) null
);

create table CLab_Impact_SitesUsingSolution (
	uuid_ VARCHAR(75) null,
	sitesUsingSolutionId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	asOfDate DATE null,
	number_ INTEGER,
	optionalComments VARCHAR(75) null
);

create table CLab_Impact_Support (
	uuid_ VARCHAR(75) null,
	supportId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	startDate DATE null,
	duration INTEGER,
	totalValue DOUBLE,
	sponsorType INTEGER,
	supportType INTEGER
);

create table CLab_Impact_TeamMemberProjectRecognition (
	uuid_ VARCHAR(75) null,
	teamMemberProjectRecognitionId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	dateOfRecognition DATE null,
	Name VARCHAR(75) null,
	CatRating INTEGER,
	URL VARCHAR(75) null,
	optionalComments VARCHAR(75) null
);

create table if not exists CLab_Impact_CareerImpact (
	uuid_ VARCHAR(75) null,
	careerImpactId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	asOfDate DATE null,
	categoryRaing INTEGER,
	optionalComments VARCHAR(75) null
);

create table if not exists CLab_Impact_Collaborators (
	uuid_ VARCHAR(75) null,
	collaboratorId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	startDate DATE null,
	endDate DATE null,
	nameLink VARCHAR(75) null,
	type_ INTEGER,
	optionalComments VARCHAR(75) null
);

create table if not exists CLab_Impact_IntellectualProperty (
	uuid_ VARCHAR(75) null,
	intellectualPropertyId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	oneStFileDate DATE null,
	asOfDate DATE null,
	title VARCHAR(75) null,
	stage INTEGER,
	licenseStatus INTEGER
);

create table if not exists CLab_Impact_Jobs (
	uuid_ VARCHAR(75) null,
	jobsId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	asOfDate DATE null,
	sizeOfTeam INTEGER,
	comments VARCHAR(75) null
);

create table if not exists CLab_Impact_PatientsImpacted (
	uuid_ VARCHAR(75) null,
	patientImpactedId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	asOfDate DATE null,
	rangeEstimate INTEGER,
	population INTEGER,
	optionalComments VARCHAR(75) null
);

create table if not exists CLab_Impact_Publications (
	uuid_ VARCHAR(75) null,
	publicationsId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	titleOfPublications VARCHAR(75) null,
	date_ DATE null,
	URL VARCHAR(75) null,
	comments VARCHAR(75) null
);

create table if not exists CLab_Impact_SitesUsingSolution (
	uuid_ VARCHAR(75) null,
	sitesUsingSolutionId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	asOfDate DATE null,
	number_ INTEGER,
	optionalComments VARCHAR(75) null
);

create table if not exists CLab_Impact_Support (
	uuid_ VARCHAR(75) null,
	supportId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	startDate DATE null,
	duration DATE null,
	totalValue DOUBLE,
	sponsorType INTEGER,
	supportType INTEGER
);

create table if not exists CLab_Impact_TeamMemberProjectRecognition (
	uuid_ VARCHAR(75) null,
	teamMemberProjectRecognitionId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	dateOfRecognition DATE null,
	Name VARCHAR(75) null,
	CatRating INTEGER,
	URL VARCHAR(75) null,
	optionalComments VARCHAR(75) null
);