create index IX_B6516D61 on CLab_Impact_CareerImpact (careerImpactId);
create index IX_C9E0AB5C on CLab_Impact_CareerImpact (groupId);
create index IX_486CB382 on CLab_Impact_CareerImpact (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_D303284 on CLab_Impact_CareerImpact (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_9EC8DD44 on CLab_Impact_Collaborators (collaboratorId);
create index IX_862F273B on CLab_Impact_Collaborators (groupId);
create index IX_99322283 on CLab_Impact_Collaborators (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_2AD76DC5 on CLab_Impact_Collaborators (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_5C07BD4B on CLab_Impact_IntellectualProperty (groupId);
create index IX_820FE083 on CLab_Impact_IntellectualProperty (intellectualPropertyId);
create index IX_23395E73 on CLab_Impact_IntellectualProperty (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_5CE5B5 on CLab_Impact_IntellectualProperty (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_360FA2A on CLab_Impact_Jobs (groupId);
create index IX_D544D5C5 on CLab_Impact_Jobs (jobsId);
create index IX_B27D2C74 on CLab_Impact_Jobs (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_568E3FF6 on CLab_Impact_Jobs (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_EC6437CD on CLab_Impact_PatientsImpacted (groupId);
create index IX_EADB9918 on CLab_Impact_PatientsImpacted (patientImpactedId);
create index IX_47919631 on CLab_Impact_PatientsImpacted (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_A03BF4F3 on CLab_Impact_PatientsImpacted (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_26CD7619 on CLab_Impact_Publications (groupId);
create index IX_F218A3E7 on CLab_Impact_Publications (publicationsId);
create index IX_27BAD65 on CLab_Impact_Publications (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_ED6A4927 on CLab_Impact_Publications (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_B88D432F on CLab_Impact_SitesUsingSolution (groupId);
create index IX_828622FB on CLab_Impact_SitesUsingSolution (sitesUsingSolutionId);
create index IX_C582390F on CLab_Impact_SitesUsingSolution (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_F513B751 on CLab_Impact_SitesUsingSolution (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_8EB69A59 on CLab_Impact_Support (groupId);
create index IX_31EDC469 on CLab_Impact_Support (supportId);
create index IX_D6F75125 on CLab_Impact_Support (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_1FFBDCE7 on CLab_Impact_Support (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_F1431E8B on CLab_Impact_TeamMemberProjectRecognition (groupId);
create index IX_F9D2B103 on CLab_Impact_TeamMemberProjectRecognition (teamMemberProjectRecognitionId);
create index IX_26976533 on CLab_Impact_TeamMemberProjectRecognition (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_6D0D9C75 on CLab_Impact_TeamMemberProjectRecognition (uuid_[$COLUMN_LENGTH:75$], groupId);