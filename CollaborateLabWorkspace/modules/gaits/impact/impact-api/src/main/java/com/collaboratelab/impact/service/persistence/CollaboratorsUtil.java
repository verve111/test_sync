/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.impact.model.Collaborators;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the collaborators service. This utility wraps {@link com.collaboratelab.impact.service.persistence.impl.CollaboratorsPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CollaboratorsPersistence
 * @see com.collaboratelab.impact.service.persistence.impl.CollaboratorsPersistenceImpl
 * @generated
 */
@ProviderType
public class CollaboratorsUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Collaborators collaborators) {
		getPersistence().clearCache(collaborators);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Collaborators> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Collaborators> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Collaborators> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Collaborators> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Collaborators update(Collaborators collaborators) {
		return getPersistence().update(collaborators);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Collaborators update(Collaborators collaborators,
		ServiceContext serviceContext) {
		return getPersistence().update(collaborators, serviceContext);
	}

	/**
	* Returns all the collaboratorses where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching collaboratorses
	*/
	public static List<Collaborators> findByUuid(java.lang.String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	* Returns a range of all the collaboratorses where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of collaboratorses
	* @param end the upper bound of the range of collaboratorses (not inclusive)
	* @return the range of matching collaboratorses
	*/
	public static List<Collaborators> findByUuid(java.lang.String uuid,
		int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	* Returns an ordered range of all the collaboratorses where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of collaboratorses
	* @param end the upper bound of the range of collaboratorses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching collaboratorses
	*/
	public static List<Collaborators> findByUuid(java.lang.String uuid,
		int start, int end, OrderByComparator<Collaborators> orderByComparator) {
		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the collaboratorses where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of collaboratorses
	* @param end the upper bound of the range of collaboratorses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching collaboratorses
	*/
	public static List<Collaborators> findByUuid(java.lang.String uuid,
		int start, int end, OrderByComparator<Collaborators> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid(uuid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first collaborators in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching collaborators
	* @throws NoSuchCollaboratorsException if a matching collaborators could not be found
	*/
	public static Collaborators findByUuid_First(java.lang.String uuid,
		OrderByComparator<Collaborators> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchCollaboratorsException {
		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the first collaborators in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching collaborators, or <code>null</code> if a matching collaborators could not be found
	*/
	public static Collaborators fetchByUuid_First(java.lang.String uuid,
		OrderByComparator<Collaborators> orderByComparator) {
		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the last collaborators in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching collaborators
	* @throws NoSuchCollaboratorsException if a matching collaborators could not be found
	*/
	public static Collaborators findByUuid_Last(java.lang.String uuid,
		OrderByComparator<Collaborators> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchCollaboratorsException {
		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the last collaborators in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching collaborators, or <code>null</code> if a matching collaborators could not be found
	*/
	public static Collaborators fetchByUuid_Last(java.lang.String uuid,
		OrderByComparator<Collaborators> orderByComparator) {
		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the collaboratorses before and after the current collaborators in the ordered set where uuid = &#63;.
	*
	* @param collaboratorId the primary key of the current collaborators
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next collaborators
	* @throws NoSuchCollaboratorsException if a collaborators with the primary key could not be found
	*/
	public static Collaborators[] findByUuid_PrevAndNext(long collaboratorId,
		java.lang.String uuid,
		OrderByComparator<Collaborators> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchCollaboratorsException {
		return getPersistence()
				   .findByUuid_PrevAndNext(collaboratorId, uuid,
			orderByComparator);
	}

	/**
	* Removes all the collaboratorses where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public static void removeByUuid(java.lang.String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	* Returns the number of collaboratorses where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching collaboratorses
	*/
	public static int countByUuid(java.lang.String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	* Returns the collaborators where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchCollaboratorsException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching collaborators
	* @throws NoSuchCollaboratorsException if a matching collaborators could not be found
	*/
	public static Collaborators findByUUID_G(java.lang.String uuid, long groupId)
		throws com.collaboratelab.impact.exception.NoSuchCollaboratorsException {
		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	* Returns the collaborators where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching collaborators, or <code>null</code> if a matching collaborators could not be found
	*/
	public static Collaborators fetchByUUID_G(java.lang.String uuid,
		long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	* Returns the collaborators where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching collaborators, or <code>null</code> if a matching collaborators could not be found
	*/
	public static Collaborators fetchByUUID_G(java.lang.String uuid,
		long groupId, boolean retrieveFromCache) {
		return getPersistence().fetchByUUID_G(uuid, groupId, retrieveFromCache);
	}

	/**
	* Removes the collaborators where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the collaborators that was removed
	*/
	public static Collaborators removeByUUID_G(java.lang.String uuid,
		long groupId)
		throws com.collaboratelab.impact.exception.NoSuchCollaboratorsException {
		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	* Returns the number of collaboratorses where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching collaboratorses
	*/
	public static int countByUUID_G(java.lang.String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	* Returns all the collaboratorses where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching collaboratorses
	*/
	public static List<Collaborators> findByUuid_C(java.lang.String uuid,
		long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	* Returns a range of all the collaboratorses where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of collaboratorses
	* @param end the upper bound of the range of collaboratorses (not inclusive)
	* @return the range of matching collaboratorses
	*/
	public static List<Collaborators> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end) {
		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	* Returns an ordered range of all the collaboratorses where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of collaboratorses
	* @param end the upper bound of the range of collaboratorses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching collaboratorses
	*/
	public static List<Collaborators> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<Collaborators> orderByComparator) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the collaboratorses where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of collaboratorses
	* @param end the upper bound of the range of collaboratorses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching collaboratorses
	*/
	public static List<Collaborators> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<Collaborators> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first collaborators in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching collaborators
	* @throws NoSuchCollaboratorsException if a matching collaborators could not be found
	*/
	public static Collaborators findByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<Collaborators> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchCollaboratorsException {
		return getPersistence()
				   .findByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the first collaborators in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching collaborators, or <code>null</code> if a matching collaborators could not be found
	*/
	public static Collaborators fetchByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<Collaborators> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last collaborators in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching collaborators
	* @throws NoSuchCollaboratorsException if a matching collaborators could not be found
	*/
	public static Collaborators findByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<Collaborators> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchCollaboratorsException {
		return getPersistence()
				   .findByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last collaborators in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching collaborators, or <code>null</code> if a matching collaborators could not be found
	*/
	public static Collaborators fetchByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<Collaborators> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the collaboratorses before and after the current collaborators in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param collaboratorId the primary key of the current collaborators
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next collaborators
	* @throws NoSuchCollaboratorsException if a collaborators with the primary key could not be found
	*/
	public static Collaborators[] findByUuid_C_PrevAndNext(
		long collaboratorId, java.lang.String uuid, long companyId,
		OrderByComparator<Collaborators> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchCollaboratorsException {
		return getPersistence()
				   .findByUuid_C_PrevAndNext(collaboratorId, uuid, companyId,
			orderByComparator);
	}

	/**
	* Removes all the collaboratorses where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public static void removeByUuid_C(java.lang.String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	* Returns the number of collaboratorses where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching collaboratorses
	*/
	public static int countByUuid_C(java.lang.String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	* Returns the collaborators where collaboratorId = &#63; or throws a {@link NoSuchCollaboratorsException} if it could not be found.
	*
	* @param collaboratorId the collaborator ID
	* @return the matching collaborators
	* @throws NoSuchCollaboratorsException if a matching collaborators could not be found
	*/
	public static Collaborators findByCollaboratorsId(long collaboratorId)
		throws com.collaboratelab.impact.exception.NoSuchCollaboratorsException {
		return getPersistence().findByCollaboratorsId(collaboratorId);
	}

	/**
	* Returns the collaborators where collaboratorId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param collaboratorId the collaborator ID
	* @return the matching collaborators, or <code>null</code> if a matching collaborators could not be found
	*/
	public static Collaborators fetchByCollaboratorsId(long collaboratorId) {
		return getPersistence().fetchByCollaboratorsId(collaboratorId);
	}

	/**
	* Returns the collaborators where collaboratorId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param collaboratorId the collaborator ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching collaborators, or <code>null</code> if a matching collaborators could not be found
	*/
	public static Collaborators fetchByCollaboratorsId(long collaboratorId,
		boolean retrieveFromCache) {
		return getPersistence()
				   .fetchByCollaboratorsId(collaboratorId, retrieveFromCache);
	}

	/**
	* Removes the collaborators where collaboratorId = &#63; from the database.
	*
	* @param collaboratorId the collaborator ID
	* @return the collaborators that was removed
	*/
	public static Collaborators removeByCollaboratorsId(long collaboratorId)
		throws com.collaboratelab.impact.exception.NoSuchCollaboratorsException {
		return getPersistence().removeByCollaboratorsId(collaboratorId);
	}

	/**
	* Returns the number of collaboratorses where collaboratorId = &#63;.
	*
	* @param collaboratorId the collaborator ID
	* @return the number of matching collaboratorses
	*/
	public static int countByCollaboratorsId(long collaboratorId) {
		return getPersistence().countByCollaboratorsId(collaboratorId);
	}

	/**
	* Returns all the collaboratorses where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the matching collaboratorses
	*/
	public static List<Collaborators> findByGroupId(long groupId) {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	* Returns a range of all the collaboratorses where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of collaboratorses
	* @param end the upper bound of the range of collaboratorses (not inclusive)
	* @return the range of matching collaboratorses
	*/
	public static List<Collaborators> findByGroupId(long groupId, int start,
		int end) {
		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	* Returns an ordered range of all the collaboratorses where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of collaboratorses
	* @param end the upper bound of the range of collaboratorses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching collaboratorses
	*/
	public static List<Collaborators> findByGroupId(long groupId, int start,
		int end, OrderByComparator<Collaborators> orderByComparator) {
		return getPersistence()
				   .findByGroupId(groupId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the collaboratorses where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of collaboratorses
	* @param end the upper bound of the range of collaboratorses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching collaboratorses
	*/
	public static List<Collaborators> findByGroupId(long groupId, int start,
		int end, OrderByComparator<Collaborators> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByGroupId(groupId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first collaborators in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching collaborators
	* @throws NoSuchCollaboratorsException if a matching collaborators could not be found
	*/
	public static Collaborators findByGroupId_First(long groupId,
		OrderByComparator<Collaborators> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchCollaboratorsException {
		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	* Returns the first collaborators in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching collaborators, or <code>null</code> if a matching collaborators could not be found
	*/
	public static Collaborators fetchByGroupId_First(long groupId,
		OrderByComparator<Collaborators> orderByComparator) {
		return getPersistence().fetchByGroupId_First(groupId, orderByComparator);
	}

	/**
	* Returns the last collaborators in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching collaborators
	* @throws NoSuchCollaboratorsException if a matching collaborators could not be found
	*/
	public static Collaborators findByGroupId_Last(long groupId,
		OrderByComparator<Collaborators> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchCollaboratorsException {
		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	* Returns the last collaborators in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching collaborators, or <code>null</code> if a matching collaborators could not be found
	*/
	public static Collaborators fetchByGroupId_Last(long groupId,
		OrderByComparator<Collaborators> orderByComparator) {
		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	* Returns the collaboratorses before and after the current collaborators in the ordered set where groupId = &#63;.
	*
	* @param collaboratorId the primary key of the current collaborators
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next collaborators
	* @throws NoSuchCollaboratorsException if a collaborators with the primary key could not be found
	*/
	public static Collaborators[] findByGroupId_PrevAndNext(
		long collaboratorId, long groupId,
		OrderByComparator<Collaborators> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchCollaboratorsException {
		return getPersistence()
				   .findByGroupId_PrevAndNext(collaboratorId, groupId,
			orderByComparator);
	}

	/**
	* Removes all the collaboratorses where groupId = &#63; from the database.
	*
	* @param groupId the group ID
	*/
	public static void removeByGroupId(long groupId) {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	* Returns the number of collaboratorses where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the number of matching collaboratorses
	*/
	public static int countByGroupId(long groupId) {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	* Caches the collaborators in the entity cache if it is enabled.
	*
	* @param collaborators the collaborators
	*/
	public static void cacheResult(Collaborators collaborators) {
		getPersistence().cacheResult(collaborators);
	}

	/**
	* Caches the collaboratorses in the entity cache if it is enabled.
	*
	* @param collaboratorses the collaboratorses
	*/
	public static void cacheResult(List<Collaborators> collaboratorses) {
		getPersistence().cacheResult(collaboratorses);
	}

	/**
	* Creates a new collaborators with the primary key. Does not add the collaborators to the database.
	*
	* @param collaboratorId the primary key for the new collaborators
	* @return the new collaborators
	*/
	public static Collaborators create(long collaboratorId) {
		return getPersistence().create(collaboratorId);
	}

	/**
	* Removes the collaborators with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param collaboratorId the primary key of the collaborators
	* @return the collaborators that was removed
	* @throws NoSuchCollaboratorsException if a collaborators with the primary key could not be found
	*/
	public static Collaborators remove(long collaboratorId)
		throws com.collaboratelab.impact.exception.NoSuchCollaboratorsException {
		return getPersistence().remove(collaboratorId);
	}

	public static Collaborators updateImpl(Collaborators collaborators) {
		return getPersistence().updateImpl(collaborators);
	}

	/**
	* Returns the collaborators with the primary key or throws a {@link NoSuchCollaboratorsException} if it could not be found.
	*
	* @param collaboratorId the primary key of the collaborators
	* @return the collaborators
	* @throws NoSuchCollaboratorsException if a collaborators with the primary key could not be found
	*/
	public static Collaborators findByPrimaryKey(long collaboratorId)
		throws com.collaboratelab.impact.exception.NoSuchCollaboratorsException {
		return getPersistence().findByPrimaryKey(collaboratorId);
	}

	/**
	* Returns the collaborators with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param collaboratorId the primary key of the collaborators
	* @return the collaborators, or <code>null</code> if a collaborators with the primary key could not be found
	*/
	public static Collaborators fetchByPrimaryKey(long collaboratorId) {
		return getPersistence().fetchByPrimaryKey(collaboratorId);
	}

	public static java.util.Map<java.io.Serializable, Collaborators> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the collaboratorses.
	*
	* @return the collaboratorses
	*/
	public static List<Collaborators> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the collaboratorses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of collaboratorses
	* @param end the upper bound of the range of collaboratorses (not inclusive)
	* @return the range of collaboratorses
	*/
	public static List<Collaborators> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the collaboratorses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of collaboratorses
	* @param end the upper bound of the range of collaboratorses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of collaboratorses
	*/
	public static List<Collaborators> findAll(int start, int end,
		OrderByComparator<Collaborators> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the collaboratorses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of collaboratorses
	* @param end the upper bound of the range of collaboratorses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of collaboratorses
	*/
	public static List<Collaborators> findAll(int start, int end,
		OrderByComparator<Collaborators> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the collaboratorses from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of collaboratorses.
	*
	* @return the number of collaboratorses
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static java.util.Set<java.lang.String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static CollaboratorsPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<CollaboratorsPersistence, CollaboratorsPersistence> _serviceTracker =
		ServiceTrackerFactory.open(CollaboratorsPersistence.class);
}