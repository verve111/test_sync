/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.impact.model.PatientsImpacted;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the patients impacted service. This utility wraps {@link com.collaboratelab.impact.service.persistence.impl.PatientsImpactedPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see PatientsImpactedPersistence
 * @see com.collaboratelab.impact.service.persistence.impl.PatientsImpactedPersistenceImpl
 * @generated
 */
@ProviderType
public class PatientsImpactedUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(PatientsImpacted patientsImpacted) {
		getPersistence().clearCache(patientsImpacted);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<PatientsImpacted> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<PatientsImpacted> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<PatientsImpacted> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<PatientsImpacted> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static PatientsImpacted update(PatientsImpacted patientsImpacted) {
		return getPersistence().update(patientsImpacted);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static PatientsImpacted update(PatientsImpacted patientsImpacted,
		ServiceContext serviceContext) {
		return getPersistence().update(patientsImpacted, serviceContext);
	}

	/**
	* Returns all the patients impacteds where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching patients impacteds
	*/
	public static List<PatientsImpacted> findByUuid(java.lang.String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	* Returns a range of all the patients impacteds where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of patients impacteds
	* @param end the upper bound of the range of patients impacteds (not inclusive)
	* @return the range of matching patients impacteds
	*/
	public static List<PatientsImpacted> findByUuid(java.lang.String uuid,
		int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	* Returns an ordered range of all the patients impacteds where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of patients impacteds
	* @param end the upper bound of the range of patients impacteds (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching patients impacteds
	*/
	public static List<PatientsImpacted> findByUuid(java.lang.String uuid,
		int start, int end,
		OrderByComparator<PatientsImpacted> orderByComparator) {
		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the patients impacteds where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of patients impacteds
	* @param end the upper bound of the range of patients impacteds (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching patients impacteds
	*/
	public static List<PatientsImpacted> findByUuid(java.lang.String uuid,
		int start, int end,
		OrderByComparator<PatientsImpacted> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid(uuid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first patients impacted in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching patients impacted
	* @throws NoSuchPatientsImpactedException if a matching patients impacted could not be found
	*/
	public static PatientsImpacted findByUuid_First(java.lang.String uuid,
		OrderByComparator<PatientsImpacted> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchPatientsImpactedException {
		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the first patients impacted in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching patients impacted, or <code>null</code> if a matching patients impacted could not be found
	*/
	public static PatientsImpacted fetchByUuid_First(java.lang.String uuid,
		OrderByComparator<PatientsImpacted> orderByComparator) {
		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the last patients impacted in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching patients impacted
	* @throws NoSuchPatientsImpactedException if a matching patients impacted could not be found
	*/
	public static PatientsImpacted findByUuid_Last(java.lang.String uuid,
		OrderByComparator<PatientsImpacted> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchPatientsImpactedException {
		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the last patients impacted in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching patients impacted, or <code>null</code> if a matching patients impacted could not be found
	*/
	public static PatientsImpacted fetchByUuid_Last(java.lang.String uuid,
		OrderByComparator<PatientsImpacted> orderByComparator) {
		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the patients impacteds before and after the current patients impacted in the ordered set where uuid = &#63;.
	*
	* @param patientImpactedId the primary key of the current patients impacted
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next patients impacted
	* @throws NoSuchPatientsImpactedException if a patients impacted with the primary key could not be found
	*/
	public static PatientsImpacted[] findByUuid_PrevAndNext(
		long patientImpactedId, java.lang.String uuid,
		OrderByComparator<PatientsImpacted> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchPatientsImpactedException {
		return getPersistence()
				   .findByUuid_PrevAndNext(patientImpactedId, uuid,
			orderByComparator);
	}

	/**
	* Removes all the patients impacteds where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public static void removeByUuid(java.lang.String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	* Returns the number of patients impacteds where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching patients impacteds
	*/
	public static int countByUuid(java.lang.String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	* Returns the patients impacted where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchPatientsImpactedException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching patients impacted
	* @throws NoSuchPatientsImpactedException if a matching patients impacted could not be found
	*/
	public static PatientsImpacted findByUUID_G(java.lang.String uuid,
		long groupId)
		throws com.collaboratelab.impact.exception.NoSuchPatientsImpactedException {
		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	* Returns the patients impacted where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching patients impacted, or <code>null</code> if a matching patients impacted could not be found
	*/
	public static PatientsImpacted fetchByUUID_G(java.lang.String uuid,
		long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	* Returns the patients impacted where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching patients impacted, or <code>null</code> if a matching patients impacted could not be found
	*/
	public static PatientsImpacted fetchByUUID_G(java.lang.String uuid,
		long groupId, boolean retrieveFromCache) {
		return getPersistence().fetchByUUID_G(uuid, groupId, retrieveFromCache);
	}

	/**
	* Removes the patients impacted where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the patients impacted that was removed
	*/
	public static PatientsImpacted removeByUUID_G(java.lang.String uuid,
		long groupId)
		throws com.collaboratelab.impact.exception.NoSuchPatientsImpactedException {
		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	* Returns the number of patients impacteds where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching patients impacteds
	*/
	public static int countByUUID_G(java.lang.String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	* Returns all the patients impacteds where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching patients impacteds
	*/
	public static List<PatientsImpacted> findByUuid_C(java.lang.String uuid,
		long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	* Returns a range of all the patients impacteds where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of patients impacteds
	* @param end the upper bound of the range of patients impacteds (not inclusive)
	* @return the range of matching patients impacteds
	*/
	public static List<PatientsImpacted> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end) {
		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	* Returns an ordered range of all the patients impacteds where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of patients impacteds
	* @param end the upper bound of the range of patients impacteds (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching patients impacteds
	*/
	public static List<PatientsImpacted> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<PatientsImpacted> orderByComparator) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the patients impacteds where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of patients impacteds
	* @param end the upper bound of the range of patients impacteds (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching patients impacteds
	*/
	public static List<PatientsImpacted> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<PatientsImpacted> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first patients impacted in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching patients impacted
	* @throws NoSuchPatientsImpactedException if a matching patients impacted could not be found
	*/
	public static PatientsImpacted findByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<PatientsImpacted> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchPatientsImpactedException {
		return getPersistence()
				   .findByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the first patients impacted in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching patients impacted, or <code>null</code> if a matching patients impacted could not be found
	*/
	public static PatientsImpacted fetchByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<PatientsImpacted> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last patients impacted in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching patients impacted
	* @throws NoSuchPatientsImpactedException if a matching patients impacted could not be found
	*/
	public static PatientsImpacted findByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<PatientsImpacted> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchPatientsImpactedException {
		return getPersistence()
				   .findByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last patients impacted in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching patients impacted, or <code>null</code> if a matching patients impacted could not be found
	*/
	public static PatientsImpacted fetchByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<PatientsImpacted> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the patients impacteds before and after the current patients impacted in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param patientImpactedId the primary key of the current patients impacted
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next patients impacted
	* @throws NoSuchPatientsImpactedException if a patients impacted with the primary key could not be found
	*/
	public static PatientsImpacted[] findByUuid_C_PrevAndNext(
		long patientImpactedId, java.lang.String uuid, long companyId,
		OrderByComparator<PatientsImpacted> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchPatientsImpactedException {
		return getPersistence()
				   .findByUuid_C_PrevAndNext(patientImpactedId, uuid,
			companyId, orderByComparator);
	}

	/**
	* Removes all the patients impacteds where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public static void removeByUuid_C(java.lang.String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	* Returns the number of patients impacteds where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching patients impacteds
	*/
	public static int countByUuid_C(java.lang.String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	* Returns the patients impacted where patientImpactedId = &#63; or throws a {@link NoSuchPatientsImpactedException} if it could not be found.
	*
	* @param patientImpactedId the patient impacted ID
	* @return the matching patients impacted
	* @throws NoSuchPatientsImpactedException if a matching patients impacted could not be found
	*/
	public static PatientsImpacted findByPatientsImpactedId(
		long patientImpactedId)
		throws com.collaboratelab.impact.exception.NoSuchPatientsImpactedException {
		return getPersistence().findByPatientsImpactedId(patientImpactedId);
	}

	/**
	* Returns the patients impacted where patientImpactedId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param patientImpactedId the patient impacted ID
	* @return the matching patients impacted, or <code>null</code> if a matching patients impacted could not be found
	*/
	public static PatientsImpacted fetchByPatientsImpactedId(
		long patientImpactedId) {
		return getPersistence().fetchByPatientsImpactedId(patientImpactedId);
	}

	/**
	* Returns the patients impacted where patientImpactedId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param patientImpactedId the patient impacted ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching patients impacted, or <code>null</code> if a matching patients impacted could not be found
	*/
	public static PatientsImpacted fetchByPatientsImpactedId(
		long patientImpactedId, boolean retrieveFromCache) {
		return getPersistence()
				   .fetchByPatientsImpactedId(patientImpactedId,
			retrieveFromCache);
	}

	/**
	* Removes the patients impacted where patientImpactedId = &#63; from the database.
	*
	* @param patientImpactedId the patient impacted ID
	* @return the patients impacted that was removed
	*/
	public static PatientsImpacted removeByPatientsImpactedId(
		long patientImpactedId)
		throws com.collaboratelab.impact.exception.NoSuchPatientsImpactedException {
		return getPersistence().removeByPatientsImpactedId(patientImpactedId);
	}

	/**
	* Returns the number of patients impacteds where patientImpactedId = &#63;.
	*
	* @param patientImpactedId the patient impacted ID
	* @return the number of matching patients impacteds
	*/
	public static int countByPatientsImpactedId(long patientImpactedId) {
		return getPersistence().countByPatientsImpactedId(patientImpactedId);
	}

	/**
	* Returns all the patients impacteds where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the matching patients impacteds
	*/
	public static List<PatientsImpacted> findByGroupId(long groupId) {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	* Returns a range of all the patients impacteds where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of patients impacteds
	* @param end the upper bound of the range of patients impacteds (not inclusive)
	* @return the range of matching patients impacteds
	*/
	public static List<PatientsImpacted> findByGroupId(long groupId, int start,
		int end) {
		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	* Returns an ordered range of all the patients impacteds where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of patients impacteds
	* @param end the upper bound of the range of patients impacteds (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching patients impacteds
	*/
	public static List<PatientsImpacted> findByGroupId(long groupId, int start,
		int end, OrderByComparator<PatientsImpacted> orderByComparator) {
		return getPersistence()
				   .findByGroupId(groupId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the patients impacteds where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of patients impacteds
	* @param end the upper bound of the range of patients impacteds (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching patients impacteds
	*/
	public static List<PatientsImpacted> findByGroupId(long groupId, int start,
		int end, OrderByComparator<PatientsImpacted> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByGroupId(groupId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first patients impacted in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching patients impacted
	* @throws NoSuchPatientsImpactedException if a matching patients impacted could not be found
	*/
	public static PatientsImpacted findByGroupId_First(long groupId,
		OrderByComparator<PatientsImpacted> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchPatientsImpactedException {
		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	* Returns the first patients impacted in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching patients impacted, or <code>null</code> if a matching patients impacted could not be found
	*/
	public static PatientsImpacted fetchByGroupId_First(long groupId,
		OrderByComparator<PatientsImpacted> orderByComparator) {
		return getPersistence().fetchByGroupId_First(groupId, orderByComparator);
	}

	/**
	* Returns the last patients impacted in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching patients impacted
	* @throws NoSuchPatientsImpactedException if a matching patients impacted could not be found
	*/
	public static PatientsImpacted findByGroupId_Last(long groupId,
		OrderByComparator<PatientsImpacted> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchPatientsImpactedException {
		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	* Returns the last patients impacted in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching patients impacted, or <code>null</code> if a matching patients impacted could not be found
	*/
	public static PatientsImpacted fetchByGroupId_Last(long groupId,
		OrderByComparator<PatientsImpacted> orderByComparator) {
		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	* Returns the patients impacteds before and after the current patients impacted in the ordered set where groupId = &#63;.
	*
	* @param patientImpactedId the primary key of the current patients impacted
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next patients impacted
	* @throws NoSuchPatientsImpactedException if a patients impacted with the primary key could not be found
	*/
	public static PatientsImpacted[] findByGroupId_PrevAndNext(
		long patientImpactedId, long groupId,
		OrderByComparator<PatientsImpacted> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchPatientsImpactedException {
		return getPersistence()
				   .findByGroupId_PrevAndNext(patientImpactedId, groupId,
			orderByComparator);
	}

	/**
	* Removes all the patients impacteds where groupId = &#63; from the database.
	*
	* @param groupId the group ID
	*/
	public static void removeByGroupId(long groupId) {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	* Returns the number of patients impacteds where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the number of matching patients impacteds
	*/
	public static int countByGroupId(long groupId) {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	* Caches the patients impacted in the entity cache if it is enabled.
	*
	* @param patientsImpacted the patients impacted
	*/
	public static void cacheResult(PatientsImpacted patientsImpacted) {
		getPersistence().cacheResult(patientsImpacted);
	}

	/**
	* Caches the patients impacteds in the entity cache if it is enabled.
	*
	* @param patientsImpacteds the patients impacteds
	*/
	public static void cacheResult(List<PatientsImpacted> patientsImpacteds) {
		getPersistence().cacheResult(patientsImpacteds);
	}

	/**
	* Creates a new patients impacted with the primary key. Does not add the patients impacted to the database.
	*
	* @param patientImpactedId the primary key for the new patients impacted
	* @return the new patients impacted
	*/
	public static PatientsImpacted create(long patientImpactedId) {
		return getPersistence().create(patientImpactedId);
	}

	/**
	* Removes the patients impacted with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param patientImpactedId the primary key of the patients impacted
	* @return the patients impacted that was removed
	* @throws NoSuchPatientsImpactedException if a patients impacted with the primary key could not be found
	*/
	public static PatientsImpacted remove(long patientImpactedId)
		throws com.collaboratelab.impact.exception.NoSuchPatientsImpactedException {
		return getPersistence().remove(patientImpactedId);
	}

	public static PatientsImpacted updateImpl(PatientsImpacted patientsImpacted) {
		return getPersistence().updateImpl(patientsImpacted);
	}

	/**
	* Returns the patients impacted with the primary key or throws a {@link NoSuchPatientsImpactedException} if it could not be found.
	*
	* @param patientImpactedId the primary key of the patients impacted
	* @return the patients impacted
	* @throws NoSuchPatientsImpactedException if a patients impacted with the primary key could not be found
	*/
	public static PatientsImpacted findByPrimaryKey(long patientImpactedId)
		throws com.collaboratelab.impact.exception.NoSuchPatientsImpactedException {
		return getPersistence().findByPrimaryKey(patientImpactedId);
	}

	/**
	* Returns the patients impacted with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param patientImpactedId the primary key of the patients impacted
	* @return the patients impacted, or <code>null</code> if a patients impacted with the primary key could not be found
	*/
	public static PatientsImpacted fetchByPrimaryKey(long patientImpactedId) {
		return getPersistence().fetchByPrimaryKey(patientImpactedId);
	}

	public static java.util.Map<java.io.Serializable, PatientsImpacted> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the patients impacteds.
	*
	* @return the patients impacteds
	*/
	public static List<PatientsImpacted> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the patients impacteds.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of patients impacteds
	* @param end the upper bound of the range of patients impacteds (not inclusive)
	* @return the range of patients impacteds
	*/
	public static List<PatientsImpacted> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the patients impacteds.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of patients impacteds
	* @param end the upper bound of the range of patients impacteds (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of patients impacteds
	*/
	public static List<PatientsImpacted> findAll(int start, int end,
		OrderByComparator<PatientsImpacted> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the patients impacteds.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of patients impacteds
	* @param end the upper bound of the range of patients impacteds (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of patients impacteds
	*/
	public static List<PatientsImpacted> findAll(int start, int end,
		OrderByComparator<PatientsImpacted> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the patients impacteds from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of patients impacteds.
	*
	* @return the number of patients impacteds
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static java.util.Set<java.lang.String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static PatientsImpactedPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<PatientsImpactedPersistence, PatientsImpactedPersistence> _serviceTracker =
		ServiceTrackerFactory.open(PatientsImpactedPersistence.class);
}