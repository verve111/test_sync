/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link SupportLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see SupportLocalService
 * @generated
 */
@ProviderType
public class SupportLocalServiceWrapper implements SupportLocalService,
	ServiceWrapper<SupportLocalService> {
	public SupportLocalServiceWrapper(SupportLocalService supportLocalService) {
		_supportLocalService = supportLocalService;
	}

	/**
	* Adds the support to the database. Also notifies the appropriate model listeners.
	*
	* @param support the support
	* @return the support that was added
	*/
	@Override
	public com.collaboratelab.impact.model.Support addSupport(
		com.collaboratelab.impact.model.Support support) {
		return _supportLocalService.addSupport(support);
	}

	/**
	* Creates a new support with the primary key. Does not add the support to the database.
	*
	* @param supportId the primary key for the new support
	* @return the new support
	*/
	@Override
	public com.collaboratelab.impact.model.Support createSupport(long supportId) {
		return _supportLocalService.createSupport(supportId);
	}

	/**
	* Deletes the support from the database. Also notifies the appropriate model listeners.
	*
	* @param support the support
	* @return the support that was removed
	*/
	@Override
	public com.collaboratelab.impact.model.Support deleteSupport(
		com.collaboratelab.impact.model.Support support) {
		return _supportLocalService.deleteSupport(support);
	}

	/**
	* Deletes the support with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param supportId the primary key of the support
	* @return the support that was removed
	* @throws PortalException if a support with the primary key could not be found
	*/
	@Override
	public com.collaboratelab.impact.model.Support deleteSupport(long supportId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _supportLocalService.deleteSupport(supportId);
	}

	@Override
	public com.collaboratelab.impact.model.Support fetchSupport(long supportId) {
		return _supportLocalService.fetchSupport(supportId);
	}

	/**
	* Returns the support matching the UUID and group.
	*
	* @param uuid the support's UUID
	* @param groupId the primary key of the group
	* @return the matching support, or <code>null</code> if a matching support could not be found
	*/
	@Override
	public com.collaboratelab.impact.model.Support fetchSupportByUuidAndGroupId(
		java.lang.String uuid, long groupId) {
		return _supportLocalService.fetchSupportByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Returns the support with the primary key.
	*
	* @param supportId the primary key of the support
	* @return the support
	* @throws PortalException if a support with the primary key could not be found
	*/
	@Override
	public com.collaboratelab.impact.model.Support getSupport(long supportId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _supportLocalService.getSupport(supportId);
	}

	/**
	* Returns the support matching the UUID and group.
	*
	* @param uuid the support's UUID
	* @param groupId the primary key of the group
	* @return the matching support
	* @throws PortalException if a matching support could not be found
	*/
	@Override
	public com.collaboratelab.impact.model.Support getSupportByUuidAndGroupId(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _supportLocalService.getSupportByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Updates the support in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param support the support
	* @return the support that was updated
	*/
	@Override
	public com.collaboratelab.impact.model.Support updateSupport(
		com.collaboratelab.impact.model.Support support) {
		return _supportLocalService.updateSupport(support);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _supportLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _supportLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery getExportActionableDynamicQuery(
		com.liferay.exportimport.kernel.lar.PortletDataContext portletDataContext) {
		return _supportLocalService.getExportActionableDynamicQuery(portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _supportLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _supportLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _supportLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of supports.
	*
	* @return the number of supports
	*/
	@Override
	public int getSupportsCount() {
		return _supportLocalService.getSupportsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _supportLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _supportLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.impact.model.impl.SupportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _supportLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.impact.model.impl.SupportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _supportLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	@Override
	public java.util.List<com.collaboratelab.impact.model.Support> findByGroupId(
		long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _supportLocalService.findByGroupId(groupId);
	}

	/**
	* Returns a range of all the supports.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.impact.model.impl.SupportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of supports
	* @param end the upper bound of the range of supports (not inclusive)
	* @return the range of supports
	*/
	@Override
	public java.util.List<com.collaboratelab.impact.model.Support> getSupports(
		int start, int end) {
		return _supportLocalService.getSupports(start, end);
	}

	/**
	* Returns all the supports matching the UUID and company.
	*
	* @param uuid the UUID of the supports
	* @param companyId the primary key of the company
	* @return the matching supports, or an empty list if no matches were found
	*/
	@Override
	public java.util.List<com.collaboratelab.impact.model.Support> getSupportsByUuidAndCompanyId(
		java.lang.String uuid, long companyId) {
		return _supportLocalService.getSupportsByUuidAndCompanyId(uuid,
			companyId);
	}

	/**
	* Returns a range of supports matching the UUID and company.
	*
	* @param uuid the UUID of the supports
	* @param companyId the primary key of the company
	* @param start the lower bound of the range of supports
	* @param end the upper bound of the range of supports (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the range of matching supports, or an empty list if no matches were found
	*/
	@Override
	public java.util.List<com.collaboratelab.impact.model.Support> getSupportsByUuidAndCompanyId(
		java.lang.String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<com.collaboratelab.impact.model.Support> orderByComparator) {
		return _supportLocalService.getSupportsByUuidAndCompanyId(uuid,
			companyId, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _supportLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _supportLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public SupportLocalService getWrappedService() {
		return _supportLocalService;
	}

	@Override
	public void setWrappedService(SupportLocalService supportLocalService) {
		_supportLocalService = supportLocalService;
	}

	private SupportLocalService _supportLocalService;
}