/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.collaboratelab.impact.service.http.SitesUsingSolutionServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see com.collaboratelab.impact.service.http.SitesUsingSolutionServiceSoap
 * @generated
 */
@ProviderType
public class SitesUsingSolutionSoap implements Serializable {
	public static SitesUsingSolutionSoap toSoapModel(SitesUsingSolution model) {
		SitesUsingSolutionSoap soapModel = new SitesUsingSolutionSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setSitesUsingSolutionId(model.getSitesUsingSolutionId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setAsOfDate(model.getAsOfDate());
		soapModel.setNumber(model.getNumber());
		soapModel.setOptionalComments(model.getOptionalComments());

		return soapModel;
	}

	public static SitesUsingSolutionSoap[] toSoapModels(
		SitesUsingSolution[] models) {
		SitesUsingSolutionSoap[] soapModels = new SitesUsingSolutionSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static SitesUsingSolutionSoap[][] toSoapModels(
		SitesUsingSolution[][] models) {
		SitesUsingSolutionSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new SitesUsingSolutionSoap[models.length][models[0].length];
		}
		else {
			soapModels = new SitesUsingSolutionSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static SitesUsingSolutionSoap[] toSoapModels(
		List<SitesUsingSolution> models) {
		List<SitesUsingSolutionSoap> soapModels = new ArrayList<SitesUsingSolutionSoap>(models.size());

		for (SitesUsingSolution model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new SitesUsingSolutionSoap[soapModels.size()]);
	}

	public SitesUsingSolutionSoap() {
	}

	public long getPrimaryKey() {
		return _sitesUsingSolutionId;
	}

	public void setPrimaryKey(long pk) {
		setSitesUsingSolutionId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getSitesUsingSolutionId() {
		return _sitesUsingSolutionId;
	}

	public void setSitesUsingSolutionId(long sitesUsingSolutionId) {
		_sitesUsingSolutionId = sitesUsingSolutionId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public Date getAsOfDate() {
		return _asOfDate;
	}

	public void setAsOfDate(Date asOfDate) {
		_asOfDate = asOfDate;
	}

	public int getNumber() {
		return _number;
	}

	public void setNumber(int number) {
		_number = number;
	}

	public String getOptionalComments() {
		return _optionalComments;
	}

	public void setOptionalComments(String optionalComments) {
		_optionalComments = optionalComments;
	}

	private String _uuid;
	private long _sitesUsingSolutionId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private Date _asOfDate;
	private int _number;
	private String _optionalComments;
}