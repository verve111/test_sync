/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.exportimport.kernel.lar.StagedModelType;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Support}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Support
 * @generated
 */
@ProviderType
public class SupportWrapper implements Support, ModelWrapper<Support> {
	public SupportWrapper(Support support) {
		_support = support;
	}

	@Override
	public Class<?> getModelClass() {
		return Support.class;
	}

	@Override
	public String getModelClassName() {
		return Support.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("supportId", getSupportId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("startDate", getStartDate());
		attributes.put("duration", getDuration());
		attributes.put("totalValue", getTotalValue());
		attributes.put("sponsorType", getSponsorType());
		attributes.put("supportType", getSupportType());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long supportId = (Long)attributes.get("supportId");

		if (supportId != null) {
			setSupportId(supportId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Date startDate = (Date)attributes.get("startDate");

		if (startDate != null) {
			setStartDate(startDate);
		}

		Integer duration = (Integer)attributes.get("duration");

		if (duration != null) {
			setDuration(duration);
		}

		Double totalValue = (Double)attributes.get("totalValue");

		if (totalValue != null) {
			setTotalValue(totalValue);
		}

		Integer sponsorType = (Integer)attributes.get("sponsorType");

		if (sponsorType != null) {
			setSponsorType(sponsorType);
		}

		Integer supportType = (Integer)attributes.get("supportType");

		if (supportType != null) {
			setSupportType(supportType);
		}
	}

	@Override
	public Support toEscapedModel() {
		return new SupportWrapper(_support.toEscapedModel());
	}

	@Override
	public Support toUnescapedModel() {
		return new SupportWrapper(_support.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _support.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _support.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _support.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _support.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Support> toCacheModel() {
		return _support.toCacheModel();
	}

	/**
	* Returns the total value of this support.
	*
	* @return the total value of this support
	*/
	@Override
	public double getTotalValue() {
		return _support.getTotalValue();
	}

	@Override
	public int compareTo(Support support) {
		return _support.compareTo(support);
	}

	/**
	* Returns the duration of this support.
	*
	* @return the duration of this support
	*/
	@Override
	public int getDuration() {
		return _support.getDuration();
	}

	/**
	* Returns the sponsor type of this support.
	*
	* @return the sponsor type of this support
	*/
	@Override
	public int getSponsorType() {
		return _support.getSponsorType();
	}

	/**
	* Returns the support type of this support.
	*
	* @return the support type of this support
	*/
	@Override
	public int getSupportType() {
		return _support.getSupportType();
	}

	@Override
	public int hashCode() {
		return _support.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _support.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new SupportWrapper((Support)_support.clone());
	}

	/**
	* Returns the user name of this support.
	*
	* @return the user name of this support
	*/
	@Override
	public java.lang.String getUserName() {
		return _support.getUserName();
	}

	/**
	* Returns the user uuid of this support.
	*
	* @return the user uuid of this support
	*/
	@Override
	public java.lang.String getUserUuid() {
		return _support.getUserUuid();
	}

	/**
	* Returns the uuid of this support.
	*
	* @return the uuid of this support
	*/
	@Override
	public java.lang.String getUuid() {
		return _support.getUuid();
	}

	@Override
	public java.lang.String toString() {
		return _support.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _support.toXmlString();
	}

	/**
	* Returns the create date of this support.
	*
	* @return the create date of this support
	*/
	@Override
	public Date getCreateDate() {
		return _support.getCreateDate();
	}

	/**
	* Returns the modified date of this support.
	*
	* @return the modified date of this support
	*/
	@Override
	public Date getModifiedDate() {
		return _support.getModifiedDate();
	}

	/**
	* Returns the start date of this support.
	*
	* @return the start date of this support
	*/
	@Override
	public Date getStartDate() {
		return _support.getStartDate();
	}

	/**
	* Returns the company ID of this support.
	*
	* @return the company ID of this support
	*/
	@Override
	public long getCompanyId() {
		return _support.getCompanyId();
	}

	/**
	* Returns the group ID of this support.
	*
	* @return the group ID of this support
	*/
	@Override
	public long getGroupId() {
		return _support.getGroupId();
	}

	/**
	* Returns the primary key of this support.
	*
	* @return the primary key of this support
	*/
	@Override
	public long getPrimaryKey() {
		return _support.getPrimaryKey();
	}

	/**
	* Returns the support ID of this support.
	*
	* @return the support ID of this support
	*/
	@Override
	public long getSupportId() {
		return _support.getSupportId();
	}

	/**
	* Returns the user ID of this support.
	*
	* @return the user ID of this support
	*/
	@Override
	public long getUserId() {
		return _support.getUserId();
	}

	@Override
	public void persist() {
		_support.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_support.setCachedModel(cachedModel);
	}

	/**
	* Sets the company ID of this support.
	*
	* @param companyId the company ID of this support
	*/
	@Override
	public void setCompanyId(long companyId) {
		_support.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this support.
	*
	* @param createDate the create date of this support
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_support.setCreateDate(createDate);
	}

	/**
	* Sets the duration of this support.
	*
	* @param duration the duration of this support
	*/
	@Override
	public void setDuration(int duration) {
		_support.setDuration(duration);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_support.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_support.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_support.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the group ID of this support.
	*
	* @param groupId the group ID of this support
	*/
	@Override
	public void setGroupId(long groupId) {
		_support.setGroupId(groupId);
	}

	/**
	* Sets the modified date of this support.
	*
	* @param modifiedDate the modified date of this support
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_support.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_support.setNew(n);
	}

	/**
	* Sets the primary key of this support.
	*
	* @param primaryKey the primary key of this support
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_support.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_support.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the sponsor type of this support.
	*
	* @param sponsorType the sponsor type of this support
	*/
	@Override
	public void setSponsorType(int sponsorType) {
		_support.setSponsorType(sponsorType);
	}

	/**
	* Sets the start date of this support.
	*
	* @param startDate the start date of this support
	*/
	@Override
	public void setStartDate(Date startDate) {
		_support.setStartDate(startDate);
	}

	/**
	* Sets the support ID of this support.
	*
	* @param supportId the support ID of this support
	*/
	@Override
	public void setSupportId(long supportId) {
		_support.setSupportId(supportId);
	}

	/**
	* Sets the support type of this support.
	*
	* @param supportType the support type of this support
	*/
	@Override
	public void setSupportType(int supportType) {
		_support.setSupportType(supportType);
	}

	/**
	* Sets the total value of this support.
	*
	* @param totalValue the total value of this support
	*/
	@Override
	public void setTotalValue(double totalValue) {
		_support.setTotalValue(totalValue);
	}

	/**
	* Sets the user ID of this support.
	*
	* @param userId the user ID of this support
	*/
	@Override
	public void setUserId(long userId) {
		_support.setUserId(userId);
	}

	/**
	* Sets the user name of this support.
	*
	* @param userName the user name of this support
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_support.setUserName(userName);
	}

	/**
	* Sets the user uuid of this support.
	*
	* @param userUuid the user uuid of this support
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_support.setUserUuid(userUuid);
	}

	/**
	* Sets the uuid of this support.
	*
	* @param uuid the uuid of this support
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_support.setUuid(uuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof SupportWrapper)) {
			return false;
		}

		SupportWrapper supportWrapper = (SupportWrapper)obj;

		if (Objects.equals(_support, supportWrapper._support)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _support.getStagedModelType();
	}

	@Override
	public Support getWrappedModel() {
		return _support;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _support.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _support.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_support.resetOriginalValues();
	}

	private final Support _support;
}