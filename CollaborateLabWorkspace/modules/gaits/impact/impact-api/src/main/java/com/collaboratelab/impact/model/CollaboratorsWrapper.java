/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.exportimport.kernel.lar.StagedModelType;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Collaborators}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Collaborators
 * @generated
 */
@ProviderType
public class CollaboratorsWrapper implements Collaborators,
	ModelWrapper<Collaborators> {
	public CollaboratorsWrapper(Collaborators collaborators) {
		_collaborators = collaborators;
	}

	@Override
	public Class<?> getModelClass() {
		return Collaborators.class;
	}

	@Override
	public String getModelClassName() {
		return Collaborators.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("collaboratorId", getCollaboratorId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("startDate", getStartDate());
		attributes.put("endDate", getEndDate());
		attributes.put("name", getName());
		attributes.put("URL", getURL());
		attributes.put("type", getType());
		attributes.put("optionalComments", getOptionalComments());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long collaboratorId = (Long)attributes.get("collaboratorId");

		if (collaboratorId != null) {
			setCollaboratorId(collaboratorId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Date startDate = (Date)attributes.get("startDate");

		if (startDate != null) {
			setStartDate(startDate);
		}

		Date endDate = (Date)attributes.get("endDate");

		if (endDate != null) {
			setEndDate(endDate);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String URL = (String)attributes.get("URL");

		if (URL != null) {
			setURL(URL);
		}

		Integer type = (Integer)attributes.get("type");

		if (type != null) {
			setType(type);
		}

		String optionalComments = (String)attributes.get("optionalComments");

		if (optionalComments != null) {
			setOptionalComments(optionalComments);
		}
	}

	@Override
	public Collaborators toEscapedModel() {
		return new CollaboratorsWrapper(_collaborators.toEscapedModel());
	}

	@Override
	public Collaborators toUnescapedModel() {
		return new CollaboratorsWrapper(_collaborators.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _collaborators.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _collaborators.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _collaborators.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _collaborators.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Collaborators> toCacheModel() {
		return _collaborators.toCacheModel();
	}

	@Override
	public int compareTo(Collaborators collaborators) {
		return _collaborators.compareTo(collaborators);
	}

	/**
	* Returns the type of this collaborators.
	*
	* @return the type of this collaborators
	*/
	@Override
	public int getType() {
		return _collaborators.getType();
	}

	@Override
	public int hashCode() {
		return _collaborators.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _collaborators.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new CollaboratorsWrapper((Collaborators)_collaborators.clone());
	}

	/**
	* Returns the name of this collaborators.
	*
	* @return the name of this collaborators
	*/
	@Override
	public java.lang.String getName() {
		return _collaborators.getName();
	}

	/**
	* Returns the optional comments of this collaborators.
	*
	* @return the optional comments of this collaborators
	*/
	@Override
	public java.lang.String getOptionalComments() {
		return _collaborators.getOptionalComments();
	}

	/**
	* Returns the u r l of this collaborators.
	*
	* @return the u r l of this collaborators
	*/
	@Override
	public java.lang.String getURL() {
		return _collaborators.getURL();
	}

	/**
	* Returns the user name of this collaborators.
	*
	* @return the user name of this collaborators
	*/
	@Override
	public java.lang.String getUserName() {
		return _collaborators.getUserName();
	}

	/**
	* Returns the user uuid of this collaborators.
	*
	* @return the user uuid of this collaborators
	*/
	@Override
	public java.lang.String getUserUuid() {
		return _collaborators.getUserUuid();
	}

	/**
	* Returns the uuid of this collaborators.
	*
	* @return the uuid of this collaborators
	*/
	@Override
	public java.lang.String getUuid() {
		return _collaborators.getUuid();
	}

	@Override
	public java.lang.String toString() {
		return _collaborators.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _collaborators.toXmlString();
	}

	/**
	* Returns the create date of this collaborators.
	*
	* @return the create date of this collaborators
	*/
	@Override
	public Date getCreateDate() {
		return _collaborators.getCreateDate();
	}

	/**
	* Returns the end date of this collaborators.
	*
	* @return the end date of this collaborators
	*/
	@Override
	public Date getEndDate() {
		return _collaborators.getEndDate();
	}

	/**
	* Returns the modified date of this collaborators.
	*
	* @return the modified date of this collaborators
	*/
	@Override
	public Date getModifiedDate() {
		return _collaborators.getModifiedDate();
	}

	/**
	* Returns the start date of this collaborators.
	*
	* @return the start date of this collaborators
	*/
	@Override
	public Date getStartDate() {
		return _collaborators.getStartDate();
	}

	/**
	* Returns the collaborator ID of this collaborators.
	*
	* @return the collaborator ID of this collaborators
	*/
	@Override
	public long getCollaboratorId() {
		return _collaborators.getCollaboratorId();
	}

	/**
	* Returns the company ID of this collaborators.
	*
	* @return the company ID of this collaborators
	*/
	@Override
	public long getCompanyId() {
		return _collaborators.getCompanyId();
	}

	/**
	* Returns the group ID of this collaborators.
	*
	* @return the group ID of this collaborators
	*/
	@Override
	public long getGroupId() {
		return _collaborators.getGroupId();
	}

	/**
	* Returns the primary key of this collaborators.
	*
	* @return the primary key of this collaborators
	*/
	@Override
	public long getPrimaryKey() {
		return _collaborators.getPrimaryKey();
	}

	/**
	* Returns the user ID of this collaborators.
	*
	* @return the user ID of this collaborators
	*/
	@Override
	public long getUserId() {
		return _collaborators.getUserId();
	}

	@Override
	public void persist() {
		_collaborators.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_collaborators.setCachedModel(cachedModel);
	}

	/**
	* Sets the collaborator ID of this collaborators.
	*
	* @param collaboratorId the collaborator ID of this collaborators
	*/
	@Override
	public void setCollaboratorId(long collaboratorId) {
		_collaborators.setCollaboratorId(collaboratorId);
	}

	/**
	* Sets the company ID of this collaborators.
	*
	* @param companyId the company ID of this collaborators
	*/
	@Override
	public void setCompanyId(long companyId) {
		_collaborators.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this collaborators.
	*
	* @param createDate the create date of this collaborators
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_collaborators.setCreateDate(createDate);
	}

	/**
	* Sets the end date of this collaborators.
	*
	* @param endDate the end date of this collaborators
	*/
	@Override
	public void setEndDate(Date endDate) {
		_collaborators.setEndDate(endDate);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_collaborators.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_collaborators.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_collaborators.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the group ID of this collaborators.
	*
	* @param groupId the group ID of this collaborators
	*/
	@Override
	public void setGroupId(long groupId) {
		_collaborators.setGroupId(groupId);
	}

	/**
	* Sets the modified date of this collaborators.
	*
	* @param modifiedDate the modified date of this collaborators
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_collaborators.setModifiedDate(modifiedDate);
	}

	/**
	* Sets the name of this collaborators.
	*
	* @param name the name of this collaborators
	*/
	@Override
	public void setName(java.lang.String name) {
		_collaborators.setName(name);
	}

	@Override
	public void setNew(boolean n) {
		_collaborators.setNew(n);
	}

	/**
	* Sets the optional comments of this collaborators.
	*
	* @param optionalComments the optional comments of this collaborators
	*/
	@Override
	public void setOptionalComments(java.lang.String optionalComments) {
		_collaborators.setOptionalComments(optionalComments);
	}

	/**
	* Sets the primary key of this collaborators.
	*
	* @param primaryKey the primary key of this collaborators
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_collaborators.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_collaborators.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the start date of this collaborators.
	*
	* @param startDate the start date of this collaborators
	*/
	@Override
	public void setStartDate(Date startDate) {
		_collaborators.setStartDate(startDate);
	}

	/**
	* Sets the type of this collaborators.
	*
	* @param type the type of this collaborators
	*/
	@Override
	public void setType(int type) {
		_collaborators.setType(type);
	}

	/**
	* Sets the u r l of this collaborators.
	*
	* @param URL the u r l of this collaborators
	*/
	@Override
	public void setURL(java.lang.String URL) {
		_collaborators.setURL(URL);
	}

	/**
	* Sets the user ID of this collaborators.
	*
	* @param userId the user ID of this collaborators
	*/
	@Override
	public void setUserId(long userId) {
		_collaborators.setUserId(userId);
	}

	/**
	* Sets the user name of this collaborators.
	*
	* @param userName the user name of this collaborators
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_collaborators.setUserName(userName);
	}

	/**
	* Sets the user uuid of this collaborators.
	*
	* @param userUuid the user uuid of this collaborators
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_collaborators.setUserUuid(userUuid);
	}

	/**
	* Sets the uuid of this collaborators.
	*
	* @param uuid the uuid of this collaborators
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_collaborators.setUuid(uuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CollaboratorsWrapper)) {
			return false;
		}

		CollaboratorsWrapper collaboratorsWrapper = (CollaboratorsWrapper)obj;

		if (Objects.equals(_collaborators, collaboratorsWrapper._collaborators)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _collaborators.getStagedModelType();
	}

	@Override
	public Collaborators getWrappedModel() {
		return _collaborators;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _collaborators.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _collaborators.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_collaborators.resetOriginalValues();
	}

	private final Collaborators _collaborators;
}