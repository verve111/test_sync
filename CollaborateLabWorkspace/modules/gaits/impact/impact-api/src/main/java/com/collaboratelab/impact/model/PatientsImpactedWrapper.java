/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.exportimport.kernel.lar.StagedModelType;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link PatientsImpacted}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see PatientsImpacted
 * @generated
 */
@ProviderType
public class PatientsImpactedWrapper implements PatientsImpacted,
	ModelWrapper<PatientsImpacted> {
	public PatientsImpactedWrapper(PatientsImpacted patientsImpacted) {
		_patientsImpacted = patientsImpacted;
	}

	@Override
	public Class<?> getModelClass() {
		return PatientsImpacted.class;
	}

	@Override
	public String getModelClassName() {
		return PatientsImpacted.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("patientImpactedId", getPatientImpactedId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("asOfDate", getAsOfDate());
		attributes.put("rangeEstimate", getRangeEstimate());
		attributes.put("population", getPopulation());
		attributes.put("optionalComments", getOptionalComments());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long patientImpactedId = (Long)attributes.get("patientImpactedId");

		if (patientImpactedId != null) {
			setPatientImpactedId(patientImpactedId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Date asOfDate = (Date)attributes.get("asOfDate");

		if (asOfDate != null) {
			setAsOfDate(asOfDate);
		}

		Integer rangeEstimate = (Integer)attributes.get("rangeEstimate");

		if (rangeEstimate != null) {
			setRangeEstimate(rangeEstimate);
		}

		Integer population = (Integer)attributes.get("population");

		if (population != null) {
			setPopulation(population);
		}

		String optionalComments = (String)attributes.get("optionalComments");

		if (optionalComments != null) {
			setOptionalComments(optionalComments);
		}
	}

	@Override
	public PatientsImpacted toEscapedModel() {
		return new PatientsImpactedWrapper(_patientsImpacted.toEscapedModel());
	}

	@Override
	public PatientsImpacted toUnescapedModel() {
		return new PatientsImpactedWrapper(_patientsImpacted.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _patientsImpacted.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _patientsImpacted.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _patientsImpacted.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _patientsImpacted.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<PatientsImpacted> toCacheModel() {
		return _patientsImpacted.toCacheModel();
	}

	@Override
	public int compareTo(PatientsImpacted patientsImpacted) {
		return _patientsImpacted.compareTo(patientsImpacted);
	}

	/**
	* Returns the population of this patients impacted.
	*
	* @return the population of this patients impacted
	*/
	@Override
	public int getPopulation() {
		return _patientsImpacted.getPopulation();
	}

	/**
	* Returns the range estimate of this patients impacted.
	*
	* @return the range estimate of this patients impacted
	*/
	@Override
	public int getRangeEstimate() {
		return _patientsImpacted.getRangeEstimate();
	}

	@Override
	public int hashCode() {
		return _patientsImpacted.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _patientsImpacted.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new PatientsImpactedWrapper((PatientsImpacted)_patientsImpacted.clone());
	}

	/**
	* Returns the optional comments of this patients impacted.
	*
	* @return the optional comments of this patients impacted
	*/
	@Override
	public java.lang.String getOptionalComments() {
		return _patientsImpacted.getOptionalComments();
	}

	/**
	* Returns the user name of this patients impacted.
	*
	* @return the user name of this patients impacted
	*/
	@Override
	public java.lang.String getUserName() {
		return _patientsImpacted.getUserName();
	}

	/**
	* Returns the user uuid of this patients impacted.
	*
	* @return the user uuid of this patients impacted
	*/
	@Override
	public java.lang.String getUserUuid() {
		return _patientsImpacted.getUserUuid();
	}

	/**
	* Returns the uuid of this patients impacted.
	*
	* @return the uuid of this patients impacted
	*/
	@Override
	public java.lang.String getUuid() {
		return _patientsImpacted.getUuid();
	}

	@Override
	public java.lang.String toString() {
		return _patientsImpacted.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _patientsImpacted.toXmlString();
	}

	/**
	* Returns the as of date of this patients impacted.
	*
	* @return the as of date of this patients impacted
	*/
	@Override
	public Date getAsOfDate() {
		return _patientsImpacted.getAsOfDate();
	}

	/**
	* Returns the create date of this patients impacted.
	*
	* @return the create date of this patients impacted
	*/
	@Override
	public Date getCreateDate() {
		return _patientsImpacted.getCreateDate();
	}

	/**
	* Returns the modified date of this patients impacted.
	*
	* @return the modified date of this patients impacted
	*/
	@Override
	public Date getModifiedDate() {
		return _patientsImpacted.getModifiedDate();
	}

	/**
	* Returns the company ID of this patients impacted.
	*
	* @return the company ID of this patients impacted
	*/
	@Override
	public long getCompanyId() {
		return _patientsImpacted.getCompanyId();
	}

	/**
	* Returns the group ID of this patients impacted.
	*
	* @return the group ID of this patients impacted
	*/
	@Override
	public long getGroupId() {
		return _patientsImpacted.getGroupId();
	}

	/**
	* Returns the patient impacted ID of this patients impacted.
	*
	* @return the patient impacted ID of this patients impacted
	*/
	@Override
	public long getPatientImpactedId() {
		return _patientsImpacted.getPatientImpactedId();
	}

	/**
	* Returns the primary key of this patients impacted.
	*
	* @return the primary key of this patients impacted
	*/
	@Override
	public long getPrimaryKey() {
		return _patientsImpacted.getPrimaryKey();
	}

	/**
	* Returns the user ID of this patients impacted.
	*
	* @return the user ID of this patients impacted
	*/
	@Override
	public long getUserId() {
		return _patientsImpacted.getUserId();
	}

	@Override
	public void persist() {
		_patientsImpacted.persist();
	}

	/**
	* Sets the as of date of this patients impacted.
	*
	* @param asOfDate the as of date of this patients impacted
	*/
	@Override
	public void setAsOfDate(Date asOfDate) {
		_patientsImpacted.setAsOfDate(asOfDate);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_patientsImpacted.setCachedModel(cachedModel);
	}

	/**
	* Sets the company ID of this patients impacted.
	*
	* @param companyId the company ID of this patients impacted
	*/
	@Override
	public void setCompanyId(long companyId) {
		_patientsImpacted.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this patients impacted.
	*
	* @param createDate the create date of this patients impacted
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_patientsImpacted.setCreateDate(createDate);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_patientsImpacted.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_patientsImpacted.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_patientsImpacted.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the group ID of this patients impacted.
	*
	* @param groupId the group ID of this patients impacted
	*/
	@Override
	public void setGroupId(long groupId) {
		_patientsImpacted.setGroupId(groupId);
	}

	/**
	* Sets the modified date of this patients impacted.
	*
	* @param modifiedDate the modified date of this patients impacted
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_patientsImpacted.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_patientsImpacted.setNew(n);
	}

	/**
	* Sets the optional comments of this patients impacted.
	*
	* @param optionalComments the optional comments of this patients impacted
	*/
	@Override
	public void setOptionalComments(java.lang.String optionalComments) {
		_patientsImpacted.setOptionalComments(optionalComments);
	}

	/**
	* Sets the patient impacted ID of this patients impacted.
	*
	* @param patientImpactedId the patient impacted ID of this patients impacted
	*/
	@Override
	public void setPatientImpactedId(long patientImpactedId) {
		_patientsImpacted.setPatientImpactedId(patientImpactedId);
	}

	/**
	* Sets the population of this patients impacted.
	*
	* @param population the population of this patients impacted
	*/
	@Override
	public void setPopulation(int population) {
		_patientsImpacted.setPopulation(population);
	}

	/**
	* Sets the primary key of this patients impacted.
	*
	* @param primaryKey the primary key of this patients impacted
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_patientsImpacted.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_patientsImpacted.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the range estimate of this patients impacted.
	*
	* @param rangeEstimate the range estimate of this patients impacted
	*/
	@Override
	public void setRangeEstimate(int rangeEstimate) {
		_patientsImpacted.setRangeEstimate(rangeEstimate);
	}

	/**
	* Sets the user ID of this patients impacted.
	*
	* @param userId the user ID of this patients impacted
	*/
	@Override
	public void setUserId(long userId) {
		_patientsImpacted.setUserId(userId);
	}

	/**
	* Sets the user name of this patients impacted.
	*
	* @param userName the user name of this patients impacted
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_patientsImpacted.setUserName(userName);
	}

	/**
	* Sets the user uuid of this patients impacted.
	*
	* @param userUuid the user uuid of this patients impacted
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_patientsImpacted.setUserUuid(userUuid);
	}

	/**
	* Sets the uuid of this patients impacted.
	*
	* @param uuid the uuid of this patients impacted
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_patientsImpacted.setUuid(uuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PatientsImpactedWrapper)) {
			return false;
		}

		PatientsImpactedWrapper patientsImpactedWrapper = (PatientsImpactedWrapper)obj;

		if (Objects.equals(_patientsImpacted,
					patientsImpactedWrapper._patientsImpacted)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _patientsImpacted.getStagedModelType();
	}

	@Override
	public PatientsImpacted getWrappedModel() {
		return _patientsImpacted;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _patientsImpacted.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _patientsImpacted.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_patientsImpacted.resetOriginalValues();
	}

	private final PatientsImpacted _patientsImpacted;
}