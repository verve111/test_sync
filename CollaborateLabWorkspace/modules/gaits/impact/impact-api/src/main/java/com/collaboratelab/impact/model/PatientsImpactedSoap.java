/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.collaboratelab.impact.service.http.PatientsImpactedServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see com.collaboratelab.impact.service.http.PatientsImpactedServiceSoap
 * @generated
 */
@ProviderType
public class PatientsImpactedSoap implements Serializable {
	public static PatientsImpactedSoap toSoapModel(PatientsImpacted model) {
		PatientsImpactedSoap soapModel = new PatientsImpactedSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setPatientImpactedId(model.getPatientImpactedId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setAsOfDate(model.getAsOfDate());
		soapModel.setRangeEstimate(model.getRangeEstimate());
		soapModel.setPopulation(model.getPopulation());
		soapModel.setOptionalComments(model.getOptionalComments());

		return soapModel;
	}

	public static PatientsImpactedSoap[] toSoapModels(PatientsImpacted[] models) {
		PatientsImpactedSoap[] soapModels = new PatientsImpactedSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static PatientsImpactedSoap[][] toSoapModels(
		PatientsImpacted[][] models) {
		PatientsImpactedSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new PatientsImpactedSoap[models.length][models[0].length];
		}
		else {
			soapModels = new PatientsImpactedSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static PatientsImpactedSoap[] toSoapModels(
		List<PatientsImpacted> models) {
		List<PatientsImpactedSoap> soapModels = new ArrayList<PatientsImpactedSoap>(models.size());

		for (PatientsImpacted model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new PatientsImpactedSoap[soapModels.size()]);
	}

	public PatientsImpactedSoap() {
	}

	public long getPrimaryKey() {
		return _patientImpactedId;
	}

	public void setPrimaryKey(long pk) {
		setPatientImpactedId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getPatientImpactedId() {
		return _patientImpactedId;
	}

	public void setPatientImpactedId(long patientImpactedId) {
		_patientImpactedId = patientImpactedId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public Date getAsOfDate() {
		return _asOfDate;
	}

	public void setAsOfDate(Date asOfDate) {
		_asOfDate = asOfDate;
	}

	public int getRangeEstimate() {
		return _rangeEstimate;
	}

	public void setRangeEstimate(int rangeEstimate) {
		_rangeEstimate = rangeEstimate;
	}

	public int getPopulation() {
		return _population;
	}

	public void setPopulation(int population) {
		_population = population;
	}

	public String getOptionalComments() {
		return _optionalComments;
	}

	public void setOptionalComments(String optionalComments) {
		_optionalComments = optionalComments;
	}

	private String _uuid;
	private long _patientImpactedId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private Date _asOfDate;
	private int _rangeEstimate;
	private int _population;
	private String _optionalComments;
}