/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.collaboratelab.impact.service.http.SupportServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see com.collaboratelab.impact.service.http.SupportServiceSoap
 * @generated
 */
@ProviderType
public class SupportSoap implements Serializable {
	public static SupportSoap toSoapModel(Support model) {
		SupportSoap soapModel = new SupportSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setSupportId(model.getSupportId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setStartDate(model.getStartDate());
		soapModel.setDuration(model.getDuration());
		soapModel.setTotalValue(model.getTotalValue());
		soapModel.setSponsorType(model.getSponsorType());
		soapModel.setSupportType(model.getSupportType());

		return soapModel;
	}

	public static SupportSoap[] toSoapModels(Support[] models) {
		SupportSoap[] soapModels = new SupportSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static SupportSoap[][] toSoapModels(Support[][] models) {
		SupportSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new SupportSoap[models.length][models[0].length];
		}
		else {
			soapModels = new SupportSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static SupportSoap[] toSoapModels(List<Support> models) {
		List<SupportSoap> soapModels = new ArrayList<SupportSoap>(models.size());

		for (Support model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new SupportSoap[soapModels.size()]);
	}

	public SupportSoap() {
	}

	public long getPrimaryKey() {
		return _supportId;
	}

	public void setPrimaryKey(long pk) {
		setSupportId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getSupportId() {
		return _supportId;
	}

	public void setSupportId(long supportId) {
		_supportId = supportId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public Date getStartDate() {
		return _startDate;
	}

	public void setStartDate(Date startDate) {
		_startDate = startDate;
	}

	public int getDuration() {
		return _duration;
	}

	public void setDuration(int duration) {
		_duration = duration;
	}

	public double getTotalValue() {
		return _totalValue;
	}

	public void setTotalValue(double totalValue) {
		_totalValue = totalValue;
	}

	public int getSponsorType() {
		return _sponsorType;
	}

	public void setSponsorType(int sponsorType) {
		_sponsorType = sponsorType;
	}

	public int getSupportType() {
		return _supportType;
	}

	public void setSupportType(int supportType) {
		_supportType = supportType;
	}

	private String _uuid;
	private long _supportId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private Date _startDate;
	private int _duration;
	private double _totalValue;
	private int _sponsorType;
	private int _supportType;
}