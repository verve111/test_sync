/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for Publications. This utility wraps
 * {@link com.collaboratelab.impact.service.impl.PublicationsLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see PublicationsLocalService
 * @see com.collaboratelab.impact.service.base.PublicationsLocalServiceBaseImpl
 * @see com.collaboratelab.impact.service.impl.PublicationsLocalServiceImpl
 * @generated
 */
@ProviderType
public class PublicationsLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.collaboratelab.impact.service.impl.PublicationsLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the publications to the database. Also notifies the appropriate model listeners.
	*
	* @param publications the publications
	* @return the publications that was added
	*/
	public static com.collaboratelab.impact.model.Publications addPublications(
		com.collaboratelab.impact.model.Publications publications) {
		return getService().addPublications(publications);
	}

	/**
	* Creates a new publications with the primary key. Does not add the publications to the database.
	*
	* @param publicationsId the primary key for the new publications
	* @return the new publications
	*/
	public static com.collaboratelab.impact.model.Publications createPublications(
		long publicationsId) {
		return getService().createPublications(publicationsId);
	}

	/**
	* Deletes the publications from the database. Also notifies the appropriate model listeners.
	*
	* @param publications the publications
	* @return the publications that was removed
	*/
	public static com.collaboratelab.impact.model.Publications deletePublications(
		com.collaboratelab.impact.model.Publications publications) {
		return getService().deletePublications(publications);
	}

	/**
	* Deletes the publications with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param publicationsId the primary key of the publications
	* @return the publications that was removed
	* @throws PortalException if a publications with the primary key could not be found
	*/
	public static com.collaboratelab.impact.model.Publications deletePublications(
		long publicationsId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePublications(publicationsId);
	}

	public static com.collaboratelab.impact.model.Publications fetchPublications(
		long publicationsId) {
		return getService().fetchPublications(publicationsId);
	}

	/**
	* Returns the publications matching the UUID and group.
	*
	* @param uuid the publications's UUID
	* @param groupId the primary key of the group
	* @return the matching publications, or <code>null</code> if a matching publications could not be found
	*/
	public static com.collaboratelab.impact.model.Publications fetchPublicationsByUuidAndGroupId(
		java.lang.String uuid, long groupId) {
		return getService().fetchPublicationsByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Returns the publications with the primary key.
	*
	* @param publicationsId the primary key of the publications
	* @return the publications
	* @throws PortalException if a publications with the primary key could not be found
	*/
	public static com.collaboratelab.impact.model.Publications getPublications(
		long publicationsId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPublications(publicationsId);
	}

	/**
	* Returns the publications matching the UUID and group.
	*
	* @param uuid the publications's UUID
	* @param groupId the primary key of the group
	* @return the matching publications
	* @throws PortalException if a matching publications could not be found
	*/
	public static com.collaboratelab.impact.model.Publications getPublicationsByUuidAndGroupId(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPublicationsByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Updates the publications in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param publications the publications
	* @return the publications that was updated
	*/
	public static com.collaboratelab.impact.model.Publications updatePublications(
		com.collaboratelab.impact.model.Publications publications) {
		return getService().updatePublications(publications);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery getExportActionableDynamicQuery(
		com.liferay.exportimport.kernel.lar.PortletDataContext portletDataContext) {
		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of publicationses.
	*
	* @return the number of publicationses
	*/
	public static int getPublicationsesCount() {
		return getService().getPublicationsesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.impact.model.impl.PublicationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.impact.model.impl.PublicationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	public static java.util.List<com.collaboratelab.impact.model.Publications> findByGroupId(
		long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByGroupId(groupId);
	}

	/**
	* Returns a range of all the publicationses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.impact.model.impl.PublicationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of publicationses
	* @param end the upper bound of the range of publicationses (not inclusive)
	* @return the range of publicationses
	*/
	public static java.util.List<com.collaboratelab.impact.model.Publications> getPublicationses(
		int start, int end) {
		return getService().getPublicationses(start, end);
	}

	/**
	* Returns all the publicationses matching the UUID and company.
	*
	* @param uuid the UUID of the publicationses
	* @param companyId the primary key of the company
	* @return the matching publicationses, or an empty list if no matches were found
	*/
	public static java.util.List<com.collaboratelab.impact.model.Publications> getPublicationsesByUuidAndCompanyId(
		java.lang.String uuid, long companyId) {
		return getService().getPublicationsesByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Returns a range of publicationses matching the UUID and company.
	*
	* @param uuid the UUID of the publicationses
	* @param companyId the primary key of the company
	* @param start the lower bound of the range of publicationses
	* @param end the upper bound of the range of publicationses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the range of matching publicationses, or an empty list if no matches were found
	*/
	public static java.util.List<com.collaboratelab.impact.model.Publications> getPublicationsesByUuidAndCompanyId(
		java.lang.String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<com.collaboratelab.impact.model.Publications> orderByComparator) {
		return getService()
				   .getPublicationsesByUuidAndCompanyId(uuid, companyId, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static PublicationsLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<PublicationsLocalService, PublicationsLocalService> _serviceTracker =
		ServiceTrackerFactory.open(PublicationsLocalService.class);
}