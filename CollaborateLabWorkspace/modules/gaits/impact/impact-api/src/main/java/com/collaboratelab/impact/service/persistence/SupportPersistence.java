/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.impact.exception.NoSuchSupportException;
import com.collaboratelab.impact.model.Support;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the support service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.collaboratelab.impact.service.persistence.impl.SupportPersistenceImpl
 * @see SupportUtil
 * @generated
 */
@ProviderType
public interface SupportPersistence extends BasePersistence<Support> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link SupportUtil} to access the support persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the supports where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching supports
	*/
	public java.util.List<Support> findByUuid(java.lang.String uuid);

	/**
	* Returns a range of all the supports where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SupportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of supports
	* @param end the upper bound of the range of supports (not inclusive)
	* @return the range of matching supports
	*/
	public java.util.List<Support> findByUuid(java.lang.String uuid, int start,
		int end);

	/**
	* Returns an ordered range of all the supports where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SupportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of supports
	* @param end the upper bound of the range of supports (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching supports
	*/
	public java.util.List<Support> findByUuid(java.lang.String uuid, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Support> orderByComparator);

	/**
	* Returns an ordered range of all the supports where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SupportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of supports
	* @param end the upper bound of the range of supports (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching supports
	*/
	public java.util.List<Support> findByUuid(java.lang.String uuid, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Support> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first support in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching support
	* @throws NoSuchSupportException if a matching support could not be found
	*/
	public Support findByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Support> orderByComparator)
		throws NoSuchSupportException;

	/**
	* Returns the first support in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching support, or <code>null</code> if a matching support could not be found
	*/
	public Support fetchByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Support> orderByComparator);

	/**
	* Returns the last support in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching support
	* @throws NoSuchSupportException if a matching support could not be found
	*/
	public Support findByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Support> orderByComparator)
		throws NoSuchSupportException;

	/**
	* Returns the last support in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching support, or <code>null</code> if a matching support could not be found
	*/
	public Support fetchByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Support> orderByComparator);

	/**
	* Returns the supports before and after the current support in the ordered set where uuid = &#63;.
	*
	* @param supportId the primary key of the current support
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next support
	* @throws NoSuchSupportException if a support with the primary key could not be found
	*/
	public Support[] findByUuid_PrevAndNext(long supportId,
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Support> orderByComparator)
		throws NoSuchSupportException;

	/**
	* Removes all the supports where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public void removeByUuid(java.lang.String uuid);

	/**
	* Returns the number of supports where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching supports
	*/
	public int countByUuid(java.lang.String uuid);

	/**
	* Returns the support where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchSupportException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching support
	* @throws NoSuchSupportException if a matching support could not be found
	*/
	public Support findByUUID_G(java.lang.String uuid, long groupId)
		throws NoSuchSupportException;

	/**
	* Returns the support where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching support, or <code>null</code> if a matching support could not be found
	*/
	public Support fetchByUUID_G(java.lang.String uuid, long groupId);

	/**
	* Returns the support where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching support, or <code>null</code> if a matching support could not be found
	*/
	public Support fetchByUUID_G(java.lang.String uuid, long groupId,
		boolean retrieveFromCache);

	/**
	* Removes the support where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the support that was removed
	*/
	public Support removeByUUID_G(java.lang.String uuid, long groupId)
		throws NoSuchSupportException;

	/**
	* Returns the number of supports where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching supports
	*/
	public int countByUUID_G(java.lang.String uuid, long groupId);

	/**
	* Returns all the supports where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching supports
	*/
	public java.util.List<Support> findByUuid_C(java.lang.String uuid,
		long companyId);

	/**
	* Returns a range of all the supports where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SupportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of supports
	* @param end the upper bound of the range of supports (not inclusive)
	* @return the range of matching supports
	*/
	public java.util.List<Support> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end);

	/**
	* Returns an ordered range of all the supports where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SupportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of supports
	* @param end the upper bound of the range of supports (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching supports
	*/
	public java.util.List<Support> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Support> orderByComparator);

	/**
	* Returns an ordered range of all the supports where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SupportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of supports
	* @param end the upper bound of the range of supports (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching supports
	*/
	public java.util.List<Support> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Support> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first support in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching support
	* @throws NoSuchSupportException if a matching support could not be found
	*/
	public Support findByUuid_C_First(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Support> orderByComparator)
		throws NoSuchSupportException;

	/**
	* Returns the first support in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching support, or <code>null</code> if a matching support could not be found
	*/
	public Support fetchByUuid_C_First(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Support> orderByComparator);

	/**
	* Returns the last support in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching support
	* @throws NoSuchSupportException if a matching support could not be found
	*/
	public Support findByUuid_C_Last(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Support> orderByComparator)
		throws NoSuchSupportException;

	/**
	* Returns the last support in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching support, or <code>null</code> if a matching support could not be found
	*/
	public Support fetchByUuid_C_Last(java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Support> orderByComparator);

	/**
	* Returns the supports before and after the current support in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param supportId the primary key of the current support
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next support
	* @throws NoSuchSupportException if a support with the primary key could not be found
	*/
	public Support[] findByUuid_C_PrevAndNext(long supportId,
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Support> orderByComparator)
		throws NoSuchSupportException;

	/**
	* Removes all the supports where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public void removeByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the number of supports where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching supports
	*/
	public int countByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the support where supportId = &#63; or throws a {@link NoSuchSupportException} if it could not be found.
	*
	* @param supportId the support ID
	* @return the matching support
	* @throws NoSuchSupportException if a matching support could not be found
	*/
	public Support findBySupportId(long supportId)
		throws NoSuchSupportException;

	/**
	* Returns the support where supportId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param supportId the support ID
	* @return the matching support, or <code>null</code> if a matching support could not be found
	*/
	public Support fetchBySupportId(long supportId);

	/**
	* Returns the support where supportId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param supportId the support ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching support, or <code>null</code> if a matching support could not be found
	*/
	public Support fetchBySupportId(long supportId, boolean retrieveFromCache);

	/**
	* Removes the support where supportId = &#63; from the database.
	*
	* @param supportId the support ID
	* @return the support that was removed
	*/
	public Support removeBySupportId(long supportId)
		throws NoSuchSupportException;

	/**
	* Returns the number of supports where supportId = &#63;.
	*
	* @param supportId the support ID
	* @return the number of matching supports
	*/
	public int countBySupportId(long supportId);

	/**
	* Returns all the supports where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the matching supports
	*/
	public java.util.List<Support> findByGroupId(long groupId);

	/**
	* Returns a range of all the supports where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SupportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of supports
	* @param end the upper bound of the range of supports (not inclusive)
	* @return the range of matching supports
	*/
	public java.util.List<Support> findByGroupId(long groupId, int start,
		int end);

	/**
	* Returns an ordered range of all the supports where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SupportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of supports
	* @param end the upper bound of the range of supports (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching supports
	*/
	public java.util.List<Support> findByGroupId(long groupId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Support> orderByComparator);

	/**
	* Returns an ordered range of all the supports where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SupportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of supports
	* @param end the upper bound of the range of supports (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching supports
	*/
	public java.util.List<Support> findByGroupId(long groupId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Support> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first support in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching support
	* @throws NoSuchSupportException if a matching support could not be found
	*/
	public Support findByGroupId_First(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Support> orderByComparator)
		throws NoSuchSupportException;

	/**
	* Returns the first support in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching support, or <code>null</code> if a matching support could not be found
	*/
	public Support fetchByGroupId_First(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Support> orderByComparator);

	/**
	* Returns the last support in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching support
	* @throws NoSuchSupportException if a matching support could not be found
	*/
	public Support findByGroupId_Last(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Support> orderByComparator)
		throws NoSuchSupportException;

	/**
	* Returns the last support in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching support, or <code>null</code> if a matching support could not be found
	*/
	public Support fetchByGroupId_Last(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Support> orderByComparator);

	/**
	* Returns the supports before and after the current support in the ordered set where groupId = &#63;.
	*
	* @param supportId the primary key of the current support
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next support
	* @throws NoSuchSupportException if a support with the primary key could not be found
	*/
	public Support[] findByGroupId_PrevAndNext(long supportId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Support> orderByComparator)
		throws NoSuchSupportException;

	/**
	* Removes all the supports where groupId = &#63; from the database.
	*
	* @param groupId the group ID
	*/
	public void removeByGroupId(long groupId);

	/**
	* Returns the number of supports where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the number of matching supports
	*/
	public int countByGroupId(long groupId);

	/**
	* Caches the support in the entity cache if it is enabled.
	*
	* @param support the support
	*/
	public void cacheResult(Support support);

	/**
	* Caches the supports in the entity cache if it is enabled.
	*
	* @param supports the supports
	*/
	public void cacheResult(java.util.List<Support> supports);

	/**
	* Creates a new support with the primary key. Does not add the support to the database.
	*
	* @param supportId the primary key for the new support
	* @return the new support
	*/
	public Support create(long supportId);

	/**
	* Removes the support with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param supportId the primary key of the support
	* @return the support that was removed
	* @throws NoSuchSupportException if a support with the primary key could not be found
	*/
	public Support remove(long supportId) throws NoSuchSupportException;

	public Support updateImpl(Support support);

	/**
	* Returns the support with the primary key or throws a {@link NoSuchSupportException} if it could not be found.
	*
	* @param supportId the primary key of the support
	* @return the support
	* @throws NoSuchSupportException if a support with the primary key could not be found
	*/
	public Support findByPrimaryKey(long supportId)
		throws NoSuchSupportException;

	/**
	* Returns the support with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param supportId the primary key of the support
	* @return the support, or <code>null</code> if a support with the primary key could not be found
	*/
	public Support fetchByPrimaryKey(long supportId);

	@Override
	public java.util.Map<java.io.Serializable, Support> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the supports.
	*
	* @return the supports
	*/
	public java.util.List<Support> findAll();

	/**
	* Returns a range of all the supports.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SupportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of supports
	* @param end the upper bound of the range of supports (not inclusive)
	* @return the range of supports
	*/
	public java.util.List<Support> findAll(int start, int end);

	/**
	* Returns an ordered range of all the supports.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SupportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of supports
	* @param end the upper bound of the range of supports (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of supports
	*/
	public java.util.List<Support> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Support> orderByComparator);

	/**
	* Returns an ordered range of all the supports.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SupportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of supports
	* @param end the upper bound of the range of supports (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of supports
	*/
	public java.util.List<Support> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Support> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the supports from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of supports.
	*
	* @return the number of supports
	*/
	public int countAll();

	@Override
	public java.util.Set<java.lang.String> getBadColumnNames();
}