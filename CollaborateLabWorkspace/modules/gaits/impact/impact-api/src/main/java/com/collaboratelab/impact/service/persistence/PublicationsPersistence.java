/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.impact.exception.NoSuchPublicationsException;
import com.collaboratelab.impact.model.Publications;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the publications service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.collaboratelab.impact.service.persistence.impl.PublicationsPersistenceImpl
 * @see PublicationsUtil
 * @generated
 */
@ProviderType
public interface PublicationsPersistence extends BasePersistence<Publications> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link PublicationsUtil} to access the publications persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the publicationses where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching publicationses
	*/
	public java.util.List<Publications> findByUuid(java.lang.String uuid);

	/**
	* Returns a range of all the publicationses where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PublicationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of publicationses
	* @param end the upper bound of the range of publicationses (not inclusive)
	* @return the range of matching publicationses
	*/
	public java.util.List<Publications> findByUuid(java.lang.String uuid,
		int start, int end);

	/**
	* Returns an ordered range of all the publicationses where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PublicationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of publicationses
	* @param end the upper bound of the range of publicationses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching publicationses
	*/
	public java.util.List<Publications> findByUuid(java.lang.String uuid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Publications> orderByComparator);

	/**
	* Returns an ordered range of all the publicationses where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PublicationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of publicationses
	* @param end the upper bound of the range of publicationses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching publicationses
	*/
	public java.util.List<Publications> findByUuid(java.lang.String uuid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Publications> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first publications in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching publications
	* @throws NoSuchPublicationsException if a matching publications could not be found
	*/
	public Publications findByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Publications> orderByComparator)
		throws NoSuchPublicationsException;

	/**
	* Returns the first publications in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching publications, or <code>null</code> if a matching publications could not be found
	*/
	public Publications fetchByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Publications> orderByComparator);

	/**
	* Returns the last publications in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching publications
	* @throws NoSuchPublicationsException if a matching publications could not be found
	*/
	public Publications findByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Publications> orderByComparator)
		throws NoSuchPublicationsException;

	/**
	* Returns the last publications in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching publications, or <code>null</code> if a matching publications could not be found
	*/
	public Publications fetchByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Publications> orderByComparator);

	/**
	* Returns the publicationses before and after the current publications in the ordered set where uuid = &#63;.
	*
	* @param publicationsId the primary key of the current publications
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next publications
	* @throws NoSuchPublicationsException if a publications with the primary key could not be found
	*/
	public Publications[] findByUuid_PrevAndNext(long publicationsId,
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Publications> orderByComparator)
		throws NoSuchPublicationsException;

	/**
	* Removes all the publicationses where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public void removeByUuid(java.lang.String uuid);

	/**
	* Returns the number of publicationses where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching publicationses
	*/
	public int countByUuid(java.lang.String uuid);

	/**
	* Returns the publications where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchPublicationsException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching publications
	* @throws NoSuchPublicationsException if a matching publications could not be found
	*/
	public Publications findByUUID_G(java.lang.String uuid, long groupId)
		throws NoSuchPublicationsException;

	/**
	* Returns the publications where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching publications, or <code>null</code> if a matching publications could not be found
	*/
	public Publications fetchByUUID_G(java.lang.String uuid, long groupId);

	/**
	* Returns the publications where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching publications, or <code>null</code> if a matching publications could not be found
	*/
	public Publications fetchByUUID_G(java.lang.String uuid, long groupId,
		boolean retrieveFromCache);

	/**
	* Removes the publications where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the publications that was removed
	*/
	public Publications removeByUUID_G(java.lang.String uuid, long groupId)
		throws NoSuchPublicationsException;

	/**
	* Returns the number of publicationses where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching publicationses
	*/
	public int countByUUID_G(java.lang.String uuid, long groupId);

	/**
	* Returns all the publicationses where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching publicationses
	*/
	public java.util.List<Publications> findByUuid_C(java.lang.String uuid,
		long companyId);

	/**
	* Returns a range of all the publicationses where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PublicationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of publicationses
	* @param end the upper bound of the range of publicationses (not inclusive)
	* @return the range of matching publicationses
	*/
	public java.util.List<Publications> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end);

	/**
	* Returns an ordered range of all the publicationses where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PublicationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of publicationses
	* @param end the upper bound of the range of publicationses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching publicationses
	*/
	public java.util.List<Publications> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Publications> orderByComparator);

	/**
	* Returns an ordered range of all the publicationses where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PublicationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of publicationses
	* @param end the upper bound of the range of publicationses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching publicationses
	*/
	public java.util.List<Publications> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Publications> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first publications in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching publications
	* @throws NoSuchPublicationsException if a matching publications could not be found
	*/
	public Publications findByUuid_C_First(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Publications> orderByComparator)
		throws NoSuchPublicationsException;

	/**
	* Returns the first publications in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching publications, or <code>null</code> if a matching publications could not be found
	*/
	public Publications fetchByUuid_C_First(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Publications> orderByComparator);

	/**
	* Returns the last publications in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching publications
	* @throws NoSuchPublicationsException if a matching publications could not be found
	*/
	public Publications findByUuid_C_Last(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Publications> orderByComparator)
		throws NoSuchPublicationsException;

	/**
	* Returns the last publications in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching publications, or <code>null</code> if a matching publications could not be found
	*/
	public Publications fetchByUuid_C_Last(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Publications> orderByComparator);

	/**
	* Returns the publicationses before and after the current publications in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param publicationsId the primary key of the current publications
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next publications
	* @throws NoSuchPublicationsException if a publications with the primary key could not be found
	*/
	public Publications[] findByUuid_C_PrevAndNext(long publicationsId,
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Publications> orderByComparator)
		throws NoSuchPublicationsException;

	/**
	* Removes all the publicationses where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public void removeByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the number of publicationses where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching publicationses
	*/
	public int countByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the publications where publicationsId = &#63; or throws a {@link NoSuchPublicationsException} if it could not be found.
	*
	* @param publicationsId the publications ID
	* @return the matching publications
	* @throws NoSuchPublicationsException if a matching publications could not be found
	*/
	public Publications findByPublicationsId(long publicationsId)
		throws NoSuchPublicationsException;

	/**
	* Returns the publications where publicationsId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param publicationsId the publications ID
	* @return the matching publications, or <code>null</code> if a matching publications could not be found
	*/
	public Publications fetchByPublicationsId(long publicationsId);

	/**
	* Returns the publications where publicationsId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param publicationsId the publications ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching publications, or <code>null</code> if a matching publications could not be found
	*/
	public Publications fetchByPublicationsId(long publicationsId,
		boolean retrieveFromCache);

	/**
	* Removes the publications where publicationsId = &#63; from the database.
	*
	* @param publicationsId the publications ID
	* @return the publications that was removed
	*/
	public Publications removeByPublicationsId(long publicationsId)
		throws NoSuchPublicationsException;

	/**
	* Returns the number of publicationses where publicationsId = &#63;.
	*
	* @param publicationsId the publications ID
	* @return the number of matching publicationses
	*/
	public int countByPublicationsId(long publicationsId);

	/**
	* Returns all the publicationses where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the matching publicationses
	*/
	public java.util.List<Publications> findByGroupId(long groupId);

	/**
	* Returns a range of all the publicationses where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PublicationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of publicationses
	* @param end the upper bound of the range of publicationses (not inclusive)
	* @return the range of matching publicationses
	*/
	public java.util.List<Publications> findByGroupId(long groupId, int start,
		int end);

	/**
	* Returns an ordered range of all the publicationses where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PublicationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of publicationses
	* @param end the upper bound of the range of publicationses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching publicationses
	*/
	public java.util.List<Publications> findByGroupId(long groupId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Publications> orderByComparator);

	/**
	* Returns an ordered range of all the publicationses where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PublicationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of publicationses
	* @param end the upper bound of the range of publicationses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching publicationses
	*/
	public java.util.List<Publications> findByGroupId(long groupId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Publications> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first publications in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching publications
	* @throws NoSuchPublicationsException if a matching publications could not be found
	*/
	public Publications findByGroupId_First(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Publications> orderByComparator)
		throws NoSuchPublicationsException;

	/**
	* Returns the first publications in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching publications, or <code>null</code> if a matching publications could not be found
	*/
	public Publications fetchByGroupId_First(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Publications> orderByComparator);

	/**
	* Returns the last publications in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching publications
	* @throws NoSuchPublicationsException if a matching publications could not be found
	*/
	public Publications findByGroupId_Last(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Publications> orderByComparator)
		throws NoSuchPublicationsException;

	/**
	* Returns the last publications in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching publications, or <code>null</code> if a matching publications could not be found
	*/
	public Publications fetchByGroupId_Last(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Publications> orderByComparator);

	/**
	* Returns the publicationses before and after the current publications in the ordered set where groupId = &#63;.
	*
	* @param publicationsId the primary key of the current publications
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next publications
	* @throws NoSuchPublicationsException if a publications with the primary key could not be found
	*/
	public Publications[] findByGroupId_PrevAndNext(long publicationsId,
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Publications> orderByComparator)
		throws NoSuchPublicationsException;

	/**
	* Removes all the publicationses where groupId = &#63; from the database.
	*
	* @param groupId the group ID
	*/
	public void removeByGroupId(long groupId);

	/**
	* Returns the number of publicationses where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the number of matching publicationses
	*/
	public int countByGroupId(long groupId);

	/**
	* Caches the publications in the entity cache if it is enabled.
	*
	* @param publications the publications
	*/
	public void cacheResult(Publications publications);

	/**
	* Caches the publicationses in the entity cache if it is enabled.
	*
	* @param publicationses the publicationses
	*/
	public void cacheResult(java.util.List<Publications> publicationses);

	/**
	* Creates a new publications with the primary key. Does not add the publications to the database.
	*
	* @param publicationsId the primary key for the new publications
	* @return the new publications
	*/
	public Publications create(long publicationsId);

	/**
	* Removes the publications with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param publicationsId the primary key of the publications
	* @return the publications that was removed
	* @throws NoSuchPublicationsException if a publications with the primary key could not be found
	*/
	public Publications remove(long publicationsId)
		throws NoSuchPublicationsException;

	public Publications updateImpl(Publications publications);

	/**
	* Returns the publications with the primary key or throws a {@link NoSuchPublicationsException} if it could not be found.
	*
	* @param publicationsId the primary key of the publications
	* @return the publications
	* @throws NoSuchPublicationsException if a publications with the primary key could not be found
	*/
	public Publications findByPrimaryKey(long publicationsId)
		throws NoSuchPublicationsException;

	/**
	* Returns the publications with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param publicationsId the primary key of the publications
	* @return the publications, or <code>null</code> if a publications with the primary key could not be found
	*/
	public Publications fetchByPrimaryKey(long publicationsId);

	@Override
	public java.util.Map<java.io.Serializable, Publications> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the publicationses.
	*
	* @return the publicationses
	*/
	public java.util.List<Publications> findAll();

	/**
	* Returns a range of all the publicationses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PublicationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of publicationses
	* @param end the upper bound of the range of publicationses (not inclusive)
	* @return the range of publicationses
	*/
	public java.util.List<Publications> findAll(int start, int end);

	/**
	* Returns an ordered range of all the publicationses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PublicationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of publicationses
	* @param end the upper bound of the range of publicationses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of publicationses
	*/
	public java.util.List<Publications> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Publications> orderByComparator);

	/**
	* Returns an ordered range of all the publicationses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PublicationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of publicationses
	* @param end the upper bound of the range of publicationses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of publicationses
	*/
	public java.util.List<Publications> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Publications> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the publicationses from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of publicationses.
	*
	* @return the number of publicationses
	*/
	public int countAll();

	@Override
	public java.util.Set<java.lang.String> getBadColumnNames();
}