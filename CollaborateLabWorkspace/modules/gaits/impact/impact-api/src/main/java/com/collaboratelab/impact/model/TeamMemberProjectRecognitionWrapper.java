/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.exportimport.kernel.lar.StagedModelType;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link TeamMemberProjectRecognition}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see TeamMemberProjectRecognition
 * @generated
 */
@ProviderType
public class TeamMemberProjectRecognitionWrapper
	implements TeamMemberProjectRecognition,
		ModelWrapper<TeamMemberProjectRecognition> {
	public TeamMemberProjectRecognitionWrapper(
		TeamMemberProjectRecognition teamMemberProjectRecognition) {
		_teamMemberProjectRecognition = teamMemberProjectRecognition;
	}

	@Override
	public Class<?> getModelClass() {
		return TeamMemberProjectRecognition.class;
	}

	@Override
	public String getModelClassName() {
		return TeamMemberProjectRecognition.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("teamMemberProjectRecognitionId",
			getTeamMemberProjectRecognitionId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("dateOfRecognition", getDateOfRecognition());
		attributes.put("Name", getName());
		attributes.put("CatRating", getCatRating());
		attributes.put("URL", getURL());
		attributes.put("optionalComments", getOptionalComments());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long teamMemberProjectRecognitionId = (Long)attributes.get(
				"teamMemberProjectRecognitionId");

		if (teamMemberProjectRecognitionId != null) {
			setTeamMemberProjectRecognitionId(teamMemberProjectRecognitionId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Date dateOfRecognition = (Date)attributes.get("dateOfRecognition");

		if (dateOfRecognition != null) {
			setDateOfRecognition(dateOfRecognition);
		}

		String Name = (String)attributes.get("Name");

		if (Name != null) {
			setName(Name);
		}

		Integer CatRating = (Integer)attributes.get("CatRating");

		if (CatRating != null) {
			setCatRating(CatRating);
		}

		String URL = (String)attributes.get("URL");

		if (URL != null) {
			setURL(URL);
		}

		String optionalComments = (String)attributes.get("optionalComments");

		if (optionalComments != null) {
			setOptionalComments(optionalComments);
		}
	}

	@Override
	public TeamMemberProjectRecognition toEscapedModel() {
		return new TeamMemberProjectRecognitionWrapper(_teamMemberProjectRecognition.toEscapedModel());
	}

	@Override
	public TeamMemberProjectRecognition toUnescapedModel() {
		return new TeamMemberProjectRecognitionWrapper(_teamMemberProjectRecognition.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _teamMemberProjectRecognition.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _teamMemberProjectRecognition.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _teamMemberProjectRecognition.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _teamMemberProjectRecognition.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<TeamMemberProjectRecognition> toCacheModel() {
		return _teamMemberProjectRecognition.toCacheModel();
	}

	@Override
	public int compareTo(
		TeamMemberProjectRecognition teamMemberProjectRecognition) {
		return _teamMemberProjectRecognition.compareTo(teamMemberProjectRecognition);
	}

	/**
	* Returns the cat rating of this team member project recognition.
	*
	* @return the cat rating of this team member project recognition
	*/
	@Override
	public int getCatRating() {
		return _teamMemberProjectRecognition.getCatRating();
	}

	@Override
	public int hashCode() {
		return _teamMemberProjectRecognition.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _teamMemberProjectRecognition.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new TeamMemberProjectRecognitionWrapper((TeamMemberProjectRecognition)_teamMemberProjectRecognition.clone());
	}

	/**
	* Returns the name of this team member project recognition.
	*
	* @return the name of this team member project recognition
	*/
	@Override
	public java.lang.String getName() {
		return _teamMemberProjectRecognition.getName();
	}

	/**
	* Returns the optional comments of this team member project recognition.
	*
	* @return the optional comments of this team member project recognition
	*/
	@Override
	public java.lang.String getOptionalComments() {
		return _teamMemberProjectRecognition.getOptionalComments();
	}

	/**
	* Returns the u r l of this team member project recognition.
	*
	* @return the u r l of this team member project recognition
	*/
	@Override
	public java.lang.String getURL() {
		return _teamMemberProjectRecognition.getURL();
	}

	/**
	* Returns the user name of this team member project recognition.
	*
	* @return the user name of this team member project recognition
	*/
	@Override
	public java.lang.String getUserName() {
		return _teamMemberProjectRecognition.getUserName();
	}

	/**
	* Returns the user uuid of this team member project recognition.
	*
	* @return the user uuid of this team member project recognition
	*/
	@Override
	public java.lang.String getUserUuid() {
		return _teamMemberProjectRecognition.getUserUuid();
	}

	/**
	* Returns the uuid of this team member project recognition.
	*
	* @return the uuid of this team member project recognition
	*/
	@Override
	public java.lang.String getUuid() {
		return _teamMemberProjectRecognition.getUuid();
	}

	@Override
	public java.lang.String toString() {
		return _teamMemberProjectRecognition.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _teamMemberProjectRecognition.toXmlString();
	}

	/**
	* Returns the create date of this team member project recognition.
	*
	* @return the create date of this team member project recognition
	*/
	@Override
	public Date getCreateDate() {
		return _teamMemberProjectRecognition.getCreateDate();
	}

	/**
	* Returns the date of recognition of this team member project recognition.
	*
	* @return the date of recognition of this team member project recognition
	*/
	@Override
	public Date getDateOfRecognition() {
		return _teamMemberProjectRecognition.getDateOfRecognition();
	}

	/**
	* Returns the modified date of this team member project recognition.
	*
	* @return the modified date of this team member project recognition
	*/
	@Override
	public Date getModifiedDate() {
		return _teamMemberProjectRecognition.getModifiedDate();
	}

	/**
	* Returns the company ID of this team member project recognition.
	*
	* @return the company ID of this team member project recognition
	*/
	@Override
	public long getCompanyId() {
		return _teamMemberProjectRecognition.getCompanyId();
	}

	/**
	* Returns the group ID of this team member project recognition.
	*
	* @return the group ID of this team member project recognition
	*/
	@Override
	public long getGroupId() {
		return _teamMemberProjectRecognition.getGroupId();
	}

	/**
	* Returns the primary key of this team member project recognition.
	*
	* @return the primary key of this team member project recognition
	*/
	@Override
	public long getPrimaryKey() {
		return _teamMemberProjectRecognition.getPrimaryKey();
	}

	/**
	* Returns the team member project recognition ID of this team member project recognition.
	*
	* @return the team member project recognition ID of this team member project recognition
	*/
	@Override
	public long getTeamMemberProjectRecognitionId() {
		return _teamMemberProjectRecognition.getTeamMemberProjectRecognitionId();
	}

	/**
	* Returns the user ID of this team member project recognition.
	*
	* @return the user ID of this team member project recognition
	*/
	@Override
	public long getUserId() {
		return _teamMemberProjectRecognition.getUserId();
	}

	@Override
	public void persist() {
		_teamMemberProjectRecognition.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_teamMemberProjectRecognition.setCachedModel(cachedModel);
	}

	/**
	* Sets the cat rating of this team member project recognition.
	*
	* @param CatRating the cat rating of this team member project recognition
	*/
	@Override
	public void setCatRating(int CatRating) {
		_teamMemberProjectRecognition.setCatRating(CatRating);
	}

	/**
	* Sets the company ID of this team member project recognition.
	*
	* @param companyId the company ID of this team member project recognition
	*/
	@Override
	public void setCompanyId(long companyId) {
		_teamMemberProjectRecognition.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this team member project recognition.
	*
	* @param createDate the create date of this team member project recognition
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_teamMemberProjectRecognition.setCreateDate(createDate);
	}

	/**
	* Sets the date of recognition of this team member project recognition.
	*
	* @param dateOfRecognition the date of recognition of this team member project recognition
	*/
	@Override
	public void setDateOfRecognition(Date dateOfRecognition) {
		_teamMemberProjectRecognition.setDateOfRecognition(dateOfRecognition);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_teamMemberProjectRecognition.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_teamMemberProjectRecognition.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_teamMemberProjectRecognition.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the group ID of this team member project recognition.
	*
	* @param groupId the group ID of this team member project recognition
	*/
	@Override
	public void setGroupId(long groupId) {
		_teamMemberProjectRecognition.setGroupId(groupId);
	}

	/**
	* Sets the modified date of this team member project recognition.
	*
	* @param modifiedDate the modified date of this team member project recognition
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_teamMemberProjectRecognition.setModifiedDate(modifiedDate);
	}

	/**
	* Sets the name of this team member project recognition.
	*
	* @param Name the name of this team member project recognition
	*/
	@Override
	public void setName(java.lang.String Name) {
		_teamMemberProjectRecognition.setName(Name);
	}

	@Override
	public void setNew(boolean n) {
		_teamMemberProjectRecognition.setNew(n);
	}

	/**
	* Sets the optional comments of this team member project recognition.
	*
	* @param optionalComments the optional comments of this team member project recognition
	*/
	@Override
	public void setOptionalComments(java.lang.String optionalComments) {
		_teamMemberProjectRecognition.setOptionalComments(optionalComments);
	}

	/**
	* Sets the primary key of this team member project recognition.
	*
	* @param primaryKey the primary key of this team member project recognition
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_teamMemberProjectRecognition.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_teamMemberProjectRecognition.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the team member project recognition ID of this team member project recognition.
	*
	* @param teamMemberProjectRecognitionId the team member project recognition ID of this team member project recognition
	*/
	@Override
	public void setTeamMemberProjectRecognitionId(
		long teamMemberProjectRecognitionId) {
		_teamMemberProjectRecognition.setTeamMemberProjectRecognitionId(teamMemberProjectRecognitionId);
	}

	/**
	* Sets the u r l of this team member project recognition.
	*
	* @param URL the u r l of this team member project recognition
	*/
	@Override
	public void setURL(java.lang.String URL) {
		_teamMemberProjectRecognition.setURL(URL);
	}

	/**
	* Sets the user ID of this team member project recognition.
	*
	* @param userId the user ID of this team member project recognition
	*/
	@Override
	public void setUserId(long userId) {
		_teamMemberProjectRecognition.setUserId(userId);
	}

	/**
	* Sets the user name of this team member project recognition.
	*
	* @param userName the user name of this team member project recognition
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_teamMemberProjectRecognition.setUserName(userName);
	}

	/**
	* Sets the user uuid of this team member project recognition.
	*
	* @param userUuid the user uuid of this team member project recognition
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_teamMemberProjectRecognition.setUserUuid(userUuid);
	}

	/**
	* Sets the uuid of this team member project recognition.
	*
	* @param uuid the uuid of this team member project recognition
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_teamMemberProjectRecognition.setUuid(uuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof TeamMemberProjectRecognitionWrapper)) {
			return false;
		}

		TeamMemberProjectRecognitionWrapper teamMemberProjectRecognitionWrapper = (TeamMemberProjectRecognitionWrapper)obj;

		if (Objects.equals(_teamMemberProjectRecognition,
					teamMemberProjectRecognitionWrapper._teamMemberProjectRecognition)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _teamMemberProjectRecognition.getStagedModelType();
	}

	@Override
	public TeamMemberProjectRecognition getWrappedModel() {
		return _teamMemberProjectRecognition;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _teamMemberProjectRecognition.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _teamMemberProjectRecognition.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_teamMemberProjectRecognition.resetOriginalValues();
	}

	private final TeamMemberProjectRecognition _teamMemberProjectRecognition;
}