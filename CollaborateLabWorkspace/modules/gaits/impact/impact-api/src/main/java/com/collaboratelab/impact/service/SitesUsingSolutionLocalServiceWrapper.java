/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link SitesUsingSolutionLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see SitesUsingSolutionLocalService
 * @generated
 */
@ProviderType
public class SitesUsingSolutionLocalServiceWrapper
	implements SitesUsingSolutionLocalService,
		ServiceWrapper<SitesUsingSolutionLocalService> {
	public SitesUsingSolutionLocalServiceWrapper(
		SitesUsingSolutionLocalService sitesUsingSolutionLocalService) {
		_sitesUsingSolutionLocalService = sitesUsingSolutionLocalService;
	}

	/**
	* Adds the sites using solution to the database. Also notifies the appropriate model listeners.
	*
	* @param sitesUsingSolution the sites using solution
	* @return the sites using solution that was added
	*/
	@Override
	public com.collaboratelab.impact.model.SitesUsingSolution addSitesUsingSolution(
		com.collaboratelab.impact.model.SitesUsingSolution sitesUsingSolution) {
		return _sitesUsingSolutionLocalService.addSitesUsingSolution(sitesUsingSolution);
	}

	/**
	* Creates a new sites using solution with the primary key. Does not add the sites using solution to the database.
	*
	* @param sitesUsingSolutionId the primary key for the new sites using solution
	* @return the new sites using solution
	*/
	@Override
	public com.collaboratelab.impact.model.SitesUsingSolution createSitesUsingSolution(
		long sitesUsingSolutionId) {
		return _sitesUsingSolutionLocalService.createSitesUsingSolution(sitesUsingSolutionId);
	}

	/**
	* Deletes the sites using solution from the database. Also notifies the appropriate model listeners.
	*
	* @param sitesUsingSolution the sites using solution
	* @return the sites using solution that was removed
	*/
	@Override
	public com.collaboratelab.impact.model.SitesUsingSolution deleteSitesUsingSolution(
		com.collaboratelab.impact.model.SitesUsingSolution sitesUsingSolution) {
		return _sitesUsingSolutionLocalService.deleteSitesUsingSolution(sitesUsingSolution);
	}

	/**
	* Deletes the sites using solution with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param sitesUsingSolutionId the primary key of the sites using solution
	* @return the sites using solution that was removed
	* @throws PortalException if a sites using solution with the primary key could not be found
	*/
	@Override
	public com.collaboratelab.impact.model.SitesUsingSolution deleteSitesUsingSolution(
		long sitesUsingSolutionId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _sitesUsingSolutionLocalService.deleteSitesUsingSolution(sitesUsingSolutionId);
	}

	@Override
	public com.collaboratelab.impact.model.SitesUsingSolution fetchSitesUsingSolution(
		long sitesUsingSolutionId) {
		return _sitesUsingSolutionLocalService.fetchSitesUsingSolution(sitesUsingSolutionId);
	}

	/**
	* Returns the sites using solution matching the UUID and group.
	*
	* @param uuid the sites using solution's UUID
	* @param groupId the primary key of the group
	* @return the matching sites using solution, or <code>null</code> if a matching sites using solution could not be found
	*/
	@Override
	public com.collaboratelab.impact.model.SitesUsingSolution fetchSitesUsingSolutionByUuidAndGroupId(
		java.lang.String uuid, long groupId) {
		return _sitesUsingSolutionLocalService.fetchSitesUsingSolutionByUuidAndGroupId(uuid,
			groupId);
	}

	/**
	* Returns the sites using solution with the primary key.
	*
	* @param sitesUsingSolutionId the primary key of the sites using solution
	* @return the sites using solution
	* @throws PortalException if a sites using solution with the primary key could not be found
	*/
	@Override
	public com.collaboratelab.impact.model.SitesUsingSolution getSitesUsingSolution(
		long sitesUsingSolutionId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _sitesUsingSolutionLocalService.getSitesUsingSolution(sitesUsingSolutionId);
	}

	/**
	* Returns the sites using solution matching the UUID and group.
	*
	* @param uuid the sites using solution's UUID
	* @param groupId the primary key of the group
	* @return the matching sites using solution
	* @throws PortalException if a matching sites using solution could not be found
	*/
	@Override
	public com.collaboratelab.impact.model.SitesUsingSolution getSitesUsingSolutionByUuidAndGroupId(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _sitesUsingSolutionLocalService.getSitesUsingSolutionByUuidAndGroupId(uuid,
			groupId);
	}

	/**
	* Updates the sites using solution in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param sitesUsingSolution the sites using solution
	* @return the sites using solution that was updated
	*/
	@Override
	public com.collaboratelab.impact.model.SitesUsingSolution updateSitesUsingSolution(
		com.collaboratelab.impact.model.SitesUsingSolution sitesUsingSolution) {
		return _sitesUsingSolutionLocalService.updateSitesUsingSolution(sitesUsingSolution);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _sitesUsingSolutionLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _sitesUsingSolutionLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery getExportActionableDynamicQuery(
		com.liferay.exportimport.kernel.lar.PortletDataContext portletDataContext) {
		return _sitesUsingSolutionLocalService.getExportActionableDynamicQuery(portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _sitesUsingSolutionLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _sitesUsingSolutionLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _sitesUsingSolutionLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of sites using solutions.
	*
	* @return the number of sites using solutions
	*/
	@Override
	public int getSitesUsingSolutionsCount() {
		return _sitesUsingSolutionLocalService.getSitesUsingSolutionsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _sitesUsingSolutionLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _sitesUsingSolutionLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.impact.model.impl.SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _sitesUsingSolutionLocalService.dynamicQuery(dynamicQuery,
			start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.impact.model.impl.SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _sitesUsingSolutionLocalService.dynamicQuery(dynamicQuery,
			start, end, orderByComparator);
	}

	@Override
	public java.util.List<com.collaboratelab.impact.model.SitesUsingSolution> findByGroupId(
		long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _sitesUsingSolutionLocalService.findByGroupId(groupId);
	}

	/**
	* Returns a range of all the sites using solutions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.impact.model.impl.SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of sites using solutions
	* @param end the upper bound of the range of sites using solutions (not inclusive)
	* @return the range of sites using solutions
	*/
	@Override
	public java.util.List<com.collaboratelab.impact.model.SitesUsingSolution> getSitesUsingSolutions(
		int start, int end) {
		return _sitesUsingSolutionLocalService.getSitesUsingSolutions(start, end);
	}

	/**
	* Returns all the sites using solutions matching the UUID and company.
	*
	* @param uuid the UUID of the sites using solutions
	* @param companyId the primary key of the company
	* @return the matching sites using solutions, or an empty list if no matches were found
	*/
	@Override
	public java.util.List<com.collaboratelab.impact.model.SitesUsingSolution> getSitesUsingSolutionsByUuidAndCompanyId(
		java.lang.String uuid, long companyId) {
		return _sitesUsingSolutionLocalService.getSitesUsingSolutionsByUuidAndCompanyId(uuid,
			companyId);
	}

	/**
	* Returns a range of sites using solutions matching the UUID and company.
	*
	* @param uuid the UUID of the sites using solutions
	* @param companyId the primary key of the company
	* @param start the lower bound of the range of sites using solutions
	* @param end the upper bound of the range of sites using solutions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the range of matching sites using solutions, or an empty list if no matches were found
	*/
	@Override
	public java.util.List<com.collaboratelab.impact.model.SitesUsingSolution> getSitesUsingSolutionsByUuidAndCompanyId(
		java.lang.String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<com.collaboratelab.impact.model.SitesUsingSolution> orderByComparator) {
		return _sitesUsingSolutionLocalService.getSitesUsingSolutionsByUuidAndCompanyId(uuid,
			companyId, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _sitesUsingSolutionLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _sitesUsingSolutionLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public SitesUsingSolutionLocalService getWrappedService() {
		return _sitesUsingSolutionLocalService;
	}

	@Override
	public void setWrappedService(
		SitesUsingSolutionLocalService sitesUsingSolutionLocalService) {
		_sitesUsingSolutionLocalService = sitesUsingSolutionLocalService;
	}

	private SitesUsingSolutionLocalService _sitesUsingSolutionLocalService;
}