/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.impact.exception.NoSuchSitesUsingSolutionException;
import com.collaboratelab.impact.model.SitesUsingSolution;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the sites using solution service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.collaboratelab.impact.service.persistence.impl.SitesUsingSolutionPersistenceImpl
 * @see SitesUsingSolutionUtil
 * @generated
 */
@ProviderType
public interface SitesUsingSolutionPersistence extends BasePersistence<SitesUsingSolution> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link SitesUsingSolutionUtil} to access the sites using solution persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the sites using solutions where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching sites using solutions
	*/
	public java.util.List<SitesUsingSolution> findByUuid(java.lang.String uuid);

	/**
	* Returns a range of all the sites using solutions where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of sites using solutions
	* @param end the upper bound of the range of sites using solutions (not inclusive)
	* @return the range of matching sites using solutions
	*/
	public java.util.List<SitesUsingSolution> findByUuid(
		java.lang.String uuid, int start, int end);

	/**
	* Returns an ordered range of all the sites using solutions where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of sites using solutions
	* @param end the upper bound of the range of sites using solutions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching sites using solutions
	*/
	public java.util.List<SitesUsingSolution> findByUuid(
		java.lang.String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<SitesUsingSolution> orderByComparator);

	/**
	* Returns an ordered range of all the sites using solutions where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of sites using solutions
	* @param end the upper bound of the range of sites using solutions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching sites using solutions
	*/
	public java.util.List<SitesUsingSolution> findByUuid(
		java.lang.String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<SitesUsingSolution> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first sites using solution in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching sites using solution
	* @throws NoSuchSitesUsingSolutionException if a matching sites using solution could not be found
	*/
	public SitesUsingSolution findByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<SitesUsingSolution> orderByComparator)
		throws NoSuchSitesUsingSolutionException;

	/**
	* Returns the first sites using solution in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching sites using solution, or <code>null</code> if a matching sites using solution could not be found
	*/
	public SitesUsingSolution fetchByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<SitesUsingSolution> orderByComparator);

	/**
	* Returns the last sites using solution in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching sites using solution
	* @throws NoSuchSitesUsingSolutionException if a matching sites using solution could not be found
	*/
	public SitesUsingSolution findByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<SitesUsingSolution> orderByComparator)
		throws NoSuchSitesUsingSolutionException;

	/**
	* Returns the last sites using solution in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching sites using solution, or <code>null</code> if a matching sites using solution could not be found
	*/
	public SitesUsingSolution fetchByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<SitesUsingSolution> orderByComparator);

	/**
	* Returns the sites using solutions before and after the current sites using solution in the ordered set where uuid = &#63;.
	*
	* @param sitesUsingSolutionId the primary key of the current sites using solution
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next sites using solution
	* @throws NoSuchSitesUsingSolutionException if a sites using solution with the primary key could not be found
	*/
	public SitesUsingSolution[] findByUuid_PrevAndNext(
		long sitesUsingSolutionId, java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<SitesUsingSolution> orderByComparator)
		throws NoSuchSitesUsingSolutionException;

	/**
	* Removes all the sites using solutions where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public void removeByUuid(java.lang.String uuid);

	/**
	* Returns the number of sites using solutions where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching sites using solutions
	*/
	public int countByUuid(java.lang.String uuid);

	/**
	* Returns the sites using solution where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchSitesUsingSolutionException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching sites using solution
	* @throws NoSuchSitesUsingSolutionException if a matching sites using solution could not be found
	*/
	public SitesUsingSolution findByUUID_G(java.lang.String uuid, long groupId)
		throws NoSuchSitesUsingSolutionException;

	/**
	* Returns the sites using solution where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching sites using solution, or <code>null</code> if a matching sites using solution could not be found
	*/
	public SitesUsingSolution fetchByUUID_G(java.lang.String uuid, long groupId);

	/**
	* Returns the sites using solution where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching sites using solution, or <code>null</code> if a matching sites using solution could not be found
	*/
	public SitesUsingSolution fetchByUUID_G(java.lang.String uuid,
		long groupId, boolean retrieveFromCache);

	/**
	* Removes the sites using solution where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the sites using solution that was removed
	*/
	public SitesUsingSolution removeByUUID_G(java.lang.String uuid, long groupId)
		throws NoSuchSitesUsingSolutionException;

	/**
	* Returns the number of sites using solutions where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching sites using solutions
	*/
	public int countByUUID_G(java.lang.String uuid, long groupId);

	/**
	* Returns all the sites using solutions where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching sites using solutions
	*/
	public java.util.List<SitesUsingSolution> findByUuid_C(
		java.lang.String uuid, long companyId);

	/**
	* Returns a range of all the sites using solutions where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of sites using solutions
	* @param end the upper bound of the range of sites using solutions (not inclusive)
	* @return the range of matching sites using solutions
	*/
	public java.util.List<SitesUsingSolution> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end);

	/**
	* Returns an ordered range of all the sites using solutions where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of sites using solutions
	* @param end the upper bound of the range of sites using solutions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching sites using solutions
	*/
	public java.util.List<SitesUsingSolution> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<SitesUsingSolution> orderByComparator);

	/**
	* Returns an ordered range of all the sites using solutions where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of sites using solutions
	* @param end the upper bound of the range of sites using solutions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching sites using solutions
	*/
	public java.util.List<SitesUsingSolution> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<SitesUsingSolution> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first sites using solution in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching sites using solution
	* @throws NoSuchSitesUsingSolutionException if a matching sites using solution could not be found
	*/
	public SitesUsingSolution findByUuid_C_First(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<SitesUsingSolution> orderByComparator)
		throws NoSuchSitesUsingSolutionException;

	/**
	* Returns the first sites using solution in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching sites using solution, or <code>null</code> if a matching sites using solution could not be found
	*/
	public SitesUsingSolution fetchByUuid_C_First(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<SitesUsingSolution> orderByComparator);

	/**
	* Returns the last sites using solution in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching sites using solution
	* @throws NoSuchSitesUsingSolutionException if a matching sites using solution could not be found
	*/
	public SitesUsingSolution findByUuid_C_Last(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<SitesUsingSolution> orderByComparator)
		throws NoSuchSitesUsingSolutionException;

	/**
	* Returns the last sites using solution in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching sites using solution, or <code>null</code> if a matching sites using solution could not be found
	*/
	public SitesUsingSolution fetchByUuid_C_Last(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<SitesUsingSolution> orderByComparator);

	/**
	* Returns the sites using solutions before and after the current sites using solution in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param sitesUsingSolutionId the primary key of the current sites using solution
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next sites using solution
	* @throws NoSuchSitesUsingSolutionException if a sites using solution with the primary key could not be found
	*/
	public SitesUsingSolution[] findByUuid_C_PrevAndNext(
		long sitesUsingSolutionId, java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<SitesUsingSolution> orderByComparator)
		throws NoSuchSitesUsingSolutionException;

	/**
	* Removes all the sites using solutions where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public void removeByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the number of sites using solutions where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching sites using solutions
	*/
	public int countByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the sites using solution where sitesUsingSolutionId = &#63; or throws a {@link NoSuchSitesUsingSolutionException} if it could not be found.
	*
	* @param sitesUsingSolutionId the sites using solution ID
	* @return the matching sites using solution
	* @throws NoSuchSitesUsingSolutionException if a matching sites using solution could not be found
	*/
	public SitesUsingSolution findBySitesUsingSolutionId(
		long sitesUsingSolutionId) throws NoSuchSitesUsingSolutionException;

	/**
	* Returns the sites using solution where sitesUsingSolutionId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param sitesUsingSolutionId the sites using solution ID
	* @return the matching sites using solution, or <code>null</code> if a matching sites using solution could not be found
	*/
	public SitesUsingSolution fetchBySitesUsingSolutionId(
		long sitesUsingSolutionId);

	/**
	* Returns the sites using solution where sitesUsingSolutionId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param sitesUsingSolutionId the sites using solution ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching sites using solution, or <code>null</code> if a matching sites using solution could not be found
	*/
	public SitesUsingSolution fetchBySitesUsingSolutionId(
		long sitesUsingSolutionId, boolean retrieveFromCache);

	/**
	* Removes the sites using solution where sitesUsingSolutionId = &#63; from the database.
	*
	* @param sitesUsingSolutionId the sites using solution ID
	* @return the sites using solution that was removed
	*/
	public SitesUsingSolution removeBySitesUsingSolutionId(
		long sitesUsingSolutionId) throws NoSuchSitesUsingSolutionException;

	/**
	* Returns the number of sites using solutions where sitesUsingSolutionId = &#63;.
	*
	* @param sitesUsingSolutionId the sites using solution ID
	* @return the number of matching sites using solutions
	*/
	public int countBySitesUsingSolutionId(long sitesUsingSolutionId);

	/**
	* Returns all the sites using solutions where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the matching sites using solutions
	*/
	public java.util.List<SitesUsingSolution> findByGroupId(long groupId);

	/**
	* Returns a range of all the sites using solutions where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of sites using solutions
	* @param end the upper bound of the range of sites using solutions (not inclusive)
	* @return the range of matching sites using solutions
	*/
	public java.util.List<SitesUsingSolution> findByGroupId(long groupId,
		int start, int end);

	/**
	* Returns an ordered range of all the sites using solutions where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of sites using solutions
	* @param end the upper bound of the range of sites using solutions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching sites using solutions
	*/
	public java.util.List<SitesUsingSolution> findByGroupId(long groupId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<SitesUsingSolution> orderByComparator);

	/**
	* Returns an ordered range of all the sites using solutions where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of sites using solutions
	* @param end the upper bound of the range of sites using solutions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching sites using solutions
	*/
	public java.util.List<SitesUsingSolution> findByGroupId(long groupId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<SitesUsingSolution> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first sites using solution in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching sites using solution
	* @throws NoSuchSitesUsingSolutionException if a matching sites using solution could not be found
	*/
	public SitesUsingSolution findByGroupId_First(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<SitesUsingSolution> orderByComparator)
		throws NoSuchSitesUsingSolutionException;

	/**
	* Returns the first sites using solution in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching sites using solution, or <code>null</code> if a matching sites using solution could not be found
	*/
	public SitesUsingSolution fetchByGroupId_First(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<SitesUsingSolution> orderByComparator);

	/**
	* Returns the last sites using solution in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching sites using solution
	* @throws NoSuchSitesUsingSolutionException if a matching sites using solution could not be found
	*/
	public SitesUsingSolution findByGroupId_Last(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<SitesUsingSolution> orderByComparator)
		throws NoSuchSitesUsingSolutionException;

	/**
	* Returns the last sites using solution in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching sites using solution, or <code>null</code> if a matching sites using solution could not be found
	*/
	public SitesUsingSolution fetchByGroupId_Last(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<SitesUsingSolution> orderByComparator);

	/**
	* Returns the sites using solutions before and after the current sites using solution in the ordered set where groupId = &#63;.
	*
	* @param sitesUsingSolutionId the primary key of the current sites using solution
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next sites using solution
	* @throws NoSuchSitesUsingSolutionException if a sites using solution with the primary key could not be found
	*/
	public SitesUsingSolution[] findByGroupId_PrevAndNext(
		long sitesUsingSolutionId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<SitesUsingSolution> orderByComparator)
		throws NoSuchSitesUsingSolutionException;

	/**
	* Removes all the sites using solutions where groupId = &#63; from the database.
	*
	* @param groupId the group ID
	*/
	public void removeByGroupId(long groupId);

	/**
	* Returns the number of sites using solutions where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the number of matching sites using solutions
	*/
	public int countByGroupId(long groupId);

	/**
	* Caches the sites using solution in the entity cache if it is enabled.
	*
	* @param sitesUsingSolution the sites using solution
	*/
	public void cacheResult(SitesUsingSolution sitesUsingSolution);

	/**
	* Caches the sites using solutions in the entity cache if it is enabled.
	*
	* @param sitesUsingSolutions the sites using solutions
	*/
	public void cacheResult(
		java.util.List<SitesUsingSolution> sitesUsingSolutions);

	/**
	* Creates a new sites using solution with the primary key. Does not add the sites using solution to the database.
	*
	* @param sitesUsingSolutionId the primary key for the new sites using solution
	* @return the new sites using solution
	*/
	public SitesUsingSolution create(long sitesUsingSolutionId);

	/**
	* Removes the sites using solution with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param sitesUsingSolutionId the primary key of the sites using solution
	* @return the sites using solution that was removed
	* @throws NoSuchSitesUsingSolutionException if a sites using solution with the primary key could not be found
	*/
	public SitesUsingSolution remove(long sitesUsingSolutionId)
		throws NoSuchSitesUsingSolutionException;

	public SitesUsingSolution updateImpl(SitesUsingSolution sitesUsingSolution);

	/**
	* Returns the sites using solution with the primary key or throws a {@link NoSuchSitesUsingSolutionException} if it could not be found.
	*
	* @param sitesUsingSolutionId the primary key of the sites using solution
	* @return the sites using solution
	* @throws NoSuchSitesUsingSolutionException if a sites using solution with the primary key could not be found
	*/
	public SitesUsingSolution findByPrimaryKey(long sitesUsingSolutionId)
		throws NoSuchSitesUsingSolutionException;

	/**
	* Returns the sites using solution with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param sitesUsingSolutionId the primary key of the sites using solution
	* @return the sites using solution, or <code>null</code> if a sites using solution with the primary key could not be found
	*/
	public SitesUsingSolution fetchByPrimaryKey(long sitesUsingSolutionId);

	@Override
	public java.util.Map<java.io.Serializable, SitesUsingSolution> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the sites using solutions.
	*
	* @return the sites using solutions
	*/
	public java.util.List<SitesUsingSolution> findAll();

	/**
	* Returns a range of all the sites using solutions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of sites using solutions
	* @param end the upper bound of the range of sites using solutions (not inclusive)
	* @return the range of sites using solutions
	*/
	public java.util.List<SitesUsingSolution> findAll(int start, int end);

	/**
	* Returns an ordered range of all the sites using solutions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of sites using solutions
	* @param end the upper bound of the range of sites using solutions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of sites using solutions
	*/
	public java.util.List<SitesUsingSolution> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<SitesUsingSolution> orderByComparator);

	/**
	* Returns an ordered range of all the sites using solutions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of sites using solutions
	* @param end the upper bound of the range of sites using solutions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of sites using solutions
	*/
	public java.util.List<SitesUsingSolution> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<SitesUsingSolution> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the sites using solutions from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of sites using solutions.
	*
	* @return the number of sites using solutions
	*/
	public int countAll();

	@Override
	public java.util.Set<java.lang.String> getBadColumnNames();
}