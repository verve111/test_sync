/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.exportimport.kernel.lar.StagedModelType;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Publications}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Publications
 * @generated
 */
@ProviderType
public class PublicationsWrapper implements Publications,
	ModelWrapper<Publications> {
	public PublicationsWrapper(Publications publications) {
		_publications = publications;
	}

	@Override
	public Class<?> getModelClass() {
		return Publications.class;
	}

	@Override
	public String getModelClassName() {
		return Publications.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("publicationsId", getPublicationsId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("titleOfPublications", getTitleOfPublications());
		attributes.put("date", getDate());
		attributes.put("URL", getURL());
		attributes.put("comments", getComments());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long publicationsId = (Long)attributes.get("publicationsId");

		if (publicationsId != null) {
			setPublicationsId(publicationsId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String titleOfPublications = (String)attributes.get(
				"titleOfPublications");

		if (titleOfPublications != null) {
			setTitleOfPublications(titleOfPublications);
		}

		Date date = (Date)attributes.get("date");

		if (date != null) {
			setDate(date);
		}

		String URL = (String)attributes.get("URL");

		if (URL != null) {
			setURL(URL);
		}

		String comments = (String)attributes.get("comments");

		if (comments != null) {
			setComments(comments);
		}
	}

	@Override
	public Publications toEscapedModel() {
		return new PublicationsWrapper(_publications.toEscapedModel());
	}

	@Override
	public Publications toUnescapedModel() {
		return new PublicationsWrapper(_publications.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _publications.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _publications.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _publications.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _publications.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Publications> toCacheModel() {
		return _publications.toCacheModel();
	}

	@Override
	public int compareTo(Publications publications) {
		return _publications.compareTo(publications);
	}

	@Override
	public int hashCode() {
		return _publications.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _publications.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new PublicationsWrapper((Publications)_publications.clone());
	}

	/**
	* Returns the comments of this publications.
	*
	* @return the comments of this publications
	*/
	@Override
	public java.lang.String getComments() {
		return _publications.getComments();
	}

	/**
	* Returns the title of publications of this publications.
	*
	* @return the title of publications of this publications
	*/
	@Override
	public java.lang.String getTitleOfPublications() {
		return _publications.getTitleOfPublications();
	}

	/**
	* Returns the u r l of this publications.
	*
	* @return the u r l of this publications
	*/
	@Override
	public java.lang.String getURL() {
		return _publications.getURL();
	}

	/**
	* Returns the user name of this publications.
	*
	* @return the user name of this publications
	*/
	@Override
	public java.lang.String getUserName() {
		return _publications.getUserName();
	}

	/**
	* Returns the user uuid of this publications.
	*
	* @return the user uuid of this publications
	*/
	@Override
	public java.lang.String getUserUuid() {
		return _publications.getUserUuid();
	}

	/**
	* Returns the uuid of this publications.
	*
	* @return the uuid of this publications
	*/
	@Override
	public java.lang.String getUuid() {
		return _publications.getUuid();
	}

	@Override
	public java.lang.String toString() {
		return _publications.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _publications.toXmlString();
	}

	/**
	* Returns the create date of this publications.
	*
	* @return the create date of this publications
	*/
	@Override
	public Date getCreateDate() {
		return _publications.getCreateDate();
	}

	/**
	* Returns the date of this publications.
	*
	* @return the date of this publications
	*/
	@Override
	public Date getDate() {
		return _publications.getDate();
	}

	/**
	* Returns the modified date of this publications.
	*
	* @return the modified date of this publications
	*/
	@Override
	public Date getModifiedDate() {
		return _publications.getModifiedDate();
	}

	/**
	* Returns the company ID of this publications.
	*
	* @return the company ID of this publications
	*/
	@Override
	public long getCompanyId() {
		return _publications.getCompanyId();
	}

	/**
	* Returns the group ID of this publications.
	*
	* @return the group ID of this publications
	*/
	@Override
	public long getGroupId() {
		return _publications.getGroupId();
	}

	/**
	* Returns the primary key of this publications.
	*
	* @return the primary key of this publications
	*/
	@Override
	public long getPrimaryKey() {
		return _publications.getPrimaryKey();
	}

	/**
	* Returns the publications ID of this publications.
	*
	* @return the publications ID of this publications
	*/
	@Override
	public long getPublicationsId() {
		return _publications.getPublicationsId();
	}

	/**
	* Returns the user ID of this publications.
	*
	* @return the user ID of this publications
	*/
	@Override
	public long getUserId() {
		return _publications.getUserId();
	}

	@Override
	public void persist() {
		_publications.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_publications.setCachedModel(cachedModel);
	}

	/**
	* Sets the comments of this publications.
	*
	* @param comments the comments of this publications
	*/
	@Override
	public void setComments(java.lang.String comments) {
		_publications.setComments(comments);
	}

	/**
	* Sets the company ID of this publications.
	*
	* @param companyId the company ID of this publications
	*/
	@Override
	public void setCompanyId(long companyId) {
		_publications.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this publications.
	*
	* @param createDate the create date of this publications
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_publications.setCreateDate(createDate);
	}

	/**
	* Sets the date of this publications.
	*
	* @param date the date of this publications
	*/
	@Override
	public void setDate(Date date) {
		_publications.setDate(date);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_publications.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_publications.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_publications.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the group ID of this publications.
	*
	* @param groupId the group ID of this publications
	*/
	@Override
	public void setGroupId(long groupId) {
		_publications.setGroupId(groupId);
	}

	/**
	* Sets the modified date of this publications.
	*
	* @param modifiedDate the modified date of this publications
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_publications.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_publications.setNew(n);
	}

	/**
	* Sets the primary key of this publications.
	*
	* @param primaryKey the primary key of this publications
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_publications.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_publications.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the publications ID of this publications.
	*
	* @param publicationsId the publications ID of this publications
	*/
	@Override
	public void setPublicationsId(long publicationsId) {
		_publications.setPublicationsId(publicationsId);
	}

	/**
	* Sets the title of publications of this publications.
	*
	* @param titleOfPublications the title of publications of this publications
	*/
	@Override
	public void setTitleOfPublications(java.lang.String titleOfPublications) {
		_publications.setTitleOfPublications(titleOfPublications);
	}

	/**
	* Sets the u r l of this publications.
	*
	* @param URL the u r l of this publications
	*/
	@Override
	public void setURL(java.lang.String URL) {
		_publications.setURL(URL);
	}

	/**
	* Sets the user ID of this publications.
	*
	* @param userId the user ID of this publications
	*/
	@Override
	public void setUserId(long userId) {
		_publications.setUserId(userId);
	}

	/**
	* Sets the user name of this publications.
	*
	* @param userName the user name of this publications
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_publications.setUserName(userName);
	}

	/**
	* Sets the user uuid of this publications.
	*
	* @param userUuid the user uuid of this publications
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_publications.setUserUuid(userUuid);
	}

	/**
	* Sets the uuid of this publications.
	*
	* @param uuid the uuid of this publications
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_publications.setUuid(uuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PublicationsWrapper)) {
			return false;
		}

		PublicationsWrapper publicationsWrapper = (PublicationsWrapper)obj;

		if (Objects.equals(_publications, publicationsWrapper._publications)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _publications.getStagedModelType();
	}

	@Override
	public Publications getWrappedModel() {
		return _publications;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _publications.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _publications.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_publications.resetOriginalValues();
	}

	private final Publications _publications;
}