/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.collaboratelab.impact.service.http.CollaboratorsServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see com.collaboratelab.impact.service.http.CollaboratorsServiceSoap
 * @generated
 */
@ProviderType
public class CollaboratorsSoap implements Serializable {
	public static CollaboratorsSoap toSoapModel(Collaborators model) {
		CollaboratorsSoap soapModel = new CollaboratorsSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setCollaboratorId(model.getCollaboratorId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setStartDate(model.getStartDate());
		soapModel.setEndDate(model.getEndDate());
		soapModel.setName(model.getName());
		soapModel.setURL(model.getURL());
		soapModel.setType(model.getType());
		soapModel.setOptionalComments(model.getOptionalComments());

		return soapModel;
	}

	public static CollaboratorsSoap[] toSoapModels(Collaborators[] models) {
		CollaboratorsSoap[] soapModels = new CollaboratorsSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CollaboratorsSoap[][] toSoapModels(Collaborators[][] models) {
		CollaboratorsSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CollaboratorsSoap[models.length][models[0].length];
		}
		else {
			soapModels = new CollaboratorsSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CollaboratorsSoap[] toSoapModels(List<Collaborators> models) {
		List<CollaboratorsSoap> soapModels = new ArrayList<CollaboratorsSoap>(models.size());

		for (Collaborators model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CollaboratorsSoap[soapModels.size()]);
	}

	public CollaboratorsSoap() {
	}

	public long getPrimaryKey() {
		return _collaboratorId;
	}

	public void setPrimaryKey(long pk) {
		setCollaboratorId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getCollaboratorId() {
		return _collaboratorId;
	}

	public void setCollaboratorId(long collaboratorId) {
		_collaboratorId = collaboratorId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public Date getStartDate() {
		return _startDate;
	}

	public void setStartDate(Date startDate) {
		_startDate = startDate;
	}

	public Date getEndDate() {
		return _endDate;
	}

	public void setEndDate(Date endDate) {
		_endDate = endDate;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public String getURL() {
		return _URL;
	}

	public void setURL(String URL) {
		_URL = URL;
	}

	public int getType() {
		return _type;
	}

	public void setType(int type) {
		_type = type;
	}

	public String getOptionalComments() {
		return _optionalComments;
	}

	public void setOptionalComments(String optionalComments) {
		_optionalComments = optionalComments;
	}

	private String _uuid;
	private long _collaboratorId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private Date _startDate;
	private Date _endDate;
	private String _name;
	private String _URL;
	private int _type;
	private String _optionalComments;
}