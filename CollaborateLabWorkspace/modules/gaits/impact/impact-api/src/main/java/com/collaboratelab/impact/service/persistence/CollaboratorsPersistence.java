/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.impact.exception.NoSuchCollaboratorsException;
import com.collaboratelab.impact.model.Collaborators;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the collaborators service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.collaboratelab.impact.service.persistence.impl.CollaboratorsPersistenceImpl
 * @see CollaboratorsUtil
 * @generated
 */
@ProviderType
public interface CollaboratorsPersistence extends BasePersistence<Collaborators> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CollaboratorsUtil} to access the collaborators persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the collaboratorses where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching collaboratorses
	*/
	public java.util.List<Collaborators> findByUuid(java.lang.String uuid);

	/**
	* Returns a range of all the collaboratorses where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of collaboratorses
	* @param end the upper bound of the range of collaboratorses (not inclusive)
	* @return the range of matching collaboratorses
	*/
	public java.util.List<Collaborators> findByUuid(java.lang.String uuid,
		int start, int end);

	/**
	* Returns an ordered range of all the collaboratorses where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of collaboratorses
	* @param end the upper bound of the range of collaboratorses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching collaboratorses
	*/
	public java.util.List<Collaborators> findByUuid(java.lang.String uuid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Collaborators> orderByComparator);

	/**
	* Returns an ordered range of all the collaboratorses where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of collaboratorses
	* @param end the upper bound of the range of collaboratorses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching collaboratorses
	*/
	public java.util.List<Collaborators> findByUuid(java.lang.String uuid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Collaborators> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first collaborators in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching collaborators
	* @throws NoSuchCollaboratorsException if a matching collaborators could not be found
	*/
	public Collaborators findByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Collaborators> orderByComparator)
		throws NoSuchCollaboratorsException;

	/**
	* Returns the first collaborators in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching collaborators, or <code>null</code> if a matching collaborators could not be found
	*/
	public Collaborators fetchByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Collaborators> orderByComparator);

	/**
	* Returns the last collaborators in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching collaborators
	* @throws NoSuchCollaboratorsException if a matching collaborators could not be found
	*/
	public Collaborators findByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Collaborators> orderByComparator)
		throws NoSuchCollaboratorsException;

	/**
	* Returns the last collaborators in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching collaborators, or <code>null</code> if a matching collaborators could not be found
	*/
	public Collaborators fetchByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Collaborators> orderByComparator);

	/**
	* Returns the collaboratorses before and after the current collaborators in the ordered set where uuid = &#63;.
	*
	* @param collaboratorId the primary key of the current collaborators
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next collaborators
	* @throws NoSuchCollaboratorsException if a collaborators with the primary key could not be found
	*/
	public Collaborators[] findByUuid_PrevAndNext(long collaboratorId,
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Collaborators> orderByComparator)
		throws NoSuchCollaboratorsException;

	/**
	* Removes all the collaboratorses where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public void removeByUuid(java.lang.String uuid);

	/**
	* Returns the number of collaboratorses where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching collaboratorses
	*/
	public int countByUuid(java.lang.String uuid);

	/**
	* Returns the collaborators where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchCollaboratorsException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching collaborators
	* @throws NoSuchCollaboratorsException if a matching collaborators could not be found
	*/
	public Collaborators findByUUID_G(java.lang.String uuid, long groupId)
		throws NoSuchCollaboratorsException;

	/**
	* Returns the collaborators where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching collaborators, or <code>null</code> if a matching collaborators could not be found
	*/
	public Collaborators fetchByUUID_G(java.lang.String uuid, long groupId);

	/**
	* Returns the collaborators where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching collaborators, or <code>null</code> if a matching collaborators could not be found
	*/
	public Collaborators fetchByUUID_G(java.lang.String uuid, long groupId,
		boolean retrieveFromCache);

	/**
	* Removes the collaborators where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the collaborators that was removed
	*/
	public Collaborators removeByUUID_G(java.lang.String uuid, long groupId)
		throws NoSuchCollaboratorsException;

	/**
	* Returns the number of collaboratorses where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching collaboratorses
	*/
	public int countByUUID_G(java.lang.String uuid, long groupId);

	/**
	* Returns all the collaboratorses where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching collaboratorses
	*/
	public java.util.List<Collaborators> findByUuid_C(java.lang.String uuid,
		long companyId);

	/**
	* Returns a range of all the collaboratorses where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of collaboratorses
	* @param end the upper bound of the range of collaboratorses (not inclusive)
	* @return the range of matching collaboratorses
	*/
	public java.util.List<Collaborators> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end);

	/**
	* Returns an ordered range of all the collaboratorses where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of collaboratorses
	* @param end the upper bound of the range of collaboratorses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching collaboratorses
	*/
	public java.util.List<Collaborators> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Collaborators> orderByComparator);

	/**
	* Returns an ordered range of all the collaboratorses where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of collaboratorses
	* @param end the upper bound of the range of collaboratorses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching collaboratorses
	*/
	public java.util.List<Collaborators> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Collaborators> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first collaborators in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching collaborators
	* @throws NoSuchCollaboratorsException if a matching collaborators could not be found
	*/
	public Collaborators findByUuid_C_First(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Collaborators> orderByComparator)
		throws NoSuchCollaboratorsException;

	/**
	* Returns the first collaborators in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching collaborators, or <code>null</code> if a matching collaborators could not be found
	*/
	public Collaborators fetchByUuid_C_First(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Collaborators> orderByComparator);

	/**
	* Returns the last collaborators in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching collaborators
	* @throws NoSuchCollaboratorsException if a matching collaborators could not be found
	*/
	public Collaborators findByUuid_C_Last(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Collaborators> orderByComparator)
		throws NoSuchCollaboratorsException;

	/**
	* Returns the last collaborators in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching collaborators, or <code>null</code> if a matching collaborators could not be found
	*/
	public Collaborators fetchByUuid_C_Last(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Collaborators> orderByComparator);

	/**
	* Returns the collaboratorses before and after the current collaborators in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param collaboratorId the primary key of the current collaborators
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next collaborators
	* @throws NoSuchCollaboratorsException if a collaborators with the primary key could not be found
	*/
	public Collaborators[] findByUuid_C_PrevAndNext(long collaboratorId,
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Collaborators> orderByComparator)
		throws NoSuchCollaboratorsException;

	/**
	* Removes all the collaboratorses where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public void removeByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the number of collaboratorses where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching collaboratorses
	*/
	public int countByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the collaborators where collaboratorId = &#63; or throws a {@link NoSuchCollaboratorsException} if it could not be found.
	*
	* @param collaboratorId the collaborator ID
	* @return the matching collaborators
	* @throws NoSuchCollaboratorsException if a matching collaborators could not be found
	*/
	public Collaborators findByCollaboratorsId(long collaboratorId)
		throws NoSuchCollaboratorsException;

	/**
	* Returns the collaborators where collaboratorId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param collaboratorId the collaborator ID
	* @return the matching collaborators, or <code>null</code> if a matching collaborators could not be found
	*/
	public Collaborators fetchByCollaboratorsId(long collaboratorId);

	/**
	* Returns the collaborators where collaboratorId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param collaboratorId the collaborator ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching collaborators, or <code>null</code> if a matching collaborators could not be found
	*/
	public Collaborators fetchByCollaboratorsId(long collaboratorId,
		boolean retrieveFromCache);

	/**
	* Removes the collaborators where collaboratorId = &#63; from the database.
	*
	* @param collaboratorId the collaborator ID
	* @return the collaborators that was removed
	*/
	public Collaborators removeByCollaboratorsId(long collaboratorId)
		throws NoSuchCollaboratorsException;

	/**
	* Returns the number of collaboratorses where collaboratorId = &#63;.
	*
	* @param collaboratorId the collaborator ID
	* @return the number of matching collaboratorses
	*/
	public int countByCollaboratorsId(long collaboratorId);

	/**
	* Returns all the collaboratorses where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the matching collaboratorses
	*/
	public java.util.List<Collaborators> findByGroupId(long groupId);

	/**
	* Returns a range of all the collaboratorses where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of collaboratorses
	* @param end the upper bound of the range of collaboratorses (not inclusive)
	* @return the range of matching collaboratorses
	*/
	public java.util.List<Collaborators> findByGroupId(long groupId, int start,
		int end);

	/**
	* Returns an ordered range of all the collaboratorses where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of collaboratorses
	* @param end the upper bound of the range of collaboratorses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching collaboratorses
	*/
	public java.util.List<Collaborators> findByGroupId(long groupId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Collaborators> orderByComparator);

	/**
	* Returns an ordered range of all the collaboratorses where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of collaboratorses
	* @param end the upper bound of the range of collaboratorses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching collaboratorses
	*/
	public java.util.List<Collaborators> findByGroupId(long groupId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<Collaborators> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first collaborators in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching collaborators
	* @throws NoSuchCollaboratorsException if a matching collaborators could not be found
	*/
	public Collaborators findByGroupId_First(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Collaborators> orderByComparator)
		throws NoSuchCollaboratorsException;

	/**
	* Returns the first collaborators in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching collaborators, or <code>null</code> if a matching collaborators could not be found
	*/
	public Collaborators fetchByGroupId_First(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Collaborators> orderByComparator);

	/**
	* Returns the last collaborators in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching collaborators
	* @throws NoSuchCollaboratorsException if a matching collaborators could not be found
	*/
	public Collaborators findByGroupId_Last(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Collaborators> orderByComparator)
		throws NoSuchCollaboratorsException;

	/**
	* Returns the last collaborators in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching collaborators, or <code>null</code> if a matching collaborators could not be found
	*/
	public Collaborators fetchByGroupId_Last(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Collaborators> orderByComparator);

	/**
	* Returns the collaboratorses before and after the current collaborators in the ordered set where groupId = &#63;.
	*
	* @param collaboratorId the primary key of the current collaborators
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next collaborators
	* @throws NoSuchCollaboratorsException if a collaborators with the primary key could not be found
	*/
	public Collaborators[] findByGroupId_PrevAndNext(long collaboratorId,
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<Collaborators> orderByComparator)
		throws NoSuchCollaboratorsException;

	/**
	* Removes all the collaboratorses where groupId = &#63; from the database.
	*
	* @param groupId the group ID
	*/
	public void removeByGroupId(long groupId);

	/**
	* Returns the number of collaboratorses where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the number of matching collaboratorses
	*/
	public int countByGroupId(long groupId);

	/**
	* Caches the collaborators in the entity cache if it is enabled.
	*
	* @param collaborators the collaborators
	*/
	public void cacheResult(Collaborators collaborators);

	/**
	* Caches the collaboratorses in the entity cache if it is enabled.
	*
	* @param collaboratorses the collaboratorses
	*/
	public void cacheResult(java.util.List<Collaborators> collaboratorses);

	/**
	* Creates a new collaborators with the primary key. Does not add the collaborators to the database.
	*
	* @param collaboratorId the primary key for the new collaborators
	* @return the new collaborators
	*/
	public Collaborators create(long collaboratorId);

	/**
	* Removes the collaborators with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param collaboratorId the primary key of the collaborators
	* @return the collaborators that was removed
	* @throws NoSuchCollaboratorsException if a collaborators with the primary key could not be found
	*/
	public Collaborators remove(long collaboratorId)
		throws NoSuchCollaboratorsException;

	public Collaborators updateImpl(Collaborators collaborators);

	/**
	* Returns the collaborators with the primary key or throws a {@link NoSuchCollaboratorsException} if it could not be found.
	*
	* @param collaboratorId the primary key of the collaborators
	* @return the collaborators
	* @throws NoSuchCollaboratorsException if a collaborators with the primary key could not be found
	*/
	public Collaborators findByPrimaryKey(long collaboratorId)
		throws NoSuchCollaboratorsException;

	/**
	* Returns the collaborators with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param collaboratorId the primary key of the collaborators
	* @return the collaborators, or <code>null</code> if a collaborators with the primary key could not be found
	*/
	public Collaborators fetchByPrimaryKey(long collaboratorId);

	@Override
	public java.util.Map<java.io.Serializable, Collaborators> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the collaboratorses.
	*
	* @return the collaboratorses
	*/
	public java.util.List<Collaborators> findAll();

	/**
	* Returns a range of all the collaboratorses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of collaboratorses
	* @param end the upper bound of the range of collaboratorses (not inclusive)
	* @return the range of collaboratorses
	*/
	public java.util.List<Collaborators> findAll(int start, int end);

	/**
	* Returns an ordered range of all the collaboratorses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of collaboratorses
	* @param end the upper bound of the range of collaboratorses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of collaboratorses
	*/
	public java.util.List<Collaborators> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Collaborators> orderByComparator);

	/**
	* Returns an ordered range of all the collaboratorses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CollaboratorsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of collaboratorses
	* @param end the upper bound of the range of collaboratorses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of collaboratorses
	*/
	public java.util.List<Collaborators> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Collaborators> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the collaboratorses from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of collaboratorses.
	*
	* @return the number of collaboratorses
	*/
	public int countAll();

	@Override
	public java.util.Set<java.lang.String> getBadColumnNames();
}