/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.collaboratelab.impact.service.http.JobsServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see com.collaboratelab.impact.service.http.JobsServiceSoap
 * @generated
 */
@ProviderType
public class JobsSoap implements Serializable {
	public static JobsSoap toSoapModel(Jobs model) {
		JobsSoap soapModel = new JobsSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setJobsId(model.getJobsId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setAsOfDate(model.getAsOfDate());
		soapModel.setSizeOfTeam(model.getSizeOfTeam());
		soapModel.setComments(model.getComments());
		soapModel.setPrimaryJob(model.getPrimaryJob());

		return soapModel;
	}

	public static JobsSoap[] toSoapModels(Jobs[] models) {
		JobsSoap[] soapModels = new JobsSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static JobsSoap[][] toSoapModels(Jobs[][] models) {
		JobsSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new JobsSoap[models.length][models[0].length];
		}
		else {
			soapModels = new JobsSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static JobsSoap[] toSoapModels(List<Jobs> models) {
		List<JobsSoap> soapModels = new ArrayList<JobsSoap>(models.size());

		for (Jobs model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new JobsSoap[soapModels.size()]);
	}

	public JobsSoap() {
	}

	public long getPrimaryKey() {
		return _jobsId;
	}

	public void setPrimaryKey(long pk) {
		setJobsId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getJobsId() {
		return _jobsId;
	}

	public void setJobsId(long jobsId) {
		_jobsId = jobsId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public Date getAsOfDate() {
		return _asOfDate;
	}

	public void setAsOfDate(Date asOfDate) {
		_asOfDate = asOfDate;
	}

	public int getSizeOfTeam() {
		return _sizeOfTeam;
	}

	public void setSizeOfTeam(int sizeOfTeam) {
		_sizeOfTeam = sizeOfTeam;
	}

	public String getComments() {
		return _comments;
	}

	public void setComments(String comments) {
		_comments = comments;
	}

	public String getPrimaryJob() {
		return _primaryJob;
	}

	public void setPrimaryJob(String primaryJob) {
		_primaryJob = primaryJob;
	}

	private String _uuid;
	private long _jobsId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private Date _asOfDate;
	private int _sizeOfTeam;
	private String _comments;
	private String _primaryJob;
}