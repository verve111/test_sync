/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.impact.model.Support;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the support service. This utility wraps {@link com.collaboratelab.impact.service.persistence.impl.SupportPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see SupportPersistence
 * @see com.collaboratelab.impact.service.persistence.impl.SupportPersistenceImpl
 * @generated
 */
@ProviderType
public class SupportUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Support support) {
		getPersistence().clearCache(support);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Support> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Support> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Support> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Support> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Support update(Support support) {
		return getPersistence().update(support);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Support update(Support support, ServiceContext serviceContext) {
		return getPersistence().update(support, serviceContext);
	}

	/**
	* Returns all the supports where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching supports
	*/
	public static List<Support> findByUuid(java.lang.String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	* Returns a range of all the supports where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SupportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of supports
	* @param end the upper bound of the range of supports (not inclusive)
	* @return the range of matching supports
	*/
	public static List<Support> findByUuid(java.lang.String uuid, int start,
		int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	* Returns an ordered range of all the supports where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SupportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of supports
	* @param end the upper bound of the range of supports (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching supports
	*/
	public static List<Support> findByUuid(java.lang.String uuid, int start,
		int end, OrderByComparator<Support> orderByComparator) {
		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the supports where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SupportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of supports
	* @param end the upper bound of the range of supports (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching supports
	*/
	public static List<Support> findByUuid(java.lang.String uuid, int start,
		int end, OrderByComparator<Support> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid(uuid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first support in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching support
	* @throws NoSuchSupportException if a matching support could not be found
	*/
	public static Support findByUuid_First(java.lang.String uuid,
		OrderByComparator<Support> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchSupportException {
		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the first support in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching support, or <code>null</code> if a matching support could not be found
	*/
	public static Support fetchByUuid_First(java.lang.String uuid,
		OrderByComparator<Support> orderByComparator) {
		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the last support in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching support
	* @throws NoSuchSupportException if a matching support could not be found
	*/
	public static Support findByUuid_Last(java.lang.String uuid,
		OrderByComparator<Support> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchSupportException {
		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the last support in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching support, or <code>null</code> if a matching support could not be found
	*/
	public static Support fetchByUuid_Last(java.lang.String uuid,
		OrderByComparator<Support> orderByComparator) {
		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the supports before and after the current support in the ordered set where uuid = &#63;.
	*
	* @param supportId the primary key of the current support
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next support
	* @throws NoSuchSupportException if a support with the primary key could not be found
	*/
	public static Support[] findByUuid_PrevAndNext(long supportId,
		java.lang.String uuid, OrderByComparator<Support> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchSupportException {
		return getPersistence()
				   .findByUuid_PrevAndNext(supportId, uuid, orderByComparator);
	}

	/**
	* Removes all the supports where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public static void removeByUuid(java.lang.String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	* Returns the number of supports where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching supports
	*/
	public static int countByUuid(java.lang.String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	* Returns the support where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchSupportException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching support
	* @throws NoSuchSupportException if a matching support could not be found
	*/
	public static Support findByUUID_G(java.lang.String uuid, long groupId)
		throws com.collaboratelab.impact.exception.NoSuchSupportException {
		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	* Returns the support where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching support, or <code>null</code> if a matching support could not be found
	*/
	public static Support fetchByUUID_G(java.lang.String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	* Returns the support where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching support, or <code>null</code> if a matching support could not be found
	*/
	public static Support fetchByUUID_G(java.lang.String uuid, long groupId,
		boolean retrieveFromCache) {
		return getPersistence().fetchByUUID_G(uuid, groupId, retrieveFromCache);
	}

	/**
	* Removes the support where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the support that was removed
	*/
	public static Support removeByUUID_G(java.lang.String uuid, long groupId)
		throws com.collaboratelab.impact.exception.NoSuchSupportException {
		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	* Returns the number of supports where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching supports
	*/
	public static int countByUUID_G(java.lang.String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	* Returns all the supports where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching supports
	*/
	public static List<Support> findByUuid_C(java.lang.String uuid,
		long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	* Returns a range of all the supports where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SupportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of supports
	* @param end the upper bound of the range of supports (not inclusive)
	* @return the range of matching supports
	*/
	public static List<Support> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end) {
		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	* Returns an ordered range of all the supports where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SupportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of supports
	* @param end the upper bound of the range of supports (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching supports
	*/
	public static List<Support> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<Support> orderByComparator) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the supports where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SupportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of supports
	* @param end the upper bound of the range of supports (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching supports
	*/
	public static List<Support> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<Support> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first support in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching support
	* @throws NoSuchSupportException if a matching support could not be found
	*/
	public static Support findByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<Support> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchSupportException {
		return getPersistence()
				   .findByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the first support in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching support, or <code>null</code> if a matching support could not be found
	*/
	public static Support fetchByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<Support> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last support in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching support
	* @throws NoSuchSupportException if a matching support could not be found
	*/
	public static Support findByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<Support> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchSupportException {
		return getPersistence()
				   .findByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last support in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching support, or <code>null</code> if a matching support could not be found
	*/
	public static Support fetchByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<Support> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the supports before and after the current support in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param supportId the primary key of the current support
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next support
	* @throws NoSuchSupportException if a support with the primary key could not be found
	*/
	public static Support[] findByUuid_C_PrevAndNext(long supportId,
		java.lang.String uuid, long companyId,
		OrderByComparator<Support> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchSupportException {
		return getPersistence()
				   .findByUuid_C_PrevAndNext(supportId, uuid, companyId,
			orderByComparator);
	}

	/**
	* Removes all the supports where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public static void removeByUuid_C(java.lang.String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	* Returns the number of supports where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching supports
	*/
	public static int countByUuid_C(java.lang.String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	* Returns the support where supportId = &#63; or throws a {@link NoSuchSupportException} if it could not be found.
	*
	* @param supportId the support ID
	* @return the matching support
	* @throws NoSuchSupportException if a matching support could not be found
	*/
	public static Support findBySupportId(long supportId)
		throws com.collaboratelab.impact.exception.NoSuchSupportException {
		return getPersistence().findBySupportId(supportId);
	}

	/**
	* Returns the support where supportId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param supportId the support ID
	* @return the matching support, or <code>null</code> if a matching support could not be found
	*/
	public static Support fetchBySupportId(long supportId) {
		return getPersistence().fetchBySupportId(supportId);
	}

	/**
	* Returns the support where supportId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param supportId the support ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching support, or <code>null</code> if a matching support could not be found
	*/
	public static Support fetchBySupportId(long supportId,
		boolean retrieveFromCache) {
		return getPersistence().fetchBySupportId(supportId, retrieveFromCache);
	}

	/**
	* Removes the support where supportId = &#63; from the database.
	*
	* @param supportId the support ID
	* @return the support that was removed
	*/
	public static Support removeBySupportId(long supportId)
		throws com.collaboratelab.impact.exception.NoSuchSupportException {
		return getPersistence().removeBySupportId(supportId);
	}

	/**
	* Returns the number of supports where supportId = &#63;.
	*
	* @param supportId the support ID
	* @return the number of matching supports
	*/
	public static int countBySupportId(long supportId) {
		return getPersistence().countBySupportId(supportId);
	}

	/**
	* Returns all the supports where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the matching supports
	*/
	public static List<Support> findByGroupId(long groupId) {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	* Returns a range of all the supports where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SupportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of supports
	* @param end the upper bound of the range of supports (not inclusive)
	* @return the range of matching supports
	*/
	public static List<Support> findByGroupId(long groupId, int start, int end) {
		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	* Returns an ordered range of all the supports where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SupportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of supports
	* @param end the upper bound of the range of supports (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching supports
	*/
	public static List<Support> findByGroupId(long groupId, int start, int end,
		OrderByComparator<Support> orderByComparator) {
		return getPersistence()
				   .findByGroupId(groupId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the supports where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SupportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of supports
	* @param end the upper bound of the range of supports (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching supports
	*/
	public static List<Support> findByGroupId(long groupId, int start, int end,
		OrderByComparator<Support> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findByGroupId(groupId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first support in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching support
	* @throws NoSuchSupportException if a matching support could not be found
	*/
	public static Support findByGroupId_First(long groupId,
		OrderByComparator<Support> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchSupportException {
		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	* Returns the first support in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching support, or <code>null</code> if a matching support could not be found
	*/
	public static Support fetchByGroupId_First(long groupId,
		OrderByComparator<Support> orderByComparator) {
		return getPersistence().fetchByGroupId_First(groupId, orderByComparator);
	}

	/**
	* Returns the last support in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching support
	* @throws NoSuchSupportException if a matching support could not be found
	*/
	public static Support findByGroupId_Last(long groupId,
		OrderByComparator<Support> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchSupportException {
		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	* Returns the last support in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching support, or <code>null</code> if a matching support could not be found
	*/
	public static Support fetchByGroupId_Last(long groupId,
		OrderByComparator<Support> orderByComparator) {
		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	* Returns the supports before and after the current support in the ordered set where groupId = &#63;.
	*
	* @param supportId the primary key of the current support
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next support
	* @throws NoSuchSupportException if a support with the primary key could not be found
	*/
	public static Support[] findByGroupId_PrevAndNext(long supportId,
		long groupId, OrderByComparator<Support> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchSupportException {
		return getPersistence()
				   .findByGroupId_PrevAndNext(supportId, groupId,
			orderByComparator);
	}

	/**
	* Removes all the supports where groupId = &#63; from the database.
	*
	* @param groupId the group ID
	*/
	public static void removeByGroupId(long groupId) {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	* Returns the number of supports where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the number of matching supports
	*/
	public static int countByGroupId(long groupId) {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	* Caches the support in the entity cache if it is enabled.
	*
	* @param support the support
	*/
	public static void cacheResult(Support support) {
		getPersistence().cacheResult(support);
	}

	/**
	* Caches the supports in the entity cache if it is enabled.
	*
	* @param supports the supports
	*/
	public static void cacheResult(List<Support> supports) {
		getPersistence().cacheResult(supports);
	}

	/**
	* Creates a new support with the primary key. Does not add the support to the database.
	*
	* @param supportId the primary key for the new support
	* @return the new support
	*/
	public static Support create(long supportId) {
		return getPersistence().create(supportId);
	}

	/**
	* Removes the support with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param supportId the primary key of the support
	* @return the support that was removed
	* @throws NoSuchSupportException if a support with the primary key could not be found
	*/
	public static Support remove(long supportId)
		throws com.collaboratelab.impact.exception.NoSuchSupportException {
		return getPersistence().remove(supportId);
	}

	public static Support updateImpl(Support support) {
		return getPersistence().updateImpl(support);
	}

	/**
	* Returns the support with the primary key or throws a {@link NoSuchSupportException} if it could not be found.
	*
	* @param supportId the primary key of the support
	* @return the support
	* @throws NoSuchSupportException if a support with the primary key could not be found
	*/
	public static Support findByPrimaryKey(long supportId)
		throws com.collaboratelab.impact.exception.NoSuchSupportException {
		return getPersistence().findByPrimaryKey(supportId);
	}

	/**
	* Returns the support with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param supportId the primary key of the support
	* @return the support, or <code>null</code> if a support with the primary key could not be found
	*/
	public static Support fetchByPrimaryKey(long supportId) {
		return getPersistence().fetchByPrimaryKey(supportId);
	}

	public static java.util.Map<java.io.Serializable, Support> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the supports.
	*
	* @return the supports
	*/
	public static List<Support> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the supports.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SupportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of supports
	* @param end the upper bound of the range of supports (not inclusive)
	* @return the range of supports
	*/
	public static List<Support> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the supports.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SupportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of supports
	* @param end the upper bound of the range of supports (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of supports
	*/
	public static List<Support> findAll(int start, int end,
		OrderByComparator<Support> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the supports.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SupportModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of supports
	* @param end the upper bound of the range of supports (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of supports
	*/
	public static List<Support> findAll(int start, int end,
		OrderByComparator<Support> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the supports from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of supports.
	*
	* @return the number of supports
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static java.util.Set<java.lang.String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static SupportPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<SupportPersistence, SupportPersistence> _serviceTracker =
		ServiceTrackerFactory.open(SupportPersistence.class);
}