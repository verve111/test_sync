/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link TeamMemberProjectRecognitionLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see TeamMemberProjectRecognitionLocalService
 * @generated
 */
@ProviderType
public class TeamMemberProjectRecognitionLocalServiceWrapper
	implements TeamMemberProjectRecognitionLocalService,
		ServiceWrapper<TeamMemberProjectRecognitionLocalService> {
	public TeamMemberProjectRecognitionLocalServiceWrapper(
		TeamMemberProjectRecognitionLocalService teamMemberProjectRecognitionLocalService) {
		_teamMemberProjectRecognitionLocalService = teamMemberProjectRecognitionLocalService;
	}

	/**
	* Adds the team member project recognition to the database. Also notifies the appropriate model listeners.
	*
	* @param teamMemberProjectRecognition the team member project recognition
	* @return the team member project recognition that was added
	*/
	@Override
	public com.collaboratelab.impact.model.TeamMemberProjectRecognition addTeamMemberProjectRecognition(
		com.collaboratelab.impact.model.TeamMemberProjectRecognition teamMemberProjectRecognition) {
		return _teamMemberProjectRecognitionLocalService.addTeamMemberProjectRecognition(teamMemberProjectRecognition);
	}

	/**
	* Creates a new team member project recognition with the primary key. Does not add the team member project recognition to the database.
	*
	* @param teamMemberProjectRecognitionId the primary key for the new team member project recognition
	* @return the new team member project recognition
	*/
	@Override
	public com.collaboratelab.impact.model.TeamMemberProjectRecognition createTeamMemberProjectRecognition(
		long teamMemberProjectRecognitionId) {
		return _teamMemberProjectRecognitionLocalService.createTeamMemberProjectRecognition(teamMemberProjectRecognitionId);
	}

	/**
	* Deletes the team member project recognition from the database. Also notifies the appropriate model listeners.
	*
	* @param teamMemberProjectRecognition the team member project recognition
	* @return the team member project recognition that was removed
	*/
	@Override
	public com.collaboratelab.impact.model.TeamMemberProjectRecognition deleteTeamMemberProjectRecognition(
		com.collaboratelab.impact.model.TeamMemberProjectRecognition teamMemberProjectRecognition) {
		return _teamMemberProjectRecognitionLocalService.deleteTeamMemberProjectRecognition(teamMemberProjectRecognition);
	}

	/**
	* Deletes the team member project recognition with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param teamMemberProjectRecognitionId the primary key of the team member project recognition
	* @return the team member project recognition that was removed
	* @throws PortalException if a team member project recognition with the primary key could not be found
	*/
	@Override
	public com.collaboratelab.impact.model.TeamMemberProjectRecognition deleteTeamMemberProjectRecognition(
		long teamMemberProjectRecognitionId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _teamMemberProjectRecognitionLocalService.deleteTeamMemberProjectRecognition(teamMemberProjectRecognitionId);
	}

	@Override
	public com.collaboratelab.impact.model.TeamMemberProjectRecognition fetchTeamMemberProjectRecognition(
		long teamMemberProjectRecognitionId) {
		return _teamMemberProjectRecognitionLocalService.fetchTeamMemberProjectRecognition(teamMemberProjectRecognitionId);
	}

	/**
	* Returns the team member project recognition matching the UUID and group.
	*
	* @param uuid the team member project recognition's UUID
	* @param groupId the primary key of the group
	* @return the matching team member project recognition, or <code>null</code> if a matching team member project recognition could not be found
	*/
	@Override
	public com.collaboratelab.impact.model.TeamMemberProjectRecognition fetchTeamMemberProjectRecognitionByUuidAndGroupId(
		java.lang.String uuid, long groupId) {
		return _teamMemberProjectRecognitionLocalService.fetchTeamMemberProjectRecognitionByUuidAndGroupId(uuid,
			groupId);
	}

	/**
	* Returns the team member project recognition with the primary key.
	*
	* @param teamMemberProjectRecognitionId the primary key of the team member project recognition
	* @return the team member project recognition
	* @throws PortalException if a team member project recognition with the primary key could not be found
	*/
	@Override
	public com.collaboratelab.impact.model.TeamMemberProjectRecognition getTeamMemberProjectRecognition(
		long teamMemberProjectRecognitionId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _teamMemberProjectRecognitionLocalService.getTeamMemberProjectRecognition(teamMemberProjectRecognitionId);
	}

	/**
	* Returns the team member project recognition matching the UUID and group.
	*
	* @param uuid the team member project recognition's UUID
	* @param groupId the primary key of the group
	* @return the matching team member project recognition
	* @throws PortalException if a matching team member project recognition could not be found
	*/
	@Override
	public com.collaboratelab.impact.model.TeamMemberProjectRecognition getTeamMemberProjectRecognitionByUuidAndGroupId(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _teamMemberProjectRecognitionLocalService.getTeamMemberProjectRecognitionByUuidAndGroupId(uuid,
			groupId);
	}

	/**
	* Updates the team member project recognition in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param teamMemberProjectRecognition the team member project recognition
	* @return the team member project recognition that was updated
	*/
	@Override
	public com.collaboratelab.impact.model.TeamMemberProjectRecognition updateTeamMemberProjectRecognition(
		com.collaboratelab.impact.model.TeamMemberProjectRecognition teamMemberProjectRecognition) {
		return _teamMemberProjectRecognitionLocalService.updateTeamMemberProjectRecognition(teamMemberProjectRecognition);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _teamMemberProjectRecognitionLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _teamMemberProjectRecognitionLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery getExportActionableDynamicQuery(
		com.liferay.exportimport.kernel.lar.PortletDataContext portletDataContext) {
		return _teamMemberProjectRecognitionLocalService.getExportActionableDynamicQuery(portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _teamMemberProjectRecognitionLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _teamMemberProjectRecognitionLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _teamMemberProjectRecognitionLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of team member project recognitions.
	*
	* @return the number of team member project recognitions
	*/
	@Override
	public int getTeamMemberProjectRecognitionsCount() {
		return _teamMemberProjectRecognitionLocalService.getTeamMemberProjectRecognitionsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _teamMemberProjectRecognitionLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _teamMemberProjectRecognitionLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.impact.model.impl.TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _teamMemberProjectRecognitionLocalService.dynamicQuery(dynamicQuery,
			start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.impact.model.impl.TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _teamMemberProjectRecognitionLocalService.dynamicQuery(dynamicQuery,
			start, end, orderByComparator);
	}

	@Override
	public java.util.List<com.collaboratelab.impact.model.TeamMemberProjectRecognition> findByGroupId(
		long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _teamMemberProjectRecognitionLocalService.findByGroupId(groupId);
	}

	/**
	* Returns a range of all the team member project recognitions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.impact.model.impl.TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of team member project recognitions
	* @param end the upper bound of the range of team member project recognitions (not inclusive)
	* @return the range of team member project recognitions
	*/
	@Override
	public java.util.List<com.collaboratelab.impact.model.TeamMemberProjectRecognition> getTeamMemberProjectRecognitions(
		int start, int end) {
		return _teamMemberProjectRecognitionLocalService.getTeamMemberProjectRecognitions(start,
			end);
	}

	/**
	* Returns all the team member project recognitions matching the UUID and company.
	*
	* @param uuid the UUID of the team member project recognitions
	* @param companyId the primary key of the company
	* @return the matching team member project recognitions, or an empty list if no matches were found
	*/
	@Override
	public java.util.List<com.collaboratelab.impact.model.TeamMemberProjectRecognition> getTeamMemberProjectRecognitionsByUuidAndCompanyId(
		java.lang.String uuid, long companyId) {
		return _teamMemberProjectRecognitionLocalService.getTeamMemberProjectRecognitionsByUuidAndCompanyId(uuid,
			companyId);
	}

	/**
	* Returns a range of team member project recognitions matching the UUID and company.
	*
	* @param uuid the UUID of the team member project recognitions
	* @param companyId the primary key of the company
	* @param start the lower bound of the range of team member project recognitions
	* @param end the upper bound of the range of team member project recognitions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the range of matching team member project recognitions, or an empty list if no matches were found
	*/
	@Override
	public java.util.List<com.collaboratelab.impact.model.TeamMemberProjectRecognition> getTeamMemberProjectRecognitionsByUuidAndCompanyId(
		java.lang.String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<com.collaboratelab.impact.model.TeamMemberProjectRecognition> orderByComparator) {
		return _teamMemberProjectRecognitionLocalService.getTeamMemberProjectRecognitionsByUuidAndCompanyId(uuid,
			companyId, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _teamMemberProjectRecognitionLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _teamMemberProjectRecognitionLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public TeamMemberProjectRecognitionLocalService getWrappedService() {
		return _teamMemberProjectRecognitionLocalService;
	}

	@Override
	public void setWrappedService(
		TeamMemberProjectRecognitionLocalService teamMemberProjectRecognitionLocalService) {
		_teamMemberProjectRecognitionLocalService = teamMemberProjectRecognitionLocalService;
	}

	private TeamMemberProjectRecognitionLocalService _teamMemberProjectRecognitionLocalService;
}