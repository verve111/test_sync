/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.collaboratelab.impact.service.http.IntellectualPropertyServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see com.collaboratelab.impact.service.http.IntellectualPropertyServiceSoap
 * @generated
 */
@ProviderType
public class IntellectualPropertySoap implements Serializable {
	public static IntellectualPropertySoap toSoapModel(
		IntellectualProperty model) {
		IntellectualPropertySoap soapModel = new IntellectualPropertySoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setIntellectualPropertyId(model.getIntellectualPropertyId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setOneStFileDate(model.getOneStFileDate());
		soapModel.setAsOfDate(model.getAsOfDate());
		soapModel.setTitle(model.getTitle());
		soapModel.setStage(model.getStage());
		soapModel.setLicenseStatus(model.getLicenseStatus());

		return soapModel;
	}

	public static IntellectualPropertySoap[] toSoapModels(
		IntellectualProperty[] models) {
		IntellectualPropertySoap[] soapModels = new IntellectualPropertySoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static IntellectualPropertySoap[][] toSoapModels(
		IntellectualProperty[][] models) {
		IntellectualPropertySoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new IntellectualPropertySoap[models.length][models[0].length];
		}
		else {
			soapModels = new IntellectualPropertySoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static IntellectualPropertySoap[] toSoapModels(
		List<IntellectualProperty> models) {
		List<IntellectualPropertySoap> soapModels = new ArrayList<IntellectualPropertySoap>(models.size());

		for (IntellectualProperty model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new IntellectualPropertySoap[soapModels.size()]);
	}

	public IntellectualPropertySoap() {
	}

	public long getPrimaryKey() {
		return _intellectualPropertyId;
	}

	public void setPrimaryKey(long pk) {
		setIntellectualPropertyId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getIntellectualPropertyId() {
		return _intellectualPropertyId;
	}

	public void setIntellectualPropertyId(long intellectualPropertyId) {
		_intellectualPropertyId = intellectualPropertyId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public Date getOneStFileDate() {
		return _oneStFileDate;
	}

	public void setOneStFileDate(Date oneStFileDate) {
		_oneStFileDate = oneStFileDate;
	}

	public Date getAsOfDate() {
		return _asOfDate;
	}

	public void setAsOfDate(Date asOfDate) {
		_asOfDate = asOfDate;
	}

	public String getTitle() {
		return _title;
	}

	public void setTitle(String title) {
		_title = title;
	}

	public int getStage() {
		return _stage;
	}

	public void setStage(int stage) {
		_stage = stage;
	}

	public int getLicenseStatus() {
		return _licenseStatus;
	}

	public void setLicenseStatus(int licenseStatus) {
		_licenseStatus = licenseStatus;
	}

	private String _uuid;
	private long _intellectualPropertyId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private Date _oneStFileDate;
	private Date _asOfDate;
	private String _title;
	private int _stage;
	private int _licenseStatus;
}