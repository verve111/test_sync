/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.impact.model.CareerImpact;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the career impact service. This utility wraps {@link com.collaboratelab.impact.service.persistence.impl.CareerImpactPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CareerImpactPersistence
 * @see com.collaboratelab.impact.service.persistence.impl.CareerImpactPersistenceImpl
 * @generated
 */
@ProviderType
public class CareerImpactUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(CareerImpact careerImpact) {
		getPersistence().clearCache(careerImpact);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<CareerImpact> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<CareerImpact> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<CareerImpact> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<CareerImpact> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static CareerImpact update(CareerImpact careerImpact) {
		return getPersistence().update(careerImpact);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static CareerImpact update(CareerImpact careerImpact,
		ServiceContext serviceContext) {
		return getPersistence().update(careerImpact, serviceContext);
	}

	/**
	* Returns all the career impacts where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching career impacts
	*/
	public static List<CareerImpact> findByUuid(java.lang.String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	* Returns a range of all the career impacts where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of career impacts
	* @param end the upper bound of the range of career impacts (not inclusive)
	* @return the range of matching career impacts
	*/
	public static List<CareerImpact> findByUuid(java.lang.String uuid,
		int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	* Returns an ordered range of all the career impacts where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of career impacts
	* @param end the upper bound of the range of career impacts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching career impacts
	*/
	public static List<CareerImpact> findByUuid(java.lang.String uuid,
		int start, int end, OrderByComparator<CareerImpact> orderByComparator) {
		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the career impacts where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of career impacts
	* @param end the upper bound of the range of career impacts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching career impacts
	*/
	public static List<CareerImpact> findByUuid(java.lang.String uuid,
		int start, int end, OrderByComparator<CareerImpact> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid(uuid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first career impact in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching career impact
	* @throws NoSuchCareerImpactException if a matching career impact could not be found
	*/
	public static CareerImpact findByUuid_First(java.lang.String uuid,
		OrderByComparator<CareerImpact> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchCareerImpactException {
		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the first career impact in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching career impact, or <code>null</code> if a matching career impact could not be found
	*/
	public static CareerImpact fetchByUuid_First(java.lang.String uuid,
		OrderByComparator<CareerImpact> orderByComparator) {
		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the last career impact in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching career impact
	* @throws NoSuchCareerImpactException if a matching career impact could not be found
	*/
	public static CareerImpact findByUuid_Last(java.lang.String uuid,
		OrderByComparator<CareerImpact> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchCareerImpactException {
		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the last career impact in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching career impact, or <code>null</code> if a matching career impact could not be found
	*/
	public static CareerImpact fetchByUuid_Last(java.lang.String uuid,
		OrderByComparator<CareerImpact> orderByComparator) {
		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the career impacts before and after the current career impact in the ordered set where uuid = &#63;.
	*
	* @param careerImpactId the primary key of the current career impact
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next career impact
	* @throws NoSuchCareerImpactException if a career impact with the primary key could not be found
	*/
	public static CareerImpact[] findByUuid_PrevAndNext(long careerImpactId,
		java.lang.String uuid, OrderByComparator<CareerImpact> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchCareerImpactException {
		return getPersistence()
				   .findByUuid_PrevAndNext(careerImpactId, uuid,
			orderByComparator);
	}

	/**
	* Removes all the career impacts where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public static void removeByUuid(java.lang.String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	* Returns the number of career impacts where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching career impacts
	*/
	public static int countByUuid(java.lang.String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	* Returns the career impact where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchCareerImpactException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching career impact
	* @throws NoSuchCareerImpactException if a matching career impact could not be found
	*/
	public static CareerImpact findByUUID_G(java.lang.String uuid, long groupId)
		throws com.collaboratelab.impact.exception.NoSuchCareerImpactException {
		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	* Returns the career impact where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching career impact, or <code>null</code> if a matching career impact could not be found
	*/
	public static CareerImpact fetchByUUID_G(java.lang.String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	* Returns the career impact where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching career impact, or <code>null</code> if a matching career impact could not be found
	*/
	public static CareerImpact fetchByUUID_G(java.lang.String uuid,
		long groupId, boolean retrieveFromCache) {
		return getPersistence().fetchByUUID_G(uuid, groupId, retrieveFromCache);
	}

	/**
	* Removes the career impact where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the career impact that was removed
	*/
	public static CareerImpact removeByUUID_G(java.lang.String uuid,
		long groupId)
		throws com.collaboratelab.impact.exception.NoSuchCareerImpactException {
		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	* Returns the number of career impacts where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching career impacts
	*/
	public static int countByUUID_G(java.lang.String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	* Returns all the career impacts where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching career impacts
	*/
	public static List<CareerImpact> findByUuid_C(java.lang.String uuid,
		long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	* Returns a range of all the career impacts where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of career impacts
	* @param end the upper bound of the range of career impacts (not inclusive)
	* @return the range of matching career impacts
	*/
	public static List<CareerImpact> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end) {
		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	* Returns an ordered range of all the career impacts where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of career impacts
	* @param end the upper bound of the range of career impacts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching career impacts
	*/
	public static List<CareerImpact> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<CareerImpact> orderByComparator) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the career impacts where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of career impacts
	* @param end the upper bound of the range of career impacts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching career impacts
	*/
	public static List<CareerImpact> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<CareerImpact> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first career impact in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching career impact
	* @throws NoSuchCareerImpactException if a matching career impact could not be found
	*/
	public static CareerImpact findByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<CareerImpact> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchCareerImpactException {
		return getPersistence()
				   .findByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the first career impact in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching career impact, or <code>null</code> if a matching career impact could not be found
	*/
	public static CareerImpact fetchByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<CareerImpact> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last career impact in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching career impact
	* @throws NoSuchCareerImpactException if a matching career impact could not be found
	*/
	public static CareerImpact findByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<CareerImpact> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchCareerImpactException {
		return getPersistence()
				   .findByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last career impact in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching career impact, or <code>null</code> if a matching career impact could not be found
	*/
	public static CareerImpact fetchByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<CareerImpact> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the career impacts before and after the current career impact in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param careerImpactId the primary key of the current career impact
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next career impact
	* @throws NoSuchCareerImpactException if a career impact with the primary key could not be found
	*/
	public static CareerImpact[] findByUuid_C_PrevAndNext(long careerImpactId,
		java.lang.String uuid, long companyId,
		OrderByComparator<CareerImpact> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchCareerImpactException {
		return getPersistence()
				   .findByUuid_C_PrevAndNext(careerImpactId, uuid, companyId,
			orderByComparator);
	}

	/**
	* Removes all the career impacts where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public static void removeByUuid_C(java.lang.String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	* Returns the number of career impacts where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching career impacts
	*/
	public static int countByUuid_C(java.lang.String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	* Returns the career impact where careerImpactId = &#63; or throws a {@link NoSuchCareerImpactException} if it could not be found.
	*
	* @param careerImpactId the career impact ID
	* @return the matching career impact
	* @throws NoSuchCareerImpactException if a matching career impact could not be found
	*/
	public static CareerImpact findByCareerImpactId(long careerImpactId)
		throws com.collaboratelab.impact.exception.NoSuchCareerImpactException {
		return getPersistence().findByCareerImpactId(careerImpactId);
	}

	/**
	* Returns the career impact where careerImpactId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param careerImpactId the career impact ID
	* @return the matching career impact, or <code>null</code> if a matching career impact could not be found
	*/
	public static CareerImpact fetchByCareerImpactId(long careerImpactId) {
		return getPersistence().fetchByCareerImpactId(careerImpactId);
	}

	/**
	* Returns the career impact where careerImpactId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param careerImpactId the career impact ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching career impact, or <code>null</code> if a matching career impact could not be found
	*/
	public static CareerImpact fetchByCareerImpactId(long careerImpactId,
		boolean retrieveFromCache) {
		return getPersistence()
				   .fetchByCareerImpactId(careerImpactId, retrieveFromCache);
	}

	/**
	* Removes the career impact where careerImpactId = &#63; from the database.
	*
	* @param careerImpactId the career impact ID
	* @return the career impact that was removed
	*/
	public static CareerImpact removeByCareerImpactId(long careerImpactId)
		throws com.collaboratelab.impact.exception.NoSuchCareerImpactException {
		return getPersistence().removeByCareerImpactId(careerImpactId);
	}

	/**
	* Returns the number of career impacts where careerImpactId = &#63;.
	*
	* @param careerImpactId the career impact ID
	* @return the number of matching career impacts
	*/
	public static int countByCareerImpactId(long careerImpactId) {
		return getPersistence().countByCareerImpactId(careerImpactId);
	}

	/**
	* Returns all the career impacts where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the matching career impacts
	*/
	public static List<CareerImpact> findByGroupId(long groupId) {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	* Returns a range of all the career impacts where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of career impacts
	* @param end the upper bound of the range of career impacts (not inclusive)
	* @return the range of matching career impacts
	*/
	public static List<CareerImpact> findByGroupId(long groupId, int start,
		int end) {
		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	* Returns an ordered range of all the career impacts where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of career impacts
	* @param end the upper bound of the range of career impacts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching career impacts
	*/
	public static List<CareerImpact> findByGroupId(long groupId, int start,
		int end, OrderByComparator<CareerImpact> orderByComparator) {
		return getPersistence()
				   .findByGroupId(groupId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the career impacts where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of career impacts
	* @param end the upper bound of the range of career impacts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching career impacts
	*/
	public static List<CareerImpact> findByGroupId(long groupId, int start,
		int end, OrderByComparator<CareerImpact> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByGroupId(groupId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first career impact in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching career impact
	* @throws NoSuchCareerImpactException if a matching career impact could not be found
	*/
	public static CareerImpact findByGroupId_First(long groupId,
		OrderByComparator<CareerImpact> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchCareerImpactException {
		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	* Returns the first career impact in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching career impact, or <code>null</code> if a matching career impact could not be found
	*/
	public static CareerImpact fetchByGroupId_First(long groupId,
		OrderByComparator<CareerImpact> orderByComparator) {
		return getPersistence().fetchByGroupId_First(groupId, orderByComparator);
	}

	/**
	* Returns the last career impact in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching career impact
	* @throws NoSuchCareerImpactException if a matching career impact could not be found
	*/
	public static CareerImpact findByGroupId_Last(long groupId,
		OrderByComparator<CareerImpact> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchCareerImpactException {
		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	* Returns the last career impact in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching career impact, or <code>null</code> if a matching career impact could not be found
	*/
	public static CareerImpact fetchByGroupId_Last(long groupId,
		OrderByComparator<CareerImpact> orderByComparator) {
		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	* Returns the career impacts before and after the current career impact in the ordered set where groupId = &#63;.
	*
	* @param careerImpactId the primary key of the current career impact
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next career impact
	* @throws NoSuchCareerImpactException if a career impact with the primary key could not be found
	*/
	public static CareerImpact[] findByGroupId_PrevAndNext(
		long careerImpactId, long groupId,
		OrderByComparator<CareerImpact> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchCareerImpactException {
		return getPersistence()
				   .findByGroupId_PrevAndNext(careerImpactId, groupId,
			orderByComparator);
	}

	/**
	* Removes all the career impacts where groupId = &#63; from the database.
	*
	* @param groupId the group ID
	*/
	public static void removeByGroupId(long groupId) {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	* Returns the number of career impacts where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the number of matching career impacts
	*/
	public static int countByGroupId(long groupId) {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	* Caches the career impact in the entity cache if it is enabled.
	*
	* @param careerImpact the career impact
	*/
	public static void cacheResult(CareerImpact careerImpact) {
		getPersistence().cacheResult(careerImpact);
	}

	/**
	* Caches the career impacts in the entity cache if it is enabled.
	*
	* @param careerImpacts the career impacts
	*/
	public static void cacheResult(List<CareerImpact> careerImpacts) {
		getPersistence().cacheResult(careerImpacts);
	}

	/**
	* Creates a new career impact with the primary key. Does not add the career impact to the database.
	*
	* @param careerImpactId the primary key for the new career impact
	* @return the new career impact
	*/
	public static CareerImpact create(long careerImpactId) {
		return getPersistence().create(careerImpactId);
	}

	/**
	* Removes the career impact with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param careerImpactId the primary key of the career impact
	* @return the career impact that was removed
	* @throws NoSuchCareerImpactException if a career impact with the primary key could not be found
	*/
	public static CareerImpact remove(long careerImpactId)
		throws com.collaboratelab.impact.exception.NoSuchCareerImpactException {
		return getPersistence().remove(careerImpactId);
	}

	public static CareerImpact updateImpl(CareerImpact careerImpact) {
		return getPersistence().updateImpl(careerImpact);
	}

	/**
	* Returns the career impact with the primary key or throws a {@link NoSuchCareerImpactException} if it could not be found.
	*
	* @param careerImpactId the primary key of the career impact
	* @return the career impact
	* @throws NoSuchCareerImpactException if a career impact with the primary key could not be found
	*/
	public static CareerImpact findByPrimaryKey(long careerImpactId)
		throws com.collaboratelab.impact.exception.NoSuchCareerImpactException {
		return getPersistence().findByPrimaryKey(careerImpactId);
	}

	/**
	* Returns the career impact with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param careerImpactId the primary key of the career impact
	* @return the career impact, or <code>null</code> if a career impact with the primary key could not be found
	*/
	public static CareerImpact fetchByPrimaryKey(long careerImpactId) {
		return getPersistence().fetchByPrimaryKey(careerImpactId);
	}

	public static java.util.Map<java.io.Serializable, CareerImpact> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the career impacts.
	*
	* @return the career impacts
	*/
	public static List<CareerImpact> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the career impacts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of career impacts
	* @param end the upper bound of the range of career impacts (not inclusive)
	* @return the range of career impacts
	*/
	public static List<CareerImpact> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the career impacts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of career impacts
	* @param end the upper bound of the range of career impacts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of career impacts
	*/
	public static List<CareerImpact> findAll(int start, int end,
		OrderByComparator<CareerImpact> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the career impacts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of career impacts
	* @param end the upper bound of the range of career impacts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of career impacts
	*/
	public static List<CareerImpact> findAll(int start, int end,
		OrderByComparator<CareerImpact> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the career impacts from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of career impacts.
	*
	* @return the number of career impacts
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static java.util.Set<java.lang.String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static CareerImpactPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<CareerImpactPersistence, CareerImpactPersistence> _serviceTracker =
		ServiceTrackerFactory.open(CareerImpactPersistence.class);
}