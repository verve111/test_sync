/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.impact.model.Publications;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the publications service. This utility wraps {@link com.collaboratelab.impact.service.persistence.impl.PublicationsPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see PublicationsPersistence
 * @see com.collaboratelab.impact.service.persistence.impl.PublicationsPersistenceImpl
 * @generated
 */
@ProviderType
public class PublicationsUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Publications publications) {
		getPersistence().clearCache(publications);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Publications> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Publications> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Publications> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Publications> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Publications update(Publications publications) {
		return getPersistence().update(publications);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Publications update(Publications publications,
		ServiceContext serviceContext) {
		return getPersistence().update(publications, serviceContext);
	}

	/**
	* Returns all the publicationses where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching publicationses
	*/
	public static List<Publications> findByUuid(java.lang.String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	* Returns a range of all the publicationses where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PublicationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of publicationses
	* @param end the upper bound of the range of publicationses (not inclusive)
	* @return the range of matching publicationses
	*/
	public static List<Publications> findByUuid(java.lang.String uuid,
		int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	* Returns an ordered range of all the publicationses where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PublicationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of publicationses
	* @param end the upper bound of the range of publicationses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching publicationses
	*/
	public static List<Publications> findByUuid(java.lang.String uuid,
		int start, int end, OrderByComparator<Publications> orderByComparator) {
		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the publicationses where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PublicationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of publicationses
	* @param end the upper bound of the range of publicationses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching publicationses
	*/
	public static List<Publications> findByUuid(java.lang.String uuid,
		int start, int end, OrderByComparator<Publications> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid(uuid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first publications in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching publications
	* @throws NoSuchPublicationsException if a matching publications could not be found
	*/
	public static Publications findByUuid_First(java.lang.String uuid,
		OrderByComparator<Publications> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchPublicationsException {
		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the first publications in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching publications, or <code>null</code> if a matching publications could not be found
	*/
	public static Publications fetchByUuid_First(java.lang.String uuid,
		OrderByComparator<Publications> orderByComparator) {
		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the last publications in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching publications
	* @throws NoSuchPublicationsException if a matching publications could not be found
	*/
	public static Publications findByUuid_Last(java.lang.String uuid,
		OrderByComparator<Publications> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchPublicationsException {
		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the last publications in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching publications, or <code>null</code> if a matching publications could not be found
	*/
	public static Publications fetchByUuid_Last(java.lang.String uuid,
		OrderByComparator<Publications> orderByComparator) {
		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the publicationses before and after the current publications in the ordered set where uuid = &#63;.
	*
	* @param publicationsId the primary key of the current publications
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next publications
	* @throws NoSuchPublicationsException if a publications with the primary key could not be found
	*/
	public static Publications[] findByUuid_PrevAndNext(long publicationsId,
		java.lang.String uuid, OrderByComparator<Publications> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchPublicationsException {
		return getPersistence()
				   .findByUuid_PrevAndNext(publicationsId, uuid,
			orderByComparator);
	}

	/**
	* Removes all the publicationses where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public static void removeByUuid(java.lang.String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	* Returns the number of publicationses where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching publicationses
	*/
	public static int countByUuid(java.lang.String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	* Returns the publications where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchPublicationsException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching publications
	* @throws NoSuchPublicationsException if a matching publications could not be found
	*/
	public static Publications findByUUID_G(java.lang.String uuid, long groupId)
		throws com.collaboratelab.impact.exception.NoSuchPublicationsException {
		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	* Returns the publications where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching publications, or <code>null</code> if a matching publications could not be found
	*/
	public static Publications fetchByUUID_G(java.lang.String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	* Returns the publications where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching publications, or <code>null</code> if a matching publications could not be found
	*/
	public static Publications fetchByUUID_G(java.lang.String uuid,
		long groupId, boolean retrieveFromCache) {
		return getPersistence().fetchByUUID_G(uuid, groupId, retrieveFromCache);
	}

	/**
	* Removes the publications where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the publications that was removed
	*/
	public static Publications removeByUUID_G(java.lang.String uuid,
		long groupId)
		throws com.collaboratelab.impact.exception.NoSuchPublicationsException {
		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	* Returns the number of publicationses where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching publicationses
	*/
	public static int countByUUID_G(java.lang.String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	* Returns all the publicationses where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching publicationses
	*/
	public static List<Publications> findByUuid_C(java.lang.String uuid,
		long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	* Returns a range of all the publicationses where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PublicationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of publicationses
	* @param end the upper bound of the range of publicationses (not inclusive)
	* @return the range of matching publicationses
	*/
	public static List<Publications> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end) {
		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	* Returns an ordered range of all the publicationses where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PublicationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of publicationses
	* @param end the upper bound of the range of publicationses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching publicationses
	*/
	public static List<Publications> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<Publications> orderByComparator) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the publicationses where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PublicationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of publicationses
	* @param end the upper bound of the range of publicationses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching publicationses
	*/
	public static List<Publications> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<Publications> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first publications in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching publications
	* @throws NoSuchPublicationsException if a matching publications could not be found
	*/
	public static Publications findByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<Publications> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchPublicationsException {
		return getPersistence()
				   .findByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the first publications in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching publications, or <code>null</code> if a matching publications could not be found
	*/
	public static Publications fetchByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<Publications> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last publications in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching publications
	* @throws NoSuchPublicationsException if a matching publications could not be found
	*/
	public static Publications findByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<Publications> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchPublicationsException {
		return getPersistence()
				   .findByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last publications in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching publications, or <code>null</code> if a matching publications could not be found
	*/
	public static Publications fetchByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<Publications> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the publicationses before and after the current publications in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param publicationsId the primary key of the current publications
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next publications
	* @throws NoSuchPublicationsException if a publications with the primary key could not be found
	*/
	public static Publications[] findByUuid_C_PrevAndNext(long publicationsId,
		java.lang.String uuid, long companyId,
		OrderByComparator<Publications> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchPublicationsException {
		return getPersistence()
				   .findByUuid_C_PrevAndNext(publicationsId, uuid, companyId,
			orderByComparator);
	}

	/**
	* Removes all the publicationses where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public static void removeByUuid_C(java.lang.String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	* Returns the number of publicationses where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching publicationses
	*/
	public static int countByUuid_C(java.lang.String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	* Returns the publications where publicationsId = &#63; or throws a {@link NoSuchPublicationsException} if it could not be found.
	*
	* @param publicationsId the publications ID
	* @return the matching publications
	* @throws NoSuchPublicationsException if a matching publications could not be found
	*/
	public static Publications findByPublicationsId(long publicationsId)
		throws com.collaboratelab.impact.exception.NoSuchPublicationsException {
		return getPersistence().findByPublicationsId(publicationsId);
	}

	/**
	* Returns the publications where publicationsId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param publicationsId the publications ID
	* @return the matching publications, or <code>null</code> if a matching publications could not be found
	*/
	public static Publications fetchByPublicationsId(long publicationsId) {
		return getPersistence().fetchByPublicationsId(publicationsId);
	}

	/**
	* Returns the publications where publicationsId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param publicationsId the publications ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching publications, or <code>null</code> if a matching publications could not be found
	*/
	public static Publications fetchByPublicationsId(long publicationsId,
		boolean retrieveFromCache) {
		return getPersistence()
				   .fetchByPublicationsId(publicationsId, retrieveFromCache);
	}

	/**
	* Removes the publications where publicationsId = &#63; from the database.
	*
	* @param publicationsId the publications ID
	* @return the publications that was removed
	*/
	public static Publications removeByPublicationsId(long publicationsId)
		throws com.collaboratelab.impact.exception.NoSuchPublicationsException {
		return getPersistence().removeByPublicationsId(publicationsId);
	}

	/**
	* Returns the number of publicationses where publicationsId = &#63;.
	*
	* @param publicationsId the publications ID
	* @return the number of matching publicationses
	*/
	public static int countByPublicationsId(long publicationsId) {
		return getPersistence().countByPublicationsId(publicationsId);
	}

	/**
	* Returns all the publicationses where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the matching publicationses
	*/
	public static List<Publications> findByGroupId(long groupId) {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	* Returns a range of all the publicationses where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PublicationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of publicationses
	* @param end the upper bound of the range of publicationses (not inclusive)
	* @return the range of matching publicationses
	*/
	public static List<Publications> findByGroupId(long groupId, int start,
		int end) {
		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	* Returns an ordered range of all the publicationses where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PublicationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of publicationses
	* @param end the upper bound of the range of publicationses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching publicationses
	*/
	public static List<Publications> findByGroupId(long groupId, int start,
		int end, OrderByComparator<Publications> orderByComparator) {
		return getPersistence()
				   .findByGroupId(groupId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the publicationses where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PublicationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of publicationses
	* @param end the upper bound of the range of publicationses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching publicationses
	*/
	public static List<Publications> findByGroupId(long groupId, int start,
		int end, OrderByComparator<Publications> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByGroupId(groupId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first publications in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching publications
	* @throws NoSuchPublicationsException if a matching publications could not be found
	*/
	public static Publications findByGroupId_First(long groupId,
		OrderByComparator<Publications> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchPublicationsException {
		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	* Returns the first publications in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching publications, or <code>null</code> if a matching publications could not be found
	*/
	public static Publications fetchByGroupId_First(long groupId,
		OrderByComparator<Publications> orderByComparator) {
		return getPersistence().fetchByGroupId_First(groupId, orderByComparator);
	}

	/**
	* Returns the last publications in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching publications
	* @throws NoSuchPublicationsException if a matching publications could not be found
	*/
	public static Publications findByGroupId_Last(long groupId,
		OrderByComparator<Publications> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchPublicationsException {
		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	* Returns the last publications in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching publications, or <code>null</code> if a matching publications could not be found
	*/
	public static Publications fetchByGroupId_Last(long groupId,
		OrderByComparator<Publications> orderByComparator) {
		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	* Returns the publicationses before and after the current publications in the ordered set where groupId = &#63;.
	*
	* @param publicationsId the primary key of the current publications
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next publications
	* @throws NoSuchPublicationsException if a publications with the primary key could not be found
	*/
	public static Publications[] findByGroupId_PrevAndNext(
		long publicationsId, long groupId,
		OrderByComparator<Publications> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchPublicationsException {
		return getPersistence()
				   .findByGroupId_PrevAndNext(publicationsId, groupId,
			orderByComparator);
	}

	/**
	* Removes all the publicationses where groupId = &#63; from the database.
	*
	* @param groupId the group ID
	*/
	public static void removeByGroupId(long groupId) {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	* Returns the number of publicationses where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the number of matching publicationses
	*/
	public static int countByGroupId(long groupId) {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	* Caches the publications in the entity cache if it is enabled.
	*
	* @param publications the publications
	*/
	public static void cacheResult(Publications publications) {
		getPersistence().cacheResult(publications);
	}

	/**
	* Caches the publicationses in the entity cache if it is enabled.
	*
	* @param publicationses the publicationses
	*/
	public static void cacheResult(List<Publications> publicationses) {
		getPersistence().cacheResult(publicationses);
	}

	/**
	* Creates a new publications with the primary key. Does not add the publications to the database.
	*
	* @param publicationsId the primary key for the new publications
	* @return the new publications
	*/
	public static Publications create(long publicationsId) {
		return getPersistence().create(publicationsId);
	}

	/**
	* Removes the publications with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param publicationsId the primary key of the publications
	* @return the publications that was removed
	* @throws NoSuchPublicationsException if a publications with the primary key could not be found
	*/
	public static Publications remove(long publicationsId)
		throws com.collaboratelab.impact.exception.NoSuchPublicationsException {
		return getPersistence().remove(publicationsId);
	}

	public static Publications updateImpl(Publications publications) {
		return getPersistence().updateImpl(publications);
	}

	/**
	* Returns the publications with the primary key or throws a {@link NoSuchPublicationsException} if it could not be found.
	*
	* @param publicationsId the primary key of the publications
	* @return the publications
	* @throws NoSuchPublicationsException if a publications with the primary key could not be found
	*/
	public static Publications findByPrimaryKey(long publicationsId)
		throws com.collaboratelab.impact.exception.NoSuchPublicationsException {
		return getPersistence().findByPrimaryKey(publicationsId);
	}

	/**
	* Returns the publications with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param publicationsId the primary key of the publications
	* @return the publications, or <code>null</code> if a publications with the primary key could not be found
	*/
	public static Publications fetchByPrimaryKey(long publicationsId) {
		return getPersistence().fetchByPrimaryKey(publicationsId);
	}

	public static java.util.Map<java.io.Serializable, Publications> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the publicationses.
	*
	* @return the publicationses
	*/
	public static List<Publications> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the publicationses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PublicationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of publicationses
	* @param end the upper bound of the range of publicationses (not inclusive)
	* @return the range of publicationses
	*/
	public static List<Publications> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the publicationses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PublicationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of publicationses
	* @param end the upper bound of the range of publicationses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of publicationses
	*/
	public static List<Publications> findAll(int start, int end,
		OrderByComparator<Publications> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the publicationses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PublicationsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of publicationses
	* @param end the upper bound of the range of publicationses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of publicationses
	*/
	public static List<Publications> findAll(int start, int end,
		OrderByComparator<Publications> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the publicationses from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of publicationses.
	*
	* @return the number of publicationses
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static java.util.Set<java.lang.String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static PublicationsPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<PublicationsPersistence, PublicationsPersistence> _serviceTracker =
		ServiceTrackerFactory.open(PublicationsPersistence.class);
}