/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.exportimport.kernel.lar.StagedModelType;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link CareerImpact}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CareerImpact
 * @generated
 */
@ProviderType
public class CareerImpactWrapper implements CareerImpact,
	ModelWrapper<CareerImpact> {
	public CareerImpactWrapper(CareerImpact careerImpact) {
		_careerImpact = careerImpact;
	}

	@Override
	public Class<?> getModelClass() {
		return CareerImpact.class;
	}

	@Override
	public String getModelClassName() {
		return CareerImpact.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("careerImpactId", getCareerImpactId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("asOfDate", getAsOfDate());
		attributes.put("categoryRaing", getCategoryRaing());
		attributes.put("optionalComments", getOptionalComments());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long careerImpactId = (Long)attributes.get("careerImpactId");

		if (careerImpactId != null) {
			setCareerImpactId(careerImpactId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Date asOfDate = (Date)attributes.get("asOfDate");

		if (asOfDate != null) {
			setAsOfDate(asOfDate);
		}

		Integer categoryRaing = (Integer)attributes.get("categoryRaing");

		if (categoryRaing != null) {
			setCategoryRaing(categoryRaing);
		}

		String optionalComments = (String)attributes.get("optionalComments");

		if (optionalComments != null) {
			setOptionalComments(optionalComments);
		}
	}

	@Override
	public CareerImpact toEscapedModel() {
		return new CareerImpactWrapper(_careerImpact.toEscapedModel());
	}

	@Override
	public CareerImpact toUnescapedModel() {
		return new CareerImpactWrapper(_careerImpact.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _careerImpact.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _careerImpact.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _careerImpact.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _careerImpact.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<CareerImpact> toCacheModel() {
		return _careerImpact.toCacheModel();
	}

	@Override
	public int compareTo(CareerImpact careerImpact) {
		return _careerImpact.compareTo(careerImpact);
	}

	/**
	* Returns the category raing of this career impact.
	*
	* @return the category raing of this career impact
	*/
	@Override
	public int getCategoryRaing() {
		return _careerImpact.getCategoryRaing();
	}

	@Override
	public int hashCode() {
		return _careerImpact.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _careerImpact.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new CareerImpactWrapper((CareerImpact)_careerImpact.clone());
	}

	/**
	* Returns the optional comments of this career impact.
	*
	* @return the optional comments of this career impact
	*/
	@Override
	public java.lang.String getOptionalComments() {
		return _careerImpact.getOptionalComments();
	}

	/**
	* Returns the user name of this career impact.
	*
	* @return the user name of this career impact
	*/
	@Override
	public java.lang.String getUserName() {
		return _careerImpact.getUserName();
	}

	/**
	* Returns the user uuid of this career impact.
	*
	* @return the user uuid of this career impact
	*/
	@Override
	public java.lang.String getUserUuid() {
		return _careerImpact.getUserUuid();
	}

	/**
	* Returns the uuid of this career impact.
	*
	* @return the uuid of this career impact
	*/
	@Override
	public java.lang.String getUuid() {
		return _careerImpact.getUuid();
	}

	@Override
	public java.lang.String toString() {
		return _careerImpact.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _careerImpact.toXmlString();
	}

	/**
	* Returns the as of date of this career impact.
	*
	* @return the as of date of this career impact
	*/
	@Override
	public Date getAsOfDate() {
		return _careerImpact.getAsOfDate();
	}

	/**
	* Returns the create date of this career impact.
	*
	* @return the create date of this career impact
	*/
	@Override
	public Date getCreateDate() {
		return _careerImpact.getCreateDate();
	}

	/**
	* Returns the modified date of this career impact.
	*
	* @return the modified date of this career impact
	*/
	@Override
	public Date getModifiedDate() {
		return _careerImpact.getModifiedDate();
	}

	/**
	* Returns the career impact ID of this career impact.
	*
	* @return the career impact ID of this career impact
	*/
	@Override
	public long getCareerImpactId() {
		return _careerImpact.getCareerImpactId();
	}

	/**
	* Returns the company ID of this career impact.
	*
	* @return the company ID of this career impact
	*/
	@Override
	public long getCompanyId() {
		return _careerImpact.getCompanyId();
	}

	/**
	* Returns the group ID of this career impact.
	*
	* @return the group ID of this career impact
	*/
	@Override
	public long getGroupId() {
		return _careerImpact.getGroupId();
	}

	/**
	* Returns the primary key of this career impact.
	*
	* @return the primary key of this career impact
	*/
	@Override
	public long getPrimaryKey() {
		return _careerImpact.getPrimaryKey();
	}

	/**
	* Returns the user ID of this career impact.
	*
	* @return the user ID of this career impact
	*/
	@Override
	public long getUserId() {
		return _careerImpact.getUserId();
	}

	@Override
	public void persist() {
		_careerImpact.persist();
	}

	/**
	* Sets the as of date of this career impact.
	*
	* @param asOfDate the as of date of this career impact
	*/
	@Override
	public void setAsOfDate(Date asOfDate) {
		_careerImpact.setAsOfDate(asOfDate);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_careerImpact.setCachedModel(cachedModel);
	}

	/**
	* Sets the career impact ID of this career impact.
	*
	* @param careerImpactId the career impact ID of this career impact
	*/
	@Override
	public void setCareerImpactId(long careerImpactId) {
		_careerImpact.setCareerImpactId(careerImpactId);
	}

	/**
	* Sets the category raing of this career impact.
	*
	* @param categoryRaing the category raing of this career impact
	*/
	@Override
	public void setCategoryRaing(int categoryRaing) {
		_careerImpact.setCategoryRaing(categoryRaing);
	}

	/**
	* Sets the company ID of this career impact.
	*
	* @param companyId the company ID of this career impact
	*/
	@Override
	public void setCompanyId(long companyId) {
		_careerImpact.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this career impact.
	*
	* @param createDate the create date of this career impact
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_careerImpact.setCreateDate(createDate);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_careerImpact.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_careerImpact.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_careerImpact.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the group ID of this career impact.
	*
	* @param groupId the group ID of this career impact
	*/
	@Override
	public void setGroupId(long groupId) {
		_careerImpact.setGroupId(groupId);
	}

	/**
	* Sets the modified date of this career impact.
	*
	* @param modifiedDate the modified date of this career impact
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_careerImpact.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_careerImpact.setNew(n);
	}

	/**
	* Sets the optional comments of this career impact.
	*
	* @param optionalComments the optional comments of this career impact
	*/
	@Override
	public void setOptionalComments(java.lang.String optionalComments) {
		_careerImpact.setOptionalComments(optionalComments);
	}

	/**
	* Sets the primary key of this career impact.
	*
	* @param primaryKey the primary key of this career impact
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_careerImpact.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_careerImpact.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the user ID of this career impact.
	*
	* @param userId the user ID of this career impact
	*/
	@Override
	public void setUserId(long userId) {
		_careerImpact.setUserId(userId);
	}

	/**
	* Sets the user name of this career impact.
	*
	* @param userName the user name of this career impact
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_careerImpact.setUserName(userName);
	}

	/**
	* Sets the user uuid of this career impact.
	*
	* @param userUuid the user uuid of this career impact
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_careerImpact.setUserUuid(userUuid);
	}

	/**
	* Sets the uuid of this career impact.
	*
	* @param uuid the uuid of this career impact
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_careerImpact.setUuid(uuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CareerImpactWrapper)) {
			return false;
		}

		CareerImpactWrapper careerImpactWrapper = (CareerImpactWrapper)obj;

		if (Objects.equals(_careerImpact, careerImpactWrapper._careerImpact)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _careerImpact.getStagedModelType();
	}

	@Override
	public CareerImpact getWrappedModel() {
		return _careerImpact;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _careerImpact.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _careerImpact.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_careerImpact.resetOriginalValues();
	}

	private final CareerImpact _careerImpact;
}