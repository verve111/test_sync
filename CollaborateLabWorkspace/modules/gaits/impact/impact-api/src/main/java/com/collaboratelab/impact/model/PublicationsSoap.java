/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.collaboratelab.impact.service.http.PublicationsServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see com.collaboratelab.impact.service.http.PublicationsServiceSoap
 * @generated
 */
@ProviderType
public class PublicationsSoap implements Serializable {
	public static PublicationsSoap toSoapModel(Publications model) {
		PublicationsSoap soapModel = new PublicationsSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setPublicationsId(model.getPublicationsId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setTitleOfPublications(model.getTitleOfPublications());
		soapModel.setDate(model.getDate());
		soapModel.setURL(model.getURL());
		soapModel.setComments(model.getComments());

		return soapModel;
	}

	public static PublicationsSoap[] toSoapModels(Publications[] models) {
		PublicationsSoap[] soapModels = new PublicationsSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static PublicationsSoap[][] toSoapModels(Publications[][] models) {
		PublicationsSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new PublicationsSoap[models.length][models[0].length];
		}
		else {
			soapModels = new PublicationsSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static PublicationsSoap[] toSoapModels(List<Publications> models) {
		List<PublicationsSoap> soapModels = new ArrayList<PublicationsSoap>(models.size());

		for (Publications model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new PublicationsSoap[soapModels.size()]);
	}

	public PublicationsSoap() {
	}

	public long getPrimaryKey() {
		return _publicationsId;
	}

	public void setPrimaryKey(long pk) {
		setPublicationsId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getPublicationsId() {
		return _publicationsId;
	}

	public void setPublicationsId(long publicationsId) {
		_publicationsId = publicationsId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getTitleOfPublications() {
		return _titleOfPublications;
	}

	public void setTitleOfPublications(String titleOfPublications) {
		_titleOfPublications = titleOfPublications;
	}

	public Date getDate() {
		return _date;
	}

	public void setDate(Date date) {
		_date = date;
	}

	public String getURL() {
		return _URL;
	}

	public void setURL(String URL) {
		_URL = URL;
	}

	public String getComments() {
		return _comments;
	}

	public void setComments(String comments) {
		_comments = comments;
	}

	private String _uuid;
	private long _publicationsId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _titleOfPublications;
	private Date _date;
	private String _URL;
	private String _comments;
}