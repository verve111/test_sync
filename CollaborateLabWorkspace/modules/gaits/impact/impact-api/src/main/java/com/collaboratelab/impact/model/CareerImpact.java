/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

/**
 * The extended model interface for the CareerImpact service. Represents a row in the &quot;CLab_Impact_CareerImpact&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see CareerImpactModel
 * @see com.collaboratelab.impact.model.impl.CareerImpactImpl
 * @see com.collaboratelab.impact.model.impl.CareerImpactModelImpl
 * @generated
 */
@ImplementationClassName("com.collaboratelab.impact.model.impl.CareerImpactImpl")
@ProviderType
public interface CareerImpact extends CareerImpactModel, PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link com.collaboratelab.impact.model.impl.CareerImpactImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<CareerImpact, Long> CAREER_IMPACT_ID_ACCESSOR = new Accessor<CareerImpact, Long>() {
			@Override
			public Long get(CareerImpact careerImpact) {
				return careerImpact.getCareerImpactId();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<CareerImpact> getTypeClass() {
				return CareerImpact.class;
			}
		};
}