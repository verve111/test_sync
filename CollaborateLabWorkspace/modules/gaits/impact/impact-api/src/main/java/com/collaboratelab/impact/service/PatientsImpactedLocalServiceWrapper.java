/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link PatientsImpactedLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see PatientsImpactedLocalService
 * @generated
 */
@ProviderType
public class PatientsImpactedLocalServiceWrapper
	implements PatientsImpactedLocalService,
		ServiceWrapper<PatientsImpactedLocalService> {
	public PatientsImpactedLocalServiceWrapper(
		PatientsImpactedLocalService patientsImpactedLocalService) {
		_patientsImpactedLocalService = patientsImpactedLocalService;
	}

	/**
	* Adds the patients impacted to the database. Also notifies the appropriate model listeners.
	*
	* @param patientsImpacted the patients impacted
	* @return the patients impacted that was added
	*/
	@Override
	public com.collaboratelab.impact.model.PatientsImpacted addPatientsImpacted(
		com.collaboratelab.impact.model.PatientsImpacted patientsImpacted) {
		return _patientsImpactedLocalService.addPatientsImpacted(patientsImpacted);
	}

	/**
	* Creates a new patients impacted with the primary key. Does not add the patients impacted to the database.
	*
	* @param patientImpactedId the primary key for the new patients impacted
	* @return the new patients impacted
	*/
	@Override
	public com.collaboratelab.impact.model.PatientsImpacted createPatientsImpacted(
		long patientImpactedId) {
		return _patientsImpactedLocalService.createPatientsImpacted(patientImpactedId);
	}

	/**
	* Deletes the patients impacted from the database. Also notifies the appropriate model listeners.
	*
	* @param patientsImpacted the patients impacted
	* @return the patients impacted that was removed
	*/
	@Override
	public com.collaboratelab.impact.model.PatientsImpacted deletePatientsImpacted(
		com.collaboratelab.impact.model.PatientsImpacted patientsImpacted) {
		return _patientsImpactedLocalService.deletePatientsImpacted(patientsImpacted);
	}

	/**
	* Deletes the patients impacted with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param patientImpactedId the primary key of the patients impacted
	* @return the patients impacted that was removed
	* @throws PortalException if a patients impacted with the primary key could not be found
	*/
	@Override
	public com.collaboratelab.impact.model.PatientsImpacted deletePatientsImpacted(
		long patientImpactedId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _patientsImpactedLocalService.deletePatientsImpacted(patientImpactedId);
	}

	@Override
	public com.collaboratelab.impact.model.PatientsImpacted fetchPatientsImpacted(
		long patientImpactedId) {
		return _patientsImpactedLocalService.fetchPatientsImpacted(patientImpactedId);
	}

	/**
	* Returns the patients impacted matching the UUID and group.
	*
	* @param uuid the patients impacted's UUID
	* @param groupId the primary key of the group
	* @return the matching patients impacted, or <code>null</code> if a matching patients impacted could not be found
	*/
	@Override
	public com.collaboratelab.impact.model.PatientsImpacted fetchPatientsImpactedByUuidAndGroupId(
		java.lang.String uuid, long groupId) {
		return _patientsImpactedLocalService.fetchPatientsImpactedByUuidAndGroupId(uuid,
			groupId);
	}

	/**
	* Returns the patients impacted with the primary key.
	*
	* @param patientImpactedId the primary key of the patients impacted
	* @return the patients impacted
	* @throws PortalException if a patients impacted with the primary key could not be found
	*/
	@Override
	public com.collaboratelab.impact.model.PatientsImpacted getPatientsImpacted(
		long patientImpactedId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _patientsImpactedLocalService.getPatientsImpacted(patientImpactedId);
	}

	/**
	* Returns the patients impacted matching the UUID and group.
	*
	* @param uuid the patients impacted's UUID
	* @param groupId the primary key of the group
	* @return the matching patients impacted
	* @throws PortalException if a matching patients impacted could not be found
	*/
	@Override
	public com.collaboratelab.impact.model.PatientsImpacted getPatientsImpactedByUuidAndGroupId(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _patientsImpactedLocalService.getPatientsImpactedByUuidAndGroupId(uuid,
			groupId);
	}

	/**
	* Updates the patients impacted in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param patientsImpacted the patients impacted
	* @return the patients impacted that was updated
	*/
	@Override
	public com.collaboratelab.impact.model.PatientsImpacted updatePatientsImpacted(
		com.collaboratelab.impact.model.PatientsImpacted patientsImpacted) {
		return _patientsImpactedLocalService.updatePatientsImpacted(patientsImpacted);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _patientsImpactedLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _patientsImpactedLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery getExportActionableDynamicQuery(
		com.liferay.exportimport.kernel.lar.PortletDataContext portletDataContext) {
		return _patientsImpactedLocalService.getExportActionableDynamicQuery(portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _patientsImpactedLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _patientsImpactedLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _patientsImpactedLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of patients impacteds.
	*
	* @return the number of patients impacteds
	*/
	@Override
	public int getPatientsImpactedsCount() {
		return _patientsImpactedLocalService.getPatientsImpactedsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _patientsImpactedLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _patientsImpactedLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.impact.model.impl.PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _patientsImpactedLocalService.dynamicQuery(dynamicQuery, start,
			end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.impact.model.impl.PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _patientsImpactedLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	@Override
	public java.util.List<com.collaboratelab.impact.model.PatientsImpacted> findByGroupId(
		long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _patientsImpactedLocalService.findByGroupId(groupId);
	}

	/**
	* Returns a range of all the patients impacteds.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.impact.model.impl.PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of patients impacteds
	* @param end the upper bound of the range of patients impacteds (not inclusive)
	* @return the range of patients impacteds
	*/
	@Override
	public java.util.List<com.collaboratelab.impact.model.PatientsImpacted> getPatientsImpacteds(
		int start, int end) {
		return _patientsImpactedLocalService.getPatientsImpacteds(start, end);
	}

	/**
	* Returns all the patients impacteds matching the UUID and company.
	*
	* @param uuid the UUID of the patients impacteds
	* @param companyId the primary key of the company
	* @return the matching patients impacteds, or an empty list if no matches were found
	*/
	@Override
	public java.util.List<com.collaboratelab.impact.model.PatientsImpacted> getPatientsImpactedsByUuidAndCompanyId(
		java.lang.String uuid, long companyId) {
		return _patientsImpactedLocalService.getPatientsImpactedsByUuidAndCompanyId(uuid,
			companyId);
	}

	/**
	* Returns a range of patients impacteds matching the UUID and company.
	*
	* @param uuid the UUID of the patients impacteds
	* @param companyId the primary key of the company
	* @param start the lower bound of the range of patients impacteds
	* @param end the upper bound of the range of patients impacteds (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the range of matching patients impacteds, or an empty list if no matches were found
	*/
	@Override
	public java.util.List<com.collaboratelab.impact.model.PatientsImpacted> getPatientsImpactedsByUuidAndCompanyId(
		java.lang.String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<com.collaboratelab.impact.model.PatientsImpacted> orderByComparator) {
		return _patientsImpactedLocalService.getPatientsImpactedsByUuidAndCompanyId(uuid,
			companyId, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _patientsImpactedLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _patientsImpactedLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public PatientsImpactedLocalService getWrappedService() {
		return _patientsImpactedLocalService;
	}

	@Override
	public void setWrappedService(
		PatientsImpactedLocalService patientsImpactedLocalService) {
		_patientsImpactedLocalService = patientsImpactedLocalService;
	}

	private PatientsImpactedLocalService _patientsImpactedLocalService;
}