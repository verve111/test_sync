/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CareerImpactLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see CareerImpactLocalService
 * @generated
 */
@ProviderType
public class CareerImpactLocalServiceWrapper implements CareerImpactLocalService,
	ServiceWrapper<CareerImpactLocalService> {
	public CareerImpactLocalServiceWrapper(
		CareerImpactLocalService careerImpactLocalService) {
		_careerImpactLocalService = careerImpactLocalService;
	}

	/**
	* Adds the career impact to the database. Also notifies the appropriate model listeners.
	*
	* @param careerImpact the career impact
	* @return the career impact that was added
	*/
	@Override
	public com.collaboratelab.impact.model.CareerImpact addCareerImpact(
		com.collaboratelab.impact.model.CareerImpact careerImpact) {
		return _careerImpactLocalService.addCareerImpact(careerImpact);
	}

	/**
	* Creates a new career impact with the primary key. Does not add the career impact to the database.
	*
	* @param careerImpactId the primary key for the new career impact
	* @return the new career impact
	*/
	@Override
	public com.collaboratelab.impact.model.CareerImpact createCareerImpact(
		long careerImpactId) {
		return _careerImpactLocalService.createCareerImpact(careerImpactId);
	}

	/**
	* Deletes the career impact from the database. Also notifies the appropriate model listeners.
	*
	* @param careerImpact the career impact
	* @return the career impact that was removed
	*/
	@Override
	public com.collaboratelab.impact.model.CareerImpact deleteCareerImpact(
		com.collaboratelab.impact.model.CareerImpact careerImpact) {
		return _careerImpactLocalService.deleteCareerImpact(careerImpact);
	}

	/**
	* Deletes the career impact with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param careerImpactId the primary key of the career impact
	* @return the career impact that was removed
	* @throws PortalException if a career impact with the primary key could not be found
	*/
	@Override
	public com.collaboratelab.impact.model.CareerImpact deleteCareerImpact(
		long careerImpactId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _careerImpactLocalService.deleteCareerImpact(careerImpactId);
	}

	@Override
	public com.collaboratelab.impact.model.CareerImpact fetchCareerImpact(
		long careerImpactId) {
		return _careerImpactLocalService.fetchCareerImpact(careerImpactId);
	}

	/**
	* Returns the career impact matching the UUID and group.
	*
	* @param uuid the career impact's UUID
	* @param groupId the primary key of the group
	* @return the matching career impact, or <code>null</code> if a matching career impact could not be found
	*/
	@Override
	public com.collaboratelab.impact.model.CareerImpact fetchCareerImpactByUuidAndGroupId(
		java.lang.String uuid, long groupId) {
		return _careerImpactLocalService.fetchCareerImpactByUuidAndGroupId(uuid,
			groupId);
	}

	/**
	* Returns the career impact with the primary key.
	*
	* @param careerImpactId the primary key of the career impact
	* @return the career impact
	* @throws PortalException if a career impact with the primary key could not be found
	*/
	@Override
	public com.collaboratelab.impact.model.CareerImpact getCareerImpact(
		long careerImpactId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _careerImpactLocalService.getCareerImpact(careerImpactId);
	}

	/**
	* Returns the career impact matching the UUID and group.
	*
	* @param uuid the career impact's UUID
	* @param groupId the primary key of the group
	* @return the matching career impact
	* @throws PortalException if a matching career impact could not be found
	*/
	@Override
	public com.collaboratelab.impact.model.CareerImpact getCareerImpactByUuidAndGroupId(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _careerImpactLocalService.getCareerImpactByUuidAndGroupId(uuid,
			groupId);
	}

	/**
	* Updates the career impact in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param careerImpact the career impact
	* @return the career impact that was updated
	*/
	@Override
	public com.collaboratelab.impact.model.CareerImpact updateCareerImpact(
		com.collaboratelab.impact.model.CareerImpact careerImpact) {
		return _careerImpactLocalService.updateCareerImpact(careerImpact);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _careerImpactLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _careerImpactLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery getExportActionableDynamicQuery(
		com.liferay.exportimport.kernel.lar.PortletDataContext portletDataContext) {
		return _careerImpactLocalService.getExportActionableDynamicQuery(portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _careerImpactLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _careerImpactLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _careerImpactLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of career impacts.
	*
	* @return the number of career impacts
	*/
	@Override
	public int getCareerImpactsCount() {
		return _careerImpactLocalService.getCareerImpactsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _careerImpactLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _careerImpactLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.impact.model.impl.CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _careerImpactLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.impact.model.impl.CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _careerImpactLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	@Override
	public java.util.List<com.collaboratelab.impact.model.CareerImpact> findByGroupId(
		long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _careerImpactLocalService.findByGroupId(groupId);
	}

	/**
	* Returns a range of all the career impacts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.impact.model.impl.CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of career impacts
	* @param end the upper bound of the range of career impacts (not inclusive)
	* @return the range of career impacts
	*/
	@Override
	public java.util.List<com.collaboratelab.impact.model.CareerImpact> getCareerImpacts(
		int start, int end) {
		return _careerImpactLocalService.getCareerImpacts(start, end);
	}

	/**
	* Returns all the career impacts matching the UUID and company.
	*
	* @param uuid the UUID of the career impacts
	* @param companyId the primary key of the company
	* @return the matching career impacts, or an empty list if no matches were found
	*/
	@Override
	public java.util.List<com.collaboratelab.impact.model.CareerImpact> getCareerImpactsByUuidAndCompanyId(
		java.lang.String uuid, long companyId) {
		return _careerImpactLocalService.getCareerImpactsByUuidAndCompanyId(uuid,
			companyId);
	}

	/**
	* Returns a range of career impacts matching the UUID and company.
	*
	* @param uuid the UUID of the career impacts
	* @param companyId the primary key of the company
	* @param start the lower bound of the range of career impacts
	* @param end the upper bound of the range of career impacts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the range of matching career impacts, or an empty list if no matches were found
	*/
	@Override
	public java.util.List<com.collaboratelab.impact.model.CareerImpact> getCareerImpactsByUuidAndCompanyId(
		java.lang.String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<com.collaboratelab.impact.model.CareerImpact> orderByComparator) {
		return _careerImpactLocalService.getCareerImpactsByUuidAndCompanyId(uuid,
			companyId, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _careerImpactLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _careerImpactLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public CareerImpactLocalService getWrappedService() {
		return _careerImpactLocalService;
	}

	@Override
	public void setWrappedService(
		CareerImpactLocalService careerImpactLocalService) {
		_careerImpactLocalService = careerImpactLocalService;
	}

	private CareerImpactLocalService _careerImpactLocalService;
}