/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.exportimport.kernel.lar.StagedModelType;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Jobs}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Jobs
 * @generated
 */
@ProviderType
public class JobsWrapper implements Jobs, ModelWrapper<Jobs> {
	public JobsWrapper(Jobs jobs) {
		_jobs = jobs;
	}

	@Override
	public Class<?> getModelClass() {
		return Jobs.class;
	}

	@Override
	public String getModelClassName() {
		return Jobs.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("jobsId", getJobsId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("asOfDate", getAsOfDate());
		attributes.put("sizeOfTeam", getSizeOfTeam());
		attributes.put("comments", getComments());
		attributes.put("primaryJob", getPrimaryJob());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long jobsId = (Long)attributes.get("jobsId");

		if (jobsId != null) {
			setJobsId(jobsId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Date asOfDate = (Date)attributes.get("asOfDate");

		if (asOfDate != null) {
			setAsOfDate(asOfDate);
		}

		Integer sizeOfTeam = (Integer)attributes.get("sizeOfTeam");

		if (sizeOfTeam != null) {
			setSizeOfTeam(sizeOfTeam);
		}

		String comments = (String)attributes.get("comments");

		if (comments != null) {
			setComments(comments);
		}

		String primaryJob = (String)attributes.get("primaryJob");

		if (primaryJob != null) {
			setPrimaryJob(primaryJob);
		}
	}

	@Override
	public Jobs toEscapedModel() {
		return new JobsWrapper(_jobs.toEscapedModel());
	}

	@Override
	public Jobs toUnescapedModel() {
		return new JobsWrapper(_jobs.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _jobs.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _jobs.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _jobs.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _jobs.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Jobs> toCacheModel() {
		return _jobs.toCacheModel();
	}

	@Override
	public int compareTo(Jobs jobs) {
		return _jobs.compareTo(jobs);
	}

	/**
	* Returns the size of team of this jobs.
	*
	* @return the size of team of this jobs
	*/
	@Override
	public int getSizeOfTeam() {
		return _jobs.getSizeOfTeam();
	}

	@Override
	public int hashCode() {
		return _jobs.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _jobs.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new JobsWrapper((Jobs)_jobs.clone());
	}

	/**
	* Returns the comments of this jobs.
	*
	* @return the comments of this jobs
	*/
	@Override
	public java.lang.String getComments() {
		return _jobs.getComments();
	}

	/**
	* Returns the primary job of this jobs.
	*
	* @return the primary job of this jobs
	*/
	@Override
	public java.lang.String getPrimaryJob() {
		return _jobs.getPrimaryJob();
	}

	/**
	* Returns the user name of this jobs.
	*
	* @return the user name of this jobs
	*/
	@Override
	public java.lang.String getUserName() {
		return _jobs.getUserName();
	}

	/**
	* Returns the user uuid of this jobs.
	*
	* @return the user uuid of this jobs
	*/
	@Override
	public java.lang.String getUserUuid() {
		return _jobs.getUserUuid();
	}

	/**
	* Returns the uuid of this jobs.
	*
	* @return the uuid of this jobs
	*/
	@Override
	public java.lang.String getUuid() {
		return _jobs.getUuid();
	}

	@Override
	public java.lang.String toString() {
		return _jobs.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _jobs.toXmlString();
	}

	/**
	* Returns the as of date of this jobs.
	*
	* @return the as of date of this jobs
	*/
	@Override
	public Date getAsOfDate() {
		return _jobs.getAsOfDate();
	}

	/**
	* Returns the create date of this jobs.
	*
	* @return the create date of this jobs
	*/
	@Override
	public Date getCreateDate() {
		return _jobs.getCreateDate();
	}

	/**
	* Returns the modified date of this jobs.
	*
	* @return the modified date of this jobs
	*/
	@Override
	public Date getModifiedDate() {
		return _jobs.getModifiedDate();
	}

	/**
	* Returns the company ID of this jobs.
	*
	* @return the company ID of this jobs
	*/
	@Override
	public long getCompanyId() {
		return _jobs.getCompanyId();
	}

	/**
	* Returns the group ID of this jobs.
	*
	* @return the group ID of this jobs
	*/
	@Override
	public long getGroupId() {
		return _jobs.getGroupId();
	}

	/**
	* Returns the jobs ID of this jobs.
	*
	* @return the jobs ID of this jobs
	*/
	@Override
	public long getJobsId() {
		return _jobs.getJobsId();
	}

	/**
	* Returns the primary key of this jobs.
	*
	* @return the primary key of this jobs
	*/
	@Override
	public long getPrimaryKey() {
		return _jobs.getPrimaryKey();
	}

	/**
	* Returns the user ID of this jobs.
	*
	* @return the user ID of this jobs
	*/
	@Override
	public long getUserId() {
		return _jobs.getUserId();
	}

	@Override
	public void persist() {
		_jobs.persist();
	}

	/**
	* Sets the as of date of this jobs.
	*
	* @param asOfDate the as of date of this jobs
	*/
	@Override
	public void setAsOfDate(Date asOfDate) {
		_jobs.setAsOfDate(asOfDate);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_jobs.setCachedModel(cachedModel);
	}

	/**
	* Sets the comments of this jobs.
	*
	* @param comments the comments of this jobs
	*/
	@Override
	public void setComments(java.lang.String comments) {
		_jobs.setComments(comments);
	}

	/**
	* Sets the company ID of this jobs.
	*
	* @param companyId the company ID of this jobs
	*/
	@Override
	public void setCompanyId(long companyId) {
		_jobs.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this jobs.
	*
	* @param createDate the create date of this jobs
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_jobs.setCreateDate(createDate);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_jobs.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_jobs.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_jobs.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the group ID of this jobs.
	*
	* @param groupId the group ID of this jobs
	*/
	@Override
	public void setGroupId(long groupId) {
		_jobs.setGroupId(groupId);
	}

	/**
	* Sets the jobs ID of this jobs.
	*
	* @param jobsId the jobs ID of this jobs
	*/
	@Override
	public void setJobsId(long jobsId) {
		_jobs.setJobsId(jobsId);
	}

	/**
	* Sets the modified date of this jobs.
	*
	* @param modifiedDate the modified date of this jobs
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_jobs.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_jobs.setNew(n);
	}

	/**
	* Sets the primary job of this jobs.
	*
	* @param primaryJob the primary job of this jobs
	*/
	@Override
	public void setPrimaryJob(java.lang.String primaryJob) {
		_jobs.setPrimaryJob(primaryJob);
	}

	/**
	* Sets the primary key of this jobs.
	*
	* @param primaryKey the primary key of this jobs
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_jobs.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_jobs.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the size of team of this jobs.
	*
	* @param sizeOfTeam the size of team of this jobs
	*/
	@Override
	public void setSizeOfTeam(int sizeOfTeam) {
		_jobs.setSizeOfTeam(sizeOfTeam);
	}

	/**
	* Sets the user ID of this jobs.
	*
	* @param userId the user ID of this jobs
	*/
	@Override
	public void setUserId(long userId) {
		_jobs.setUserId(userId);
	}

	/**
	* Sets the user name of this jobs.
	*
	* @param userName the user name of this jobs
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_jobs.setUserName(userName);
	}

	/**
	* Sets the user uuid of this jobs.
	*
	* @param userUuid the user uuid of this jobs
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_jobs.setUserUuid(userUuid);
	}

	/**
	* Sets the uuid of this jobs.
	*
	* @param uuid the uuid of this jobs
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_jobs.setUuid(uuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof JobsWrapper)) {
			return false;
		}

		JobsWrapper jobsWrapper = (JobsWrapper)obj;

		if (Objects.equals(_jobs, jobsWrapper._jobs)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _jobs.getStagedModelType();
	}

	@Override
	public Jobs getWrappedModel() {
		return _jobs;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _jobs.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _jobs.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_jobs.resetOriginalValues();
	}

	private final Jobs _jobs;
}