/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.impact.exception.NoSuchPatientsImpactedException;
import com.collaboratelab.impact.model.PatientsImpacted;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the patients impacted service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.collaboratelab.impact.service.persistence.impl.PatientsImpactedPersistenceImpl
 * @see PatientsImpactedUtil
 * @generated
 */
@ProviderType
public interface PatientsImpactedPersistence extends BasePersistence<PatientsImpacted> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link PatientsImpactedUtil} to access the patients impacted persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the patients impacteds where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching patients impacteds
	*/
	public java.util.List<PatientsImpacted> findByUuid(java.lang.String uuid);

	/**
	* Returns a range of all the patients impacteds where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of patients impacteds
	* @param end the upper bound of the range of patients impacteds (not inclusive)
	* @return the range of matching patients impacteds
	*/
	public java.util.List<PatientsImpacted> findByUuid(java.lang.String uuid,
		int start, int end);

	/**
	* Returns an ordered range of all the patients impacteds where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of patients impacteds
	* @param end the upper bound of the range of patients impacteds (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching patients impacteds
	*/
	public java.util.List<PatientsImpacted> findByUuid(java.lang.String uuid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<PatientsImpacted> orderByComparator);

	/**
	* Returns an ordered range of all the patients impacteds where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of patients impacteds
	* @param end the upper bound of the range of patients impacteds (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching patients impacteds
	*/
	public java.util.List<PatientsImpacted> findByUuid(java.lang.String uuid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<PatientsImpacted> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first patients impacted in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching patients impacted
	* @throws NoSuchPatientsImpactedException if a matching patients impacted could not be found
	*/
	public PatientsImpacted findByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<PatientsImpacted> orderByComparator)
		throws NoSuchPatientsImpactedException;

	/**
	* Returns the first patients impacted in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching patients impacted, or <code>null</code> if a matching patients impacted could not be found
	*/
	public PatientsImpacted fetchByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<PatientsImpacted> orderByComparator);

	/**
	* Returns the last patients impacted in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching patients impacted
	* @throws NoSuchPatientsImpactedException if a matching patients impacted could not be found
	*/
	public PatientsImpacted findByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<PatientsImpacted> orderByComparator)
		throws NoSuchPatientsImpactedException;

	/**
	* Returns the last patients impacted in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching patients impacted, or <code>null</code> if a matching patients impacted could not be found
	*/
	public PatientsImpacted fetchByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<PatientsImpacted> orderByComparator);

	/**
	* Returns the patients impacteds before and after the current patients impacted in the ordered set where uuid = &#63;.
	*
	* @param patientImpactedId the primary key of the current patients impacted
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next patients impacted
	* @throws NoSuchPatientsImpactedException if a patients impacted with the primary key could not be found
	*/
	public PatientsImpacted[] findByUuid_PrevAndNext(long patientImpactedId,
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<PatientsImpacted> orderByComparator)
		throws NoSuchPatientsImpactedException;

	/**
	* Removes all the patients impacteds where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public void removeByUuid(java.lang.String uuid);

	/**
	* Returns the number of patients impacteds where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching patients impacteds
	*/
	public int countByUuid(java.lang.String uuid);

	/**
	* Returns the patients impacted where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchPatientsImpactedException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching patients impacted
	* @throws NoSuchPatientsImpactedException if a matching patients impacted could not be found
	*/
	public PatientsImpacted findByUUID_G(java.lang.String uuid, long groupId)
		throws NoSuchPatientsImpactedException;

	/**
	* Returns the patients impacted where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching patients impacted, or <code>null</code> if a matching patients impacted could not be found
	*/
	public PatientsImpacted fetchByUUID_G(java.lang.String uuid, long groupId);

	/**
	* Returns the patients impacted where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching patients impacted, or <code>null</code> if a matching patients impacted could not be found
	*/
	public PatientsImpacted fetchByUUID_G(java.lang.String uuid, long groupId,
		boolean retrieveFromCache);

	/**
	* Removes the patients impacted where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the patients impacted that was removed
	*/
	public PatientsImpacted removeByUUID_G(java.lang.String uuid, long groupId)
		throws NoSuchPatientsImpactedException;

	/**
	* Returns the number of patients impacteds where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching patients impacteds
	*/
	public int countByUUID_G(java.lang.String uuid, long groupId);

	/**
	* Returns all the patients impacteds where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching patients impacteds
	*/
	public java.util.List<PatientsImpacted> findByUuid_C(
		java.lang.String uuid, long companyId);

	/**
	* Returns a range of all the patients impacteds where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of patients impacteds
	* @param end the upper bound of the range of patients impacteds (not inclusive)
	* @return the range of matching patients impacteds
	*/
	public java.util.List<PatientsImpacted> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end);

	/**
	* Returns an ordered range of all the patients impacteds where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of patients impacteds
	* @param end the upper bound of the range of patients impacteds (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching patients impacteds
	*/
	public java.util.List<PatientsImpacted> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<PatientsImpacted> orderByComparator);

	/**
	* Returns an ordered range of all the patients impacteds where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of patients impacteds
	* @param end the upper bound of the range of patients impacteds (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching patients impacteds
	*/
	public java.util.List<PatientsImpacted> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<PatientsImpacted> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first patients impacted in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching patients impacted
	* @throws NoSuchPatientsImpactedException if a matching patients impacted could not be found
	*/
	public PatientsImpacted findByUuid_C_First(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<PatientsImpacted> orderByComparator)
		throws NoSuchPatientsImpactedException;

	/**
	* Returns the first patients impacted in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching patients impacted, or <code>null</code> if a matching patients impacted could not be found
	*/
	public PatientsImpacted fetchByUuid_C_First(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<PatientsImpacted> orderByComparator);

	/**
	* Returns the last patients impacted in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching patients impacted
	* @throws NoSuchPatientsImpactedException if a matching patients impacted could not be found
	*/
	public PatientsImpacted findByUuid_C_Last(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<PatientsImpacted> orderByComparator)
		throws NoSuchPatientsImpactedException;

	/**
	* Returns the last patients impacted in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching patients impacted, or <code>null</code> if a matching patients impacted could not be found
	*/
	public PatientsImpacted fetchByUuid_C_Last(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<PatientsImpacted> orderByComparator);

	/**
	* Returns the patients impacteds before and after the current patients impacted in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param patientImpactedId the primary key of the current patients impacted
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next patients impacted
	* @throws NoSuchPatientsImpactedException if a patients impacted with the primary key could not be found
	*/
	public PatientsImpacted[] findByUuid_C_PrevAndNext(long patientImpactedId,
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<PatientsImpacted> orderByComparator)
		throws NoSuchPatientsImpactedException;

	/**
	* Removes all the patients impacteds where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public void removeByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the number of patients impacteds where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching patients impacteds
	*/
	public int countByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the patients impacted where patientImpactedId = &#63; or throws a {@link NoSuchPatientsImpactedException} if it could not be found.
	*
	* @param patientImpactedId the patient impacted ID
	* @return the matching patients impacted
	* @throws NoSuchPatientsImpactedException if a matching patients impacted could not be found
	*/
	public PatientsImpacted findByPatientsImpactedId(long patientImpactedId)
		throws NoSuchPatientsImpactedException;

	/**
	* Returns the patients impacted where patientImpactedId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param patientImpactedId the patient impacted ID
	* @return the matching patients impacted, or <code>null</code> if a matching patients impacted could not be found
	*/
	public PatientsImpacted fetchByPatientsImpactedId(long patientImpactedId);

	/**
	* Returns the patients impacted where patientImpactedId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param patientImpactedId the patient impacted ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching patients impacted, or <code>null</code> if a matching patients impacted could not be found
	*/
	public PatientsImpacted fetchByPatientsImpactedId(long patientImpactedId,
		boolean retrieveFromCache);

	/**
	* Removes the patients impacted where patientImpactedId = &#63; from the database.
	*
	* @param patientImpactedId the patient impacted ID
	* @return the patients impacted that was removed
	*/
	public PatientsImpacted removeByPatientsImpactedId(long patientImpactedId)
		throws NoSuchPatientsImpactedException;

	/**
	* Returns the number of patients impacteds where patientImpactedId = &#63;.
	*
	* @param patientImpactedId the patient impacted ID
	* @return the number of matching patients impacteds
	*/
	public int countByPatientsImpactedId(long patientImpactedId);

	/**
	* Returns all the patients impacteds where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the matching patients impacteds
	*/
	public java.util.List<PatientsImpacted> findByGroupId(long groupId);

	/**
	* Returns a range of all the patients impacteds where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of patients impacteds
	* @param end the upper bound of the range of patients impacteds (not inclusive)
	* @return the range of matching patients impacteds
	*/
	public java.util.List<PatientsImpacted> findByGroupId(long groupId,
		int start, int end);

	/**
	* Returns an ordered range of all the patients impacteds where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of patients impacteds
	* @param end the upper bound of the range of patients impacteds (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching patients impacteds
	*/
	public java.util.List<PatientsImpacted> findByGroupId(long groupId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<PatientsImpacted> orderByComparator);

	/**
	* Returns an ordered range of all the patients impacteds where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of patients impacteds
	* @param end the upper bound of the range of patients impacteds (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching patients impacteds
	*/
	public java.util.List<PatientsImpacted> findByGroupId(long groupId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<PatientsImpacted> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first patients impacted in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching patients impacted
	* @throws NoSuchPatientsImpactedException if a matching patients impacted could not be found
	*/
	public PatientsImpacted findByGroupId_First(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<PatientsImpacted> orderByComparator)
		throws NoSuchPatientsImpactedException;

	/**
	* Returns the first patients impacted in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching patients impacted, or <code>null</code> if a matching patients impacted could not be found
	*/
	public PatientsImpacted fetchByGroupId_First(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<PatientsImpacted> orderByComparator);

	/**
	* Returns the last patients impacted in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching patients impacted
	* @throws NoSuchPatientsImpactedException if a matching patients impacted could not be found
	*/
	public PatientsImpacted findByGroupId_Last(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<PatientsImpacted> orderByComparator)
		throws NoSuchPatientsImpactedException;

	/**
	* Returns the last patients impacted in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching patients impacted, or <code>null</code> if a matching patients impacted could not be found
	*/
	public PatientsImpacted fetchByGroupId_Last(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<PatientsImpacted> orderByComparator);

	/**
	* Returns the patients impacteds before and after the current patients impacted in the ordered set where groupId = &#63;.
	*
	* @param patientImpactedId the primary key of the current patients impacted
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next patients impacted
	* @throws NoSuchPatientsImpactedException if a patients impacted with the primary key could not be found
	*/
	public PatientsImpacted[] findByGroupId_PrevAndNext(
		long patientImpactedId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<PatientsImpacted> orderByComparator)
		throws NoSuchPatientsImpactedException;

	/**
	* Removes all the patients impacteds where groupId = &#63; from the database.
	*
	* @param groupId the group ID
	*/
	public void removeByGroupId(long groupId);

	/**
	* Returns the number of patients impacteds where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the number of matching patients impacteds
	*/
	public int countByGroupId(long groupId);

	/**
	* Caches the patients impacted in the entity cache if it is enabled.
	*
	* @param patientsImpacted the patients impacted
	*/
	public void cacheResult(PatientsImpacted patientsImpacted);

	/**
	* Caches the patients impacteds in the entity cache if it is enabled.
	*
	* @param patientsImpacteds the patients impacteds
	*/
	public void cacheResult(java.util.List<PatientsImpacted> patientsImpacteds);

	/**
	* Creates a new patients impacted with the primary key. Does not add the patients impacted to the database.
	*
	* @param patientImpactedId the primary key for the new patients impacted
	* @return the new patients impacted
	*/
	public PatientsImpacted create(long patientImpactedId);

	/**
	* Removes the patients impacted with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param patientImpactedId the primary key of the patients impacted
	* @return the patients impacted that was removed
	* @throws NoSuchPatientsImpactedException if a patients impacted with the primary key could not be found
	*/
	public PatientsImpacted remove(long patientImpactedId)
		throws NoSuchPatientsImpactedException;

	public PatientsImpacted updateImpl(PatientsImpacted patientsImpacted);

	/**
	* Returns the patients impacted with the primary key or throws a {@link NoSuchPatientsImpactedException} if it could not be found.
	*
	* @param patientImpactedId the primary key of the patients impacted
	* @return the patients impacted
	* @throws NoSuchPatientsImpactedException if a patients impacted with the primary key could not be found
	*/
	public PatientsImpacted findByPrimaryKey(long patientImpactedId)
		throws NoSuchPatientsImpactedException;

	/**
	* Returns the patients impacted with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param patientImpactedId the primary key of the patients impacted
	* @return the patients impacted, or <code>null</code> if a patients impacted with the primary key could not be found
	*/
	public PatientsImpacted fetchByPrimaryKey(long patientImpactedId);

	@Override
	public java.util.Map<java.io.Serializable, PatientsImpacted> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the patients impacteds.
	*
	* @return the patients impacteds
	*/
	public java.util.List<PatientsImpacted> findAll();

	/**
	* Returns a range of all the patients impacteds.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of patients impacteds
	* @param end the upper bound of the range of patients impacteds (not inclusive)
	* @return the range of patients impacteds
	*/
	public java.util.List<PatientsImpacted> findAll(int start, int end);

	/**
	* Returns an ordered range of all the patients impacteds.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of patients impacteds
	* @param end the upper bound of the range of patients impacteds (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of patients impacteds
	*/
	public java.util.List<PatientsImpacted> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<PatientsImpacted> orderByComparator);

	/**
	* Returns an ordered range of all the patients impacteds.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link PatientsImpactedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of patients impacteds
	* @param end the upper bound of the range of patients impacteds (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of patients impacteds
	*/
	public java.util.List<PatientsImpacted> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<PatientsImpacted> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the patients impacteds from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of patients impacteds.
	*
	* @return the number of patients impacteds
	*/
	public int countAll();

	@Override
	public java.util.Set<java.lang.String> getBadColumnNames();
}