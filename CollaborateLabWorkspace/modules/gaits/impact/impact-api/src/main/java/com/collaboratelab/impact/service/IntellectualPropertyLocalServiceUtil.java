/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for IntellectualProperty. This utility wraps
 * {@link com.collaboratelab.impact.service.impl.IntellectualPropertyLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see IntellectualPropertyLocalService
 * @see com.collaboratelab.impact.service.base.IntellectualPropertyLocalServiceBaseImpl
 * @see com.collaboratelab.impact.service.impl.IntellectualPropertyLocalServiceImpl
 * @generated
 */
@ProviderType
public class IntellectualPropertyLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.collaboratelab.impact.service.impl.IntellectualPropertyLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the intellectual property to the database. Also notifies the appropriate model listeners.
	*
	* @param intellectualProperty the intellectual property
	* @return the intellectual property that was added
	*/
	public static com.collaboratelab.impact.model.IntellectualProperty addIntellectualProperty(
		com.collaboratelab.impact.model.IntellectualProperty intellectualProperty) {
		return getService().addIntellectualProperty(intellectualProperty);
	}

	/**
	* Creates a new intellectual property with the primary key. Does not add the intellectual property to the database.
	*
	* @param intellectualPropertyId the primary key for the new intellectual property
	* @return the new intellectual property
	*/
	public static com.collaboratelab.impact.model.IntellectualProperty createIntellectualProperty(
		long intellectualPropertyId) {
		return getService().createIntellectualProperty(intellectualPropertyId);
	}

	/**
	* Deletes the intellectual property from the database. Also notifies the appropriate model listeners.
	*
	* @param intellectualProperty the intellectual property
	* @return the intellectual property that was removed
	*/
	public static com.collaboratelab.impact.model.IntellectualProperty deleteIntellectualProperty(
		com.collaboratelab.impact.model.IntellectualProperty intellectualProperty) {
		return getService().deleteIntellectualProperty(intellectualProperty);
	}

	/**
	* Deletes the intellectual property with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param intellectualPropertyId the primary key of the intellectual property
	* @return the intellectual property that was removed
	* @throws PortalException if a intellectual property with the primary key could not be found
	*/
	public static com.collaboratelab.impact.model.IntellectualProperty deleteIntellectualProperty(
		long intellectualPropertyId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteIntellectualProperty(intellectualPropertyId);
	}

	public static com.collaboratelab.impact.model.IntellectualProperty fetchIntellectualProperty(
		long intellectualPropertyId) {
		return getService().fetchIntellectualProperty(intellectualPropertyId);
	}

	/**
	* Returns the intellectual property matching the UUID and group.
	*
	* @param uuid the intellectual property's UUID
	* @param groupId the primary key of the group
	* @return the matching intellectual property, or <code>null</code> if a matching intellectual property could not be found
	*/
	public static com.collaboratelab.impact.model.IntellectualProperty fetchIntellectualPropertyByUuidAndGroupId(
		java.lang.String uuid, long groupId) {
		return getService()
				   .fetchIntellectualPropertyByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Returns the intellectual property with the primary key.
	*
	* @param intellectualPropertyId the primary key of the intellectual property
	* @return the intellectual property
	* @throws PortalException if a intellectual property with the primary key could not be found
	*/
	public static com.collaboratelab.impact.model.IntellectualProperty getIntellectualProperty(
		long intellectualPropertyId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getIntellectualProperty(intellectualPropertyId);
	}

	/**
	* Returns the intellectual property matching the UUID and group.
	*
	* @param uuid the intellectual property's UUID
	* @param groupId the primary key of the group
	* @return the matching intellectual property
	* @throws PortalException if a matching intellectual property could not be found
	*/
	public static com.collaboratelab.impact.model.IntellectualProperty getIntellectualPropertyByUuidAndGroupId(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService()
				   .getIntellectualPropertyByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Updates the intellectual property in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param intellectualProperty the intellectual property
	* @return the intellectual property that was updated
	*/
	public static com.collaboratelab.impact.model.IntellectualProperty updateIntellectualProperty(
		com.collaboratelab.impact.model.IntellectualProperty intellectualProperty) {
		return getService().updateIntellectualProperty(intellectualProperty);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery getExportActionableDynamicQuery(
		com.liferay.exportimport.kernel.lar.PortletDataContext portletDataContext) {
		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of intellectual properties.
	*
	* @return the number of intellectual properties
	*/
	public static int getIntellectualPropertiesCount() {
		return getService().getIntellectualPropertiesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.impact.model.impl.IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.impact.model.impl.IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	public static java.util.List<com.collaboratelab.impact.model.IntellectualProperty> findByGroupId(
		long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().findByGroupId(groupId);
	}

	/**
	* Returns a range of all the intellectual properties.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.collaboratelab.impact.model.impl.IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of intellectual properties
	* @param end the upper bound of the range of intellectual properties (not inclusive)
	* @return the range of intellectual properties
	*/
	public static java.util.List<com.collaboratelab.impact.model.IntellectualProperty> getIntellectualProperties(
		int start, int end) {
		return getService().getIntellectualProperties(start, end);
	}

	/**
	* Returns all the intellectual properties matching the UUID and company.
	*
	* @param uuid the UUID of the intellectual properties
	* @param companyId the primary key of the company
	* @return the matching intellectual properties, or an empty list if no matches were found
	*/
	public static java.util.List<com.collaboratelab.impact.model.IntellectualProperty> getIntellectualPropertiesByUuidAndCompanyId(
		java.lang.String uuid, long companyId) {
		return getService()
				   .getIntellectualPropertiesByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Returns a range of intellectual properties matching the UUID and company.
	*
	* @param uuid the UUID of the intellectual properties
	* @param companyId the primary key of the company
	* @param start the lower bound of the range of intellectual properties
	* @param end the upper bound of the range of intellectual properties (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the range of matching intellectual properties, or an empty list if no matches were found
	*/
	public static java.util.List<com.collaboratelab.impact.model.IntellectualProperty> getIntellectualPropertiesByUuidAndCompanyId(
		java.lang.String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<com.collaboratelab.impact.model.IntellectualProperty> orderByComparator) {
		return getService()
				   .getIntellectualPropertiesByUuidAndCompanyId(uuid,
			companyId, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static IntellectualPropertyLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<IntellectualPropertyLocalService, IntellectualPropertyLocalService> _serviceTracker =
		ServiceTrackerFactory.open(IntellectualPropertyLocalService.class);
}