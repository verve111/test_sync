/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.impact.exception.NoSuchIntellectualPropertyException;
import com.collaboratelab.impact.model.IntellectualProperty;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the intellectual property service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.collaboratelab.impact.service.persistence.impl.IntellectualPropertyPersistenceImpl
 * @see IntellectualPropertyUtil
 * @generated
 */
@ProviderType
public interface IntellectualPropertyPersistence extends BasePersistence<IntellectualProperty> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link IntellectualPropertyUtil} to access the intellectual property persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the intellectual properties where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching intellectual properties
	*/
	public java.util.List<IntellectualProperty> findByUuid(
		java.lang.String uuid);

	/**
	* Returns a range of all the intellectual properties where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of intellectual properties
	* @param end the upper bound of the range of intellectual properties (not inclusive)
	* @return the range of matching intellectual properties
	*/
	public java.util.List<IntellectualProperty> findByUuid(
		java.lang.String uuid, int start, int end);

	/**
	* Returns an ordered range of all the intellectual properties where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of intellectual properties
	* @param end the upper bound of the range of intellectual properties (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching intellectual properties
	*/
	public java.util.List<IntellectualProperty> findByUuid(
		java.lang.String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<IntellectualProperty> orderByComparator);

	/**
	* Returns an ordered range of all the intellectual properties where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of intellectual properties
	* @param end the upper bound of the range of intellectual properties (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching intellectual properties
	*/
	public java.util.List<IntellectualProperty> findByUuid(
		java.lang.String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<IntellectualProperty> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first intellectual property in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching intellectual property
	* @throws NoSuchIntellectualPropertyException if a matching intellectual property could not be found
	*/
	public IntellectualProperty findByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<IntellectualProperty> orderByComparator)
		throws NoSuchIntellectualPropertyException;

	/**
	* Returns the first intellectual property in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching intellectual property, or <code>null</code> if a matching intellectual property could not be found
	*/
	public IntellectualProperty fetchByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<IntellectualProperty> orderByComparator);

	/**
	* Returns the last intellectual property in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching intellectual property
	* @throws NoSuchIntellectualPropertyException if a matching intellectual property could not be found
	*/
	public IntellectualProperty findByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<IntellectualProperty> orderByComparator)
		throws NoSuchIntellectualPropertyException;

	/**
	* Returns the last intellectual property in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching intellectual property, or <code>null</code> if a matching intellectual property could not be found
	*/
	public IntellectualProperty fetchByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<IntellectualProperty> orderByComparator);

	/**
	* Returns the intellectual properties before and after the current intellectual property in the ordered set where uuid = &#63;.
	*
	* @param intellectualPropertyId the primary key of the current intellectual property
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next intellectual property
	* @throws NoSuchIntellectualPropertyException if a intellectual property with the primary key could not be found
	*/
	public IntellectualProperty[] findByUuid_PrevAndNext(
		long intellectualPropertyId, java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<IntellectualProperty> orderByComparator)
		throws NoSuchIntellectualPropertyException;

	/**
	* Removes all the intellectual properties where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public void removeByUuid(java.lang.String uuid);

	/**
	* Returns the number of intellectual properties where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching intellectual properties
	*/
	public int countByUuid(java.lang.String uuid);

	/**
	* Returns the intellectual property where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchIntellectualPropertyException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching intellectual property
	* @throws NoSuchIntellectualPropertyException if a matching intellectual property could not be found
	*/
	public IntellectualProperty findByUUID_G(java.lang.String uuid, long groupId)
		throws NoSuchIntellectualPropertyException;

	/**
	* Returns the intellectual property where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching intellectual property, or <code>null</code> if a matching intellectual property could not be found
	*/
	public IntellectualProperty fetchByUUID_G(java.lang.String uuid,
		long groupId);

	/**
	* Returns the intellectual property where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching intellectual property, or <code>null</code> if a matching intellectual property could not be found
	*/
	public IntellectualProperty fetchByUUID_G(java.lang.String uuid,
		long groupId, boolean retrieveFromCache);

	/**
	* Removes the intellectual property where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the intellectual property that was removed
	*/
	public IntellectualProperty removeByUUID_G(java.lang.String uuid,
		long groupId) throws NoSuchIntellectualPropertyException;

	/**
	* Returns the number of intellectual properties where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching intellectual properties
	*/
	public int countByUUID_G(java.lang.String uuid, long groupId);

	/**
	* Returns all the intellectual properties where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching intellectual properties
	*/
	public java.util.List<IntellectualProperty> findByUuid_C(
		java.lang.String uuid, long companyId);

	/**
	* Returns a range of all the intellectual properties where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of intellectual properties
	* @param end the upper bound of the range of intellectual properties (not inclusive)
	* @return the range of matching intellectual properties
	*/
	public java.util.List<IntellectualProperty> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end);

	/**
	* Returns an ordered range of all the intellectual properties where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of intellectual properties
	* @param end the upper bound of the range of intellectual properties (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching intellectual properties
	*/
	public java.util.List<IntellectualProperty> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<IntellectualProperty> orderByComparator);

	/**
	* Returns an ordered range of all the intellectual properties where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of intellectual properties
	* @param end the upper bound of the range of intellectual properties (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching intellectual properties
	*/
	public java.util.List<IntellectualProperty> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<IntellectualProperty> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first intellectual property in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching intellectual property
	* @throws NoSuchIntellectualPropertyException if a matching intellectual property could not be found
	*/
	public IntellectualProperty findByUuid_C_First(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<IntellectualProperty> orderByComparator)
		throws NoSuchIntellectualPropertyException;

	/**
	* Returns the first intellectual property in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching intellectual property, or <code>null</code> if a matching intellectual property could not be found
	*/
	public IntellectualProperty fetchByUuid_C_First(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<IntellectualProperty> orderByComparator);

	/**
	* Returns the last intellectual property in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching intellectual property
	* @throws NoSuchIntellectualPropertyException if a matching intellectual property could not be found
	*/
	public IntellectualProperty findByUuid_C_Last(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<IntellectualProperty> orderByComparator)
		throws NoSuchIntellectualPropertyException;

	/**
	* Returns the last intellectual property in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching intellectual property, or <code>null</code> if a matching intellectual property could not be found
	*/
	public IntellectualProperty fetchByUuid_C_Last(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<IntellectualProperty> orderByComparator);

	/**
	* Returns the intellectual properties before and after the current intellectual property in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param intellectualPropertyId the primary key of the current intellectual property
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next intellectual property
	* @throws NoSuchIntellectualPropertyException if a intellectual property with the primary key could not be found
	*/
	public IntellectualProperty[] findByUuid_C_PrevAndNext(
		long intellectualPropertyId, java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<IntellectualProperty> orderByComparator)
		throws NoSuchIntellectualPropertyException;

	/**
	* Removes all the intellectual properties where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public void removeByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the number of intellectual properties where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching intellectual properties
	*/
	public int countByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the intellectual property where intellectualPropertyId = &#63; or throws a {@link NoSuchIntellectualPropertyException} if it could not be found.
	*
	* @param intellectualPropertyId the intellectual property ID
	* @return the matching intellectual property
	* @throws NoSuchIntellectualPropertyException if a matching intellectual property could not be found
	*/
	public IntellectualProperty findByIntellectualPropertyId(
		long intellectualPropertyId) throws NoSuchIntellectualPropertyException;

	/**
	* Returns the intellectual property where intellectualPropertyId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param intellectualPropertyId the intellectual property ID
	* @return the matching intellectual property, or <code>null</code> if a matching intellectual property could not be found
	*/
	public IntellectualProperty fetchByIntellectualPropertyId(
		long intellectualPropertyId);

	/**
	* Returns the intellectual property where intellectualPropertyId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param intellectualPropertyId the intellectual property ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching intellectual property, or <code>null</code> if a matching intellectual property could not be found
	*/
	public IntellectualProperty fetchByIntellectualPropertyId(
		long intellectualPropertyId, boolean retrieveFromCache);

	/**
	* Removes the intellectual property where intellectualPropertyId = &#63; from the database.
	*
	* @param intellectualPropertyId the intellectual property ID
	* @return the intellectual property that was removed
	*/
	public IntellectualProperty removeByIntellectualPropertyId(
		long intellectualPropertyId) throws NoSuchIntellectualPropertyException;

	/**
	* Returns the number of intellectual properties where intellectualPropertyId = &#63;.
	*
	* @param intellectualPropertyId the intellectual property ID
	* @return the number of matching intellectual properties
	*/
	public int countByIntellectualPropertyId(long intellectualPropertyId);

	/**
	* Returns all the intellectual properties where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the matching intellectual properties
	*/
	public java.util.List<IntellectualProperty> findByGroupId(long groupId);

	/**
	* Returns a range of all the intellectual properties where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of intellectual properties
	* @param end the upper bound of the range of intellectual properties (not inclusive)
	* @return the range of matching intellectual properties
	*/
	public java.util.List<IntellectualProperty> findByGroupId(long groupId,
		int start, int end);

	/**
	* Returns an ordered range of all the intellectual properties where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of intellectual properties
	* @param end the upper bound of the range of intellectual properties (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching intellectual properties
	*/
	public java.util.List<IntellectualProperty> findByGroupId(long groupId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<IntellectualProperty> orderByComparator);

	/**
	* Returns an ordered range of all the intellectual properties where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of intellectual properties
	* @param end the upper bound of the range of intellectual properties (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching intellectual properties
	*/
	public java.util.List<IntellectualProperty> findByGroupId(long groupId,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<IntellectualProperty> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first intellectual property in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching intellectual property
	* @throws NoSuchIntellectualPropertyException if a matching intellectual property could not be found
	*/
	public IntellectualProperty findByGroupId_First(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<IntellectualProperty> orderByComparator)
		throws NoSuchIntellectualPropertyException;

	/**
	* Returns the first intellectual property in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching intellectual property, or <code>null</code> if a matching intellectual property could not be found
	*/
	public IntellectualProperty fetchByGroupId_First(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<IntellectualProperty> orderByComparator);

	/**
	* Returns the last intellectual property in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching intellectual property
	* @throws NoSuchIntellectualPropertyException if a matching intellectual property could not be found
	*/
	public IntellectualProperty findByGroupId_Last(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<IntellectualProperty> orderByComparator)
		throws NoSuchIntellectualPropertyException;

	/**
	* Returns the last intellectual property in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching intellectual property, or <code>null</code> if a matching intellectual property could not be found
	*/
	public IntellectualProperty fetchByGroupId_Last(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<IntellectualProperty> orderByComparator);

	/**
	* Returns the intellectual properties before and after the current intellectual property in the ordered set where groupId = &#63;.
	*
	* @param intellectualPropertyId the primary key of the current intellectual property
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next intellectual property
	* @throws NoSuchIntellectualPropertyException if a intellectual property with the primary key could not be found
	*/
	public IntellectualProperty[] findByGroupId_PrevAndNext(
		long intellectualPropertyId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<IntellectualProperty> orderByComparator)
		throws NoSuchIntellectualPropertyException;

	/**
	* Removes all the intellectual properties where groupId = &#63; from the database.
	*
	* @param groupId the group ID
	*/
	public void removeByGroupId(long groupId);

	/**
	* Returns the number of intellectual properties where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the number of matching intellectual properties
	*/
	public int countByGroupId(long groupId);

	/**
	* Caches the intellectual property in the entity cache if it is enabled.
	*
	* @param intellectualProperty the intellectual property
	*/
	public void cacheResult(IntellectualProperty intellectualProperty);

	/**
	* Caches the intellectual properties in the entity cache if it is enabled.
	*
	* @param intellectualProperties the intellectual properties
	*/
	public void cacheResult(
		java.util.List<IntellectualProperty> intellectualProperties);

	/**
	* Creates a new intellectual property with the primary key. Does not add the intellectual property to the database.
	*
	* @param intellectualPropertyId the primary key for the new intellectual property
	* @return the new intellectual property
	*/
	public IntellectualProperty create(long intellectualPropertyId);

	/**
	* Removes the intellectual property with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param intellectualPropertyId the primary key of the intellectual property
	* @return the intellectual property that was removed
	* @throws NoSuchIntellectualPropertyException if a intellectual property with the primary key could not be found
	*/
	public IntellectualProperty remove(long intellectualPropertyId)
		throws NoSuchIntellectualPropertyException;

	public IntellectualProperty updateImpl(
		IntellectualProperty intellectualProperty);

	/**
	* Returns the intellectual property with the primary key or throws a {@link NoSuchIntellectualPropertyException} if it could not be found.
	*
	* @param intellectualPropertyId the primary key of the intellectual property
	* @return the intellectual property
	* @throws NoSuchIntellectualPropertyException if a intellectual property with the primary key could not be found
	*/
	public IntellectualProperty findByPrimaryKey(long intellectualPropertyId)
		throws NoSuchIntellectualPropertyException;

	/**
	* Returns the intellectual property with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param intellectualPropertyId the primary key of the intellectual property
	* @return the intellectual property, or <code>null</code> if a intellectual property with the primary key could not be found
	*/
	public IntellectualProperty fetchByPrimaryKey(long intellectualPropertyId);

	@Override
	public java.util.Map<java.io.Serializable, IntellectualProperty> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the intellectual properties.
	*
	* @return the intellectual properties
	*/
	public java.util.List<IntellectualProperty> findAll();

	/**
	* Returns a range of all the intellectual properties.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of intellectual properties
	* @param end the upper bound of the range of intellectual properties (not inclusive)
	* @return the range of intellectual properties
	*/
	public java.util.List<IntellectualProperty> findAll(int start, int end);

	/**
	* Returns an ordered range of all the intellectual properties.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of intellectual properties
	* @param end the upper bound of the range of intellectual properties (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of intellectual properties
	*/
	public java.util.List<IntellectualProperty> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<IntellectualProperty> orderByComparator);

	/**
	* Returns an ordered range of all the intellectual properties.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of intellectual properties
	* @param end the upper bound of the range of intellectual properties (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of intellectual properties
	*/
	public java.util.List<IntellectualProperty> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<IntellectualProperty> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the intellectual properties from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of intellectual properties.
	*
	* @return the number of intellectual properties
	*/
	public int countAll();

	@Override
	public java.util.Set<java.lang.String> getBadColumnNames();
}