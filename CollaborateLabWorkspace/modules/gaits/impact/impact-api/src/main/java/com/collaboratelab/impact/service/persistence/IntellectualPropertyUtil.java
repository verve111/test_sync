/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.impact.model.IntellectualProperty;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the intellectual property service. This utility wraps {@link com.collaboratelab.impact.service.persistence.impl.IntellectualPropertyPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see IntellectualPropertyPersistence
 * @see com.collaboratelab.impact.service.persistence.impl.IntellectualPropertyPersistenceImpl
 * @generated
 */
@ProviderType
public class IntellectualPropertyUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(IntellectualProperty intellectualProperty) {
		getPersistence().clearCache(intellectualProperty);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<IntellectualProperty> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<IntellectualProperty> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<IntellectualProperty> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<IntellectualProperty> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static IntellectualProperty update(
		IntellectualProperty intellectualProperty) {
		return getPersistence().update(intellectualProperty);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static IntellectualProperty update(
		IntellectualProperty intellectualProperty, ServiceContext serviceContext) {
		return getPersistence().update(intellectualProperty, serviceContext);
	}

	/**
	* Returns all the intellectual properties where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching intellectual properties
	*/
	public static List<IntellectualProperty> findByUuid(java.lang.String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	* Returns a range of all the intellectual properties where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of intellectual properties
	* @param end the upper bound of the range of intellectual properties (not inclusive)
	* @return the range of matching intellectual properties
	*/
	public static List<IntellectualProperty> findByUuid(java.lang.String uuid,
		int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	* Returns an ordered range of all the intellectual properties where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of intellectual properties
	* @param end the upper bound of the range of intellectual properties (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching intellectual properties
	*/
	public static List<IntellectualProperty> findByUuid(java.lang.String uuid,
		int start, int end,
		OrderByComparator<IntellectualProperty> orderByComparator) {
		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the intellectual properties where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of intellectual properties
	* @param end the upper bound of the range of intellectual properties (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching intellectual properties
	*/
	public static List<IntellectualProperty> findByUuid(java.lang.String uuid,
		int start, int end,
		OrderByComparator<IntellectualProperty> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid(uuid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first intellectual property in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching intellectual property
	* @throws NoSuchIntellectualPropertyException if a matching intellectual property could not be found
	*/
	public static IntellectualProperty findByUuid_First(java.lang.String uuid,
		OrderByComparator<IntellectualProperty> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchIntellectualPropertyException {
		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the first intellectual property in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching intellectual property, or <code>null</code> if a matching intellectual property could not be found
	*/
	public static IntellectualProperty fetchByUuid_First(
		java.lang.String uuid,
		OrderByComparator<IntellectualProperty> orderByComparator) {
		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the last intellectual property in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching intellectual property
	* @throws NoSuchIntellectualPropertyException if a matching intellectual property could not be found
	*/
	public static IntellectualProperty findByUuid_Last(java.lang.String uuid,
		OrderByComparator<IntellectualProperty> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchIntellectualPropertyException {
		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the last intellectual property in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching intellectual property, or <code>null</code> if a matching intellectual property could not be found
	*/
	public static IntellectualProperty fetchByUuid_Last(java.lang.String uuid,
		OrderByComparator<IntellectualProperty> orderByComparator) {
		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the intellectual properties before and after the current intellectual property in the ordered set where uuid = &#63;.
	*
	* @param intellectualPropertyId the primary key of the current intellectual property
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next intellectual property
	* @throws NoSuchIntellectualPropertyException if a intellectual property with the primary key could not be found
	*/
	public static IntellectualProperty[] findByUuid_PrevAndNext(
		long intellectualPropertyId, java.lang.String uuid,
		OrderByComparator<IntellectualProperty> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchIntellectualPropertyException {
		return getPersistence()
				   .findByUuid_PrevAndNext(intellectualPropertyId, uuid,
			orderByComparator);
	}

	/**
	* Removes all the intellectual properties where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public static void removeByUuid(java.lang.String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	* Returns the number of intellectual properties where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching intellectual properties
	*/
	public static int countByUuid(java.lang.String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	* Returns the intellectual property where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchIntellectualPropertyException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching intellectual property
	* @throws NoSuchIntellectualPropertyException if a matching intellectual property could not be found
	*/
	public static IntellectualProperty findByUUID_G(java.lang.String uuid,
		long groupId)
		throws com.collaboratelab.impact.exception.NoSuchIntellectualPropertyException {
		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	* Returns the intellectual property where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching intellectual property, or <code>null</code> if a matching intellectual property could not be found
	*/
	public static IntellectualProperty fetchByUUID_G(java.lang.String uuid,
		long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	* Returns the intellectual property where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching intellectual property, or <code>null</code> if a matching intellectual property could not be found
	*/
	public static IntellectualProperty fetchByUUID_G(java.lang.String uuid,
		long groupId, boolean retrieveFromCache) {
		return getPersistence().fetchByUUID_G(uuid, groupId, retrieveFromCache);
	}

	/**
	* Removes the intellectual property where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the intellectual property that was removed
	*/
	public static IntellectualProperty removeByUUID_G(java.lang.String uuid,
		long groupId)
		throws com.collaboratelab.impact.exception.NoSuchIntellectualPropertyException {
		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	* Returns the number of intellectual properties where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching intellectual properties
	*/
	public static int countByUUID_G(java.lang.String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	* Returns all the intellectual properties where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching intellectual properties
	*/
	public static List<IntellectualProperty> findByUuid_C(
		java.lang.String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	* Returns a range of all the intellectual properties where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of intellectual properties
	* @param end the upper bound of the range of intellectual properties (not inclusive)
	* @return the range of matching intellectual properties
	*/
	public static List<IntellectualProperty> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end) {
		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	* Returns an ordered range of all the intellectual properties where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of intellectual properties
	* @param end the upper bound of the range of intellectual properties (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching intellectual properties
	*/
	public static List<IntellectualProperty> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end,
		OrderByComparator<IntellectualProperty> orderByComparator) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the intellectual properties where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of intellectual properties
	* @param end the upper bound of the range of intellectual properties (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching intellectual properties
	*/
	public static List<IntellectualProperty> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end,
		OrderByComparator<IntellectualProperty> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first intellectual property in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching intellectual property
	* @throws NoSuchIntellectualPropertyException if a matching intellectual property could not be found
	*/
	public static IntellectualProperty findByUuid_C_First(
		java.lang.String uuid, long companyId,
		OrderByComparator<IntellectualProperty> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchIntellectualPropertyException {
		return getPersistence()
				   .findByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the first intellectual property in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching intellectual property, or <code>null</code> if a matching intellectual property could not be found
	*/
	public static IntellectualProperty fetchByUuid_C_First(
		java.lang.String uuid, long companyId,
		OrderByComparator<IntellectualProperty> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last intellectual property in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching intellectual property
	* @throws NoSuchIntellectualPropertyException if a matching intellectual property could not be found
	*/
	public static IntellectualProperty findByUuid_C_Last(
		java.lang.String uuid, long companyId,
		OrderByComparator<IntellectualProperty> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchIntellectualPropertyException {
		return getPersistence()
				   .findByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last intellectual property in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching intellectual property, or <code>null</code> if a matching intellectual property could not be found
	*/
	public static IntellectualProperty fetchByUuid_C_Last(
		java.lang.String uuid, long companyId,
		OrderByComparator<IntellectualProperty> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the intellectual properties before and after the current intellectual property in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param intellectualPropertyId the primary key of the current intellectual property
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next intellectual property
	* @throws NoSuchIntellectualPropertyException if a intellectual property with the primary key could not be found
	*/
	public static IntellectualProperty[] findByUuid_C_PrevAndNext(
		long intellectualPropertyId, java.lang.String uuid, long companyId,
		OrderByComparator<IntellectualProperty> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchIntellectualPropertyException {
		return getPersistence()
				   .findByUuid_C_PrevAndNext(intellectualPropertyId, uuid,
			companyId, orderByComparator);
	}

	/**
	* Removes all the intellectual properties where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public static void removeByUuid_C(java.lang.String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	* Returns the number of intellectual properties where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching intellectual properties
	*/
	public static int countByUuid_C(java.lang.String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	* Returns the intellectual property where intellectualPropertyId = &#63; or throws a {@link NoSuchIntellectualPropertyException} if it could not be found.
	*
	* @param intellectualPropertyId the intellectual property ID
	* @return the matching intellectual property
	* @throws NoSuchIntellectualPropertyException if a matching intellectual property could not be found
	*/
	public static IntellectualProperty findByIntellectualPropertyId(
		long intellectualPropertyId)
		throws com.collaboratelab.impact.exception.NoSuchIntellectualPropertyException {
		return getPersistence()
				   .findByIntellectualPropertyId(intellectualPropertyId);
	}

	/**
	* Returns the intellectual property where intellectualPropertyId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param intellectualPropertyId the intellectual property ID
	* @return the matching intellectual property, or <code>null</code> if a matching intellectual property could not be found
	*/
	public static IntellectualProperty fetchByIntellectualPropertyId(
		long intellectualPropertyId) {
		return getPersistence()
				   .fetchByIntellectualPropertyId(intellectualPropertyId);
	}

	/**
	* Returns the intellectual property where intellectualPropertyId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param intellectualPropertyId the intellectual property ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching intellectual property, or <code>null</code> if a matching intellectual property could not be found
	*/
	public static IntellectualProperty fetchByIntellectualPropertyId(
		long intellectualPropertyId, boolean retrieveFromCache) {
		return getPersistence()
				   .fetchByIntellectualPropertyId(intellectualPropertyId,
			retrieveFromCache);
	}

	/**
	* Removes the intellectual property where intellectualPropertyId = &#63; from the database.
	*
	* @param intellectualPropertyId the intellectual property ID
	* @return the intellectual property that was removed
	*/
	public static IntellectualProperty removeByIntellectualPropertyId(
		long intellectualPropertyId)
		throws com.collaboratelab.impact.exception.NoSuchIntellectualPropertyException {
		return getPersistence()
				   .removeByIntellectualPropertyId(intellectualPropertyId);
	}

	/**
	* Returns the number of intellectual properties where intellectualPropertyId = &#63;.
	*
	* @param intellectualPropertyId the intellectual property ID
	* @return the number of matching intellectual properties
	*/
	public static int countByIntellectualPropertyId(long intellectualPropertyId) {
		return getPersistence()
				   .countByIntellectualPropertyId(intellectualPropertyId);
	}

	/**
	* Returns all the intellectual properties where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the matching intellectual properties
	*/
	public static List<IntellectualProperty> findByGroupId(long groupId) {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	* Returns a range of all the intellectual properties where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of intellectual properties
	* @param end the upper bound of the range of intellectual properties (not inclusive)
	* @return the range of matching intellectual properties
	*/
	public static List<IntellectualProperty> findByGroupId(long groupId,
		int start, int end) {
		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	* Returns an ordered range of all the intellectual properties where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of intellectual properties
	* @param end the upper bound of the range of intellectual properties (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching intellectual properties
	*/
	public static List<IntellectualProperty> findByGroupId(long groupId,
		int start, int end,
		OrderByComparator<IntellectualProperty> orderByComparator) {
		return getPersistence()
				   .findByGroupId(groupId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the intellectual properties where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of intellectual properties
	* @param end the upper bound of the range of intellectual properties (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching intellectual properties
	*/
	public static List<IntellectualProperty> findByGroupId(long groupId,
		int start, int end,
		OrderByComparator<IntellectualProperty> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByGroupId(groupId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first intellectual property in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching intellectual property
	* @throws NoSuchIntellectualPropertyException if a matching intellectual property could not be found
	*/
	public static IntellectualProperty findByGroupId_First(long groupId,
		OrderByComparator<IntellectualProperty> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchIntellectualPropertyException {
		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	* Returns the first intellectual property in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching intellectual property, or <code>null</code> if a matching intellectual property could not be found
	*/
	public static IntellectualProperty fetchByGroupId_First(long groupId,
		OrderByComparator<IntellectualProperty> orderByComparator) {
		return getPersistence().fetchByGroupId_First(groupId, orderByComparator);
	}

	/**
	* Returns the last intellectual property in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching intellectual property
	* @throws NoSuchIntellectualPropertyException if a matching intellectual property could not be found
	*/
	public static IntellectualProperty findByGroupId_Last(long groupId,
		OrderByComparator<IntellectualProperty> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchIntellectualPropertyException {
		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	* Returns the last intellectual property in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching intellectual property, or <code>null</code> if a matching intellectual property could not be found
	*/
	public static IntellectualProperty fetchByGroupId_Last(long groupId,
		OrderByComparator<IntellectualProperty> orderByComparator) {
		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	* Returns the intellectual properties before and after the current intellectual property in the ordered set where groupId = &#63;.
	*
	* @param intellectualPropertyId the primary key of the current intellectual property
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next intellectual property
	* @throws NoSuchIntellectualPropertyException if a intellectual property with the primary key could not be found
	*/
	public static IntellectualProperty[] findByGroupId_PrevAndNext(
		long intellectualPropertyId, long groupId,
		OrderByComparator<IntellectualProperty> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchIntellectualPropertyException {
		return getPersistence()
				   .findByGroupId_PrevAndNext(intellectualPropertyId, groupId,
			orderByComparator);
	}

	/**
	* Removes all the intellectual properties where groupId = &#63; from the database.
	*
	* @param groupId the group ID
	*/
	public static void removeByGroupId(long groupId) {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	* Returns the number of intellectual properties where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the number of matching intellectual properties
	*/
	public static int countByGroupId(long groupId) {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	* Caches the intellectual property in the entity cache if it is enabled.
	*
	* @param intellectualProperty the intellectual property
	*/
	public static void cacheResult(IntellectualProperty intellectualProperty) {
		getPersistence().cacheResult(intellectualProperty);
	}

	/**
	* Caches the intellectual properties in the entity cache if it is enabled.
	*
	* @param intellectualProperties the intellectual properties
	*/
	public static void cacheResult(
		List<IntellectualProperty> intellectualProperties) {
		getPersistence().cacheResult(intellectualProperties);
	}

	/**
	* Creates a new intellectual property with the primary key. Does not add the intellectual property to the database.
	*
	* @param intellectualPropertyId the primary key for the new intellectual property
	* @return the new intellectual property
	*/
	public static IntellectualProperty create(long intellectualPropertyId) {
		return getPersistence().create(intellectualPropertyId);
	}

	/**
	* Removes the intellectual property with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param intellectualPropertyId the primary key of the intellectual property
	* @return the intellectual property that was removed
	* @throws NoSuchIntellectualPropertyException if a intellectual property with the primary key could not be found
	*/
	public static IntellectualProperty remove(long intellectualPropertyId)
		throws com.collaboratelab.impact.exception.NoSuchIntellectualPropertyException {
		return getPersistence().remove(intellectualPropertyId);
	}

	public static IntellectualProperty updateImpl(
		IntellectualProperty intellectualProperty) {
		return getPersistence().updateImpl(intellectualProperty);
	}

	/**
	* Returns the intellectual property with the primary key or throws a {@link NoSuchIntellectualPropertyException} if it could not be found.
	*
	* @param intellectualPropertyId the primary key of the intellectual property
	* @return the intellectual property
	* @throws NoSuchIntellectualPropertyException if a intellectual property with the primary key could not be found
	*/
	public static IntellectualProperty findByPrimaryKey(
		long intellectualPropertyId)
		throws com.collaboratelab.impact.exception.NoSuchIntellectualPropertyException {
		return getPersistence().findByPrimaryKey(intellectualPropertyId);
	}

	/**
	* Returns the intellectual property with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param intellectualPropertyId the primary key of the intellectual property
	* @return the intellectual property, or <code>null</code> if a intellectual property with the primary key could not be found
	*/
	public static IntellectualProperty fetchByPrimaryKey(
		long intellectualPropertyId) {
		return getPersistence().fetchByPrimaryKey(intellectualPropertyId);
	}

	public static java.util.Map<java.io.Serializable, IntellectualProperty> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the intellectual properties.
	*
	* @return the intellectual properties
	*/
	public static List<IntellectualProperty> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the intellectual properties.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of intellectual properties
	* @param end the upper bound of the range of intellectual properties (not inclusive)
	* @return the range of intellectual properties
	*/
	public static List<IntellectualProperty> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the intellectual properties.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of intellectual properties
	* @param end the upper bound of the range of intellectual properties (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of intellectual properties
	*/
	public static List<IntellectualProperty> findAll(int start, int end,
		OrderByComparator<IntellectualProperty> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the intellectual properties.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IntellectualPropertyModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of intellectual properties
	* @param end the upper bound of the range of intellectual properties (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of intellectual properties
	*/
	public static List<IntellectualProperty> findAll(int start, int end,
		OrderByComparator<IntellectualProperty> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the intellectual properties from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of intellectual properties.
	*
	* @return the number of intellectual properties
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static java.util.Set<java.lang.String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static IntellectualPropertyPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<IntellectualPropertyPersistence, IntellectualPropertyPersistence> _serviceTracker =
		ServiceTrackerFactory.open(IntellectualPropertyPersistence.class);
}