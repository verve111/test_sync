/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.impact.exception.NoSuchTeamMemberProjectRecognitionException;
import com.collaboratelab.impact.model.TeamMemberProjectRecognition;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the team member project recognition service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.collaboratelab.impact.service.persistence.impl.TeamMemberProjectRecognitionPersistenceImpl
 * @see TeamMemberProjectRecognitionUtil
 * @generated
 */
@ProviderType
public interface TeamMemberProjectRecognitionPersistence extends BasePersistence<TeamMemberProjectRecognition> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link TeamMemberProjectRecognitionUtil} to access the team member project recognition persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the team member project recognitions where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching team member project recognitions
	*/
	public java.util.List<TeamMemberProjectRecognition> findByUuid(
		java.lang.String uuid);

	/**
	* Returns a range of all the team member project recognitions where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of team member project recognitions
	* @param end the upper bound of the range of team member project recognitions (not inclusive)
	* @return the range of matching team member project recognitions
	*/
	public java.util.List<TeamMemberProjectRecognition> findByUuid(
		java.lang.String uuid, int start, int end);

	/**
	* Returns an ordered range of all the team member project recognitions where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of team member project recognitions
	* @param end the upper bound of the range of team member project recognitions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching team member project recognitions
	*/
	public java.util.List<TeamMemberProjectRecognition> findByUuid(
		java.lang.String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<TeamMemberProjectRecognition> orderByComparator);

	/**
	* Returns an ordered range of all the team member project recognitions where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of team member project recognitions
	* @param end the upper bound of the range of team member project recognitions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching team member project recognitions
	*/
	public java.util.List<TeamMemberProjectRecognition> findByUuid(
		java.lang.String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<TeamMemberProjectRecognition> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first team member project recognition in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching team member project recognition
	* @throws NoSuchTeamMemberProjectRecognitionException if a matching team member project recognition could not be found
	*/
	public TeamMemberProjectRecognition findByUuid_First(
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<TeamMemberProjectRecognition> orderByComparator)
		throws NoSuchTeamMemberProjectRecognitionException;

	/**
	* Returns the first team member project recognition in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching team member project recognition, or <code>null</code> if a matching team member project recognition could not be found
	*/
	public TeamMemberProjectRecognition fetchByUuid_First(
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<TeamMemberProjectRecognition> orderByComparator);

	/**
	* Returns the last team member project recognition in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching team member project recognition
	* @throws NoSuchTeamMemberProjectRecognitionException if a matching team member project recognition could not be found
	*/
	public TeamMemberProjectRecognition findByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<TeamMemberProjectRecognition> orderByComparator)
		throws NoSuchTeamMemberProjectRecognitionException;

	/**
	* Returns the last team member project recognition in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching team member project recognition, or <code>null</code> if a matching team member project recognition could not be found
	*/
	public TeamMemberProjectRecognition fetchByUuid_Last(
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<TeamMemberProjectRecognition> orderByComparator);

	/**
	* Returns the team member project recognitions before and after the current team member project recognition in the ordered set where uuid = &#63;.
	*
	* @param teamMemberProjectRecognitionId the primary key of the current team member project recognition
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next team member project recognition
	* @throws NoSuchTeamMemberProjectRecognitionException if a team member project recognition with the primary key could not be found
	*/
	public TeamMemberProjectRecognition[] findByUuid_PrevAndNext(
		long teamMemberProjectRecognitionId, java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<TeamMemberProjectRecognition> orderByComparator)
		throws NoSuchTeamMemberProjectRecognitionException;

	/**
	* Removes all the team member project recognitions where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public void removeByUuid(java.lang.String uuid);

	/**
	* Returns the number of team member project recognitions where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching team member project recognitions
	*/
	public int countByUuid(java.lang.String uuid);

	/**
	* Returns the team member project recognition where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchTeamMemberProjectRecognitionException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching team member project recognition
	* @throws NoSuchTeamMemberProjectRecognitionException if a matching team member project recognition could not be found
	*/
	public TeamMemberProjectRecognition findByUUID_G(java.lang.String uuid,
		long groupId) throws NoSuchTeamMemberProjectRecognitionException;

	/**
	* Returns the team member project recognition where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching team member project recognition, or <code>null</code> if a matching team member project recognition could not be found
	*/
	public TeamMemberProjectRecognition fetchByUUID_G(java.lang.String uuid,
		long groupId);

	/**
	* Returns the team member project recognition where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching team member project recognition, or <code>null</code> if a matching team member project recognition could not be found
	*/
	public TeamMemberProjectRecognition fetchByUUID_G(java.lang.String uuid,
		long groupId, boolean retrieveFromCache);

	/**
	* Removes the team member project recognition where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the team member project recognition that was removed
	*/
	public TeamMemberProjectRecognition removeByUUID_G(java.lang.String uuid,
		long groupId) throws NoSuchTeamMemberProjectRecognitionException;

	/**
	* Returns the number of team member project recognitions where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching team member project recognitions
	*/
	public int countByUUID_G(java.lang.String uuid, long groupId);

	/**
	* Returns all the team member project recognitions where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching team member project recognitions
	*/
	public java.util.List<TeamMemberProjectRecognition> findByUuid_C(
		java.lang.String uuid, long companyId);

	/**
	* Returns a range of all the team member project recognitions where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of team member project recognitions
	* @param end the upper bound of the range of team member project recognitions (not inclusive)
	* @return the range of matching team member project recognitions
	*/
	public java.util.List<TeamMemberProjectRecognition> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end);

	/**
	* Returns an ordered range of all the team member project recognitions where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of team member project recognitions
	* @param end the upper bound of the range of team member project recognitions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching team member project recognitions
	*/
	public java.util.List<TeamMemberProjectRecognition> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<TeamMemberProjectRecognition> orderByComparator);

	/**
	* Returns an ordered range of all the team member project recognitions where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of team member project recognitions
	* @param end the upper bound of the range of team member project recognitions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching team member project recognitions
	*/
	public java.util.List<TeamMemberProjectRecognition> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<TeamMemberProjectRecognition> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first team member project recognition in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching team member project recognition
	* @throws NoSuchTeamMemberProjectRecognitionException if a matching team member project recognition could not be found
	*/
	public TeamMemberProjectRecognition findByUuid_C_First(
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<TeamMemberProjectRecognition> orderByComparator)
		throws NoSuchTeamMemberProjectRecognitionException;

	/**
	* Returns the first team member project recognition in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching team member project recognition, or <code>null</code> if a matching team member project recognition could not be found
	*/
	public TeamMemberProjectRecognition fetchByUuid_C_First(
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<TeamMemberProjectRecognition> orderByComparator);

	/**
	* Returns the last team member project recognition in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching team member project recognition
	* @throws NoSuchTeamMemberProjectRecognitionException if a matching team member project recognition could not be found
	*/
	public TeamMemberProjectRecognition findByUuid_C_Last(
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<TeamMemberProjectRecognition> orderByComparator)
		throws NoSuchTeamMemberProjectRecognitionException;

	/**
	* Returns the last team member project recognition in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching team member project recognition, or <code>null</code> if a matching team member project recognition could not be found
	*/
	public TeamMemberProjectRecognition fetchByUuid_C_Last(
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<TeamMemberProjectRecognition> orderByComparator);

	/**
	* Returns the team member project recognitions before and after the current team member project recognition in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param teamMemberProjectRecognitionId the primary key of the current team member project recognition
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next team member project recognition
	* @throws NoSuchTeamMemberProjectRecognitionException if a team member project recognition with the primary key could not be found
	*/
	public TeamMemberProjectRecognition[] findByUuid_C_PrevAndNext(
		long teamMemberProjectRecognitionId, java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<TeamMemberProjectRecognition> orderByComparator)
		throws NoSuchTeamMemberProjectRecognitionException;

	/**
	* Removes all the team member project recognitions where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public void removeByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the number of team member project recognitions where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching team member project recognitions
	*/
	public int countByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the team member project recognition where teamMemberProjectRecognitionId = &#63; or throws a {@link NoSuchTeamMemberProjectRecognitionException} if it could not be found.
	*
	* @param teamMemberProjectRecognitionId the team member project recognition ID
	* @return the matching team member project recognition
	* @throws NoSuchTeamMemberProjectRecognitionException if a matching team member project recognition could not be found
	*/
	public TeamMemberProjectRecognition findByTeamMemberProjectRecognitionId(
		long teamMemberProjectRecognitionId)
		throws NoSuchTeamMemberProjectRecognitionException;

	/**
	* Returns the team member project recognition where teamMemberProjectRecognitionId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param teamMemberProjectRecognitionId the team member project recognition ID
	* @return the matching team member project recognition, or <code>null</code> if a matching team member project recognition could not be found
	*/
	public TeamMemberProjectRecognition fetchByTeamMemberProjectRecognitionId(
		long teamMemberProjectRecognitionId);

	/**
	* Returns the team member project recognition where teamMemberProjectRecognitionId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param teamMemberProjectRecognitionId the team member project recognition ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching team member project recognition, or <code>null</code> if a matching team member project recognition could not be found
	*/
	public TeamMemberProjectRecognition fetchByTeamMemberProjectRecognitionId(
		long teamMemberProjectRecognitionId, boolean retrieveFromCache);

	/**
	* Removes the team member project recognition where teamMemberProjectRecognitionId = &#63; from the database.
	*
	* @param teamMemberProjectRecognitionId the team member project recognition ID
	* @return the team member project recognition that was removed
	*/
	public TeamMemberProjectRecognition removeByTeamMemberProjectRecognitionId(
		long teamMemberProjectRecognitionId)
		throws NoSuchTeamMemberProjectRecognitionException;

	/**
	* Returns the number of team member project recognitions where teamMemberProjectRecognitionId = &#63;.
	*
	* @param teamMemberProjectRecognitionId the team member project recognition ID
	* @return the number of matching team member project recognitions
	*/
	public int countByTeamMemberProjectRecognitionId(
		long teamMemberProjectRecognitionId);

	/**
	* Returns all the team member project recognitions where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the matching team member project recognitions
	*/
	public java.util.List<TeamMemberProjectRecognition> findByGroupId(
		long groupId);

	/**
	* Returns a range of all the team member project recognitions where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of team member project recognitions
	* @param end the upper bound of the range of team member project recognitions (not inclusive)
	* @return the range of matching team member project recognitions
	*/
	public java.util.List<TeamMemberProjectRecognition> findByGroupId(
		long groupId, int start, int end);

	/**
	* Returns an ordered range of all the team member project recognitions where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of team member project recognitions
	* @param end the upper bound of the range of team member project recognitions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching team member project recognitions
	*/
	public java.util.List<TeamMemberProjectRecognition> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<TeamMemberProjectRecognition> orderByComparator);

	/**
	* Returns an ordered range of all the team member project recognitions where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of team member project recognitions
	* @param end the upper bound of the range of team member project recognitions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching team member project recognitions
	*/
	public java.util.List<TeamMemberProjectRecognition> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<TeamMemberProjectRecognition> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first team member project recognition in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching team member project recognition
	* @throws NoSuchTeamMemberProjectRecognitionException if a matching team member project recognition could not be found
	*/
	public TeamMemberProjectRecognition findByGroupId_First(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<TeamMemberProjectRecognition> orderByComparator)
		throws NoSuchTeamMemberProjectRecognitionException;

	/**
	* Returns the first team member project recognition in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching team member project recognition, or <code>null</code> if a matching team member project recognition could not be found
	*/
	public TeamMemberProjectRecognition fetchByGroupId_First(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<TeamMemberProjectRecognition> orderByComparator);

	/**
	* Returns the last team member project recognition in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching team member project recognition
	* @throws NoSuchTeamMemberProjectRecognitionException if a matching team member project recognition could not be found
	*/
	public TeamMemberProjectRecognition findByGroupId_Last(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<TeamMemberProjectRecognition> orderByComparator)
		throws NoSuchTeamMemberProjectRecognitionException;

	/**
	* Returns the last team member project recognition in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching team member project recognition, or <code>null</code> if a matching team member project recognition could not be found
	*/
	public TeamMemberProjectRecognition fetchByGroupId_Last(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<TeamMemberProjectRecognition> orderByComparator);

	/**
	* Returns the team member project recognitions before and after the current team member project recognition in the ordered set where groupId = &#63;.
	*
	* @param teamMemberProjectRecognitionId the primary key of the current team member project recognition
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next team member project recognition
	* @throws NoSuchTeamMemberProjectRecognitionException if a team member project recognition with the primary key could not be found
	*/
	public TeamMemberProjectRecognition[] findByGroupId_PrevAndNext(
		long teamMemberProjectRecognitionId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<TeamMemberProjectRecognition> orderByComparator)
		throws NoSuchTeamMemberProjectRecognitionException;

	/**
	* Removes all the team member project recognitions where groupId = &#63; from the database.
	*
	* @param groupId the group ID
	*/
	public void removeByGroupId(long groupId);

	/**
	* Returns the number of team member project recognitions where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the number of matching team member project recognitions
	*/
	public int countByGroupId(long groupId);

	/**
	* Caches the team member project recognition in the entity cache if it is enabled.
	*
	* @param teamMemberProjectRecognition the team member project recognition
	*/
	public void cacheResult(
		TeamMemberProjectRecognition teamMemberProjectRecognition);

	/**
	* Caches the team member project recognitions in the entity cache if it is enabled.
	*
	* @param teamMemberProjectRecognitions the team member project recognitions
	*/
	public void cacheResult(
		java.util.List<TeamMemberProjectRecognition> teamMemberProjectRecognitions);

	/**
	* Creates a new team member project recognition with the primary key. Does not add the team member project recognition to the database.
	*
	* @param teamMemberProjectRecognitionId the primary key for the new team member project recognition
	* @return the new team member project recognition
	*/
	public TeamMemberProjectRecognition create(
		long teamMemberProjectRecognitionId);

	/**
	* Removes the team member project recognition with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param teamMemberProjectRecognitionId the primary key of the team member project recognition
	* @return the team member project recognition that was removed
	* @throws NoSuchTeamMemberProjectRecognitionException if a team member project recognition with the primary key could not be found
	*/
	public TeamMemberProjectRecognition remove(
		long teamMemberProjectRecognitionId)
		throws NoSuchTeamMemberProjectRecognitionException;

	public TeamMemberProjectRecognition updateImpl(
		TeamMemberProjectRecognition teamMemberProjectRecognition);

	/**
	* Returns the team member project recognition with the primary key or throws a {@link NoSuchTeamMemberProjectRecognitionException} if it could not be found.
	*
	* @param teamMemberProjectRecognitionId the primary key of the team member project recognition
	* @return the team member project recognition
	* @throws NoSuchTeamMemberProjectRecognitionException if a team member project recognition with the primary key could not be found
	*/
	public TeamMemberProjectRecognition findByPrimaryKey(
		long teamMemberProjectRecognitionId)
		throws NoSuchTeamMemberProjectRecognitionException;

	/**
	* Returns the team member project recognition with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param teamMemberProjectRecognitionId the primary key of the team member project recognition
	* @return the team member project recognition, or <code>null</code> if a team member project recognition with the primary key could not be found
	*/
	public TeamMemberProjectRecognition fetchByPrimaryKey(
		long teamMemberProjectRecognitionId);

	@Override
	public java.util.Map<java.io.Serializable, TeamMemberProjectRecognition> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the team member project recognitions.
	*
	* @return the team member project recognitions
	*/
	public java.util.List<TeamMemberProjectRecognition> findAll();

	/**
	* Returns a range of all the team member project recognitions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of team member project recognitions
	* @param end the upper bound of the range of team member project recognitions (not inclusive)
	* @return the range of team member project recognitions
	*/
	public java.util.List<TeamMemberProjectRecognition> findAll(int start,
		int end);

	/**
	* Returns an ordered range of all the team member project recognitions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of team member project recognitions
	* @param end the upper bound of the range of team member project recognitions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of team member project recognitions
	*/
	public java.util.List<TeamMemberProjectRecognition> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<TeamMemberProjectRecognition> orderByComparator);

	/**
	* Returns an ordered range of all the team member project recognitions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of team member project recognitions
	* @param end the upper bound of the range of team member project recognitions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of team member project recognitions
	*/
	public java.util.List<TeamMemberProjectRecognition> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<TeamMemberProjectRecognition> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the team member project recognitions from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of team member project recognitions.
	*
	* @return the number of team member project recognitions
	*/
	public int countAll();

	@Override
	public java.util.Set<java.lang.String> getBadColumnNames();
}