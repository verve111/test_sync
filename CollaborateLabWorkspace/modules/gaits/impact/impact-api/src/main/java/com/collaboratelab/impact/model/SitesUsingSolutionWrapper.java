/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.exportimport.kernel.lar.StagedModelType;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link SitesUsingSolution}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see SitesUsingSolution
 * @generated
 */
@ProviderType
public class SitesUsingSolutionWrapper implements SitesUsingSolution,
	ModelWrapper<SitesUsingSolution> {
	public SitesUsingSolutionWrapper(SitesUsingSolution sitesUsingSolution) {
		_sitesUsingSolution = sitesUsingSolution;
	}

	@Override
	public Class<?> getModelClass() {
		return SitesUsingSolution.class;
	}

	@Override
	public String getModelClassName() {
		return SitesUsingSolution.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("sitesUsingSolutionId", getSitesUsingSolutionId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("asOfDate", getAsOfDate());
		attributes.put("number", getNumber());
		attributes.put("optionalComments", getOptionalComments());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long sitesUsingSolutionId = (Long)attributes.get("sitesUsingSolutionId");

		if (sitesUsingSolutionId != null) {
			setSitesUsingSolutionId(sitesUsingSolutionId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Date asOfDate = (Date)attributes.get("asOfDate");

		if (asOfDate != null) {
			setAsOfDate(asOfDate);
		}

		Integer number = (Integer)attributes.get("number");

		if (number != null) {
			setNumber(number);
		}

		String optionalComments = (String)attributes.get("optionalComments");

		if (optionalComments != null) {
			setOptionalComments(optionalComments);
		}
	}

	@Override
	public SitesUsingSolution toEscapedModel() {
		return new SitesUsingSolutionWrapper(_sitesUsingSolution.toEscapedModel());
	}

	@Override
	public SitesUsingSolution toUnescapedModel() {
		return new SitesUsingSolutionWrapper(_sitesUsingSolution.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _sitesUsingSolution.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _sitesUsingSolution.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _sitesUsingSolution.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _sitesUsingSolution.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<SitesUsingSolution> toCacheModel() {
		return _sitesUsingSolution.toCacheModel();
	}

	@Override
	public int compareTo(SitesUsingSolution sitesUsingSolution) {
		return _sitesUsingSolution.compareTo(sitesUsingSolution);
	}

	/**
	* Returns the number of this sites using solution.
	*
	* @return the number of this sites using solution
	*/
	@Override
	public int getNumber() {
		return _sitesUsingSolution.getNumber();
	}

	@Override
	public int hashCode() {
		return _sitesUsingSolution.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _sitesUsingSolution.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new SitesUsingSolutionWrapper((SitesUsingSolution)_sitesUsingSolution.clone());
	}

	/**
	* Returns the optional comments of this sites using solution.
	*
	* @return the optional comments of this sites using solution
	*/
	@Override
	public java.lang.String getOptionalComments() {
		return _sitesUsingSolution.getOptionalComments();
	}

	/**
	* Returns the user name of this sites using solution.
	*
	* @return the user name of this sites using solution
	*/
	@Override
	public java.lang.String getUserName() {
		return _sitesUsingSolution.getUserName();
	}

	/**
	* Returns the user uuid of this sites using solution.
	*
	* @return the user uuid of this sites using solution
	*/
	@Override
	public java.lang.String getUserUuid() {
		return _sitesUsingSolution.getUserUuid();
	}

	/**
	* Returns the uuid of this sites using solution.
	*
	* @return the uuid of this sites using solution
	*/
	@Override
	public java.lang.String getUuid() {
		return _sitesUsingSolution.getUuid();
	}

	@Override
	public java.lang.String toString() {
		return _sitesUsingSolution.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _sitesUsingSolution.toXmlString();
	}

	/**
	* Returns the as of date of this sites using solution.
	*
	* @return the as of date of this sites using solution
	*/
	@Override
	public Date getAsOfDate() {
		return _sitesUsingSolution.getAsOfDate();
	}

	/**
	* Returns the create date of this sites using solution.
	*
	* @return the create date of this sites using solution
	*/
	@Override
	public Date getCreateDate() {
		return _sitesUsingSolution.getCreateDate();
	}

	/**
	* Returns the modified date of this sites using solution.
	*
	* @return the modified date of this sites using solution
	*/
	@Override
	public Date getModifiedDate() {
		return _sitesUsingSolution.getModifiedDate();
	}

	/**
	* Returns the company ID of this sites using solution.
	*
	* @return the company ID of this sites using solution
	*/
	@Override
	public long getCompanyId() {
		return _sitesUsingSolution.getCompanyId();
	}

	/**
	* Returns the group ID of this sites using solution.
	*
	* @return the group ID of this sites using solution
	*/
	@Override
	public long getGroupId() {
		return _sitesUsingSolution.getGroupId();
	}

	/**
	* Returns the primary key of this sites using solution.
	*
	* @return the primary key of this sites using solution
	*/
	@Override
	public long getPrimaryKey() {
		return _sitesUsingSolution.getPrimaryKey();
	}

	/**
	* Returns the sites using solution ID of this sites using solution.
	*
	* @return the sites using solution ID of this sites using solution
	*/
	@Override
	public long getSitesUsingSolutionId() {
		return _sitesUsingSolution.getSitesUsingSolutionId();
	}

	/**
	* Returns the user ID of this sites using solution.
	*
	* @return the user ID of this sites using solution
	*/
	@Override
	public long getUserId() {
		return _sitesUsingSolution.getUserId();
	}

	@Override
	public void persist() {
		_sitesUsingSolution.persist();
	}

	/**
	* Sets the as of date of this sites using solution.
	*
	* @param asOfDate the as of date of this sites using solution
	*/
	@Override
	public void setAsOfDate(Date asOfDate) {
		_sitesUsingSolution.setAsOfDate(asOfDate);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_sitesUsingSolution.setCachedModel(cachedModel);
	}

	/**
	* Sets the company ID of this sites using solution.
	*
	* @param companyId the company ID of this sites using solution
	*/
	@Override
	public void setCompanyId(long companyId) {
		_sitesUsingSolution.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this sites using solution.
	*
	* @param createDate the create date of this sites using solution
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_sitesUsingSolution.setCreateDate(createDate);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_sitesUsingSolution.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_sitesUsingSolution.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_sitesUsingSolution.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the group ID of this sites using solution.
	*
	* @param groupId the group ID of this sites using solution
	*/
	@Override
	public void setGroupId(long groupId) {
		_sitesUsingSolution.setGroupId(groupId);
	}

	/**
	* Sets the modified date of this sites using solution.
	*
	* @param modifiedDate the modified date of this sites using solution
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_sitesUsingSolution.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_sitesUsingSolution.setNew(n);
	}

	/**
	* Sets the number of this sites using solution.
	*
	* @param number the number of this sites using solution
	*/
	@Override
	public void setNumber(int number) {
		_sitesUsingSolution.setNumber(number);
	}

	/**
	* Sets the optional comments of this sites using solution.
	*
	* @param optionalComments the optional comments of this sites using solution
	*/
	@Override
	public void setOptionalComments(java.lang.String optionalComments) {
		_sitesUsingSolution.setOptionalComments(optionalComments);
	}

	/**
	* Sets the primary key of this sites using solution.
	*
	* @param primaryKey the primary key of this sites using solution
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_sitesUsingSolution.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_sitesUsingSolution.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the sites using solution ID of this sites using solution.
	*
	* @param sitesUsingSolutionId the sites using solution ID of this sites using solution
	*/
	@Override
	public void setSitesUsingSolutionId(long sitesUsingSolutionId) {
		_sitesUsingSolution.setSitesUsingSolutionId(sitesUsingSolutionId);
	}

	/**
	* Sets the user ID of this sites using solution.
	*
	* @param userId the user ID of this sites using solution
	*/
	@Override
	public void setUserId(long userId) {
		_sitesUsingSolution.setUserId(userId);
	}

	/**
	* Sets the user name of this sites using solution.
	*
	* @param userName the user name of this sites using solution
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_sitesUsingSolution.setUserName(userName);
	}

	/**
	* Sets the user uuid of this sites using solution.
	*
	* @param userUuid the user uuid of this sites using solution
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_sitesUsingSolution.setUserUuid(userUuid);
	}

	/**
	* Sets the uuid of this sites using solution.
	*
	* @param uuid the uuid of this sites using solution
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_sitesUsingSolution.setUuid(uuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof SitesUsingSolutionWrapper)) {
			return false;
		}

		SitesUsingSolutionWrapper sitesUsingSolutionWrapper = (SitesUsingSolutionWrapper)obj;

		if (Objects.equals(_sitesUsingSolution,
					sitesUsingSolutionWrapper._sitesUsingSolution)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _sitesUsingSolution.getStagedModelType();
	}

	@Override
	public SitesUsingSolution getWrappedModel() {
		return _sitesUsingSolution;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _sitesUsingSolution.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _sitesUsingSolution.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_sitesUsingSolution.resetOriginalValues();
	}

	private final SitesUsingSolution _sitesUsingSolution;
}