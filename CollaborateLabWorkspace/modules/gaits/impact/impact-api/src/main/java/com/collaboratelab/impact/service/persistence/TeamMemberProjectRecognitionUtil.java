/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.impact.model.TeamMemberProjectRecognition;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the team member project recognition service. This utility wraps {@link com.collaboratelab.impact.service.persistence.impl.TeamMemberProjectRecognitionPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see TeamMemberProjectRecognitionPersistence
 * @see com.collaboratelab.impact.service.persistence.impl.TeamMemberProjectRecognitionPersistenceImpl
 * @generated
 */
@ProviderType
public class TeamMemberProjectRecognitionUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(
		TeamMemberProjectRecognition teamMemberProjectRecognition) {
		getPersistence().clearCache(teamMemberProjectRecognition);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<TeamMemberProjectRecognition> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<TeamMemberProjectRecognition> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<TeamMemberProjectRecognition> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static TeamMemberProjectRecognition update(
		TeamMemberProjectRecognition teamMemberProjectRecognition) {
		return getPersistence().update(teamMemberProjectRecognition);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static TeamMemberProjectRecognition update(
		TeamMemberProjectRecognition teamMemberProjectRecognition,
		ServiceContext serviceContext) {
		return getPersistence()
				   .update(teamMemberProjectRecognition, serviceContext);
	}

	/**
	* Returns all the team member project recognitions where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching team member project recognitions
	*/
	public static List<TeamMemberProjectRecognition> findByUuid(
		java.lang.String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	* Returns a range of all the team member project recognitions where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of team member project recognitions
	* @param end the upper bound of the range of team member project recognitions (not inclusive)
	* @return the range of matching team member project recognitions
	*/
	public static List<TeamMemberProjectRecognition> findByUuid(
		java.lang.String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	* Returns an ordered range of all the team member project recognitions where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of team member project recognitions
	* @param end the upper bound of the range of team member project recognitions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching team member project recognitions
	*/
	public static List<TeamMemberProjectRecognition> findByUuid(
		java.lang.String uuid, int start, int end,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator) {
		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the team member project recognitions where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of team member project recognitions
	* @param end the upper bound of the range of team member project recognitions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching team member project recognitions
	*/
	public static List<TeamMemberProjectRecognition> findByUuid(
		java.lang.String uuid, int start, int end,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid(uuid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first team member project recognition in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching team member project recognition
	* @throws NoSuchTeamMemberProjectRecognitionException if a matching team member project recognition could not be found
	*/
	public static TeamMemberProjectRecognition findByUuid_First(
		java.lang.String uuid,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchTeamMemberProjectRecognitionException {
		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the first team member project recognition in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching team member project recognition, or <code>null</code> if a matching team member project recognition could not be found
	*/
	public static TeamMemberProjectRecognition fetchByUuid_First(
		java.lang.String uuid,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator) {
		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the last team member project recognition in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching team member project recognition
	* @throws NoSuchTeamMemberProjectRecognitionException if a matching team member project recognition could not be found
	*/
	public static TeamMemberProjectRecognition findByUuid_Last(
		java.lang.String uuid,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchTeamMemberProjectRecognitionException {
		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the last team member project recognition in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching team member project recognition, or <code>null</code> if a matching team member project recognition could not be found
	*/
	public static TeamMemberProjectRecognition fetchByUuid_Last(
		java.lang.String uuid,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator) {
		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the team member project recognitions before and after the current team member project recognition in the ordered set where uuid = &#63;.
	*
	* @param teamMemberProjectRecognitionId the primary key of the current team member project recognition
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next team member project recognition
	* @throws NoSuchTeamMemberProjectRecognitionException if a team member project recognition with the primary key could not be found
	*/
	public static TeamMemberProjectRecognition[] findByUuid_PrevAndNext(
		long teamMemberProjectRecognitionId, java.lang.String uuid,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchTeamMemberProjectRecognitionException {
		return getPersistence()
				   .findByUuid_PrevAndNext(teamMemberProjectRecognitionId,
			uuid, orderByComparator);
	}

	/**
	* Removes all the team member project recognitions where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public static void removeByUuid(java.lang.String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	* Returns the number of team member project recognitions where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching team member project recognitions
	*/
	public static int countByUuid(java.lang.String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	* Returns the team member project recognition where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchTeamMemberProjectRecognitionException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching team member project recognition
	* @throws NoSuchTeamMemberProjectRecognitionException if a matching team member project recognition could not be found
	*/
	public static TeamMemberProjectRecognition findByUUID_G(
		java.lang.String uuid, long groupId)
		throws com.collaboratelab.impact.exception.NoSuchTeamMemberProjectRecognitionException {
		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	* Returns the team member project recognition where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching team member project recognition, or <code>null</code> if a matching team member project recognition could not be found
	*/
	public static TeamMemberProjectRecognition fetchByUUID_G(
		java.lang.String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	* Returns the team member project recognition where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching team member project recognition, or <code>null</code> if a matching team member project recognition could not be found
	*/
	public static TeamMemberProjectRecognition fetchByUUID_G(
		java.lang.String uuid, long groupId, boolean retrieveFromCache) {
		return getPersistence().fetchByUUID_G(uuid, groupId, retrieveFromCache);
	}

	/**
	* Removes the team member project recognition where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the team member project recognition that was removed
	*/
	public static TeamMemberProjectRecognition removeByUUID_G(
		java.lang.String uuid, long groupId)
		throws com.collaboratelab.impact.exception.NoSuchTeamMemberProjectRecognitionException {
		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	* Returns the number of team member project recognitions where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching team member project recognitions
	*/
	public static int countByUUID_G(java.lang.String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	* Returns all the team member project recognitions where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching team member project recognitions
	*/
	public static List<TeamMemberProjectRecognition> findByUuid_C(
		java.lang.String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	* Returns a range of all the team member project recognitions where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of team member project recognitions
	* @param end the upper bound of the range of team member project recognitions (not inclusive)
	* @return the range of matching team member project recognitions
	*/
	public static List<TeamMemberProjectRecognition> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end) {
		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	* Returns an ordered range of all the team member project recognitions where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of team member project recognitions
	* @param end the upper bound of the range of team member project recognitions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching team member project recognitions
	*/
	public static List<TeamMemberProjectRecognition> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the team member project recognitions where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of team member project recognitions
	* @param end the upper bound of the range of team member project recognitions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching team member project recognitions
	*/
	public static List<TeamMemberProjectRecognition> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first team member project recognition in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching team member project recognition
	* @throws NoSuchTeamMemberProjectRecognitionException if a matching team member project recognition could not be found
	*/
	public static TeamMemberProjectRecognition findByUuid_C_First(
		java.lang.String uuid, long companyId,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchTeamMemberProjectRecognitionException {
		return getPersistence()
				   .findByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the first team member project recognition in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching team member project recognition, or <code>null</code> if a matching team member project recognition could not be found
	*/
	public static TeamMemberProjectRecognition fetchByUuid_C_First(
		java.lang.String uuid, long companyId,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last team member project recognition in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching team member project recognition
	* @throws NoSuchTeamMemberProjectRecognitionException if a matching team member project recognition could not be found
	*/
	public static TeamMemberProjectRecognition findByUuid_C_Last(
		java.lang.String uuid, long companyId,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchTeamMemberProjectRecognitionException {
		return getPersistence()
				   .findByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last team member project recognition in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching team member project recognition, or <code>null</code> if a matching team member project recognition could not be found
	*/
	public static TeamMemberProjectRecognition fetchByUuid_C_Last(
		java.lang.String uuid, long companyId,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the team member project recognitions before and after the current team member project recognition in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param teamMemberProjectRecognitionId the primary key of the current team member project recognition
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next team member project recognition
	* @throws NoSuchTeamMemberProjectRecognitionException if a team member project recognition with the primary key could not be found
	*/
	public static TeamMemberProjectRecognition[] findByUuid_C_PrevAndNext(
		long teamMemberProjectRecognitionId, java.lang.String uuid,
		long companyId,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchTeamMemberProjectRecognitionException {
		return getPersistence()
				   .findByUuid_C_PrevAndNext(teamMemberProjectRecognitionId,
			uuid, companyId, orderByComparator);
	}

	/**
	* Removes all the team member project recognitions where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public static void removeByUuid_C(java.lang.String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	* Returns the number of team member project recognitions where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching team member project recognitions
	*/
	public static int countByUuid_C(java.lang.String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	* Returns the team member project recognition where teamMemberProjectRecognitionId = &#63; or throws a {@link NoSuchTeamMemberProjectRecognitionException} if it could not be found.
	*
	* @param teamMemberProjectRecognitionId the team member project recognition ID
	* @return the matching team member project recognition
	* @throws NoSuchTeamMemberProjectRecognitionException if a matching team member project recognition could not be found
	*/
	public static TeamMemberProjectRecognition findByTeamMemberProjectRecognitionId(
		long teamMemberProjectRecognitionId)
		throws com.collaboratelab.impact.exception.NoSuchTeamMemberProjectRecognitionException {
		return getPersistence()
				   .findByTeamMemberProjectRecognitionId(teamMemberProjectRecognitionId);
	}

	/**
	* Returns the team member project recognition where teamMemberProjectRecognitionId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param teamMemberProjectRecognitionId the team member project recognition ID
	* @return the matching team member project recognition, or <code>null</code> if a matching team member project recognition could not be found
	*/
	public static TeamMemberProjectRecognition fetchByTeamMemberProjectRecognitionId(
		long teamMemberProjectRecognitionId) {
		return getPersistence()
				   .fetchByTeamMemberProjectRecognitionId(teamMemberProjectRecognitionId);
	}

	/**
	* Returns the team member project recognition where teamMemberProjectRecognitionId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param teamMemberProjectRecognitionId the team member project recognition ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching team member project recognition, or <code>null</code> if a matching team member project recognition could not be found
	*/
	public static TeamMemberProjectRecognition fetchByTeamMemberProjectRecognitionId(
		long teamMemberProjectRecognitionId, boolean retrieveFromCache) {
		return getPersistence()
				   .fetchByTeamMemberProjectRecognitionId(teamMemberProjectRecognitionId,
			retrieveFromCache);
	}

	/**
	* Removes the team member project recognition where teamMemberProjectRecognitionId = &#63; from the database.
	*
	* @param teamMemberProjectRecognitionId the team member project recognition ID
	* @return the team member project recognition that was removed
	*/
	public static TeamMemberProjectRecognition removeByTeamMemberProjectRecognitionId(
		long teamMemberProjectRecognitionId)
		throws com.collaboratelab.impact.exception.NoSuchTeamMemberProjectRecognitionException {
		return getPersistence()
				   .removeByTeamMemberProjectRecognitionId(teamMemberProjectRecognitionId);
	}

	/**
	* Returns the number of team member project recognitions where teamMemberProjectRecognitionId = &#63;.
	*
	* @param teamMemberProjectRecognitionId the team member project recognition ID
	* @return the number of matching team member project recognitions
	*/
	public static int countByTeamMemberProjectRecognitionId(
		long teamMemberProjectRecognitionId) {
		return getPersistence()
				   .countByTeamMemberProjectRecognitionId(teamMemberProjectRecognitionId);
	}

	/**
	* Returns all the team member project recognitions where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the matching team member project recognitions
	*/
	public static List<TeamMemberProjectRecognition> findByGroupId(long groupId) {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	* Returns a range of all the team member project recognitions where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of team member project recognitions
	* @param end the upper bound of the range of team member project recognitions (not inclusive)
	* @return the range of matching team member project recognitions
	*/
	public static List<TeamMemberProjectRecognition> findByGroupId(
		long groupId, int start, int end) {
		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	* Returns an ordered range of all the team member project recognitions where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of team member project recognitions
	* @param end the upper bound of the range of team member project recognitions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching team member project recognitions
	*/
	public static List<TeamMemberProjectRecognition> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator) {
		return getPersistence()
				   .findByGroupId(groupId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the team member project recognitions where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of team member project recognitions
	* @param end the upper bound of the range of team member project recognitions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching team member project recognitions
	*/
	public static List<TeamMemberProjectRecognition> findByGroupId(
		long groupId, int start, int end,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByGroupId(groupId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first team member project recognition in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching team member project recognition
	* @throws NoSuchTeamMemberProjectRecognitionException if a matching team member project recognition could not be found
	*/
	public static TeamMemberProjectRecognition findByGroupId_First(
		long groupId,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchTeamMemberProjectRecognitionException {
		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	* Returns the first team member project recognition in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching team member project recognition, or <code>null</code> if a matching team member project recognition could not be found
	*/
	public static TeamMemberProjectRecognition fetchByGroupId_First(
		long groupId,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator) {
		return getPersistence().fetchByGroupId_First(groupId, orderByComparator);
	}

	/**
	* Returns the last team member project recognition in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching team member project recognition
	* @throws NoSuchTeamMemberProjectRecognitionException if a matching team member project recognition could not be found
	*/
	public static TeamMemberProjectRecognition findByGroupId_Last(
		long groupId,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchTeamMemberProjectRecognitionException {
		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	* Returns the last team member project recognition in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching team member project recognition, or <code>null</code> if a matching team member project recognition could not be found
	*/
	public static TeamMemberProjectRecognition fetchByGroupId_Last(
		long groupId,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator) {
		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	* Returns the team member project recognitions before and after the current team member project recognition in the ordered set where groupId = &#63;.
	*
	* @param teamMemberProjectRecognitionId the primary key of the current team member project recognition
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next team member project recognition
	* @throws NoSuchTeamMemberProjectRecognitionException if a team member project recognition with the primary key could not be found
	*/
	public static TeamMemberProjectRecognition[] findByGroupId_PrevAndNext(
		long teamMemberProjectRecognitionId, long groupId,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchTeamMemberProjectRecognitionException {
		return getPersistence()
				   .findByGroupId_PrevAndNext(teamMemberProjectRecognitionId,
			groupId, orderByComparator);
	}

	/**
	* Removes all the team member project recognitions where groupId = &#63; from the database.
	*
	* @param groupId the group ID
	*/
	public static void removeByGroupId(long groupId) {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	* Returns the number of team member project recognitions where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the number of matching team member project recognitions
	*/
	public static int countByGroupId(long groupId) {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	* Caches the team member project recognition in the entity cache if it is enabled.
	*
	* @param teamMemberProjectRecognition the team member project recognition
	*/
	public static void cacheResult(
		TeamMemberProjectRecognition teamMemberProjectRecognition) {
		getPersistence().cacheResult(teamMemberProjectRecognition);
	}

	/**
	* Caches the team member project recognitions in the entity cache if it is enabled.
	*
	* @param teamMemberProjectRecognitions the team member project recognitions
	*/
	public static void cacheResult(
		List<TeamMemberProjectRecognition> teamMemberProjectRecognitions) {
		getPersistence().cacheResult(teamMemberProjectRecognitions);
	}

	/**
	* Creates a new team member project recognition with the primary key. Does not add the team member project recognition to the database.
	*
	* @param teamMemberProjectRecognitionId the primary key for the new team member project recognition
	* @return the new team member project recognition
	*/
	public static TeamMemberProjectRecognition create(
		long teamMemberProjectRecognitionId) {
		return getPersistence().create(teamMemberProjectRecognitionId);
	}

	/**
	* Removes the team member project recognition with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param teamMemberProjectRecognitionId the primary key of the team member project recognition
	* @return the team member project recognition that was removed
	* @throws NoSuchTeamMemberProjectRecognitionException if a team member project recognition with the primary key could not be found
	*/
	public static TeamMemberProjectRecognition remove(
		long teamMemberProjectRecognitionId)
		throws com.collaboratelab.impact.exception.NoSuchTeamMemberProjectRecognitionException {
		return getPersistence().remove(teamMemberProjectRecognitionId);
	}

	public static TeamMemberProjectRecognition updateImpl(
		TeamMemberProjectRecognition teamMemberProjectRecognition) {
		return getPersistence().updateImpl(teamMemberProjectRecognition);
	}

	/**
	* Returns the team member project recognition with the primary key or throws a {@link NoSuchTeamMemberProjectRecognitionException} if it could not be found.
	*
	* @param teamMemberProjectRecognitionId the primary key of the team member project recognition
	* @return the team member project recognition
	* @throws NoSuchTeamMemberProjectRecognitionException if a team member project recognition with the primary key could not be found
	*/
	public static TeamMemberProjectRecognition findByPrimaryKey(
		long teamMemberProjectRecognitionId)
		throws com.collaboratelab.impact.exception.NoSuchTeamMemberProjectRecognitionException {
		return getPersistence().findByPrimaryKey(teamMemberProjectRecognitionId);
	}

	/**
	* Returns the team member project recognition with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param teamMemberProjectRecognitionId the primary key of the team member project recognition
	* @return the team member project recognition, or <code>null</code> if a team member project recognition with the primary key could not be found
	*/
	public static TeamMemberProjectRecognition fetchByPrimaryKey(
		long teamMemberProjectRecognitionId) {
		return getPersistence().fetchByPrimaryKey(teamMemberProjectRecognitionId);
	}

	public static java.util.Map<java.io.Serializable, TeamMemberProjectRecognition> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the team member project recognitions.
	*
	* @return the team member project recognitions
	*/
	public static List<TeamMemberProjectRecognition> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the team member project recognitions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of team member project recognitions
	* @param end the upper bound of the range of team member project recognitions (not inclusive)
	* @return the range of team member project recognitions
	*/
	public static List<TeamMemberProjectRecognition> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the team member project recognitions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of team member project recognitions
	* @param end the upper bound of the range of team member project recognitions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of team member project recognitions
	*/
	public static List<TeamMemberProjectRecognition> findAll(int start,
		int end,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the team member project recognitions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link TeamMemberProjectRecognitionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of team member project recognitions
	* @param end the upper bound of the range of team member project recognitions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of team member project recognitions
	*/
	public static List<TeamMemberProjectRecognition> findAll(int start,
		int end,
		OrderByComparator<TeamMemberProjectRecognition> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the team member project recognitions from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of team member project recognitions.
	*
	* @return the number of team member project recognitions
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static java.util.Set<java.lang.String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static TeamMemberProjectRecognitionPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<TeamMemberProjectRecognitionPersistence, TeamMemberProjectRecognitionPersistence> _serviceTracker =
		ServiceTrackerFactory.open(TeamMemberProjectRecognitionPersistence.class);
}