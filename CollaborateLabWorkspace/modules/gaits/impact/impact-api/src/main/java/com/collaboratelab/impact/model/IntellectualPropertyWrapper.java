/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.exportimport.kernel.lar.StagedModelType;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link IntellectualProperty}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see IntellectualProperty
 * @generated
 */
@ProviderType
public class IntellectualPropertyWrapper implements IntellectualProperty,
	ModelWrapper<IntellectualProperty> {
	public IntellectualPropertyWrapper(
		IntellectualProperty intellectualProperty) {
		_intellectualProperty = intellectualProperty;
	}

	@Override
	public Class<?> getModelClass() {
		return IntellectualProperty.class;
	}

	@Override
	public String getModelClassName() {
		return IntellectualProperty.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("intellectualPropertyId", getIntellectualPropertyId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("oneStFileDate", getOneStFileDate());
		attributes.put("asOfDate", getAsOfDate());
		attributes.put("title", getTitle());
		attributes.put("stage", getStage());
		attributes.put("licenseStatus", getLicenseStatus());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long intellectualPropertyId = (Long)attributes.get(
				"intellectualPropertyId");

		if (intellectualPropertyId != null) {
			setIntellectualPropertyId(intellectualPropertyId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Date oneStFileDate = (Date)attributes.get("oneStFileDate");

		if (oneStFileDate != null) {
			setOneStFileDate(oneStFileDate);
		}

		Date asOfDate = (Date)attributes.get("asOfDate");

		if (asOfDate != null) {
			setAsOfDate(asOfDate);
		}

		String title = (String)attributes.get("title");

		if (title != null) {
			setTitle(title);
		}

		Integer stage = (Integer)attributes.get("stage");

		if (stage != null) {
			setStage(stage);
		}

		Integer licenseStatus = (Integer)attributes.get("licenseStatus");

		if (licenseStatus != null) {
			setLicenseStatus(licenseStatus);
		}
	}

	@Override
	public IntellectualProperty toEscapedModel() {
		return new IntellectualPropertyWrapper(_intellectualProperty.toEscapedModel());
	}

	@Override
	public IntellectualProperty toUnescapedModel() {
		return new IntellectualPropertyWrapper(_intellectualProperty.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _intellectualProperty.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _intellectualProperty.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _intellectualProperty.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _intellectualProperty.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<IntellectualProperty> toCacheModel() {
		return _intellectualProperty.toCacheModel();
	}

	@Override
	public int compareTo(IntellectualProperty intellectualProperty) {
		return _intellectualProperty.compareTo(intellectualProperty);
	}

	/**
	* Returns the license status of this intellectual property.
	*
	* @return the license status of this intellectual property
	*/
	@Override
	public int getLicenseStatus() {
		return _intellectualProperty.getLicenseStatus();
	}

	/**
	* Returns the stage of this intellectual property.
	*
	* @return the stage of this intellectual property
	*/
	@Override
	public int getStage() {
		return _intellectualProperty.getStage();
	}

	@Override
	public int hashCode() {
		return _intellectualProperty.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _intellectualProperty.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new IntellectualPropertyWrapper((IntellectualProperty)_intellectualProperty.clone());
	}

	/**
	* Returns the title of this intellectual property.
	*
	* @return the title of this intellectual property
	*/
	@Override
	public java.lang.String getTitle() {
		return _intellectualProperty.getTitle();
	}

	/**
	* Returns the user name of this intellectual property.
	*
	* @return the user name of this intellectual property
	*/
	@Override
	public java.lang.String getUserName() {
		return _intellectualProperty.getUserName();
	}

	/**
	* Returns the user uuid of this intellectual property.
	*
	* @return the user uuid of this intellectual property
	*/
	@Override
	public java.lang.String getUserUuid() {
		return _intellectualProperty.getUserUuid();
	}

	/**
	* Returns the uuid of this intellectual property.
	*
	* @return the uuid of this intellectual property
	*/
	@Override
	public java.lang.String getUuid() {
		return _intellectualProperty.getUuid();
	}

	@Override
	public java.lang.String toString() {
		return _intellectualProperty.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _intellectualProperty.toXmlString();
	}

	/**
	* Returns the as of date of this intellectual property.
	*
	* @return the as of date of this intellectual property
	*/
	@Override
	public Date getAsOfDate() {
		return _intellectualProperty.getAsOfDate();
	}

	/**
	* Returns the create date of this intellectual property.
	*
	* @return the create date of this intellectual property
	*/
	@Override
	public Date getCreateDate() {
		return _intellectualProperty.getCreateDate();
	}

	/**
	* Returns the modified date of this intellectual property.
	*
	* @return the modified date of this intellectual property
	*/
	@Override
	public Date getModifiedDate() {
		return _intellectualProperty.getModifiedDate();
	}

	/**
	* Returns the one st file date of this intellectual property.
	*
	* @return the one st file date of this intellectual property
	*/
	@Override
	public Date getOneStFileDate() {
		return _intellectualProperty.getOneStFileDate();
	}

	/**
	* Returns the company ID of this intellectual property.
	*
	* @return the company ID of this intellectual property
	*/
	@Override
	public long getCompanyId() {
		return _intellectualProperty.getCompanyId();
	}

	/**
	* Returns the group ID of this intellectual property.
	*
	* @return the group ID of this intellectual property
	*/
	@Override
	public long getGroupId() {
		return _intellectualProperty.getGroupId();
	}

	/**
	* Returns the intellectual property ID of this intellectual property.
	*
	* @return the intellectual property ID of this intellectual property
	*/
	@Override
	public long getIntellectualPropertyId() {
		return _intellectualProperty.getIntellectualPropertyId();
	}

	/**
	* Returns the primary key of this intellectual property.
	*
	* @return the primary key of this intellectual property
	*/
	@Override
	public long getPrimaryKey() {
		return _intellectualProperty.getPrimaryKey();
	}

	/**
	* Returns the user ID of this intellectual property.
	*
	* @return the user ID of this intellectual property
	*/
	@Override
	public long getUserId() {
		return _intellectualProperty.getUserId();
	}

	@Override
	public void persist() {
		_intellectualProperty.persist();
	}

	/**
	* Sets the as of date of this intellectual property.
	*
	* @param asOfDate the as of date of this intellectual property
	*/
	@Override
	public void setAsOfDate(Date asOfDate) {
		_intellectualProperty.setAsOfDate(asOfDate);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_intellectualProperty.setCachedModel(cachedModel);
	}

	/**
	* Sets the company ID of this intellectual property.
	*
	* @param companyId the company ID of this intellectual property
	*/
	@Override
	public void setCompanyId(long companyId) {
		_intellectualProperty.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this intellectual property.
	*
	* @param createDate the create date of this intellectual property
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_intellectualProperty.setCreateDate(createDate);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_intellectualProperty.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_intellectualProperty.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_intellectualProperty.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the group ID of this intellectual property.
	*
	* @param groupId the group ID of this intellectual property
	*/
	@Override
	public void setGroupId(long groupId) {
		_intellectualProperty.setGroupId(groupId);
	}

	/**
	* Sets the intellectual property ID of this intellectual property.
	*
	* @param intellectualPropertyId the intellectual property ID of this intellectual property
	*/
	@Override
	public void setIntellectualPropertyId(long intellectualPropertyId) {
		_intellectualProperty.setIntellectualPropertyId(intellectualPropertyId);
	}

	/**
	* Sets the license status of this intellectual property.
	*
	* @param licenseStatus the license status of this intellectual property
	*/
	@Override
	public void setLicenseStatus(int licenseStatus) {
		_intellectualProperty.setLicenseStatus(licenseStatus);
	}

	/**
	* Sets the modified date of this intellectual property.
	*
	* @param modifiedDate the modified date of this intellectual property
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_intellectualProperty.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_intellectualProperty.setNew(n);
	}

	/**
	* Sets the one st file date of this intellectual property.
	*
	* @param oneStFileDate the one st file date of this intellectual property
	*/
	@Override
	public void setOneStFileDate(Date oneStFileDate) {
		_intellectualProperty.setOneStFileDate(oneStFileDate);
	}

	/**
	* Sets the primary key of this intellectual property.
	*
	* @param primaryKey the primary key of this intellectual property
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_intellectualProperty.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_intellectualProperty.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the stage of this intellectual property.
	*
	* @param stage the stage of this intellectual property
	*/
	@Override
	public void setStage(int stage) {
		_intellectualProperty.setStage(stage);
	}

	/**
	* Sets the title of this intellectual property.
	*
	* @param title the title of this intellectual property
	*/
	@Override
	public void setTitle(java.lang.String title) {
		_intellectualProperty.setTitle(title);
	}

	/**
	* Sets the user ID of this intellectual property.
	*
	* @param userId the user ID of this intellectual property
	*/
	@Override
	public void setUserId(long userId) {
		_intellectualProperty.setUserId(userId);
	}

	/**
	* Sets the user name of this intellectual property.
	*
	* @param userName the user name of this intellectual property
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_intellectualProperty.setUserName(userName);
	}

	/**
	* Sets the user uuid of this intellectual property.
	*
	* @param userUuid the user uuid of this intellectual property
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_intellectualProperty.setUserUuid(userUuid);
	}

	/**
	* Sets the uuid of this intellectual property.
	*
	* @param uuid the uuid of this intellectual property
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_intellectualProperty.setUuid(uuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof IntellectualPropertyWrapper)) {
			return false;
		}

		IntellectualPropertyWrapper intellectualPropertyWrapper = (IntellectualPropertyWrapper)obj;

		if (Objects.equals(_intellectualProperty,
					intellectualPropertyWrapper._intellectualProperty)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _intellectualProperty.getStagedModelType();
	}

	@Override
	public IntellectualProperty getWrappedModel() {
		return _intellectualProperty;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _intellectualProperty.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _intellectualProperty.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_intellectualProperty.resetOriginalValues();
	}

	private final IntellectualProperty _intellectualProperty;
}