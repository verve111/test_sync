/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.impact.model.SitesUsingSolution;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the sites using solution service. This utility wraps {@link com.collaboratelab.impact.service.persistence.impl.SitesUsingSolutionPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see SitesUsingSolutionPersistence
 * @see com.collaboratelab.impact.service.persistence.impl.SitesUsingSolutionPersistenceImpl
 * @generated
 */
@ProviderType
public class SitesUsingSolutionUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(SitesUsingSolution sitesUsingSolution) {
		getPersistence().clearCache(sitesUsingSolution);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<SitesUsingSolution> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<SitesUsingSolution> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<SitesUsingSolution> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<SitesUsingSolution> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static SitesUsingSolution update(
		SitesUsingSolution sitesUsingSolution) {
		return getPersistence().update(sitesUsingSolution);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static SitesUsingSolution update(
		SitesUsingSolution sitesUsingSolution, ServiceContext serviceContext) {
		return getPersistence().update(sitesUsingSolution, serviceContext);
	}

	/**
	* Returns all the sites using solutions where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching sites using solutions
	*/
	public static List<SitesUsingSolution> findByUuid(java.lang.String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	* Returns a range of all the sites using solutions where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of sites using solutions
	* @param end the upper bound of the range of sites using solutions (not inclusive)
	* @return the range of matching sites using solutions
	*/
	public static List<SitesUsingSolution> findByUuid(java.lang.String uuid,
		int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	* Returns an ordered range of all the sites using solutions where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of sites using solutions
	* @param end the upper bound of the range of sites using solutions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching sites using solutions
	*/
	public static List<SitesUsingSolution> findByUuid(java.lang.String uuid,
		int start, int end,
		OrderByComparator<SitesUsingSolution> orderByComparator) {
		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the sites using solutions where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of sites using solutions
	* @param end the upper bound of the range of sites using solutions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching sites using solutions
	*/
	public static List<SitesUsingSolution> findByUuid(java.lang.String uuid,
		int start, int end,
		OrderByComparator<SitesUsingSolution> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid(uuid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first sites using solution in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching sites using solution
	* @throws NoSuchSitesUsingSolutionException if a matching sites using solution could not be found
	*/
	public static SitesUsingSolution findByUuid_First(java.lang.String uuid,
		OrderByComparator<SitesUsingSolution> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchSitesUsingSolutionException {
		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the first sites using solution in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching sites using solution, or <code>null</code> if a matching sites using solution could not be found
	*/
	public static SitesUsingSolution fetchByUuid_First(java.lang.String uuid,
		OrderByComparator<SitesUsingSolution> orderByComparator) {
		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the last sites using solution in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching sites using solution
	* @throws NoSuchSitesUsingSolutionException if a matching sites using solution could not be found
	*/
	public static SitesUsingSolution findByUuid_Last(java.lang.String uuid,
		OrderByComparator<SitesUsingSolution> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchSitesUsingSolutionException {
		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the last sites using solution in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching sites using solution, or <code>null</code> if a matching sites using solution could not be found
	*/
	public static SitesUsingSolution fetchByUuid_Last(java.lang.String uuid,
		OrderByComparator<SitesUsingSolution> orderByComparator) {
		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the sites using solutions before and after the current sites using solution in the ordered set where uuid = &#63;.
	*
	* @param sitesUsingSolutionId the primary key of the current sites using solution
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next sites using solution
	* @throws NoSuchSitesUsingSolutionException if a sites using solution with the primary key could not be found
	*/
	public static SitesUsingSolution[] findByUuid_PrevAndNext(
		long sitesUsingSolutionId, java.lang.String uuid,
		OrderByComparator<SitesUsingSolution> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchSitesUsingSolutionException {
		return getPersistence()
				   .findByUuid_PrevAndNext(sitesUsingSolutionId, uuid,
			orderByComparator);
	}

	/**
	* Removes all the sites using solutions where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public static void removeByUuid(java.lang.String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	* Returns the number of sites using solutions where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching sites using solutions
	*/
	public static int countByUuid(java.lang.String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	* Returns the sites using solution where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchSitesUsingSolutionException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching sites using solution
	* @throws NoSuchSitesUsingSolutionException if a matching sites using solution could not be found
	*/
	public static SitesUsingSolution findByUUID_G(java.lang.String uuid,
		long groupId)
		throws com.collaboratelab.impact.exception.NoSuchSitesUsingSolutionException {
		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	* Returns the sites using solution where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching sites using solution, or <code>null</code> if a matching sites using solution could not be found
	*/
	public static SitesUsingSolution fetchByUUID_G(java.lang.String uuid,
		long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	* Returns the sites using solution where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching sites using solution, or <code>null</code> if a matching sites using solution could not be found
	*/
	public static SitesUsingSolution fetchByUUID_G(java.lang.String uuid,
		long groupId, boolean retrieveFromCache) {
		return getPersistence().fetchByUUID_G(uuid, groupId, retrieveFromCache);
	}

	/**
	* Removes the sites using solution where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the sites using solution that was removed
	*/
	public static SitesUsingSolution removeByUUID_G(java.lang.String uuid,
		long groupId)
		throws com.collaboratelab.impact.exception.NoSuchSitesUsingSolutionException {
		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	* Returns the number of sites using solutions where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching sites using solutions
	*/
	public static int countByUUID_G(java.lang.String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	* Returns all the sites using solutions where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching sites using solutions
	*/
	public static List<SitesUsingSolution> findByUuid_C(java.lang.String uuid,
		long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	* Returns a range of all the sites using solutions where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of sites using solutions
	* @param end the upper bound of the range of sites using solutions (not inclusive)
	* @return the range of matching sites using solutions
	*/
	public static List<SitesUsingSolution> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end) {
		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	* Returns an ordered range of all the sites using solutions where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of sites using solutions
	* @param end the upper bound of the range of sites using solutions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching sites using solutions
	*/
	public static List<SitesUsingSolution> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<SitesUsingSolution> orderByComparator) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the sites using solutions where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of sites using solutions
	* @param end the upper bound of the range of sites using solutions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching sites using solutions
	*/
	public static List<SitesUsingSolution> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		OrderByComparator<SitesUsingSolution> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first sites using solution in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching sites using solution
	* @throws NoSuchSitesUsingSolutionException if a matching sites using solution could not be found
	*/
	public static SitesUsingSolution findByUuid_C_First(java.lang.String uuid,
		long companyId, OrderByComparator<SitesUsingSolution> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchSitesUsingSolutionException {
		return getPersistence()
				   .findByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the first sites using solution in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching sites using solution, or <code>null</code> if a matching sites using solution could not be found
	*/
	public static SitesUsingSolution fetchByUuid_C_First(
		java.lang.String uuid, long companyId,
		OrderByComparator<SitesUsingSolution> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last sites using solution in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching sites using solution
	* @throws NoSuchSitesUsingSolutionException if a matching sites using solution could not be found
	*/
	public static SitesUsingSolution findByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<SitesUsingSolution> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchSitesUsingSolutionException {
		return getPersistence()
				   .findByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last sites using solution in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching sites using solution, or <code>null</code> if a matching sites using solution could not be found
	*/
	public static SitesUsingSolution fetchByUuid_C_Last(java.lang.String uuid,
		long companyId, OrderByComparator<SitesUsingSolution> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the sites using solutions before and after the current sites using solution in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param sitesUsingSolutionId the primary key of the current sites using solution
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next sites using solution
	* @throws NoSuchSitesUsingSolutionException if a sites using solution with the primary key could not be found
	*/
	public static SitesUsingSolution[] findByUuid_C_PrevAndNext(
		long sitesUsingSolutionId, java.lang.String uuid, long companyId,
		OrderByComparator<SitesUsingSolution> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchSitesUsingSolutionException {
		return getPersistence()
				   .findByUuid_C_PrevAndNext(sitesUsingSolutionId, uuid,
			companyId, orderByComparator);
	}

	/**
	* Removes all the sites using solutions where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public static void removeByUuid_C(java.lang.String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	* Returns the number of sites using solutions where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching sites using solutions
	*/
	public static int countByUuid_C(java.lang.String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	* Returns the sites using solution where sitesUsingSolutionId = &#63; or throws a {@link NoSuchSitesUsingSolutionException} if it could not be found.
	*
	* @param sitesUsingSolutionId the sites using solution ID
	* @return the matching sites using solution
	* @throws NoSuchSitesUsingSolutionException if a matching sites using solution could not be found
	*/
	public static SitesUsingSolution findBySitesUsingSolutionId(
		long sitesUsingSolutionId)
		throws com.collaboratelab.impact.exception.NoSuchSitesUsingSolutionException {
		return getPersistence().findBySitesUsingSolutionId(sitesUsingSolutionId);
	}

	/**
	* Returns the sites using solution where sitesUsingSolutionId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param sitesUsingSolutionId the sites using solution ID
	* @return the matching sites using solution, or <code>null</code> if a matching sites using solution could not be found
	*/
	public static SitesUsingSolution fetchBySitesUsingSolutionId(
		long sitesUsingSolutionId) {
		return getPersistence().fetchBySitesUsingSolutionId(sitesUsingSolutionId);
	}

	/**
	* Returns the sites using solution where sitesUsingSolutionId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param sitesUsingSolutionId the sites using solution ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching sites using solution, or <code>null</code> if a matching sites using solution could not be found
	*/
	public static SitesUsingSolution fetchBySitesUsingSolutionId(
		long sitesUsingSolutionId, boolean retrieveFromCache) {
		return getPersistence()
				   .fetchBySitesUsingSolutionId(sitesUsingSolutionId,
			retrieveFromCache);
	}

	/**
	* Removes the sites using solution where sitesUsingSolutionId = &#63; from the database.
	*
	* @param sitesUsingSolutionId the sites using solution ID
	* @return the sites using solution that was removed
	*/
	public static SitesUsingSolution removeBySitesUsingSolutionId(
		long sitesUsingSolutionId)
		throws com.collaboratelab.impact.exception.NoSuchSitesUsingSolutionException {
		return getPersistence()
				   .removeBySitesUsingSolutionId(sitesUsingSolutionId);
	}

	/**
	* Returns the number of sites using solutions where sitesUsingSolutionId = &#63;.
	*
	* @param sitesUsingSolutionId the sites using solution ID
	* @return the number of matching sites using solutions
	*/
	public static int countBySitesUsingSolutionId(long sitesUsingSolutionId) {
		return getPersistence().countBySitesUsingSolutionId(sitesUsingSolutionId);
	}

	/**
	* Returns all the sites using solutions where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the matching sites using solutions
	*/
	public static List<SitesUsingSolution> findByGroupId(long groupId) {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	* Returns a range of all the sites using solutions where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of sites using solutions
	* @param end the upper bound of the range of sites using solutions (not inclusive)
	* @return the range of matching sites using solutions
	*/
	public static List<SitesUsingSolution> findByGroupId(long groupId,
		int start, int end) {
		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	* Returns an ordered range of all the sites using solutions where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of sites using solutions
	* @param end the upper bound of the range of sites using solutions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching sites using solutions
	*/
	public static List<SitesUsingSolution> findByGroupId(long groupId,
		int start, int end,
		OrderByComparator<SitesUsingSolution> orderByComparator) {
		return getPersistence()
				   .findByGroupId(groupId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the sites using solutions where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of sites using solutions
	* @param end the upper bound of the range of sites using solutions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching sites using solutions
	*/
	public static List<SitesUsingSolution> findByGroupId(long groupId,
		int start, int end,
		OrderByComparator<SitesUsingSolution> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByGroupId(groupId, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first sites using solution in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching sites using solution
	* @throws NoSuchSitesUsingSolutionException if a matching sites using solution could not be found
	*/
	public static SitesUsingSolution findByGroupId_First(long groupId,
		OrderByComparator<SitesUsingSolution> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchSitesUsingSolutionException {
		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	* Returns the first sites using solution in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching sites using solution, or <code>null</code> if a matching sites using solution could not be found
	*/
	public static SitesUsingSolution fetchByGroupId_First(long groupId,
		OrderByComparator<SitesUsingSolution> orderByComparator) {
		return getPersistence().fetchByGroupId_First(groupId, orderByComparator);
	}

	/**
	* Returns the last sites using solution in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching sites using solution
	* @throws NoSuchSitesUsingSolutionException if a matching sites using solution could not be found
	*/
	public static SitesUsingSolution findByGroupId_Last(long groupId,
		OrderByComparator<SitesUsingSolution> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchSitesUsingSolutionException {
		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	* Returns the last sites using solution in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching sites using solution, or <code>null</code> if a matching sites using solution could not be found
	*/
	public static SitesUsingSolution fetchByGroupId_Last(long groupId,
		OrderByComparator<SitesUsingSolution> orderByComparator) {
		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	* Returns the sites using solutions before and after the current sites using solution in the ordered set where groupId = &#63;.
	*
	* @param sitesUsingSolutionId the primary key of the current sites using solution
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next sites using solution
	* @throws NoSuchSitesUsingSolutionException if a sites using solution with the primary key could not be found
	*/
	public static SitesUsingSolution[] findByGroupId_PrevAndNext(
		long sitesUsingSolutionId, long groupId,
		OrderByComparator<SitesUsingSolution> orderByComparator)
		throws com.collaboratelab.impact.exception.NoSuchSitesUsingSolutionException {
		return getPersistence()
				   .findByGroupId_PrevAndNext(sitesUsingSolutionId, groupId,
			orderByComparator);
	}

	/**
	* Removes all the sites using solutions where groupId = &#63; from the database.
	*
	* @param groupId the group ID
	*/
	public static void removeByGroupId(long groupId) {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	* Returns the number of sites using solutions where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the number of matching sites using solutions
	*/
	public static int countByGroupId(long groupId) {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	* Caches the sites using solution in the entity cache if it is enabled.
	*
	* @param sitesUsingSolution the sites using solution
	*/
	public static void cacheResult(SitesUsingSolution sitesUsingSolution) {
		getPersistence().cacheResult(sitesUsingSolution);
	}

	/**
	* Caches the sites using solutions in the entity cache if it is enabled.
	*
	* @param sitesUsingSolutions the sites using solutions
	*/
	public static void cacheResult(List<SitesUsingSolution> sitesUsingSolutions) {
		getPersistence().cacheResult(sitesUsingSolutions);
	}

	/**
	* Creates a new sites using solution with the primary key. Does not add the sites using solution to the database.
	*
	* @param sitesUsingSolutionId the primary key for the new sites using solution
	* @return the new sites using solution
	*/
	public static SitesUsingSolution create(long sitesUsingSolutionId) {
		return getPersistence().create(sitesUsingSolutionId);
	}

	/**
	* Removes the sites using solution with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param sitesUsingSolutionId the primary key of the sites using solution
	* @return the sites using solution that was removed
	* @throws NoSuchSitesUsingSolutionException if a sites using solution with the primary key could not be found
	*/
	public static SitesUsingSolution remove(long sitesUsingSolutionId)
		throws com.collaboratelab.impact.exception.NoSuchSitesUsingSolutionException {
		return getPersistence().remove(sitesUsingSolutionId);
	}

	public static SitesUsingSolution updateImpl(
		SitesUsingSolution sitesUsingSolution) {
		return getPersistence().updateImpl(sitesUsingSolution);
	}

	/**
	* Returns the sites using solution with the primary key or throws a {@link NoSuchSitesUsingSolutionException} if it could not be found.
	*
	* @param sitesUsingSolutionId the primary key of the sites using solution
	* @return the sites using solution
	* @throws NoSuchSitesUsingSolutionException if a sites using solution with the primary key could not be found
	*/
	public static SitesUsingSolution findByPrimaryKey(long sitesUsingSolutionId)
		throws com.collaboratelab.impact.exception.NoSuchSitesUsingSolutionException {
		return getPersistence().findByPrimaryKey(sitesUsingSolutionId);
	}

	/**
	* Returns the sites using solution with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param sitesUsingSolutionId the primary key of the sites using solution
	* @return the sites using solution, or <code>null</code> if a sites using solution with the primary key could not be found
	*/
	public static SitesUsingSolution fetchByPrimaryKey(
		long sitesUsingSolutionId) {
		return getPersistence().fetchByPrimaryKey(sitesUsingSolutionId);
	}

	public static java.util.Map<java.io.Serializable, SitesUsingSolution> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the sites using solutions.
	*
	* @return the sites using solutions
	*/
	public static List<SitesUsingSolution> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the sites using solutions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of sites using solutions
	* @param end the upper bound of the range of sites using solutions (not inclusive)
	* @return the range of sites using solutions
	*/
	public static List<SitesUsingSolution> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the sites using solutions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of sites using solutions
	* @param end the upper bound of the range of sites using solutions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of sites using solutions
	*/
	public static List<SitesUsingSolution> findAll(int start, int end,
		OrderByComparator<SitesUsingSolution> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the sites using solutions.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SitesUsingSolutionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of sites using solutions
	* @param end the upper bound of the range of sites using solutions (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of sites using solutions
	*/
	public static List<SitesUsingSolution> findAll(int start, int end,
		OrderByComparator<SitesUsingSolution> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the sites using solutions from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of sites using solutions.
	*
	* @return the number of sites using solutions
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static java.util.Set<java.lang.String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static SitesUsingSolutionPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<SitesUsingSolutionPersistence, SitesUsingSolutionPersistence> _serviceTracker =
		ServiceTrackerFactory.open(SitesUsingSolutionPersistence.class);
}