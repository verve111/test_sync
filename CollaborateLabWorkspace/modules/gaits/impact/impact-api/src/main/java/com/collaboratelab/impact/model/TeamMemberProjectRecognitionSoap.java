/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.collaboratelab.impact.service.http.TeamMemberProjectRecognitionServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see com.collaboratelab.impact.service.http.TeamMemberProjectRecognitionServiceSoap
 * @generated
 */
@ProviderType
public class TeamMemberProjectRecognitionSoap implements Serializable {
	public static TeamMemberProjectRecognitionSoap toSoapModel(
		TeamMemberProjectRecognition model) {
		TeamMemberProjectRecognitionSoap soapModel = new TeamMemberProjectRecognitionSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setTeamMemberProjectRecognitionId(model.getTeamMemberProjectRecognitionId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setDateOfRecognition(model.getDateOfRecognition());
		soapModel.setName(model.getName());
		soapModel.setCatRating(model.getCatRating());
		soapModel.setURL(model.getURL());
		soapModel.setOptionalComments(model.getOptionalComments());

		return soapModel;
	}

	public static TeamMemberProjectRecognitionSoap[] toSoapModels(
		TeamMemberProjectRecognition[] models) {
		TeamMemberProjectRecognitionSoap[] soapModels = new TeamMemberProjectRecognitionSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static TeamMemberProjectRecognitionSoap[][] toSoapModels(
		TeamMemberProjectRecognition[][] models) {
		TeamMemberProjectRecognitionSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new TeamMemberProjectRecognitionSoap[models.length][models[0].length];
		}
		else {
			soapModels = new TeamMemberProjectRecognitionSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static TeamMemberProjectRecognitionSoap[] toSoapModels(
		List<TeamMemberProjectRecognition> models) {
		List<TeamMemberProjectRecognitionSoap> soapModels = new ArrayList<TeamMemberProjectRecognitionSoap>(models.size());

		for (TeamMemberProjectRecognition model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new TeamMemberProjectRecognitionSoap[soapModels.size()]);
	}

	public TeamMemberProjectRecognitionSoap() {
	}

	public long getPrimaryKey() {
		return _teamMemberProjectRecognitionId;
	}

	public void setPrimaryKey(long pk) {
		setTeamMemberProjectRecognitionId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getTeamMemberProjectRecognitionId() {
		return _teamMemberProjectRecognitionId;
	}

	public void setTeamMemberProjectRecognitionId(
		long teamMemberProjectRecognitionId) {
		_teamMemberProjectRecognitionId = teamMemberProjectRecognitionId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public Date getDateOfRecognition() {
		return _dateOfRecognition;
	}

	public void setDateOfRecognition(Date dateOfRecognition) {
		_dateOfRecognition = dateOfRecognition;
	}

	public String getName() {
		return _Name;
	}

	public void setName(String Name) {
		_Name = Name;
	}

	public int getCatRating() {
		return _CatRating;
	}

	public void setCatRating(int CatRating) {
		_CatRating = CatRating;
	}

	public String getURL() {
		return _URL;
	}

	public void setURL(String URL) {
		_URL = URL;
	}

	public String getOptionalComments() {
		return _optionalComments;
	}

	public void setOptionalComments(String optionalComments) {
		_optionalComments = optionalComments;
	}

	private String _uuid;
	private long _teamMemberProjectRecognitionId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private Date _dateOfRecognition;
	private String _Name;
	private int _CatRating;
	private String _URL;
	private String _optionalComments;
}