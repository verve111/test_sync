/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.collaboratelab.impact.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.collaboratelab.impact.exception.NoSuchCareerImpactException;
import com.collaboratelab.impact.model.CareerImpact;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the career impact service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.collaboratelab.impact.service.persistence.impl.CareerImpactPersistenceImpl
 * @see CareerImpactUtil
 * @generated
 */
@ProviderType
public interface CareerImpactPersistence extends BasePersistence<CareerImpact> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CareerImpactUtil} to access the career impact persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the career impacts where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching career impacts
	*/
	public java.util.List<CareerImpact> findByUuid(java.lang.String uuid);

	/**
	* Returns a range of all the career impacts where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of career impacts
	* @param end the upper bound of the range of career impacts (not inclusive)
	* @return the range of matching career impacts
	*/
	public java.util.List<CareerImpact> findByUuid(java.lang.String uuid,
		int start, int end);

	/**
	* Returns an ordered range of all the career impacts where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of career impacts
	* @param end the upper bound of the range of career impacts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching career impacts
	*/
	public java.util.List<CareerImpact> findByUuid(java.lang.String uuid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CareerImpact> orderByComparator);

	/**
	* Returns an ordered range of all the career impacts where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of career impacts
	* @param end the upper bound of the range of career impacts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching career impacts
	*/
	public java.util.List<CareerImpact> findByUuid(java.lang.String uuid,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CareerImpact> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first career impact in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching career impact
	* @throws NoSuchCareerImpactException if a matching career impact could not be found
	*/
	public CareerImpact findByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<CareerImpact> orderByComparator)
		throws NoSuchCareerImpactException;

	/**
	* Returns the first career impact in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching career impact, or <code>null</code> if a matching career impact could not be found
	*/
	public CareerImpact fetchByUuid_First(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<CareerImpact> orderByComparator);

	/**
	* Returns the last career impact in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching career impact
	* @throws NoSuchCareerImpactException if a matching career impact could not be found
	*/
	public CareerImpact findByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<CareerImpact> orderByComparator)
		throws NoSuchCareerImpactException;

	/**
	* Returns the last career impact in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching career impact, or <code>null</code> if a matching career impact could not be found
	*/
	public CareerImpact fetchByUuid_Last(java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<CareerImpact> orderByComparator);

	/**
	* Returns the career impacts before and after the current career impact in the ordered set where uuid = &#63;.
	*
	* @param careerImpactId the primary key of the current career impact
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next career impact
	* @throws NoSuchCareerImpactException if a career impact with the primary key could not be found
	*/
	public CareerImpact[] findByUuid_PrevAndNext(long careerImpactId,
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<CareerImpact> orderByComparator)
		throws NoSuchCareerImpactException;

	/**
	* Removes all the career impacts where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public void removeByUuid(java.lang.String uuid);

	/**
	* Returns the number of career impacts where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching career impacts
	*/
	public int countByUuid(java.lang.String uuid);

	/**
	* Returns the career impact where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchCareerImpactException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching career impact
	* @throws NoSuchCareerImpactException if a matching career impact could not be found
	*/
	public CareerImpact findByUUID_G(java.lang.String uuid, long groupId)
		throws NoSuchCareerImpactException;

	/**
	* Returns the career impact where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching career impact, or <code>null</code> if a matching career impact could not be found
	*/
	public CareerImpact fetchByUUID_G(java.lang.String uuid, long groupId);

	/**
	* Returns the career impact where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching career impact, or <code>null</code> if a matching career impact could not be found
	*/
	public CareerImpact fetchByUUID_G(java.lang.String uuid, long groupId,
		boolean retrieveFromCache);

	/**
	* Removes the career impact where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the career impact that was removed
	*/
	public CareerImpact removeByUUID_G(java.lang.String uuid, long groupId)
		throws NoSuchCareerImpactException;

	/**
	* Returns the number of career impacts where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching career impacts
	*/
	public int countByUUID_G(java.lang.String uuid, long groupId);

	/**
	* Returns all the career impacts where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching career impacts
	*/
	public java.util.List<CareerImpact> findByUuid_C(java.lang.String uuid,
		long companyId);

	/**
	* Returns a range of all the career impacts where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of career impacts
	* @param end the upper bound of the range of career impacts (not inclusive)
	* @return the range of matching career impacts
	*/
	public java.util.List<CareerImpact> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end);

	/**
	* Returns an ordered range of all the career impacts where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of career impacts
	* @param end the upper bound of the range of career impacts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching career impacts
	*/
	public java.util.List<CareerImpact> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CareerImpact> orderByComparator);

	/**
	* Returns an ordered range of all the career impacts where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of career impacts
	* @param end the upper bound of the range of career impacts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching career impacts
	*/
	public java.util.List<CareerImpact> findByUuid_C(java.lang.String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CareerImpact> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first career impact in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching career impact
	* @throws NoSuchCareerImpactException if a matching career impact could not be found
	*/
	public CareerImpact findByUuid_C_First(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<CareerImpact> orderByComparator)
		throws NoSuchCareerImpactException;

	/**
	* Returns the first career impact in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching career impact, or <code>null</code> if a matching career impact could not be found
	*/
	public CareerImpact fetchByUuid_C_First(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<CareerImpact> orderByComparator);

	/**
	* Returns the last career impact in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching career impact
	* @throws NoSuchCareerImpactException if a matching career impact could not be found
	*/
	public CareerImpact findByUuid_C_Last(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<CareerImpact> orderByComparator)
		throws NoSuchCareerImpactException;

	/**
	* Returns the last career impact in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching career impact, or <code>null</code> if a matching career impact could not be found
	*/
	public CareerImpact fetchByUuid_C_Last(java.lang.String uuid,
		long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<CareerImpact> orderByComparator);

	/**
	* Returns the career impacts before and after the current career impact in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param careerImpactId the primary key of the current career impact
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next career impact
	* @throws NoSuchCareerImpactException if a career impact with the primary key could not be found
	*/
	public CareerImpact[] findByUuid_C_PrevAndNext(long careerImpactId,
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<CareerImpact> orderByComparator)
		throws NoSuchCareerImpactException;

	/**
	* Removes all the career impacts where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public void removeByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the number of career impacts where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching career impacts
	*/
	public int countByUuid_C(java.lang.String uuid, long companyId);

	/**
	* Returns the career impact where careerImpactId = &#63; or throws a {@link NoSuchCareerImpactException} if it could not be found.
	*
	* @param careerImpactId the career impact ID
	* @return the matching career impact
	* @throws NoSuchCareerImpactException if a matching career impact could not be found
	*/
	public CareerImpact findByCareerImpactId(long careerImpactId)
		throws NoSuchCareerImpactException;

	/**
	* Returns the career impact where careerImpactId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param careerImpactId the career impact ID
	* @return the matching career impact, or <code>null</code> if a matching career impact could not be found
	*/
	public CareerImpact fetchByCareerImpactId(long careerImpactId);

	/**
	* Returns the career impact where careerImpactId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param careerImpactId the career impact ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching career impact, or <code>null</code> if a matching career impact could not be found
	*/
	public CareerImpact fetchByCareerImpactId(long careerImpactId,
		boolean retrieveFromCache);

	/**
	* Removes the career impact where careerImpactId = &#63; from the database.
	*
	* @param careerImpactId the career impact ID
	* @return the career impact that was removed
	*/
	public CareerImpact removeByCareerImpactId(long careerImpactId)
		throws NoSuchCareerImpactException;

	/**
	* Returns the number of career impacts where careerImpactId = &#63;.
	*
	* @param careerImpactId the career impact ID
	* @return the number of matching career impacts
	*/
	public int countByCareerImpactId(long careerImpactId);

	/**
	* Returns all the career impacts where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the matching career impacts
	*/
	public java.util.List<CareerImpact> findByGroupId(long groupId);

	/**
	* Returns a range of all the career impacts where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of career impacts
	* @param end the upper bound of the range of career impacts (not inclusive)
	* @return the range of matching career impacts
	*/
	public java.util.List<CareerImpact> findByGroupId(long groupId, int start,
		int end);

	/**
	* Returns an ordered range of all the career impacts where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of career impacts
	* @param end the upper bound of the range of career impacts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching career impacts
	*/
	public java.util.List<CareerImpact> findByGroupId(long groupId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<CareerImpact> orderByComparator);

	/**
	* Returns an ordered range of all the career impacts where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of career impacts
	* @param end the upper bound of the range of career impacts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching career impacts
	*/
	public java.util.List<CareerImpact> findByGroupId(long groupId, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<CareerImpact> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first career impact in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching career impact
	* @throws NoSuchCareerImpactException if a matching career impact could not be found
	*/
	public CareerImpact findByGroupId_First(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<CareerImpact> orderByComparator)
		throws NoSuchCareerImpactException;

	/**
	* Returns the first career impact in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching career impact, or <code>null</code> if a matching career impact could not be found
	*/
	public CareerImpact fetchByGroupId_First(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<CareerImpact> orderByComparator);

	/**
	* Returns the last career impact in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching career impact
	* @throws NoSuchCareerImpactException if a matching career impact could not be found
	*/
	public CareerImpact findByGroupId_Last(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<CareerImpact> orderByComparator)
		throws NoSuchCareerImpactException;

	/**
	* Returns the last career impact in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching career impact, or <code>null</code> if a matching career impact could not be found
	*/
	public CareerImpact fetchByGroupId_Last(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<CareerImpact> orderByComparator);

	/**
	* Returns the career impacts before and after the current career impact in the ordered set where groupId = &#63;.
	*
	* @param careerImpactId the primary key of the current career impact
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next career impact
	* @throws NoSuchCareerImpactException if a career impact with the primary key could not be found
	*/
	public CareerImpact[] findByGroupId_PrevAndNext(long careerImpactId,
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator<CareerImpact> orderByComparator)
		throws NoSuchCareerImpactException;

	/**
	* Removes all the career impacts where groupId = &#63; from the database.
	*
	* @param groupId the group ID
	*/
	public void removeByGroupId(long groupId);

	/**
	* Returns the number of career impacts where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the number of matching career impacts
	*/
	public int countByGroupId(long groupId);

	/**
	* Caches the career impact in the entity cache if it is enabled.
	*
	* @param careerImpact the career impact
	*/
	public void cacheResult(CareerImpact careerImpact);

	/**
	* Caches the career impacts in the entity cache if it is enabled.
	*
	* @param careerImpacts the career impacts
	*/
	public void cacheResult(java.util.List<CareerImpact> careerImpacts);

	/**
	* Creates a new career impact with the primary key. Does not add the career impact to the database.
	*
	* @param careerImpactId the primary key for the new career impact
	* @return the new career impact
	*/
	public CareerImpact create(long careerImpactId);

	/**
	* Removes the career impact with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param careerImpactId the primary key of the career impact
	* @return the career impact that was removed
	* @throws NoSuchCareerImpactException if a career impact with the primary key could not be found
	*/
	public CareerImpact remove(long careerImpactId)
		throws NoSuchCareerImpactException;

	public CareerImpact updateImpl(CareerImpact careerImpact);

	/**
	* Returns the career impact with the primary key or throws a {@link NoSuchCareerImpactException} if it could not be found.
	*
	* @param careerImpactId the primary key of the career impact
	* @return the career impact
	* @throws NoSuchCareerImpactException if a career impact with the primary key could not be found
	*/
	public CareerImpact findByPrimaryKey(long careerImpactId)
		throws NoSuchCareerImpactException;

	/**
	* Returns the career impact with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param careerImpactId the primary key of the career impact
	* @return the career impact, or <code>null</code> if a career impact with the primary key could not be found
	*/
	public CareerImpact fetchByPrimaryKey(long careerImpactId);

	@Override
	public java.util.Map<java.io.Serializable, CareerImpact> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the career impacts.
	*
	* @return the career impacts
	*/
	public java.util.List<CareerImpact> findAll();

	/**
	* Returns a range of all the career impacts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of career impacts
	* @param end the upper bound of the range of career impacts (not inclusive)
	* @return the range of career impacts
	*/
	public java.util.List<CareerImpact> findAll(int start, int end);

	/**
	* Returns an ordered range of all the career impacts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of career impacts
	* @param end the upper bound of the range of career impacts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of career impacts
	*/
	public java.util.List<CareerImpact> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CareerImpact> orderByComparator);

	/**
	* Returns an ordered range of all the career impacts.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CareerImpactModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of career impacts
	* @param end the upper bound of the range of career impacts (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of career impacts
	*/
	public java.util.List<CareerImpact> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<CareerImpact> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the career impacts from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of career impacts.
	*
	* @return the number of career impacts
	*/
	public int countAll();

	@Override
	public java.util.Set<java.lang.String> getBadColumnNames();
}