<liferay-ui:search-container delta="<%= delta %>" emptyResultsMessage="empty" iteratorURL="<%=iteratorURL%>">
				
				<liferay-ui:search-container-results
					results="<%=  intellectualProperties %>"
				/>
				
				<liferay-ui:search-container-row
					className="com.collaboratelab.impact.model.IntellectualProperty"
					keyProperty="intellectualPropertyId"
					modelVar="prop"
				>
				
				<liferay-ui:search-container-column-text
					name="fileDate"
					value="<%= ImpactWebUtil.getDateByLocale(prop.getOneStFileDate(), locale2) %>"
				/>
				
				<liferay-ui:search-container-column-text
					name="asOfDate"
					value="<%= ImpactWebUtil.getDateByLocale(prop.getAsOfDate(),locale2) %>"
				/>
				
				<liferay-ui:search-container-column-text
					name="title"
					value="<%= prop.getTitle() %>"
				/>
				
				<%
					String  stage = "";
				
					for (ImpactWebPortletKeys.Stage s : ImpactWebPortletKeys.Stage.values()) {
						if(s.getId() ==  prop.getStage())
							stage = s.getName();
					}
				%>
				
				<liferay-ui:search-container-column-text
					name="stage"
					value="<%= stage %>"
				/>
				
				<%
					String  licenseStatus = "";
				
					for (ImpactWebPortletKeys.LicenseStatus ls : ImpactWebPortletKeys.LicenseStatus.values()) {
						if(ls.getId() ==  prop.getLicenseStatus())
							licenseStatus = ls.getName();
					}
				%>
				<liferay-ui:search-container-column-text
					name="licenseStatus"
					value="<%= licenseStatus %>"
				/>
				
			
				<%if(isAdmin) {%>
					<liferay-ui:search-container-column-jsp path="/META-INF/resources/searchcontainer/actions/intellectualproperty_action.jsp" align="center"/>
				<% } %>
				
			</liferay-ui:search-container-row>
		
			<liferay-ui:search-iterator />
		
</liferay-ui:search-container>