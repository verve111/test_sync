<%@ include file="/init.jsp" %>

<%
	Date asOfDate = null;
	long jobsId  = ParamUtil.getLong(request,"jobsId", 0L);
	
	Jobs jobs = null;
	boolean isJobsNotNull = false;
	if(jobsId != 0) {
		isJobsNotNull = true;
		try {
			jobs = JobsLocalServiceUtil.getJobs(jobsId);
		} catch (PortalException e) {
			isJobsNotNull = false;
			e.printStackTrace();
		}
		asOfDate = jobs.getAsOfDate();
		//System.out.println(asOfDate.toString());	
	}else {
		asOfDate = new Date();
	}
	
	Calendar asOfDateCal = Calendar.getInstance();
	asOfDateCal.setTime(asOfDate);
	
%>

<liferay-portlet:actionURL name="addRowJobs" var="addRowURL">
</liferay-portlet:actionURL>

<div class="container-fluid-1280">
			<aui:form method="post" name="fm">
				<input type="hidden" name="<portlet:namespace />jobsId" value="<%= jobsId %>" >
				<div class="row">
					<label class="control-label" for="asOfDate">
			            AS OF DATE
			        </label>			
					<liferay-ui:input-date name="asOfDate"
						dayValue="<%=asOfDateCal.get(Calendar.DAY_OF_MONTH)%>" dayParam="asOfDateDay"
						monthValue="<%=(asOfDateCal.get(Calendar.MONTH))%>" monthParam="asOfDateMonth"
						yearValue="<%=asOfDateCal.get(Calendar.YEAR)%>" yearParam="asOfDateYear" />
				</div>
				<div class="row">
					<aui:select name="sizeOfTeam" id="sizeOfTeamSelect"
						label="SIZE OF TEAM">
						<%
							for (ImpactWebPortletKeys.SizeOfTeam size : ImpactWebPortletKeys.SizeOfTeam.values()) {
						%>
								<aui:option selected="<%= (isJobsNotNull && jobs.getSizeOfTeam() == size.getId()) ? true : false %>"  value="<%=size.getId()%>"><%=size.getName()%></aui:option>
						<%
							}
						%>
					</aui:select>
				</div>
				<div class="row">
					<div class="col-md-5 col-xs-10" style="padding-left:0px;">						
						<aui:select multiple="true" name="primaryJob" id="primaryJobSelect"
							label="PRIMARY JOB SETTING(S)">
							<%
								
								for (ImpactWebPortletKeys.PrimaryJob prjob : ImpactWebPortletKeys.PrimaryJob.values()) {
									
									
									boolean isPrimaryId = false;
									
									if(Validator.isNotNull(jobs)) {
										if(jobs.getPrimaryJob() != null && !jobs.getPrimaryJob().isEmpty())
										{
											String[] pr = jobs.getPrimaryJob().split(",");
											for(String p : pr) {
												if(prjob.getId() == Integer.parseInt(p))
													isPrimaryId = true;
											
											}
										}
									}
							%>
									<aui:option selected="<%= (isJobsNotNull && isPrimaryId) ? true : false %>"  value="<%=prjob.getId()%>"><%=prjob.getName()%></aui:option>
							<%
								}
							%>
						</aui:select>
					</div>
					<div class="col-md-7 col-xs-1 pull-left" style="padding-top: 20px;">
						<liferay-ui:icon message="To select multiple options: Ctrl + Mouse Click (Windows), or Cmd + Mouse Click (Mac)" iconCssClass="icon-question-sign"  />
					</div>
				</div>
				<div class="row">						
					<aui:input label="COMMENTS" name="comments"  value="<%= (isJobsNotNull) ? jobs.getComments() : "" %>" />
					<br />

					<div class="row">
						<aui:button type="submit" id="addRow"
							class="aui-button aui-button-primary right" value="SAVE & BACK" />
					</div>
				</div>
						 
			</aui:form>
	</div>
	
	
<aui:script>
	AUI().use('aui-node',
		function(A) {

			var $ = AUI.$;
			var form = $(document.<portlet:namespace />fm);

			var addRowURL = '<%=  addRowURL.toString() %>';
			var addRowButton = A.one('#<portlet:namespace/>addRow');

			addRowButton.on('click',function(event) {
				document.<portlet:namespace />fm.action = addRowURL;
			});
	});
</aui:script>
