<%@ include file="/init.jsp"%>

<%
	ResultRow resultRow = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	Jobs job = (Jobs) resultRow.getObject();
%>

<portlet:renderURL var="editURL">
	<portlet:param name="jobsId"
		value="<%=String.valueOf(job.getJobsId())%>" />
	<portlet:param name="mvcPath"
		value="/META-INF/resources/form/jobsform.jsp" />
</portlet:renderURL>
<portlet:actionURL var="deleteURL" name="deleteRowJobs">
	<portlet:param name="jobsId"
		value="<%=String.valueOf(job.getJobsId())%>" />
</portlet:actionURL>

<liferay-ui:icon label="false" message="Edit" iconCssClass="icon-edit"
	url="<%=editURL.toString()%>" />
<liferay-ui:icon label="false" message="Delete"
	iconCssClass="icon-trash" url="<%=deleteURL.toString()%>" />