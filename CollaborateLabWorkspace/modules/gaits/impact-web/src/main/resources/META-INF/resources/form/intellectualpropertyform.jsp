<%@ include file="/init.jsp" %>

<%
	Date fileDate = null;
	Date asOfDate = null;
	long intellectualId  = ParamUtil.getLong(request,"intellectualPropertyId", 0L);
	
	IntellectualProperty intellectual = null;
	boolean isIntellectualNotNull = false;
	if(intellectualId != 0) {
		isIntellectualNotNull = true;
		try {
			intellectual = IntellectualPropertyLocalServiceUtil.getIntellectualProperty(intellectualId);
		} catch (PortalException e) {
			isIntellectualNotNull = false;
			e.printStackTrace();
		}
		fileDate = intellectual.getOneStFileDate();
		asOfDate = intellectual.getAsOfDate();
		
	}else {
		fileDate = new Date();
		asOfDate = new Date();
	}
	
	Calendar fileDateCal = Calendar.getInstance();
	fileDateCal.setTime(fileDate);
	
	Calendar asOfDateCal = Calendar.getInstance();
	asOfDateCal.setTime(asOfDate);
%>

<liferay-portlet:actionURL name="addRowIntellectualProperty" var="addRowURL">
</liferay-portlet:actionURL>

<div class="container-fluid-1280">
		<div class="row">
			<aui:form method="post" name="fm">
						<input type="hidden" name="<portlet:namespace />intellectualPropertyId" value="<%= intellectualId %>" >
						
						<aui:fieldset>
							<aui:field-wrapper label="1ST FILE DATE">
							</aui:field-wrapper>
						</aui:fieldset>
						
						<liferay-ui:input-date name="fileDate"
							dayValue="<%=fileDateCal.get(Calendar.DAY_OF_MONTH)%>" dayParam="fileDateDay"
							monthValue="<%=fileDateCal.get(Calendar.MONTH)%>" monthParam="fileDateMonth"
							yearValue="<%=fileDateCal.get(Calendar.YEAR)%>" yearParam="fileDateYear" />
						
						<aui:fieldset>
							<aui:field-wrapper label="AS OF DATE">
							</aui:field-wrapper>
						</aui:fieldset>
						
						<liferay-ui:input-date name="asOfDate"
							dayValue="<%=asOfDateCal.get(Calendar.DAY_OF_MONTH)%>" dayParam="asOfDateDay"
							monthValue="<%=(asOfDateCal.get(Calendar.MONTH))%>" monthParam="asOfDateMonth"
							yearValue="<%=asOfDateCal.get(Calendar.YEAR)%>" yearParam="asOfDateYear" />
						
						<aui:input label="TITLE" name="title" value="<%= (isIntellectualNotNull) ? intellectual.getTitle() : "" %>" />
						
						<aui:select name="stage" id="stageSelect"
							label="STAGE">
							<%
								for (ImpactWebPortletKeys.Stage stage : ImpactWebPortletKeys.Stage.values()) {
							%>
									<aui:option selected="<%= (isIntellectualNotNull && intellectual.getStage() == stage.getId()) ? true : false %>" value="<%=stage.getId()%>"><%=stage.getName()%></aui:option>
									
							<%
								}
							%>
						</aui:select>
						
						<aui:select name="licenseStatus" id="licenseStatusSelect"
							label="LICENSE STATUS">
							<%
								for (ImpactWebPortletKeys.LicenseStatus status : ImpactWebPortletKeys.LicenseStatus.values()) {
							%>
									<aui:option selected="<%= (isIntellectualNotNull && intellectual.getLicenseStatus() == status.getId()) ? true : false %>" value="<%=status.getId()%>"><%=status.getName()%></aui:option>
									
							<%
								}
							%>
						</aui:select>
					
						<br />
	
						<div class="row">
							<aui:button type="submit" id="addRow"
								class="aui-button aui-button-primary right" value="SAVE & BACK" />
						</div>
						
						 
			</aui:form>
		</div>
	</div>
	
	
<aui:script>
	AUI().use('aui-node',
		function(A) {

			var $ = AUI.$;
			var form = $(document.<portlet:namespace />fm);

			var addRowURL = '<%=  addRowURL.toString() %>';
			var addRowButton = A.one('#<portlet:namespace/>addRow');

			addRowButton.on('click',function(event) {
				document.<portlet:namespace />fm.action = addRowURL;
			});
	});
</aui:script>
