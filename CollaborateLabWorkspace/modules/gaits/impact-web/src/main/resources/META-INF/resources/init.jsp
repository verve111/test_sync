<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %><%@
taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %><%@
taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %><%@
taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>

<%@page import="com.liferay.portal.kernel.dao.search.SearchContainer"%>
<%@page import="com.collaboratelab.impact.model.CareerImpact"%>
<%@page import="com.collaboratelab.impact.model.Collaborators"%>
<%@page import="com.collaboratelab.impact.model.IntellectualProperty"%>
<%@page import="com.collaboratelab.impact.model.PatientsImpacted"%>
<%@page import="com.collaboratelab.impact.model.SitesUsingSolution"%>
<%@page import="com.collaboratelab.impact.model.TeamMemberProjectRecognition"%>
<%@page import="com.collaboratelab.impact.model.Publications"%>
<%@page import="com.collaboratelab.impact.model.Jobs"%>
<%@page import="com.collaboratelab.impact.model.Support"%>

<%@page import="com.collaboratelab.impact.service.CareerImpactLocalServiceUtil"%>
<%@page import="com.collaboratelab.impact.service.CollaboratorsLocalServiceUtil"%>
<%@page import="com.collaboratelab.impact.service.IntellectualPropertyLocalServiceUtil"%>
<%@page import="com.collaboratelab.impact.service.PatientsImpactedLocalServiceUtil"%>
<%@page import="com.collaboratelab.impact.service.SitesUsingSolutionLocalServiceUtil"%>
<%@page import="com.collaboratelab.impact.service.TeamMemberProjectRecognitionLocalServiceUtil"%>
<%@page import="com.collaboratelab.impact.service.PublicationsLocalServiceUtil"%>
<%@page import="com.collaboratelab.impact.service.SupportLocalServiceUtil"%>
<%@page import="com.collaboratelab.impact.service.JobsLocalServiceUtil"%>


<%@page import="com.collaboratelab.impactweb.constants.ImpactWebPortletKeys"%>
<%@page import="com.collaboratelab.impactweb.util.ImpactWebUtil"%>
<%@page import="java.util.Date"%>

<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="java.util.Locale"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayPortletMode"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.liferay.portal.kernel.util.DateUtil"%>
<%@page import="com.liferay.portal.kernel.util.LocaleUtil"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="com.liferay.portal.kernel.exception.PortalException"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>

<%@page import="com.liferay.portal.kernel.service.UserGroupRoleLocalServiceUtil" %>
<%@page import="com.liferay.portal.kernel.model.RoleConstants" %>
<%@page import="com.liferay.portal.kernel.service.RoleLocalServiceUtil" %>

<liferay-theme:defineObjects />

<portlet:defineObjects />

<%
	Locale locale2 = LocaleUtil.getSiteDefault();
	boolean isAdmin = UserGroupRoleLocalServiceUtil.hasUserGroupRole(themeDisplay.getUserId(), themeDisplay.getScopeGroupId(), RoleConstants.SITE_ADMINISTRATOR, true) || permissionChecker.isOmniadmin();
%>
<style>
	.icon-edit , .icon-trash{
		font-size:25px;
		color: #29353d !important;
		margin-right:5px;
		vertical-align:middle;
	}
	.icon-question-sign {
		font-size:25px;
	}

 
  
</style>