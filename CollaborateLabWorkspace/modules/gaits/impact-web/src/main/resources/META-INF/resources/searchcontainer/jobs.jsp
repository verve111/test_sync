<liferay-ui:search-container delta="<%= delta %>" emptyResultsMessage="empty" iteratorURL="<%=iteratorURL%>">
				
				<liferay-ui:search-container-results
					results="<%=  jobs %>"
				/>
				
				<liferay-ui:search-container-row
					className="com.collaboratelab.impact.model.Jobs"
					keyProperty="jobsId"
					modelVar="job"
				>

				<liferay-ui:search-container-column-text
					name="asOfDate"
					value="<%= ImpactWebUtil.getDateByLocale(job.getAsOfDate(),locale2) %>"
				/>
				
				<%
					String  sizeOfTeam = "";
				
					for (ImpactWebPortletKeys.SizeOfTeam s : ImpactWebPortletKeys.SizeOfTeam.values()) {
						if(s.getId() ==  job.getSizeOfTeam())
							sizeOfTeam = s.getName();
					}
				%>
				
				<liferay-ui:search-container-column-text
					name="sizeOfTeam"
					value="<%= sizeOfTeam %>"
				/>
				
				<%
					StringBuilder primaryJobBuilder = new StringBuilder("");
					String primaryJob = "";
					if (job.getPrimaryJob() != null && !job.getPrimaryJob().isEmpty()) {
						String[] prId = job.getPrimaryJob().split(",");
						
						for (ImpactWebPortletKeys.PrimaryJob prjob : ImpactWebPortletKeys.PrimaryJob.values()) {
							for(String id : prId)
								if(prjob.getId() == Integer.parseInt(id))
									primaryJobBuilder.append(prjob.getName() + ",");
						}
						primaryJob = primaryJobBuilder.toString();
						if(!primaryJob.equals(""))
							primaryJob = primaryJob.substring(0, primaryJob.length()-1);
					}
				%>
				
				
				<liferay-ui:search-container-column-text
					name="primaryJob"
					value="<%= primaryJob %>"
				/>
				
				
				<liferay-ui:search-container-column-text
					name="comments"
					value="<%= job.getComments() %>"
				/>

				<%if(isAdmin) {%>
					<liferay-ui:search-container-column-jsp path="/META-INF/resources/searchcontainer/actions/jobs_action.jsp" align="center"/>
				<% } %>
				
			</liferay-ui:search-container-row>
		
			<liferay-ui:search-iterator />
		
</liferay-ui:search-container>