<%@ include file="/init.jsp" %>

<%
	Date startDate = null;
	Date endDate = null;
	long collaboratorsId  = ParamUtil.getLong(request,"collaboratorsId", 0L);
	
	Collaborators colab = null;
	boolean isColabNotNull = false;
	if(collaboratorsId != 0) {
		isColabNotNull = true;
		try {
			colab = CollaboratorsLocalServiceUtil.getCollaborators(collaboratorsId);
		} catch (PortalException e) {
			isColabNotNull = false;
			e.printStackTrace();
		}
		startDate = colab.getStartDate();
		endDate = colab.getEndDate();
		
	}else {
		startDate = new Date();
		endDate = new Date();
	}
	
	Calendar startDateCal = Calendar.getInstance();
	startDateCal.setTime(startDate);
	
	Calendar endDateCal = Calendar.getInstance();
	if (endDate != null) {
		endDateCal.setTime(endDate);
	}
%>

<liferay-portlet:actionURL name="addRowCollaborators" var="addRowURL">
</liferay-portlet:actionURL>

<div class="container-fluid-1280">
		<div class="row">
			
			<aui:form method="post" name="fm">
						<input type="hidden" name="<portlet:namespace />collaboratorsId" value="<%= collaboratorsId %>" >
						<aui:fieldset>
							<aui:field-wrapper label="START DATE">
							</aui:field-wrapper>
						</aui:fieldset>
						
						<liferay-ui:input-date name="startDate"
							dayValue="<%=startDateCal.get(Calendar.DAY_OF_MONTH)%>" dayParam="startDateDay"
							monthValue="<%=(startDateCal.get(Calendar.MONTH))%>" monthParam="startDateMonth"
							yearValue="<%=startDateCal.get(Calendar.YEAR)%>" yearParam="startDateYear" />
						
						<aui:fieldset>
							<aui:field-wrapper label="END DATE">
							</aui:field-wrapper>
						</aui:fieldset>
						 
						<liferay-ui:input-date name="endDate" nullable="true"
							dayValue="<%=endDateCal.get(Calendar.DAY_OF_MONTH)%>" dayParam="endDateDay"
							monthValue="<%=endDateCal.get(Calendar.MONTH)%>" monthParam="endDateMonth"
							yearValue="<%=endDateCal.get(Calendar.YEAR)%>" yearParam="endDateYear" />
						
						<aui:input label="NAME" name="name" value="<%= (isColabNotNull) ? colab.getName() : "" %>" />
						
						<aui:input label="URL" name="url" value="<%= (isColabNotNull) ? colab.getURL() : "" %>" />
						 
						<aui:select name="type" id="typeSelect"
							label="TYPE">
							<%
								for (ImpactWebPortletKeys.Type type : ImpactWebPortletKeys.Type.values()) {
							%>
									<aui:option selected="<%= (isColabNotNull && colab.getType() == type.getId()) ? true : false %>" value="<%=type.getId()%>"><%=type.getName()%></aui:option>
							<%
								}
							%>
						</aui:select>
						
						<aui:input label="OPTIONAL COMMENTS" name="optionalComments" value="<%= (isColabNotNull) ? colab.getOptionalComments() : "" %>"  />
						<br />
	
						<div class="row">
							<aui:button type="submit" id="addRow"
								class="aui-button aui-button-primary right" value="SAVE & BACK" />
						</div>
			</aui:form>
		</div>
	</div>
	
	
<aui:script>
	AUI().use('aui-node',
		function(A) {

			var $ = AUI.$;
			var form = $(document.<portlet:namespace />fm);

			var addRowURL = '<%=  addRowURL.toString() %>';
			var addRowButton = A.one('#<portlet:namespace/>addRow');

			addRowButton.on('click',function(event) {
				document.<portlet:namespace />fm.action = addRowURL;
			});
	});
</aui:script>
