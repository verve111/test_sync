<liferay-ui:search-container delta="<%= delta %>" emptyResultsMessage="empty" iteratorURL="<%=iteratorURL%>">
				
				<liferay-ui:search-container-results
					results="<%= sitesUsingSolutions %>"
				/>
				
				<liferay-ui:search-container-row
					className="com.collaboratelab.impact.model.SitesUsingSolution"
					keyProperty="sitesUsingSolutionId"
					modelVar="site"
				>
				
				<liferay-ui:search-container-column-text
					name="asOfDate"
					value="<%= ImpactWebUtil.getDateByLocale(site.getAsOfDate(), locale2) %>"
				/>
				<%
					String  number = "";
				
					for (ImpactWebPortletKeys.Number num : ImpactWebPortletKeys.Number.values()) {
						if(num.getId() ==  site.getNumber())
							number = num.getName();
					}
				%>
				
				<liferay-ui:search-container-column-text
					name="number"
					value="<%= number %>"
				/>
				
				<liferay-ui:search-container-column-text
					name="optionalComments"
					value="<%= site.getOptionalComments() %>"
				/>
				
				<%if(isAdmin) {%>
					<liferay-ui:search-container-column-jsp path="/META-INF/resources/searchcontainer/actions/sitesusingsolution_action.jsp" align="center"/>
				<% } %>
				
			</liferay-ui:search-container-row>
		
			<liferay-ui:search-iterator />
		
</liferay-ui:search-container>