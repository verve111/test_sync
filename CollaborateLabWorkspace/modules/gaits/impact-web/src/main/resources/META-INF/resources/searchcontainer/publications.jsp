<liferay-ui:search-container delta="<%= delta %>" emptyResultsMessage="empty" iteratorURL="<%=iteratorURL%>">
				
				<liferay-ui:search-container-results
					results="<%=  publications %>"
				/>
				
				<liferay-ui:search-container-row
					className="com.collaboratelab.impact.model.Publications"
					keyProperty="publicationsId"
					modelVar="publication"
				>
				
				<liferay-ui:search-container-column-text
					name="date"
					value="<%= ImpactWebUtil.getDateByLocale(publication.getDate(), locale2)  %>"
				/>
				
				<liferay-ui:search-container-column-text
					name="titleOfPublications"
					value="<%= publication.getTitleOfPublications() %>"
				/>
				
				
				<liferay-ui:search-container-column-text
					name="url"
					value="<%= publication.getURL() %>"
				/>
			
			
				<liferay-ui:search-container-column-text
					name="comments"
					value="<%= publication.getComments() %>"
				/>
				
				<%if(isAdmin) {%>
					<liferay-ui:search-container-column-jsp path="/META-INF/resources/searchcontainer/actions/publications_action.jsp" align="center"/>
				<% } %>
				
			</liferay-ui:search-container-row>
		
			<liferay-ui:search-iterator />
		
</liferay-ui:search-container>