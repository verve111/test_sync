<%@ include file="/init.jsp" %>

<%
	Date startDate = null;
	int duration = 0;
	
	long supportId  = ParamUtil.getLong(request,"supportId", 0L);
	Support support = null;
	
	boolean isSupportNotNull = false;
	if(supportId != 0) {
		isSupportNotNull = true;
		try {
			support = SupportLocalServiceUtil.getSupport(supportId);
		} catch (PortalException e) {
			isSupportNotNull = false;
			e.printStackTrace();
		}
		startDate = support.getStartDate();
		duration = support.getDuration();
	} else {
		startDate = new Date();
	}
	
	Calendar startDateCal = Calendar.getInstance();
	startDateCal.setTime(startDate);

%>

<liferay-portlet:actionURL name="addRowSupport" var="addRowURL">
</liferay-portlet:actionURL>

<div class="container-fluid-1280">
		<div class="row">
			<aui:form method="post" name="fm">
						<input type="hidden" name="<portlet:namespace />supportId" value="<%= supportId %>" >
						
						<aui:fieldset>
							<aui:field-wrapper label="START DATE">
							</aui:field-wrapper>
						</aui:fieldset>
						
						<liferay-ui:input-date name="startDate"
							dayValue="<%=startDateCal.get(Calendar.DAY_OF_MONTH)%>" dayParam="startDateDay"
							monthValue="<%=startDateCal.get(Calendar.MONTH)%>" monthParam="startDateMonth"
							yearValue="<%=startDateCal.get(Calendar.YEAR)%>" yearParam="startDateYear" />
						
					
						<aui:input name="duration" value="<%= duration %>" label="DURATION (MONTHS)"/>
						
						
						<aui:input label="TOTAL VALUE" name="totalValue" value="<%= (isSupportNotNull) ? support.getTotalValue() : "" %>"   />
						
						<aui:select name="sponsorType" id="sponsorTypeSelect"
							label="SPONSOR TYPE">
							<%
								for (ImpactWebPortletKeys.SponsorType sponsor : ImpactWebPortletKeys.SponsorType.values()) {
							%>
									<aui:option selected="<%= (isSupportNotNull && support.getSponsorType() == sponsor.getId()) ? true : false %>" value="<%=sponsor.getId()%>"><%=sponsor.getName()%></aui:option>
							<%
								}
							%>
						</aui:select>
						
						<aui:select name="supportType" id="supportTypeSelect"
							label="SUPPORT TYPE">
							<%
								for (ImpactWebPortletKeys.SupportType support2 : ImpactWebPortletKeys.SupportType.values()) {
							%>
									<aui:option selected="<%= (isSupportNotNull && support.getSupportType() == support2.getId()) ? true : false %>" value="<%=support2.getId()%>"><%=support2.getName()%></aui:option>
							<%
								}
							%>
						</aui:select>
						
						<br />
	
						<div class="row">
							<aui:button type="submit" id="addRow"
								class="aui-button aui-button-primary right" value="SAVE & BACK" />
						</div>
						
						 
			</aui:form>
		</div>
	</div>
	
	
<aui:script>
	AUI().use('aui-node',
		function(A) {

			var $ = AUI.$;
			var form = $(document.<portlet:namespace />fm);

			var addRowURL = '<%=  addRowURL.toString() %>';
			var addRowButton = A.one('#<portlet:namespace/>addRow');

			addRowButton.on('click',function(event) {
				document.<portlet:namespace />fm.action = addRowURL;
			});
	});
</aui:script>
