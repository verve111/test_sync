<%@ include file="/init.jsp"%>

<%
	ResultRow resultRow = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	Collaborators collaborator = (Collaborators) resultRow.getObject();
%>

<portlet:renderURL var="editURL">
	<portlet:param name="collaboratorsId"
		value="<%=String.valueOf(collaborator.getCollaboratorId())%>" />
	<portlet:param name="mvcPath"
		value="/META-INF/resources/form/collaboratorsform.jsp" />
</portlet:renderURL>
<portlet:actionURL var="deleteURL" name="deleteRowCollaborators">
	<portlet:param name="collaboratorsId"
		value="<%=String.valueOf(collaborator.getCollaboratorId())%>" />
</portlet:actionURL>


<liferay-ui:icon label="false" message="Edit" iconCssClass="icon-edit"
	url="<%=editURL.toString()%>" />
<liferay-ui:icon label="false" message="Delete"
	iconCssClass="icon-trash" url="<%=deleteURL.toString()%>" />