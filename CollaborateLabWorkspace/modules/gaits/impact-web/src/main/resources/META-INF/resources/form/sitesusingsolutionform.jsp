<%@ include file="/init.jsp" %>

<%
	Date asOfDate = null;
	
	long sitesUsingSolutionId  = ParamUtil.getLong(request,"sitesUsingSolutionId", 0L);
	SitesUsingSolution sites = null;
	
	boolean isSitesUsingSolutionNotNull = false;
	if(sitesUsingSolutionId != 0) {
		isSitesUsingSolutionNotNull = true;
		try {
			sites = SitesUsingSolutionLocalServiceUtil.getSitesUsingSolution(sitesUsingSolutionId);
		} catch (PortalException e) {
			isSitesUsingSolutionNotNull = false;
			e.printStackTrace();
		}
		asOfDate = sites.getAsOfDate();
		
	}else {
		asOfDate = new Date();
	}
	
	Calendar asOfDateCal = Calendar.getInstance();
	asOfDateCal.setTime(asOfDate);
%>

<liferay-portlet:actionURL name="addRowSitesUsingSolution" var="addRowURL">
</liferay-portlet:actionURL>

<div class="container-fluid-1280">
		<div class="row">
			<aui:form method="post" name="fm">
						<input type="hidden" name="<portlet:namespace />sitesUsingSolutionId" value="<%= sitesUsingSolutionId %>" >
						
						<aui:fieldset>
							<aui:field-wrapper label="AS OF DATE">
							</aui:field-wrapper>
						</aui:fieldset>
						
						<liferay-ui:input-date name="asOfDate"
							dayValue="<%=asOfDateCal.get(Calendar.DAY_OF_MONTH)%>" dayParam="asOfDateDay"
							monthValue="<%=(asOfDateCal.get(Calendar.MONTH))%>" monthParam="asOfDateMonth"
							yearValue="<%=asOfDateCal.get(Calendar.YEAR)%>" yearParam="asOfDateYear" />
						
						<aui:select name="number" id="numberSelect"
							label="NUMBER">
							<%
								for (ImpactWebPortletKeys.Number number : ImpactWebPortletKeys.Number.values()) {
							%>
									<aui:option  selected="<%= (isSitesUsingSolutionNotNull && sites.getNumber() == number.getId()) ? true : false %>" value="<%=number.getId()%>"><%=number.getName()%></aui:option>
							<%
								}
							%>
						</aui:select>
						
						<aui:input label="OPTIONAL COMMENTS" name="optionalComments" value="<%= (isSitesUsingSolutionNotNull) ? sites.getOptionalComments() : "" %>"  />
						<br />
	
						<div class="row">
							<aui:button type="submit" id="addRow"
								class="aui-button aui-button-primary right" value="SAVE & BACK" />
						</div>
			</aui:form>
		</div>
	</div>
	
	
<aui:script>
	AUI().use('aui-node',
		function(A) {

			var $ = AUI.$;
			var form = $(document.<portlet:namespace />fm);

			var addRowURL = '<%=  addRowURL.toString() %>';
			var addRowButton = A.one('#<portlet:namespace/>addRow');

			addRowButton.on('click',function(event) {
				document.<portlet:namespace />fm.action = addRowURL;
			});
	});
</aui:script>
