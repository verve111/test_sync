<liferay-ui:search-container delta="<%= delta %>" emptyResultsMessage="empty" iteratorURL="<%=iteratorURL%>">
				
				<liferay-ui:search-container-results
					results="<%=  careerImpacts %>"
				/>
				
				<liferay-ui:search-container-row
					className="com.collaboratelab.impact.model.CareerImpact"
					keyProperty="careerImpactId"
					modelVar="careerImpact"
				>
				
				<liferay-ui:search-container-column-text
					name="asOfDate"
					value="<%= ImpactWebUtil.getDateByLocale(careerImpact.getAsOfDate(),locale2) %>"
				/>
				
				<%
					String  crat = "";
				
					for (ImpactWebPortletKeys.CategoryRating c : ImpactWebPortletKeys.CategoryRating.values()) {
						if(c.getId() ==  careerImpact.getCategoryRaing())
							crat = c.getName();
					}
				%>
				
				<liferay-ui:search-container-column-text
					name="categoryRating"
					value="<%= crat %>"
				/>
				
				<liferay-ui:search-container-column-text
					name="optionalComments"
					value="<%= careerImpact.getOptionalComments() %>"
				/>
				<%if(isAdmin) {%>
					<liferay-ui:search-container-column-jsp path="/META-INF/resources/searchcontainer/actions/careerimpacts_action.jsp" align="center"/>
				<% } %>
				
			</liferay-ui:search-container-row>
		
			<liferay-ui:search-iterator />
		
</liferay-ui:search-container>