<%@ include file="/init.jsp" %>

<%
	Date asOfDate = null;

	long patientsImpactedId  = ParamUtil.getLong(request,"patientsImpactedId", 0L);
	PatientsImpacted patientsImpacted = null;
	
	boolean isPatientsImpactedNotNull = false;
	if(patientsImpactedId != 0) {
		isPatientsImpactedNotNull = true;
		try {
			patientsImpacted = PatientsImpactedLocalServiceUtil.getPatientsImpacted(patientsImpactedId);
		} catch (PortalException e) {
			isPatientsImpactedNotNull = false;
			e.printStackTrace();
		}
		asOfDate = patientsImpacted.getAsOfDate();
		
	}else {
		asOfDate = new Date();
	}
	
	Calendar asOfDateCal = Calendar.getInstance();
	asOfDateCal.setTime(asOfDate);
%>

<liferay-portlet:actionURL name="addRowPatientImpacted" var="addRowURL">
</liferay-portlet:actionURL>

<div class="container-fluid-1280">
		<div class="row">
			<aui:form method="post" name="fm">
						<input type="hidden" name="<portlet:namespace />patientsImpactedId" value="<%= patientsImpactedId %>" >
						
						<aui:fieldset>
							<aui:field-wrapper label="AS OF DATE">
							</aui:field-wrapper>
						</aui:fieldset>
						
						<liferay-ui:input-date name="asOfDate"
							dayValue="<%=asOfDateCal.get(Calendar.DAY_OF_MONTH)%>" dayParam="asOfDateDay"
							monthValue="<%=(asOfDateCal.get(Calendar.MONTH))%>" monthParam="asOfDateMonth"
							yearValue="<%=asOfDateCal.get(Calendar.YEAR)%>" yearParam="asOfDateYear" />
						
						
						
						<aui:select name="rangeEstimate" id="rangeEstimateSelect"
							label="RANGE ESTIMATE">
							<%
								for (ImpactWebPortletKeys.RangeEstimate range : ImpactWebPortletKeys.RangeEstimate.values()) {
							%>
									<aui:option selected="<%= (isPatientsImpactedNotNull && patientsImpacted.getRangeEstimate() == range.getId()) ? true : false %>" value="<%=range.getId()%>"><%=range.getName()%></aui:option>
							<%
								}
							%>
						</aui:select>
						
						<aui:select name="population" id="populationSelect"
							label="POPULATION">
							<%
								for (ImpactWebPortletKeys.Population population : ImpactWebPortletKeys.Population.values()) {
							%>
									<aui:option selected="<%= (isPatientsImpactedNotNull && patientsImpacted.getPopulation() == population.getId()) ? true : false %>" value="<%=population.getId()%>"><%=population.getName()%></aui:option>
							<%
								}
							%>
						</aui:select>
						
						<aui:input label="OPTIONAL COMMENTS" name="optionalComments"  value="<%= (isPatientsImpactedNotNull) ? patientsImpacted.getOptionalComments() : "" %>" />
						<br />
	
						<div class="row">
							<aui:button type="submit" id="addRow"
								class="aui-button aui-button-primary right" value="SAVE & BACK" />
						</div>
						
						 
			</aui:form>
		</div>
	</div>
	
	
<aui:script>
	AUI().use('aui-node',
		function(A) {

			var $ = AUI.$;
			var form = $(document.<portlet:namespace />fm);

			var addRowURL = '<%=  addRowURL.toString() %>';
			var addRowButton = A.one('#<portlet:namespace/>addRow');

			addRowButton.on('click',function(event) {
				document.<portlet:namespace />fm.action = addRowURL;
			});
	});
</aui:script>
