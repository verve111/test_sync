<%@ include file="/init.jsp"%>

<%
	ResultRow resultRow = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	Publications publication = (Publications) resultRow.getObject();
%>

<portlet:renderURL var="editURL">
	<portlet:param name="publicationsId"
		value="<%=String.valueOf(publication.getPublicationsId())%>" />
	<portlet:param name="mvcPath"
		value="/META-INF/resources/form/publicationsform.jsp" />
</portlet:renderURL>
<portlet:actionURL var="deleteURL" name="deleteRowPublications">
	<portlet:param name="publicationsId"
		value="<%=String.valueOf(publication.getPublicationsId())%>" />
</portlet:actionURL>

<liferay-ui:icon label="false" message="Edit" iconCssClass="icon-edit"
	url="<%=editURL.toString()%>" />
<liferay-ui:icon label="false" message="Delete"
	iconCssClass="icon-trash" url="<%=deleteURL.toString()%>" />