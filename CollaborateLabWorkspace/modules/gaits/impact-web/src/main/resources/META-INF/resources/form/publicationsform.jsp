<%@ include file="/init.jsp" %>

<%
	Date date = null;

	long publicationsId  = ParamUtil.getLong(request,"publicationsId", 0L);
	Publications pub = null;
	
	boolean isPublicationsNotNull = false;
	if(publicationsId != 0) {
		isPublicationsNotNull = true;
		try {
			pub = PublicationsLocalServiceUtil.getPublications(publicationsId);
		} catch (PortalException e) {
			isPublicationsNotNull = false;
			e.printStackTrace();
		}
		date = pub.getDate();
		
	}else {
		date = new Date();
	}
	
	Calendar dateCal = Calendar.getInstance();
	dateCal.setTime(date);
%>

<liferay-portlet:actionURL name="addRowPublications" var="addRowURL">
</liferay-portlet:actionURL>

<div class="container-fluid-1280">
		<div class="row">
			<aui:form method="post" name="fm">
						<input type="hidden" name="<portlet:namespace />publicationsId" value="<%= publicationsId %>" >
						<aui:fieldset>
							<aui:field-wrapper label="DATE">
							</aui:field-wrapper>
						</aui:fieldset>
						
						<liferay-ui:input-date name="date"
							dayValue="<%=dateCal.get(Calendar.DAY_OF_MONTH)%>" dayParam="dateDay"
							monthValue="<%=dateCal.get(Calendar.MONTH)%>" monthParam="dateMonth"
							yearValue="<%=dateCal.get(Calendar.YEAR)%>" yearParam="dateYear" />
						
						<aui:input label="URL" name="url"  value="<%= (isPublicationsNotNull) ? pub.getURL() : "" %>" />
						
						<aui:input label="TITLE OF PUBLICATION" name="titleOfPublications"  value="<%= (isPublicationsNotNull) ? pub.getTitleOfPublications() : "" %>" />
						
						<aui:input label="COMMENTS" name="comments"  value="<%= (isPublicationsNotNull) ? pub.getComments() : "" %>"/>
						<br />
	
						<div class="row">
							<aui:button type="submit" id="addRow"
								class="aui-button aui-button-primary right" value="SAVE & BACK" />
						</div>
						
						 
			</aui:form>
		</div>
	</div>
	
	
<aui:script>
	AUI().use('aui-node',
		function(A) {

			var $ = AUI.$;
			var form = $(document.<portlet:namespace />fm);

			var addRowURL = '<%=  addRowURL.toString() %>';
			var addRowButton = A.one('#<portlet:namespace/>addRow');

			addRowButton.on('click',function(event) {
				document.<portlet:namespace />fm.action = addRowURL;
			});
	});
</aui:script>
