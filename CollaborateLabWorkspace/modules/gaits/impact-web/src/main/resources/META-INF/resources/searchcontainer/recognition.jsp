<liferay-ui:search-container delta="<%= delta %>" emptyResultsMessage="empty" iteratorURL="<%=iteratorURL%>">
				
				<liferay-ui:search-container-results
					results="<%=  recognitions %>"
				/>
				
				<liferay-ui:search-container-row
					className="com.collaboratelab.impact.model.TeamMemberProjectRecognition"
					keyProperty="teamMemberProjectRecognitionId"
					modelVar="recognition"
				>
				
				<liferay-ui:search-container-column-text
					name="asOfDate"
					value="<%= ImpactWebUtil.getDateByLocale(recognition.getDateOfRecognition(), locale2)  %>"
				/>
			
				<liferay-ui:search-container-column-text
					name="name"
					value="<%= recognition.getName() %>"
				/>
				
				<%
					String  catr = "";
				
					for (ImpactWebPortletKeys.TeamCatRating c : ImpactWebPortletKeys.TeamCatRating.values()) {
						if(c.getId() ==  recognition.getCatRating())
							catr = c.getName();
					}
				%>
				
				<liferay-ui:search-container-column-text
					name="catRating"
					value="<%= catr %>"
				/>
				<liferay-ui:search-container-column-text
					name="url"
					value="<%= recognition.getURL() %>"
				/>
				<liferay-ui:search-container-column-text
					name="optionalComments"
					value="<%= recognition.getOptionalComments() %>"
				/>
				
				<%if(isAdmin) {%>
					<liferay-ui:search-container-column-jsp path="/META-INF/resources/searchcontainer/actions/recognition_action.jsp" align="center"/>
				<% } %>
				
			</liferay-ui:search-container-row>
		
			<liferay-ui:search-iterator />
		
</liferay-ui:search-container>