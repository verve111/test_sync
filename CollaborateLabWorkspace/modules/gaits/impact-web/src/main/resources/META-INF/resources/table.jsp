<%@ include file="/init.jsp"%>


<%	
	long groupId = themeDisplay.getScopeGroupId();
	ImpactWebUtil impactwebutil = new ImpactWebUtil();
	int delta = 10;
	PortletURL iteratorURL = renderResponse.createRenderURL();

	String searchContainerName = request.getParameter("searchContainerName");
	
	List<PatientsImpacted> patientsImpacteds = null;
	List<SitesUsingSolution> sitesUsingSolutions = null;
	List<CareerImpact> careerImpacts = null;
	List<IntellectualProperty> intellectualProperties = null;
	List<Collaborators> collaborators = null;
	List<TeamMemberProjectRecognition> recognitions = null;
	List<Support> support = null;
	List<Jobs> jobs = null;
	List<Publications> publications = null;
	
	if(!Validator.isNull(searchContainerName)) {
		if(searchContainerName.equalsIgnoreCase(ImpactWebPortletKeys.Tables.PATIENTS_IMPACTED.getName())) {
			patientsImpacteds = (List<PatientsImpacted>)impactwebutil.getListForSearchContainer(searchContainerName,groupId);
		}
		if(searchContainerName.equalsIgnoreCase(ImpactWebPortletKeys.Tables.SITES_USING_SOLUTION.getName())) {
			sitesUsingSolutions = (List<SitesUsingSolution>)impactwebutil.getListForSearchContainer(searchContainerName,groupId);
		}
		if(searchContainerName.equalsIgnoreCase(ImpactWebPortletKeys.Tables.CAREER_IMPACTED.getName())) {
			careerImpacts = (List<CareerImpact>)impactwebutil.getListForSearchContainer(searchContainerName,groupId);
		}
		if(searchContainerName.equalsIgnoreCase(ImpactWebPortletKeys.Tables.INTELLECTUAL_PROPERTY.getName())) {
			intellectualProperties = (List<IntellectualProperty>)impactwebutil.getListForSearchContainer(searchContainerName,groupId);
		}
		if(searchContainerName.equalsIgnoreCase(ImpactWebPortletKeys.Tables.COLLABORATES.getName())) {
			collaborators = (List<Collaborators>)impactwebutil.getListForSearchContainer(searchContainerName,groupId);
		}
		if(searchContainerName.equalsIgnoreCase(ImpactWebPortletKeys.Tables.RECOGNITION.getName())) {
			recognitions = (List<TeamMemberProjectRecognition>)impactwebutil.getListForSearchContainer(searchContainerName,groupId);
		}
		if(searchContainerName.equalsIgnoreCase(ImpactWebPortletKeys.Tables.FUNDING.getName())) {
			support = (List<Support>)impactwebutil.getListForSearchContainer(searchContainerName,groupId);
		}
		if(searchContainerName.equalsIgnoreCase(ImpactWebPortletKeys.Tables.PUBLICATIONS.getName())) {
			publications = (List<Publications>)impactwebutil.getListForSearchContainer(searchContainerName,groupId);
		}
		if(searchContainerName.equalsIgnoreCase(ImpactWebPortletKeys.Tables.JOBS_CREATED.getName())) {
			jobs = (List<Jobs>)impactwebutil.getListForSearchContainer(searchContainerName,groupId);
		}
	}
%>


<div class="container">
	<div class="row">
		<div class="col-md-12">
			<% 
			if(!Validator.isNull(searchContainerName)) { %>
			
				<% if(searchContainerName.equalsIgnoreCase(ImpactWebPortletKeys.Tables.PATIENTS_IMPACTED.getName())) {
				%>
						<%@include file="searchcontainer/patientsimpacted.jsp"%>
				<% 
					}
				%>
				
				<% if(searchContainerName.equalsIgnoreCase(ImpactWebPortletKeys.Tables.SITES_USING_SOLUTION.getName())) {
				%>
						<%@include file="searchcontainer/sitesusingsolution.jsp"%>
				<% 
					}
				%>
				
				<% if(searchContainerName.equalsIgnoreCase(ImpactWebPortletKeys.Tables.CAREER_IMPACTED.getName())) {
				%>
						<%@include file="searchcontainer/careerimpacts.jsp"%>
				<% 
					}
				%>
				
				<% if(searchContainerName.equalsIgnoreCase(ImpactWebPortletKeys.Tables.INTELLECTUAL_PROPERTY.getName())) {
				%>
						<%@include file="searchcontainer/intellectualproperty.jsp"%>
				<% 
					}
				%>
				<% if(searchContainerName.equalsIgnoreCase(ImpactWebPortletKeys.Tables.COLLABORATES.getName())) {
				%>
						<%@include file="searchcontainer/collaborators.jsp"%>
				<%  
					}
				%>
					<% if(searchContainerName.equalsIgnoreCase(ImpactWebPortletKeys.Tables.RECOGNITION.getName())) {
				%>
						<%@include file="searchcontainer/recognition.jsp"%>
				<% 
					}
				%>
				
				<% if(searchContainerName.equalsIgnoreCase(ImpactWebPortletKeys.Tables.JOBS_CREATED.getName())) {
				%>
						<%@include file="searchcontainer/jobs.jsp"%>
				<% 
					}
				%>
				
				<% if(searchContainerName.equalsIgnoreCase(ImpactWebPortletKeys.Tables.FUNDING.getName())) {
				%>
						<%@include file="searchcontainer/support.jsp"%>
				<% 
					}
				%>
				
				<% if(searchContainerName.equalsIgnoreCase(ImpactWebPortletKeys.Tables.PUBLICATIONS.getName())) {
				%>
						<%@include file="searchcontainer/publications.jsp"%>
				<% 
					}
				%>
			<% 
			}
			%>
			
		
			
		</div>
		<button id="goToTable" onclick="goToTable()" class="submit aui-button aui-button-primary"  >ADD ENTRY</button>
	</div>
</div>

<aui:script>
	
function goToTable() {
		AUI().use('liferay-portlet-url', function(A) {
						var $ = AUI.$;
						var renderUrl = Liferay.PortletURL.createRenderURL();
						renderUrl.setWindowState("<%=LiferayWindowState.POP_UP.toString() %>");
						renderUrl.setParameter("jspPage","/META-INF/resources/form/<%= ImpactWebPortletKeys.formMap.get(searchContainerName) %>.jsp");
						renderUrl.setPortletMode("<%=LiferayPortletMode.VIEW %>");
						renderUrl.setPortletId("<%= themeDisplay.getPortletDisplay().getId() %>");
						window.location.href = renderUrl.toString();
		});
}

</aui:script>
