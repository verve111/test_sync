<liferay-ui:search-container delta="<%= delta %>" emptyResultsMessage="empty" iteratorURL="<%=iteratorURL%>">
				
				<liferay-ui:search-container-results
					results="<%=  collaborators %>"
				/>
				
				<liferay-ui:search-container-row
					className="com.collaboratelab.impact.model.Collaborators"
					keyProperty="collaboratorId"
					modelVar="collaborator"
				>
				
				<liferay-ui:search-container-column-text
					name="startDate"
					value="<%=  ImpactWebUtil.getDateByLocale(collaborator.getStartDate(),locale2) %>"
				/>
				
				
				<liferay-ui:search-container-column-text
					name="endDate"
					value="<%= ImpactWebUtil.getDateByLocale(collaborator.getEndDate(),locale2) %>"
				/>
		
				<liferay-ui:search-container-column-text
					name="name"
					value="<%= collaborator.getName() %>"
				/>
				 
					<liferay-ui:search-container-column-text
					name="URL"
					value="<%= collaborator.getURL() %>"
				/>
				
				
				<%
					String  type = "";
				
					for (ImpactWebPortletKeys.Type t : ImpactWebPortletKeys.Type.values()) {
						if(t.getId() ==  collaborator.getType())
							type = t.getName();
					}
				%>
				
				<liferay-ui:search-container-column-text
					name="type"
					value="<%= type %>"
				/> 

				<liferay-ui:search-container-column-text
					name="optionalComments"
					value="<%= collaborator.getOptionalComments() %>"
				/>
				
				<%if(isAdmin) {%>
					<liferay-ui:search-container-column-jsp path="/META-INF/resources/searchcontainer/actions/collaborator_action.jsp" align="center"/>
				<% } %>	
								
			</liferay-ui:search-container-row>
		
			<liferay-ui:search-iterator />
		
</liferay-ui:search-container>