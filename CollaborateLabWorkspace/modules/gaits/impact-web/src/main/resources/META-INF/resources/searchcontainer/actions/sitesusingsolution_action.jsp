<%@ include file="/init.jsp"%>

<%
	ResultRow resultRow = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	SitesUsingSolution site = (SitesUsingSolution) resultRow.getObject();
%>

<portlet:renderURL var="editURL">
	<portlet:param name="sitesUsingSolutionId"
		value="<%=String.valueOf(site.getSitesUsingSolutionId())%>" />
	<portlet:param name="mvcPath"
		value="/META-INF/resources/form/sitesusingsolutionform.jsp" />
</portlet:renderURL>
<portlet:actionURL var="deleteURL" name="deleteRowSitesUsingSolution">
	<portlet:param name="sitesUsingSolutionId"
		value="<%=String.valueOf(site.getSitesUsingSolutionId())%>" />
</portlet:actionURL>

<liferay-ui:icon label="false" message="Edit" iconCssClass="icon-edit"
	url="<%=editURL.toString()%>" />
<liferay-ui:icon label="false" message="Delete"
	iconCssClass="icon-trash" url="<%=deleteURL.toString()%>" />