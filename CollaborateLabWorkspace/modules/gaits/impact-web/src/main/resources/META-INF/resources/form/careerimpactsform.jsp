<%@ include file="/init.jsp" %>

<%
	long careerImpactId  = ParamUtil.getLong(request,"careerImpactId", 0L);
	
	Date asOfDate = null;
	CareerImpact career = null;
	boolean isCareerNotNull = false;
	if(careerImpactId != 0) {
		isCareerNotNull = true;
		try {
			career = CareerImpactLocalServiceUtil.getCareerImpact(careerImpactId);
		} catch (PortalException e) {
			isCareerNotNull = false;
			e.printStackTrace();
		}
		asOfDate = career.getAsOfDate();
		
	}else {
		asOfDate = new Date();
	}
	
	Calendar asOfDateCal = Calendar.getInstance();
	asOfDateCal.setTime(asOfDate);
%>

<liferay-portlet:actionURL name="addRowCareerImpact" var="addRowURL">
</liferay-portlet:actionURL>

<div class="container-fluid-1280">
		<div class="row">
			<aui:form method="post" name="fm">
						<input type="hidden" name="<portlet:namespace />careerImpactId" value="<%= careerImpactId %>" >
						
						<aui:fieldset>
							<aui:field-wrapper label="AS OF DATE">
							</aui:field-wrapper>
						</aui:fieldset>
						
						<liferay-ui:input-date name="asOfDate"
							dayValue="<%=asOfDateCal.get(Calendar.DAY_OF_MONTH)%>" dayParam="asOfDateDay"
							monthValue="<%=(asOfDateCal.get(Calendar.MONTH))%>" monthParam="asOfDateMonth"
							yearValue="<%=asOfDateCal.get(Calendar.YEAR)%>" yearParam="asOfDateYear" />
						
						
						<aui:select name="categoryRating" id="categoryRatingSelect"
							label="CATEGORY RATING">
							<%
								for (ImpactWebPortletKeys.CategoryRating crat : ImpactWebPortletKeys.CategoryRating.values()) {
										
							%>
									<aui:option selected="<%= (isCareerNotNull && career.getCategoryRaing() == crat.getId()) ? true : false %>" value="<%=crat.getId()%>"><%=crat.getName()%></aui:option>
									
							<%
								}
							%>
						</aui:select>
						
						<aui:input label="OPTIONAL COMMENTS" name="optionalComments" value="<%= (isCareerNotNull) ? career.getOptionalComments() : "" %>" />
						
						<br />
						<div class="row">
							<aui:button type="submit" id="addRow"
								class="aui-button aui-button-primary right" value="SAVE & BACK" />
						</div>
			</aui:form>
		</div>
	</div>
	
	
<aui:script>
	AUI().use('aui-node',
		function(A) {

			var $ = AUI.$;
			var form = $(document.<portlet:namespace />fm);

			var addRowURL = '<%=  addRowURL.toString() %>';
			var addRowButton = A.one('#<portlet:namespace/>addRow');

			addRowButton.on('click',function(event) {
				document.<portlet:namespace />fm.action = addRowURL;
			});
	});
</aui:script>
