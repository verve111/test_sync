<%@ include file="/init.jsp"%>

<%
	ResultRow resultRow = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	PatientsImpacted patient = (PatientsImpacted) resultRow.getObject();
%>

<portlet:renderURL var="editURL">
	<portlet:param name="patientsImpactedId"
		value="<%=String.valueOf(patient.getPatientImpactedId())%>" />
	<portlet:param name="mvcPath"
		value="/META-INF/resources/form/patientsimpactedform.jsp" />
</portlet:renderURL>
<portlet:actionURL var="deleteURL" name="deleteRowPatientImpacted">
	<portlet:param name="patientsImpactedId"
		value="<%=String.valueOf(patient.getPatientImpactedId())%>" />
</portlet:actionURL>

<liferay-ui:icon label="false" message="Edit" iconCssClass="icon-edit"
	url="<%=editURL.toString()%>" />
<liferay-ui:icon label="false" message="Delete"
	iconCssClass="icon-trash" url="<%=deleteURL.toString()%>" />