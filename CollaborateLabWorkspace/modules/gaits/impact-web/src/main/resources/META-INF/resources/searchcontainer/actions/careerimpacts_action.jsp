<%@ include file="/init.jsp"%>

<%
	ResultRow resultRow = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	CareerImpact careerImpact = (CareerImpact) resultRow.getObject();
%>

<portlet:renderURL var="editURL">
	<portlet:param name="careerImpactId"
		value="<%=String.valueOf(careerImpact.getCareerImpactId())%>" />
	<portlet:param name="mvcPath"
		value="/META-INF/resources/form/careerimpactsform.jsp" />
</portlet:renderURL>

<portlet:actionURL var="deleteURL" name="deleteRowCareerImpact">
	<portlet:param name="careerImpactId"
		value="<%=String.valueOf(careerImpact.getCareerImpactId())%>" />
</portlet:actionURL>


<liferay-ui:icon label="false" message="Edit" iconCssClass="icon-edit"
	url="<%=editURL.toString()%>" />
<liferay-ui:icon label="false" message="Delete"
	iconCssClass="icon-trash" url="<%=deleteURL.toString()%>" />