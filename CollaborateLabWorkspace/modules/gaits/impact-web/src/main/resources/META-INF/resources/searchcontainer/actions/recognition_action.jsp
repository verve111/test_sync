<%@ include file="/init.jsp"%>

<%
	ResultRow resultRow = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	TeamMemberProjectRecognition recognition = (TeamMemberProjectRecognition) resultRow.getObject();
%>

<portlet:renderURL var="editURL">
	<portlet:param name="recognitionId"
		value="<%=String.valueOf(recognition.getTeamMemberProjectRecognitionId())%>" />
	<portlet:param name="mvcPath"
		value="/META-INF/resources/form/recognitionform.jsp" />
</portlet:renderURL>
<portlet:actionURL var="deleteURL" name="deleteRowTeamMemberProjectRecognition">
	<portlet:param name="recognitionId"
		value="<%=String.valueOf(recognition.getTeamMemberProjectRecognitionId())%>" />
</portlet:actionURL>

<liferay-ui:icon label="false" message="Edit" iconCssClass="icon-edit"
	url="<%=editURL.toString()%>" />
<liferay-ui:icon label="false" message="Delete"
	iconCssClass="icon-trash" url="<%=deleteURL.toString()%>" />