<%@ include file="/init.jsp" %>

<%
	Date dateOfRecog = null;

	long recognitionId  = ParamUtil.getLong(request,"recognitionId", 0L);
	TeamMemberProjectRecognition recog = null;
	
	boolean isRecognitionNotNull = false;
	if(recognitionId != 0) {
		isRecognitionNotNull = true;
		try {
			recog = TeamMemberProjectRecognitionLocalServiceUtil.getTeamMemberProjectRecognition(recognitionId);
		} catch (PortalException e) {
			isRecognitionNotNull = false;
			e.printStackTrace();
		}
		dateOfRecog = recog.getDateOfRecognition();
		
	}else {
		dateOfRecog = new Date();
	}
	
	Calendar dateOfRecogCal = Calendar.getInstance();
	dateOfRecogCal.setTime(dateOfRecog);
%>

<liferay-portlet:actionURL name="addRowTeamMemberProjectRecognition" var="addRowURL">
</liferay-portlet:actionURL>

<div class="container-fluid-1280">
		<div class="row">
			
			<aui:form method="post" name="fm">
						<input type="hidden" name="<portlet:namespace />recognitionId" value="<%= recognitionId %>" >
						
						<aui:fieldset>
							<aui:field-wrapper label="DATE OF RECOG.">
							</aui:field-wrapper>
						</aui:fieldset>
						
						<liferay-ui:input-date name="dateOfRecog"
							dayValue="<%=dateOfRecogCal.get(Calendar.DAY_OF_MONTH)%>" dayParam="dateOfRecogDay"
							monthValue="<%=dateOfRecogCal.get(Calendar.MONTH)%>" monthParam="dateOfRecogMonth"
							yearValue="<%=dateOfRecogCal.get(Calendar.YEAR)%>" yearParam="dateOfRecogYear" />
						
						<aui:input label="NAME / TITLE" name="name"  />
						
						<aui:select name="catRating" id="categoryRatingSelect"
							label="CATEGORY RATING">
							<%
								for (ImpactWebPortletKeys.TeamCatRating crat : ImpactWebPortletKeys.TeamCatRating.values()) {
							%>
									<aui:option selected="<%= (isRecognitionNotNull && recog.getCatRating() == crat.getId()) ? true : false %>"  value="<%=crat.getId()%>"><%=crat.getName()%></aui:option>
							<%
								}
							%>
						</aui:select>
						<aui:input label="URL" name="url" value="<%= (isRecognitionNotNull) ? recog.getURL() : "" %>" />
						<aui:input label="OPTIONAL COMMENTS" name="optionalComments" value="<%= (isRecognitionNotNull) ? recog.getOptionalComments() : "" %>"  />
						<br />
	
						<div class="row">
							<aui:button type="submit" id="addRow"
								class="aui-button aui-button-primary right" value="SAVE & BACK" />
						</div>
						
						 
			</aui:form>
		</div>
	</div>
	
	
<aui:script>
	AUI().use('aui-node',
		function(A) {

			var $ = AUI.$;
			var form = $(document.<portlet:namespace />fm);

			var addRowURL = '<%=  addRowURL.toString() %>';
			var addRowButton = A.one('#<portlet:namespace/>addRow');

			addRowButton.on('click',function(event) {
				document.<portlet:namespace />fm.action = addRowURL;
			});
	});
</aui:script>
