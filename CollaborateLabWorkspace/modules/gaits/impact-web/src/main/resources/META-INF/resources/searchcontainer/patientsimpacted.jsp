


<liferay-ui:search-container delta="4" emptyResultsMessage="empty" deltaConfigurable="true" iteratorURL="<%=iteratorURL%>">
				
				<liferay-ui:search-container-results
					results="<%=  patientsImpacteds %>"
				/>
				
				<liferay-ui:search-container-row
					className="com.collaboratelab.impact.model.PatientsImpacted"
					keyProperty="patientsImpactedId"
					modelVar="patient"
				>
				
				<liferay-ui:search-container-column-text
					name="asOfDate"
					value="<%= ImpactWebUtil.getDateByLocale(patient.getAsOfDate(),locale2)  %>"
				/>
				
				<%
					String  range = "";
				
					for (ImpactWebPortletKeys.RangeEstimate r : ImpactWebPortletKeys.RangeEstimate.values()) {
						if(r.getId() ==  patient.getRangeEstimate())
							range = r.getName();
					}
				%>
				
				<liferay-ui:search-container-column-text
					name="rangeEstimate"
					value="<%= range %>"
				/>
				
				<%
					String popul = "";
				
					for (ImpactWebPortletKeys.Population population : ImpactWebPortletKeys.Population.values()) {
						if(population.getId() ==  patient.getPopulation())
							 popul = population.getName();
					}
				%>
				
				<liferay-ui:search-container-column-text
					name="population"
					value="<%= popul %>"
				/>
				
				<liferay-ui:search-container-column-text
					name="optionalComments"
					value="<%= patient.getOptionalComments() %>"
				/>
				
				<%if(isAdmin) {%>
					<liferay-ui:search-container-column-jsp path="/META-INF/resources/searchcontainer/actions/patientsimpacted_action.jsp" align="center"/>
				<% } %>
				
			</liferay-ui:search-container-row>
		
			<liferay-ui:search-iterator />
		
</liferay-ui:search-container>