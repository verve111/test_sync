<%@ include file="/init.jsp" %>

<style>
	.grey_box {
		padding:3px 10px; background:#ccc; color:black; 
		min-height: 28px;
		cursor: pointer;
	}
	
	.tooltip {
		    position: relative;
	    z-index: 0;
	    display: block;
	    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
	    font-style: normal;
	    font-weight: normal;
	    letter-spacing: normal;
	    line-break: auto;
	    /* line-height: 1.42857; */
	    text-align: left;
	    text-align: start;
	    text-decoration: none;
	    text-shadow: none;
	    text-transform: none;
	    white-space: normal;
	    word-break: normal;
	    word-spacing: normal;
	    word-wrap: normal;
	    font-size: 18px;
	    opacity: 1;
	    float: right;
	
	}
</style>

<%
	Long groupId = themeDisplay.getLayout().getGroupId();
	List<PatientsImpacted> patients = PatientsImpactedLocalServiceUtil.findByGroupId(groupId);
	PatientsImpacted patient = null;
	String range = "n/a"; 
	if (patients != null && patients.size() > 0) {
		patient = patients.get(patients.size() - 1);
		for (ImpactWebPortletKeys.RangeEstimate r : ImpactWebPortletKeys.RangeEstimate.values()) {
			if (r.getId() ==  patient.getRangeEstimate())
				range = r.getName();
		}
	}
	
	List<SitesUsingSolution> sites = SitesUsingSolutionLocalServiceUtil.findByGroupId(groupId);
	SitesUsingSolution site = null;
	String number = "n/a";
	if(sites != null && sites.size() > 0) {
		site = sites.get(sites.size() - 1);
		for (ImpactWebPortletKeys.Number num : ImpactWebPortletKeys.Number.values()) {
			if(num.getId() ==  site.getNumber())
				number = num.getName();
		}
	}
	
	List<CareerImpact> careers = CareerImpactLocalServiceUtil.findByGroupId(groupId);
	CareerImpact career = null;
	ImpactWebPortletKeys.CategoryRating crat = null;
	if(careers != null && careers.size() > 0) {
		career = careers.get(careers.size()-1);
		for (ImpactWebPortletKeys.CategoryRating c : ImpactWebPortletKeys.CategoryRating.values()) {
			if(c.getId() ==  career.getCategoryRaing())
				crat = c;
		}
	}
	
	List<TeamMemberProjectRecognition> recognitions = TeamMemberProjectRecognitionLocalServiceUtil.findByGroupId(groupId);
	TeamMemberProjectRecognition recognition = null;
	
	ImpactWebPortletKeys.CategoryRating catr = null;
	if(recognitions != null && recognitions.size() > 0) {
		for (TeamMemberProjectRecognition r : recognitions) {
			if (recognition == null || r.getCatRating() > recognition.getCatRating()) {
				recognition = r;
			}
		}

		/*for (ImpactWebPortletKeys.TeamCatRating c : ImpactWebPortletKeys.TeamCatRating.values()) {
			if(c.getId() ==  recognition.getCatRating())
				catr = c;
		}*/
	}
	
	List<Collaborators> collaborators = CollaboratorsLocalServiceUtil.findByGroupId(groupId);
	Collaborators collaborator = null;
	String nameLink = "0";
	if (collaborators != null && collaborators.size() > 0) {
		nameLink = String.valueOf(collaborators.size());
	}	
	
	List<IntellectualProperty> intelprops = IntellectualPropertyLocalServiceUtil.findByGroupId(groupId);
	int intelpropsSize = 0;
	if( intelprops != null && intelprops.size() > 0) {
		intelpropsSize = intelprops.size();
	}	
	
	List<Jobs> jobs = JobsLocalServiceUtil.findByGroupId(groupId);
	String  sizeOfTeam = "0";
	if(jobs != null && jobs.size() > 0 ) {
		Jobs job = jobs.get(jobs.size()-1);
		for (ImpactWebPortletKeys.SizeOfTeam s : ImpactWebPortletKeys.SizeOfTeam.values()) {
			if(s.getId() ==  job.getSizeOfTeam())
				sizeOfTeam = s.getName();
		}
	}
	
	List<Support> supports = SupportLocalServiceUtil.findByGroupId(groupId);
	double  totalValue = 0;
	if (supports != null && supports.size() > 0) {
		for (Support support : supports) {
			totalValue += support.getTotalValue();
		}
	}
	
	List<Publications> publications = PublicationsLocalServiceUtil.findByGroupId(groupId);
	int  sizePub = 0;
	if(publications != null && publications.size() > 0 ) {
		sizePub = publications.size();
	}	
	
	String recogMessage = LanguageUtil.get(Locale.US, "recognition-alert");
	String patientMessage = LanguageUtil.get(Locale.US, "patient-alert");
	
	String sitesMessage = LanguageUtil.get(Locale.US, "sites-alert");
	String pubMessage = LanguageUtil.get(Locale.US, "publication-alert");
	
	String careerMessage = LanguageUtil.get(Locale.US, "career-alert");
	
	String colabMessage = LanguageUtil.get(Locale.US, "colaboration-alert");
	String propMessage = LanguageUtil.get(Locale.US, "prop-alert");
	
	String jobMessage = LanguageUtil.get(Locale.US, "job-alert");
	String supMessage = LanguageUtil.get(Locale.US, "support-alert");
	
	
%>

<div class="container-fluid-1280">
	<h3><%= ImpactWebPortletKeys.CatTables.CLINICAL.getName() %></h3>
	<div class="row" style="border:1px solid #ccc; padding:10px;">
		<div class="col-md-12">
			<div class="col-md-6 " id="popup_id">
				<div class="table_name" >1 <%= ImpactWebPortletKeys.Tables.PATIENTS_IMPACTED.getName() %></div>
			</div>
			<div class="col-md-3 grey_box" style="" onclick="onPopup('<%= ImpactWebPortletKeys.Tables.PATIENTS_IMPACTED.getName() %>')">
				<div class="range_enstimate"><%= range %></div>
			</div>
			<div class="col-md-3 right text-right">
				<a class="edit-href" href="javascript:onPopup('<%= ImpactWebPortletKeys.Tables.PATIENTS_IMPACTED.getName() %>')" >
					<liferay-ui:icon iconCssClass="icon-edit" message="Edit" />
		  		</a>
				<span  id="patient" >
					<i class="icon-info-sign"></i>
				</span>
			</div>
		</div>
		
		<br /><br />
		<div class="col-md-12">
			<div class="col-md-6 " id="popup_id">
				<div class="table_name" >2 <%= ImpactWebPortletKeys.Tables.SITES_USING_SOLUTION.getName()  %></div>
			</div>
			<div class="col-md-3 grey_box" style="" onclick="onPopup('<%= ImpactWebPortletKeys.Tables.SITES_USING_SOLUTION.getName() %>')">
				<div class="range_enstimate"><%= number %></div>
			</div>
			<div class="col-md-3 right text-right">
				<a class="edit-href" href="javascript:onPopup('<%= ImpactWebPortletKeys.Tables.SITES_USING_SOLUTION.getName() %>')" >
					<liferay-ui:icon iconCssClass="icon-edit" message="Edit" />
		  		</a>
				<span  id="sites" >
					<i class="icon-info-sign"></i>
				</span>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid-1280">
	<h3><%= ImpactWebPortletKeys.CatTables.ACADEMIC.getName() %></h3>
	<div class="row" style="border:1px solid #ccc; padding:10px;">
			<div class="col-md-12">
			<div class="col-md-6 " id="popup_id">
				<div class="table_name" >3 <%= ImpactWebPortletKeys.Tables.PUBLICATIONS.getName() %></div>
			</div>
			<div class="col-md-3 grey_box" style="" onclick="onPopup('<%= ImpactWebPortletKeys.Tables.PUBLICATIONS.getName() %>')">
				<div class="range_enstimate"><%= sizePub %></div>
			</div>
			<div class="col-md-3 right text-right">
				<a class="edit-href" href="javascript:onPopup('<%= ImpactWebPortletKeys.Tables.PUBLICATIONS.getName() %>')" >
					<liferay-ui:icon iconCssClass="icon-edit" message="Edit" />
		  		</a>
				<span  id="pub" >
					<i class="icon-info-sign"></i>
				</span>
			</div>
		</div>
		<br /><br />
	
		<div class="col-md-12">
			<div class="col-md-6 " id="popup_id">
				<div class="table_name" >4 <%= ImpactWebPortletKeys.Tables.CAREER_IMPACTED.getName() %></div>
			</div>
			<div class="col-md-3 grey_box" style="" onclick="onPopup('<%= ImpactWebPortletKeys.Tables.CAREER_IMPACTED.getName() %>')">
				<div class="range_enstimate">
					<c:choose>
						<c:when test="<%=crat == null %>">
							n/a
						</c:when>
						<c:otherwise>
							<c:forEach begin="1" end="<%=crat.getCompleteStars() %>">
							    <liferay-ui:icon iconCssClass="icon-star" />
							</c:forEach>
							<c:if test="<%=crat.isHalfStarAtTheEnd() %>">
								<liferay-ui:icon iconCssClass="icon-star-half" />
							</c:if>							
						</c:otherwise>
					</c:choose>
				</div>
			</div>
			<div class="col-md-3 right text-right">
				<a class="edit-href" href="javascript:onPopup('<%= ImpactWebPortletKeys.Tables.CAREER_IMPACTED.getName() %>')" >
					<liferay-ui:icon iconCssClass="icon-edit" message="Edit" />
		  		</a>
				<span  id="career" >
					<i class="icon-info-sign"></i>
				</span>
			</div>
		</div>
		
		<br /><br />
		<div class="col-md-12">
			<div class="col-md-6 " id="popup_id">
				<div class="table_name" >5 <%= ImpactWebPortletKeys.Tables.RECOGNITION.getName()  %></div>
			</div>
			<div class="col-md-3 grey_box" style="" onclick="onPopup('<%= ImpactWebPortletKeys.Tables.RECOGNITION.getName() %>')">
				<div class="range_enstimate">
					<c:choose>
						<c:when test="<%=recognition == null %>">
							n/a
						</c:when>
						<c:otherwise>
							<c:forEach begin="1" end="<%=recognition.getCatRating() + 1 %>">
							    <liferay-ui:icon iconCssClass="icon-star" />
							</c:forEach>
						</c:otherwise>
					</c:choose>				
				</div>
			</div>
			<div class="col-md-3 right text-right">
				<a class="edit-href" href="javascript:onPopup('<%= ImpactWebPortletKeys.Tables.RECOGNITION.getName() %>')" >
					<liferay-ui:icon iconCssClass="icon-edit" message="Edit" />
		  		</a>
				<span  id="recog" >
					<i class="icon-info-sign"></i>
				</span>
			</div>
		</div>
		<br /><br />
				
		<div class="col-md-12">
			<div class="col-md-6 " id="popup_id">
				<div class="table_name" >6 <%= ImpactWebPortletKeys.Tables.COLLABORATES.getName()  %></div>
			</div>
			<div class="col-md-3 grey_box" style="" onclick="onPopup('<%= ImpactWebPortletKeys.Tables.COLLABORATES.getName() %>')">
				<div class="range_enstimate"><%= nameLink %></div>
			</div>
			<div class="col-md-3 right text-right">
				<a class="edit-href" href="javascript:onPopup('<%= ImpactWebPortletKeys.Tables.COLLABORATES.getName() %>')" >
					<liferay-ui:icon iconCssClass="icon-edit" message="Edit" />
		  		</a>
				<span  id="colab" >
					<i class="icon-info-sign"></i>
				</span>
			</div>
		</div>
	</div>

</div>

<div class="container-fluid-1280">
	<h3><%= ImpactWebPortletKeys.CatTables.COMMERCIAL.getName() %></h3>
	<div class="row" style="border:1px solid #ccc; padding:10px;">
		<div class="col-md-12">
			<div class="col-md-6 " id="popup_id">
				<div class="table_name" >7 <%= ImpactWebPortletKeys.Tables.INTELLECTUAL_PROPERTY.getName() %></div>
			</div>
			<div class="col-md-3 grey_box" style="" onclick="onPopup('<%= ImpactWebPortletKeys.Tables.INTELLECTUAL_PROPERTY.getName() %>')">
				<div class="range_enstimate"><%= intelpropsSize %></div>
			</div>
			<div class="col-md-3 right text-right">
				<a class="edit-href" href="javascript:onPopup('<%= ImpactWebPortletKeys.Tables.INTELLECTUAL_PROPERTY.getName() %>')" >
					<liferay-ui:icon iconCssClass="icon-edit" message="Edit" />
		  		</a>
				<span  id="prop" >
					<i class="icon-info-sign"></i>
				</span>
			</div>
		</div>
		<br /><br />
		<div class="col-md-12">
			<div class="col-md-6 " id="popup_id">
				<div class="table_name" >8 <%= ImpactWebPortletKeys.Tables.FUNDING.getName() %></div>
			</div>
			<div class="col-md-3 grey_box" style="" onclick="onPopup('<%= ImpactWebPortletKeys.Tables.FUNDING.getName() %>')">
				<div class="range_enstimate">$<%= totalValue %></div>
			</div>
			<div class="col-md-3 right text-right">
				<a class="edit-href" href="javascript:onPopup('<%= ImpactWebPortletKeys.Tables.FUNDING.getName() %>')" >
					<liferay-ui:icon iconCssClass="icon-edit" message="Edit" />
		  		</a>
				<span  id="support" >
					<i class="icon-info-sign"></i>
				</span>	
			</div>
		</div>
		<br /><br />		
		<div class="col-md-12">
			<div class="col-md-6 " id="popup_id">
				<div class="table_name" >9 <%= ImpactWebPortletKeys.Tables.JOBS_CREATED.getName() %></div>
			</div>
			<div class="col-md-3 grey_box" style="" onclick="onPopup('<%= ImpactWebPortletKeys.Tables.JOBS_CREATED.getName() %>')">
				<div class="range_enstimate"><%= sizeOfTeam %></div>
			</div>
			<div class="col-md-3 right text-right">
				<a class="edit-href" href="javascript:onPopup('<%= ImpactWebPortletKeys.Tables.JOBS_CREATED.getName() %>')" >
					<liferay-ui:icon iconCssClass="icon-edit" message="Edit" />
		  		</a>
		  		<span  id="job" >
					<i class="icon-info-sign"></i>
				</span>
			</div>
		</div>
		
	</div>
</div>

<div class="tooltip_templates" style="display: none;">
    <div id="tooltip_patient">
        <!-- span>
		An estimate of the total cumulative number patients served by/with the solution at an "As of Date"". <br>Please select the range of patients impacted and primary populations from the pull-down options that best fits.</span--> 
		<span>Patients impacted:</span>
		<br>1 = &lt;10		
		<br>2 = 10 to 50
		<br>3 = 50 to 100
		<br>4 = 100 to 1K
		<br>5 = 1K to 10K 
		<br>6 = 10K to 100K 
		<br>7 = 100K to 200K
		<br>8 = 200K to 1M 
		<br>9 = 1M to 10M 
		<br>10 = &gt;10M
    </div>
    <div id="tooltip_career">
    	<!-- span>
        A self-rating at a given date as to the extent to which the project has had an impact on the careers of one or more team members.<br>
        Use the 0-9 rating from the pull-down options to reflect the progress at the time and add comments if you wish to help the team recognize the progress of its members.<br> 
    	</span-->        
    	<span>
		Career Impact:
		</span>
		<br>1 = No impact
		<br>2 = Some acknowledged impact on one person - enhanced career
		<br>3 = Moderate acknowledged impact on one person - accelerated career
		<br>4 = Substantial acknowledged impact on one person - changed career path
		<br>5 = Some acknowledged impact on several (&lt;=3) people - enhanced career 
		<br>6 = Moderate acknowledged impact on several (&lt;= 3) people - accelerated career 
		<br>7 = Substantial acknowledged impact on several (&lt;=3) - changed career path
		<br>8 = Some acknowledged impact on many (>=3) people - enhanced career
		<br>9 = Moderate acknowledged impact on many (>=3) people - accelerated career 
		<br>10 = Substantial acknowledged impact on many (>=3) people - changed career path
    </div>    
    <div id="tooltip_sites">
    	<span>
		Sites Using Solution:
		</span>
		<br>1 = 1
		<br>2 = 2
		<br>3 = 3 to 5
		<br>4 = 5 to 10 
		<br>5 = 10 to 20 
		<br>6 = 20 to 50
		<br>7 = 50 to 100 
		<br>8 = 100 to 200 
		<br>9 = 200 to 500
		<br>10 = &gt;500
    </div>     
    <div id="tooltip_fte">
    	<span>
		FTEs created rating:
		</span>
		<br>1 = 1
		<br>2 = 2 to 5
		<br>3 = 5 to 10 
		<br>4 = 10 to 20 
		<br>5 = 20 to 50
		<br>6 = 50 to 100 
		<br>7 = 100 to 200 
		<br>8 = 200 to 500
		<br>9 = 500 to 1,000		
		<br>10 = &gt;1,000
    </div>    
    <div id="tooltip_recog">
    	<span>
		Recognition:
		</span>
		<br>1 = Institutional award and/or recognition
		<br>2 = Regional award and/or recognition
		<br>3 = National award and/or recognition
		<br>4 = International award and/or recognition
		<br>5 = Highest award and/or recognition in field
    </div>       
</div>

<aui:script>
	function onPopup(name) {
		var conatainerName = name;
		AUI().use('aui-base',function(A) {
			var renderUrl = Liferay.PortletURL.createRenderURL();
			renderUrl.setWindowState("<%=LiferayWindowState.POP_UP.toString() %>");
			renderUrl.setParameter("jspPage","/table.jsp");
			renderUrl.setParameter("searchContainerName", name);
			renderUrl.setPortletMode("<%=LiferayPortletMode.VIEW %>");
			renderUrl.setPortletId("<%= themeDisplay.getPortletDisplay().getId() %>");
			
			Liferay.Util.openWindow({ dialog: { 
			 	centered: true,  
			 	width: 1330,
			 	height: 650,
			 	modal: true,
			 	constrain: true,
			 	destroyOnClose: true,
			 	destroyOnHide: true,
			 	on: {
			 		destroy: function() { 
							var impactPortlet = A.one('#p_p_id_ImpactWeb_');
							if (impactPortlet) { 
								Liferay.Portlet.refresh(impactPortlet);
							}
           
			 			}
			 		}
			}, 
				id: '<portlet:namespace />dialog',
				title: name, 
				uri: renderUrl 
			}); 
		});  
	}
	<% int hintWidth = 400; %>
	$(document).ready(function() {
		AUI().use('aui-base',function(A) {
			var recog= "<liferay-ui:message key='recognition-alert'/>";
			var pub= "<liferay-ui:message key='publication-alert'/>";
			var prop= "<liferay-ui:message key='prop-alert'/>";
			var colab= "<liferay-ui:message key='colaboration-alert'/>";
			var sup= "<liferay-ui:message key='support-alert'/>";
			$('#job').tooltipster({
    	    	content: $('#tooltip_fte'),
    	 		theme: 'tooltipster-borderless',
    	    	interactive: true,
    	    	maxWidth: <%=hintWidth %>
    		});
    		
    		$('#patient').tooltipster({
    	    	content: $('#tooltip_patient'),
    	 		theme: 'tooltipster-borderless',
    	 		maxWidth: <%=hintWidth %>,
    	    	interactive: true
    		});
    		$('#recog').tooltipster({
    	    	content:  $('#tooltip_recog'),
    	 		theme: 'tooltipster-borderless',
    	 		maxWidth: <%=hintWidth %>,
    	    	interactive: true
    		});
    		
    		$('#support').tooltipster({
    	    	content: sup,
    	 		theme: 'tooltipster-borderless',
    	 		maxWidth: <%=hintWidth %>,
    	    	interactive: true
    		});
    		$('#colab').tooltipster({
    	    	content: colab,
    	 		theme: 'tooltipster-borderless',
    	 		maxWidth: <%=hintWidth %>,
    	    	interactive: true
    		});
    		
    		$('#prop').tooltipster({
    	    	content: prop,
    	 		theme: 'tooltipster-borderless',
    	 		maxWidth: <%=hintWidth %>,
    	    	interactive: true
    		});
    		$('#career').tooltipster({
    	    	content: $('#tooltip_career'),
    	 		theme: 'tooltipster-borderless',
    	 		maxWidth: <%=hintWidth %>,
    	    	interactive: true
    		});
    		$('#sites').tooltipster({
    	    	content: $('#tooltip_sites'),
    	 		theme: 'tooltipster-borderless',
    	 		maxWidth: <%=hintWidth %>,
    	    	interactive: true
    		});
    		$('#pub').tooltipster({
    	    	content: pub,
    	 		theme: 'tooltipster-borderless',
    	 		maxWidth: <%=hintWidth %>,
    	    	interactive: true
    		});
		});
		
    });
</aui:script>

