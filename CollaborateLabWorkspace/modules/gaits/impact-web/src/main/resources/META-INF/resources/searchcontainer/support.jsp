<liferay-ui:search-container delta="<%= delta %>" emptyResultsMessage="empty" iteratorURL="<%=iteratorURL%>">
				
				<liferay-ui:search-container-results
					results="<%=  support %>"
				/>
				
				<liferay-ui:search-container-row
					className="com.collaboratelab.impact.model.Support"
					keyProperty="supportId"
					modelVar="sup"
				>
				
				<liferay-ui:search-container-column-text
					name="startDate"
					value="<%= ImpactWebUtil.getDateByLocale(sup.getStartDate(), locale2) %>"
				/>
				
				<liferay-ui:search-container-column-text
					name="duration"
					value="<%= String.valueOf(sup.getDuration())  %>"
				/>
				
				<liferay-ui:search-container-column-text
					name="totalValue"
					value="<%= "$" + String.valueOf(sup.getTotalValue()) %>"
					
				/>
				
				<%
					String  sponsorType = "";
				
					for (ImpactWebPortletKeys.SponsorType s : ImpactWebPortletKeys.SponsorType.values()) {
						if(s.getId() ==  sup.getSponsorType())
							sponsorType = s.getName();
					}
				%>
				
				<liferay-ui:search-container-column-text
					name="sponsorType"
					value="<%= sponsorType %>"
				/>
				
				<%
					String  supportType = "";
				
					for (ImpactWebPortletKeys.SupportType st : ImpactWebPortletKeys.SupportType.values()) {
						if(st.getId() ==  sup.getSupportType())
							supportType = st.getName();
					}
				%>
				
				<liferay-ui:search-container-column-text
					name="supportType"
					value="<%= supportType %>"
				/>
				
				<%if(isAdmin) {%>
					<liferay-ui:search-container-column-jsp path="/META-INF/resources/searchcontainer/actions/support_action.jsp" align="center"/>
				<% } %>
				
			</liferay-ui:search-container-row>
		
			<liferay-ui:search-iterator />
		
</liferay-ui:search-container>