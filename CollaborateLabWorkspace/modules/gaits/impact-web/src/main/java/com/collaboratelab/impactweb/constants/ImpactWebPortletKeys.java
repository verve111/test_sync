package com.collaboratelab.impactweb.constants;

import java.util.HashMap;

public class ImpactWebPortletKeys {

	public static final String ImpactWeb = "ImpactWeb";
	
	public static final String VIEW_TEMPLATE_TABLE_PATH= "/META-INF/resources/table.jsp";
	
	public static final HashMap<String, String> formMap = new HashMap<>();
	static {
		formMap.put(ImpactWebPortletKeys.Tables.PATIENTS_IMPACTED.getName(), "patientsimpactedform");
		formMap.put(ImpactWebPortletKeys.Tables.SITES_USING_SOLUTION.getName(), "sitesusingsolutionform");
		formMap.put(ImpactWebPortletKeys.Tables.CAREER_IMPACTED.getName(), "careerimpactsform");
		formMap.put(ImpactWebPortletKeys.Tables.RECOGNITION.getName(), "recognitionform");
		formMap.put(ImpactWebPortletKeys.Tables.COLLABORATES.getName(), "collaboratorsform");
		formMap.put(ImpactWebPortletKeys.Tables.INTELLECTUAL_PROPERTY.getName(), "intellectualpropertyform");
		formMap.put(ImpactWebPortletKeys.Tables.JOBS_CREATED.getName(), "jobsform");
		formMap.put(ImpactWebPortletKeys.Tables.PUBLICATIONS.getName(), "publicationsform");
		formMap.put(ImpactWebPortletKeys.Tables.FUNDING.getName(), "supportform");
	}
	
	/* Table CAREER IMPACT - column Category Rating */
	public enum CategoryRating{
		
		NO_IMPACT(0,"No impact"), 
		SOME_IMPACT(1,"Some acknowledged impact on one person - enhanced career"), 
		MODERATE_ACK(2,"Moderate acknowledged impact on one person - accelerated career"),
		SUBST_IMPACT(3,"Substantial acknowledged impact on one person - changed career path"),
		SOME_IMPACT_3(4,"Some acknowledged impact on several (<=3) people - enhanced career"), 
		MODERATE_ACK_3(5,"Moderate acknowledged impact on several (<= 3) people - accelerated career"),
		SUBST_IMPACT_3(6,"Substantial acknowledged impact on several (<=3) - changed career path"),		
		SOME_IMPACT_M_3(7,"Some acknowledged impact on many (>=3) people - enhanced career"), 
		MODERATE_ACK_M_3(8,"Moderate acknowledged impact on many (>=3) people - accelerated career"),
		SUBST_IMPACT_M_3(9,"Substantial acknowledged impact on many (>=3) people - changed career path");			
		
		CategoryRating(int id,String name) {
	        _name = name;
	        _id = id;
	        //_stars = stars;
	    }
		
	    private String _name;
	    //private String _stars;
	    private int _id;
	    
	    public String getName() {
	        return _name;
	    }
	    
	    public int getId() {
	        return _id;
	    }
	    
	    public int getCompleteStars() {
	        return (_id + 1) / 2;
	    }
	    
	    public boolean isHalfStarAtTheEnd() {
	    	return ((_id + 1) % 2) == 1;
	    }
	}
	
	/* Table PATIENTS IMPACTED - column range estimate */
	public enum RangeEstimate{
		ONE(0,"None"), 
		TWO(1,"<10"), 
		THREE(2,"10 to 50"),
		FOUR(3, "50 to 100"),
		FIVE(4,"100 to 1K"), 
		SIX(5,"1K to 10K"), 
		SEVEN(6,"10K to 100K"),
		EIGHT(7, "100K to 200K"),
		NINE(8,"200K to 1M"), 
		TEN(9,"1M to 10M"),
		ELEV(10, "> 10M");		
		
		RangeEstimate(int id,String name) {
	        _name = name;
	        _id = id;
	    }

	    private String _name;
	    private int _id;

	    public String getName() {
	        return _name;
	    }
	    
	    public int getId() {
	        return _id;
	    }
	}
	
	public enum PrimaryJob{
		ACADEMIA(0,"Academia/Hospital"), 
		COMMERCIAL(1,"Commercial");
		
		PrimaryJob(int id,String name) {
	        _name = name;
	        _id = id;
	    }

	    private String _name;
	    private int _id;

	    public String getName() {
	        return _name;
	    }
	
	    public int getId() {
	        return _id;
	    }
	}
	
	/* Table PATIENTS IMPACTED - column population */
	public enum Population{
		CLINICAL_TRIALS(0,"Clinical Trials"), 
		US(1, "US Civilians"),
		DOD(2,"DoD"), 
		EU(3,"EU"),
		ASIA(4, "Asia Pacific"),
		AFRICA(5, "Africa"),
		MID_EAST(6, "Middle East");
		
		Population(int id,String name) {
	        _name = name;
	        _id = id;
	    }

	    private String _name;
	    private int _id;

	    public String getName() {
	        return _name;
	    }
	    
	    public int getId() {
	        return _id;
	    }
	}
	
	/* Table SITES USING SOLUTION - column number */
	
	public enum Number{
		ZERO(0,"None"), 
		ONE(1,"1"), 
		TWO(2,"2"),
		THREE(3,"3 to 5"), 
		FOUR(4,"5 to 10"), 
		TENS_OF_THOUSANDS(5,"10 to 20"),
		MILLIONS(6,"20 to 50"), 
		TEN_MILLIONS(7,"50 to 100"), 
		E100_200(8,"100 to 200"), 
		HUNDRED_MILLIONS(9,"200 to 500"),
		MORE_THAN_500(10,">500");		
		
		Number(int id,String name) {
	        _name = name;
	        _id = id;
	    }

	    private String _name;
	    private int _id;

	    public String getName() {
	        return _name;
	    }
	    
	    public int getId() {
	        return _id;
	    }
	}
	
	/* Table COLLABORATORS - column type */
	
	public enum Type{
		NIH_LAB(0,"NIH Lab"), 
		DOD_LAB(1,"DoD Lab"), 
		OTHER_FED_LAB(2,"Other Fed/Country Lab"),
		INDEPENDENT_LAB(3,"Independent Lab"), 
		ACADEMIC_MEDICAL_CENTER(4,"Academic Medical Center"), 
		HOSPITAL(5,"Hospital/Care Provider"),
		UNIVERSITY(6,"University"), 
		CLINICAL_RESEARCH(7,"Clinical Research Organization"), 
		SERVICE_PROVIDER(8,"Service Provider(consultants, etc.)"),
		STARTUP(9,"Start-Up"),
		INDUSTRY(10,"Industry"),
		OTHER(11,"Other");
	
		 Type(int id,String name) {
	        _name = name;
	        _id = id;
	    }

	    private String _name;
	    private int _id;

	    public String getName() {
	        return _name;
	    }
	    
	    public int getId() {
	        return _id;
	    }
	}
	
	/* Table INTELLECTUAL PROPERTY - column stage */
	
	public enum Stage{
		NONE(0,"None"),
		INSTITUTIONAL(1,"Institutional Disclosure"), 
		PROVISIONAL_APPL(2,"Provisional Appl."),
		FULL_APPLICATION(3,"Full Application"), 		
		PCT(4,"PCT Application"), 
		ISSUED_US(5,"Issued US"), 
		ISR(6,"International Search Report (ISR)"),
		INTL_PATIENTS(7,"Int'l Patents"), 
		CONTINUATIONS(8,"Continuations, etc.");
	
		Stage(int id,String name) {
	        _name = name;
	        _id = id;
	    }

	    private String _name;
	    private int _id;

	    public String getName() {
	        return _name;
	    }
	    
	    public int getId() {
	        return _id;
	    }
	}
	
	/* Table INTELLECTUAL PROPERTY - column license status */
	
	public enum LicenseStatus{
		NO_LICENSE_ACTIVITY(0,"No License Activity"), 
		LICENSE_DISCUSSIONS(1,"License Discussions"), 
		OPTION_EXECUTED(2,"Option Executed"),
		NON_EXCLUSION_LICENSE(3,"Non-Exclusive License Executed"), 
		EXECUTED(4,"Exclusive License Executed "), 
		MULIPLY_LICENSES(5,"Multiple Licenses Executed"),
		REVENUE_GENERATED(6,"Revenue Generating"), 
		EXPIRED(7,"Licensed Expired/ Terminated");
	
		LicenseStatus(int id,String name) {
	        _name = name;
	        _id = id;
	    }

	    private String _name;
	    private int _id;

	    public String getName() {
	        return _name;
	    }
	    
	    public int getId() {
	        return _id;
	    }
	}
	
	/* Table TEAM MEMBER - column stage */
	
	public enum TeamCatRating{
		INSTITUTIONAL(0,"Institutional award and/or recognition"), 
		REGIONAL(1,"Regional award and/or recognition"), 
		NATIONAL(2,"National award and/or recognition"),
		INTERNATIONAL(3,"International award and/or recognition"), 
		HIGHEST(4,"Highest award and/or recognition in field");
		
		TeamCatRating(int id,String name) {
	        _name = name;
	        _id = id;
	    }

	    private String _name;
	    private int _id;

	    public String getName() {
	        return _name;
	    }
	    
	    public int getId() {
	        return _id;
	    }
	}
	
	

	public enum SizeOfTeam{
		NULL(0,"None"), 
		ONE(1,"1"), 
		TWO(2,"2 to 5"),
		THREE(3,"5 to 10"), 
		FOUR(4,"10 to 20"),
		FIVE(5,"20 to 50"), 
		SIX(6,"50 to 100"),
		SEVEN(7,"100 to 200"), 
		EIGHT(8,"200 to 500"),
		NINE(9,"500 to 1,000"), 
		TEN(10,">1,000");
	
		SizeOfTeam(int id,String name) {
	        _name = name;
	        _id = id;
	    }

	    private String _name;
	    private int _id;

	    public String getName() {
	        return _name;
	    }
	    
	    public int getId() {
	        return _id;
	    }
	}
	
	public enum SponsorType{
		HOME_INST(0,"HOME Inst."), 
		CIMIT(1,"CIMIT"), 
		NIH(2,"NIH"),
		DOD(3,"DoD"), 
		OTHER(4,"Other Fed"),
		STATE(5,"State"), 
		FOUNDATION(6,"Foundation"),
		PHIL(7,"Philantropy"), 
		CROWD(8,"Crowd"),
		INDUSTRY(9,"Industry"), 
		EQUITY(10,"Equity");
	
		SponsorType(int id,String name) {
	        _name = name;
	        _id = id;
	    }

	    private String _name;
	    private int _id;

	    public String getName() {
	        return _name;
	    }
	    
	    public int getId() {
	        return _id;
	    }
	}
	
	public enum SupportType{
		FUNDING(0,"Funding"), 
		MENTORING(1,"Mentoring"), 
		SERVICE(2,"Service"),
		OTHER(3,"Other (Facilities, etc.)"), 
		COMBINATION(4,"Combination");
		
	
		SupportType(int id,String name) {
	        _name = name;
	        _id = id;
	    }

	    private String _name;
	    private int _id;

	    public String getName() {
	        return _name;
	    }
	    
	    public int getId() {
	        return _id;
	    }
	}
	
	
	public enum CatTables{
		CLINICAL(0,"Clinical"), 
		ACADEMIC(1,"Knowledge"), 
		COMMERCIAL(2,"Commercial");
	
		CatTables(int id,String name) {
	        _name = name;
	        _id = id;
	    }

	    private String _name;
	    private int _id;

	    public String getName() {
	        return _name;
	    }
	    
	    public int getId() {
	        return _id;
	    }
	}
	
	public enum Tables{
		PATIENTS_IMPACTED(0,"Patients Impacted"), 
		SITES_USING_SOLUTION(1,"Sites Using Solution"), 
		PUBLICATIONS(2,"Publications"),
		CAREER_IMPACTED(3,"Career Impact"),
		RECOGNITION(4,"Recognition"),
		COLLABORATES(5,"Collaborators"),
		INTELLECTUAL_PROPERTY(6,"Intellectual Property"),
		FUNDING(7,"Funding / Support"),
		JOBS_CREATED(8,"Jobs Created");
		Tables(int id,String name) {
	        _name = name;
	        _id = id;
	    }

	    private String _name;
	    private int _id;

	    public String getName() {
	        return _name;
	    }
	    
	    public int getId() {
	        return _id;
	    }
	}
}