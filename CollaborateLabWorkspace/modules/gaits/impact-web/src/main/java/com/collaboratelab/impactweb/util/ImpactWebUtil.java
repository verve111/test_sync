package com.collaboratelab.impactweb.util;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.collaboratelab.impact.model.CareerImpact;
import com.collaboratelab.impact.model.Collaborators;
import com.collaboratelab.impact.model.IntellectualProperty;
import com.collaboratelab.impact.model.Jobs;
import com.collaboratelab.impact.model.PatientsImpacted;
import com.collaboratelab.impact.model.Publications;
import com.collaboratelab.impact.model.SitesUsingSolution;
import com.collaboratelab.impact.model.Support;
import com.collaboratelab.impact.model.TeamMemberProjectRecognition;
import com.collaboratelab.impact.service.CareerImpactLocalServiceUtil;
import com.collaboratelab.impact.service.CollaboratorsLocalServiceUtil;
import com.collaboratelab.impact.service.IntellectualPropertyLocalServiceUtil;
import com.collaboratelab.impact.service.JobsLocalServiceUtil;
import com.collaboratelab.impact.service.PatientsImpactedLocalServiceUtil;
import com.collaboratelab.impact.service.PublicationsLocalServiceUtil;
import com.collaboratelab.impact.service.SitesUsingSolutionLocalServiceUtil;
import com.collaboratelab.impact.service.SupportLocalServiceUtil;
import com.collaboratelab.impact.service.TeamMemberProjectRecognitionLocalServiceUtil;
import com.collaboratelab.impactweb.constants.ImpactWebPortletKeys;

public class ImpactWebUtil {

	public List<?> getListForSearchContainer(String name, long groupId) {
		
		List modifiableList = null;
		
		if (ImpactWebPortletKeys.Tables.PATIENTS_IMPACTED.getName().equalsIgnoreCase(name)) {
			List<PatientsImpacted> patients = PatientsImpactedLocalServiceUtil.findByGroupId(groupId);
			modifiableList = new ArrayList(patients);
			Comparator<PatientsImpacted> comparator = new Comparator<PatientsImpacted>() {
			    public int compare(PatientsImpacted c11, PatientsImpacted c22){
			    	return c11.getAsOfDate().compareTo(c22.getAsOfDate());
			 	}
			};
			Collections.sort(modifiableList, comparator);
		}

		if (ImpactWebPortletKeys.Tables.SITES_USING_SOLUTION.getName().equalsIgnoreCase(name)) {
			List<SitesUsingSolution> sites = SitesUsingSolutionLocalServiceUtil.findByGroupId(groupId);
			modifiableList = new ArrayList(sites);
			Comparator<SitesUsingSolution> comparator = new Comparator<SitesUsingSolution>() {
			    public int compare(SitesUsingSolution c11, SitesUsingSolution c22){
			    	return c11.getAsOfDate().compareTo(c22.getAsOfDate());
			 	}
			};
			Collections.sort(modifiableList, comparator) ;
		}
		
		if (ImpactWebPortletKeys.Tables.CAREER_IMPACTED.getName().equalsIgnoreCase(name)) {
			List<CareerImpact> career = CareerImpactLocalServiceUtil.findByGroupId(groupId);
			modifiableList = new ArrayList(career);
			Comparator<CareerImpact> comparator = new Comparator<CareerImpact>() {
			    public int compare(CareerImpact c11, CareerImpact c22){
			    	return c11.getAsOfDate().compareTo(c22.getAsOfDate());
			 	}
			};
			Collections.sort(modifiableList, comparator);
		}
		
		if (ImpactWebPortletKeys.Tables.INTELLECTUAL_PROPERTY.getName().equalsIgnoreCase(name)) {
			List<IntellectualProperty> props = IntellectualPropertyLocalServiceUtil.findByGroupId(groupId);
			modifiableList = new ArrayList(props);
			Comparator<IntellectualProperty> comparator = new Comparator<IntellectualProperty>() {
			    public int compare(IntellectualProperty c11, IntellectualProperty c22){
			    	return c11.getOneStFileDate().compareTo(c22.getOneStFileDate());
			 	}
			};
			Collections.sort(modifiableList, comparator);
		}

		if (ImpactWebPortletKeys.Tables.COLLABORATES.getName().equalsIgnoreCase(name)) {
			
			List<Collaborators> colabs = CollaboratorsLocalServiceUtil.findByGroupId(groupId);
			modifiableList = new ArrayList(colabs);
			Comparator<Collaborators> comparator = new Comparator<Collaborators>() {
			    public int compare(Collaborators c11, Collaborators c22){
			    	return c11.getStartDate().compareTo(c22.getStartDate());
			 	}
			};
			Collections.sort(modifiableList, comparator);
		}

		if (ImpactWebPortletKeys.Tables.RECOGNITION.getName().equalsIgnoreCase(name)) {
			List<TeamMemberProjectRecognition> recognitions =  TeamMemberProjectRecognitionLocalServiceUtil.findByGroupId(groupId);
			modifiableList = new ArrayList(recognitions);
			Comparator<TeamMemberProjectRecognition> comparator = new Comparator<TeamMemberProjectRecognition>() {
			    public int compare(TeamMemberProjectRecognition c11, TeamMemberProjectRecognition c22){
			    	return c11.getDateOfRecognition().compareTo(c22.getDateOfRecognition());
			 	}
			};
			Collections.sort(modifiableList, comparator);
		
		}
		
		if (ImpactWebPortletKeys.Tables.FUNDING.getName().equalsIgnoreCase(name)) {
			List<Support> supports =  SupportLocalServiceUtil.findByGroupId(groupId);
			modifiableList = new ArrayList(supports);
			Comparator<Support> comparator = new Comparator<Support>() {
			    public int compare(Support c11, Support c22){
			    	return c11.getStartDate().compareTo(c22.getStartDate());
			 	}
			};
			Collections.sort(modifiableList, comparator);
		}
		
		if (ImpactWebPortletKeys.Tables.JOBS_CREATED.getName().equalsIgnoreCase(name)) {
			List<Jobs> jobs =  JobsLocalServiceUtil.findByGroupId(groupId);
			modifiableList = new ArrayList(jobs);
			Comparator<Jobs> comparator = new Comparator<Jobs>() {
			    public int compare(Jobs c11, Jobs c22){
			    	return c11.getAsOfDate().compareTo(c22.getAsOfDate());
			 	}
			};
			Collections.sort(modifiableList, comparator);
		}
		
		if (ImpactWebPortletKeys.Tables.PUBLICATIONS.getName().equalsIgnoreCase(name)) {
			List<Publications> pubs =  PublicationsLocalServiceUtil.findByGroupId(groupId);
			modifiableList = new ArrayList(pubs);
			Comparator<Publications> comparator = new Comparator<Publications>() {
			    public int compare(Publications c11, Publications c22){
			    	return c11.getDate().compareTo(c22.getDate());
			 	}
			};
			Collections.sort(modifiableList, comparator);
		}
		
		return modifiableList;
	}
	
	public static String getDateByLocale(Date date, Locale locale) {
		if (date == null) {
			return "";
		}
		
		DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, locale);
		String formattedDate = df.format(date);
		
		return formattedDate;	
	}
	
	
	public static String getDateFormatDDMMYYY(Date date) {
		String datestr = "";
		if(date != null) {
			Calendar dateCal = Calendar.getInstance();
			dateCal.setTime(date);
			datestr = dateCal.get(Calendar.DAY_OF_MONTH) + "/" + (dateCal.get(Calendar.MONTH)+1) + "/" +dateCal.get(Calendar.YEAR);
		}
		return datestr;
	}

}
