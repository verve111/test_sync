package com.collaboratelab.impactweb.portlet;

import java.io.IOException;
import java.util.Calendar;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletModeException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.WindowStateException;

import org.osgi.service.component.annotations.Component;

import com.collaboratelab.impact.model.CareerImpact;
import com.collaboratelab.impact.model.Collaborators;
import com.collaboratelab.impact.model.IntellectualProperty;
import com.collaboratelab.impact.model.Jobs;
import com.collaboratelab.impact.model.PatientsImpacted;
import com.collaboratelab.impact.model.Publications;
import com.collaboratelab.impact.model.SitesUsingSolution;
import com.collaboratelab.impact.model.Support;
import com.collaboratelab.impact.model.TeamMemberProjectRecognition;
import com.collaboratelab.impact.service.CareerImpactLocalServiceUtil;
import com.collaboratelab.impact.service.CollaboratorsLocalServiceUtil;
import com.collaboratelab.impact.service.IntellectualPropertyLocalServiceUtil;
import com.collaboratelab.impact.service.JobsLocalServiceUtil;
import com.collaboratelab.impact.service.PatientsImpactedLocalServiceUtil;
import com.collaboratelab.impact.service.PublicationsLocalServiceUtil;
import com.collaboratelab.impact.service.SitesUsingSolutionLocalServiceUtil;
import com.collaboratelab.impact.service.SupportLocalServiceUtil;
import com.collaboratelab.impact.service.TeamMemberProjectRecognitionLocalServiceUtil;
import com.collaboratelab.impactweb.constants.ImpactWebPortletKeys;
import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.LiferayPortletMode;
import com.liferay.portal.kernel.portlet.LiferayWindowState;
import com.liferay.portal.kernel.portlet.PortletURLFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;

@Component(immediate = true, property = { "com.liferay.portlet.display-category=category.gaits",
		"com.liferay.portlet.instanceable=false", "javax.portlet.display-name=Impact Display Portlet",
		"javax.portlet.init-param.template-path=/", "javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + ImpactWebPortletKeys.ImpactWeb, "javax.portlet.resource-bundle=content.Language",
		"com.liferay.portlet.header-portlet-javascript=/js/tooltipster.bundle.min.js",
		"com.liferay.portlet.header-portlet-css=/css/tooltipster.bundle.min.css",
		"com.liferay.portlet.header-portlet-css=/css/tooltipster-sideTip-borderless.min.css",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)

public class ImpactWebPortlet extends MVCPortlet {

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
 
		super.render(renderRequest, renderResponse);
	}

	public void addRowPatientImpacted(ActionRequest request, ActionResponse response) {

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		
		long patientsImpactedId = ParamUtil.getLong(request, "patientsImpactedId");
		PatientsImpacted patientsImpacted = null;
		 
		if(patientsImpactedId != 0) {
			try {
				patientsImpacted =  PatientsImpactedLocalServiceUtil.getPatientsImpacted(patientsImpactedId);
			} catch (PortalException e) {
				e.printStackTrace();
			}
		}
		else {
			patientsImpactedId = CounterLocalServiceUtil.increment();
			patientsImpacted = PatientsImpactedLocalServiceUtil.createPatientsImpacted(patientsImpactedId);
		}
		
		long groupId = themeDisplay.getScopeGroupId();

		int asOfDateDay = ParamUtil.getInteger(request, "asOfDateDay");
		int asOfDateMonth = ParamUtil.getInteger(request, "asOfDateMonth");
		int asOfDateYear = ParamUtil.getInteger(request, "asOfDateYear");

		Calendar startCal = Calendar.getInstance();
		startCal.set(Calendar.DAY_OF_MONTH, asOfDateDay);
		startCal.set(Calendar.MONTH, asOfDateMonth);
		startCal.set(Calendar.YEAR, asOfDateYear);

		String comment = ParamUtil.getString(request, "optionalComments");
		int rangeEstimate = ParamUtil.getInteger(request, "rangeEstimate");
		int population = ParamUtil.getInteger(request, "population");

		patientsImpacted.setAsOfDate(startCal.getTime());
		patientsImpacted.setGroupId(groupId);
		patientsImpacted.setPopulation(population);
		patientsImpacted.setOptionalComments(comment);
		patientsImpacted.setRangeEstimate(rangeEstimate);

		PatientsImpactedLocalServiceUtil.updatePatientsImpacted(patientsImpacted);

		createRenderUrl(request, response, themeDisplay, ImpactWebPortletKeys.Tables.PATIENTS_IMPACTED.getName());
	}

	public void addRowSitesUsingSolution(ActionRequest request, ActionResponse response) {

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		
		long sitesUsingSolutionId = ParamUtil.getLong(request, "sitesUsingSolutionId");
		SitesUsingSolution sitesUsingSolution = null;
		
		if(sitesUsingSolutionId != 0) {
			try {
				sitesUsingSolution = SitesUsingSolutionLocalServiceUtil.getSitesUsingSolution(sitesUsingSolutionId);
			} catch (PortalException e) {
				e.printStackTrace();
			}
		}
		else {
			sitesUsingSolutionId = CounterLocalServiceUtil.increment();
			sitesUsingSolution = SitesUsingSolutionLocalServiceUtil.createSitesUsingSolution(sitesUsingSolutionId);
		}
		
		long groupId = themeDisplay.getScopeGroupId();

		int asOfDateDay = ParamUtil.getInteger(request, "asOfDateDay");
		int asOfDateMonth = ParamUtil.getInteger(request, "asOfDateMonth");
		int asOfDateYear = ParamUtil.getInteger(request, "asOfDateYear");

		Calendar startCal = Calendar.getInstance();
		startCal.set(Calendar.DAY_OF_MONTH, asOfDateDay);
		startCal.set(Calendar.MONTH, asOfDateMonth);
		startCal.set(Calendar.YEAR, asOfDateYear);

		String comment = ParamUtil.getString(request, "optionalComments");
		int number = ParamUtil.getInteger(request, "number");

		sitesUsingSolution.setNumber(number);
		sitesUsingSolution.setGroupId(groupId);
		sitesUsingSolution.setAsOfDate(startCal.getTime());

		sitesUsingSolution.setOptionalComments(comment);

		SitesUsingSolutionLocalServiceUtil.updateSitesUsingSolution(sitesUsingSolution);

		createRenderUrl(request, response, themeDisplay, ImpactWebPortletKeys.Tables.SITES_USING_SOLUTION.getName());

	}

	public void addRowCareerImpact(ActionRequest request, ActionResponse response) {

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);

		long careerImpactId = ParamUtil.getLong(request, "careerImpactId");
		CareerImpact careerImpact = null;
		
		if(careerImpactId != 0) {
			try {
				careerImpact = CareerImpactLocalServiceUtil.getCareerImpact(careerImpactId);
			} catch (PortalException e) {
				e.printStackTrace();
			}
		}
		else {
			careerImpactId = CounterLocalServiceUtil.increment();
			careerImpact = CareerImpactLocalServiceUtil.createCareerImpact(careerImpactId);
		}
		
		long groupId = themeDisplay.getScopeGroupId();
		
		int asOfDateDay = ParamUtil.getInteger(request, "asOfDateDay");
		int asOfDateMonth = ParamUtil.getInteger(request, "asOfDateMonth");
		int asOfDateYear = ParamUtil.getInteger(request, "asOfDateYear");

		Calendar startCal = Calendar.getInstance();
		startCal.set(Calendar.DAY_OF_MONTH, asOfDateDay);
		startCal.set(Calendar.MONTH, asOfDateMonth);
		startCal.set(Calendar.YEAR, asOfDateYear);

		String comment = ParamUtil.getString(request, "optionalComments");
		int categoryRating = ParamUtil.getInteger(request, "categoryRating");

		careerImpact.setCategoryRaing(categoryRating);
		careerImpact.setGroupId(groupId);
		careerImpact.setAsOfDate(startCal.getTime());
		careerImpact.setOptionalComments(comment);

		CareerImpactLocalServiceUtil.updateCareerImpact(careerImpact);
	
		createRenderUrl(request, response, themeDisplay, ImpactWebPortletKeys.Tables.CAREER_IMPACTED.getName());

	}

	public void addRowCollaborators(ActionRequest request, ActionResponse response) {

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		
		long collaboratorsId = ParamUtil.getLong(request, "collaboratorsId");
		Collaborators collaborators = null;
		
		if(collaboratorsId != 0) {
			try {
				collaborators = CollaboratorsLocalServiceUtil.getCollaborators(collaboratorsId);
			} catch (PortalException e) {
				e.printStackTrace();
			}
		}
		else {
			collaboratorsId = CounterLocalServiceUtil.increment();
			collaborators = CollaboratorsLocalServiceUtil.createCollaborators(collaboratorsId);
		}
		
		long groupId = themeDisplay.getScopeGroupId();

		int startDateDay = ParamUtil.getInteger(request, "startDateDay");
		int startDateMonth = ParamUtil.getInteger(request, "startDateMonth");
		int startDateYear = ParamUtil.getInteger(request, "startDateYear");

		int endDateDay = ParamUtil.getInteger(request, "endDateDay");
		int endDateMonth = ParamUtil.getInteger(request, "endDateMonth");
		int endDateYear = ParamUtil.getInteger(request, "endDateYear");

		Calendar startCal = Calendar.getInstance();
		startCal.set(Calendar.DAY_OF_MONTH, startDateDay);
		startCal.set(Calendar.MONTH, startDateMonth);
		startCal.set(Calendar.YEAR, startDateYear);

		Calendar endCal = null;
		
		if (endDateYear > 0) {
			endCal = Calendar.getInstance();
			endCal.set(Calendar.DAY_OF_MONTH, endDateDay);
			endCal.set(Calendar.MONTH, endDateMonth);
			endCal.set(Calendar.YEAR, endDateYear);
		}

		String comment = ParamUtil.getString(request, "optionalComments");
		String name = ParamUtil.getString(request, "name");
		String url = ParamUtil.getString(request, "url");
		int type = ParamUtil.getInteger(request, "type");

		collaborators.setName(name);
		collaborators.setURL(url);
		collaborators.setType(type);
		collaborators.setGroupId(groupId);
		collaborators.setStartDate(startCal.getTime());
		if (endDateYear > 0) {
			collaborators.setEndDate(endCal.getTime());
		} else {
			collaborators.setEndDate(null);
		}
		collaborators.setOptionalComments(comment);
		
		CollaboratorsLocalServiceUtil.updateCollaborators(collaborators);

		createRenderUrl(request, response, themeDisplay, ImpactWebPortletKeys.Tables.COLLABORATES.getName());

	}

	public void addRowIntellectualProperty(ActionRequest request, ActionResponse response) {

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		
		long intellectualPropertyId = ParamUtil.getLong(request, "intellectualPropertyId");
		IntellectualProperty intellectualProperty = null;
		
		if(intellectualPropertyId != 0) {
			try {
				intellectualProperty = IntellectualPropertyLocalServiceUtil.getIntellectualProperty(intellectualPropertyId);
			} catch (PortalException e) {
				e.printStackTrace();
			}
		}
		else {
			intellectualPropertyId = CounterLocalServiceUtil.increment();
			intellectualProperty = IntellectualPropertyLocalServiceUtil.createIntellectualProperty(intellectualPropertyId);
		}
		
		long groupId = themeDisplay.getScopeGroupId();

		int fileDateDay = ParamUtil.getInteger(request, "fileDateDay");
		int fileDateMonth = ParamUtil.getInteger(request, "fileDateMonth");
		int fileDateYear = ParamUtil.getInteger(request, "fileDateYear");

		Calendar fileCal = Calendar.getInstance();
		fileCal.set(Calendar.DAY_OF_MONTH, fileDateDay);
		fileCal.set(Calendar.MONTH, fileDateMonth);
		fileCal.set(Calendar.YEAR, fileDateYear);

		int asOfDateDay = ParamUtil.getInteger(request, "asOfDateDay");
		int asOfDateMonth = ParamUtil.getInteger(request, "asOfDateMonth");
		int asOfDateYear = ParamUtil.getInteger(request, "asOfDateYear");

		Calendar startCal = Calendar.getInstance();
		startCal.set(Calendar.DAY_OF_MONTH, asOfDateDay);
		startCal.set(Calendar.MONTH, asOfDateMonth);
		startCal.set(Calendar.YEAR, asOfDateYear);

		String title = ParamUtil.getString(request, "title");
		int licenseStatus = ParamUtil.getInteger(request, "licenseStatus");
		int stage = ParamUtil.getInteger(request, "stage");

		intellectualProperty.setOneStFileDate(fileCal.getTime());
		intellectualProperty.setLicenseStatus(licenseStatus);
		intellectualProperty.setStage(stage);
		intellectualProperty.setTitle(title);
		intellectualProperty.setGroupId(groupId);
		intellectualProperty.setAsOfDate(startCal.getTime());

		IntellectualPropertyLocalServiceUtil.updateIntellectualProperty(intellectualProperty);

		createRenderUrl(request, response, themeDisplay, ImpactWebPortletKeys.Tables.INTELLECTUAL_PROPERTY.getName());

	}
	
	public void addRowTeamMemberProjectRecognition(ActionRequest request, ActionResponse response) {

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		
		long teamMemberProjectRecognitionId = ParamUtil.getLong(request, "recognitionId");
		TeamMemberProjectRecognition teamMemberProjectRecognition = null;
		
		if(teamMemberProjectRecognitionId != 0) {
			try {
				teamMemberProjectRecognition = TeamMemberProjectRecognitionLocalServiceUtil.getTeamMemberProjectRecognition(teamMemberProjectRecognitionId);
			} catch (PortalException e) {
				e.printStackTrace();
			}
		}
		else {
			teamMemberProjectRecognitionId = CounterLocalServiceUtil.increment();
			teamMemberProjectRecognition = TeamMemberProjectRecognitionLocalServiceUtil.createTeamMemberProjectRecognition(teamMemberProjectRecognitionId);
		}
		
		long groupId = themeDisplay.getScopeGroupId();

		int dateOfRecogDay = ParamUtil.getInteger(request, "dateOfRecogDay");
		int dateOfRecogMonth = ParamUtil.getInteger(request, "dateOfRecogMonth");
		int dateOfRecogYear = ParamUtil.getInteger(request, "dateOfRecogYear");

		Calendar startCal = Calendar.getInstance();
		startCal.set(Calendar.DAY_OF_MONTH,dateOfRecogDay);
		startCal.set(Calendar.MONTH, dateOfRecogMonth);
		startCal.set(Calendar.YEAR, dateOfRecogYear);

		String name = ParamUtil.getString(request, "name");
		int catRating = ParamUtil.getInteger(request, "catRating");
		String comment = ParamUtil.getString(request, "optionalComments");
		String url = ParamUtil.getString(request, "url");
		
		teamMemberProjectRecognition.setDateOfRecognition(startCal.getTime());
		teamMemberProjectRecognition.setName(name);
		teamMemberProjectRecognition.setGroupId(groupId);
		teamMemberProjectRecognition.setCatRating(catRating);
		teamMemberProjectRecognition.setOptionalComments(comment);
		teamMemberProjectRecognition.setURL(url);

		TeamMemberProjectRecognitionLocalServiceUtil.updateTeamMemberProjectRecognition(teamMemberProjectRecognition);

		createRenderUrl(request, response, themeDisplay, ImpactWebPortletKeys.Tables.RECOGNITION.getName());

	}
	
	public void addRowJobs(ActionRequest request, ActionResponse response) {

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		
		long jobsId = ParamUtil.getLong(request, "jobsId");
		Jobs jobs = null;
		
		if(jobsId != 0) {
			try {
				jobs = JobsLocalServiceUtil.getJobs(jobsId);
			} catch (PortalException e) {
				e.printStackTrace();
			}
		}
		else {
			jobsId = CounterLocalServiceUtil.increment();
			jobs = JobsLocalServiceUtil.createJobs(jobsId);
		}
		
		long groupId = themeDisplay.getScopeGroupId();

		int asOfDateDay = ParamUtil.getInteger(request, "asOfDateDay");
		int asOfDateMonth = ParamUtil.getInteger(request, "asOfDateMonth");
		int asOfDateYear = ParamUtil.getInteger(request, "asOfDateYear");

		Calendar startCal = Calendar.getInstance();
		startCal.set(Calendar.DAY_OF_MONTH, asOfDateDay);
		startCal.set(Calendar.MONTH, asOfDateMonth);
		startCal.set(Calendar.YEAR, asOfDateYear);

		int size = ParamUtil.getInteger(request, "sizeOfTeam");
		String comment = ParamUtil.getString(request, "comments");
		String[] primaryJobs = ParamUtil.getParameterValues(request, "primaryJob");
		String primaryJob = "";
		if(primaryJobs.length != 0 ) {
			StringBuilder primaryJobBuilder = new StringBuilder("");
			for(String pr : primaryJobs) {
				primaryJobBuilder.append(pr+ ",") ;
			}
			primaryJob = primaryJobBuilder.toString().substring(0, primaryJobBuilder.toString().length()-1);
		}
		
		jobs.setPrimaryJob(primaryJob);
		jobs.setAsOfDate(startCal.getTime());
		jobs.setSizeOfTeam(size);
		jobs.setGroupId(groupId);
		jobs.setComments(comment);

		JobsLocalServiceUtil.updateJobs(jobs);

		createRenderUrl(request, response, themeDisplay, ImpactWebPortletKeys.Tables.JOBS_CREATED.getName());

	}
	
	public void addRowSupport(ActionRequest request, ActionResponse response) {

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		
		long supportId = ParamUtil.getLong(request, "supportId");
		Support support = null;
		
		if(supportId != 0) {
			try {
				support = SupportLocalServiceUtil.getSupport(supportId);
			} catch (PortalException e) {
				e.printStackTrace();
			}
		}
		else {
			supportId = CounterLocalServiceUtil.increment();
			support = SupportLocalServiceUtil.createSupport(supportId);
		}
		
		long groupId = themeDisplay.getScopeGroupId();

		int startDateDay = ParamUtil.getInteger(request, "startDateDay");
		int startDateMonth = ParamUtil.getInteger(request, "startDateMonth");
		int startDateYear = ParamUtil.getInteger(request, "startDateYear");

		Calendar startCal = Calendar.getInstance();
		startCal.set(Calendar.DAY_OF_MONTH, startDateDay);
		startCal.set(Calendar.MONTH, startDateMonth);
		startCal.set(Calendar.YEAR, startDateYear);
		
		int duration = ParamUtil.getInteger(request, "duration");

		double total = ParamUtil.getDouble(request, "totalValue");
		int sponsorType = ParamUtil.getInteger(request, "sponsorType");
		int supportType = ParamUtil.getInteger(request, "supportType");
		
		support.setStartDate(startCal.getTime());
		support.setDuration(duration);
		support.setSponsorType(sponsorType);
		support.setSupportType(supportType);
		support.setGroupId(groupId);
		support.setTotalValue(total);

		SupportLocalServiceUtil.updateSupport(support);

		createRenderUrl(request, response, themeDisplay, ImpactWebPortletKeys.Tables.FUNDING.getName());
	}
	
	
	public void addRowPublications(ActionRequest request, ActionResponse response) {

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		
		long publicationsId = ParamUtil.getLong(request, "publicationsId");
		Publications publications = null;
		
		if(publicationsId != 0) {
			try {
				publications = PublicationsLocalServiceUtil.getPublications(publicationsId);
			} catch (PortalException e) {
				e.printStackTrace();
			}
		}
		else {
			publicationsId = CounterLocalServiceUtil.increment();
			publications = PublicationsLocalServiceUtil.createPublications(publicationsId);
		}
			
		long groupId = themeDisplay.getScopeGroupId();

		int dateDay = ParamUtil.getInteger(request, "dateDay");
		int dateMonth = ParamUtil.getInteger(request, "dateMonth");
		int dateYear = ParamUtil.getInteger(request, "dateYear");

		Calendar startCal = Calendar.getInstance();
		startCal.set(Calendar.DAY_OF_MONTH, dateDay);
		startCal.set(Calendar.MONTH, dateMonth);
		startCal.set(Calendar.YEAR, dateYear);
		
		String title = ParamUtil.getString(request, "titleOfPublications");
		String url = ParamUtil.getString(request, "url");
		String comments  = ParamUtil.getString(request, "comments");
		
		publications.setDate(startCal.getTime());
		publications.setTitleOfPublications(title);
		publications.setURL(url);
		publications.setComments(comments);
		publications.setGroupId(groupId);

		PublicationsLocalServiceUtil.updatePublications(publications);

		createRenderUrl(request, response, themeDisplay, ImpactWebPortletKeys.Tables.PUBLICATIONS.getName());
	}

	private void createRenderUrl(ActionRequest request, ActionResponse response, ThemeDisplay themeDisplay,
			String name) {

		PortletURL redirectURL = PortletURLFactoryUtil.create(PortalUtil.getHttpServletRequest(request),
				themeDisplay.getPortletDisplay().getId(), themeDisplay.getLayout().getPlid(),
				PortletRequest.RENDER_PHASE);

		redirectURL.setParameter("jspPage", ImpactWebPortletKeys.VIEW_TEMPLATE_TABLE_PATH);
		redirectURL.setParameter("searchContainerName", name);

		try {
			redirectURL.setWindowState(LiferayWindowState.POP_UP);
		} catch (WindowStateException e1) {
			e1.printStackTrace();
		}
		try {
			redirectURL.setPortletMode(LiferayPortletMode.VIEW);
		} catch (PortletModeException e1) {
			e1.printStackTrace();
		}

		try {
			response.sendRedirect(redirectURL.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	
	public void deleteRowPatientImpacted(ActionRequest request, ActionResponse response) {

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		
		long patientsImpactedId = ParamUtil.getLong(request, "patientsImpactedId");
		
		try {
			PatientsImpactedLocalServiceUtil.deletePatientsImpacted(patientsImpactedId);
		} catch (PortalException e) {
			e.printStackTrace();
		}

		createRenderUrl(request, response, themeDisplay, ImpactWebPortletKeys.Tables.PATIENTS_IMPACTED.getName());
	}

	public void deleteRowSitesUsingSolution(ActionRequest request, ActionResponse response) {

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		
		long sitesUsingSolutionId = ParamUtil.getLong(request, "sitesUsingSolutionId");

		try {
			SitesUsingSolutionLocalServiceUtil.deleteSitesUsingSolution(sitesUsingSolutionId);
		} catch (PortalException e) {
			e.printStackTrace();
		}

		createRenderUrl(request, response, themeDisplay, ImpactWebPortletKeys.Tables.SITES_USING_SOLUTION.getName());

	}

	public void deleteRowCareerImpact(ActionRequest request, ActionResponse response) {

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);

		long careerImpactId = ParamUtil.getLong(request, "careerImpactId");
		
		try {
			CareerImpactLocalServiceUtil.deleteCareerImpact(careerImpactId);
		} catch (PortalException e) {
			e.printStackTrace();
		}
	
		createRenderUrl(request, response, themeDisplay, ImpactWebPortletKeys.Tables.CAREER_IMPACTED.getName());

	}

	public void deleteRowCollaborators(ActionRequest request, ActionResponse response) {

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		
		long collaboratorsId = ParamUtil.getLong(request, "collaboratorsId");
		
		try {
			CollaboratorsLocalServiceUtil.deleteCollaborators(collaboratorsId);
		} catch (PortalException e) {
			e.printStackTrace();
		}

		createRenderUrl(request, response, themeDisplay, ImpactWebPortletKeys.Tables.COLLABORATES.getName());

	}

	public void deleteRowIntellectualProperty(ActionRequest request, ActionResponse response) {

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		
		long intellectualPropertyId = ParamUtil.getLong(request, "intellectualPropertyId");

		try {
			IntellectualPropertyLocalServiceUtil.deleteIntellectualProperty(intellectualPropertyId);
		} catch (PortalException e) {
			e.printStackTrace();
		}

		createRenderUrl(request, response, themeDisplay, ImpactWebPortletKeys.Tables.INTELLECTUAL_PROPERTY.getName());

	}
	
	public void deleteRowTeamMemberProjectRecognition(ActionRequest request, ActionResponse response) {

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		
		long teamMemberProjectRecognitionId = ParamUtil.getLong(request, "recognitionId");

		try {
			TeamMemberProjectRecognitionLocalServiceUtil.deleteTeamMemberProjectRecognition(teamMemberProjectRecognitionId);
		} catch (PortalException e) {
			e.printStackTrace();
		}

		createRenderUrl(request, response, themeDisplay, ImpactWebPortletKeys.Tables.RECOGNITION.getName());

	}
	
	public void deleteRowJobs(ActionRequest request, ActionResponse response) {

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		
		long jobsId = ParamUtil.getLong(request, "jobsId");

		try {
			JobsLocalServiceUtil.deleteJobs(jobsId);
		} catch (PortalException e) {
			e.printStackTrace();
		}

		createRenderUrl(request, response, themeDisplay, ImpactWebPortletKeys.Tables.JOBS_CREATED.getName());

	}
	
	public void deleteRowSupport(ActionRequest request, ActionResponse response) {

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		
		long supportId = ParamUtil.getLong(request, "supportId");
		
		try {
			SupportLocalServiceUtil.deleteSupport(supportId);
		} catch (PortalException e) {
			e.printStackTrace();
		}
		
		createRenderUrl(request, response, themeDisplay, ImpactWebPortletKeys.Tables.FUNDING.getName());
	}
	
	
	public void deleteRowPublications(ActionRequest request, ActionResponse response) {

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		
		long publicationsId = ParamUtil.getLong(request, "publicationsId");

		try {
			PublicationsLocalServiceUtil.deletePublications(publicationsId);
		} catch (PortalException e) {
			e.printStackTrace();
		}

		createRenderUrl(request, response, themeDisplay, ImpactWebPortletKeys.Tables.PUBLICATIONS.getName());
	}

}