package com.collaboratelab.project.upgrade.v1_0_3;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.collaboratelab.project.constants.Utils;
import com.collaboratelab.project.constants.enums.Enums;
import com.collaboratelab.project.model.Project;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalFolder;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.LayoutTypePortlet;
import com.liferay.portal.kernel.portlet.PortletPreferencesFactoryUtil;
import com.liferay.portal.kernel.service.GroupLocalServiceUtil;
import com.liferay.portal.kernel.service.LayoutLocalServiceUtil;
import com.liferay.portal.kernel.service.PortletPreferencesLocalServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.util.PortletKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;

public class UpgradeMembersLayout extends UpgradeProcess {
	
	private static final Log logger = LogFactoryUtil.getLog(UpgradeMembersLayout.class);	
	
    @Override
    protected void doUpgrade() throws Exception {
        int type = Enums.TypeProject.MEDICAL.getId();
        List<Project> projectsByType = Utils.getProjectsByType(type);

        for (Project pr : projectsByType) {
            long groupId = pr.getGroupId();

            Group group = GroupLocalServiceUtil.fetchGroup(groupId);
            if (group == null)
                continue;

            updateLayout(group);
        }
    }

    private void updateLayout(Group group) throws Exception {
        Layout activityLayout = null;
        LayoutTypePortlet layoutTypePortlet;
        String webContentPortletId = "";
        long groupId = group.getGroupId();
        long userId = group.getCreatorUserId();
        long companyId = group.getCompanyId();

        String typeSettings = "column-1=com_liferay_directory_web_portlet_SiteMembersDirectoryPortlet\n" +
                "column-2=com_liferay_invitation_invite_members_web_portlet_InviteMembersPortlet\n" +
                "layout-template-id=2_columns_iii\n";

        try {
            activityLayout = LayoutLocalServiceUtil.getFriendlyURLLayout(groupId, true, "/members");
            activityLayout.setTypeSettings(typeSettings);
            layoutTypePortlet = (LayoutTypePortlet) activityLayout.getLayoutType();
            webContentPortletId = layoutTypePortlet.addPortletId(userId, "com_liferay_journal_content_web_portlet_JournalContentPortlet",
                    "column-2", 0, false);
            LayoutLocalServiceUtil.updateLayout(activityLayout);
            setPreference(activityLayout, "com_liferay_invitation_invite_members_web_portlet_InviteMembersPortlet",
                    new HashMap<String, String>() { { put("portletSetupPortletDecoratorId", "borderless"); } });

            setPreference(activityLayout, "com_liferay_directory_web_portlet_SiteMembersDirectoryPortlet",
                    new HashMap<String, String>() { { put("portletSetupPortletDecoratorId", "borderless"); } });

            JournalArticle article = addArticle(userId, groupId, companyId);

            if (article == null)
                return;

            setPreference(activityLayout, webContentPortletId,
                    new HashMap<String, String>() {
                        { put("portletSetupPortletDecoratorId", "barebone"); }
                        { put("articleId", String.valueOf(article.getArticleId())); }
                        { put("groupId", String.valueOf(Utils.getGlobalGroupId(companyId))); }
                    });            
        } catch (Exception e) {
        	logger.warn("Layout not exists for the site :: " + group.getGroupKey());
        }


    }

    private void setPreference(Layout layout, String portletId, Map<String, String> kv) throws Exception {
        long ownerId = PortletKeys.PREFS_OWNER_ID_DEFAULT;
        int ownerType = PortletKeys.PREFS_OWNER_TYPE_LAYOUT;

        try {
            javax.portlet.PortletPreferences preferences = PortletPreferencesFactoryUtil.
                    getLayoutPortletSetup(layout, portletId);

            for(Map.Entry<String, String> entry : kv.entrySet()) {
                preferences.setValue(entry.getKey(), entry.getValue());
            }
            preferences.store();

            PortletPreferencesLocalServiceUtil.updatePreferences(ownerId, ownerType, layout.getPlid(), portletId, preferences);
        } catch (Exception e) {
            logger.error("updatePreferences ex:: ", e);
        }
    }

    private JournalArticle addArticle(long userId, long groupId, long companyId) throws Exception {
        String xmlContent = "<?xml version=\"1.0\"?>\n" +
                "\n" +
                "<root available-locales=\"en_US\" default-locale=\"en_US\">\n" +
                "\t<dynamic-element name=\"content\" type=\"text_area\" index-type=\"text\" instance-id=\"ciuh\">\n" +
                "\t\t<dynamic-content language-id=\"en_US\"><![CDATA[<p>Please contact the team site administrator to request additional members be given access.</p>]]></dynamic-content>\n" +
                "\t</dynamic-element>\n" +
                "</root>";

        String title = "members_wc";
        ServiceContext serviceContext = new ServiceContext();
        serviceContext.setAddGroupPermissions(true);
        serviceContext.setAddGuestPermissions(true);
        serviceContext.setScopeGroupId(groupId);
        serviceContext.setWorkflowAction(WorkflowConstants.ACTION_PUBLISH);

        Map<Locale, String> titleMap = new HashMap<Locale, String>();
        Map<Locale, String> descriptionMap = new HashMap<Locale, String>();
        titleMap.put(Locale.US, title);
        descriptionMap.put(Locale.US, title);

        String ddmStructureKey = "BASIC-WEB-CONTENT";
        String ddmTemplateKey = "BASIC-WEB-CONTENT";

        String folderName = com.collaboratelab.project.constants.Constants.GLOBAL_FOLDER_SYSTEM;
        long globalGroupId =  Utils.getGlobalGroupId(companyId);
        JournalFolder folder = Utils.fetchFolder(globalGroupId, folderName, 0);

        if(folder == null)
            return null;

        JournalArticle article = null;

        article = Utils.fetchJournalArticleByParentFolderId(globalGroupId, title, folder.getFolderId());

        if(article != null)
            return article;

        try {
            article = JournalArticleLocalServiceUtil.addArticle(userId, globalGroupId, folder.getFolderId(),
                    titleMap, descriptionMap, xmlContent, ddmStructureKey, ddmTemplateKey, serviceContext);
        } catch (Exception e) {
            logger.error("addArticle ex:: ", e);
        }

        return article;
    }
}
