package com.collaboratelab.project.upgrade.v1_0_0;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.GroupLocalServiceUtil;
import com.liferay.portal.kernel.service.LayoutSetLocalServiceUtil;
import com.collaboratelab.project.model.Project;
import com.collaboratelab.project.constants.enums.Enums.TypeProject;
import com.collaboratelab.project.constants.Utils;

import java.util.*;

public class UpgradeSetThemeForSites extends UpgradeProcess {

	@Override
	protected void doUpgrade() throws Exception {
		int type = TypeProject.MEDICAL.getId();

		List<Project> projectsByType = Utils.getProjectsByType(type);
		for (Project pr : projectsByType) {
			long groupId = pr.getGroupId();
			Group group = GroupLocalServiceUtil.fetchGroup(groupId);
			if (group != null) {
				LayoutSetLocalServiceUtil.updateLookAndFeel(groupId, "gaitstheme_WAR_gaitstheme", "", "");
			}
		}
	}
}