package com.collaboratelab.project.upgrade;

import com.liferay.portal.upgrade.registry.UpgradeStepRegistrator;
import com.collaboratelab.project.upgrade.v1_0_0.AlterSupportImpactType;
import com.collaboratelab.project.upgrade.v1_0_0.UpgradeSetThemeForSites;
import com.collaboratelab.project.upgrade.v1_0_2.UpgradeEnableRatings;
import com.collaboratelab.project.upgrade.v1_0_3.UpgradeMembersLayout;
import com.collaboratelab.project.upgrade.v1_0_4.AlterCommentsLength;

import org.osgi.service.component.annotations.Component;


@Component(immediate = true, service = UpgradeStepRegistrator.class)
public class GaitsUpgradeStepRegistrator implements UpgradeStepRegistrator {
    @Override
    public void register(Registry registry) {
        registry.register("com.collaboratelab.gaits.upgrade.module", "0.0.0", "1.0.0",
        new AlterSupportImpactType(),        		
        new UpgradeSetThemeForSites());
        
        registry.register("com.collaboratelab.gaits.upgrade.module", "1.0.0", "1.0.2",
        new UpgradeEnableRatings());    
        
        registry.register("com.collaboratelab.gaits.upgrade.module", "1.0.2", "1.0.3",
        	new UpgradeMembersLayout());      
    
        registry.register("com.collaboratelab.gaits.upgrade.module", "1.0.3", "1.0.4",
            	new AlterCommentsLength());              
        
    }
}