package com.collaboratelab.project.upgrade.v1_0_0.util;

import java.sql.Types;

public class ImpactSupportTable {
	
	public static final String TABLE_NAME = "CLab_Impact_Support";
	
	public static final Object[][] TABLE_COLUMNS = {
			{ "uuid_", Types.VARCHAR },
			{ "supportId", Types.BIGINT },
			{ "groupId", Types.BIGINT },
			{ "companyId", Types.BIGINT },
			{ "userId", Types.BIGINT },
			{ "userName", Types.VARCHAR },
			{ "createDate", Types.TIMESTAMP },
			{ "modifiedDate", Types.TIMESTAMP },
			{ "startDate", Types.TIMESTAMP },
			{ "duration", Types.INTEGER },
			{ "totalValue", Types.DOUBLE },
			{ "sponsorType", Types.INTEGER },
			{ "supportType", Types.INTEGER }
		};	

	public static final String TABLE_SQL_CREATE = "create table CLab_Impact_Support (uuid_ VARCHAR(75) null,supportId LONG not null primary key,groupId LONG,companyId LONG,userId LONG,userName VARCHAR(75) null,createDate DATE null,modifiedDate DATE null,startDate DATE null,duration INT 0,totalValue DOUBLE,sponsorType INTEGER,supportType INTEGER)";
	
	public static final String TABLE_SQL_DROP = "drop table CLab_Impact_Support";
	
	public static final String[] TABLE_SQL_ADD_INDEXES = {
			"create index IX_8EB69A59 on CLab_Impact_Support (groupId)",
			"create index IX_D6F75125 on CLab_Impact_Support (uuid_[$COLUMN_LENGTH:75$], companyId)",
			"create index IX_31EDC469 on CLab_Impact_Support (supportId)",
			"create unique index IX_1FFBDCE7 on CLab_Impact_Support (uuid_[$COLUMN_LENGTH:75$], groupId)"
		};
	
	

}
