package com.collaboratelab.project.upgrade.v1_0_4;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;

public class AlterCommentsLength extends UpgradeProcess {
	
	@Override
	protected void doUpgrade() throws Exception {
		runSQL("ALTER TABLE CLab_Prj_ProjectDelivery \r\n" + 
				"	CHANGE COLUMN comment_ comment_ VARCHAR(2500) NULL DEFAULT NULL COLLATE 'utf8_bin' AFTER outcomeCategory;");
	}
}
