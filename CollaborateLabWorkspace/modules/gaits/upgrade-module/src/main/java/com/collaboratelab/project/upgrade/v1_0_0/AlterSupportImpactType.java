package com.collaboratelab.project.upgrade.v1_0_0;

//import com.collaboratelab.project.upgrade.v1_0_0.util.ImpactSupportTable;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;

public class AlterSupportImpactType extends UpgradeProcess {

	@Override
	protected void doUpgrade() throws Exception {
		// alter() fails on reindex, so use a usual query
		//alter(ImpactSupportTable.class, new AlterColumnType("duration", "INTEGER not null"));
		
		runSQL("ALTER TABLE CLab_Impact_Support DROP COLUMN duration; ");
		runSQL("ALTER TABLE CLab_Impact_Support ADD COLUMN duration INT(11) NOT NULL AFTER startDate;");
		
	}

}
