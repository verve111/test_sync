package com.collaboratelab.project.upgrade.v1_0_2;

import com.collaboratelab.project.constants.Utils;
import com.collaboratelab.project.constants.enums.Enums;
import com.collaboratelab.project.model.Project;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.GroupLocalServiceUtil;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Iterator;

import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.Element;
import com.liferay.portal.kernel.xml.SAXReaderUtil;


public class UpgradeEnableRatings extends UpgradeProcess {
    @Override
    protected void doUpgrade() throws Exception {
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        ResultSet rs2 = null;

        ps = connection.prepareStatement("update PortletPreferences set preferences = ? where plid = ? " +
                "and portletId like '%com_liferay_asset_publisher%'");

        int type = Enums.TypeProject.MEDICAL.getId();
        List<Project> projectsByType = Utils.getProjectsByType(type);

        for (Project pr : projectsByType) {
            long groupId = pr.getGroupId();

            Group group = GroupLocalServiceUtil.fetchGroup(groupId);
            if(group == null)
                continue;

            ps2 = connection.prepareStatement("select pp.plid, pp.preferences from PortletPreferences as pp " +
                    "join Layout on pp.plid=Layout.plid " +
                    "where Layout.groupId = ? and pp.portletId like '%com_liferay_asset_publisher%'");


            ps2.setLong(1, groupId);

            rs2 = ps2.executeQuery();

            while (rs2.next()) {
                long plid = rs2.getLong(1);
                String preferences = rs2.getString(2);

                preferences = setRatingsTrue(preferences); 
                ps.setString(1, preferences);
                ps.setLong(2, plid);

                ps.addBatch();
            }

            ps.executeUpdate();
        }

    }

    private String setRatingsTrue(String preferences) throws Exception {
        Document newDocument = SAXReaderUtil.createDocument();

        Element newRootElement = SAXReaderUtil.createElement(
                "portlet-preferences");

        newDocument.add(newRootElement);

        Document document = SAXReaderUtil.read(preferences);

        Element rootElement = document.getRootElement();

        Iterator<Element> iterator = rootElement.elementIterator();

        while (iterator.hasNext()) {
            Element preferenceElement = iterator.next();
            String preferenceName = preferenceElement.elementText("name");

            if(!preferenceName.contains("enableRatings"))
                newRootElement.add(preferenceElement.createCopy());

        }

        Element element = SAXReaderUtil.createElement("preference");
        element.addElement("name");
        element.addElement("value");
        element.elements().get(0).addEntity("name", "enableRatings");
        element.elements().get(1).addEntity("value", "true");
        newRootElement.add(element);

        return newDocument.asXML().replaceAll("\\<\\?xml(.+?)\\?\\>", "").trim();
    }
}
