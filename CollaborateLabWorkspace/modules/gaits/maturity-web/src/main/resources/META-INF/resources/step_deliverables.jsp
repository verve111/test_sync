<%@ include file="/init.jsp"%>


<%
	Long groupId = themeDisplay.getLayout().getGroupId();
	
	Project project = ProjectLocalServiceUtil.findByGroupId(groupId);

	ProjectStructure projectStructure = ProjectStructureLocalServiceUtil
			.findByProjectId(project.getProjectId());

	List<ProjectPhase> phases = ProjectPhaseLocalServiceUtil
			.findByProjectStructureId(projectStructure.getProjectStructureId());

	List<ProjectDomain> domains = ProjectDomainLocalServiceUtil
			.findByProjectStructureId(projectStructure.getProjectStructureId());



	long phaseId = (long)renderRequest.getAttribute("phaseId");
	long domainId = (long)renderRequest.getAttribute("domainId");
	int phaseEnumId = (int)renderRequest.getAttribute("phaseEnumId");
	int domainEnumId = (int)renderRequest.getAttribute("domainEnumId");

	

	
	boolean isSiteContentReviewerOrAdmin = UserGroupRoleLocalServiceUtil.hasUserGroupRole(user.getUserId(), groupId, RoleConstants.SITE_CONTENT_REVIEWER, true) || permissionChecker.isOmniadmin();

%>

<style>
	.portlet-asset-publisher, #p_p_id_CategoryFiltersPortlet_ {
		display: block !important;
	}
</style>

<div class="container-fluid-1280">
	<div class="row">
		<aui:form method="post" name="fm">
			<div class="col-xl-4 col-md-4">
				<aui:select name="phasesSelect" id="phasesSelect"
					label="Stage">
					<%
						boolean isFlagPhaseSelected = false;
						phases = MaturityUtils.sortPhaseListByArray(phases, Enums.PHASES_ARRAY);
						for (ProjectPhase phaseInner : phases) {
							if(phaseId == phaseInner.getProjectPhaseId()) { 
								isFlagPhaseSelected = true;
							}
					%>
					
					<aui:option  value="<%=phaseInner.getProjectPhaseId()%>" selected="<%= isFlagPhaseSelected %>" >
						<%=LanguageUtil.get(request, phaseInner.getIdentifier())%>
					</aui:option>
					
					<%
						isFlagPhaseSelected = false;
						}
					%>
				</aui:select>
			</div>
			<div class="col-xl-4 col-md-4">
				<aui:select name="domainsSelect" id="domainsSelect"
					label="Domain Select">
					
					<%
						boolean isFlagDomainSelected = false;
						for (ProjectDomain domainInner : domains) {
						
							if(domainId == domainInner.getProjectDomainId()) { 
								isFlagDomainSelected = true;
							}
					%>
					<aui:option  value="<%=domainInner.getProjectDomainId()%>" selected="<%= isFlagDomainSelected %>" >
							<%=LanguageUtil.get(request, domainInner.getIdentifier())%>
					</aui:option>
					<%
							isFlagDomainSelected = false;
						}
					%>
				</aui:select>
			</div>
		</aui:form>
	</div>
</div>


<div class="container-fluid-1280">
	<div class="row">
		<h1><strong>Deliverables:</strong></h1>
		<div class="col-xl-12 col-md-12">
			<%
				ProjectStep step = ProjectStepLocalServiceUtil
						.findByProjectIdProjectDomainIdProjectPhaseId(project.getProjectId(), domainId, phaseId);
				
				List<ProjectDelivery> deliveries = ProjectDeliveryLocalServiceUtil.findByStepId(step.getProjectStepId());
				deliveries = MaturityUtils.sortDeliverablesByName(globalGroupId, deliveries);
			%>
			
			<div class="row">
				<%  int j = 0;
					for(ProjectDelivery delivery : deliveries) {
						String deliveryName = Utils.getNameDeliveryFromJournalArticleContent(globalGroupId,String.valueOf(delivery.getType()));
						String escapedDeliveryName = deliveryName.replace("'", "\\'").replace("\"", "\\'");
						
					      String what = MaturityUtils.getWWHDeliveryFromJournalArticleContent(globalGroupId,String.valueOf(delivery.getType()),"what");
						  String why = MaturityUtils.getWWHDeliveryFromJournalArticleContent(globalGroupId,String.valueOf(delivery.getType()),"why");
						  String how = MaturityUtils.getWWHDeliveryFromJournalArticleContent(globalGroupId,String.valueOf(delivery.getType()),"how");						
				%>
					<div class="col-xl-12 col-md-12">
						<div class="row">
							<h2> <%= deliveryName %> </h2>
						</div>
					     <div class="row">
					     	<ul>
					     		<% if (!what.isEmpty()) { %>
					     			<li><%= what %></li>
					     		<% } %>
					     		<%-- if (!why.isEmpty()) { %>					     		
					     			<li><h3>WHY</h3><%= why %></li>
					     		<% } %>					     			
					     		<% if (!how.isEmpty()) { %>					     		
					     			<li><h3>HOW</h3><%= how %></li>
					     		<% } --%>					     							     							     		
					     	</ul>
					     </div>						
						<liferay-portlet:renderURL var="deliveryRenderURL"
						  portletMode="<%=LiferayPortletMode.VIEW.toString()%>"
						  windowState="<%=LiferayWindowState.NORMAL.toString()%>">
							  <liferay-portlet:param name="jspPage" value="/update_form.jsp" />
							  <liferay-portlet:param name="phaseEnumId" value="<%=String.valueOf(phaseEnumId) %>" />
							  <liferay-portlet:param name="domainEnumId" value="<%=String.valueOf(domainEnumId) %>" />  
							  <liferay-portlet:param name="deliveryId" value="<%=String.valueOf(delivery.getDeliveryId()) %>" />  
							  <liferay-portlet:param name="deliveryNumInMilestone" value="<%=String.valueOf(++j) %>" /> 
							  <liferay-portlet:param name="prevUrl" value="<%=themeDisplay.getURLCurrent()  %>"/>	  
						</liferay-portlet:renderURL>						
						<aui:button-row>
						    <aui:button onClick="<%= deliveryRenderURL.toString() %>" value="Update status"></aui:button>
						     <% if (isSiteContentReviewerOrAdmin) { %>
						   
							    <aui:button onClick="<%= renderResponse.getNamespace() + "showReviewPopup('" 
								    	+ String.valueOf(delivery.getDeliveryId()) + "', '" + escapedDeliveryName + "','" + delivery.getReviewerIsCompleted() + "','" + delivery.getReviewerComment() +"', '" + String.valueOf(delivery.getReviewerRate())+ "')" %>" 
								    	value="Review"></aui:button>

						    <% } %>		    
						</aui:button-row>
					
					</div>
				<% } %>
			</div>
		</div>
	</div>
	<br />
	

	<%
		//catId is needed only to make clicked category grey 
		//long categoryId = ParamUtil.getLong(request, "categoryId");
		

	
	%>
	
	<%--c:set var="domainPhaseCatId" scope="request" value="<%=String.valueOf(domainPhaseCatId)%>"/>
	
	<div class="row">
		<div class="col-md-8">
			<h1><strong>Resources</strong></h1>
					
		<% 
		   if (domainPhaseCatId > 0) { 
		%>
			<liferay-portlet:runtime portletName="com_liferay_asset_publisher_web_portlet_AssetPublisherPortlet" queryString="&catIdToRender=${domainPhaseCatId}"/>
		<% 
		   } else {
		%>
			Please create domain->phase category to see asset publisher
		<% } %>
		</div>
		<div class="col-md-4">
			<h1><strong>Filtering</strong></h1>

			<liferay-util:include page="/components/category_selector.jsp" servletContext="<%= application %>">
				<liferay-util:param name="globalGroupId" value="<%=String.valueOf(globalGroupId) %>" />
				<liferay-util:param name="vocabularyName" value="<%=Utils.PROJECT_MEDICAL_TYPE_VOCABULARY %>" />
				<liferay-util:param name="phaseEnumId" value="<%=String.valueOf(phaseEnumId) %>" />
				<liferay-util:param name="domainEnumId" value="<%=String.valueOf(domainEnumId) %>" />
				<liferay-util:param name="headerText" value="<%="Deliverables (" + Utils.PROJECT_MEDICAL_TYPE_VOCABULARY + ")" %>" />	
			</liferay-util:include> 	
			
			<liferay-util:include page="/components/category_selector.jsp" servletContext="<%= application %>">
				<liferay-util:param name="globalGroupId" value="<%=String.valueOf(globalGroupId) %>" />
				<liferay-util:param name="phaseEnumId" value="<%=String.valueOf(-1) %>" />
				<liferay-util:param name="vocabularyName" value="<%=MaturityUtils.RESOURCE_TYPE_VOCABULARY %>" />
				<!--  catId is needed only to make clicked category grey -->
				<liferay-util:param name="categoryId" value="<%=String.valueOf(categoryId) %>" />
			</liferay-util:include> 

		</div>

		
	</div--%>
	
</div>

<aui:script>

	function <portlet:namespace/>showReviewPopup(deliveryId, deliveryName, isCompleted, comment, rate) {
		AUI().use('aui-base',function(A) {
			var renderUrl = Liferay.PortletURL.createRenderURL();
			renderUrl.setWindowState("<%=LiferayWindowState.POP_UP.toString() %>");
			renderUrl.setParameter("jspPage","/popups/delivery_review.jsp");
			renderUrl.setPortletMode("<%=LiferayPortletMode.VIEW %>");
			renderUrl.setPortletId("<%= themeDisplay.getPortletDisplay().getId() %>");
			renderUrl.setParameter("isCompleted",isCompleted);
			renderUrl.setParameter("deliveryId",deliveryId);
			renderUrl.setParameter("comment",comment);
			renderUrl.setParameter("rate",rate);
			
			
			Liferay.Util.openWindow({ 
				dialog: { 
				 	centered: true,  
				 	width: 640,
				 	height: 485,
				 	modal: true,
				 	constrain: true,
				 	destroyOnClose: true,
				 	destroyOnHide: true,
				 	on: {
				 		destroy: function() { 
				 			//window.location.reload();                   
				 		}
				 	}
				}, 
				id: '<portlet:namespace />dialog',
				title: deliveryName, 
				uri: renderUrl 
			}); 
		});  
	}

	AUI().use('aui-base',function(A) {
			A.one("#<portlet:namespace/>phasesSelect").on(
										'change',
										function() {
											var $ = AUI.$;

											var form = $(document.<portlet:namespace />fm);
											var selectedPhaseValue = A.one('#<portlet:namespace/>phasesSelect').get('value');
											var selectedDomainValue = A.one("#<portlet:namespace/>domainsSelect").get('value');
									
											var renderUrl = Liferay.PortletURL.createRenderURL();
											renderUrl.setWindowState("<%=LiferayWindowState.NORMAL.toString() %>");
											renderUrl.setParameter("phaseId", selectedPhaseValue);
											renderUrl.setParameter("domainId", selectedDomainValue);
											renderUrl.setParameter("CMD", "THREE");
											renderUrl.setParameter("jspPage","/step_deliverables.jsp");
											
											renderUrl.setPortletMode("<%=LiferayPortletMode.VIEW %>");
											renderUrl.setPortletId("<%=themeDisplay.getPortletDisplay().getId() %>");
											
											A.one(document.createElement('a')).attr('href',renderUrl).simulate('click');
										})
										
					});
	
	AUI().use('aui-base',function(A) {
		A.one("#<portlet:namespace/>domainsSelect").on(
									'change',
									function() {
										var $ = AUI.$;

										var form = $(document.<portlet:namespace />fm);
										var selectedPhaseValue = A.one('#<portlet:namespace/>phasesSelect').get('value');
										var selectedDomainValue = A.one("#<portlet:namespace/>domainsSelect").get('value');
								
										var renderUrl = Liferay.PortletURL.createRenderURL();
										renderUrl.setWindowState("<%=LiferayWindowState.NORMAL.toString() %>");
										renderUrl.setParameter("phaseId", selectedPhaseValue);
										renderUrl.setParameter("domainId", selectedDomainValue);
										renderUrl.setParameter("CMD", "THREE");
										renderUrl.setParameter("jspPage","/step_deliverables.jsp");
										
										renderUrl.setPortletMode("<%=LiferayPortletMode.VIEW %>");
										renderUrl.setPortletId("<%=themeDisplay.getPortletDisplay().getId() %>");
										
										A.one(document.createElement('a')).attr('href',renderUrl).simulate('click');
									})
									
				});
	
</aui:script>
<aui:script>	
AUI().ready('aui-base',function(A) {
	var assetPub = A.one('.portlet-asset-publisher');
	if (assetPub) 
		Liferay.Portlet.refresh(assetPub);
})

Liferay.provide(window, 'refreshPortlet', function() {
    var curPortlet = '#p_p_id<portlet:namespace/>';
    Liferay.Portlet.refresh(curPortlet);
 },
  ['aui-dialog','aui-dialog-iframe']
);
</aui:script>
<aui:script>
Liferay.provide(window, 'closePopup', function(dialogId) {
      var A = AUI();
      var dialog = Liferay.Util.Window.getById(dialogId);
     dialog.destroy();
 },
  ['liferay-util-window']
);
</aui:script>