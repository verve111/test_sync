<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %><%@
taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %><%@
taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %><%@
taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<%@ taglib uri="http://liferay.com/tld/asset" prefix="liferay-asset" %>

<%@page import="com.collaboratelab.project.service.ProjectPhaseLocalServiceUtil"%>
<%@page import="com.collaboratelab.project.service.ProjectStepLocalServiceUtil"%>
<%@page import="com.collaboratelab.project.service.ProjectStructureLocalServiceUtil"%>
<%@page import="com.collaboratelab.project.service.ProjectLocalServiceUtil"%>
<%@page import="com.collaboratelab.project.service.ProjectDomainLocalServiceUtil"%>
<%@page import="com.collaboratelab.project.service.ProjectDeliveryLocalServiceUtil"%>

<%@page import="com.liferay.portal.kernel.util.PortalUtil" %>

<%@page import="com.collaboratelab.project.model.Project"%>
<%@page import="com.collaboratelab.project.model.ProjectDomain"%>
<%@page import="com.collaboratelab.project.model.ProjectPhase"%>
<%@page import="com.collaboratelab.project.model.ProjectStep"%>
<%@page import="com.collaboratelab.project.model.ProjectStructure"%>
<%@page import="com.collaboratelab.project.model.ProjectDelivery"%>
<%@page import="com.collaboratelab.constants.MaturityDisplayPortletKeys "%> 
<%@page import="com.collaboratelab.project.constants.Utils"%> 
<%@page import="com.collaboratelab.portlet.MaturityDisplayPortletInstanceConfiguration "%> 
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="java.lang.Enum"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="com.liferay.taglib.aui.AUIUtil"%>

<%@page import="com.collaboratelab.project.constants.Constants"%>

<%@page import="com.collaboratelab.project.constants.enums.Enums"%>

<%@page import="com.liferay.portal.kernel.util.CalendarFactoryUtil"%>
<%@page import="com.collaboratelab.MaturityUtils"%>
<%@page import="com.liferay.asset.kernel.model.AssetCategory"%>
<%@page import="com.liferay.asset.kernel.model.AssetVocabulary"%>
<%@page import="javax.portlet.PortletURL" %>

<%@page import="com.liferay.portal.kernel.dao.orm.DynamicQuery"%>
<%@page import="com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.util.PortalClassLoaderUtil"%>

<%@page import="com.liferay.portal.kernel.portlet.LiferayPortletMode"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>

<%@page import="com.liferay.document.library.kernel.model.DLFolder"%>
<%@page import="com.liferay.document.library.kernel.service.DLFolderLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.util.PortletKeys"%>

<%@page import="com.liferay.portal.kernel.model.RoleConstants" %>
<%@page import="com.liferay.portal.kernel.service.RoleLocalServiceUtil" %>
<%@page import="com.liferay.document.library.kernel.service.DLFileEntryLocalServiceUtil"%>
<%@page import="com.liferay.document.library.kernel.model.DLFileEntry"%>

<%@page import="com.liferay.portal.kernel.portlet.LiferayPortletMode"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="com.liferay.portal.kernel.util.StringPool" %>
<%@page import="com.liferay.portal.kernel.util.Validator" %>
<%@page import="com.liferay.portal.kernel.service.UserLocalServiceUtil" %>

<%@page import="com.liferay.portal.kernel.service.UserGroupRoleLocalServiceUtil" %>

<%@page import="com.collaboratelab.project.exception.NoSuchProjectDomainException"%>
<%@page import="com.collaboratelab.project.exception.NoSuchProjectPhaseException"%>
<%@page import="com.liferay.portal.kernel.exception.SystemException"%>

<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.liferay.portal.kernel.util.DateUtil"%>
<%@page import="com.liferay.portal.kernel.util.LocaleUtil"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="javax.portlet.PortletSession"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<style>
	.portlet-asset-publisher, #p_p_id_CategoryFiltersPortlet_ {
		display: none;
	}
</style>

<%
	MaturityDisplayPortletInstanceConfiguration _portletConfiguration = 
		(MaturityDisplayPortletInstanceConfiguration) renderRequest.getAttribute(MaturityDisplayPortletInstanceConfiguration.class.getName());
	String milestoneDeliverableJournalArticleId = StringPool.BLANK;
    
	if (Validator.isNotNull(_portletConfiguration)) {
		milestoneDeliverableJournalArticleId =
                portletPreferences.getValue(
                    "milestoneDeliverableJournalArticleId", _portletConfiguration.milestoneDeliverableJournalArticleId());	
	}
	long companyId = themeDisplay.getCompanyId();
	long globalGroupId = Utils.getGlobalGroupId(companyId);
	Locale locale2 = LocaleUtil.getSiteDefault();
%>
