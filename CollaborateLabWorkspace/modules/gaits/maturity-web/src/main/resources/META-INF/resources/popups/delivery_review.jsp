<%@ include file="/init.jsp" %>
<style>
     .hidden { visibility: hidden; }
     .visible { visibility: visible; }
     
	.range-labels {
	  margin: -26px -38px 0;
	  padding: 0;
	  list-style: none;
	}
	.range-labels li {
	  position: relative;
	  float: left;
	  width: 95px;
	  text-align: center;
	  color: #b2b2b2;
	  font-size: 18px;
	  /*cursor: pointer;*/
	}
</style>


<%
	String comment = "";
	boolean isCompleted = false;
	int rate = 0;
	long deliveryId = 0L;
	if (request.getParameter("deliveryId") != null)
		deliveryId = Long.valueOf(request.getParameter("deliveryId"));
	if (request.getParameter("rate") != null)
		rate = Integer.valueOf(request.getParameter("rate"));
	if (request.getParameter("isCompleted") != null)
		isCompleted = Boolean.valueOf(request.getParameter("isCompleted"));
	if (request.getParameter("comment") != null)
		comment = String.valueOf(request.getParameter("comment"));
	
%>

<liferay-portlet:actionURL name="saveReview" var="saveReviewURL">

</liferay-portlet:actionURL>

<div style="padding:40px;text-transform:uppercase;">
	<aui:form method="post" name="fm" action="<%= saveReviewURL.toString() %>">
		<aui:input name="deliveryId" type="hidden"
					value="<%=deliveryId%>" />
		<strong>Is this complete?</strong>
		<br><br>
		<aui:field-wrapper name="review_radio">
	        <aui:input name="review_radio"  checked="<%= (isCompleted) %>" type="radio" value="1" label="YES" onClick="showDiv('markDiv')" />
	        <br>
	        <aui:input name="review_radio" checked="<%= (!isCompleted) %>" type="radio" value="4" label="NO" onClick="hideDiv('markDiv')"/>
	    </aui:field-wrapper>
	    <br />
	    <div  class="<%= (isCompleted) ? "visible" : "hidden" %>" id="markDiv">
	        <aui:input type="range" name="mark" label="" min="0" max="4" step="1" value="<%= rate %>" style="width: 400px;"/>
			<ul class="range-labels">
			  <li class="active selected">1</li>
			  <li>2</li>
			  <li>3</li>
			  <li>4</li>
			  <li>5</li>
			</ul>         
	    </div>    
	    <br />
	   	<br />
	    <aui:input name="comment"  type="textarea" value="<%= comment %>"/>
	    <br />	    
	    <aui:button id="saveReview"
						class="aui-button aui-button-primary right" value="SAVE" />
    </aui:form>
</div>


<script type="text/javascript">
      function showDiv(divID) {
         var item = document.getElementById(divID);
         if (item) {
             item.className='visible';
         }
      }

     function hideDiv(divID) {
         var item = document.getElementById(divID);
         if (item) {
            item.className='hidden';
         }
     }

  
</script>

<aui:script use="aui-base,aui-io-request">
A.one('#<portlet:namespace/>saveReview').on('click', function(event) {
        var A = AUI();
        var url = '<%=saveReviewURL.toString()%>';
      A.io.request(
          url,
            {
             method: 'POST',
                form: {id: '<portlet:namespace/>fm'},
             	on: {
                   success: function() {
                     Liferay.Util.getOpener().refreshPortlet();
                     Liferay.Util.getOpener().closePopup('<portlet:namespace/>dialog');
                }
              }
          }
       );
   });

</aui:script>