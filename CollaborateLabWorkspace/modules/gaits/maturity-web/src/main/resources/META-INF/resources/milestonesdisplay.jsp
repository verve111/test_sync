<%@ include file="/init.jsp"%>


<% 
	Long groupId = themeDisplay.getLayout().getGroupId();
	
	Project project = ProjectLocalServiceUtil.findByGroupId(groupId);

	ProjectStructure projectStructure = ProjectStructureLocalServiceUtil.findByProjectId(project.getProjectId());
	
	boolean isSiteContentReviewerOrAdmin = UserGroupRoleLocalServiceUtil.hasUserGroupRole(user.getUserId(), groupId, RoleConstants.SITE_CONTENT_REVIEWER, true) || permissionChecker.isOmniadmin();
%>


<div class="container-fluid-1280">
	<div class="row">
		<%
		for(String d : Enums.DOMAIN_ARRAY) {
			ProjectDomain domain = ProjectDomainLocalServiceUtil
					.findByProjectStructureIdIdentifier(projectStructure.getProjectStructureId(), d);
		%>
			
				<h1><%= d %></h1>
				<table class="table table-bordered">
				    <thead>
				      <tr>
				        <th>DELIVERABLES PER MILESTONES</th>
				        <th>PROGRESS</th>
				        <th>START</th>
				        <th>END</th>
				        <th>OUTCOME</th>
				        <th>COMMENTS</th>
				        <% if (isSiteContentReviewerOrAdmin) { %>
					        <th>REVIEW IS COMPLETED?</th>
					        <th>REVIEW RATE</th>
					        <th>REVIEW COMMENT</th>
				        <%} %>
				      </tr>
				    </thead>
				    <tbody>
				    <% for(String p : Enums.PHASES_ARRAY)  { 
				    	ProjectPhase phase = ProjectPhaseLocalServiceUtil.findByProjectStructureIdIdentifier(
								projectStructure.getProjectStructureId(), p);
				    	ProjectStep step = ProjectStepLocalServiceUtil.findByProjectIdProjectDomainIdProjectPhaseId(project.getProjectId(), domain.getProjectDomainId(), phase.getProjectPhaseId());
				    	
				    	List<ProjectDelivery> delivers = ProjectDeliveryLocalServiceUtil.findByStepId(step.getProjectStepId());
				    
				    %> 
					  <tr>
					    <td>
					    <strong><%= p %> </strong><br>
					    <%
					  
			
					    	for(ProjectDelivery del : delivers)
					    	{
					    %>
					    		<%= Utils.getNameDeliveryFromJournalArticleContent(globalGroupId,String.valueOf(del.getType())) %><br />
					   	<% } %>
					    </td>
					     <td><br>
					    	<%
					    	for(ProjectDelivery del : delivers)
					    	{
					   		%>
					    		<%= del.getStatus() %><br />
					   		<% } %>
					    </td>	
					    <td><br>
					    	<%
					    	for(ProjectDelivery del : delivers)
					    	{
					    		if(del.getStartDate() != null) {
					    			
					    			String datestr = Utils.getDateByLocale(del.getStartDate(),locale2);
					    			
					   		%>
					    		<%= datestr %><br />
					   		<% 
					    		}
					    	} %>
					    </td>	
					    <td><br>
					    	<%
					    	for(ProjectDelivery del : delivers)
					    	{
					    		if(del.getEndDate() != null) {
					    			
					    			String datestr = Utils.getDateByLocale(del.getEndDate(),locale2);
					    			
					   		%>
					    		<%= datestr %><br />
					   		<% 
					    		}
					    	} %>
					    </td>	
					     <td><br>
					    	<%
					    	for(ProjectDelivery del : delivers)
					    	{
					   		%>
					    		<%= Enums.OutcomeCategory.getNameById(del.getOutcomeCategory()) %><br />
					   		<% } %>
					    </td>
					    <td><br>
					    	<%
					    	for(ProjectDelivery del : delivers)
					    	{
					   		%>
					    		<%= del.getComment() %><br />
					   		<% } %>
					    </td>
							<% if (isSiteContentReviewerOrAdmin) { %>	    	
								    <td><br>
								    	<%
								    	for(ProjectDelivery del : delivers)
								    	{
								    		if(del.getReviewerIsCompleted()) {
								   		%>
								    		<%= "YES" %><br />
								   		<% }else { %>
								   			<%= "NO" %><br />
								   		<%}
								   		
								    		} %>
								    </td>
								    <td><br>
								    	<%
								    	for(ProjectDelivery del : delivers)
								    	{
								   		%>
								    		<%= String.valueOf(del.getReviewerRate() + 1) %><br />
								   		<% } %>
								    </td>
								      <td><br>
								    	<%
								    	for(ProjectDelivery del : delivers)
								    	{
								   		%>
								    		<%= del.getReviewerComment() %><br />
								   		<% } %>
								    </td>	
						    <%} %>		   
					  </tr>
					  <% }%> 
				    </tbody>
				  </table>
			 
		 <%
			}
		 %>
		</div>
</div>
