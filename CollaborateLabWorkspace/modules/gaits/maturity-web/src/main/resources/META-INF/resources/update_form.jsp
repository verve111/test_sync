<%@ include file="init.jsp"%>

<%
	Object delId = (Object) renderRequest.getParameter("deliveryId");
	Object prId = (Object) renderRequest.getParameter("projectId");
	Long deliveryId = 0L;

	ProjectDelivery delivery = null;
	ProjectStep step = null;
	Calendar today = Calendar.getInstance();
	Date startDate = null;
	Date endDate = null;
	Date startPlanningDate = null;
	Date endPlanningDate = null;
	Long projectId = 0L;
	if (delId != null) {
		deliveryId = Long.parseLong(String.valueOf(delId));
		delivery = ProjectDeliveryLocalServiceUtil.getProjectDelivery(deliveryId);
		step = ProjectStepLocalServiceUtil.getProjectStep(delivery.getStepId());
		startDate = ((Date) delivery.getStartDate() != null) ? (Date) delivery.getStartDate() : null;
		endDate = ((Date) delivery.getEndDate() != null) ? (Date) delivery.getEndDate() : null;
		startPlanningDate = ((Date) delivery.getStartPlanningDate() != null)
				? (Date) delivery.getStartPlanningDate()
				: null;
		endPlanningDate = ((Date) delivery.getEndPlanningDate() != null)
				? (Date) delivery.getEndPlanningDate()
				: null;
		projectId = Long.valueOf(step.getProjectId());
	}

	Calendar startCal = Calendar.getInstance();
	startCal.setTime(startDate == null ? new Date() : startDate);
	Calendar endCal = Calendar.getInstance();
	endCal.setTime(endDate == null ? new Date() : endDate);

	Calendar startPlanningCal = Calendar.getInstance();
	startPlanningCal.setTime(startPlanningDate == null ? new Date() : startPlanningDate);

	Calendar endPlanningCal = Calendar.getInstance();
	endPlanningCal.setTime(endPlanningDate == null ? new Date() : endPlanningDate);

	int phaseEnumId = ParamUtil.getInteger(request, "phaseEnumId");
	int domainEnumId = ParamUtil.getInteger(request, "domainEnumId");
	String prevUrl = ParamUtil.getString(request, "prevUrl");
	int deliveryNumInMilestone = ParamUtil.getInteger(request, "deliveryNumInMilestone");
	String domainName = Enums.DOMAIN_ENUM.values()[domainEnumId].toString();
	String phaseName = Enums.PHASES_ARRAY[phaseEnumId];

	String templateKey = Utils.getDDMTemplateKeyDeliveryWWH(globalGroupId);
	String structureKey = Utils.getDDMStructureKeyDeliveryWWH(globalGroupId);

	String portletId = PortletKeys.DOCUMENT_LIBRARY;

	boolean isAdmin = UserGroupRoleLocalServiceUtil.hasUserGroupRole(themeDisplay.getUserId(),
			themeDisplay.getScopeGroupId(), RoleConstants.SITE_ADMINISTRATOR, true)
			|| permissionChecker.isOmniadmin();
	
	boolean isPlanner = UserGroupRoleLocalServiceUtil.hasUserGroupRole(themeDisplay.getUserId(),
			themeDisplay.getScopeGroupId(), "Planner", true);
	
	boolean isSiteMember = UserLocalServiceUtil.hasGroupUser(themeDisplay.getScopeGroupId(), themeDisplay.getUserId());
	boolean isReadonlyPlanning = true;
	
	if(delivery != null && isSiteMember) {
		isReadonlyPlanning = delivery.getIsEditPlanning();
	}

%>

<liferay-portlet:actionURL name="saveBack" var="saveBackURL">
	<portlet:param name="redirect" value="<%=prevUrl%>" />
</liferay-portlet:actionURL>
<liferay-portlet:actionURL name="saveContinue" var="saveContinueURL">
</liferay-portlet:actionURL>


<style>
.panel {
	border-width: 0;
	box-shadow: none;
}

.portlet-asset-publisher, #p_p_id_CategoryFiltersPortlet_ {
	display: block !important;
}
</style>

<div class="container-fluid-1280">
	<%--div class="row">
		<div class="col-md-11">
			<h3>
				<strong>Tracking Deliverables & Impact:</strong> Deliveries are captured in a secure team site and tracked during and post-funding
			</h3>
		</div>
		<div class="col-md-1 right">
			<h1><%=step.getPercentage()%></h1>
		</div>
	</div--%>
	<div class="row" style="border: 1px solid #ccc; padding: 10px;">
		<div class="col-md-12">
			<h4>
				MILESTONE:
				<%=phaseName%>, DOMAIN:
				<%=domainName%></h4>
		</div>
	</div>
	<br /> <br />

	<div class="container-fluid-1280">
		<div class="row">
			<div class="col-md-12 left">
				<div class="row">
					<h1>
						<strong>Deliverable: <%=Utils.getNameDeliveryFromJournalArticleContent(globalGroupId, String.valueOf(delivery.getType()))%></strong>
					</h1>
				</div>
			</div>
		</div>
		<br /> <br />
		<%
			String what = MaturityUtils.getWWHDeliveryFromJournalArticleContent(globalGroupId,
					String.valueOf(delivery.getType()), "what");
			String why = MaturityUtils.getWWHDeliveryFromJournalArticleContent(globalGroupId,
					String.valueOf(delivery.getType()), "why");
			String how = MaturityUtils.getWWHDeliveryFromJournalArticleContent(globalGroupId,
					String.valueOf(delivery.getType()), "how");

			/* if(!Validator.isNull(templateKey) && !Validator.isNull(structureKey)) {
			try {
			  html = Utils.fetchHTMLMainJournalArticle(themeDisplay,templateKey,globalGroupId, Constants.DDM_STRUCTURE_WHAT_WHY_HOW_NAME + "_", String.valueOf(delivery.getType()));
			} catch (Exception e) {
			  // ignore
			}
			 } */
		%>
		<div class="row">
			<div class="col-md-4">
				<h3>WHAT</h3>
				<%=what%>
			</div>
			<div class="col-md-4">
				<h3>WHY</h3>
				<%=why%>
			</div>
			<div class="col-md-4">
				<h3>HOW</h3>
				<%=how%>
			</div>
		</div>
	</div>

	<br /> <br />
	<div class="container-fluid-1280">


		<aui:form method="post" name="fm" action="<%= saveBackURL %>" >
			<aui:input name="deliveryId" type="hidden" value="<%=deliveryId%>" />
			<aui:input name="projectId" type="hidden"
				value="<%=Long.parseLong(String.valueOf(projectId))%>" />
			<c:if test="<%=isAdmin || isPlanner || isSiteMember%>">
				<div class="panel panel-default">
					<div class="row " style="border: 1px solid #ccc; padding: 10px;">
						<div class="panel-heading" role="tab" id="headingTwo">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse" href="#collapseTwo"
									aria-expanded="true" aria-controls="collapseTwo"
									class="trigger collapsed"> Planning </a>
							</h4>
						</div>
						<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel"
							aria-labelledby="headingTwo">
							
							<div class="panel-body">
								<c:if test="<%=isAdmin%>">
									<div class="row" style="padding: 10px;">
									<aui:input name="deliverableViewSiteMembers" id="checkDeliverableViewSiteMembers"
										label="DISABLE EDIT FOR SITE MEMBERS" type="checkbox" value="<%= delivery.getIsEditPlanning() %>" />
									</div>
								</c:if>
								<div class="col-md-6">
									
									<% if(!isReadonlyPlanning || isAdmin) {%>
										<aui:fieldset>
											<aui:field-wrapper label="START DATE">
											</aui:field-wrapper>
										</aui:fieldset>
											<%--liferay-ui:input-date name="startPlanningDate"  nullable="true" showDisableCheckbox="true"/--%>
											
											<!-- liferay-ui:input-date is replaced by js func to make it custom customize, 
												e.g. empty on start, filled in on click  -->
											<aui:input
											    type="text"
											    id="startPlanningDate"
											    name="startPlanningDate"
												value="<%=startPlanningDate == null ? "" : MaturityUtils.getDateAsStr(startPlanningCal) %>"
											
												placeholder="mm/dd/YYYY"/>											    
											    
											<style>
												label[for="<portlet:namespace/>startPlanningDate"] {
													display: none;
												}
											</style>

									<% } else { %>
											<aui:input 
												id="viewStartDate" name="viewStartDate" label="START DATE" readonly="true"
												value="<%= startPlanningDate == null ? "" : 
													MaturityUtils.getDateAsStr(startPlanningCal)%>" />
									<% } %>
									<br />
									
									<% if(!isReadonlyPlanning || isAdmin) {%>
											<aui:input label="Responsible Person" name="responsiblePerson"
											id="responsiblePerson"
											value="<%=delivery.getPersonResponsible()%>" />
	
											<aui:input label="Budget"  name="budget" id="budget"
											value="<%=delivery.getBudget()%>" />
									<% }else { %>
											<aui:input label="Responsible Person" name="responsiblePerson"
											id="responsiblePerson" readonly="true"
											value="<%=delivery.getPersonResponsible()%>" />
	
											<aui:input label="Budget"  name="budget" readonly="true" id="budget"
											value="<%=delivery.getBudget()%>" />
									<%} %>
								</div>

								<div class="col-md-6">
									
									<% if(!isReadonlyPlanning || isAdmin) {%>
										<aui:fieldset>
											<aui:field-wrapper label="END DATE">
											</aui:field-wrapper>
										</aui:fieldset>
										<% if (endPlanningDate != null) { %>
											<liferay-ui:input-date name="endPlanningDate" 
												dayValue="<%=endPlanningCal.get(Calendar.DAY_OF_MONTH)%>"
												dayParam="endPlanningDay"
												monthValue="<%=endPlanningCal.get(Calendar.MONTH)%>"
												monthParam="endPlanningMonth"
												yearValue="<%=endPlanningCal.get(Calendar.YEAR)%>"
												yearParam="endPlanningYear" />
										<% } else { %>
											<liferay-ui:input-date name="endPlanningDate" nullable="true" showDisableCheckbox="true"/>
										<% } %>
									<% } else { %>
											<aui:input label="END DATE"  readonly="true" 
												id="viewEndDate" name="viewEndDate"
												value="<%= endPlanningDate == null ? "" : 
													MaturityUtils.getDateAsStr(endPlanningCal)%>" />
									<% } %>
									<br>

									<%
										DLFolder dlParentPlanningFolder = DLFolderLocalServiceUtil
														.fetchFolder(themeDisplay.getScopeGroupId(), 0, Utils.DELIVERABLE_FILES_FOLDER);
												long deliveryPlanningFolderId = 0;

												if (dlParentPlanningFolder != null) {
													String folderTitle = MaturityUtils.getDeliverableTitle(globalGroupId, delivery) + Utils.DELIVERABLE_PLANNING_POSTFIX;
													DLFolder deliveryFolder = DLFolderLocalServiceUtil.fetchFolder(themeDisplay.getScopeGroupId(),
															dlParentPlanningFolder.getFolderId(), folderTitle);
													if (deliveryFolder != null) {
														deliveryPlanningFolderId = deliveryFolder.getFolderId();
													}
												}
									%>

									<%
										if (deliveryPlanningFolderId > 0) {
									%>
									<liferay-portlet:renderURL portletName="<%=portletId%>"
										var="viewPlanningFolderURL"
										windowState="<%=LiferayWindowState.POP_UP.toString()%>">
										<portlet:param name="struts_action"
											value='/document_library/view' />
										<portlet:param name="folderId"
											value='<%=String.valueOf(deliveryPlanningFolderId)%>' />
									</liferay-portlet:renderURL>

									<a
										onclick="callPopup('<%=viewPlanningFolderURL.toString()%> ')">
										PREVIEW </a>
									<%
										}
									%>
									<% if(!isReadonlyPlanning || isAdmin) {%>
									<aui:input type="file"  name="filePlanning" id="filePlanning"
										label="Upload Deliverable Plan" />
									<% } %>


								</div>
								<div class="col-md-6">
									<% if(!isReadonlyPlanning || isAdmin ) {%>
									<aui:select name="funding"  id="fundingSelect"
										label="Funding Source">

										<%
											boolean isFundingFlag = false;

														for (Enums.Funding f : Enums.Funding.values()) {
															if (f.getId() == delivery.getFundingSource())
																isFundingFlag = true;
										%>
										<aui:option selected="<%=isFundingFlag%>"
											value="<%=f.getId()%>"><%=f.getName()%></aui:option>
										<%
											isFundingFlag = false;
														}
										%>
									</aui:select>
									<% } else { 
										String funding = "";

										for (Enums.Funding f : Enums.Funding.values()) {
											if (f.getId() == delivery.getFundingSource())
												funding = f.getName();	
										}
									%>
											<aui:input name="funding" readonly="true" label="Funding Source" 
										id="funding" value="<%=funding%>" />
									<% } %>
								</div>
								<br />
							</div>
							<div class="row text-right" style="padding-right: 30px;">
						<!-- refresh page to clear date calendars that make the page slow after submit -->							
								<aui:button type="submit" id="saveBack1"  onClick="setTimeout(function(){window.location.reload();},10)"
									class="aui-button aui-button-primary" value="SAVE & BACK" />
								<aui:button onclick="resetFirst();"
									class="aui-button" value="RESET" />						
							</div>								
						</div>					
					</div>
				</div>
			</c:if>
			<br />
			<br />
			<div class="panel panel-default">
				<div class="row " style="border: 1px solid #ccc; padding: 10px;">
					<div class="panel-heading" role="tab" id="headingOne">
						<h4 class="panel-title">
							<a role="button" data-toggle="collapse" href="#collapseOne"
								aria-expanded="true" aria-controls="collapseOne"
								class="trigger collapsed">
								Tracking </a>
						</h4>
					</div>
					<div id="collapseOne" class="panel-collapse collapse"
						role="tabpanel" aria-labelledby="headingOne">
						<div class="panel-body">
							<div class="row" style="padding: 10px;">
                                <!-- TODO: available -> false, unavailable -> true, to change -->
								<aui:input name="deliverableNA" id="checkDeliverableNA"
									label="DELIVERABLE N/A" type="checkbox" value="<%=delivery.getIsAvailable()%>"/>
							</div>
							<div class="col-md-6">
								<aui:select name="status" id="statusSelect" label="STATUS" onChange="statusChanged(this.value, this);">
									<%
										boolean isStatusFlag = false;
												for (Enums.Status status : Enums.Status.values()) {
													if (status.getId() == delivery.getStatus())
														isStatusFlag = true;
									%>
									<aui:option selected="<%=isStatusFlag%>"
										value="<%=status.getId()%>"><%=status.getName()%></aui:option>
									<%
										isStatusFlag = false;
												}
									%>
								</aui:select>
								<aui:fieldset>
									<aui:field-wrapper label="START DATE">
									</aui:field-wrapper>
								</aui:fieldset>



									<%--liferay-ui:input-date name="startPlanningDate"  nullable="true" showDisableCheckbox="true"/--%>
									
									<!-- liferay-ui:input-date is replaced by js func to make it custom customize, 
										e.g. empty on start, filled in on click  -->
									<aui:input
									    type="text"
									    id="startDate"
									    name="startDate"
										value="<%=startDate == null ? "" : MaturityUtils.getDateAsStr(startCal) %>"									    
									    placeholder="mm/dd/YYYY"/>
									    
									<style>
										label[for="<portlet:namespace/>startDate"] {
											display: none;
										}
									</style>
									

								<br />

								<aui:input label="ESTIMATED COST" name="estimatedCost" id="cost"
									value="<%=delivery.getCost()%>" />
							</div>

							<div class="col-md-6">
								<aui:select name="outcomeCategory" id="outcomeCategorySelect"
									label="OUTCOME CATEGORY">

									<%
										boolean isOutcomeCategoryFlag = false;

												for (Enums.OutcomeCategory cat : Enums.OutcomeCategory.values()) {
													if (cat.getId() == delivery.getOutcomeCategory())
														isOutcomeCategoryFlag = true;
									%>
									<aui:option selected="<%=isOutcomeCategoryFlag%>"
										value="<%=cat.getId()%>"><%=cat.getName()%></aui:option>
									<%
										isOutcomeCategoryFlag = false;
												}
									%>
								</aui:select>
								<aui:fieldset>
									<aui:field-wrapper label="END DATE">
									</aui:field-wrapper>
								</aui:fieldset>


									<%--liferay-ui:input-date name="endDate"  nullable="true" showDisableCheckbox="true"/--%>
									
									<aui:input
									    type="text"
									    id="endDate"
									    name="endDate"
										value="<%=endDate == null ? "" : MaturityUtils.getDateAsStr(endCal) %>"									    
									    placeholder="mm/dd/YYYY"/>
									    
									<style>
										label[for="<portlet:namespace/>endDate"] {
											display: none;
										}
									</style>									

								<%
									DLFolder dlParentFolder = DLFolderLocalServiceUtil.fetchFolder(themeDisplay.getScopeGroupId(), 0,
											Utils.DELIVERABLE_FILES_FOLDER);
										long deliveryFolderId = 0;
										if (dlParentFolder != null) {
											String folderTitle = MaturityUtils.getDeliverableTitle(globalGroupId, delivery);
											DLFolder deliveryFolder = DLFolderLocalServiceUtil.fetchFolder(themeDisplay.getScopeGroupId(),
													dlParentFolder.getFolderId(), folderTitle);
											if (deliveryFolder != null) {
												deliveryFolderId = deliveryFolder.getFolderId();
											}
										}
								%>

								<%
									if (deliveryFolderId > 0) {
								%>
								<liferay-portlet:renderURL portletName="<%=portletId%>"
									var="viewFolderURL"
									windowState="<%=LiferayWindowState.POP_UP.toString()%>">
									<portlet:param name="struts_action"
										value='/document_library/view' />
									<portlet:param name="folderId"
										value='<%=String.valueOf(deliveryFolderId)%>' />
								</liferay-portlet:renderURL>

								<a onclick="callPopup('<%=viewFolderURL.toString()%> ')">
									PREVIEW </a>
								<%
									}
								%>
								<aui:input type="file" name="file" id="file" label="FILE UPLOAD" />

							</div>
							<div class="col-md-12">
								<div class="left col-md-12">
									<div class="row">
										<aui:input type="textarea" name="comment" id="comment" label="COMMENT ON NOTEWORTHY ISSUES"
											value="<%=delivery.getComment()%>" />
									</div>
								</div>
							</div>
						</div>
						<div class="row text-right" style="padding-right: 30px;">
						<!-- refresh page to clear date calendars that make the page slow after submit -->
							<aui:button type="submit" id="saveBack" onClick="setTimeout(function(){window.location.reload();},10)"
								class="aui-button aui-button-primary" value="SAVE & BACK" />
							<aui:button id="reset" onclick="reset2();"
								class="aui-button" value="RESET" />						
						</div>						
					</div>
				</div>

			</div>
			<br />


		</aui:form>
	</div>

	<%
		// this categoryId is used within AssetPublisher hook
		long deliverableCatId = Utils.getCategoryByDomainAndPhaseAndDeliverable(globalGroupId, domainEnumId,
				phaseEnumId, deliveryNumInMilestone);
		portletSession.setAttribute("catIdToRender", String.valueOf(deliverableCatId),
				PortletSession.APPLICATION_SCOPE);
	%>
</div>

<aui:script>

	function resetFirst() {
		AUI().one('#<portlet:namespace/>responsiblePerson').set('value', '');
		AUI().one('#<portlet:namespace/>filePlanning').set('value', null);
		AUI().one('#<portlet:namespace/>budget').set('value', 0);
		AUI().one('#<portlet:namespace/>startPlanningDate').set('value', null);
		AUI().one('#<portlet:namespace/>endPlanningDate').set('value', null);	
		AUI().one('#<portlet:namespace/>fundingSelect').set('value', 0);		
	}

	function reset2() {
		A = AUI();
		A.one('#<portlet:namespace/>statusSelect').set('value', 0);
		A.one('#<portlet:namespace/>cost').set('value', 0);
		A.one('#<portlet:namespace/>outcomeCategorySelect').set('value', 0);
		A.one('#<portlet:namespace/>file').set('value', null);
		A.one('#<portlet:namespace/>comment').set('value', '');
		A.one('#<portlet:namespace/>startDate').set('value', null);
		A.one('#<portlet:namespace/>endDate').set('value', null);		
	}


	//asset pub 
	AUI().ready('aui-base', function(A) {
		var assetPub = A.one('.portlet-asset-publisher');
		if (assetPub)
			Liferay.Portlet.refresh(assetPub);
	})


	// calendars and dates
    AUI().use(
        'aui-datepicker',
        function(A) {
        	var f = function(pickerName) {
	            var dp = new A.DatePicker(
	                {
	                    trigger: '#<portlet:namespace/>' + pickerName,
	                    popover: {
	                        zIndex: 1000
	                    }
	                }
	            );        	
             	var date = A.one('#<portlet:namespace/>' + pickerName);
             	if (!date) {
             		// return if readonly field
             		return;
             	}
	            date.datePicker = dp; 
	            if (pickerName === 'endDate') {
	            	return;
	            }
				date.on('click', function(event) {
					focusedDate = event.currentTarget;
					if(event.currentTarget.val().length == 0) {
						event.currentTarget.datePicker.clearSelection(true);
						event.currentTarget.datePicker.selectDates(new Date());
					}
				})         		 
        	}
        	
        	f('startPlanningDate');
        	f('startDate');
        	f('endDate');
        }
    );	
    
    function statusChanged(value, combo) {
    // if completed
    	if (value == 10) {
    		var el = AUI().one('#<portlet:namespace/>endDate');
    		if (el) {
    			el.focus();
    			el.datePicker.clearSelection(true);
    			el.datePicker.selectDates(new Date());
    			combo.focus();
    		}
    	}
    } 

	function callPopup(url) {
		var url1 = url;
		Liferay.Util.openWindow({
			dialog : {

				destroyOnHide : true,
				height : 650,
				width : 550
			},
			dialogIframe : {

			},
			title : 'FOLDER',
			uri : url1
		});
	}

	
</aui:script>