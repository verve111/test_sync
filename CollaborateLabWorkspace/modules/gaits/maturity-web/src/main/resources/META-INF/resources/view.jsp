<%@ include file="/init.jsp"%>

<%
	Long groupId = themeDisplay.getLayout().getGroupId();

	Project project = ProjectLocalServiceUtil.findByGroupId(groupId);

	ProjectStructure projectStructure = ProjectStructureLocalServiceUtil
			.findByProjectId(project.getProjectId());

	List<ProjectPhase> phases = ProjectPhaseLocalServiceUtil
			.findByProjectStructureId(projectStructure.getProjectStructureId());
	
	List<ProjectDomain> domains = ProjectDomainLocalServiceUtil
			.findByProjectStructureId(projectStructure.getProjectStructureId());
	
	String[] strs= Utils.getColorsForSteps(project.getProjectId(), projectStructure.getProjectStructureId());
	StringBuilder str = new StringBuilder();
	for(String s: strs)
		str.append(s);
	HashMap<String[], String> outcameCatMap = new HashMap<String[], String>();
	for(ProjectPhase ph :  phases) {
		for(ProjectDomain dom :  domains) {
				ProjectStep step = ProjectStepLocalServiceUtil.findByProjectIdProjectDomainIdProjectPhaseId(project.getProjectId(), dom.getProjectDomainId(), ph.getProjectPhaseId());
				//System.out.println(ph.getIdentifier() + " " + dom.getIdentifier());
				List<ProjectDelivery> del = ProjectDeliveryLocalServiceUtil.findByStepId(step.getProjectStepId());
				del = MaturityUtils.sortDeliverablesByName(globalGroupId, del);
				if(del.size() != 0) {
					ProjectDelivery d = del.get(0);
					String phaseRen = ph.getIdentifier().replace("&", "AND");
					String domRen = dom.getIdentifier().replace("/", "");
					if(d.getOutcomeCategory() == 1)
						outcameCatMap.put(new String[]{phaseRen, domRen},"mark-circle");
					if(d.getOutcomeCategory() == 2)
						outcameCatMap.put(new String[]{phaseRen, domRen},"mark-square");
					if(d.getOutcomeCategory() == 3)
						outcameCatMap.put(new String[]{phaseRen, domRen},"mark-diamond");
					if(d.getOutcomeCategory() == 4 || d.getOutcomeCategory() == 0)
						outcameCatMap.put(new String[]{phaseRen, domRen},"-");
				}
				
			}
	}
	StringBuilder builder = new StringBuilder();
	for(String[] t :outcameCatMap.keySet()) {
		builder.append(t[0].toString()+"-"+t[1].toString()+","+String.valueOf(outcameCatMap.get(t)));
		builder.append(";");
	}
	//System.out.println(Utils.getColorsForSteps(project.getProjectId(), projectStructure.getProjectStructureId()).toString());
%>

<style>
	*{
    box-sizing: border-box;
}

.wheel-container{
    width: 90%;
    max-width: 758px;
    margin: 10px auto;
    position: relative;
}
.wheel-object{
    width: 100%;
}


</style>


<div class="wheel-container">
   <object id="wheel-object" class="wheel-object" data="<%=request.getContextPath() %>/Gaits_wheel.svg" type="" <%--onload="wheelInit()"--%>></object>
</div>


<script>
	$(".wheel-object").on('load', function(){
		wheelInit();
	});
	
	var timeoutCounter = 0;

	function wheelInit(){
	    var wheelObj = $('#wheel-object');
	    var wheelDOM = wheelObj[0].contentDocument;
	    if (!wheelDOM && timeoutCounter < 100) {
	    	timeoutCounter++;
	    	console.log(timeoutCounter);
	    	window.setTimeout(wheelInit, 100);
	    	return;
	    }
	    var markers = "<%= builder.toString() %>";
		var arrayMarkers = markers.split(";");
	    for (var l = 1; l <= 4; l++) {
			for (var p = 1; p <= 10; p++) {
				var name = $("#arc"+l+"-sec"+p, wheelDOM).attr("name");
				//console.log(name);
				var style = "-";
				for(var a=0; a < arrayMarkers.length; a++) {
					
					var temp = arrayMarkers[a].split(",");
					
					if(temp[0] == name)
						style = temp[1];
				} 
				//console.log(style);
				if(style == "mark-square") {
					$("#arc"+l+"-sec"+p+" .mark-square", wheelDOM).css("display","block");
					$("#arc"+l+"-sec"+p+" .mark-circle", wheelDOM).css("display","none");
					$("#arc"+l+"-sec"+p+" .mark-diamond", wheelDOM).css("display","none");
				}
				if(style == "mark-circle"){
					$("#arc"+l+"-sec"+p+" .mark-square", wheelDOM).css("display","none");
					$("#arc"+l+"-sec"+p+" .mark-circle", wheelDOM).css("display","block");
					$("#arc"+l+"-sec"+p+" .mark-diamond", wheelDOM).css("display","none");
				}
				if(style == "mark-diamond"){
					$("#arc"+l+"-sec"+p+" .mark-square", wheelDOM).css("display","none");
					$("#arc"+l+"-sec"+p+" .mark-circle", wheelDOM).css("display","none");
					$("#arc"+l+"-sec"+p+" .mark-diamond", wheelDOM).css("display","block");
				}
				
				if(style == "-"){ 
					$("#arc"+l+"-sec"+p+" .mark-square", wheelDOM).css("display","none");
					$("#arc"+l+"-sec"+p+" .mark-circle", wheelDOM).css("display","none");
					$("#arc"+l+"-sec"+p+" .mark-diamond", wheelDOM).css("display","none");
				}
				
				
			}
		}

	    
	    //console.log("fill: arc1");
	    var i = 1;
	    var array = "<%= str.toString() %>";
	    var ar = array.split(",");
	    
	    for(var l=1; l<45; l++) {
	    	   $("path#sec"+l, wheelDOM).click(function(event) {
	           AUI().use('aui-base',function(A) {
	           		var $ = AUI.$;
	           	 	var l = $("path#sec"+event.target.id.substring(3,5), wheelDOM).attr("name");
	   				var renderUrl = Liferay.PortletURL.createRenderURL();
	   				renderUrl.setWindowState("<%=LiferayWindowState.NORMAL.toString() %>");
	   				renderUrl.setParameter("jspPage","/step_deliverables.jsp");
	   				renderUrl.setParameter("CMD","THREE");
	   				renderUrl.setParameter("region_num", l);
	   				renderUrl.setPortletMode("<%=LiferayPortletMode.VIEW %>");
	   				renderUrl.setPortletId("<%=themeDisplay.getPortletDisplay().getId() %>");								
	   				A.one(document.createElement('a')).attr('href',renderUrl).simulate('click');
	   				});
	           });
	    }
	    
	  
	var s = 1;
		for (var l = 1; l < 45; l++) {

			var newColor = ar[s];
			s++;
			$('path#sec' + (l), wheelDOM).css({
				'fill' : newColor
			});
		}
		
	
		for (var p = 1; p < 11; p++) {
			$("g#text" + p, wheelDOM)
					.click(
							function(event) {
								var k = $(this).attr("id").substring(4, 6);
								AUI()
										.use(
												'aui-base',
												function(A) {
													var $ = AUI.$;
													var renderUrl = Liferay.PortletURL
															.createRenderURL();
													renderUrl
															.setWindowState("<%=LiferayWindowState.NORMAL.toString() %>");
													renderUrl.setParameter("jspPage","/milestone_deliverables.jsp");
													renderUrl.setParameter("CMD","TWO");
													renderUrl.setParameter("phaseNameId", k);
													renderUrl.setPortletMode("<%=LiferayPortletMode.VIEW %>");
													renderUrl.setPortletId("<%=themeDisplay.getPortletDisplay().getId() %>");								
													A.one(document.createElement('a')).attr('href',renderUrl).simulate('click');		  
	    	 });
		});
	}
	}
	function generateRandomAlfa(){
	    /*var minimun = 0.5;
	    var rand = Math.random().toFixed(2);

	    return Math.max(minimun, rand);*/
	    return 1;
	}
	
</script>

