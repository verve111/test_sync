<%@ include file="/init.jsp"%>


<% 
	String html = "";
	String namespace = AUIUtil.getNamespace(request);
	PortletURL portletURL = renderResponse.createRenderURL();
	String catIdToRender = (String) portletSession.getAttribute("catIdToRender", javax.portlet.PortletSession.APPLICATION_SCOPE);
	if (catIdToRender != null && !catIdToRender.isEmpty()) {
		html = MaturityUtils._buildDomainsVocabularyNavigation(Long.valueOf(catIdToRender), globalGroupId, portletURL, themeDisplay);
	}
	
	// html == null when _buildDomainsVocabularyNavigation() returns null. This happens e.g. when current cat has no any child cat
	if (html != null) {
%>

		<!-- h1><strong>Filtering</strong></h1 -->
		<liferay-ui:panel-container cssClass="taglib-asset-categories-navigation" extended="<%= true %>" id='<%= namespace + "taglibAssetCategoriesNavigationPanel" %>' 
			persistState="<%= true %>">
			<liferay-ui:panel collapsible="<%= false %>" extended="<%= true %>" markupView="lexicon" persistState="<%= true %>" title="Deliverables">
				<%=html %>
			</liferay-ui:panel> 		
		</liferay-ui:panel-container>
		
<%
	}
%>		

	
<liferay-util:include page="/components/category_selector.jsp" servletContext="<%= application %>">
	<liferay-util:param name="globalGroupId" value="<%=String.valueOf(globalGroupId) %>" />
	<liferay-util:param name="vocabularyName" value="<%=MaturityUtils.RESOURCE_TYPE_VOCABULARY %>" />
	<!--  catId is needed only to make clicked category grey -->
	<liferay-util:param name="categoryId" value="<%="0" %>" />
</liferay-util:include> 


