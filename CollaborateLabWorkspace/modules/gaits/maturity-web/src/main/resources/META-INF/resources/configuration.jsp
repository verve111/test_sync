<%@ include file="/init.jsp" %>


<liferay-portlet:actionURL portletConfiguration="<%= true %>"
    var="configurationActionURL" />

<liferay-portlet:renderURL portletConfiguration="<%= true %>"
    var="configurationRenderURL" />

<aui:form action="<%= configurationActionURL %>" method="post" name="fm">

    <aui:input name="<%= com.liferay.portal.kernel.util.Constants.CMD %>" type="hidden"
        value="<%= com.liferay.portal.kernel.util.Constants.UPDATE %>" />

    <aui:input name="redirect" type="hidden"
        value="<%= configurationRenderURL %>" />

    <aui:fieldset>
	    <aui:input label="Milestone Deliverable Journal Article Id" name="milestoneDeliverableJournalArticleId" type="text" value="<%= milestoneDeliverableJournalArticleId %>" />
	    <br/> 
	</aui:fieldset>    

    <aui:button-row>
        <aui:button type="submit"></aui:button>
    </aui:button-row>

</aui:form>