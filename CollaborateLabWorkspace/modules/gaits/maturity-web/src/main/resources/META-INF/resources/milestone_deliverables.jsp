<%@ include file="/init.jsp"%>


<%
	Long groupId = themeDisplay.getLayout().getGroupId();

	Project project = ProjectLocalServiceUtil.findByGroupId(groupId);

	ProjectStructure projectStructure = ProjectStructureLocalServiceUtil
			.findByProjectId(project.getProjectId());

	List<ProjectPhase> phases = ProjectPhaseLocalServiceUtil
			.findByProjectStructureId(projectStructure.getProjectStructureId());

	List<ProjectDomain> domains = ProjectDomainLocalServiceUtil
			.findByProjectStructureId(projectStructure.getProjectStructureId());

	Long phaseId = 0L;
	ProjectPhase selectedPhase = null;

	if (request.getParameter("phaseNameId") != null) {
		int id = Integer.valueOf(request.getParameter("phaseNameId"));
		String phaseName = Enums.PHASE_ENUM.getPhaseById(id);
		ProjectPhase phaseInner = ProjectPhaseLocalServiceUtil
					.findByProjectStructureIdIdentifier(projectStructure.getProjectStructureId(), phaseName);
		phaseId = phaseInner.getProjectPhaseId();	
	} else if (request.getParameter("phaseId") != null) {
		phaseId = Long.parseLong(request.getParameter("phaseId"));
	} else {
		ProjectPhase phaseInner = ProjectPhaseLocalServiceUtil
				.findByProjectStructureIdIdentifier(projectStructure.getProjectStructureId(), "CLINICAL NEED");
		phaseId = phaseInner.getProjectPhaseId();
	}
%>

<div class="container-fluid-1280">
	<div class="row">
		<div class="col-xl-3 col-md-3"> 
			<aui:form method="post" name="fm">
				<aui:select name="phasesSelect" id="phasesSelect" label="Milestone">
					<%
						boolean isFlagSelected = false;
						phases = MaturityUtils.sortPhaseListByArray(phases, Enums.PHASES_ARRAY);
						for (ProjectPhase phase : phases) {

							if (phaseId.equals(phase.getProjectPhaseId())) {
								isFlagSelected = true;
								selectedPhase = phase;
							}
					%>
	
					<aui:option value="<%=phase.getProjectPhaseId()%>"
						selected="<%=isFlagSelected%>">
	
						<%=LanguageUtil.get(request, phase.getIdentifier())%></aui:option>
					<%
							isFlagSelected = false;
						}
					%>
				</aui:select>
			</aui:form>
		</div>
	</div>
</div>

<style>
	#container1, #container2, #container3, #container4 {
	  margin: 20px;
	  width: 100px;
	  height: 100px;
	}
</style>

<div class="container-fluid-1280">
	<div class="row">
		<%
			int k = 0;
			for (ProjectDomain domain : domains) {
		%>
		<div class="col-xl-3 col-md-3">
			<%
				ProjectStep step = ProjectStepLocalServiceUtil.findByProjectIdProjectDomainIdProjectPhaseId(
							project.getProjectId(), domain.getProjectDomainId(), phaseId);

				//long step_percentage = step.getPercentage();

				k++;
				List<ProjectDelivery> deliveries = ProjectDeliveryLocalServiceUtil
						.findByStepId(step.getProjectStepId());
				
				int number = deliveries.size();
				int step_percentage = 0;
			
				for(ProjectDelivery d : deliveries){
					step_percentage += Enums.Status.getPrecentageById(d.getStatus());
				}
				int sum = 0;
				if(number != 0)
					sum = step_percentage / number; 	
			%>
			
			<!-- circle progress bar -->
		 	<div id="container<%=k%>"></div>	
		 	<img style="position:absolute; top: 27px;left: 42px;" src="<%=renderRequest.getContextPath()%>/images/<%= k %>.png" />
			<script>
				var bar<%=k%> = new ProgressBar.Circle(container<%=k%>, {
					  strokeWidth: 6,
					  easing: 'easeInOut',
					  duration: 1400,
					  color: '#FFEA82',
					  trailColor: '#eee',
					  trailWidth: 1,
					  svgStyle: null
					});
				
					bar<%=k%>.animate(<%=(float)sum/100%>);  // Number from 0.0 to 1.0
			</script>
			
			<h1><%=domain.getIdentifier()%></h1>

			<div class="row">
				<%
					String phaseEnumId = (String)request.getAttribute("phaseEnumId");
					int domainEnumId = Enums.getArrayNum(Enums.DOMAIN_ARRAY, domain.getIdentifier());
					int j = 0;
					deliveries = MaturityUtils.sortDeliverablesByName(globalGroupId, deliveries);
					for (ProjectDelivery delivery : deliveries) {
				%>
				<div class="col-xl-12 col-md-12" style="margin-bottom:20px;">
					<div>
					 
						<%= Utils.getNameDeliveryFromJournalArticleContent(globalGroupId,String.valueOf(delivery.getType())) %>
					</div>
					<div>
							<liferay-portlet:renderURL var="deliveryRenderURL"
									  portletMode="<%=LiferayPortletMode.VIEW.toString()%>"
									  windowState="<%=LiferayWindowState.NORMAL.toString()%>">
								  <liferay-portlet:param name="jspPage" value="/update_form.jsp" />
								  <liferay-portlet:param name="phaseEnumId" value="<%=phaseEnumId %>" />
								  <liferay-portlet:param name="domainEnumId" value="<%=String.valueOf(domainEnumId) %>" /> 
								  <liferay-portlet:param name="deliveryId" value="<%=String.valueOf(delivery.getDeliveryId()) %>" /> 
								  <liferay-portlet:param name="deliveryNumInMilestone" value="<%=String.valueOf(++j) %>" /> 
							</liferay-portlet:renderURL>

							<aui:button-row>
							    <aui:button onClick="<%= deliveryRenderURL.toString() %>" value="Update status"></aui:button>
							</aui:button-row>							
					</div>
				</div>
				<%
					}
				%>
			</div>
		</div>
		<%
			}
		%>
	</div>
</div>

<aui:script>
	AUI().use('aui-base',function(A) {
			A.one("#<portlet:namespace/>phasesSelect").on(
				'change',
				function() {
					var $ = AUI.$;
					var form = $(document.<portlet:namespace />fm);
					var selectedPhaseValue = A.one('#<portlet:namespace/>phasesSelect').get('value');
					var renderUrl = Liferay.PortletURL.createRenderURL();
					renderUrl.setWindowState("<%=LiferayWindowState.NORMAL.toString() %>");
					renderUrl.setParameter("phaseId", selectedPhaseValue);
					renderUrl.setParameter("CMD","TWO");
					renderUrl.setParameter("jspPage", "/milestone_deliverables.jsp");
					renderUrl.setPortletMode("<%=LiferayPortletMode.VIEW %>");
					renderUrl.setPortletId("<%=themeDisplay.getPortletDisplay().getId() %>");						
					A.one(document.createElement('a')).attr('href',renderUrl).simulate('click');
										
				})
										
	});

</aui:script>
	



