<%@ include file="/init.jsp"%>

<%
	String namespace = AUIUtil.getNamespace(request);
	String vocabularyName = ParamUtil.getString(request, "vocabularyName");
	// catId is needed only to make clicked category grey
	long categoryId = 0; // ParamUtil.getLong(request, "categoryId");
	String headerText = ParamUtil.getString(request, "headerText");	
	int phaseEnumId = ParamUtil.getInteger(request, "phaseEnumId");
	int domainEnumId = ParamUtil.getInteger(request, "domainEnumId");
	PortletURL portletURL = renderResponse.createRenderURL();
%>


<liferay-ui:panel-container cssClass="taglib-asset-categories-navigation" extended="<%= true %>" id='<%= namespace + "taglibAssetCategoriesNavigationPanel" + vocabularyName.replaceAll(" ","") %>' 
	persistState="<%= true %>">

	<%
	//long categoryId = ParamUtil.getLong(request, "categoryId");
	
	
	List<AssetVocabulary> vocabularies = Utils.getAssetVocabularyByName(globalGroupId, vocabularyName);
	for (AssetVocabulary assetVocabulary : vocabularies) {

		String vocabularyNavigation = MaturityUtils._buildVocabularyNavigation(assetVocabulary, categoryId, portletURL, themeDisplay);
	%>

			<liferay-ui:panel collapsible="<%= false %>" extended="<%= true %>" markupView="lexicon" persistState="<%= true %>" 
				title="<%= !headerText.isEmpty() ? headerText : assetVocabulary.getUnambiguousTitle(vocabularies, themeDisplay.getSiteGroupId(), themeDisplay.getLocale()) %>">
				<%= vocabularyNavigation == null ? "" : vocabularyNavigation %>
			</liferay-ui:panel>

	<%
		break;

	}
	%>

</liferay-ui:panel-container>