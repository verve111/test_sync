package com.collaboratelab.portlet;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;

import com.collaboratelab.MaturityUtils;
import com.collaboratelab.constants.MaturityDisplayPortletKeys;
import com.collaboratelab.project.constants.Utils;
import com.collaboratelab.project.constants.enums.Enums;
import com.collaboratelab.project.model.Project;
import com.collaboratelab.project.model.ProjectDelivery;
import com.collaboratelab.project.model.ProjectDomain;
import com.collaboratelab.project.model.ProjectPhase;
import com.collaboratelab.project.model.ProjectStructure;
import com.collaboratelab.project.service.ProjectDeliveryLocalServiceUtil;
import com.collaboratelab.project.service.ProjectDomainLocalServiceUtil;
import com.collaboratelab.project.service.ProjectLocalServiceUtil;
import com.collaboratelab.project.service.ProjectPhaseLocalServiceUtil;
import com.collaboratelab.project.service.ProjectStructureLocalServiceUtil;
import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.document.library.kernel.service.DLAppLocalServiceUtil;
import com.liferay.document.library.kernel.service.DLFolderLocalServiceUtil;
import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.io.ByteArrayFileInputStream;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.UserNotificationEvent;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.UserNotificationEventLocalService;
import com.liferay.portal.kernel.service.UserNotificationEventLocalServiceUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.StreamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;

@Component(immediate = true, property = { "com.liferay.portlet.display-category=category.gaits",
		"com.liferay.portlet.instanceable=false", "javax.portlet.display-name=MaturityDisplay Portlet",
		"javax.portlet.init-param.template-path=/", "javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + MaturityDisplayPortletKeys.MaturityDisplay,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user",
		"javax.portlet.init-param.config-template=/configuration.jsp",
		"com.liferay.portlet.header-portlet-javascript=/js/progressbar.js",
		"com.liferay.portlet.private-session-attributes=false"
		}, 
	configurationPid = "com.collaboratelab.portlet.MaturityDisplayPortletInstanceConfiguration",
	service = Portlet.class)
public class MaturityDisplayPortlet extends MVCPortlet {
	
	private static final Log logger = LogFactoryUtil.getLog(MaturityDisplayPortlet.class);
	private long globalSiteId;

	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		renderRequest.setAttribute(MaturityDisplayPortletInstanceConfiguration.class.getName(), _configuration);
		super.doView(renderRequest, renderResponse);
	}
	
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		Long groupId = themeDisplay.getLayout().getGroupId();

		Project project = ProjectLocalServiceUtil.findByGroupId(groupId);

		ProjectStructure projectStructure = ProjectStructureLocalServiceUtil.findByProjectId(project.getProjectId());
		Object cmd = (Object) renderRequest.getParameter("CMD");

		if (cmd == null)
			cmd = "ONE";

		if (cmd.equals("ONE")) {

		} else if (cmd.equals("TWO")) {

			String phaseId = renderRequest.getParameter("phaseId");
			String phaseNameId = renderRequest.getParameter("phaseNameId");
			String phaseEnumId = "";
			
			// on wheel click 
			if (phaseNameId != null) {
				phaseEnumId = String.valueOf(Integer.valueOf(phaseNameId) - 1);
			// on combo change
			} else if (phaseId != null) {
				ProjectPhase phase = ProjectPhaseLocalServiceUtil.fetchProjectPhase(Long.valueOf(phaseId));
				phaseEnumId = String.valueOf(Enums.getArrayNum(Enums.PHASES_ARRAY, phase.getIdentifier()));
		
			}
			renderRequest.setAttribute("phaseEnumId", phaseEnumId);	
			
		} else if (cmd.equals("THREE")) {
			ProjectPhase phase = null;
			ProjectDomain domain = null;
			long phaseId = 0;
			long domainId = 0;
			if (renderRequest.getParameter("phaseId") != null && renderRequest.getParameter("domainId") != null) {
				// on dropdown change
				phaseId = Long.parseLong(renderRequest.getParameter("phaseId"));
				domainId = Long.parseLong(renderRequest.getParameter("domainId"));
				domain = ProjectDomainLocalServiceUtil.findByProjectDomainId(domainId);
				phase = ProjectPhaseLocalServiceUtil.findByProjectPhaseId(phaseId);
			} else if (renderRequest.getParameter("region_num") != null) {
				//on wheel region click
				String[] array = Enums.phaseDomainMap.get(String.valueOf(renderRequest.getParameter("region_num")));
				String phaseStr = array[0];
				String domainStr = array[1];
				domain = ProjectDomainLocalServiceUtil
						.findByProjectStructureIdIdentifier(projectStructure.getProjectStructureId(), domainStr);
				phase = ProjectPhaseLocalServiceUtil
						.findByProjectStructureIdIdentifier(projectStructure.getProjectStructureId(), phaseStr);
			
			}
			phaseId = phase.getProjectPhaseId();
			domainId = domain.getProjectDomainId();
			int phaseEnumId = Enums.getArrayNum(Enums.PHASES_ARRAY, phase.getIdentifier());
			int domainEnumId = Enums.getArrayNum(Enums.DOMAIN_ARRAY, domain.getIdentifier());
			
			renderRequest.setAttribute("phaseId", phaseId);
			renderRequest.setAttribute("domainId", domainId);
			renderRequest.setAttribute("phaseEnumId", phaseEnumId);
			renderRequest.setAttribute("domainEnumId", domainEnumId);
			
			// this categoryId is used within AssetPublisher hook
			try {
				long domainPhaseCatId = Utils.getCategoryByDomainAndPhase(getGlobalSiteId(themeDisplay.getCompanyId()), domainEnumId, phaseEnumId);
				renderRequest.getPortletSession().setAttribute("catIdToRender", String.valueOf(domainPhaseCatId), PortletSession.APPLICATION_SCOPE);
			} catch (PortalException e) {
				logger.error("can't get milestone category", e);
			}
			
		}
		super.render(renderRequest, renderResponse);
	}
	
	private void setDateOnCalendar(Calendar cal, String dateStr) {
		try {
			cal.setTime(MaturityUtils.dateFormat.parse(dateStr));
		} catch (ParseException e) {
			logger.error("ParseException:: ", e);
		}
	}
	
	public void saveBack(ActionRequest request, ActionResponse response) {
		//request.getAttribute("");
		Long deliveryId = ParamUtil.getLong(request, "deliveryId");
		//Long projectId = ParamUtil.getLong(request, "projectId");
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		long groupId = themeDisplay.getScopeGroupId();
		ProjectDelivery delivery = null;
		
		try {
			delivery = ProjectDeliveryLocalServiceUtil.getProjectDelivery(deliveryId);
		} catch (PortalException e) {
			e.printStackTrace();
		}

		// TODO: available -> false, unavailable -> true, to change
		boolean isAvailable = ParamUtil.getBoolean(request, "deliverableNA");
		boolean isDeliverableViewSiteMembers = ParamUtil.getBoolean(request, "deliverableViewSiteMembers");
		
		int status = ParamUtil.getInteger(request, "status");
		int outcomeCategory = ParamUtil.getInteger(request, "outcomeCategory");

		String startDate = ParamUtil.getString(request, "startDate");
		boolean isEmptyStartDate = startDate.isEmpty();
		Calendar startCal = null;
		if (!isEmptyStartDate) {
			startCal = Calendar.getInstance();
			setDateOnCalendar(startCal, startDate);
		}
		
		String endDate = ParamUtil.getString(request, "endDate");
		boolean isEmptyEndDate = startDate.isEmpty();
		Calendar endCal = null;
		if (!isEmptyEndDate) {
			endCal = Calendar.getInstance();
			setDateOnCalendar(endCal, endDate);
		}		

		String startPlanningDate = ParamUtil.getString(request, "startPlanningDate");
		boolean isEmptyPlanStartDate = startPlanningDate.isEmpty();
		Calendar startPlanningCal = null;
		if (!isEmptyPlanStartDate) {
			startPlanningCal = Calendar.getInstance();	
			setDateOnCalendar(startPlanningCal, startPlanningDate);
		}
		
		String endPlanningDate = ParamUtil.getString(request, "endPlanningDate");
		boolean isEmptyPlanEndDate = endPlanningDate.isEmpty(); 
		Calendar endPlanningCal = null;
		if (!isEmptyPlanEndDate) {
			endPlanningCal = Calendar.getInstance();	
			setDateOnCalendar(endPlanningCal, endPlanningDate);
		}
		
		int estimatedCost = ParamUtil.getInteger(request, "estimatedCost");
		int funding = ParamUtil.getInteger(request, "funding");
		String responsiblePerson = ParamUtil.getString(request,"responsiblePerson");
		double budget = ParamUtil.getDouble(request,"budget");
		String comment = ParamUtil.getString(request, "comment");
		
		delivery.setIsEditPlanning(isDeliverableViewSiteMembers);
		delivery.setIsAvailable(isAvailable);
		delivery.setStatus(status);
		delivery.setOutcomeCategory(outcomeCategory);
		delivery.setStartDate(isEmptyStartDate ? null : new Date(startCal.getTimeInMillis()));
		delivery.setEndDate(isEmptyEndDate ? null : new Date(endCal.getTimeInMillis()));
		delivery.setCost(estimatedCost);
		delivery.setStartPlanningDate(isEmptyPlanStartDate ? null : new Date(startPlanningCal.getTimeInMillis()));
		delivery.setEndPlanningDate(isEmptyPlanEndDate ? null : new Date(endPlanningCal.getTimeInMillis()));
		delivery.setBudget(budget);
		delivery.setPersonResponsible(responsiblePerson);
		delivery.setFundingSource(funding);
		delivery.setComment(comment);

		//String titleTracking =  uploadPortletRequest.getFileName("file");
		
		fileUpload(themeDisplay, groupId, delivery, request, false);
		
		fileUpload(themeDisplay, groupId, delivery, request, true);		
		
		ProjectDeliveryLocalServiceUtil.updateProjectDelivery(delivery);
		
		/*return userNotificationEventLocalService.addUserNotificationEvent(
				userId, "event", 0,
				UserNotificationDeliveryConstants.TYPE_WEBSITE, 0,
				RandomTestUtil.randomString(), false,
				ServiceContextTestUtil.getServiceContext());*/
	}
	
	private void fileUpload(ThemeDisplay themeDisplay, long groupId, ProjectDelivery delivery, ActionRequest request, boolean isPlanning) {
		File file = getFile(request, !isPlanning ? "file" : "filePlanning");		
		if (file != null) {
			long globalId = getGlobalSiteId(themeDisplay.getCompanyId());
			
			UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(request);
			String fileTitle = uploadPortletRequest.getFileName( !isPlanning ? "file" : "filePlanning"); 
			
			DLFolder dlParentFolder = null;
			
			dlParentFolder = DLFolderLocalServiceUtil.fetchFolder(groupId, 0, Utils.DELIVERABLE_FILES_FOLDER);
			
			if (dlParentFolder == null) {
				dlParentFolder = createFolder(Utils.DELIVERABLE_FILES_FOLDER, groupId, 0, themeDisplay);
			}

			DLFolder dlFolder = null;
			String folderTitle = MaturityUtils.getDeliverableTitle(globalId, delivery);
			if (!folderTitle.isEmpty()) {
				folderTitle += (isPlanning ? Utils.DELIVERABLE_PLANNING_POSTFIX : "");
				dlFolder = DLFolderLocalServiceUtil.fetchFolder(themeDisplay.getScopeGroupId(),
						dlParentFolder.getFolderId(), folderTitle);
				if (dlFolder == null) {
					dlFolder = createFolder(folderTitle, groupId, dlParentFolder.getFolderId(), themeDisplay);
					/*if (dlFolder != null && dlFolder.getExpandoBridge().hasAttribute("deliverableId")) {
						dlFolder.getExpandoBridge().setAttribute(Constants.EXPANDO_FOLDER_DELIVERABLE_ID,
								delivery.getDeliveryId() + (isPlanning ? "p" : ""));
					}*/
				}
			
				fileUploadByDL(folderTitle, file, request, fileTitle);
			}
		}
	}

	private long getGlobalSiteId(long companyId) {
		if (globalSiteId == 0) {
			globalSiteId = Utils.getGlobalGroupId(companyId);
		}
		return globalSiteId;
	}
	
	public void saveReview(ActionRequest request, ActionResponse response) {

		Long deliveryId = ParamUtil.getLong(request, "deliveryId");

		ProjectDelivery delivery = null;

		try {
			delivery = ProjectDeliveryLocalServiceUtil.getProjectDelivery(deliveryId);
		} catch (PortalException e) {
			e.printStackTrace();
		}
		
		int rate = ParamUtil.getInteger(request, "mark");
		String comment = ParamUtil.getString(request, "comment");
		boolean isCompleted = ParamUtil.getBoolean(request, "review_radio");
		
		delivery.setReviewerComment(comment);
		delivery.setReviewerIsCompleted(isCompleted);
		delivery.setReviewerRate(rate);
		
		ProjectDeliveryLocalServiceUtil.updateProjectDelivery(delivery);
	}

	public void fileUploadByDL(String folderName, File file, ActionRequest request, String title) {
		if (file != null) {
			ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
			long userId = themeDisplay.getUserId();
			long groupId = themeDisplay.getScopeGroupId();
			String mimeType = MimeTypesUtil.getContentType(file);
			try {
				DLFolder dlParentFolder = DLFolderLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(), 0,
						Utils.DELIVERABLE_FILES_FOLDER);
				DLFolder dlFolder = DLFolderLocalServiceUtil.getFolder(themeDisplay.getScopeGroupId(),
						dlParentFolder.getFolderId(), folderName);

				//long fileEntryTypeId = dlFolder.getDefaultFileEntryTypeId();
				ServiceContext serviceContext = ServiceContextFactory.getInstance(DLFileEntry.class.getName(), request);
				serviceContext.setWorkflowAction(WorkflowConstants.ACTION_PUBLISH);
				byte[] bytes = Files.readAllBytes(file.toPath());
				
				
				DLAppLocalServiceUtil.addFileEntry(userId, groupId, dlFolder.getFolderId(),
						title, mimeType, title, "", "", bytes, serviceContext);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} 
	}

	private DLFolder createFolder(String folderName, long groupId, long parentFolderId,
			ThemeDisplay themeDisplay) {
		DLFolder folder = null;
		try {
			ServiceContext serviceContext = new ServiceContext();
			serviceContext.setScopeGroupId(groupId);
			// Create a delivery folder in an Project Folder
			folder = DLFolderLocalServiceUtil.addFolder(themeDisplay.getUserId(), groupId, groupId, false,
					parentFolderId, folderName, "", false, serviceContext);
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (PortalException e) {
			e.printStackTrace();
		}
		return folder;
	}

	public File getFile(ActionRequest request, String name) {
		File file = null;
		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(request);
		
		ByteArrayFileInputStream inputStream = null;
		try {
			file = uploadPortletRequest.getFile(name);
			if (file != null && !file.exists()) {
				//System.out.println("Empty File");
				return null;
			}

		} finally {
			StreamUtil.cleanUp(inputStream);
		}
		return file;
	}
	
    @Activate
    @Modified
    protected void activate(Map<Object, Object> properties) {
            _configuration = ConfigurableUtil.createConfigurable(
            		MaturityDisplayPortletInstanceConfiguration.class, properties);
    }
    
	@BeanReference(type = UserNotificationEventLocalService.class)
	protected UserNotificationEventLocalService userNotificationEventLocalService;    

    private volatile MaturityDisplayPortletInstanceConfiguration _configuration;	
}