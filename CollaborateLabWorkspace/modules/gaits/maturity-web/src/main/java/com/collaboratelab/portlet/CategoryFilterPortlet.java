package com.collaboratelab.portlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

import com.collaboratelab.constants.MaturityDisplayPortletKeys;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

@Component(immediate = true, property = { "com.liferay.portlet.display-category=category.gaits",
		"com.liferay.portlet.instanceable=false", "javax.portlet.display-name=Category Filter Portlet",
		"javax.portlet.init-param.template-path=/", "javax.portlet.init-param.view-template=/category_filter.jsp",
		"javax.portlet.name=" + MaturityDisplayPortletKeys.CategoryFiltersPortlet,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user",
		"com.liferay.portlet.private-session-attributes=false"
		}, 
	service = Portlet.class)
public class CategoryFilterPortlet extends MVCPortlet {

}
