package com.collaboratelab;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.portlet.PortletURL;

import com.collaboratelab.project.constants.Utils;
import com.collaboratelab.project.model.ProjectDelivery;
import com.collaboratelab.project.model.ProjectPhase;
import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetCategoryServiceUtil;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.xml.DocumentException;
import com.liferay.portal.kernel.xml.SAXReaderUtil;

public class MaturityUtils {

	public static final DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	
	public static final String RESOURCE_TYPE_VOCABULARY = "Resource Type";

	private static String getCatSelectorCategoryUrl(String portletUrlAsString, long catId) {
		return portletUrlAsString.substring(0, portletUrlAsString.indexOf("?")) + "/-/categories/" + String.valueOf(catId);
	}
	
	public static List<ProjectDelivery> sortDeliverablesByName(long globalGroupId, List<ProjectDelivery> dels) {
		List<ProjectDelivery> list = new ArrayList<>(dels);
		list.sort((ProjectDelivery o1, ProjectDelivery o2) -> Utils
				.getNameDeliveryFromJournalArticleContent(globalGroupId, String.valueOf(o1.getType())).compareTo(
						Utils.getNameDeliveryFromJournalArticleContent(globalGroupId, String.valueOf(o2.getType()))));
		return list;
	}
	
	private static void _buildCategoriesNavigation(List<AssetCategory> categories, long categoryId,
			PortletURL portletURL, ThemeDisplay themeDisplay, StringBundler sb) throws Exception {
		portletURL.setParameter("categoryId", StringPool.BLANK);
		String originalPortletURLString = portletURL.toString();
		for (AssetCategory category : categories) {
			category = category.toEscapedModel();

			String title = category.getTitle(themeDisplay.getLocale());
			List<AssetCategory> categoriesChildren = AssetCategoryServiceUtil
					.getChildCategories(category.getCategoryId(), QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);

			sb.append("<li class=\"tree-node\"><span>");

			if (categoryId == category.getCategoryId()) {
				sb.append("<a class=\"tag-selected\" href=\"");
				sb.append(HtmlUtil.escape(originalPortletURLString));
			} else {
				portletURL.setParameter("categoryId", String.valueOf(category.getCategoryId()));

				sb.append("<a href=\"");
				sb.append(HtmlUtil.escape(getCatSelectorCategoryUrl(portletURL.toString(), category.getCategoryId())/*portletURL.toString()*/));
			}

			sb.append("\">");
			sb.append(title);
			sb.append("</a>");
			sb.append("</span>");

			if (!categoriesChildren.isEmpty()) {
				sb.append("<ul>");

				_buildCategoriesNavigation(categoriesChildren, categoryId, portletURL, themeDisplay, sb);

				sb.append("</ul>");
			}

			sb.append("</li>");
		}
	}

	public static String _buildVocabularyNavigation(AssetVocabulary vocabulary, long categoryId, PortletURL portletURL,
			ThemeDisplay themeDisplay) throws Exception {
		List<AssetCategory> categories = AssetCategoryServiceUtil.getVocabularyRootCategories(vocabulary.getGroupId(),
				vocabulary.getVocabularyId(), QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
		return wrapInHTML(categories, categoryId, portletURL, themeDisplay);
	}
	
	private static String wrapInHTML(List<AssetCategory> categories, long categoryId, PortletURL portletURL,
			ThemeDisplay themeDisplay) throws Exception {
		if (categories.isEmpty()) {
			return null;
		}
		StringBundler sb = new StringBundler();
		sb.append("<div class=\"lfr-asset-category-list-container\"><ul class=\"lfr-asset-category-list\">");
		_buildCategoriesNavigation(categories, categoryId, portletURL, themeDisplay, sb);
		sb.append("</ul></div>");
		return sb.toString();
	}
	
	public static String _buildDomainsVocabularyNavigation(long categoryId, long globalGroupId, PortletURL portletURL,
			ThemeDisplay themeDisplay) throws Exception {
		List<AssetCategory> deliverableCategories = new ArrayList<AssetCategory>();
		if (categoryId > 0) {
			OrderByComparator<AssetCategory> orderByComparator = null; //new AssetCategoryCreateDateComparator(true);
			deliverableCategories = AssetCategoryServiceUtil.getChildCategories(categoryId, QueryUtil.ALL_POS,
					QueryUtil.ALL_POS, orderByComparator);
		}
		return wrapInHTML(deliverableCategories, 0, portletURL, themeDisplay);
	}
	
	public static String getWWHDeliveryFromJournalArticleContent(long groupId,String articleId, String title) {
		String name ="";
		
		if(title.equals("what")) {
			name = "TextBoxbng4";
		}else if(title.equals("why")) {
			name = "TextBoxgg6k";
		}else if(title.equals("how")) {
			name="TextBoxerrk";
		}
		
		JournalArticle ja = Utils.getJournalArticle(groupId,articleId);
		if(ja != null) {
			com.liferay.portal.kernel.xml.Document document = null;
			try {
				document = SAXReaderUtil.read(ja.getContent());
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			com.liferay.portal.kernel.xml.Node deliveryNameNode = document.selectSingleNode("/root/dynamic-element[@name='"+name+"']/dynamic-content");
			if(deliveryNameNode != null)
				return deliveryNameNode.getText();
			
			return "";
		}
		return "";
	}
		
	public static String getDateByLocale(Date date, Locale locale) {
		
		DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, locale);
		String formattedDate = df.format(date);
		
		return formattedDate;	
	}
	
	public static String getDateAsStr(Calendar cal) {
		String d = String.valueOf(cal.get(Calendar.DAY_OF_MONTH));
		String day =  d.length() == 1 ? "0".concat(d) : d;
		String m = String.valueOf(cal.get(Calendar.MONTH) + 1);
		String month =  m.length() == 1 ? "0".concat(m) : m;		
		return month + "/" + day +"/" + cal.get(Calendar.YEAR);
	}
	
	public static List<ProjectPhase> sortPhaseListByArray(List<ProjectPhase> list, String[] arr) {
		List<ProjectPhase> resList = new ArrayList<ProjectPhase>(list);
		for (int i = arr.length - 1; i > 0; i--) { // Downward for efficiency
		    final String id = arr[i];
		    // Big optimization: we don't have to search the full list as the part
		    // before i is already sorted and object for id can only be on the remaining
		    for (int j = i; j >= 0; j--) // NOTE: loop starting at i
		        if (id.equals(resList.get(j).getIdentifier())) {
		            Collections.swap(resList, j, i);
		            break;
		        }
		}
		return resList;
	}
	
	public static String getDeliverableTitle(long globalGroupId, ProjectDelivery delivery) {
		return Utils.getNameDeliveryFromJournalArticleContent(globalGroupId, String.valueOf(delivery.getType()));
	}

}
