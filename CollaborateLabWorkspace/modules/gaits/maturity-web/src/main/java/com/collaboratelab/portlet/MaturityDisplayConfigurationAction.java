package com.collaboratelab.portlet;

import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.liferay.portal.kernel.portlet.ConfigurationAction;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.util.ParamUtil;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;

import org.osgi.service.component.annotations.ConfigurationPolicy;

import com.collaboratelab.constants.MaturityDisplayPortletKeys;
import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;

@Component(
	    configurationPid = "com.collaboratelab.portlet.MaturityDisplayPortletInstanceConfiguration",
	    configurationPolicy = ConfigurationPolicy.OPTIONAL,
	    immediate = true,
	    property = {
	        "javax.portlet.name=" + MaturityDisplayPortletKeys.MaturityDisplay
	    },
	    service = ConfigurationAction.class
	)
public class MaturityDisplayConfigurationAction extends DefaultConfigurationAction {

	private static final String[] CONFIG_PARAMS_NAMES = new String[] { "milestoneDeliverableJournalArticleId" };

	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse)
			throws Exception {

		for (String cParam : CONFIG_PARAMS_NAMES) {
			String param = ParamUtil.getString(actionRequest, cParam);
			setPreference(actionRequest, cParam, param);
		}

		super.processAction(portletConfig, actionRequest, actionResponse);
	}

	@Override
	public void include(PortletConfig portletConfig, HttpServletRequest httpServletRequest,
			HttpServletResponse httpServletResponse) throws Exception {

		httpServletRequest.setAttribute(MaturityDisplayPortletInstanceConfiguration.class.getName(), _configuration);

		super.include(portletConfig, httpServletRequest, httpServletResponse);
	}

	@Activate
	@Modified
	protected void activate(Map<Object, Object> properties) {
		_configuration = ConfigurableUtil.createConfigurable(MaturityDisplayPortletInstanceConfiguration.class,
				properties);
	}

	private volatile MaturityDisplayPortletInstanceConfiguration _configuration;
}
