package com.collaboratelab.portlet;

import java.io.IOException;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.collaboratelab.constants.MaturityDisplayPortletKeys;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

@Component(immediate = true, property = { "com.liferay.portlet.display-category=category.gaits",
		"com.liferay.portlet.instanceable=false", "javax.portlet.display-name=Milestones Report Portlet",
		"javax.portlet.init-param.template-path=/", "javax.portlet.init-param.view-template=/milestonesdisplay.jsp",
		"javax.portlet.name=" + MaturityDisplayPortletKeys.MilestonesReportDisplay,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
		}, 
	service = Portlet.class)
public class MilestonesDisplayPortlet extends MVCPortlet {

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {

		super.render(renderRequest, renderResponse);
	}
	

}