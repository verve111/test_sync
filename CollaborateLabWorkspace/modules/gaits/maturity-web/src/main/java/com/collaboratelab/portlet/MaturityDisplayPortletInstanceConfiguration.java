package com.collaboratelab.portlet;

import aQute.bnd.annotation.metatype.Meta;

@Meta.OCD(id = "com.collaboratelab.portlet.MaturityDisplayPortletInstanceConfiguration")
public interface MaturityDisplayPortletInstanceConfiguration {

	@Meta.AD(deflt = "", required = false)
	public String milestoneDeliverableJournalArticleId();

}
