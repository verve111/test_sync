package com.collaboratelab.constants;

public class MaturityDisplayPortletKeys {

	public static final String MaturityDisplay = "MaturityDisplay";
	public static final String MilestonesReportDisplay = "MilestonesReportDisplay";
	public static final String CategoryFiltersPortlet = "CategoryFiltersPortlet";	
}