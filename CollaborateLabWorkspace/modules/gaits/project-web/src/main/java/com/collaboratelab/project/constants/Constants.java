package com.collaboratelab.project.constants;

public class Constants {
	public static final String DDM_STRUCTURE_WHAT_WHY_HOW_NAME = "delivery_what_why_how";
	public static final String DDM_TEMPLATE_WHAT_WHY_HOW_NAME = "delivery_what_why_how";
	public static final String GLOBAL_GROUP_URL = "/global";
	public static final String GLOBAL_FOLDER_SYSTEM = "System";
	public static final String GLOBAL_HOME_HEADER = "HomeHeader";
	public static final String GLOBAL_PROJECT_PROFILE_CONTENT = "project_profile_content";
	public static final String GLOBAL_PROJECT_PROFILE_CAROUSEL = "Carousel";
	public static final String GLOBAL_PROJECT_PROFILE_ITEMS_ROW = "Items Row";
	public static final String GLOBAL_PROJECT_PROFILE_PROJECT_TEAM = "Items Row with Big Heading";
	public static final String GLOBAL_PROJECT_PROFILE_FOOTER = "Footer";
	public static final String GLOBAL_PROJECT_PROFILE_TEXT_COLUMN_IMAGE = "Text Column With Image";
	public static final String GLOBAL_PROJECT_PROFILE_CAROUSEL_URL_TITLE = "carousel";
	public static final String GLOBAL_PROJECT_PROFILE_ITEMS_ROW_URL_TITLE = "items-row-type";
	public static final String GLOBAL_PROJECT_PROFILE_PROJECT_TEAM_URL_TITLE = "items-row-with-big-heading";
	public static final String GLOBAL_PROJECT_PROFILE_TEXT_COLUMN_IMAGE_URL_TITLE = "text-column-with-image";
	public static final String GLOBAL_PROJECT_PROFILE_FOOTER_URL_TITLE = "footer";
	public static final String GLOBAL_TEMPLATE_VIDEO_TITLE = "Video";
	public static final String GLOBAL_TEMPLATE_URL_TITLE = "URL";
	// keeps linked to curr folder deliverable 
	// public static final String EXPANDO_FOLDER_DELIVERABLE_ID = "deliverableId";
	
	
	public static final String CAROUSEL_CONTENT = "<?xml version=\"1.0\"?>\r\n" + 
			"\r\n" + 
			"<root available-locales=\"en_US\" default-locale=\"en_US\">\r\n" + 
			"	<dynamic-element name=\"ItemHeading\" type=\"text\" index-type=\"keyword\" instance-id=\"ubuw\">\r\n" + 
			"		<dynamic-element name=\"OpeningShortText\" instance-id=\"bfeb\" type=\"text\" index-type=\"keyword\">\r\n" + 
			"			<dynamic-content language-id=\"en_US\"><![CDATA[Opening short text]]></dynamic-content>\r\n" + 
			"		</dynamic-element>\r\n" + 
			"		<dynamic-element name=\"ItemImage\" instance-id=\"awda\" type=\"image\" index-type=\"keyword\">\r\n" + 
			"			<dynamic-content language-id=\"en_US\" fileEntryId=\"\"></dynamic-content>\r\n" + 
			"		</dynamic-element>\r\n" + 
			"		<dynamic-element name=\"ItemParagraph\" instance-id=\"prcf\" type=\"text\" index-type=\"keyword\">\r\n" + 
			"			<dynamic-content language-id=\"en_US\"><![CDATA[Project Description in 50 words or less. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tristique eget nibh at sollicitudin. Aliquam euismod lorem sollicitudin ipsum mollis bibendum. Aliquam eu posuere felis. Proin interdum posuere nulla, ac pharetra lacus ultrices et. Praesent elementum, ante et iaculis tincidunt, dolor leo sodales augue, a molestie risus elit facilisis quam.]]></dynamic-content>\r\n" + 
			"		</dynamic-element>\r\n" + 
			"		<dynamic-element name=\"ItemLinkText\" instance-id=\"gmyv\" type=\"text\" index-type=\"keyword\">\r\n" + 
			"			<dynamic-element name=\"ItemLinkURL\" instance-id=\"pwep\" type=\"text\" index-type=\"keyword\">\r\n" + 
			"				<dynamic-content language-id=\"en_US\"><![CDATA[]]></dynamic-content>\r\n" + 
			"			</dynamic-element>\r\n" + 
			"			<dynamic-content language-id=\"en_US\"><![CDATA[]]></dynamic-content>\r\n" + 
			"		</dynamic-element>\r\n" + 
			"		<dynamic-content language-id=\"en_US\"><![CDATA[Enter Your Project Name]]></dynamic-content>\r\n" + 
			"	</dynamic-element>\r\n" + 
			"</root>";
	
	public static final String ITEMS_ROW_CONTENT = "<?xml version='1.0' encoding='UTF-8'?>\r\n" + 
			"\r\n" + 
			"<root available-locales=\"en_US\" default-locale=\"en_US\">\r\n" + 
			"        <dynamic-element name=\"ItemTitle\" type=\"text\" index-type=\"keyword\" instance-id=\"tyax\">\r\n" + 
			"                <dynamic-element name=\"ItemImage\" instance-id=\"vggr\" type=\"image\" index-type=\"keyword\">\r\n" + 
			"                        <dynamic-content language-id=\"en_US\" alt=\"\" name=\"problem.png\" title=\"problem.png\" type=\"document\" fileEntryId=\"80702\"><![CDATA[/documents/48102/0/problem.png/11e7e9a6-cecb-8c32-2736-9af69ee75705?t=1522164213189]]></dynamic-content>\r\n" + 
			"                </dynamic-element>\r\n" + 
			"                <dynamic-element name=\"htmlContent\" instance-id=\"vhqn\" type=\"text_area\" index-type=\"keyword\">\r\n" + 
			"                        <dynamic-content language-id=\"en_US\"><![CDATA[<ul>\r\n" + 
			"        <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n" + 
			"        <li>Nullam sed augue luctus, molestie ligula sed, tempus lorem.</li>\r\n" + 
			"        <li>Suspendisse aliquam in neque a commodo.&nbsp;</li>\r\n" + 
			"</ul>]]></dynamic-content>\r\n" + 
			"                </dynamic-element>\r\n" + 
			"                <dynamic-element name=\"linkTo\" instance-id=\"hgwt\" type=\"text\" index-type=\"keyword\">\r\n" + 
			"                        <dynamic-element name=\"linkText\" instance-id=\"kqfv\" type=\"text\" index-type=\"keyword\">\r\n" + 
			"                                <dynamic-content language-id=\"en_US\"><![CDATA[]]></dynamic-content>\r\n" + 
			"                        </dynamic-element>\r\n" + 
			"                        <dynamic-content language-id=\"en_US\"><![CDATA[]]></dynamic-content>\r\n" + 
			"                </dynamic-element>\r\n" + 
			"                <dynamic-content language-id=\"en_US\"><![CDATA[Problem]]></dynamic-content>\r\n" + 
			"        </dynamic-element>\r\n" + 
			"        <dynamic-element name=\"ItemTitle\" type=\"text\" index-type=\"keyword\" instance-id=\"tpes\">\r\n" + 
			"                <dynamic-element name=\"ItemImage\" instance-id=\"dioh\" type=\"image\" index-type=\"keyword\">\r\n" + 
			"                        <dynamic-content language-id=\"en_US\" alt=\"\" name=\"solution.png\" title=\"solution.png\" type=\"document\" fileEntryId=\"80713\"><![CDATA[/documents/48102/0/solution.png/7a186ec7-2cbd-81a3-e27a-766a8eec5ee1?t=1522164234904]]></dynamic-content>\r\n" + 
			"                </dynamic-element>\r\n" + 
			"                <dynamic-element name=\"htmlContent\" instance-id=\"noiv\" type=\"text_area\" index-type=\"keyword\">\r\n" + 
			"                        <dynamic-content language-id=\"en_US\"><![CDATA[<ul>\r\n" + 
			"        <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n" + 
			"        <li>Nullam sed augue luctus, molestie ligula sed, tempus lorem.</li>\r\n" + 
			"        <li>Suspendisse aliquam in neque a commodo.&nbsp;</li>\r\n" + 
			"</ul>]]></dynamic-content>\r\n" + 
			"                </dynamic-element>\r\n" + 
			"                <dynamic-element name=\"linkTo\" instance-id=\"stoq\" type=\"text\" index-type=\"keyword\">\r\n" + 
			"                        <dynamic-element name=\"linkText\" instance-id=\"aqdx\" type=\"text\" index-type=\"keyword\">\r\n" + 
			"                                <dynamic-content language-id=\"en_US\"><![CDATA[]]></dynamic-content>\r\n" + 
			"                        </dynamic-element>\r\n" + 
			"                        <dynamic-content language-id=\"en_US\"><![CDATA[]]></dynamic-content>\r\n" + 
			"                </dynamic-element>\r\n" + 
			"                <dynamic-content language-id=\"en_US\"><![CDATA[Solution]]></dynamic-content>\r\n" + 
			"        </dynamic-element>\r\n" + 
			"        <dynamic-element name=\"ItemTitle\" type=\"text\" index-type=\"keyword\" instance-id=\"idtb\">\r\n" + 
			"                <dynamic-element name=\"ItemImage\" instance-id=\"jhkp\" type=\"image\" index-type=\"keyword\">\r\n" + 
			"                        <dynamic-content language-id=\"en_US\" alt=\"\" name=\"impact.png\" title=\"impact.png\" type=\"document\" fileEntryId=\"80691\"><![CDATA[/documents/48102/0/impact.png/ae8d05c9-3754-7abf-f9e3-d09eccb1f72b?t=1522164193966]]></dynamic-content>\r\n" + 
			"                </dynamic-element>\r\n" + 
			"                <dynamic-element name=\"htmlContent\" instance-id=\"qpol\" type=\"text_area\" index-type=\"keyword\">\r\n" + 
			"                        <dynamic-content language-id=\"en_US\"><![CDATA[<ul>\r\n" + 
			"        <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n" + 
			"        <li>Nullam sed augue luctus, molestie ligula sed, tempus lorem.</li>\r\n" + 
			"        <li>Suspendisse aliquam in neque a commodo.&nbsp;</li>\r\n" + 
			"</ul>]]></dynamic-content>\r\n" + 
			"                </dynamic-element>\r\n" + 
			"                <dynamic-element name=\"linkTo\" instance-id=\"khlm\" type=\"text\" index-type=\"keyword\">\r\n" + 
			"                        <dynamic-element name=\"linkText\" instance-id=\"wqvb\" type=\"text\" index-type=\"keyword\">\r\n" + 
			"                                <dynamic-content language-id=\"en_US\"><![CDATA[]]></dynamic-content>\r\n" + 
			"                        </dynamic-element>\r\n" + 
			"                        <dynamic-content language-id=\"en_US\"><![CDATA[]]></dynamic-content>\r\n" + 
			"                </dynamic-element>\r\n" + 
			"                <dynamic-content language-id=\"en_US\"><![CDATA[Impact]]></dynamic-content>\r\n" + 
			"        </dynamic-element>\r\n" + 
			"        <dynamic-element name=\"ItemTitle\" type=\"text\" index-type=\"keyword\" instance-id=\"lmnn\">\r\n" + 
			"                <dynamic-element name=\"ItemImage\" instance-id=\"fgnj\" type=\"image\" index-type=\"keyword\">\r\n" + 
			"                        <dynamic-content language-id=\"en_US\" alt=\"\" name=\"next-steps.png\" title=\"next-steps.png\" type=\"document\" fileEntryId=\"80724\"><![CDATA[/documents/48102/0/next-steps.png/ccb60c5c-bb67-0991-31b7-920c93b37f66?t=1522164264401]]></dynamic-content>\r\n" + 
			"                </dynamic-element>\r\n" + 
			"                <dynamic-element name=\"htmlContent\" instance-id=\"pkej\" type=\"text_area\" index-type=\"keyword\">\r\n" + 
			"                        <dynamic-content language-id=\"en_US\"><![CDATA[<ul>\r\n" + 
			"        <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\r\n" + 
			"        <li>Nullam sed augue luctus, molestie ligula sed, tempus lorem.</li>\r\n" + 
			"        <li>Suspendisse aliquam in neque a commodo.&nbsp;</li>\r\n" + 
			"</ul>]]></dynamic-content>\r\n" + 
			"                </dynamic-element>\r\n" + 
			"                <dynamic-element name=\"linkTo\" instance-id=\"xpgs\" type=\"text\" index-type=\"keyword\">\r\n" + 
			"                        <dynamic-element name=\"linkText\" instance-id=\"jiqx\" type=\"text\" index-type=\"keyword\">\r\n" + 
			"                                <dynamic-content language-id=\"en_US\"><![CDATA[]]></dynamic-content>\r\n" + 
			"                        </dynamic-element>\r\n" + 
			"                        <dynamic-content language-id=\"en_US\"><![CDATA[]]></dynamic-content>\r\n" + 
			"                </dynamic-element>\r\n" + 
			"                <dynamic-content language-id=\"en_US\"><![CDATA[Next Steps]]></dynamic-content>\r\n" + 
			"        </dynamic-element>\r\n" + 
			"</root> ";
	
	
	public static final String PROJECT_TEAM_CONTENT = " <?xml version=\"1.0\"?>\r\n" + 
			"\r\n" + 
			"<root available-locales=\"en_US\" default-locale=\"en_US\">\r\n" + 
			"        <dynamic-element name=\"BigHeading\" type=\"text\" index-type=\"keyword\" instance-id=\"libk\">\r\n" + 
			"                <dynamic-content language-id=\"en_US\"><![CDATA[Our Team]]></dynamic-content>\r\n" + 
			"        </dynamic-element>\r\n" + 
			"        <dynamic-element name=\"ShortParagraph\" type=\"text\" index-type=\"keyword\" instance-id=\"nagg\">\r\n" + 
			"                <dynamic-content language-id=\"en_US\"><![CDATA[]]></dynamic-content>\r\n" + 
			"        </dynamic-element>\r\n" + 
			"        <dynamic-element name=\"ItemTitle\" type=\"text\" index-type=\"keyword\" instance-id=\"nsne\">\r\n" + 
			"                <dynamic-element name=\"ItemImage\" instance-id=\"raaa\" type=\"image\" index-type=\"keyword\">\r\n" + 
			"                        <dynamic-content language-id=\"en_US\" alt=\"[Alternative image description]\" name=\"ss1.png\" title=\"ss1.png\" type=\"journal\" fileEntryId=\"\" id=\"79096\">/image/journal/article?img_id=79096&amp;t=1522078397824</dynamic-content>\r\n" + 
			"                </dynamic-element>\r\n" + 
			"                <dynamic-element name=\"htmlContent\" instance-id=\"bjkf\" type=\"text_area\" index-type=\"keyword\">\r\n" + 
			"                        <dynamic-content language-id=\"en_US\"><![CDATA[<ul>\r\n" + 
			"        <li>Title/Role</li>\r\n" + 
			"        <li>Institution</li>\r\n" + 
			"</ul>]]></dynamic-content>\r\n" + 
			"                </dynamic-element>\r\n" + 
			"                <dynamic-element name=\"linkTo\" instance-id=\"ghvx\" type=\"text\" index-type=\"keyword\">\r\n" + 
			"                        <dynamic-element name=\"linkText\" instance-id=\"odsp\" type=\"text\" index-type=\"keyword\">\r\n" + 
			"                                <dynamic-content language-id=\"en_US\"><![CDATA[]]></dynamic-content>\r\n" + 
			"                        </dynamic-element>\r\n" + 
			"                        <dynamic-content language-id=\"en_US\"><![CDATA[]]></dynamic-content>\r\n" + 
			"                </dynamic-element>\r\n" + 
			"                <dynamic-content language-id=\"en_US\"><![CDATA[First and Last Name]]></dynamic-content>\r\n" + 
			"        </dynamic-element>\r\n" + 
			"        <dynamic-element name=\"ItemTitle\" type=\"text\" index-type=\"keyword\" instance-id=\"bkte\">\r\n" + 
			"                <dynamic-element name=\"ItemImage\" instance-id=\"fcvt\" type=\"image\" index-type=\"keyword\">\r\n" + 
			"                        <dynamic-content language-id=\"en_US\" alt=\"[Alternative image description]\" name=\"upload_pic.png\" title=\"upload_pic.png\" type=\"journal\" fileEntryId=\"\" id=\"79097\">/image/journal/article?img_id=79097&amp;t=1522078397841</dynamic-content>\r\n" + 
			"                </dynamic-element>\r\n" + 
			"                <dynamic-element name=\"htmlContent\" instance-id=\"udot\" type=\"text_area\" index-type=\"keyword\">\r\n" + 
			"                        <dynamic-content language-id=\"en_US\"><![CDATA[<ul>\r\n" + 
			"        <li>Title/Role</li>\r\n" + 
			"        <li>Institution</li>\r\n" + 
			"</ul>]]></dynamic-content>\r\n" + 
			"                </dynamic-element>\r\n" + 
			"                <dynamic-element name=\"linkTo\" instance-id=\"qgno\" type=\"text\" index-type=\"keyword\">\r\n" + 
			"                        <dynamic-element name=\"linkText\" instance-id=\"twdh\" type=\"text\" index-type=\"keyword\">\r\n" + 
			"                                <dynamic-content language-id=\"en_US\"><![CDATA[]]></dynamic-content>\r\n" + 
			"                        </dynamic-element>\r\n" + 
			"                        <dynamic-content language-id=\"en_US\"><![CDATA[]]></dynamic-content>\r\n" + 
			"                </dynamic-element>\r\n" + 
			"                <dynamic-content language-id=\"en_US\"><![CDATA[First and Last Name]]></dynamic-content>\r\n" + 
			"        </dynamic-element>\r\n" + 
			"        <dynamic-element name=\"ItemTitle\" type=\"text\" index-type=\"keyword\" instance-id=\"zxqb\">\r\n" + 
			"                <dynamic-element name=\"ItemImage\" instance-id=\"eqye\" type=\"image\" index-type=\"keyword\">\r\n" + 
			"                        <dynamic-content language-id=\"en_US\" alt=\"\" name=\"upload_pic (2).png\" title=\"upload_pic (2).png\" type=\"journal\" fileEntryId=\"\" id=\"79098\">/image/journal/article?img_id=79098&amp;t=1522078397862</dynamic-content>\r\n" + 
			"                </dynamic-element>\r\n" + 
			"                <dynamic-element name=\"htmlContent\" instance-id=\"ugll\" type=\"text_area\" index-type=\"keyword\">\r\n" + 
			"                        <dynamic-content language-id=\"en_US\"><![CDATA[<ul>\r\n" + 
			"        <li>Title/Role</li>\r\n" + 
			"        <li>Institution</li>\r\n" + 
			"</ul>]]></dynamic-content>\r\n" + 
			"                </dynamic-element>\r\n" + 
			"                <dynamic-element name=\"linkTo\" instance-id=\"kumj\" type=\"text\" index-type=\"keyword\">\r\n" + 
			"                        <dynamic-element name=\"linkText\" instance-id=\"iekk\" type=\"text\" index-type=\"keyword\">\r\n" + 
			"                                <dynamic-content language-id=\"en_US\"><![CDATA[]]></dynamic-content>\r\n" + 
			"                        </dynamic-element>\r\n" + 
			"                        <dynamic-content language-id=\"en_US\"><![CDATA[]]></dynamic-content>\r\n" + 
			"                </dynamic-element>\r\n" + 
			"                <dynamic-content language-id=\"en_US\"><![CDATA[First and Last Name]]></dynamic-content>\r\n" + 
			"        </dynamic-element>\r\n" + 
			"</root>";
	
	public static final String TEXT_COLUMN_IMAGE_CONTENT = "<?xml version=\"1.0\"?>\r\n" + 
			"\r\n" + 
			"<root available-locales=\"en_US\" default-locale=\"en_US\">\r\n" + 
			"        <dynamic-element name=\"Heading\" type=\"text\" index-type=\"keyword\" instance-id=\"duaq\">\r\n" + 
			"                <dynamic-element name=\"HeadingBackgroundImage\" instance-id=\"hxfr\" type=\"image\" index-type=\"keyword\">\r\n" + 
			"                        <dynamic-content language-id=\"en_US\" alt=\"\" name=\"matlevel2 (3).jpg\" title=\"matlevel2 (3).jpg\" type=\"journal\" fileEntryId=\"\" id=\"74124\">/image/journal/article?img_id=74124&amp;t=1521742467557</dynamic-content>\r\n" + 
			"                </dynamic-element>\r\n" + 
			"                <dynamic-content language-id=\"en_US\"><![CDATA[Help this Project Succeed]]></dynamic-content>\r\n" + 
			"        </dynamic-element>\r\n" + 
			"        <dynamic-element name=\"FirstParagraph\" type=\"text\" index-type=\"keyword\" instance-id=\"agcn\">\r\n" + 
			"                <dynamic-content language-id=\"en_US\"><![CDATA[I would like to be able to put 3 bullets here that will help describe the type of support this project is seeking. It doesn't appear I can do that from this template.]]></dynamic-content>\r\n" + 
			"        </dynamic-element>\r\n" + 
			"        <dynamic-element name=\"Image18oh\" type=\"image\" index-type=\"keyword\" instance-id=\"vqed\">\r\n" + 
			"                <dynamic-element name=\"ImageCaption\" instance-id=\"nrrf\" type=\"text\" index-type=\"keyword\">\r\n" + 
			"                        <dynamic-content language-id=\"en_US\"><![CDATA[]]></dynamic-content>\r\n" + 
			"                </dynamic-element>\r\n" + 
			"                <dynamic-content language-id=\"en_US\" fileEntryId=\"\"></dynamic-content>\r\n" + 
			"        </dynamic-element>\r\n" + 
			"        <dynamic-element name=\"MainText\" type=\"text_area\" index-type=\"keyword\" instance-id=\"occv\">\r\n" + 
			"                <dynamic-element name=\"HighlightedShortText\" instance-id=\"lclk\" type=\"text\" index-type=\"keyword\">\r\n" + 
			"                        <dynamic-content language-id=\"en_US\"><![CDATA[Contact]]></dynamic-content>\r\n" + 
			"                </dynamic-element>\r\n" + 
			"                <dynamic-content language-id=\"en_US\"><![CDATA[<p><a href=\"mailto:cimitcommunications@partners.org\" target=\"\">Contact</a></p>]]></dynamic-content>\r\n" + 
			"        </dynamic-element>\r\n" + 
			"</root> ";
	
	public static final String FOOTER_CONTENT = "<?xml version=\"1.0\"?>\r\n" + 
			"\r\n" + 
			"<root available-locales=\"en_US\" default-locale=\"en_US\">\r\n" + 
			"	<dynamic-element name=\"ModulesHeading\" type=\"text\" index-type=\"keyword\" instance-id=\"dita\">\r\n" + 
			"		<dynamic-content language-id=\"en_US\"><![CDATA[Contact Us]]></dynamic-content>\r\n" + 
			"	</dynamic-element>\r\n" + 
			"	<dynamic-element name=\"ModuleMainText\" type=\"text\" index-type=\"keyword\" instance-id=\"nwaq\">\r\n" + 
			"		<dynamic-element name=\"ModuleLink\" instance-id=\"ivat\" type=\"text\" index-type=\"keyword\">\r\n" + 
			"			<dynamic-content language-id=\"en_US\"><![CDATA[javascript:void(0);]]></dynamic-content>\r\n" + 
			"		</dynamic-element>\r\n" + 
			"		<dynamic-element name=\"ModuleIcon\" instance-id=\"ltqw\" type=\"list\" index-type=\"keyword\">\r\n" + 
			"			<dynamic-content language-id=\"en_US\"><![CDATA[icon-phone]]></dynamic-content>\r\n" + 
			"		</dynamic-element>\r\n" + 
			"		<dynamic-element name=\"ModuleSecondTextLine\" instance-id=\"zpqt\" type=\"text\" index-type=\"keyword\">\r\n" + 
			"			<dynamic-content language-id=\"en_US\"><![CDATA[617.643.3800]]></dynamic-content>\r\n" + 
			"		</dynamic-element>\r\n" + 
			"		<dynamic-content language-id=\"en_US\"><![CDATA[Phone]]></dynamic-content>\r\n" + 
			"	</dynamic-element>\r\n" + 
			"	<dynamic-element name=\"ModuleMainText\" type=\"text\" index-type=\"keyword\" instance-id=\"lqnj\">\r\n" + 
			"		<dynamic-element name=\"ModuleLink\" instance-id=\"xtqj\" type=\"text\" index-type=\"keyword\">\r\n" + 
			"			<dynamic-content language-id=\"en_US\"><![CDATA[mailto:cimitwebmaster@partners.org]]></dynamic-content>\r\n" + 
			"		</dynamic-element>\r\n" + 
			"		<dynamic-element name=\"ModuleIcon\" instance-id=\"nrtm\" type=\"list\" index-type=\"keyword\">\r\n" + 
			"			<dynamic-content language-id=\"en_US\"><![CDATA[icon-paper-clip]]></dynamic-content>\r\n" + 
			"		</dynamic-element>\r\n" + 
			"		<dynamic-element name=\"ModuleSecondTextLine\" instance-id=\"srrs\" type=\"text\" index-type=\"keyword\">\r\n" + 
			"			<dynamic-content language-id=\"en_US\"><![CDATA[cimitwebmaster@partners.org]]></dynamic-content>\r\n" + 
			"		</dynamic-element>\r\n" + 
			"		<dynamic-content language-id=\"en_US\"><![CDATA[Email]]></dynamic-content>\r\n" + 
			"	</dynamic-element>\r\n" + 
			"	<dynamic-element name=\"ModuleMainText\" type=\"text\" index-type=\"keyword\" instance-id=\"fmdu\">\r\n" + 
			"		<dynamic-element name=\"ModuleLink\" instance-id=\"wynx\" type=\"text\" index-type=\"keyword\">\r\n" + 
			"			<dynamic-content language-id=\"en_US\"><![CDATA[http://cimit.org]]></dynamic-content>\r\n" + 
			"		</dynamic-element>\r\n" + 
			"		<dynamic-element name=\"ModuleIcon\" instance-id=\"hvfd\" type=\"list\" index-type=\"keyword\">\r\n" + 
			"			<dynamic-content language-id=\"en_US\"><![CDATA[icon-home]]></dynamic-content>\r\n" + 
			"		</dynamic-element>\r\n" + 
			"		<dynamic-element name=\"ModuleSecondTextLine\" instance-id=\"pbop\" type=\"text\" index-type=\"keyword\">\r\n" + 
			"			<dynamic-content language-id=\"en_US\"><![CDATA[cimit.org]]></dynamic-content>\r\n" + 
			"		</dynamic-element>\r\n" + 
			"		<dynamic-content language-id=\"en_US\"><![CDATA[CIMIT]]></dynamic-content>\r\n" + 
			"	</dynamic-element>\r\n" + 
			"	<dynamic-element name=\"ModuleMainText\" type=\"text\" index-type=\"keyword\" instance-id=\"aivu\">\r\n" + 
			"		<dynamic-element name=\"ModuleLink\" instance-id=\"msnr\" type=\"text\" index-type=\"keyword\">\r\n" + 
			"			<dynamic-content language-id=\"en_US\"><![CDATA[javascript:void(0);]]></dynamic-content>\r\n" + 
			"		</dynamic-element>\r\n" + 
			"		<dynamic-element name=\"ModuleIcon\" instance-id=\"bptx\" type=\"list\" index-type=\"keyword\">\r\n" + 
			"			<dynamic-content language-id=\"en_US\"><![CDATA[icon-briefcase]]></dynamic-content>\r\n" + 
			"		</dynamic-element>\r\n" + 
			"		<dynamic-element name=\"ModuleSecondTextLine\" instance-id=\"awsr\" type=\"text\" index-type=\"keyword\">\r\n" + 
			"			<dynamic-content language-id=\"en_US\"><![CDATA[125 Nashua Street, Suite 324  |  Boston, MA 02114]]></dynamic-content>\r\n" + 
			"		</dynamic-element>\r\n" + 
			"		<dynamic-content language-id=\"en_US\"><![CDATA[Address]]></dynamic-content>\r\n" + 
			"	</dynamic-element>\r\n" + 
			"</root>";
}
