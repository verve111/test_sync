package com.collaboratelab.project.listener;

import java.util.List;

import org.osgi.service.component.annotations.Component;

import com.collaboratelab.project.constants.Utils;
import com.collaboratelab.project.constants.enums.Enums;
import com.collaboratelab.project.model.Project;
import com.collaboratelab.project.model.ProjectDelivery;
import com.collaboratelab.project.model.ProjectDomain;
import com.collaboratelab.project.model.ProjectPhase;
import com.collaboratelab.project.model.ProjectStep;
import com.collaboratelab.project.model.ProjectStructure;
import com.collaboratelab.project.service.ProjectDeliveryLocalServiceUtil;
import com.collaboratelab.project.service.ProjectDomainLocalServiceUtil;
import com.collaboratelab.project.service.ProjectLocalServiceUtil;
import com.collaboratelab.project.service.ProjectPhaseLocalServiceUtil;
import com.collaboratelab.project.service.ProjectStepLocalServiceUtil;
import com.collaboratelab.project.service.ProjectStructureLocalServiceUtil;
import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.document.library.kernel.service.DLFolderLocalServiceUtil;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalFolder;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.ModelListenerException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModelListener;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.ModelListener;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.GroupLocalServiceUtil;
import com.liferay.trash.kernel.model.TrashEntry;
import com.liferay.trash.kernel.service.TrashEntryLocalServiceUtil;

@Component(
immediate = true,
service = ModelListener.class
)

public class JournalArticleModelListener extends BaseModelListener<JournalArticle> {
	
	private long globalSiteId;
	private static final Log log = LogFactoryUtil.getLog(JournalArticleModelListener.class);
	
	@Override
	public void onBeforeCreate(JournalArticle model) throws ModelListenerException {
		
		if (getGlobalSite() == model.getGroupId()) {
			boolean isCreateDelivery = false;
			
			try {
				isCreateDelivery = isCreateDelivery(model);
			} catch (PortalException e) {
				log.error("isCreateDelivery", e);
			}
			
			if (isCreateDelivery) {
				
				int type = 0;
				try {
					type = getTypeIdProject(model);
				} catch (PortalException e) {
					log.error("getTypeIdProject", e);
				}
				
				List<Project> projectsByType = Utils.getProjectsByType(type);
				for(Project pr : projectsByType) {
					createDelivery(model,pr);
				}
				
			} else {
				// update deliverable folder names for planning and tracking (if needed) on deliverable update 
				try {
					JournalArticle existingDelivery = isUpdateDelivery(model);
					boolean isUpdateDelivery = existingDelivery != null;
					if (isUpdateDelivery) {
						updateFolderNames(existingDelivery, model);
					}
				} catch (PortalException e) {
					log.error("isUpdateDelivery", e);
				}				
				
			}
 		}
		
		super.onAfterCreate(model);
	}
	
	private void updateFolderNames(JournalArticle existingDelivery, JournalArticle updatedDelivery) {
		String existingDeliveryName = Utils.getNameDeliveryFromJournalArticleContent(getGlobalSite(), existingDelivery);
		String updatedDeliveryName = Utils.getNameDeliveryFromJournalArticleContent(getGlobalSite(), updatedDelivery);
		if (!existingDeliveryName.isEmpty() && !updatedDeliveryName.isEmpty()
				&& !existingDeliveryName.equals(updatedDeliveryName)) {
			int type = 0;
			try {
				type = getTypeIdProject(existingDelivery);
			} catch (PortalException e) {
				log.error("getTypeIdProject", e);
			}
			
			List<Project> projectsByType = Utils.getProjectsByType(type);
			for (Project pr : projectsByType) {
				long groupId = pr.getGroupId();
				DLFolder dlParentFolder = DLFolderLocalServiceUtil.fetchFolder(groupId, 0, Utils.DELIVERABLE_FILES_FOLDER);
				if (dlParentFolder != null) {
					DLFolder dlFolder = DLFolderLocalServiceUtil.fetchFolder(groupId, dlParentFolder.getFolderId(),
							existingDeliveryName);
					if (dlFolder != null) {
						dlFolder.setName(updatedDeliveryName);
						DLFolderLocalServiceUtil.updateDLFolder(dlFolder);
						// update planning as well
						DLFolder planningFolder = DLFolderLocalServiceUtil.fetchFolder(groupId, dlParentFolder.getFolderId(),
								existingDeliveryName + Utils.DELIVERABLE_PLANNING_POSTFIX);
						if (planningFolder != null) {
							planningFolder.setName(updatedDeliveryName + Utils.DELIVERABLE_PLANNING_POSTFIX);
							DLFolderLocalServiceUtil.updateDLFolder(planningFolder);
						}
					}
				}
			}
		}
	}

	private long getGlobalSite() {
		if (globalSiteId == 0) {
			long companyId = CompanyThreadLocal.getCompanyId();
			globalSiteId = Utils.getGlobalGroupId(companyId);
		}
		return globalSiteId;
	}
	
	private boolean isCreateDelivery(JournalArticle model) throws PortalException {
		boolean isCreateDelivery = false;
		if (model.getDDMStructure().getNameCurrentValue().equals("Deliverable")) {
		
			JournalFolder folder = model.getFolder();
	
			while (folder.getParentFolderId() != 0) {
				folder = folder.getParentFolder();
			}
	
			for (Enums.TypeProject type : Enums.TypeProject.values())
				if (folder.getName().equals(type.getName()))
					isCreateDelivery = true;
	
			List<JournalArticle> journals = JournalArticleLocalServiceUtil.getArticles(model.getGroupId(),
					model.getFolderId());
	
			for (JournalArticle j : journals) {
	
				if (model.getArticleId().equals(j.getArticleId())) {
					isCreateDelivery = false;
				}
	
			}
		}

		return isCreateDelivery;
	}
	
	private boolean isDeleteDelivery(JournalArticle model) throws PortalException {
		if (model.getDDMStructure().getNameCurrentValue().equals("Deliverable")) {
			JournalFolder folder = model.getFolder();
			while (folder.getParentFolderId() != 0) {
				folder = folder.getParentFolder();
			}
			for (Enums.TypeProject type : Enums.TypeProject.values())
				if (folder.getName().equals(type.getName()))
					return true;
		}
		return false;
	}
	
	private JournalArticle isUpdateDelivery(JournalArticle model) throws PortalException {
		if (model.getDDMStructure().getNameCurrentValue().equals("Deliverable")) {

			JournalFolder folder = model.getFolder();

			while (folder.getParentFolderId() != 0) {
				folder = folder.getParentFolder();
			}

			for (Enums.TypeProject type : Enums.TypeProject.values()) {
				if (folder.getName().equals(type.getName())) {

					List<JournalArticle> journals = JournalArticleLocalServiceUtil.getArticles(model.getGroupId(),
							model.getFolderId());

					for (JournalArticle j : journals) {

						if (model.getArticleId().equals(j.getArticleId())) {
							return j;
						}
					}
				}
			}
		}

		return null;
	}
		
	
	private int getTypeIdProject(JournalArticle model) throws PortalException {
		
		JournalFolder folder = model.getFolder();
		
		while(folder.getParentFolderId() != 0) {	
			folder = folder.getParentFolder();
		}
		
		for(Enums.TypeProject type : Enums.TypeProject.values())
			if(folder.getName().equals(type.getName()))
				return type.getId();
		
		return 0;
	}
	
	private String getPhaseIdentifierFromModel(JournalArticle model) throws PortalException {
		
		JournalFolder folder = model.getFolder();
		
		for(Enums.PHASE_ENUM  phase : Enums.PHASE_ENUM.values())
			if(folder.getName().equals(phase.getIdentifier()))
				return phase.getPhase();
		
		return "";
	}
	
	private String getDomainIdentifierFromModel(JournalArticle model) throws PortalException {
		
		JournalFolder folder = model.getFolder().getParentFolder();
		
		for(Enums.DOMAIN_ENUM  domain : Enums.DOMAIN_ENUM.values())
			if(folder.getName().equals(domain.getIdentifier()))
				return domain.getDomain();
		
		return "";
	}
	
	private void createDelivery(JournalArticle model, Project project) {
		
		String domainName = "";
		String phaseName = "";
		try {
			phaseName = getPhaseIdentifierFromModel(model);
			domainName = getDomainIdentifierFromModel(model);
		} catch (PortalException e) {
			e.printStackTrace();
		}
		
		if( !domainName.equals("") && !phaseName.equals("")) {
			
			long deliveryId = CounterLocalServiceUtil.increment();
			ProjectDelivery delivery = ProjectDeliveryLocalServiceUtil.createProjectDelivery(deliveryId);
			
			ProjectStructure ps = ProjectStructureLocalServiceUtil.findByProjectId(project.getProjectId());
			
			ProjectDomain domain = ProjectDomainLocalServiceUtil.findByProjectStructureIdIdentifier(ps.getProjectStructureId(), domainName);
			ProjectPhase phase = ProjectPhaseLocalServiceUtil.findByProjectStructureIdIdentifier(ps.getProjectStructureId(), phaseName);
			
			ProjectStep step = getStep(project.getProjectId(), domain.getProjectDomainId(), phase.getProjectPhaseId());
			
			//step == null for borken projects
			if (step == null) {
				return;
			}			
			//ProjectStep step = ProjectStepLocalServiceUtil.findByProjectIdProjectDomainIdProjectPhaseId(project.getProjectId(), domain.getProjectDomainId(), phase.getProjectPhaseId());
			
			//delivery.setUserName(user.getScreenName());
			//delivery.setUserId(user.getUserId());
			delivery.setStepId(step.getProjectStepId());
			delivery.setType(Integer.parseInt(model.getArticleId()));
			delivery.setDomain(domain.getIdentifier());
			delivery.setPhase(phase.getIdentifier());
			delivery.setCost(0);
			delivery.setComment("");
			delivery.setReviewerComment("");
			delivery.setReviewerIsCompleted(false);
			delivery.setReviewerRate(0);
			delivery.setStatus(Enums.Status.ZERO.getId());
			ProjectDeliveryLocalServiceUtil.addProjectDelivery(delivery);
		}
			
	}
	
	private ProjectStep getStep(long prjId, long domainId, long phaseId) {
		DynamicQuery steps = ProjectStepLocalServiceUtil.dynamicQuery()
				.add(RestrictionsFactoryUtil.eq("projectId", prjId))
				.add(RestrictionsFactoryUtil.eq("projectDomainId", domainId))
				.add(RestrictionsFactoryUtil.eq("projectPhaseId", phaseId));

		List<ProjectStep> stepList = ProjectLocalServiceUtil.dynamicQuery(steps);
		
		if (stepList.size() != 1) {
			return null;
		}
		return stepList.get(0);
	}
	
	
	private void deleteDelivery(JournalArticle model, Project project) {
		
		String domainName = "";
		String phaseName = "";
		try {
			phaseName = getPhaseIdentifierFromModel(model);
			domainName = getDomainIdentifierFromModel(model);
		} catch (PortalException e) {
			e.printStackTrace();
		}
		
		if(!domainName.equals("") && !phaseName.equals("")) {
			
			//long deliveryId = CounterLocalServiceUtil.increment();
			//ProjectDelivery delivery = ProjectDeliveryLocalServiceUtil.createProjectDelivery(deliveryId);
			
			ProjectStructure ps = ProjectStructureLocalServiceUtil.findByProjectId(project.getProjectId());
			
			ProjectDomain domain = ProjectDomainLocalServiceUtil.findByProjectStructureIdIdentifier(ps.getProjectStructureId(), domainName);
			ProjectPhase phase = ProjectPhaseLocalServiceUtil.findByProjectStructureIdIdentifier(ps.getProjectStructureId(), phaseName);
			ProjectStep step = getStep(project.getProjectId(), domain.getProjectDomainId(), phase.getProjectPhaseId());
			
			//step == null for borken projects
			if (step == null) {
				return;
			}
			
			//step = ProjectStepLocalServiceUtil.findByProjectIdProjectDomainIdProjectPhaseId(project.getProjectId(), domain.getProjectDomainId(), phase.getProjectPhaseId());
			
			List<ProjectDelivery> deliveries = ProjectDeliveryLocalServiceUtil.findByStepId(step.getProjectStepId());
			
			long globalGroupId = getGlobalSite();
			String modelName = Utils.getNameDeliveryFromJournalArticleContent(globalGroupId,model.getArticleId());
			for(ProjectDelivery del : deliveries) {	
				String name = Utils.getNameDeliveryFromJournalArticleContent(globalGroupId,String.valueOf(del.getType()));
				if(name.equals(modelName)) {
					try {
						ProjectDeliveryLocalServiceUtil.deleteProjectDelivery(del.getDeliveryId());
					} catch (PortalException e) {
						log.error("deleteDelivery ex:: ", e);
					}
				}
				
			}
		}
			
	}
	
	@Override
	public void onBeforeRemove(JournalArticle model) throws ModelListenerException {
		
		if (getGlobalSite() == model.getGroupId()) {
			boolean isDeleteDelivery = false;
			try {
				isDeleteDelivery = isDeleteDelivery(model);
			} catch (PortalException e) {
				e.printStackTrace();
			}
			
			if(isDeleteDelivery) {
				int type = 0;
				try {
					type = getTypeIdProject(model);
				} catch (PortalException e) {
					e.printStackTrace();
				}
				
				List<Project> projectsByType = Utils.getProjectsByType(type);
				for (Project pr : projectsByType) {
					long grId = pr.getGroupId();
					Group group = GroupLocalServiceUtil.fetchGroup(grId);
					if (group != null) {
						deleteDelivery(model, pr);
					}
				}
			}
		}
		super.onBeforeRemove(model);		
	}
	
}
