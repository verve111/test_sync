package com.collaboratelab.project.portlet.display.context;

import javax.portlet.PortletURL;
import javax.servlet.http.HttpServletRequest;

import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalServiceUtil;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.portlet.LiferayPortletRequest;
import com.liferay.portal.kernel.portlet.LiferayPortletResponse;
import com.liferay.portal.kernel.service.GroupServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;


public class ProjectsDisplayContext  {

	public ProjectsDisplayContext(HttpServletRequest request, LiferayPortletRequest liferayPortletRequest,
			LiferayPortletResponse liferayPortletResponse) throws PortalException {
		
		_request = request;
		_liferayPortletRequest = liferayPortletRequest;
		_liferayPortletResponse = liferayPortletResponse;
		
	}
	
	public String getDisplayStyle() {
		if (Validator.isNotNull(_displayStyle)) {
			return _displayStyle;
		}
		_displayStyle = "list";
		return _displayStyle;
	}
	
	public Group getGroup() throws PortalException {
		long groupId = getGroupId();

		if (groupId > 0) {
			_group = GroupServiceUtil.getGroup(groupId);
		}

		return _group;
	}
	
	
	public static String fetchHTMLMainJournalArticle(ThemeDisplay themeDisplay) {
		
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		
		//DDMTemplate ddmTemplate = //ddmTemplateRegistry.get(key);
			
		String templateName = "";//getWcmTemplateName(referenceCommunityType,templateType);
			
		DDMTemplate ddmTemplate = DDMTemplateLocalServiceUtil.fetchDDMTemplate(34410L);//getAllGlobalDDMTemplates(community.getCompanyId());
			
		String articleURL = "article_maturity";//getWcmJournalArticleName(referenceCommunityType, community); 
		
		
		JournalArticle journalArticle =  JournalArticleLocalServiceUtil.fetchArticleByUrlTitle(ddmTemplate.getGroupId(), articleURL);
		
		
		if ( journalArticle == null){
			return null;
		} else {
			try {
				return JournalArticleLocalServiceUtil.getArticleContent(journalArticle, ddmTemplate.getTemplateKey(), Constants.VIEW, themeDisplay.getLanguageId(), themeDisplay);
			} catch (PortalException e) {
				
			}
			return null;
		}
		
		
	}
	
	/* public SearchContainer<Group> getSearchContainer() throws PortalException {
		return _groupSearchProvider.getGroupSearch(
			_liferayPortletRequest, getPortletURL());
	}*/
	
	public PortletURL getPortletURL() throws PortalException {
		PortletURL portletURL = _liferayPortletResponse.createRenderURL();

		portletURL.setParameter("groupId", String.valueOf(getGroupId()));
		portletURL.setParameter("displayStyle", getDisplayStyle());

		return portletURL;
	}

	public long getGroupId() {
		ThemeDisplay themeDisplay = (ThemeDisplay) _request.getAttribute(WebKeys.THEME_DISPLAY);
		return themeDisplay.getScopeGroupId();
	}
	
	
	private String _displayStyle;
	private Group _group;
	private final LiferayPortletRequest _liferayPortletRequest;
	private final LiferayPortletResponse _liferayPortletResponse;
	protected final HttpServletRequest _request;
}