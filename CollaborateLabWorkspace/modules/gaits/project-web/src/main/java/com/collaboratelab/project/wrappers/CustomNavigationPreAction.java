package com.collaboratelab.project.wrappers;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.events.LifecycleAction;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.service.LayoutServiceUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;

@Component(property = { "key=servlet.service.events.pre" }, service = LifecycleAction.class)
public class CustomNavigationPreAction extends Action {

	@Override
	public void run(HttpServletRequest request, HttpServletResponse response) throws ActionException {
		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
			final long scopeGroupId = themeDisplay.getScopeGroupId();
			long companyId = themeDisplay.getCompanyId();
			Layout layout = themeDisplay.getLayout();
			List<Layout> layouts = getLayouts(scopeGroupId, layout.isPublicLayout(), companyId);
			if (!layouts.isEmpty()) {
				themeDisplay.setLayouts(layouts);
			}
		} catch (SystemException e1) {
			e1.printStackTrace();
		} catch (Exception ignore) {
			ignore.printStackTrace();
		}
	}

	private LinkedList<Layout> getLayouts(final long scopeGroupId, boolean publicLayout, long companyId)
			throws PortalException, SystemException {
		LinkedList<Layout> layouts = new LinkedList<Layout>();
		layouts.addAll(LayoutServiceUtil.getLayouts(scopeGroupId, true, 0L));
		layouts.addAll(LayoutServiceUtil.getLayouts(scopeGroupId, false, 0L));

		// put project-profile after Files tab
		if (layouts.size() == 7) {
			if (layouts.get(6).getFriendlyURL().endsWith("project-profile")) {
				layouts.add(3, layouts.get(6));
				layouts.removeLast();
			}
		}
		return layouts;
	}
}
