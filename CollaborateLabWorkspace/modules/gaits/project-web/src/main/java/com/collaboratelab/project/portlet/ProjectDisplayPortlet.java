package com.collaboratelab.project.portlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

import com.collaboratelab.project.constants.ProjectDisplayPortletKeys;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.gaits",
		"com.liferay.portlet.instanceable=false",
		"javax.portlet.display-name=Project Display Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + ProjectDisplayPortletKeys.PROJECT_DISPLAY_PORTLET,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)

public class ProjectDisplayPortlet extends MVCPortlet {
	
	
}