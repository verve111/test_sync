package com.collaboratelab.project.portlet.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletPreferences;
import javax.portlet.ReadOnlyException;

import com.liferay.portal.kernel.portlet.PortletPreferencesFactoryUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.collaboratelab.project.constants.Constants;
import com.collaboratelab.project.constants.ProjectDisplayPortletKeys;
import com.collaboratelab.project.constants.Utils;
import com.collaboratelab.project.constants.enums.Enums;
import com.collaboratelab.project.model.Project;
import com.collaboratelab.project.model.ProjectDelivery;
import com.collaboratelab.project.model.ProjectDomain;
import com.collaboratelab.project.model.ProjectPhase;
import com.collaboratelab.project.model.ProjectStep;
import com.collaboratelab.project.model.ProjectStructure;
import com.collaboratelab.project.service.ProjectDeliveryLocalServiceUtil;
import com.collaboratelab.project.service.ProjectDomainLocalServiceUtil;
import com.collaboratelab.project.service.ProjectLocalServiceUtil;
import com.collaboratelab.project.service.ProjectPhaseLocalServiceUtil;
import com.collaboratelab.project.service.ProjectStepLocalServiceUtil;
import com.collaboratelab.project.service.ProjectStructureLocalServiceUtil;
import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.document.library.kernel.model.DLFolderConstants;
import com.liferay.document.library.kernel.service.DLFolderLocalServiceUtil;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalFolder;
import com.liferay.journal.model.JournalFolderConstants;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.journal.service.JournalContentSearchLocalServiceUtil;
import com.liferay.journal.service.JournalFolderLocalServiceUtil;
import com.liferay.journal.util.JournalConverter;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.GroupConstants;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.LayoutConstants;
import com.liferay.portal.kernel.model.LayoutSetPrototype;
import com.liferay.portal.kernel.model.LayoutTypePortlet;
import com.liferay.portal.kernel.model.ResourceConstants;
import com.liferay.portal.kernel.model.RoleConstants;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.service.GroupLocalServiceUtil;
import com.liferay.portal.kernel.service.GroupService;
import com.liferay.portal.kernel.service.LayoutLocalServiceUtil;
import com.liferay.portal.kernel.service.PortletPreferencesLocalServiceUtil;
import com.liferay.portal.kernel.service.ResourceActionLocalServiceUtil;
import com.liferay.portal.kernel.service.ResourcePermissionLocalServiceUtil;
import com.liferay.portal.kernel.service.RoleLocalServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PortletKeys;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.social.kernel.model.SocialActivity;
import com.liferay.social.kernel.model.SocialActivitySet;
import com.liferay.social.kernel.service.SocialActivityLocalServiceUtil;
import com.liferay.social.kernel.service.SocialActivitySetLocalServiceUtil;

@Component(property = { "javax.portlet.name=" + ProjectDisplayPortletKeys.PROJECT_DISPLAY_PORTLET,
		"mvc.command.name=create_pr" }, service = MVCActionCommand.class)

public class CreateProjectsStructureActionCommand extends BaseMVCActionCommand {

	private static final Log logger = LogFactoryUtil.getLog(CreateProjectsStructureActionCommand.class);

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);

		long userId = PortalUtil.getUserId(actionRequest);

		long companyId = CompanyThreadLocal.getCompanyId();

		Map<Locale, String> nameMap = null;
		Map<Locale, String> descriptionMap = null;
		long globalSiteId = Utils.getGlobalGroupId(companyId);
	    String structureKey = Utils.getDDMStructureKeyDeliveryWWH(globalSiteId);
	    String templateKey = Utils.getDDMTemplateKeyDeliveryWWH(globalSiteId);		

		int type = 0;
		String friendlyURL = null;
		boolean inheritContent = false;
		boolean active = true;
		int membershipRestriction = GroupConstants.DEFAULT_MEMBERSHIP_RESTRICTION;

		ServiceContext serviceContext = ServiceContextFactory.getInstance(Group.class.getName(), actionRequest);
		ServiceContextThreadLocal.pushServiceContext(serviceContext);

		long liveGroupId = CounterLocalServiceUtil.increment();

		Group liveGroup = GroupLocalServiceUtil.createGroup(liveGroupId);

		nameMap = LocalizationUtil.getLocalizationMap(actionRequest, "name");
		descriptionMap = LocalizationUtil.getLocalizationMap(actionRequest, "description");
		type = GroupConstants.TYPE_SITE_RESTRICTED;
		int typeProject = ParamUtil.getInteger(actionRequest, "typeProject");

		friendlyURL = "/-" + nameMap.get(LocaleUtil.US);
		boolean manualMembership = true;

		inheritContent = ParamUtil.getBoolean(actionRequest, "inheritContent");

		/* Create Project */

		liveGroup = groupService.addGroup(0, GroupConstants.DEFAULT_LIVE_GROUP_ID, nameMap, descriptionMap, type,
				manualMembership, membershipRestriction, friendlyURL, true, inheritContent, active, serviceContext);
		 
		try {
			createStructureInDB(themeDisplay, liveGroup.getGroupId(), typeProject);
		} catch (Exception e) {
			logger.error(e);
		}
		
		//createJournalArticleDeliveries(userId, globalSiteId, structureKey, templateKey);		

		/* Set template project on the Project 

		LayoutSetPrototype projectTemplate = getProjectTemplate(companyId);

		String layoutSetPrototypeUuid = projectTemplate.getUuid();

		try {
			LayoutSetLocalServiceUtil.updateLayoutSetPrototypeLinkEnabled(liveGroup.getGroupId(), false, true,
					layoutSetPrototypeUuid);
			LayoutLocalServiceUtil.updatePriorities(liveGroup.getGroupId(), false);
		} catch (PortalException e) {
			e.printStackTrace();
		}*/

		addLayoutsPageOnProjects(null, liveGroup, userId, serviceContext);
		
		// clear social activities
		clearRecentSocialActivities(liveGroup.getGroupId());
	}
	
	
	// remove activities that were created during site creation (e.g. when resources were populated)
	private void clearRecentSocialActivities(long groupId) {
		for (SocialActivitySet set : SocialActivitySetLocalServiceUtil.getGroupActivitySets(groupId, -1, -1)) {
			for (SocialActivity activity : SocialActivityLocalServiceUtil.getActivitySetActivities(set.getActivitySetId(), -1, -1)) {
				try {
					SocialActivityLocalServiceUtil.deleteSocialActivity(activity.getActivityId());
				} catch (PortalException e) {
					logger.error("error while deleting Social Activity", e);
				}
			}
			SocialActivitySetLocalServiceUtil.deleteSocialActivitySet(set);
		}
	}	
	

	/*private static LayoutSetPrototype getProjectTemplate(long companyId) {

		List<LayoutSetPrototype> layoutSetPrototypes = LayoutSetPrototypeLocalServiceUtil
				.getLayoutSetPrototypes(companyId);
		LayoutSetPrototype projectTemplate = null;
		for (LayoutSetPrototype layoutSetPrototype : layoutSetPrototypes) {
			if (layoutSetPrototype.getName(LocaleUtil.US).equalsIgnoreCase("ProjectTemplate")) {
				projectTemplate = layoutSetPrototype;
				break;
			}
		}

		return projectTemplate;
	}*/
	
	private void applyTheme(Layout layout) {
		try {
			LayoutLocalServiceUtil.updateLookAndFeel(layout.getGroupId(), layout.isPrivateLayout(), layout.getLayoutId(),
					"gaitstheme_WAR_gaitstheme", "01", StringPool.BLANK);
		} catch (PortalException e) {
			logger.error("theme can not be applied", e);
		}
	}

	private void addLayoutsPageOnProjects(LayoutSetPrototype siteTemplate, Group liveGroup, long userId,
			ServiceContext serviceContext) {
		
		
		/***********************************************
		 *********************************************** 
		 *
		 * HOME
		 *
		 ***********************************************
		 ***********************************************/
		{
			if (true /*siteTemplate != null*/) {
				Layout activityLayout = null;

				try {
					activityLayout = LayoutLocalServiceUtil.addLayout(userId, liveGroup.getGroupId(), true, 0L,
							"Home", "Home", "", LayoutConstants.TYPE_PORTLET, false, null, serviceContext);
				} catch (PortalException e1) {
					e1.printStackTrace();
				}

				String typeSettings;

				typeSettings = "layout-template-id=1_2_columns_i \n";
				//typeSettings += "column-2=" + "com_liferay_social_activities_web_portlet_SocialActivitiesPortlet" + "\n";
				//typeSettings += "column-3=" + "com_liferay_message_boards_web_portlet_MBPortlet" + "\n";
				
				//typeSettings += "column-1=" + "com_liferay_journal_content_web_portlet_JournalContentPortlet" + "\n";
				
				activityLayout.setTypeSettings(typeSettings);
				LayoutLocalServiceUtil.updateLayout(activityLayout);
				
				LayoutTypePortlet layoutTypePortlet = null;
				try {
					layoutTypePortlet = (LayoutTypePortlet) activityLayout.getLayoutType();
					//layoutTypePortlet.setLayoutTemplateId(userId, siteTemplate.getUuid());
					
					activityLayout = LayoutLocalServiceUtil.updateLayout(activityLayout.getGroupId(),
							activityLayout.getPrivateLayout(), activityLayout.getLayoutId(),
							activityLayout.getTypeSettings());
					layoutTypePortlet = (LayoutTypePortlet) activityLayout.getLayoutType();
					//LayoutLocalServiceUtil.updatePriority(activityLayout, 0);
					layoutTypePortlet = (LayoutTypePortlet) activityLayout.getLayoutType();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				try {
					updateProjectLayoutWithHeader(userId, liveGroup.getGroupId(), liveGroup.getCompanyId(),
							activityLayout);
				} catch (PortalException e) {
					e.printStackTrace();
				} catch (ReadOnlyException e) {
					e.printStackTrace();
				}
				List<String> actionsResource = new ArrayList<String>();
				actionsResource.add("ADD_TO_PAGE");
				ResourceActionLocalServiceUtil.checkResourceActions("com_liferay_social_activities_web_portlet_SocialActivitiesPortlet",actionsResource, true);
				
				String activityPortletId = layoutTypePortlet.addPortletId(userId, "com_liferay_social_activities_web_portlet_SocialActivitiesPortlet", "column-2", -1,
						true);
				
				String messegeBoardPortletId = layoutTypePortlet.addPortletId(userId, "com_liferay_message_boards_web_portlet_MBPortlet", "column-3", -1,
						true);

				// update layout (important)
				try {
					activityLayout = LayoutLocalServiceUtil.updateLayout(activityLayout.getGroupId(),
							activityLayout.isPrivateLayout(), activityLayout.getLayoutId(),
							activityLayout.getTypeSettings());
					applyTheme(activityLayout);
				} catch (PortalException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				

			}

		}
		

		/***********************************************
		 *********************************************** 
		 *
		 * MATURITY (PLAN/TRACK)
		 *
		 ***********************************************
		 ***********************************************/
		{
			if (true /*siteTemplate != null*/) {
				Layout activityLayout = null;

				try {
					activityLayout = LayoutLocalServiceUtil.addLayout(userId, liveGroup.getGroupId(), true, 0L,
							"Plan/Track", "Plan-Track", "", LayoutConstants.TYPE_PORTLET, false, null, serviceContext);
				} catch (PortalException e1) {
					e1.printStackTrace();
				}

				String typeSettings;

				typeSettings = "layout-template-id=1_2_columns_ii ";
				//typeSettings += "column-2=" + "com_liferay_asset_publisher_web_portlet_AssetPublisherPortlet" + "\n";
				//typeSettings += "column-3=" + "CategoryFiltersPortlet" + "\n";
				
				activityLayout.setTypeSettings(typeSettings);
				LayoutLocalServiceUtil.updateLayout(activityLayout);

				LayoutTypePortlet layoutTypePortlet = null;
				try {
					layoutTypePortlet = (LayoutTypePortlet) activityLayout.getLayoutType();
					//layoutTypePortlet.setLayoutTemplateId(userId, siteTemplate.getUuid());

					activityLayout = LayoutLocalServiceUtil.updateLayout(activityLayout.getGroupId(),
							activityLayout.getPrivateLayout(), activityLayout.getLayoutId(),
							activityLayout.getTypeSettings());
					layoutTypePortlet = (LayoutTypePortlet) activityLayout.getLayoutType();
					//LayoutLocalServiceUtil.updatePriority(activityLayout, 4);
					layoutTypePortlet = (LayoutTypePortlet) activityLayout.getLayoutType();
				} catch (Exception e) {
					e.printStackTrace();
				}

				String maturityPortletId = layoutTypePortlet.addPortletId(userId, "MaturityDisplay", "column-1", -1, true);

				String assetPortletId = layoutTypePortlet.addPortletId(userId,
						"com_liferay_asset_publisher_web_portlet_AssetPublisherPortlet", "column-2", -1, true);

				String filterPortletId = layoutTypePortlet.addPortletId(userId, "CategoryFiltersPortlet", "column-3",
						-1, true);	
				
				boolean isUpdatePortletProps = true;
				if (isUpdatePortletProps) {
					long ownerId = PortletKeys.PREFS_OWNER_ID_DEFAULT;
					int ownerType = PortletKeys.PREFS_OWNER_TYPE_LAYOUT;
					//PortletPreferences prefsMaturity = PortletPreferencesLocalServiceUtil.getPreferences(liveGroup.getCompanyId(), ownerId, ownerType,
						//	activityLayout.getPlid(), maturityPortletId);
					PortletPreferences prefsAssetPub = PortletPreferencesLocalServiceUtil.getPreferences(liveGroup.getCompanyId(), ownerId, ownerType,
							activityLayout.getPlid(), assetPortletId);
					PortletPreferences prefsFilter = PortletPreferencesLocalServiceUtil.getPreferences(liveGroup.getCompanyId(), ownerId, ownerType,
							activityLayout.getPlid(), filterPortletId);						
					try {
						//prefsMaturity.setValue("portletSetupPortletDecoratorId", "barebone");
						prefsAssetPub.setValue("portletSetupTitle_en_US", "Resources");
						prefsAssetPub.setValue("portletSetupUseCustomTitle", "true");
						prefsAssetPub.setValue("enableRatings", "true");
						//enable conversion to pdf (for view button) 
						prefsAssetPub.setValue("extensions", "pdf");
						prefsFilter.setValue("portletSetupTitle_en_US", "Filters");
						prefsFilter.setValue("portletSetupUseCustomTitle", "true");						
						//PortletPreferencesLocalServiceUtil.updatePreferences(ownerId, ownerType,
							//	activityLayout.getPlid(), maturityPortletId, prefsMaturity);
						PortletPreferencesLocalServiceUtil.updatePreferences(ownerId, ownerType,
								activityLayout.getPlid(), assetPortletId, prefsAssetPub);
						PortletPreferencesLocalServiceUtil.updatePreferences(ownerId, ownerType,
								activityLayout.getPlid(), filterPortletId, prefsFilter);
						
					} catch (ReadOnlyException e) {
						logger.error("ReadOnlyException", e);
					}
				}

				// update layout (important)
				try {
					activityLayout = LayoutLocalServiceUtil.updateLayout(activityLayout.getGroupId(),
							activityLayout.isPrivateLayout(), activityLayout.getLayoutId(),
							activityLayout.getTypeSettings());
					applyTheme(activityLayout);
				} catch (PortalException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				/*
				 * try { updateProjectLayoutWithFooter(userId,
				 * liveGroup.getGroupId(), liveGroup.getCompanyId(),
				 * activityLayout); } catch (PortalException e) {
				 * e.printStackTrace(); } catch (ReadOnlyException e) {
				 * e.printStackTrace(); }
				 */

			}

		}
		
		/***********************************************
		 *********************************************** 
		 *
		 * IMPACT
		 *
		 ***********************************************
		 ***********************************************/
		{
			if (true/*siteTemplate == null*/) {
				Layout activityLayout = null;

				try {
					activityLayout = LayoutLocalServiceUtil.addLayout(userId, liveGroup.getGroupId(), true, 0L,
							"Impact", "Impact", "", LayoutConstants.TYPE_PORTLET, false, null, serviceContext);
				} catch (PortalException e1) {
					e1.printStackTrace();
				}

				String typeSettings;

				typeSettings = "layout-template-id=1_column";
				activityLayout.setTypeSettings(typeSettings);
				LayoutLocalServiceUtil.updateLayout(activityLayout);

				LayoutTypePortlet layoutTypePortlet = null;
				try {
					layoutTypePortlet = (LayoutTypePortlet) activityLayout.getLayoutType();
					activityLayout = LayoutLocalServiceUtil.updateLayout(activityLayout.getGroupId(),
							activityLayout.getPrivateLayout(), activityLayout.getLayoutId(),
							activityLayout.getTypeSettings());
					layoutTypePortlet = (LayoutTypePortlet) activityLayout.getLayoutType();
					//LayoutLocalServiceUtil.updatePriority(activityLayout, 5);
					layoutTypePortlet = (LayoutTypePortlet) activityLayout.getLayoutType();
				} catch (Exception e) {
					e.printStackTrace();
				}

				String impactPortletId = layoutTypePortlet.addPortletId(userId, "ImpactWeb", "column-1", -1,
						true);

				// update layout (important)
				try {
					activityLayout = LayoutLocalServiceUtil.updateLayout(activityLayout.getGroupId(),
							activityLayout.isPrivateLayout(), activityLayout.getLayoutId(),
							activityLayout.getTypeSettings());
					applyTheme(activityLayout);					
				} catch (PortalException e1) {
					e1.printStackTrace();
				}
				/*
				 * try { updateProjectLayoutWithFooter(userId,
				 * liveGroup.getGroupId(), liveGroup.getCompanyId(),
				 * activityLayout); } catch (PortalException e) {
				 * e.printStackTrace(); } catch (ReadOnlyException e) {
				 * e.printStackTrace(); }
				 */

			}
		}
		
		/***********************************************
		 *********************************************** 
		 *
		 * FILES
		 *
		 ***********************************************
		 ***********************************************/
		{
			if (true /*siteTemplate != null*/) {
				Layout activityLayout = null;
				
				try {
					activityLayout = LayoutLocalServiceUtil.addLayout(userId, liveGroup.getGroupId(), true, 0L,
							"Files", "Files", "", LayoutConstants.TYPE_PORTLET, false, null, serviceContext);
				} catch (PortalException e1) {
					e1.printStackTrace();
				}
				
				String typeSettings;

				typeSettings = "layout-template-id=1_column";
				activityLayout.setTypeSettings(typeSettings);
				LayoutLocalServiceUtil.updateLayout(activityLayout);
				
				long userRoleId = 0L;
				try {
					userRoleId = RoleLocalServiceUtil.getRole(activityLayout.getCompanyId(), "Guest").getRoleId();
				} catch (PortalException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				
				//try {
					//ResourcePermissionLocalServiceUtil.removeResourcePermission(
						//	activityLayout.getCompanyId(),
						//	activityLayout.getModelClassName(),
						 //   ResourceConstants.SCOPE_INDIVIDUAL,
						  //  String.valueOf(activityLayout.getPrimaryKey()),
						   // userRoleId,
						   // ActionKeys.VIEW
				//	);
				//} catch (PortalException e2) {
					// TODO Auto-generated catch block
					//e2.printStackTrace();
			//	}
				
				LayoutTypePortlet layoutTypePortlet = null;
				try {
					layoutTypePortlet = (LayoutTypePortlet) activityLayout.getLayoutType();
					//layoutTypePortlet.setLayoutTemplateId(userId, siteTemplate.getUuid());

					activityLayout = LayoutLocalServiceUtil.updateLayout(activityLayout.getGroupId(),
							activityLayout.getPrivateLayout(), activityLayout.getLayoutId(),
							activityLayout.getTypeSettings());
					layoutTypePortlet = (LayoutTypePortlet) activityLayout.getLayoutType();
					//LayoutLocalServiceUtil.updatePriority(activityLayout, 1);
					layoutTypePortlet = (LayoutTypePortlet) activityLayout.getLayoutType();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				
				
				String filesPortletId = layoutTypePortlet.addPortletId(userId, "com_liferay_document_library_web_portlet_DLPortlet", "column-1", -1,
						true);

				// update layout (important)
				try {
					activityLayout = LayoutLocalServiceUtil.updateLayout(activityLayout.getGroupId(),
							activityLayout.isPrivateLayout(), activityLayout.getLayoutId(),
							activityLayout.getTypeSettings());
					applyTheme(activityLayout);
				} catch (PortalException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			

			}

		}
		
		/***********************************************
		 *********************************************** 
		 *
		 * PROJECT PROFILE
		 *
		 ***********************************************
		 ***********************************************/
		{
			if (true /*siteTemplate != null*/) {
				Layout activityLayout = null;

				try {
					activityLayout = LayoutLocalServiceUtil.addLayout(userId, liveGroup.getGroupId(), true, 0L,
							"Project Profile", "Project Profile", "", LayoutConstants.TYPE_PORTLET, false, null, serviceContext);
				} catch (PortalException e1) {
					e1.printStackTrace();
				}

				String typeSettings;

				typeSettings = "layout-template-id=1_column";
				activityLayout.setTypeSettings(typeSettings);
				LayoutLocalServiceUtil.updateLayout(activityLayout);
				
				LayoutTypePortlet layoutTypePortlet = null;
				try {
					layoutTypePortlet = (LayoutTypePortlet) activityLayout.getLayoutType();
					//layoutTypePortlet.setLayoutTemplateId(userId, siteTemplate.getUuid());

					activityLayout = LayoutLocalServiceUtil.updateLayout(activityLayout.getGroupId(),
							activityLayout.getPrivateLayout(), activityLayout.getLayoutId(),
							activityLayout.getTypeSettings());
					layoutTypePortlet = (LayoutTypePortlet) activityLayout.getLayoutType();
					//LayoutLocalServiceUtil.updatePriority(activityLayout, 2);
					layoutTypePortlet = (LayoutTypePortlet) activityLayout.getLayoutType();
				} catch (Exception e) {
					e.printStackTrace();
				}

				long companyId = CompanyThreadLocal.getCompanyId();

				
				long globalSiteId = Utils.getGlobalGroupId(companyId);
				try {
					updateProjectLayoutWithProjectProfileContent(globalSiteId,userId, liveGroup.getGroupId(), liveGroup.getCompanyId(),
							activityLayout);
				} catch (PortalException e) {
					e.printStackTrace();
				} catch (ReadOnlyException e) {
					e.printStackTrace();
				}
				
				
				//String filesPortletId = layoutTypePortlet.addPortletId(userId, "com_liferay_asset_publisher_web_portlet_AssetPublisherPortlet", "column-1", -1,
				//		true);

				// update layout (important)
				try {
					activityLayout = LayoutLocalServiceUtil.updateLayout(activityLayout.getGroupId(),
							activityLayout.isPrivateLayout(), activityLayout.getLayoutId(),
							activityLayout.getTypeSettings());
				} catch (PortalException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			
			}

		}
		
		
		/***********************************************
		 *********************************************** 
		 *
		 * MEMBERS
		 *
		 ***********************************************
		 ***********************************************/
		{
			if (true /*siteTemplate != null*/) {
				Layout activityLayout = null;

				try {
					activityLayout = LayoutLocalServiceUtil.addLayout(userId, liveGroup.getGroupId(), true, 0L,
							"Members", "Members", "", LayoutConstants.TYPE_PORTLET, false, null, serviceContext);
				} catch (PortalException e1) {
					e1.printStackTrace();
				}

				String typeSettings = "layout-template-id=2_columns_iii";
				activityLayout.setTypeSettings(typeSettings);
				LayoutLocalServiceUtil.updateLayout(activityLayout);

				LayoutTypePortlet layoutTypePortlet = null;

				String filesPortletId = "";
				String webContentPortletId = "";
				String inviteMembersPortletId = "";

				try {
					layoutTypePortlet = (LayoutTypePortlet) activityLayout.getLayoutType();
					filesPortletId = layoutTypePortlet.addPortletId(userId, "com_liferay_directory_web_portlet_SiteMembersDirectoryPortlet", "column-1", -1,
							true);
					webContentPortletId = layoutTypePortlet.addPortletId(userId, "com_liferay_journal_content_web_portlet_JournalContentPortlet",
							"column-2", -1, true);
					inviteMembersPortletId = layoutTypePortlet.addPortletId(userId, "com_liferay_invitation_invite_members_web_portlet_InviteMembersPortlet",
							"column-2", -1, true);
				} catch (Exception e) {
					logger.error("adding portlets error", e);
				}

				try {
					setPortletPreference(activityLayout, filesPortletId,
							new HashMap<String, String>() { { put("portletSetupPortletDecoratorId", "borderless"); } });

					setPortletPreference(activityLayout, inviteMembersPortletId,
							new HashMap<String, String>() { { put("portletSetupPortletDecoratorId", "borderless"); } });

					JournalArticle article = addMembersArticle(userId, liveGroup.getGroupId(), liveGroup.getCompanyId());

					if (article != null) {
						setPortletPreference(activityLayout, webContentPortletId,
								new HashMap<String, String>() {
									{ put("portletSetupPortletDecoratorId", "barebone"); }
									{ put("articleId", String.valueOf(article.getArticleId())); }
									{ put("groupId", String.valueOf(Utils.getGlobalGroupId(liveGroup.getCompanyId()))); }
								});
					}
				} catch (Exception e) {
					logger.error("setting preferences err", e);
				}

				try {
					ResourcePermissionLocalServiceUtil.setResourcePermissions(liveGroup.getCompanyId(), filesPortletId,
							ResourceConstants.SCOPE_INDIVIDUAL, activityLayout.getPlid() + "_LAYOUT_" + filesPortletId,
							Utils.getRoleById(liveGroup.getCompanyId(), RoleConstants.SITE_MEMBER).getRoleId(),
							new String[] { ActionKeys.VIEW });
				} catch (PortalException e) {
					logger.error("setting persmission err", e);
				}
				
				// update layout (important)
				try {
					activityLayout = LayoutLocalServiceUtil.updateLayout(activityLayout.getGroupId(),
							activityLayout.isPrivateLayout(), activityLayout.getLayoutId(),
							activityLayout.getTypeSettings());
					applyTheme(activityLayout);
				} catch (PortalException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}


			}

		}		
		
		/***********************************************
		 *********************************************** 
		 *
		 * REPORTS
		 *
		 ***********************************************
		 ***********************************************/
		{
			if (true/*siteTemplate == null*/) {
				boolean isPrivate = true;
				
				Layout activityLayout = null;

				try {
					activityLayout = LayoutLocalServiceUtil.addLayout(userId, liveGroup.getGroupId(), isPrivate, 0L,
							"Reports", "Reports", "", LayoutConstants.TYPE_PORTLET, false, null, serviceContext);
				} catch (PortalException e1) {
					e1.printStackTrace();
				}
				

				String typeSettings;

				typeSettings = "layout-template-id=1_column";
				activityLayout.setTypeSettings(typeSettings);
				LayoutLocalServiceUtil.updateLayout(activityLayout);
				applyTheme(activityLayout);

				Layout impRepLayout = null;

				try {
					impRepLayout = LayoutLocalServiceUtil.addLayout(userId, liveGroup.getGroupId(), isPrivate, activityLayout.getLayoutId(),
							"Impact", "Impact", "", LayoutConstants.TYPE_PORTLET, false, null, serviceContext);
				} catch (PortalException e1) {
					e1.printStackTrace();
				}
				
				String  impRepTypeSettings = "layout-template-id=1_column";
				impRepLayout.setTypeSettings(impRepTypeSettings );
				LayoutLocalServiceUtil.updateLayout(impRepLayout);
				
				LayoutTypePortlet impLayoutTypePortlet = null;
				try {
					impLayoutTypePortlet = (LayoutTypePortlet) impRepLayout.getLayoutType();
					impRepLayout = LayoutLocalServiceUtil.updateLayout(impRepLayout.getGroupId(),
							impRepLayout.getPrivateLayout(), impRepLayout.getLayoutId(),
							impRepLayout.getTypeSettings());
					impLayoutTypePortlet = (LayoutTypePortlet)impRepLayout.getLayoutType();
					LayoutLocalServiceUtil.updatePriority(impRepLayout, 0);
					impLayoutTypePortlet = (LayoutTypePortlet) impRepLayout.getLayoutType();
					applyTheme(impRepLayout);
				} catch (Exception e) {
					e.printStackTrace();
				}

				String reportImpPortletId = impLayoutTypePortlet.addPortletId(userId, "ImpactReports", "column-1", -1,
						true);

				// update layout (important)
				try {
					impRepLayout = LayoutLocalServiceUtil.updateLayout(impRepLayout.getGroupId(),
							impRepLayout.isPrivateLayout(), impRepLayout.getLayoutId(),
							impRepLayout.getTypeSettings());
				} catch (PortalException e1) {
					e1.printStackTrace();
				}
				
				
				Layout matRepLayout = null;

				try {
					matRepLayout = LayoutLocalServiceUtil.addLayout(userId, liveGroup.getGroupId(), isPrivate, activityLayout.getLayoutId(),
							"Maturity", "Maturity", "", LayoutConstants.TYPE_PORTLET, false, null, serviceContext);
				} catch (PortalException e1) {
					e1.printStackTrace();
				}
				

				String  matRepTypeSettings = "layout-template-id=1_column";
				matRepLayout.setTypeSettings(matRepTypeSettings );
				LayoutLocalServiceUtil.updateLayout(matRepLayout);

				
				LayoutTypePortlet layoutTypePortlet = null;
				try {
					layoutTypePortlet = (LayoutTypePortlet) matRepLayout.getLayoutType();
					matRepLayout = LayoutLocalServiceUtil.updateLayout(matRepLayout.getGroupId(),
							matRepLayout.getPrivateLayout(), matRepLayout.getLayoutId(),
							matRepLayout.getTypeSettings());
					layoutTypePortlet = (LayoutTypePortlet)matRepLayout.getLayoutType();
					//LayoutLocalServiceUtil.updatePriority(matRepLayout, 0);
					applyTheme(matRepLayout);
				} catch (Exception e) {
					e.printStackTrace();
				}

				layoutTypePortlet.addPortletId(userId, "MaturityReports", "column-1", -1, true);
				layoutTypePortlet.addPortletId(userId, "MilestonesReportDisplay", "column-1", -1, true);
				

				// update layout (important)
				try {
					matRepLayout = LayoutLocalServiceUtil.updateLayout(matRepLayout.getGroupId(),
							matRepLayout.isPrivateLayout(), matRepLayout.getLayoutId(),
							matRepLayout.getTypeSettings());
				} catch (PortalException e1) {
					e1.printStackTrace();
				}
				
				Layout planRepLayout = null;

				try {
					planRepLayout = LayoutLocalServiceUtil.addLayout(userId, liveGroup.getGroupId(), isPrivate, activityLayout.getLayoutId(),
							"Planning", "Planning", "", LayoutConstants.TYPE_PORTLET, false, null, serviceContext);
				} catch (PortalException e1) {
					e1.printStackTrace();
				}
				

				String  planRepTypeSettings = "layout-template-id=1_column";
				planRepLayout.setTypeSettings(planRepTypeSettings );
				LayoutLocalServiceUtil.updateLayout(planRepLayout);

				
				LayoutTypePortlet layoutTypePortlet2 = null;
				try {
					layoutTypePortlet2 = (LayoutTypePortlet) planRepLayout.getLayoutType();
					planRepLayout = LayoutLocalServiceUtil.updateLayout(planRepLayout.getGroupId(),
							planRepLayout.getPrivateLayout(), planRepLayout.getLayoutId(),
							planRepLayout.getTypeSettings());
					layoutTypePortlet2 = (LayoutTypePortlet)planRepLayout.getLayoutType();
					//LayoutLocalServiceUtil.updatePriority(matRepLayout, 0);
					applyTheme(planRepLayout);
				} catch (Exception e) {
					e.printStackTrace();
				}

				layoutTypePortlet2.addPortletId(userId, "PlanningReports", "column-1", -1, true);
				

				// update layout (important)
				try {
					planRepLayout = LayoutLocalServiceUtil.updateLayout(planRepLayout.getGroupId(),
							planRepLayout.isPrivateLayout(), planRepLayout.getLayoutId(),
							planRepLayout.getTypeSettings());
					applyTheme(activityLayout);
				} catch (PortalException e1) {
					e1.printStackTrace();
				}
				/*
				 * try { updateProjectLayoutWithFooter(userId,
				 * liveGroup.getGroupId(), liveGroup.getCompanyId(),
				 * activityLayout); } catch (PortalException e) {
				 * e.printStackTrace(); } catch (ReadOnlyException e) {
				 * e.printStackTrace(); }
				 */

			}
		}

	}
	
	private static DDMTemplate fetchDDMTemplateFromGlobal(long companyId, String templateName, long globalGroupId) {
		DDMTemplate ddmTemplate = null;
		List<DDMTemplate> ddmTemplates = Utils.getAllGlobalDDMTemplates(globalGroupId);
		for (DDMTemplate template : ddmTemplates) {
			if (template.getNameCurrentValue().equals(templateName))
				ddmTemplate = template;
		}
		return ddmTemplate;
	}

	private static DDMStructure fetchDDMStructureFromGlobal(long companyId, String structureName, long globalGroupId) {
		DDMStructure ddmStructure = null;
		List<DDMStructure> ddmStructures = Utils.getAllGlobalDDMStructures(globalGroupId);
		for (DDMStructure structure : ddmStructures) {
			if (structure.getNameCurrentValue().equals(structureName))
				ddmStructure = structure;
		}
		return ddmStructure;
	}

	
	private static void updateProjectLayoutWithProjectProfileContent(long globalGroupId, long userId, long groupId, long compId, Layout layout)
			throws PortalException, ReadOnlyException {

		long companyId = compId;
		long ownerId = PortletKeys.PREFS_OWNER_ID_DEFAULT;
		int ownerType = PortletKeys.PREFS_OWNER_TYPE_LAYOUT;
		
		LayoutTypePortlet layoutTypePortlet1 = (LayoutTypePortlet) layout.getLayoutType();
		//LayoutTypePortlet layoutTypePortlet2 = (LayoutTypePortlet) layout.getLayoutType();
		//LayoutTypePortlet layoutTypePortlet3 = (LayoutTypePortlet) layout.getLayoutType();
		//LayoutTypePortlet layoutTypePortlet4 = (LayoutTypePortlet) layout.getLayoutType();
		
		String templateNameCarousel = com.collaboratelab.project.constants.Constants.GLOBAL_PROJECT_PROFILE_CAROUSEL;
		String structureNameCarousel = com.collaboratelab.project.constants.Constants.GLOBAL_PROJECT_PROFILE_CAROUSEL;
		 
		String templateNameItemsRow = com.collaboratelab.project.constants.Constants.GLOBAL_PROJECT_PROFILE_ITEMS_ROW;
		String structureNameItemsRow = com.collaboratelab.project.constants.Constants.GLOBAL_PROJECT_PROFILE_ITEMS_ROW;
		
		String templateNameProjectTeam = com.collaboratelab.project.constants.Constants.GLOBAL_PROJECT_PROFILE_PROJECT_TEAM;
		String structureNameProjectTeam = com.collaboratelab.project.constants.Constants.GLOBAL_PROJECT_PROFILE_PROJECT_TEAM;
		 
		String templateNameTextColumnImage = com.collaboratelab.project.constants.Constants.GLOBAL_PROJECT_PROFILE_TEXT_COLUMN_IMAGE;
		String structureNameTextColumnImage = com.collaboratelab.project.constants.Constants.GLOBAL_PROJECT_PROFILE_TEXT_COLUMN_IMAGE;
		 
		/* Get template */
		DDMTemplate ddmTemplateCarousel = fetchDDMTemplateFromGlobal(companyId, templateNameCarousel, globalGroupId);
		DDMStructure ddmStructureCarousel = fetchDDMStructureFromGlobal(companyId, structureNameCarousel, globalGroupId);
		
		DDMTemplate ddmTemplateItemsRow = fetchDDMTemplateFromGlobal(companyId, templateNameItemsRow, globalGroupId);
		DDMStructure ddmStructureItemsRow = fetchDDMStructureFromGlobal(companyId, structureNameItemsRow, globalGroupId);
		
		DDMTemplate ddmTemplateProjectTeam = fetchDDMTemplateFromGlobal(companyId, templateNameProjectTeam, globalGroupId);
		DDMStructure ddmStructureProjectTeam = fetchDDMStructureFromGlobal(companyId, structureNameProjectTeam, globalGroupId);
		
		DDMTemplate ddmTemplateTextColumnImage = fetchDDMTemplateFromGlobal(companyId, templateNameTextColumnImage, globalGroupId);
		DDMStructure ddmStructureTextColumnImage = fetchDDMStructureFromGlobal(companyId, structureNameTextColumnImage, globalGroupId);
		
		DDMTemplate ddmTemplateFooter = fetchDDMTemplateFromGlobal(companyId, Constants.GLOBAL_PROJECT_PROFILE_FOOTER , globalGroupId);
		DDMStructure ddmStructureFooter = fetchDDMStructureFromGlobal(companyId, Constants.GLOBAL_PROJECT_PROFILE_FOOTER, globalGroupId);		
		
		/* Get Journal Article Content for part of footer */
		
		String folderName = com.collaboratelab.project.constants.Constants.GLOBAL_FOLDER_SYSTEM;
		String articleTitleCarousel = com.collaboratelab.project.constants.Constants.GLOBAL_PROJECT_PROFILE_CAROUSEL;
		String articleTitleItemsRow = com.collaboratelab.project.constants.Constants.GLOBAL_PROJECT_PROFILE_ITEMS_ROW;
		String articleTitleProjectTeam = com.collaboratelab.project.constants.Constants.GLOBAL_PROJECT_PROFILE_PROJECT_TEAM;
		String articleTitleTextColumnImage = com.collaboratelab.project.constants.Constants.GLOBAL_PROJECT_PROFILE_TEXT_COLUMN_IMAGE;
		
		String articleTitleCarouselUrlTitle = com.collaboratelab.project.constants.Constants.GLOBAL_PROJECT_PROFILE_CAROUSEL_URL_TITLE;
		String articleTitleItemsRowUrlTitle = com.collaboratelab.project.constants.Constants.GLOBAL_PROJECT_PROFILE_ITEMS_ROW_URL_TITLE;
		String articleTitleProjectTeamUrlTitle = com.collaboratelab.project.constants.Constants.GLOBAL_PROJECT_PROFILE_PROJECT_TEAM_URL_TITLE;
		String articleTitleTextColumnImageUrlTitle = com.collaboratelab.project.constants.Constants.GLOBAL_PROJECT_PROFILE_TEXT_COLUMN_IMAGE_URL_TITLE;
		
		
		ServiceContext serviceContext = new ServiceContext();
		serviceContext.setScopeGroupId(globalGroupId);
		//System.out.println(ddmStructureCarousel.getDDMForm().getDDMFormFields().size();
		
		//articleTitleCarousel..DDMFormValues ddmFormValues = DDMUtil.(ddmStructureCarousel.getStructureId(), "", serviceContext);
		//Fields fields = DDMUtil.getFields(ddmStructure.getStructureId(), ddmFormValues); 
		//String content = _journalConverter.getContent(ddmStructureCarousel, fields) 
		//List<DDMFormField>  ddmFields =  ddmStructureProjectTeam.getDDMForm().
		//Fields ddmFields = DDMUtil.getFields(ddmStructureCarousel.getStructureId(), ddmFormValues); 
		//String content = _journalConverter.getContent(ddmStructureCarousel, ddmFields);
		 //Fields fields = DDMUtil.ddmStructureCarousel.getStructureId(), serviceContext);
		 
		String content = "";
		//try {
			//content = _journalConverter.getContent(ddmStructureCarousel, fields);
		//} catch (Exception e1) {
			// TODO Auto-generated catch block
		//	e1.printStackTrace();
		//} 
		
		
		//String articleTitle = com.collaboratelab.project.constants.Constants.GLOBAL_PROJECT_PROFILE_CONTENT;// getFooterJournalArticleName();

		JournalFolder folder = Utils.fetchFolder(globalGroupId, folderName, 0);
		JournalArticle journalArticleCarousel = null;
		JournalArticle journalArticleItemsRow = null;
		JournalArticle journalArticleProjectTeam = null;
		JournalArticle journalArticleTextColumnImage = null;
		JournalArticle journalArticleFooter = null;
		
		// create article in global (if needed)
		if(folder != null) {
			
			try {
				journalArticleCarousel = JournalArticleLocalServiceUtil.fetchArticleByUrlTitle(globalGroupId, articleTitleCarouselUrlTitle);
				journalArticleItemsRow = JournalArticleLocalServiceUtil.fetchArticleByUrlTitle(globalGroupId, articleTitleItemsRowUrlTitle);
				journalArticleProjectTeam = JournalArticleLocalServiceUtil.fetchArticleByUrlTitle(globalGroupId, articleTitleProjectTeamUrlTitle);
				journalArticleTextColumnImage = JournalArticleLocalServiceUtil.fetchArticleByUrlTitle(globalGroupId, articleTitleTextColumnImageUrlTitle);
				journalArticleFooter = JournalArticleLocalServiceUtil.fetchArticleByUrlTitle(globalGroupId, Constants.GLOBAL_PROJECT_PROFILE_FOOTER_URL_TITLE);
				
				if(journalArticleCarousel == null) {
					journalArticleCarousel = addJournalArticle(userId, globalGroupId, folder.getFolderId(), articleTitleCarousel,
						ddmStructureCarousel.getStructureKey(), ddmTemplateCarousel.getTemplateKey(),
						Constants.CAROUSEL_CONTENT, articleTitleCarouselUrlTitle);
				}
				if(journalArticleItemsRow == null) {
					journalArticleItemsRow = addJournalArticle(userId,globalGroupId, folder.getFolderId(), articleTitleItemsRow, ddmStructureItemsRow.getStructureKey(),
						ddmTemplateItemsRow.getTemplateKey(), Constants.ITEMS_ROW_CONTENT, articleTitleItemsRowUrlTitle);
				}
				
				if(journalArticleProjectTeam == null) {
					journalArticleProjectTeam = addJournalArticle(userId,globalGroupId, folder.getFolderId(), articleTitleProjectTeam, ddmStructureProjectTeam.getStructureKey(),
						ddmTemplateProjectTeam.getTemplateKey(), Constants.PROJECT_TEAM_CONTENT, articleTitleProjectTeamUrlTitle);
				}
				
				if(journalArticleTextColumnImage == null) {
					journalArticleTextColumnImage = addJournalArticle(userId,globalGroupId, folder.getFolderId(), articleTitleTextColumnImage, ddmStructureTextColumnImage.getStructureKey(),
						ddmTemplateTextColumnImage.getTemplateKey(), Constants.TEXT_COLUMN_IMAGE_CONTENT, articleTitleTextColumnImageUrlTitle);
				}
				
				if(journalArticleFooter == null) {
					journalArticleFooter = addJournalArticle(userId,globalGroupId, folder.getFolderId(), Constants.GLOBAL_PROJECT_PROFILE_FOOTER, ddmStructureFooter.getStructureKey(),
							ddmTemplateFooter.getTemplateKey(), Constants.FOOTER_CONTENT, Constants.GLOBAL_PROJECT_PROFILE_FOOTER_URL_TITLE);
				}				
				
			} catch (Exception e) {
				logger.error(e);
			}
		}
		 
		JournalFolder folderSyst = Utils.fetchFolder(groupId, folderName, 0);
		
		if(folderSyst == null) {
			folderSyst = Utils.createFolder(folderName, groupId, 0,
					userId);
		}
		
		// create article in subsite 		
		if(folderSyst != null) {
			if(journalArticleCarousel != null) {
				try {
					journalArticleCarousel = addJournalArticle(userId, groupId, folderSyst.getFolderId(), journalArticleCarousel.getTitleCurrentValue(),
						ddmStructureCarousel.getStructureKey(), journalArticleCarousel.getDDMTemplateKey(),
						journalArticleCarousel.getContent(), articleTitleCarouselUrlTitle);
				} catch (Exception e) {
					logger.error(e);
				}
			}
			if(journalArticleItemsRow != null) {
				try {
					journalArticleItemsRow = addJournalArticle(userId,groupId, folderSyst.getFolderId(), journalArticleItemsRow.getTitleCurrentValue(), journalArticleItemsRow.getDDMStructureKey(),
							journalArticleItemsRow.getDDMTemplateKey(), journalArticleItemsRow.getContent(), articleTitleItemsRowUrlTitle);
				} catch (Exception e) {
					logger.error(e);
				}
			}
			if(journalArticleProjectTeam != null) {
				try {
					journalArticleProjectTeam = addJournalArticle(userId,groupId, folderSyst.getFolderId(), journalArticleProjectTeam.getTitleCurrentValue(), journalArticleProjectTeam.getDDMStructureKey(),
							journalArticleProjectTeam.getDDMTemplateKey(), journalArticleProjectTeam.getContent(), articleTitleProjectTeamUrlTitle);
				} catch (Exception e) {
					logger.error(e);
				}
			}
			if(journalArticleTextColumnImage != null) {
				try {
					journalArticleTextColumnImage = addJournalArticle(userId,groupId, folderSyst.getFolderId(), journalArticleTextColumnImage.getTitleCurrentValue(), journalArticleTextColumnImage.getDDMStructureKey(),
							journalArticleTextColumnImage.getDDMTemplateKey(), journalArticleTextColumnImage.getContent(), articleTitleTextColumnImageUrlTitle);
				} catch (Exception e) {
					logger.error(e);
				}
			}
			if(journalArticleFooter != null) {
				try {
					journalArticleFooter = addJournalArticle(userId,groupId, folderSyst.getFolderId(), journalArticleFooter.getTitleCurrentValue(), journalArticleFooter.getDDMStructureKey(),
							journalArticleFooter.getDDMTemplateKey(), journalArticleFooter.getContent(), Constants.GLOBAL_PROJECT_PROFILE_FOOTER_URL_TITLE);
				} catch (Exception e) {
					logger.error(e);
				}
			}			
		}
		
		// add empty portlets to page
		String journalPortletCarouselId = layoutTypePortlet1.addPortletId(userId,
				"com_liferay_journal_content_web_portlet_JournalContentPortlet", "column-1", -1, true);
		
		String journalPortletProjectTeamId = layoutTypePortlet1.addPortletId(userId,
				"com_liferay_journal_content_web_portlet_JournalContentPortlet", "column-1", -1, true);
		
		String journalPortletItemsRowId = layoutTypePortlet1.addPortletId(userId,
				"com_liferay_journal_content_web_portlet_JournalContentPortlet", "column-1", -1, true);
		
		String journalPortletTextColumnImageId = layoutTypePortlet1.addPortletId(userId,
				"com_liferay_journal_content_web_portlet_JournalContentPortlet", "column-1", -1, true);
		
		String journalPortletFooterId = layoutTypePortlet1.addPortletId(userId,
				"com_liferay_journal_content_web_portlet_JournalContentPortlet", "column-1", -1, true);		
		
		// update layout (important)
		layout = LayoutLocalServiceUtil.updateLayout(layout.getGroupId(), layout.isPrivateLayout(),
				layout.getLayoutId(), layout.getTypeSettings());
		
		// set westeros theme for Project Profile page 
		LayoutLocalServiceUtil.updateLookAndFeel(layout.getGroupId(), layout.isPrivateLayout(), layout.getLayoutId(),
				"westerosbank_WAR_westerosbanktheme", "01", StringPool.BLANK);
		
		/*layoutTypePortlet1 = (LayoutTypePortlet) layout.getLayoutType();
		layoutTypePortlet2 = (LayoutTypePortlet) layout.getLayoutType();
		layoutTypePortlet3 = (LayoutTypePortlet) layout.getLayoutType();
		layoutTypePortlet4 = (LayoutTypePortlet) layout.getLayoutType();*/
		// Retrieve the portlet preferences for the journal portlet instance
		// just created
		PortletPreferences prefsCarousel = PortletPreferencesLocalServiceUtil.getPreferences(companyId, ownerId, ownerType,
				layout.getPlid(), journalPortletCarouselId);

		PortletPreferences prefsItemsRow = PortletPreferencesLocalServiceUtil.getPreferences(companyId, ownerId, ownerType,
				layout.getPlid(), journalPortletItemsRowId);
		
		PortletPreferences prefsProjectTeam = PortletPreferencesLocalServiceUtil.getPreferences(companyId, ownerId, ownerType,
				layout.getPlid(), journalPortletProjectTeamId);
		
		PortletPreferences prefsTextColumnImage = PortletPreferencesLocalServiceUtil.getPreferences(companyId, ownerId, ownerType,
				layout.getPlid(), journalPortletTextColumnImageId);
		
		PortletPreferences prefsFooter = PortletPreferencesLocalServiceUtil.getPreferences(companyId, ownerId, ownerType,
				layout.getPlid(), journalPortletFooterId);		
		
		// set desired article id for content display portlet
		
		boolean isBarebone = true;
		
		if(journalArticleCarousel != null) {
			prefsCarousel.setValue("articleId", journalArticleCarousel.getArticleId());
			prefsCarousel.setValue("groupId", String.valueOf(journalArticleCarousel.getGroupId()));
			if (isBarebone) prefsCarousel.setValue("portletSetupPortletDecoratorId", "barebone");
		}
		
		if(journalArticleItemsRow != null) {
			prefsProjectTeam.setValue("articleId", journalArticleItemsRow.getArticleId());
			prefsProjectTeam.setValue("groupId", String.valueOf(journalArticleItemsRow.getGroupId()));
			if (isBarebone) prefsProjectTeam.setValue("portletSetupPortletDecoratorId", "barebone");
		}
		
		if(journalArticleProjectTeam != null) {
			prefsItemsRow.setValue("articleId", journalArticleProjectTeam.getArticleId());
			prefsItemsRow.setValue("groupId", String.valueOf(journalArticleProjectTeam.getGroupId()));
			if (isBarebone) prefsItemsRow.setValue("portletSetupPortletDecoratorId", "barebone");
		}
		
		if(journalArticleTextColumnImage != null) {
			prefsTextColumnImage.setValue("articleId", journalArticleTextColumnImage.getArticleId());
			prefsTextColumnImage.setValue("groupId", String.valueOf(journalArticleTextColumnImage.getGroupId()));
			if (isBarebone) prefsTextColumnImage.setValue("portletSetupPortletDecoratorId", "barebone");
		}
		
		if(journalArticleFooter != null) {
			prefsFooter.setValue("articleId", journalArticleFooter.getArticleId());
			prefsFooter.setValue("groupId", String.valueOf(journalArticleFooter.getGroupId()));
			if (isBarebone) prefsFooter.setValue("portletSetupPortletDecoratorId", "barebone");
		}		
		
		//prefs.setValue("ddmTemplateKey", ddmTemplateFooter.getTemplateKey());
		//prefs.setValue("templateId", String.valueOf(ddmTemplateFooter.getTemplateId()));

		// update the portlet preferences
		PortletPreferencesLocalServiceUtil.updatePreferences(ownerId, ownerType, layout.getPlid(), journalPortletCarouselId,
				prefsCarousel);
		
		PortletPreferencesLocalServiceUtil.updatePreferences(ownerId, ownerType, layout.getPlid(), journalPortletItemsRowId,
				prefsProjectTeam);
		
		PortletPreferencesLocalServiceUtil.updatePreferences(ownerId, ownerType, layout.getPlid(), journalPortletProjectTeamId,
				prefsItemsRow);
		
		PortletPreferencesLocalServiceUtil.updatePreferences(ownerId, ownerType, layout.getPlid(), journalPortletTextColumnImageId,
				prefsTextColumnImage);
		
		PortletPreferencesLocalServiceUtil.updatePreferences(ownerId, ownerType, layout.getPlid(), journalPortletFooterId,
				prefsFooter);		
		
		if(journalArticleCarousel != null) {
			JournalContentSearchLocalServiceUtil.updateContentSearch(layout.getGroupId(), layout.isPrivateLayout(),
				layout.getLayoutId(), journalPortletCarouselId, journalArticleCarousel.getArticleId());
		}
		
		if(journalArticleItemsRow != null) {
			JournalContentSearchLocalServiceUtil.updateContentSearch(layout.getGroupId(), layout.isPrivateLayout(),
				layout.getLayoutId(), journalPortletItemsRowId, journalArticleItemsRow.getArticleId());
		}
		
		if(journalArticleProjectTeam != null) {
			JournalContentSearchLocalServiceUtil.updateContentSearch(layout.getGroupId(), layout.isPrivateLayout(),
				layout.getLayoutId(), journalPortletProjectTeamId, journalArticleProjectTeam.getArticleId());
		}
		
		if(journalArticleTextColumnImage != null) {
			JournalContentSearchLocalServiceUtil.updateContentSearch(layout.getGroupId(), layout.isPrivateLayout(),
				layout.getLayoutId(), journalPortletTextColumnImageId, journalArticleTextColumnImage.getArticleId());
		}
		
		if(journalArticleFooter != null) {
			JournalContentSearchLocalServiceUtil.updateContentSearch(layout.getGroupId(), layout.isPrivateLayout(),
				layout.getLayoutId(), journalPortletFooterId, journalArticleFooter.getArticleId());
		}		
	}
	
	
	private static void updateProjectLayoutWithHeader(long userId, long groupId, long compId, Layout layout)
			throws PortalException, ReadOnlyException {

		long companyId = compId;
		long ownerId = PortletKeys.PREFS_OWNER_ID_DEFAULT;
		int ownerType = PortletKeys.PREFS_OWNER_TYPE_LAYOUT;
		LayoutTypePortlet layoutTypePortlet = (LayoutTypePortlet) layout.getLayoutType();
		
		/* Get Journal Article Content for part of footer */
		
		String folderName = com.collaboratelab.project.constants.Constants.GLOBAL_FOLDER_SYSTEM;
		
		String articleTitle = com.collaboratelab.project.constants.Constants.GLOBAL_HOME_HEADER;// getFooterJournalArticleName();
		long globalGroupId =  Utils.getGlobalGroupId(companyId);
		
		JournalFolder folder = Utils.fetchFolder(globalGroupId, folderName, 0);
		JournalArticle journalArticle = null;
		
		if(folder != null)
			journalArticle = Utils.fetchJournalArticleByParentFolderId(globalGroupId, articleTitle, folder.getFolderId());
		
		String journalPortletId = "com_liferay_journal_content_web_portlet_JournalContentPortlet";
		if(journalArticle != null)
		journalPortletId = layoutTypePortlet.addPortletId(userId,
				"com_liferay_journal_content_web_portlet_JournalContentPortlet", "column-1", -1, true);
		
		// update layout (important)
		layout = LayoutLocalServiceUtil.updateLayout(layout.getGroupId(), layout.isPrivateLayout(),
				layout.getLayoutId(), layout.getTypeSettings());
		layoutTypePortlet = (LayoutTypePortlet) layout.getLayoutType();
		
		// Retrieve the portlet preferences for the journal portlet instance
		// just created
		PortletPreferences prefs = PortletPreferencesLocalServiceUtil.getPreferences(companyId, ownerId, ownerType,
				layout.getPlid(), journalPortletId);
		if(journalArticle != null) {
			// set desired article id for content display portlet
			prefs.setValue("articleId", journalArticle.getArticleId());
			prefs.setValue("groupId", String.valueOf(journalArticle.getGroupId()));
			//prefs.setValue("ddmTemplateKey", ddmTemplateFooter.getTemplateKey());
			//prefs.setValue("templateId", String.valueOf(ddmTemplateFooter.getTemplateId()));
		}
		// update the portlet preferences
		PortletPreferencesLocalServiceUtil.updatePreferences(ownerId, ownerType, layout.getPlid(), journalPortletId,
				prefs);
		if(journalArticle != null) {
			JournalContentSearchLocalServiceUtil.updateContentSearch(layout.getGroupId(), layout.isPrivateLayout(),
				layout.getLayoutId(), journalPortletId, journalArticle.getArticleId());
		}
	}

	/*public static List<DDMTemplate> getAllGlobalDDMTemplates(long companyId) {

	
		int i =  DDMTemplateLocalServiceUtil.getDDMTemplatesCount();
		List<DDMTemplate> templates = DDMTemplateLocalServiceUtil.getDDMTemplates(0, i);

		return templates;
	}*/
	
	/*public static List<DDMStructure> getAllGlobalDDMStructure(long companyId) {
		
		int i =  DDMStructureLocalServiceUtil.getDDMStructuresCount();
		List<DDMStructure> structures = DDMStructureLocalServiceUtil.getDDMStructures(0, i);

		return structures;
	}*/
	

	private boolean isProjectFolderExist(long groupId, long projectId) {
		if (getProjectFolder(groupId, projectId) != null)
			return true;
		return false;
	}

	private DLFolder getProjectFolder(long groupId, long projectId) {

		DLFolder projectFolder = DLFolderLocalServiceUtil.fetchFolder(groupId,
				DLFolderConstants.DEFAULT_PARENT_FOLDER_ID, "Project" + projectId);

		return projectFolder;
	}
	
	
	

	private void createProjectFolder(long projectId, long groupId, ThemeDisplay themeDisplay) {
		try {
			ServiceContext serviceContext = new ServiceContext();
			serviceContext.setScopeGroupId(groupId);
			// To create a project folder in an Organization
			DLFolderLocalServiceUtil.addFolder(themeDisplay.getUserId(), groupId, groupId, false,
					DLFolderConstants.DEFAULT_PARENT_FOLDER_ID, "Project" + projectId, "", false, serviceContext);
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (PortalException e) {
			e.printStackTrace();
		}
	}

	
	private boolean isTypeProjectFolderExist(long groupId, int projectType) {
		if (getTypeProjectFolder(groupId, projectType) != null)
			return true;
		return false;
	}
	
	private JournalFolder getTypeProjectFolder(long groupId, int projectType) {
		
		String projectTypeName = Enums.TypeProject.getNameById(projectType);
		
		JournalFolder projectTypeFolder = JournalFolderLocalServiceUtil.fetchFolder(groupId,
				JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID, projectTypeName);

		return projectTypeFolder;
	}
	

	private void createTypeProjectFolder(int projectType, long groupId, ThemeDisplay themeDisplay) {
		JournalFolder folder = null;
		try {
			ServiceContext serviceContext = new ServiceContext();
			serviceContext.setScopeGroupId(groupId);
			// To create a project folder in an Organization
			String projectTypeName = Enums.TypeProject.getNameById(projectType);
			folder = JournalFolderLocalServiceUtil.addFolder(themeDisplay.getUserId(), groupId, JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID, projectTypeName, "", serviceContext);
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (PortalException e) {
			e.printStackTrace();
		}
	
		createProjectPhaseAndDomainFolder(groupId,folder.getFolderId(),themeDisplay);
	}
	
	private void createProjectPhaseAndDomainFolder(long groupId, long parentId, ThemeDisplay themeDisplay) {

		for (Enums.DOMAIN_ENUM domain : Enums.DOMAIN_ENUM.values()) {

			JournalFolder folderDomain = null;
			long userId = themeDisplay.getUserId();

			folderDomain = Utils.createWebContentFolder(groupId, userId, parentId, domain.getIdentifier());

			for (Enums.PHASE_ENUM phase : Enums.PHASE_ENUM.values()) {
				Utils.createWebContentFolder(groupId, userId, folderDomain.getFolderId(), phase.getIdentifier());
			}
		}
	}
	
	/*private JournalFolder getFolder(long groupId, String name) {
	
		JournalFolder folder = JournalFolderLocalServiceUtil.fetchFolder(groupId,
				JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID, name);

		return folder;
	}*/
	

	
	private List<JournalArticle> getJournalArticlesFromFolder(JournalFolder folder) {
		List<JournalArticle> journalArticles = JournalArticleLocalServiceUtil.getArticles(folder.getGroupId(), folder.getFolderId());
		Set<String> uniqueJournalArticleIds = new HashSet<String>();
 		for (JournalArticle ja : journalArticles) {
 			// filter out duplicated JA of different versions
			uniqueJournalArticleIds.add(ja.getArticleId());
		}
		List<JournalArticle> latestVersionJournalArticles = new ArrayList<JournalArticle>();
		for (String articleId : uniqueJournalArticleIds) {
			JournalArticle latestJA = null;
			try {
				latestJA = JournalArticleLocalServiceUtil.getLatestArticle(folder.getGroupId(), articleId);
			} catch (PortalException e) {
				logger.warn("current delivery in trash bin, articleId = " + articleId);
			}
			if (latestJA != null) {
				latestVersionJournalArticles.add(latestJA);
			}
		}
		return latestVersionJournalArticles;
	}
	
	private String getDeliveryTypeFromJournalArticle(JournalArticle ja) {
		
		return ja.getContent();
	}
	
	
	/*private void createDeliveryFolder(long projectId, long deliveryId, long groupId, long parentFolderId,
			ThemeDisplay themeDisplay) {
		try {
			ServiceContext serviceContext = new ServiceContext();
			serviceContext.setScopeGroupId(groupId);
			// To create a delivery folder in an Project Folder
			DLFolder folder = DLFolderLocalServiceUtil.addFolder(themeDisplay.getUserId(), groupId, groupId, false,
					parentFolderId, "Delivery" + deliveryId, "", false, serviceContext);
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (PortalException e) {
			e.printStackTrace();
		}
	}*/
	
	private List<JournalFolder> getJournalFolders(JournalFolder folder) {
		List<JournalFolder> jf = JournalFolderLocalServiceUtil.getFolders(folder.getGroupId(), folder.getFolderId());
		return jf;
	}
	
	
	private List<JournalArticle> getJournalArticles(JournalFolder type,String phase, String domain) {
		
		List<JournalFolder> folderDomain = getJournalFolders(type);
		
		for(JournalFolder d :folderDomain) {
			String nameDomain = Enums.DOMAIN_ENUM.getDomainByIdentifier(d.getName());
			if(nameDomain.equals(domain)) {
				List<JournalFolder> folderPhase = getJournalFolders(d);
				for(JournalFolder p :folderPhase) {
					String namePhase = Enums.PHASE_ENUM.getPhaseByIdentifier(p.getName());
					if (namePhase.equals(phase)) {
						return getJournalArticlesFromFolder(p);
					}
				}
			}
			
		}
		return null;
	}

	protected void createStructureInDB(ThemeDisplay themeDisplay, long groupId, int typeProject) throws Exception {

		long companyId = CompanyThreadLocal.getCompanyId();

		User user = themeDisplay.getUser();

		long userId = user.getUserId();

		/* Create Project */

		long projectId = CounterLocalServiceUtil.increment();

		Project project = ProjectLocalServiceUtil.createProject(projectId);

		project.setCompanyId(companyId);
		project.setUserId(user.getUserId());
		//project.setName("Project 1");
		//project.setDescription("description");
		//project.setIdentifier("Project 1");
		project.setGroupId(groupId);
		project.setUserName(user.getScreenName());
		project.setProjectType(typeProject);
		ProjectLocalServiceUtil.addProject(project);

		long globalGroupId = Utils.getGlobalGroupId(companyId);
		/* create Project Folder in Document Library */
		//if (!isProjectFolderExist(groupId, project.getProjectId()))
			//createProjectFolder(project.getProjectId(),groupId, themeDisplay);

		if(!isTypeProjectFolderExist(globalGroupId,typeProject))
			createTypeProjectFolder(typeProject, globalGroupId, themeDisplay);

		//DLFolder parentFolder = getProjectFolder(groupId, project.getProjectId());

		/* Create ProjectStructure */

		long projectStructureId = CounterLocalServiceUtil.increment();

		ProjectStructure projectStructure = ProjectStructureLocalServiceUtil.createProjectStructure(projectStructureId);

		projectStructure.setCompanyId(companyId);
		projectStructure.setUserId(user.getUserId());
		//projectStructure.setName("Project Structure 1");
		//projectStructure.setDescription("description");
		//projectStructure.setIdentifier("Project Structure 1");
		projectStructure.setUserName(user.getScreenName());
		projectStructure.setProjectId(project.getProjectId());

		ProjectStructureLocalServiceUtil.addProjectStructure(projectStructure);

		for (String identifier : Enums.PHASES_ARRAY) {

			long phaseId = CounterLocalServiceUtil.increment();

			ProjectPhase phase = ProjectPhaseLocalServiceUtil.createProjectPhase(phaseId);
			phase.setProjectStructureId(projectStructureId);
			//phase.setName("name");
			//phase.setDescription("description");
			phase.setCompanyId(companyId);
			phase.setIdentifier(identifier);
			phase.setUserId(userId);
			phase.setUserName(user.getScreenName());
			phase.setProjectStructureId(projectStructure.getProjectStructureId());
			ProjectPhaseLocalServiceUtil.addProjectPhase(phase);

		}

		for (String identifier : Enums.DOMAIN_ARRAY) {

			long domainId = CounterLocalServiceUtil.increment();

			ProjectDomain domain = ProjectDomainLocalServiceUtil.createProjectDomain(domainId);

			domain.setName("name");
			domain.setDescription("description");
			domain.setCompanyId(companyId);
			domain.setIdentifier(identifier);
			domain.setUserId(userId);
			domain.setUserName(user.getScreenName());
			domain.setProjectStructureId(projectStructure.getProjectStructureId());

			ProjectDomainLocalServiceUtil.addProjectDomain(domain);

		}

		/* Create Step */

		List<ProjectPhase> phases = ProjectPhaseLocalServiceUtil
				.findByProjectStructureId(projectStructure.getProjectStructureId());

		List<ProjectDomain> domains = ProjectDomainLocalServiceUtil
				.findByProjectStructureId(projectStructure.getProjectStructureId());

		for (ProjectPhase phase : phases) {

			int phaseEnumId = Enums.getArrayNum(Enums.PHASES_ARRAY, phase.getIdentifier());

			for (ProjectDomain domain : domains) {

				int domainEnumId = Enums.getArrayNum(Enums.DOMAIN_ARRAY, domain.getIdentifier());

				// populate resources
				long stepCatId = Utils.getCategoryByDomainAndPhase(globalGroupId, domainEnumId, phaseEnumId);
				Utils.populateResourcesByCategory(stepCatId, userId, groupId, globalGroupId);

				long stepId = CounterLocalServiceUtil.increment();

				ProjectStep step = ProjectStepLocalServiceUtil.createProjectStep(stepId);

				step.setCompanyId(companyId);
				step.setUserId(user.getUserId());
				step.setUserName(user.getScreenName());
				step.setProjectId(projectStructure.getProjectId());
				step.setProjectPhaseId(phase.getProjectPhaseId());
				step.setProjectDomainId(domain.getProjectDomainId());
				step.setPercentage(0);

				long percentage = ThreadLocalRandom.current().nextLong(100);
				step.setPercentage(percentage);

				ProjectStepLocalServiceUtil.addProjectStep(step);
				//System.out.println("Step: " + step.getProjectStepId() + " domain: " + domain.getIdentifier() + " phase "+ phase.getIdentifier());

				JournalFolder folder = getTypeProjectFolder(globalGroupId, typeProject);
				List<JournalArticle> deliveryTypes = getJournalArticles(folder,phase.getIdentifier(), domain.getIdentifier());
				if (deliveryTypes != null) {
					for (JournalArticle dt : deliveryTypes) {

						long deliveryId = CounterLocalServiceUtil.increment();

						ProjectDelivery delivery = ProjectDeliveryLocalServiceUtil.createProjectDelivery(deliveryId);

						delivery.setUserName(user.getScreenName());
						delivery.setUserId(user.getUserId());
						delivery.setStepId(step.getProjectStepId());
						delivery.setType(Integer.parseInt(dt.getArticleId()));
						delivery.setDomain(domain.getIdentifier());
						delivery.setPhase(phase.getIdentifier());
						//delivery.setName(d.getDelivery());
						delivery.setCost(0);
						//delivery.setStartDate(new Date());
						//delivery.setEndDate(new Date());
						delivery.setComment("");
						delivery.setReviewerComment("");
						delivery.setReviewerIsCompleted(false);
						delivery.setReviewerRate(0);
						delivery.setStatus(Enums.Status.ZERO.getId());

						delivery.setFundingSource(Enums.Funding.OTHER.getId());
						delivery.setBudget(0);
						delivery.setPersonResponsible("");

						//delivery.setOutcomeCategory(OutcomeCategory.AS_PLANNED.getId());
						ProjectDeliveryLocalServiceUtil.addProjectDelivery(delivery);

						/* create Delivery Folder in Document Library */
						/*if (parentFolder != null)
							createDeliveryFolder(projectId, delivery.getDeliveryId(), groupId, parentFolder.getFolderId(),
									themeDisplay);*/

					}
				}
			}
		}
	}

	public static JournalArticle addJournalArticle(long userId, long groupId, long folderId, String title, String ddmStructureKey,
			String ddmTemplateKey, String content, String urlTitle) throws Exception {

		ServiceContext serviceContext = new ServiceContext();
		serviceContext.setAddGroupPermissions(true);
		serviceContext.setAddGuestPermissions(true);
		serviceContext.setScopeGroupId(groupId);

		serviceContext.setWorkflowAction(WorkflowConstants.ACTION_PUBLISH);
		Map<Locale, String> titleMap = new HashMap<Locale, String>();
		Map<Locale, String> descriptionMap = new HashMap<Locale, String>();

		titleMap.put(Locale.US, title);
		descriptionMap.put(Locale.US, title);
		String xmlContent = "<?xml version='1.0' encoding='UTF-8'?>"
				+ "<root default-locale=\"en_US\" available-locales=\"en_US\">"
				+ "<static-content language-id=\"en_US\">" + "<![CDATA[" + "" + "]]>" + "</static-content>" + "</root>";


		JournalArticle ja = JournalArticleLocalServiceUtil.addArticle(userId, groupId,folderId, titleMap, descriptionMap,
				content.isEmpty() ? xmlContent : content, ddmStructureKey, ddmTemplateKey, serviceContext);
		ja.setUrlTitle(urlTitle);
		JournalArticleLocalServiceUtil.updateJournalArticle(ja);
		return ja;
	}

	private void setPortletPreference(Layout layout, String portletId, Map<String, String> kv) throws Exception {
		long ownerId = PortletKeys.PREFS_OWNER_ID_DEFAULT;
		int ownerType = PortletKeys.PREFS_OWNER_TYPE_LAYOUT;

		try {
			javax.portlet.PortletPreferences preferences = PortletPreferencesFactoryUtil.
					getLayoutPortletSetup(layout, portletId);

			for(Map.Entry<String, String> entry : kv.entrySet()) {
				preferences.setValue(entry.getKey(), entry.getValue());
			}
			preferences.store();

			PortletPreferencesLocalServiceUtil.updatePreferences(ownerId, ownerType, layout.getPlid(), portletId, preferences);
		} catch (Exception e) {
			logger.error("updatePreferences ex:: ", e);
		}
	}

	private JournalArticle addMembersArticle(long userId, long groupId, long companyId) throws Exception {
		String title = "members_wc";
		String folderName = com.collaboratelab.project.constants.Constants.GLOBAL_FOLDER_SYSTEM;
		long globalGroupId =  Utils.getGlobalGroupId(companyId);
		JournalFolder folder = Utils.fetchFolder(globalGroupId, folderName, 0);

		if(folder == null)
			return null;

		JournalArticle article = null;

		article = Utils.fetchJournalArticleByParentFolderId(globalGroupId, title, folder.getFolderId());

		if(article != null)
			return article;

		String xmlContent = "<?xml version=\"1.0\"?>\n" +
				"\n" +
				"<root available-locales=\"en_US\" default-locale=\"en_US\">\n" +
				"\t<dynamic-element name=\"content\" type=\"text_area\" index-type=\"text\" instance-id=\"ciuh\">\n" +
				"\t\t<dynamic-content language-id=\"en_US\"><![CDATA[<p>Please contact the team site administrator to request additional members be given access.</p>]]></dynamic-content>\n" +
				"\t</dynamic-element>\n" +
				"</root>";


		ServiceContext serviceContext = new ServiceContext();
		serviceContext.setAddGroupPermissions(true);
		serviceContext.setAddGuestPermissions(true);
		serviceContext.setScopeGroupId(groupId);
		serviceContext.setWorkflowAction(WorkflowConstants.ACTION_PUBLISH);

		Map<Locale, String> titleMap = new HashMap<Locale, String>();
		Map<Locale, String> descriptionMap = new HashMap<Locale, String>();
		titleMap.put(Locale.US, title);
		descriptionMap.put(Locale.US, title);

		String ddmStructureKey = "BASIC-WEB-CONTENT";
		String ddmTemplateKey = "BASIC-WEB-CONTENT";


		try {
			article = JournalArticleLocalServiceUtil.addArticle(userId, globalGroupId, folder.getFolderId(),
					titleMap, descriptionMap, xmlContent, ddmStructureKey, ddmTemplateKey, serviceContext);
		} catch (Exception e) {
			logger.error("addMembersArticle ex:: ", e);
		}

		return article;
	}

	@Reference(unbind = "-")
	protected void setGroupService(GroupService groupService) {
		this.groupService = groupService;
	}
	
	@Reference(unbind = "-")
	protected void setJournalConverter(JournalConverter journalConverter) {
		this._journalConverter = journalConverter;
	}
	

	private GroupService groupService;
	private static JournalConverter _journalConverter;

}
