package com.collaboratelab.project.constants.enums;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.collaboratelab.project.constants.Utils;

public class Enums {

	private Enums() {
	};

	public static final String[] PHASES_ARRAY = { "CLINICAL NEED", "IDEA", "PROOF OF CONCEPT", "PROOF OF FEASIBILITY",
			"PROOF OF VALUE", "INITIAL CLINICAL TRIALS", "VALIDATION OF SOLUTION", "APPROVAL & LAUNCH", "CLINICAL USE",
			"STANDARD OF CARE" };

	public static final String[] DOMAIN_ARRAY = { "CLINICAL", "MARKET/BUS", "REGULATORY", "TECHNOLOGY" };

	public enum DOMAIN_ENUM { CLINICAL("CLINICAL", "CLINICAL"), MARKET("MARKETBUS","MARKET/BUS"), REGULATORY("REGULATORY","REGULATORY"), TECHNOLOGY("TECHNOLOGY","TECHNOLOGY");
		private String domain;
		private String identifier;
		DOMAIN_ENUM(String identifier, String domain) {
			this.domain = domain;
			this.identifier = identifier;
		};
		
		public String getIdentifier() {
			return this.identifier;
		}
		
		public static String getDomainByIdentifier(String name) {
			
			for(DOMAIN_ENUM d : DOMAIN_ENUM.values()) {
				if(Utils.isStringsSimilar(d.getIdentifier(), name))
					return d.getDomain();
			}
			return null;
		}
		
		public String getDomain() {
			return this.domain;
		}
		
		@Override
		public String toString() {
			return this.domain;
		}
	}
	
	
	public enum PHASE_ENUM { 
		CLINICAL_NEED(1,"CLINICAL NEED","CLINICAL NEED"), IDEA(2,"IDEA","IDEA"), PROOF_OF_CONCEPT(3,"PROOF OF CONCEPT", "PROOF OF CONCEPT"), PROOF_OF_FEASIBILITY(4, "PROOF OF FEASIBILITY", "PROOF OF FEASIBILITY"),
		PROOF_OF_VALUE(5, "PROOF OF VALUE","PROOF OF VALUE"), INITIAL_CLINICAL_TRIALS(6, "INITIAL CLINICAL TRIALS", "INITIAL CLINICAL TRIALS"), VALIDATION_OF_SOLUTION(7, "VALIDATION OF SOLUTION", "VALIDATION OF SOLUTION"),APPROVAL_AND_LAUNCH(8, "APPROVAL AND LAUNCH", "APPROVAL & LAUNCH"), CLINICAL_USE(9,"CLINICAL USE", "CLINICAL USE"),STANDARD_OF_CARE(10,"STANDARD OF CARE", "STANDARD OF CARE");
		private String phase;
		private String identifier;
		private int id;
		PHASE_ENUM(int id, String identifier,String phase) {
			this.phase = phase;
			this.id=id;
			this.identifier = identifier;
		};
		public String getIdentifier() {
			return this.identifier;
		}
		
		public static String getPhaseByIdentifier(String name) {
			
			for(PHASE_ENUM p : PHASE_ENUM.values()) {
				if(Utils.isStringsSimilar(p.getIdentifier(), name))
					return p.getPhase();
			}
			return null;
		}
		
		
		
		public static String getPhaseById(int id) {
			
			for(PHASE_ENUM p : PHASE_ENUM.values()) {
				if(p.getId() == id)
					return p.getPhase();
			}
			return null;
		}
		
		public int getId() {
			return this.id;
		}
		
		public String getPhase() {
			return this.phase;
		}
	
		@Override
		public String toString() {
			return this.phase;
		}
		
	}
	
	
	public enum Funding{
		HOME_INST(0,"HOME Inst."), 
		CIMIT(1,"CIMIT"), 
		NIH(2,"NIH"),
		DOD(3,"DoD"), 
		OTHER(4,"Other Fed"),
		STATE(5,"State"), 
		FOUNDATION(6,"Foundation"),
		PHIL(7,"Philantropy"), 
		CROWD(8,"Crowd"),
		INDUSTRY(9,"Industry"), 
		EQUITY(10,"Equity");
	
		Funding(int id,String name) {
	        _name = name;
	        _id = id;
	    }

	    private String _name;
	    private int _id;

	    public String getName() {
	        return _name;
	    }
	    
	    public int getId() {
	        return _id;
	    }
	}
	
	
	
	public static int getArrayNum(String[] arr, String id) {
		int j = 0;
		for (String s : Arrays.asList(arr)) {
			if (s.equals(id)) {
				return j;
			}
			j++;
		}
		return 0;
	}

	public enum Status {
		ZERO(0, "Please Select", 0), ONE(1, "0% to 20%", 10), TWO(2, "20% to 40%",30), FOUR(4, "40% to 60%", 50), SIX(6, "60% to 80%", 70), EIGHT(8,
				"80% to 99%", 90), COMPLETED(10, "Completed",100);

		Status(int id, String name, int precentage) {
			_name = name;
			_id = id;
			_precentage = precentage;
		}

		private String _name;
		private int _id;
		private int _precentage;

		public String getName() {
			return _name;
		}

		public int getId() {
			return _id;
		}
		
		public int getPrecentage() {
			return _precentage;
		}
		
		public static int getPrecentageById(int id) {
			for(Status s : Status.values()) {	
				if(s.getId() == id)
					return s.getPrecentage();
			}
			return 0;
		}
	}
	
	public enum TypeProject {
		MEDICAL(0, "HealthTech US");

		TypeProject(int id, String name) {
			_name = name;
			_id = id;
		}

		private String _name;
		private int _id;

		public String getName() {
			return _name;
		}

		public int getId() {
			return _id;
		}
		
		public static String getNameById(int id) {

			for(Enums.TypeProject t : Enums.TypeProject.values()) {
				if(t.getId() == id) {
					return t.getName();
				}
			}
			
			return null;
		}
	}

	public enum OutcomeCategory {
		AS_PLANNED(0, "As Planned"), MINOR_PIVOT(1, "Minor Pivot"), MAJOR_PIVOT(2, "Major Pivot"), STOPPED_PROGRAM(3,
				"Stopped Program"), LAUNCHED_NEW_PROGRAM(4, "Launched New Program");

		OutcomeCategory(int id, String name) {
			_name = name;
			_id = id;
		}

		private String _name;
		private int _id;

		public String getName() {
			return _name;
		}

		public int getId() {
			return _id;
		}
		
		public static String getNameById(int id) {

			for(Enums.OutcomeCategory t : Enums.OutcomeCategory.values()) {
				if(t.getId() == id) {
					return t.getName().replaceAll(" ", "&nbsp;");
				}
			}
			
			return null;
		}		

	}
	
	/* IMPORTANT! ORDER HAS MATTERS FOR HASHMAP OF PHASES AND DOMAINS! */
	
	public static final HashMap<String, String[]> phaseDomainMap = new LinkedHashMap<>();
	static {
		
		phaseDomainMap.put("CLINICAL NEED-TECHNOLOGY", new String[]{"CLINICAL NEED","TECHNOLOGY"});
		phaseDomainMap.put("IDEA-TECHNOLOGY", new String[]{"IDEA","TECHNOLOGY"});
		phaseDomainMap.put("PROOF OF CONCEPT-TECHNOLOGY", new String[]{"PROOF OF CONCEPT","TECHNOLOGY"});
		phaseDomainMap.put("PROOF OF FEASIBILITY-TECHNOLOGY", new String[]{"PROOF OF FEASIBILITY","TECHNOLOGY"});
		phaseDomainMap.put("PROOF OF VALUE-TECHNOLOGY", new String[]{"PROOF OF VALUE","TECHNOLOGY"});
		phaseDomainMap.put("INITIAL CLINICAL TRIALS-TECHNOLOGY", new String[]{"INITIAL CLINICAL TRIALS","TECHNOLOGY"});
		phaseDomainMap.put("VALIDATION OF SOLUTION-TECHNOLOGY", new String[]{"VALIDATION OF SOLUTION","TECHNOLOGY"});
		phaseDomainMap.put("APPROVAL AND LAUNCH-TECHNOLOGY", new String[]{"APPROVAL & LAUNCH","TECHNOLOGY"});
		phaseDomainMap.put("CLINICAL USE-TECHNOLOGY", new String[]{"CLINICAL USE","TECHNOLOGY"});
		phaseDomainMap.put("STANDART OF CARE-TECHNOLOGY", new String[]{"STANDARD OF CARE","TECHNOLOGY"});
		
		phaseDomainMap.put("CLINICAL NEED-REGULATORY", new String[]{"CLINICAL NEED","REGULATORY"});
		phaseDomainMap.put("IDEA-REGULATORY", new String[]{"IDEA","REGULATORY"});
		phaseDomainMap.put("PROOF OF CONCEPT-REGULATORY", new String[]{"PROOF OF CONCEPT","REGULATORY"});
		phaseDomainMap.put("PROOF OF FEASIBILITY-REGULATORY", new String[]{"PROOF OF FEASIBILITY","REGULATORY"});
		phaseDomainMap.put("PROOF OF VALUE-REGULATORY", new String[]{"PROOF OF VALUE","REGULATORY"});
		phaseDomainMap.put("INITIAL CLINICAL TRIALS-REGULATORY", new String[]{"INITIAL CLINICAL TRIALS","REGULATORY"});
		phaseDomainMap.put("VALIDATION OF SOLUTION-REGULATORY", new String[]{"VALIDATION OF SOLUTION","REGULATORY"});
		phaseDomainMap.put("APPROVAL AND LAUNCH-REGULATORY", new String[]{"APPROVAL & LAUNCH","REGULATORY"});
		phaseDomainMap.put("CLINICAL USE-REGULATORY", new String[]{"CLINICAL USE","REGULATORY"});
		phaseDomainMap.put("STANDART OF CARE-REGULATORY", new String[]{"STANDARD OF CARE","REGULATORY"});
		
		phaseDomainMap.put("CLINICAL NEED-MARKETBUS", new String[]{"CLINICAL NEED","MARKET/BUS"});
		phaseDomainMap.put("IDEA-MARKETBUS", new String[]{"IDEA","MARKET/BUS"});
		phaseDomainMap.put("PROOF OF CONCEPT-MARKETBUS", new String[]{"PROOF OF CONCEPT","MARKET/BUS"});
		phaseDomainMap.put("PROOF OF FEASIBILITY-MARKETBUS", new String[]{"PROOF OF FEASIBILITY","MARKET/BUS"});
		phaseDomainMap.put("PROOF OF VALUE-MARKETBUS", new String[]{"PROOF OF VALUE","MARKET/BUS"});
		phaseDomainMap.put("INITIAL CLINICAL TRIALS-MARKETBUS", new String[]{"INITIAL CLINICAL TRIALS","MARKET/BUS"});
		phaseDomainMap.put("VALIDATION OF SOLUTION-MARKETBUS", new String[]{"VALIDATION OF SOLUTION","MARKET/BUS"});
		phaseDomainMap.put("APPROVAL AND LAUNCH-MARKETBUS", new String[]{"APPROVAL & LAUNCH","MARKET/BUS"});
		phaseDomainMap.put("CLINICAL USE-MARKETBUS", new String[]{"CLINICAL USE","MARKET/BUS"});
		phaseDomainMap.put("STANDART OF CARE-MARKETBUS", new String[]{"STANDARD OF CARE","MARKET/BUS"});
		
		phaseDomainMap.put("CLINICAL NEED-CLINICAL", new String[]{"CLINICAL NEED","CLINICAL"});
		phaseDomainMap.put("IDEA-CLINICAL", new String[]{"IDEA","CLINICAL"});
		phaseDomainMap.put("PROOF OF CONCEPT-CLINICAL", new String[]{"PROOF OF CONCEPT","CLINICAL"});
		phaseDomainMap.put("PROOF OF FEASIBILITY-CLINICAL", new String[]{"PROOF OF FEASIBILITY","CLINICAL"});
		phaseDomainMap.put("PROOF OF VALUE-CLINICAL", new String[]{"PROOF OF VALUE","CLINICAL"});
		phaseDomainMap.put("INITIAL CLINICAL TRIALS-CLINICAL", new String[]{"INITIAL CLINICAL TRIALS","CLINICAL"});
		phaseDomainMap.put("VALIDATION OF SOLUTION-CLINICAL", new String[]{"VALIDATION OF SOLUTION","CLINICAL"});
		phaseDomainMap.put("APPROVAL AND LAUNCH-CLINICAL", new String[]{"APPROVAL & LAUNCH","CLINICAL"});
		phaseDomainMap.put("CLINICAL USE-CLINICAL", new String[]{"CLINICAL USE","CLINICAL"});
		phaseDomainMap.put("STANDART OF CARE-CLINICAL", new String[]{"STANDARD OF CARE","CLINICAL"});
		phaseDomainMap.put("VALIDATION OF SOLUTION-MARKETBUS", new String[]{"VALIDATION OF SOLUTION","MARKET/BUS"});
	
		
	}	
}
