package com.collaboratelab.project.constants;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import com.collaboratelab.project.constants.enums.Enums;
import com.collaboratelab.project.model.Project;
import com.collaboratelab.project.model.ProjectDelivery;
import com.collaboratelab.project.model.ProjectDomain;
import com.collaboratelab.project.model.ProjectPhase;
import com.collaboratelab.project.model.ProjectStep;
import com.collaboratelab.project.service.ProjectDeliveryLocalServiceUtil;
import com.collaboratelab.project.service.ProjectDomainLocalServiceUtil;
import com.collaboratelab.project.service.ProjectLocalServiceUtil;
import com.collaboratelab.project.service.ProjectPhaseLocalServiceUtil;
import com.collaboratelab.project.service.ProjectStepLocalServiceUtil;
import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetCategoryLocalServiceUtil;
import com.liferay.asset.kernel.service.AssetCategoryServiceUtil;
import com.liferay.asset.kernel.service.AssetEntryLocalServiceUtil;
import com.liferay.asset.kernel.service.AssetVocabularyServiceUtil;
import com.liferay.asset.kernel.service.persistence.AssetEntryQuery;
import com.liferay.document.library.kernel.exception.NoSuchFileException;
import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.document.library.kernel.service.DLAppLocalServiceUtil;
import com.liferay.document.library.kernel.service.DLAppServiceUtil;
import com.liferay.document.library.kernel.service.DLFileEntryLocalServiceUtil;
import com.liferay.document.library.kernel.service.DLFolderLocalServiceUtil;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalServiceUtil;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalServiceUtil;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalArticleResource;
import com.liferay.journal.model.JournalFolder;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.journal.service.JournalArticleResourceLocalServiceUtil;
import com.liferay.journal.service.JournalFolderLocalServiceUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.ResourceAction;
import com.liferay.portal.kernel.model.ResourceConstants;
import com.liferay.portal.kernel.model.ResourcePermission;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.IndexerRegistryUtil;
import com.liferay.portal.kernel.service.ClassNameLocalServiceUtil;
import com.liferay.portal.kernel.service.GroupLocalServiceUtil;
import com.liferay.portal.kernel.service.ResourceActionLocalServiceUtil;
import com.liferay.portal.kernel.service.ResourcePermissionLocalServiceUtil;
import com.liferay.portal.kernel.service.RoleLocalServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ContentTypes;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.DocumentException;
import com.liferay.portal.kernel.xml.Node;
import com.liferay.portal.kernel.xml.SAXReaderUtil;
import com.liferay.portlet.asset.util.comparator.AssetCategoryCreateDateComparator;

public class Utils {
	
	private static final Log logger = LogFactoryUtil.getLog(Utils.class);
	
	private static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	public static final String PROJECT_MEDICAL_TYPE_VOCABULARY = "HealthTech US";
	public static final String DELIVERABLE_FILES_FOLDER = "Deliverable Files";
	//public static final String DELIVERABLE = "Deliverable";
	public static final String DELIVERABLE_PLANNING = "Deliverable Planning";
	public static final String DELIVERABLE_PLANNING_POSTFIX = " (Planning)";
	
	public static final String ROOT_HIDDEN_FOLDER_ON_TARGET_SITE = "Populated from Global resources";
	
	public static String getDateAsString(Date date) {
		return dateFormat.format(date);
	}
	
	public static int getDeliverablePhaseNum(ProjectDelivery d) {
		return Enums.getArrayNum(Enums.PHASES_ARRAY, d.getPhase());
	}

	public static long getGlobalGroupId(long companyId) {
		List<Group> groups = GroupLocalServiceUtil.getGroups(0, GroupLocalServiceUtil.getGroupsCount());
		for (Group site : groups) {
			if (site.getFriendlyURL().equalsIgnoreCase(Constants.GLOBAL_GROUP_URL)) {
				return site.getGroupId();
			}
		}
		return 0;
	}

	public static String getDDMTemplateKeyDeliveryWWH(long globalSiteId) {
		List<DDMTemplate> templates = getAllGlobalDDMTemplates(globalSiteId);
		for (DDMTemplate templ : templates) {
			if (templ.getName().contains(Constants.DDM_TEMPLATE_WHAT_WHY_HOW_NAME)) {
				return templ.getTemplateKey();
			}
		}
		return null;
	}

	public static List<ProjectDelivery> sortDeliverablesByName(long globalGroupId, List<ProjectDelivery> dels) {
		List<ProjectDelivery> list = new ArrayList<>(dels);
		list.sort((ProjectDelivery o1, ProjectDelivery o2) -> Utils
				.getNameDeliveryFromJournalArticleContent(globalGroupId, String.valueOf(o1.getType())).compareTo(
						Utils.getNameDeliveryFromJournalArticleContent(globalGroupId, String.valueOf(o2.getType()))));
		return list;
	}
	
	public static String getDDMStructureKeyDeliveryWWH(long globalSiteId) {
		List<DDMStructure> structures = getAllGlobalDDMStructures(globalSiteId);
		for (DDMStructure struct : structures) {
			if (struct.getName().contains(Constants.DDM_STRUCTURE_WHAT_WHY_HOW_NAME)) {
				return struct.getStructureKey();
			}
		}
		return null;
	}

	public static List<DDMTemplate> getAllGlobalDDMTemplates(long groupId) {
		long structureClassNameId = ClassNameLocalServiceUtil.getClassNameId(DDMStructure.class);
		List<DDMTemplate> templates = DDMTemplateLocalServiceUtil.getTemplates(groupId, structureClassNameId);
		return templates;
	}
	
	
	public static String getTemplateByName(long groupId, String name) {
		List<DDMTemplate> templates = getAllGlobalDDMTemplates(groupId);
		for (DDMTemplate templ : templates) {
			String nameXMl = templ.getName(Locale.ENGLISH.toString());
			if (nameXMl.equals(name)) {
				return templ.getTemplateKey();
			}
		}
		return null;
	}
	
	public static JournalFolder createFolder(String folderName, long groupId, long parentFolderId,
			long userId) {
		JournalFolder folder = null;
		try {
			ServiceContext serviceContext = new ServiceContext();
			serviceContext.setScopeGroupId(groupId);
			folder = JournalFolderLocalServiceUtil.addFolder(userId, groupId, 0, folderName, "", serviceContext);
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (PortalException e) {
			e.printStackTrace();
		}
		return folder;
	}

	public static List<DDMStructure> getAllGlobalDDMStructures(long groupId) {
		long structureClassNameId = ClassNameLocalServiceUtil.getClassNameId(JournalArticle.class);
		List<DDMStructure> structures = DDMStructureLocalServiceUtil.getStructures(groupId, structureClassNameId);
		return structures;
	}

	public static String fetchHTMLMainJournalArticle(ThemeDisplay themeDisplay, String ddmTemplateKey,
			long globalGroupId, String prefix, String articleURL) throws PortalException {
		//ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		JournalArticle journalArticle = JournalArticleLocalServiceUtil.fetchArticleByUrlTitle(globalGroupId,
				prefix + articleURL);
		if (journalArticle == null) {
			return null;
		} else {
			// TODO fix deprecated
			return JournalArticleLocalServiceUtil.getArticleContent(journalArticle, ddmTemplateKey,
					com.liferay.portal.kernel.util.Constants.VIEW, themeDisplay.getLanguageId(), themeDisplay);
		}

	}
	
    public static Role getRoleById(final long companyId, final String roleStrId) {
        try {
            return RoleLocalServiceUtil.getRole(companyId, roleStrId);
        } catch (final Exception e) {
        	logger.error("Utils::getRoleById Exception", e);
        }
        return null;
    }	
	
	public static String getDateByLocale(Date date, Locale locale) {
		
		DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, locale);
		String formattedDate = df.format(date);
		
		return formattedDate;	
	}
	
	public static long[] getVocabularyByName(long globalGroupId, String name) {
		List<AssetVocabulary> vocaList = getAssetVocabularyByName(globalGroupId, name);
		long[] res = new long[vocaList.size()];
		int i = 0;
		for (AssetVocabulary voca : vocaList) {
			res[i++] = voca.getVocabularyId();
		}
		return res;
	}
	
	public static List<AssetVocabulary> getAssetVocabularyByName(long globalGroupId, String name) {
		return AssetVocabularyServiceUtil.getGroupVocabularies(globalGroupId, name,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}
	
	public static long getCategoryByDomainAndPhase(long globalGroupId, int domainEnumId, int phaseEnumId) throws PortalException {
		// TODO change when prj types are available
		List<AssetVocabulary> vocaList = getAssetVocabularyByName(globalGroupId, PROJECT_MEDICAL_TYPE_VOCABULARY);
		AssetVocabulary mainVoca = null;
		for (AssetVocabulary voca : vocaList) {
			mainVoca = voca;
			break;
		}	
		if (mainVoca != null) {
			List<AssetCategory> domainCategories = AssetCategoryServiceUtil.getVocabularyRootCategories(
					mainVoca.getGroupId(), mainVoca.getVocabularyId(), QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
			String domainName = Enums.DOMAIN_ENUM.values()[domainEnumId].toString();
			String phaseName = Enums.PHASES_ARRAY[phaseEnumId];
			for (AssetCategory domainsCat : domainCategories) {
				if (isStringsSimilar(domainsCat.getName(), domainName)) {
					for (AssetCategory milestonesCat : AssetCategoryServiceUtil
							.getChildCategories(domainsCat.getCategoryId())) {
						if (isStringsSimilar(milestonesCat.getName(), phaseName)) {
							return milestonesCat.getCategoryId();
						}
					}
				}
			}
		}
		return 0;
	}
	
	public static String getRequestParamIdForMedicalPrj(long globalId) {
		String res = "";
		List<AssetVocabulary> list = Utils.getAssetVocabularyByName(globalId, PROJECT_MEDICAL_TYPE_VOCABULARY);
		if (list.size() > 0) {
			res = "assetCategoryIds_" + list.get(0).getVocabularyId();
		}
		return res;
	}
	
	public static boolean isStringsSimilar(String s1, String s2) {
		return s1.toLowerCase().contains(s2.toLowerCase()) || s2.toLowerCase().contains(s1.toLowerCase());
	}
	
	public static long getCategoryByDomainAndPhaseAndDeliverable(long globalGroupId, int domainEnumId, int phaseEnumId,
			int deliveryNumInMilestone) throws PortalException {
		long milestoneCatId = getCategoryByDomainAndPhase(globalGroupId, domainEnumId, phaseEnumId);
		if (milestoneCatId > 0) {
			int j = 0;
			//OrderByComparator<AssetCategory> orderByComparator = new AssetCategoryCreateDateComparator(true);
			// now order by name
			List<AssetCategory> deliverableCategories = AssetCategoryServiceUtil.getChildCategories(milestoneCatId,
					QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
			for (AssetCategory d : deliverableCategories) {
				if (++j == deliveryNumInMilestone) {
					return d.getCategoryId();
				}
			}
		}
		return 0;
	}
	
	/*
	 * copy assets from global to new site
	 */
	public static void populateResourcesByCategory(long stepCatId, long userId, long targetGroupId, long globalGroupId)
			throws PortalException {
		if (stepCatId != 0) {
			List<AssetCategory> deliversListCatIds = AssetCategoryServiceUtil.getChildCategories(stepCatId);
			long deliversCatIds[];
			if (deliversListCatIds.size() > 0) {
				deliversCatIds = new long[deliversListCatIds.size() + 1];
				deliversCatIds[0] = stepCatId;
				int i = 1;
				for (AssetCategory delCatId : deliversListCatIds) {
					deliversCatIds[i] = delCatId.getCategoryId();
					i++;
				}
			} else {
				deliversCatIds = new long[1];
				deliversCatIds[0] = stepCatId;
			}

			AssetEntryQuery query = new AssetEntryQuery();
			query.setAnyCategoryIds(deliversCatIds);
			query.setGroupIds(new long[] { globalGroupId });

			List<AssetEntry> assetEntries = AssetEntryLocalServiceUtil.getEntries(query);

			for (AssetEntry entry : assetEntries) {
				addAssetEntryInProject(entry,targetGroupId,userId,globalGroupId);
			}
		}

	}
	
	
	public static void populateFourArticleForProfilePage() {
		
		
	}
	
	
	
	public static String[] getColorsForSteps(long projectId, long projectStructureId) {
		
		String[] colors = new String[45];
		
		for(int j=0; j <45; j++)
		{
			colors[j] = "shaded,";
		}
		
		int i = 1;
		 Iterator it = Enums.phaseDomainMap.entrySet().iterator();
		    while (it.hasNext()) {
		    	if(i % 11 == 0) {
		    		i++;
		    	}
		        Map.Entry pair = (Map.Entry)it.next();
		        String color = getColorForStep(String.valueOf(pair.getKey()),projectStructureId,projectId);
		        colors[i] = color + ",";
		        i++;
		    }
		return colors;
	}
	
	
	public static String getColorForStep(String key, long projectStructureId, long projectId){
		
		String[] array = Enums.phaseDomainMap.get(key);
		String phaseStr = array[0];
		
		String domainStr = array[1];
		
		ProjectDomain domain = ProjectDomainLocalServiceUtil
				.findByProjectStructureIdIdentifier(projectStructureId, domainStr);
		ProjectPhase phase = ProjectPhaseLocalServiceUtil
				.findByProjectStructureIdIdentifier(projectStructureId, phaseStr);
		
		
		ProjectStep step = ProjectStepLocalServiceUtil.findByProjectIdProjectDomainIdProjectPhaseId(projectId, domain.getProjectDomainId(), phase.getProjectPhaseId());
		
		List<ProjectDelivery> deliveries = ProjectDeliveryLocalServiceUtil
				.findByStepId(step.getProjectStepId());


		int step_percentage = 0;
		// TODO: available -> false, unavailable -> true, to change
		List<ProjectDelivery> deliveries2 = deliveries.stream().filter(d -> !d.getIsAvailable()).collect(Collectors.toList());
		int number = deliveries.size();
		int number2 = deliveries2.size();

		for(ProjectDelivery d2 : deliveries2) {
			step_percentage += Enums.Status.getPrecentageById(d2.getStatus());
		}
		
		int sum = 0;
		if (number != 0)
			if (number2 == 0)
				sum = 100;
			else
				sum = step_percentage * 100 / (number2 * 100);

		boolean isBetween1and99 = sum > 0 && sum < 100;
		if (domainStr.equals(Enums.DOMAIN_ARRAY[0])) {
			if (isBetween1and99) {
				return "#8aa1ce";
			} else if (sum == 100) {
				return "#0D1F43";
			}
			// return "#3c61a8";
		} else if (domainStr.equals(Enums.DOMAIN_ARRAY[1])) {
			if (isBetween1and99) {
				return "#e8ef8d";
			} else if (sum == 100) {
				return "#A8B139";
			}
			// return "#dbe55b";
		} else if (domainStr.equals(Enums.DOMAIN_ARRAY[2])) {
			if (isBetween1and99) {
				return "#74bcbf";
			} else if (sum == 100) {
				return "#0F4749";
			}
			// return "#3d9599";
		} else if (domainStr.equals(Enums.DOMAIN_ARRAY[3])) {
			if (isBetween1and99) {
				return "#ffa482";
			} else if (sum == 100) {
				return "#E35A25";
			}
		}
		return "#d1d1d1";

	}
	
	
	public static void addAssetEntryInProject(AssetEntry entry, long targetGroupId, long userId , long globalGroupId) throws PortalException  {
		if (entry.getClassNameId() == ClassNameLocalServiceUtil.getClassNameId(DLFileEntry.class) ) {
			ServiceContext serviceContext = new ServiceContext();
			serviceContext.setScopeGroupId(targetGroupId);

			DLFileEntry dLFileEntry = DLFileEntryLocalServiceUtil.getDLFileEntry(entry.getClassPK());
			
			try {
				long targetSiteFolderId = getTargetSiteFolderId(DLAppServiceUtil.getFileEntry(entry.getClassPK()), targetGroupId, userId);
				
				if (DLFileEntryLocalServiceUtil.fetchFileEntry(targetGroupId, targetSiteFolderId, dLFileEntry.getTitle()) != null) {
					// skip file creation if exists. It can happen when a file belongs to 2 our categories - then it will try to create
					// this file twice (because of 2 cats e.g. clinical and clinical need) within a single dir.
					return;
				}
				
				FileEntry newDLFileEntry = DLAppServiceUtil.addFileEntry(targetGroupId, targetSiteFolderId, dLFileEntry.getFileName(),
						dLFileEntry.getMimeType(), dLFileEntry.getTitle(), dLFileEntry.getDescription(), "",
						dLFileEntry.getContentStream(), dLFileEntry.getSize(), serviceContext);
				DLAppLocalServiceUtil.updateAsset(userId, newDLFileEntry, newDLFileEntry.getFileVersion(), entry.getCategoryIds(), null, null);
				
				Indexer<FileEntry> indexer = IndexerRegistryUtil.nullSafeGetIndexer(FileEntry.class);
				
				if (dLFileEntry != null) {
					newDLFileEntry.getExpandoBridge().setAttribute("parentEntry",dLFileEntry.getFileEntryId());
				}
				
				indexer.reindex(newDLFileEntry);
				
				copyPermissionForDLFileEntry(dLFileEntry, newDLFileEntry);
			} catch (NoSuchFileException e ) {
				// ignore when no source file found (it happens when only db copied without a content)
			} catch (Exception e) {
				logger.error("addAssetEntryInProject:: ", e);
			}
				
		} else if(entry.getClassNameId() == ClassNameLocalServiceUtil.getClassNameId(JournalArticle.class) ){
			
			JournalArticleResource journalArticleResourceObj = JournalArticleResourceLocalServiceUtil
					.getJournalArticleResource(entry.getClassPK());

			JournalArticle entrArticle = JournalArticleLocalServiceUtil.getLatestArticle(globalGroupId,
					journalArticleResourceObj.getArticleId());

			
			ServiceContext serviceContext = new ServiceContext();
			serviceContext.setScopeGroupId(targetGroupId);
			
			JournalFolder folder = createWebContentFolder(targetGroupId, userId, 0, "Populated from global");
			
			// skip if duplicate by category. Needed if one entry has 2 cats at the same time (we don't need 2 copies of this)
			if (isWcDuplicateExists(targetGroupId, entrArticle.getTitleCurrentValue(), folder.getFolderId(), entry.getCategories(), true)) {
				return;
			}

			JournalArticle article = JournalArticleLocalServiceUtil.addArticle(userId, targetGroupId, folder.getFolderId(),
					entrArticle.getTitleMap(), entrArticle.getDescriptionMap(), entrArticle.getContent(),
					entrArticle.getDDMStructureKey(), entrArticle.getDDMTemplateKey(), serviceContext);

			if (entrArticle != null) {
				article.getExpandoBridge().setAttribute("parentEntry", entrArticle.getArticleId());
			}
			
			
			
			AssetEntryLocalServiceUtil.updateEntry(userId, article.getGroupId(), article.getCreateDate(),
					article.getModifiedDate(), JournalArticle.class.getName(), article.getResourcePrimKey(),
					article.getUuid(), 0, entry.getCategoryIds(), entry.getTagNames(), true, true, null, null,
					article.getCreateDate(), null, ContentTypes.TEXT_HTML, entry.getTitle(), null, null, null, null,
					0, 0, null);

			Indexer<JournalArticle> indexer = IndexerRegistryUtil.nullSafeGetIndexer(JournalArticle.class);

			indexer.reindex(article);
			
			copyPermissionForJournalArticle(entrArticle, article);
				
		}
		
	}
	
	public static boolean isWcDuplicateExists(long targetroup, String articleTitle, long folderId, List<AssetCategory> cats, boolean isOnSiteCreate) {
		JournalArticle duplicate = Utils.fetchJournalArticleByParentFolderId(targetroup, articleTitle, folderId);		
		if (duplicate != null && !duplicate.isInTrash() && cats.size() > 0) {
			AssetEntry ae = AssetEntryLocalServiceUtil.fetchEntry(JournalArticle.class.getName(), duplicate.getResourcePrimKey()); 
			List<AssetCategory> intersect = ae.getCategories().stream()
                     .filter(cats::contains)
                     .collect(Collectors.toList());
			if (isOnSiteCreate) {
				if (intersect.size() == cats.size()) {
					return true;
				}
			} else {
				if (intersect.size() > 0) {
					return true;
				}
			}
		}
		return false;
	}

	public static void copyPermissionForDLFileEntry(DLFileEntry dLFileEntry, FileEntry newDLFileEntry) throws PortalException {
		
		List<Role> roles = RoleLocalServiceUtil.getRoles(dLFileEntry.getCompanyId());
		List<ResourceAction> actions = ResourceActionLocalServiceUtil.getResourceActions(DLFileEntry.class.getName());
		if(newDLFileEntry != null) {
			for(Role r : roles) {
				List<String> actionsIds = new ArrayList<>();
				for(ResourceAction a : actions) {
					ResourcePermission resourcePermission = ResourcePermissionLocalServiceUtil.fetchResourcePermission(r.getCompanyId(),
							 DLFileEntry.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL, String.valueOf(dLFileEntry.getFileEntryId()), r.getRoleId());
					boolean isHasActionId = true;
					
					if(resourcePermission != null)
						isHasActionId = ResourcePermissionLocalServiceUtil.hasActionId(resourcePermission, a);

					if(isHasActionId && ResourcePermissionLocalServiceUtil.hasResourcePermission(r.getCompanyId(), DLFileEntry.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL, String.valueOf(dLFileEntry.getFileEntryId()), r.getRoleId(), a.getActionId())) {
						actionsIds.add(a.getActionId());
					}
					
				}
				String[] actionIdsStr = new String[actionsIds.size()];
				int j=0;
				for(String a : actionsIds) {
					actionIdsStr[j] = a;
					j++;
				}
				try {
					ResourcePermissionLocalServiceUtil.setResourcePermissions(newDLFileEntry.getCompanyId(), DLFileEntry.class.getName(),ResourceConstants.SCOPE_INDIVIDUAL, String.valueOf(newDLFileEntry.getFileEntryId()), r.getRoleId(),actionIdsStr);
				} catch (PortalException e) {
					logger.error("can't copy permissions for resource", e);
				}
				
			}
		}
		

		/*long companyId = dLFileEntry.getCompanyId();
		Role role = getRoleById(companyId, RoleConstants.ADMINISTRATOR);
		try {
			String[] actionIdsStr = new String[] { ActionKeys.VIEW, ActionKeys.UPDATE, ActionKeys.DEFINE_PERMISSIONS };
			ResourcePermissionLocalServiceUtil.setResourcePermissions(newDLFileEntry.getCompanyId(),
					DLFileEntry.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL,
					String.valueOf(newDLFileEntry.getFileEntryId()), role.getRoleId(), actionIdsStr);
		} catch (PortalException e) {
			logger.error("can't add permissions for resource", e);
		}*/		
	}
	
	
	public static void copyPermissionForJournalArticle(JournalArticle article, JournalArticle newArticle) throws PortalException {
		
		List<Role> roles = RoleLocalServiceUtil.getRoles(article.getCompanyId());
		List<ResourceAction> actions = ResourceActionLocalServiceUtil.getResourceActions(JournalArticle.class.getName());
		if(article != null) {
			for(Role r : roles) {
				List<String> actionsIds = new ArrayList<>();
				for(ResourceAction a : actions) {
					ResourcePermission resourcePermission = ResourcePermissionLocalServiceUtil.fetchResourcePermission(r.getCompanyId(),
							JournalArticle.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL, String.valueOf(article.getResourcePrimKey()), r.getRoleId());
					boolean isHasActionId = true;
					
					if(resourcePermission != null)
						isHasActionId = ResourcePermissionLocalServiceUtil.hasActionId(resourcePermission, a);

					if(isHasActionId && ResourcePermissionLocalServiceUtil.hasResourcePermission(r.getCompanyId(), JournalArticle.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL, String.valueOf(article.getResourcePrimKey()), r.getRoleId(), a.getActionId())) {
						actionsIds.add(a.getActionId());
					}
					
				}
				String[] actionIdsStr = new String[actionsIds.size()];
				int j=0;
				for(String a : actionsIds) {
					actionIdsStr[j] = a;
					j++;
				}
				try {
					ResourcePermissionLocalServiceUtil.setResourcePermissions(newArticle.getCompanyId(), JournalArticle.class.getName(),ResourceConstants.SCOPE_INDIVIDUAL, String.valueOf(newArticle.getResourcePrimKey()), r.getRoleId(),actionIdsStr);
				} catch (PortalException e) {
					logger.error("can't copy permissions for resource", e);
				}
				
			}
		}
	}
	
	public static void updateAssetEntryInProject(AssetEntry entry, long targetGroupId, long userId , long globalGroupId) throws PortalException  {
		if (entry.getClassNameId() == ClassNameLocalServiceUtil.getClassNameId(DLFileEntry.class) ) {
			ServiceContext serviceContext = new ServiceContext();
			serviceContext.setScopeGroupId(targetGroupId);

			DLFileEntry dLFileEntry = DLFileEntryLocalServiceUtil.getDLFileEntry(entry.getClassPK());
			// create if not exists only
			if (DLFileEntryLocalServiceUtil.fetchFileEntry(targetGroupId, 0, dLFileEntry.getTitle()) == null) {
				try {
					FileEntry newDLFileEntry = DLAppServiceUtil.addFileEntry(targetGroupId, 0, dLFileEntry.getTitle(),
							dLFileEntry.getMimeType(), dLFileEntry.getTitle(), dLFileEntry.getDescription(), "",
							dLFileEntry.getContentStream(), dLFileEntry.getSize(), serviceContext);
					DLAppLocalServiceUtil.updateAsset(userId, newDLFileEntry, newDLFileEntry.getFileVersion(), entry.getCategoryIds(), null, null);
	
					Indexer<FileEntry> indexer = IndexerRegistryUtil.nullSafeGetIndexer(FileEntry.class);
	
					indexer.reindex(newDLFileEntry);
				} catch (NoSuchFileException e ) {
					// ignore when no source file found (it happens when only db copied without a content)
				}
			}
			else {
				
				FileEntry newDLFileEntry = DLAppServiceUtil.updateFileEntry(dLFileEntry.getFileEntryId(), dLFileEntry.getTitle(), dLFileEntry.getMimeType(), dLFileEntry.getTitle(),dLFileEntry.getDescription(), "", true, dLFileEntry.getContentStream(), dLFileEntry.getSize(), serviceContext);
				
				DLAppLocalServiceUtil.updateAsset(userId, newDLFileEntry, newDLFileEntry.getFileVersion(), entry.getCategoryIds(), null, null);

				DLFileEntryLocalServiceUtil.fetchFileEntry(targetGroupId, 0, dLFileEntry.getTitle());
				
			}
		} else if(entry.getClassNameId() == ClassNameLocalServiceUtil.getClassNameId(JournalArticle.class) ){
			
			JournalArticleResource journalArticleResourceObj = JournalArticleResourceLocalServiceUtil
					.getJournalArticleResource(entry.getClassPK());

			JournalArticle entrArticle = JournalArticleLocalServiceUtil.getLatestArticle(globalGroupId,
					journalArticleResourceObj.getArticleId());

			if(JournalArticleLocalServiceUtil.fetchArticleByUrlTitle(targetGroupId, entrArticle.getUrlTitle()) == null) {
				
				ServiceContext serviceContext = new ServiceContext();
				serviceContext.setScopeGroupId(targetGroupId);
				
				JournalFolder folder = createWebContentFolder(targetGroupId, userId, 0, "Populated from global");
	
				JournalArticle article = JournalArticleLocalServiceUtil.addArticle(userId, targetGroupId, folder.getFolderId(),
						entrArticle.getTitleMap(), entrArticle.getDescriptionMap(), entrArticle.getContent(),
						entrArticle.getDDMStructureKey(), entrArticle.getDDMTemplateKey(), serviceContext);
	
				if (entrArticle != null) {
					article.getExpandoBridge().setAttribute("parentEntry", entrArticle.getArticleId());
				}
				
				
				
				AssetEntryLocalServiceUtil.updateEntry(userId, article.getGroupId(), article.getCreateDate(),
						article.getModifiedDate(), JournalArticle.class.getName(), article.getResourcePrimKey(),
						article.getUuid(), 0, entry.getCategoryIds(), entry.getTagNames(), true, true, null, null,
						article.getCreateDate(), null, ContentTypes.TEXT_HTML, entry.getTitle(), null, null, null, null,
						0, 0, null);
	
				Indexer<JournalArticle> indexer = IndexerRegistryUtil.nullSafeGetIndexer(JournalArticle.class);
	
				indexer.reindex(article);
			}
			else {
				
				ServiceContext serviceContext = new ServiceContext();
				serviceContext.setScopeGroupId(targetGroupId);
				
				JournalFolder folder = createWebContentFolder(targetGroupId, userId, 0, "Populated from global");
				JournalArticle article = JournalArticleLocalServiceUtil.fetchArticleByUrlTitle(targetGroupId, entrArticle.getUrlTitle());
				
				JournalArticle newArticle = JournalArticleLocalServiceUtil.updateArticle(article.getUserId(), targetGroupId, folder.getFolderId(), entrArticle.getUrlTitle(), article.getVersion(), entrArticle.getTitleMap(), entrArticle.getDescriptionMap(), entrArticle.getContent(), article.getLayoutUuid(), serviceContext);
	
				if (entrArticle != null) {
					newArticle.getExpandoBridge().setAttribute("parentEntry", entrArticle.getArticleId());
				}
				newArticle = JournalArticleLocalServiceUtil.updateJournalArticle(newArticle);
				
				AssetEntryLocalServiceUtil.updateEntry(userId, newArticle.getGroupId(), newArticle.getCreateDate(),
						newArticle.getModifiedDate(), JournalArticle.class.getName(), newArticle.getResourcePrimKey(),
						newArticle.getUuid(), 0, entry.getCategoryIds(), entry.getTagNames(), true, true, null, null,
						newArticle.getCreateDate(), null, ContentTypes.TEXT_HTML, entry.getTitle(), null, null, null, null,
						0, 0, null);
	
				Indexer<JournalArticle> indexer = IndexerRegistryUtil.nullSafeGetIndexer(JournalArticle.class);
	
				indexer.reindex(newArticle);
			}
		}
		
	}
	
	public static List<AssetCategory> getCategories(long[] categoryIds) {
			
			List<AssetCategory> categories = new ArrayList<AssetCategory>();
			
			for(long catId : categoryIds) {
				
				AssetCategory temp_cat = AssetCategoryLocalServiceUtil.fetchAssetCategory(catId);
				
				if(temp_cat != null)
					categories.add(temp_cat);
			}
			
			return categories;
	}
	
	public static JournalFolder fetchFolder(long groupId, String name, long parentId){
		
		JournalFolder folder = JournalFolderLocalServiceUtil.fetchFolder(groupId, parentId, name);
		
		if (folder != null) {
			return folder;
		}
		
		return null;
	}
	
	public static long[] getCategoryIds(String categoryStr) {
		String[] categoryIdsStr = categoryStr.split(",");
		long[] categoryIds = new long[categoryIdsStr.length];
		int j = 0;
		for(String catId : categoryIdsStr) {
			categoryIds[j] = Long.parseLong(catId);
			j++;
		}
		return categoryIds;
	}
	
	
	public static JournalFolder createWebContentFolder(long groupId, long userId, long parentId,
			String name) {
		JournalFolder folder = JournalFolderLocalServiceUtil.fetchFolder(groupId, parentId, name);
		if (folder != null) {
			return folder;
		}
		try {
			ServiceContext serviceContext = new ServiceContext();
			serviceContext.setScopeGroupId(groupId);
			
			// To create a project folder in an Organization
			folder = JournalFolderLocalServiceUtil.addFolder(userId, groupId, parentId, name, "",
					serviceContext);
		} catch (SystemException e) {
			e.printStackTrace();
		} catch (PortalException e) {
			e.printStackTrace();
		}
		return folder;
	}
	
	public static List<Project> getProjectsByType(int type) {

		DynamicQuery projectByTypeQuery = ProjectLocalServiceUtil.dynamicQuery()
				.add(RestrictionsFactoryUtil.eq("projectType", type));

		List<Project> projectsByType = ProjectLocalServiceUtil.dynamicQuery(projectByTypeQuery);

		return projectsByType;
	}

	public static String getNameDeliveryFromJournalArticleContent(long groupId, String articleId) {
		JournalArticle ja = getJournalArticle(groupId, articleId);
		return getNameDeliveryFromJournalArticleContent(groupId, ja);
	}
	
	public static String getNameDeliveryFromJournalArticleContent(long groupId, JournalArticle ja) {
		if (ja != null) {
			com.liferay.portal.kernel.xml.Document document = null;
			try {
				document = SAXReaderUtil.read(ja.getContent());
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			com.liferay.portal.kernel.xml.Node deliveryNameNode = document
					.selectSingleNode("/root/dynamic-element[@name='Textea5m']/dynamic-content");
			String res = "";
			if (deliveryNameNode != null) { 
				res = deliveryNameNode.getText().isEmpty() ? ja.getTitleCurrentValue() : deliveryNameNode.getText(); 
			} else {
				res = "ERROR: wrong web content structure";
			}
			return res;

		}
		return "";
	}

	public static JournalArticle getJournalArticle(long groupId, String articleId) {
		JournalArticle ja = null;
		ja = JournalArticleLocalServiceUtil.fetchArticle(groupId, articleId);
		return ja;
	}
	
	public static JournalArticle fetchJournalArticleByParentFolderId(long groupId, String articleTitle, long parentFolderId) {
		
		List<JournalArticle> jas = JournalArticleLocalServiceUtil.getArticles(groupId, parentFolderId);
		JournalArticle ja = null;
		for(JournalArticle j : jas) {
			if(j.getTitleCurrentValue().equals(articleTitle))
				return j;
		}
		return null;
	}
	
	private static LinkedList<String> getFilePath(FileEntry dLFileEntry) {
		LinkedList<String> list = new LinkedList<String>();
		if (dLFileEntry.getFolderId() > 0) {
			Folder folder = dLFileEntry.getFolder();
			try {
				while (folder != null) {
					list.add(folder.getName());
					folder = folder.getParentFolder();
				}
			} catch (PortalException e) {
				logger.error("getFolderPath:: ", e);
			}
		}
		return list;
	}
	
	public static DLFolder getOrCreateFolder(String name, long parentId, long targetGroupId, long userId) {
		DLFolder res = DLFolderLocalServiceUtil.fetchFolder(targetGroupId, parentId, name);
		if (res == null) {
			try {
				ServiceContext serviceContext = new ServiceContext();
				//serviceContext.setScopeGroupId(groupId);
				res = DLFolderLocalServiceUtil.addFolder(userId, targetGroupId, targetGroupId, false, parentId, name,
						"", false, serviceContext);
			} catch (SystemException e) {
				logger.error(e);
			} catch (PortalException e) {
				logger.error(e);
			}
		}
		return res;
	}
	
	public static long getTargetSiteFolderId(FileEntry fileEntry, long targetGroupId, long userId) {
		LinkedList<String> path = getFilePath(fileEntry);
		// get or create root folder
		DLFolder folder = getOrCreateFolder(ROOT_HIDDEN_FOLDER_ON_TARGET_SITE, 0, targetGroupId, userId);
		if (path.size() > 0) {
			Iterator<String> it = path.descendingIterator();
			while (it.hasNext()) {
				String name = it.next();
				folder = getOrCreateFolder(name, folder.getFolderId(), targetGroupId, userId);				
			}
		}
		return folder.getFolderId();
	}	
}
