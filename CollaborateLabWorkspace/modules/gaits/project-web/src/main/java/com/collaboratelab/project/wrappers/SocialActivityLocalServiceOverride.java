package com.collaboratelab.project.wrappers;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceWrapper;
import com.liferay.social.kernel.model.SocialActivity;
import com.liferay.social.kernel.model.SocialActivitySet;
import com.liferay.social.kernel.service.SocialActivityLocalServiceWrapper;
import com.liferay.social.kernel.service.SocialActivitySetLocalServiceUtil;

@Component(
	    immediate = true,
	    property = {
	    },
	    service = ServiceWrapper.class
	)
public class SocialActivityLocalServiceOverride extends SocialActivityLocalServiceWrapper {

	public SocialActivityLocalServiceOverride() {
		super(null);
	}

	@Override
	public void deleteActivities(String className, long classPK) throws PortalException {
		SocialActivity activity = fetchFirstActivity(className, classPK, 1); 
		if (activity != null) {
			SocialActivitySet set = SocialActivitySetLocalServiceUtil.fetchSocialActivitySet(activity.getActivitySetId());
			if (set != null) {
				super.deleteActivities(className, classPK);
			}
		}
	}
}
