package com.collaboratelab.project.constants;

public class DLPortletKeys {
	
	public static final String DOCUMENT_LIBRARY =
			"com_liferay_document_library_web_portlet_DLPortlet";

		public static final String DOCUMENT_LIBRARY_ADMIN =
			"com_liferay_document_library_web_portlet_DLAdminPortlet";

		public static final String MEDIA_GALLERY_DISPLAY =
			"com_liferay_document_library_web_portlet_IGDisplayPortlet";


}
