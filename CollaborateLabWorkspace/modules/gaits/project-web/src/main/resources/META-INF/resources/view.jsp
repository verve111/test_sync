<%@ include file="init.jsp" %>

 <script src="https://code.jquery.com/jquery-1.10.2.js"></script>


<%
PortletURL currentURLObj = PortletURLUtil.getCurrent(liferayPortletRequest, liferayPortletResponse);
String currentURL = currentURLObj.toString();

String viewOrganizationsRedirect = ParamUtil.getString(request, "viewOrganizationsRedirect");

String redirect = ParamUtil.getString(request, "redirect", viewOrganizationsRedirect);

if (Validator.isNull(redirect)) {
	PortletURL portletURL = renderResponse.createRenderURL();

	redirect = portletURL.toString();
}

String backURL = ParamUtil.getString(request, "backURL", redirect);

long groupId = ParamUtil.getLong(request, "groupId", GroupConstants.DEFAULT_PARENT_GROUP_ID);

Group group = null;

if (groupId > 0) {
	group = GroupLocalServiceUtil.getGroup(groupId);
}

long parentGroupId = ParamUtil.getLong(request, "parentGroupSearchContainerPrimaryKeys", GroupConstants.DEFAULT_PARENT_GROUP_ID);

Group liveGroup = null;

long liveGroupId = 0;

Group stagingGroup = null;

long stagingGroupId = 0;

UnicodeProperties liveGroupTypeSettings = null;

if (group != null) {
	if (group.isStagingGroup()) {
		liveGroup = group.getLiveGroup();

		stagingGroup = group;
	}
	else {
		liveGroup = group;

		if (group.hasStagingGroup()) {
			stagingGroup = group.getStagingGroup();
		}
	}

	liveGroupId = liveGroup.getGroupId();

	if (stagingGroup != null) {
		stagingGroupId = stagingGroup.getGroupId();
	}

	liveGroupTypeSettings = liveGroup.getTypeSettingsProperties();
}
else {
	liveGroupTypeSettings = new UnicodeProperties();
}

LayoutSetPrototype layoutSetPrototype = null;

long layoutSetPrototypeId = ParamUtil.getLong(request, "layoutSetPrototypeId");

if (layoutSetPrototypeId > 0) {
	layoutSetPrototype = LayoutSetPrototypeServiceUtil.getLayoutSetPrototype(layoutSetPrototypeId);
}

portletDisplay.setURLBack(backURL.toString());

%>


<liferay-portlet:actionURL name="create_pr" var="editGroupURL">
</liferay-portlet:actionURL>


<aui:form action="<%= editGroupURL %>" cssClass="container-fluid-1280" method="post" name="fm" >
	<aui:input name="redirect" type="hidden" value="<%= currentURL %>" />
	<aui:input name="backURL" type="hidden" value="<%= backURL %>" />
	<aui:input name="groupId" type="hidden" value="<%= groupId %>" />
	<aui:input name="liveGroupId" type="hidden" value="<%= liveGroupId %>" />
	<aui:input name="stagingGroupId" type="hidden" value="<%= stagingGroupId %>" />

	<%
	request.setAttribute("site.group", group);
	request.setAttribute("site.layoutSetPrototype", layoutSetPrototype);
	request.setAttribute("site.liveGroup", liveGroup);
	request.setAttribute("site.liveGroupId", Long.valueOf(liveGroupId));
	request.setAttribute("site.liveGroupTypeSettings", liveGroupTypeSettings);
	request.setAttribute("site.stagingGroup", stagingGroup);
	request.setAttribute("site.stagingGroupId", Long.valueOf(stagingGroupId));
	%>
		
	<%--liferay-ui:form-navigator
		backURL="<%= backURL %>"
		formModelBean="<%= group %>"
		id="FORM_NAVIGATOR_ID_SESSIONS"
		markupView="lexicon"
		showButtons="<%= false %>"
	/>
	
	<aui:fieldset-group markupView="lexicon">
		<aui:fieldset>
			<liferay-ui:custom-attribute
					className="<%= Group.class.getName() %>"
					classPK="<%=(liveGroup != null) ? liveGroup.getGroupId() : 0  %>"
					editable="<%= true %>"
					label="<%= true %>"
					name="session_order"
				/>
		</aui:fieldset>
	</aui:fieldset-group--%>
	
	<%@ include file="details.jsp" %>  
		
	<aui:button type="submit" />
    <aui:button href="<%= backURL %>" type="cancel" />
</aui:form>


	<%--  
		String html = ProjectsDisplayContext.fetchHTMLMainJournalArticle(themeDisplay);
	--%>
	
	<%-- = html --%>
	
	
<aui:script>
	function <portlet:namespace />saveGroup(forceDisable) {
		var $ = AUI.$;

		var form = $(document.<portlet:namespace />fm);

		<c:if test="<%= (group != null) && !group.isCompany() %>">
			<%--portlet:namespace />saveLocales();--%>
		</c:if>

		submitForm(form);
	}
	
</aui:script>

