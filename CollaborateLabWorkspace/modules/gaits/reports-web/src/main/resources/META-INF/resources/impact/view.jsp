<%@ include file="/init.jsp" %>

<%
	Long groupId = themeDisplay.getLayout().getGroupId();
%>

<div class="container-fluid-1280">
	<div class="row">
		 <h1>Clinical</h1>
		 <div class="col-md-5">
		 	<%@include file="patients.jsp"%> 
		 </div>
		  <div class="col-md-5 pull-right">
		 	<%@include file="sites.jsp"%>
		 </div>
		 <div class="clearfix"></div>
		 <h1>Knowledge</h1>
		 <div class="row">
			 <div class="col-md-5">
			 	<%@include file="publication.jsp"%>
			 </div>
			 <div class="col-md-5 pull-right">
			 	<%@include file="carrer.jsp"%>
			 </div>
		 </div>
		 <div class="row">	 
			 <div class="col-md-12">
			 	<%@include file="recognition.jsp"%>
			 </div>
		 </div>
		 <div class="row">		 
			 <div class="col-md-12">
			 	<%@include file="collaborators.jsp"%>
			 </div>
		 </div>

		 <h1>Commercial</h1>		 		 
		 <div class="col-md-5">
			<%@include file="fte.jsp"%>
		 </div>
		 <div class="col-md-5 pull-right">
			 <%@include file="patents.jsp"%>
  		 </div>
		 <div class="clearfix"></div>
		 <div class="col-md-12">
		 	<%@include file="funding.jsp"%>
		 </div>	 
	</div>
</div>



<div class="tooltip_templates" style="display: none;">
    <div id="tooltip_patient">
        <!-- span>
		An estimate of the total cumulative number patients served by/with the solution at an "As of Date"". <br>Please select the range of patients impacted and primary populations from the pull-down options that best fits.</span--> 
		<span>Patients impacted:</span>
		<br>1 = &lt;10		
		<br>2 = 10 to 50
		<br>3 = 50 to 100
		<br>4 = 100 to 1K
		<br>5 = 1K to 10K 
		<br>6 = 10K to 100K 
		<br>7 = 100K to 200K
		<br>8 = 200K to 1M 
		<br>9 = 1M to 10M 
		<br>10 = &gt;10M
    </div>
    <div id="tooltip_career">
    	<!-- span>
        A self-rating at a given date as to the extent to which the project has had an impact on the careers of one or more team members.<br>
        Use the 0-9 rating from the pull-down options to reflect the progress at the time and add comments if you wish to help the team recognize the progress of its members.<br> 
    	</span-->        
    	<span>
		Career Impact:
		</span>
		<br>1 = No impact
		<br>2 = Some acknowledged impact on one person - enhanced career
		<br>3 = Moderate acknowledged impact on one person - accelerated career
		<br>4 = Substantial acknowledged impact on one person - changed career path
		<br>5 = Some acknowledged impact on several (&lt;=3) people - enhanced career 
		<br>6 = Moderate acknowledged impact on several (&lt;= 3) people - accelerated career 
		<br>7 = Substantial acknowledged impact on several (&lt;=3) - changed career path
		<br>8 = Some acknowledged impact on many (>=3) people - enhanced career
		<br>9 = Moderate acknowledged impact on many (>=3) people - accelerated career 
		<br>10 = Substantial acknowledged impact on many (>=3) people - changed career path
    </div>    
    <div id="tooltip_sites">
    	<span>
		Sites Using Solution:
		</span>
		<br>1 = 1
		<br>2 = 2
		<br>3 = 3 to 5
		<br>4 = 5 to 10 
		<br>5 = 10 to 20 
		<br>6 = 20 to 50
		<br>7 = 50 to 100 
		<br>8 = 100 to 200 
		<br>9 = 200 to 500
		<br>10 = &gt;500
    </div>     
    <div id="tooltip_fte">
    	<span>
		FTEs created rating:
		</span>
		<br>1 = 1
		<br>2 = 2 to 5
		<br>3 = 5 to 10 
		<br>4 = 10 to 20 
		<br>5 = 20 to 50
		<br>6 = 50 to 100 
		<br>7 = 100 to 200 
		<br>8 = 200 to 500
		<br>9 = 500 to 1,000		
		<br>10 = &gt;1,000
    </div>    
    <div id="tooltip_recog">
    	<span>
		Recognition:
		</span>
		<br>1 = Institutional award and/or recognition
		<br>2 = Regional award and/or recognition
		<br>3 = National award and/or recognition
		<br>4 = International award and/or recognition
		<br>5 = Highest award and/or recognition in field
    </div>       
</div>
<aui:script>
	<% int hintWidth = 400; %>
	$(document).ready(function() {
		AUI().use('aui-base',function(A) {
			var recog= "<liferay-ui:message key='recognition-alert'/>";
			var pub= "<liferay-ui:message key='publication-alert'/>";
			var prop= "<liferay-ui:message key='prop-alert'/>";
			var colab= "<liferay-ui:message key='colaboration-alert'/>";
			var sup= "<liferay-ui:message key='support-alert'/>";
			$('#job').tooltipster({
    	    	content: $('#tooltip_fte'),
    	 		theme: 'tooltipster-borderless',
    	    	interactive: true,
    	    	maxWidth: <%=hintWidth %>
    		});
    		
    		$('#patient').tooltipster({
    	    	content: $('#tooltip_patient'),
    	 		theme: 'tooltipster-borderless',
    	 		maxWidth: <%=hintWidth %>,
    	    	interactive: true
    		});
    		$('#recog').tooltipster({
    	    	content:  $('#tooltip_recog'),
    	 		theme: 'tooltipster-borderless',
    	 		maxWidth: <%=hintWidth %>,
    	    	interactive: true
    		});
    		
    		$('#support').tooltipster({
    	    	content: sup,
    	 		theme: 'tooltipster-borderless',
    	 		maxWidth: <%=hintWidth %>,
    	    	interactive: true
    		});
    		$('#colab').tooltipster({
    	    	content: colab,
    	 		theme: 'tooltipster-borderless',
    	 		maxWidth: <%=hintWidth %>,
    	    	interactive: true
    		});
    		
    		$('#prop').tooltipster({
    	    	content: prop,
    	 		theme: 'tooltipster-borderless',
    	 		maxWidth: <%=hintWidth %>,
    	    	interactive: true
    		});
    		$('#career').tooltipster({
    	    	content: $('#tooltip_career'),
    	 		theme: 'tooltipster-borderless',
    	 		maxWidth: <%=hintWidth %>,
    	    	interactive: true
    		});
    		$('#sites').tooltipster({
    	    	content: $('#tooltip_sites'),
    	 		theme: 'tooltipster-borderless',
    	 		maxWidth: <%=hintWidth %>,
    	    	interactive: true
    		});
    		$('#pub').tooltipster({
    	    	content: pub,
    	 		theme: 'tooltipster-borderless',
    	 		maxWidth: <%=hintWidth %>,
    	    	interactive: true
    		});
		});
		
    });
	</aui:script>