<%
	List<Collaborators> collaborators = CollaboratorsLocalServiceUtil.findByGroupId(groupId);
	Map<Date, Integer[]> dataMapCol = new TreeMap<Date, Integer[]>();
	for(Collaborators p : collaborators) {
		Calendar dateCal = Calendar.getInstance();
		dateCal.setTime(p.getStartDate());
		String date = dateCal.get(Calendar.DAY_OF_MONTH) + "/" + (dateCal.get(Calendar.MONTH)+1) + "/" +dateCal.get(Calendar.YEAR);
		Date c = sdf.parse(date);
		int l = 0;
		List<Collaborators> tempList = new ArrayList<Collaborators>();
		for(Collaborators p1 : collaborators) {
			Calendar dateCal2 = Calendar.getInstance();
			dateCal2.setTime(p1.getStartDate());
			String tdate = dateCal2.get(Calendar.DAY_OF_MONTH) + "/" + (dateCal2.get(Calendar.MONTH) + 1) + "/" +dateCal2.get(Calendar.YEAR);
			Date c1= sdf.parse(tdate);
			if(c.equals(c1))
				tempList.add(p1);
		}
		dataMapCol.put(c,ImpactUtils.putIntegerArray(tempList, date));
	}
	
	for(int r=0; r<12; r++) {  
	    int t = 0;
	    for (Date date: dataMapCol.keySet()){
	      Integer temp[] = new Integer[12];
	      temp = (Integer[])dataMapCol.get(date);
	      temp[r] += t;
	      dataMapCol.put(date, temp);
	      t = temp[r];
	    }
	  }
	  
	  for (Date date: dataMapCol.keySet()){
	    int t = 0;
	    for(int r=0; r<12; r++) {  
	      Integer temp[] = new Integer[12];
	      temp = (Integer[])dataMapCol.get(date);
	      temp[r] += t;
	      dataMapCol.put(date, temp);
	      t = temp[r];
	    }
	    
	  }	

	List<List<String[]>> tableDates = new ArrayList<List<String[]>>();
	
	for (Date date: dataMapCol.keySet()){
		List<String[]> temp = new ArrayList<String[]>();
		for(Integer t : dataMapCol.get(date)) {
			String datesData[] = new String[2];
			Calendar dateCal = Calendar.getInstance();
			dateCal.setTime(date);
			String datestr = dateCal.get(Calendar.DAY_OF_MONTH) + "/" + (dateCal.get(Calendar.MONTH)+1) + "/" +dateCal.get(Calendar.YEAR);
			
			datesData[0] =  datestr;
			datesData[1] = String.valueOf(t);	
			temp.add(datesData);
		}
		tableDates.add(temp);
	}
	StringBuilder datesCol = new StringBuilder();
	StringBuilder dataCol[] = new StringBuilder[12];
	
	int iCol = 0;
	for (Date date: dataMapCol.keySet()){
		String datestr = ImpactUtils.getDateByLocale(date,locale2);
		
		datesCol.append(datestr);
		if(iCol != (dataMapCol.keySet().size()-1)) {
			datesCol.append(",");
		}
		iCol++;
	}

	int c=0;
	
	for(int m=0; m<12; m++) {
		StringBuilder d = new StringBuilder();
		int p = 0;
		for(List<String[]> t : tableDates) {
			d.append(t.get(m)[1]);
			if( p == (tableDates.size()-1)) {
				d.append(";");
			}else {
				d.append(",");
			}
			p++;
		}
		dataCol[c] = d; 
		c++;
	}
	
	StringBuilder dat = new StringBuilder();
	
	int k = 0;
	for(StringBuilder u : dataCol)
	{
		dat.append(u.toString());
	}
	
%>

<div class="col-md-12">
	<div class="row">
		 <h2>Collaborators <span  id="colab" >
					<i class="icon-info-sign"></i>
				</span></h2>
		 <table class="table table-condensed">
		    <thead>
		      <tr>
		        <th>Date</th>
		        <% for(ImpactWebPortletKeys.Type t : ImpactWebPortletKeys.Type.values()) { %>
		        	 <th> <%= t.getName() %></th>
		        <% } %>
		      </tr>
		    </thead>
		    <tbody>
		    <% for (Date date: dataMapCol.keySet()){ 
				String datestr = ImpactUtils.getDateByLocale(date,locale2);
		    %>
		    <tr>
				<td><%= datestr %></td>
				<% for(Integer t : dataMapCol.get(date)) { %>
			  		<td><%= t.toString() %></td>
				<% 	} %>
			</tr>
			<% 
		    }
			%>
		    </tbody>
		  </table>
	</div>
	<div class="row">
		<canvas id="chartCol" width="600" height="300"></canvas>
	</div>
</div>

<aui:script>
var canvas = document.getElementById("chartCol");
var labelstr = "<%= datesCol.toString() %>";
var labels = labelstr.split(",");

var datastr = "<%= dat %>";
var data = datastr.split(";");
var arrdata = new Array(data.length);
var timeFormat = 'MM/DD/YYYY';
for(var i=0; i < data.length; i++) {
	
	var p = data[i].split(",");
	var p2 = new Array(p.length+2);
	for(var o = 0; o < p2.length; o++) {
		if(o==0) {
			
		}
		else if(o == p2.length-1) {
			
		}else {
			p2[o] = p[o-1];
		}
	}
	arrdata[i] = p2;
}


var newLabels = new Array(labels.length+2);

for(var j = 0; j<labels.length+2; j++)
{
	if(j==0) {
		var temp = labels[j].split("/");

		if(temp.length == 3)
		{
			var t = parseInt(temp[0],10) - 1;
			var t2 = parseInt(temp[2],10)+2000;
			if(t == 0) {
				t = 12;
				t2 = (t2+1);
			}
			newLabels[j] = t+"/"+temp[1]+"/"+t2;
		}
	}else if(j == labels.length+1)
	{
		var temp = labels[j-2].split("/");
		if(temp.length == 3)
		{
			var t = parseInt(temp[0],10) + 1;
			var t2 = parseInt(temp[2],10)+2000;
			if(t == 13) {
				t = 1;
				t2 = (t2+1);
			}
			newLabels[j] = t+"/"+temp[1]+"/"+t2;
		}
		
	}else {
		var temp = labels[j-1].split("/");
		if(temp.length == 3)
		{
			var t = parseInt(temp[0],10);
			var t2 = parseInt(temp[2],10)+2000;
			newLabels[j] = t+"/"+temp[1]+"/"+t2;
		}
	}
}

if(labels.length > 0) {
var unitsStep = 1;
var last = new Date(newLabels[newLabels.length-1]);
var first = new Date(newLabels[0]); 
var date = Math.ceil(Math.abs(last.getTime() - first.getTime()) / (1000 * 3600 * 24));
var num = Math.floor((date/30));
unitsStep = Math.floor(date / 3);

var canvasData = {
  labels: newLabels,
  datasets: [{

	label: "NIH Lab",
    data: arrdata[0],
    lineTension: 0,
    fill: false,
    borderColor: 'pink',
    backgroundColor: 'pink',
    pointRadius: 5
  }, 
   {
	    label: "DoD Lab",
	    data: arrdata[1],
	    lineTension: 0,
	    fill: false,
	    borderColor: 'red',
	    backgroundColor: 'red',
	    pointRadius: 5
  },
   
   {
	    label: "Other Fed/Country Lab",
	    data: arrdata[2],
	    lineTension: 0,
	    fill: false,
	    borderColor: 'rgb(72,117,198)',
	    backgroundColor: 'rgb(72,117,198)',
	    pointRadius: 5
  },
  
  {
	    label: "Independent Lab",
	    data: arrdata[3],
	    lineTension: 0,
	    fill: false,
	    borderColor: 'yellow',
	    backgroundColor: 'yellow',
	    pointRadius: 5
},
  {
	    label: "Academic Medical Center",
	    data: arrdata[4],
	    lineTension: 0,
	    fill: false,
	    borderColor: 'grey',
	    backgroundColor: 'grey',
	    pointRadius: 5
},
{
    label: "Hospital/Care Provider",
    data: arrdata[5],
    lineTension: 0,
    fill: false,
    borderColor: 'black',
    backgroundColor: 'black',
    pointRadius: 5
},
{
    label: "University",
    data: arrdata[6],
    lineTension: 0,
    fill: false,
    borderColor: 'green',
    backgroundColor: 'green',
    pointRadius: 5
},
{
    label: "Clinical Research Organization",
    data: arrdata[7],
    lineTension: 0,
    fill: false,
    borderColor: 'orange',
    backgroundColor: 'orange',
    pointRadius: 5
},
{
    label: "Service Provider(consultants, etc.)",
    data: arrdata[8],
    lineTension: 0,
    fill: false,
    borderColor: 'BlueViolet',
    backgroundColor: 'BlueViolet',
    pointRadius: 5
},
{
    label: "Start-Up",
    data: arrdata[9],
    lineTension: 0,
    fill: false,
    borderColor: 'lime',
    backgroundColor: 'lime',
    pointRadius: 5
},
{
    label: "Industry",
    data: arrdata[10],
    lineTension: 0,
    fill: false,
    borderColor: 'aqua',
    backgroundColor: 'aqua',
    pointRadius: 5
},
{
    label: "Other",
    data: arrdata[11],
    lineTension: 0,
    fill: false,
    borderColor: 'purple',
    backgroundColor: 'purple',
    pointRadius: 5
},
  ]

};


var chartOptions = {
  legend: {
    display: true,
    position: 'top',
    labels: {
      boxWidth: 30,
      fontColor: 'black'
    }
  },
  scales: {
	  xAxes: [{
	  		type: 'time',
	  		display: true,
			scaleLabel: {
				display: true,
				labelString: 'Date'
			},
			time: {
				unit:'day',
				format: timeFormat,
				round: 'day',
				unitStepSize:unitsStep,
				tooltipFormat: 'MM/DD/YYYY',
				displayFormats: {
						 day: 'MM/DD/YYYY'
					}
				 },	
				
	 	}],
		yAxes: [{
			type: 'linear',
			beginAtZero: true,
			ticks: {
				min: 0,
				stepSize: 1
			}
		}]				
	}	
};

var lineChart = new Chart(canvas, {
  type: 'line',
  data: canvasData,
  options: chartOptions
});

}
</aui:script>