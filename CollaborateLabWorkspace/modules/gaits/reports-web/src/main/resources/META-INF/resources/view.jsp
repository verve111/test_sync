<%@ include file="/init.jsp" %>

<style>
	.gantt_container {
		display: block;
		width: 600px;
		height: 600px;
	}
</style>

<% 
	String gantt1data = (String)request.getAttribute("gantt1Data");
	String gantt2data = (String)request.getAttribute("gantt2Data");
%>

<div class="container-fluid-1280">
	<div class="row">
		<div class="col-md-12" >
			<canvas id="ganttChart1" height="400"></canvas>
		</div>
		<!-- div class="col-md-6">
			<canvas id="ganttChart2" height="300"></canvas>
		</div-->	
	</div>
</div>


<script>
	function renderChart() {
		var ctx = document.getElementById("ganttChart1");

		var horizontalLine = function(value) {
			var o = {
				type : 'line',
				mode : 'horizontal',
				scaleID : 'y-axis-0',
				value : value,
				borderColor : 'gainsboro',
				borderWidth : 3
			};
			return o;
		}

		var getGreyLines = function() {
			var arr = [];
			for (var i = 0; i < 10; i++) {
				arr.push(horizontalLine(i * 5));
			}
			return arr;
		}

		var scatterChart = new Chart(
				ctx,
				{
					type : 'line',
					data : {
						datasets: <%=gantt1data%>
					},
					options : {
						legend : {
							position : 'bottom'
						},
				        tooltips: {
				            callbacks: {
				                label: function(tooltipItem, data) {
				                    var label = data.datasets[tooltipItem.datasetIndex].label || '';

				                    //if (label) {
				                    //    label += ' ';
				                    //}
				                    //label += Math.round(tooltipItem.yLabel * 100) / 100;
				                    return label;
				                }
				            }
				        },
						scales : {
							xAxes : [ {
								type : 'time',
								position : 'top',
								time : {
									unit : 'year',
									tooltipFormat: 'MM/DD/YYYY'
								}
							} ],
							yAxes : [ {
								type : 'linear',
								beginAtZero : true,
								ticks : {
									callback : function(label, index, labels) {
										if (index == 0)
											return '';
										var indexOffset = index - 2;
										if (indexOffset >= 0
												&& (indexOffset % 5 == 0)) {
											var j = indexOffset / 5;
											j++;
											if (j == 1)
												return '1) Clinical';
											if (j == 2)
												return '2) Idea';
											if (j == 3)
												return '3) Proof of Concept (PoC)';
											if (j == 4)
												return '4) Proof of Feasibility (PoF)';
											if (j == 5)
												return '5) Proof of Value (PoV)';
											if (j == 6)
												return '6) Initial Clinical Trails (ICT)';
											if (j == 7)
												return '7) Validation of Solution (VoS)';
											if (j == 8)
												return '8) Approval & Launch (A&L)';
											if (j == 9)
												return '9) Clinical Use (Use)';
											if (j == 10)
												return '10) Standard of Care (SoC)';
										}
										return "";
									},
									max : 50,
									min : 0,
									reverse : true,
									stepSize : 1
								}
							} ]
						},
						annotation : {
							drawTime : 'afterDatasetsDraw',
							annotations : getGreyLines()
						},
						maintainAspectRatio: false
					}
				});

	}
	
	function renderChart2() {
		var ctx = document.getElementById("ganttChart2");
		
		new Chart(ctx, {
			type: 'line',
			data: {
				datasets: <%=gantt2data%>	
			},
			options: {
				legend: {
					position: 'bottom'
				},
				scales: {
					xAxes: [{
						type: 'time',
						time: {
							unit: 'year'
						}					
					}],
					yAxes: [{
						type: 'linear',
						beginAtZero: true,
						ticks: {
							max: 10,
							min: 0,
							stepSize: 1
						}
					}]				
				}	
			}
		});
	}

	renderChart();
	//renderChart2();
</script>




