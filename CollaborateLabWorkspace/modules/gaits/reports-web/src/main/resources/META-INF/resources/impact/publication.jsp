<%
	List<Publications> publications = PublicationsLocalServiceUtil.findByGroupId(groupId);
	List unmodifiableList4 = Collections.unmodifiableList(publications);
	List newList4 = new ArrayList(unmodifiableList4);
	
	Comparator<Publications> comparator4 = new Comparator<Publications>() {
		public int compare(Publications c11, Publications c22){
				return c11.getDate().compareTo(c22.getDate());
		}
	};
	
	Collections.sort(newList4, comparator4);
	publications = newList4;

	Map<Date, Integer> dataMap = new TreeMap<Date, Integer>();
	Calendar dateCalPub = Calendar.getInstance();
	// add dummy 0 value (the first date - 1 year) as a first point of a chart so it would always start from 0 on Y-axis
	/*if (publications.size() > 0) {
		dateCalPub.setTime(publications.get(0).getDate());
		String date = dateCalPub.get(Calendar.DAY_OF_MONTH) + "/" + (dateCalPub.get(Calendar.MONTH)+1) + "/" +(dateCalPub.get(Calendar.YEAR)-1);
		Date c3 = sdf.parse(date);	
		dataMap.put(c3, 0);
	}*/
	int lInner = 0;
	int pubUpCounter = 0;
	for(Publications p : publications) {
		dateCalPub.setTime(p.getDate());
		String date = dateCalPub.get(Calendar.DAY_OF_MONTH) + "/" + (dateCalPub.get(Calendar.MONTH)+1) + "/" +dateCalPub.get(Calendar.YEAR);
		Date c3 = sdf.parse(date);
		for(Publications p1 : publications.subList(pubUpCounter++, publications.size())) {
			Calendar dateCal2 = Calendar.getInstance();
			dateCal2.setTime(p1.getDate());
			String tdate = dateCal2.get(Calendar.DAY_OF_MONTH) + "/" + (dateCal2.get(Calendar.MONTH)+1) + "/" +dateCal2.get(Calendar.YEAR);
			Date c4 = sdf.parse(tdate);
			if(c3.equals(c4) && dataMap.get(c3) == null)
				lInner++;
		}
		dataMap.put(c3,new Integer(lInner));
	}
	
	int iP = 0;
	StringBuilder datesPub = new StringBuilder();
	StringBuilder dataPub = new StringBuilder();
	for (Date date: dataMap.keySet()){
		String ndate = ImpactUtils.getDateByLocale(date,locale2);
		datesPub.append(ndate);
		dataPub.append(dataMap.get(date).toString());
		if(iP != (dataMap.keySet().size()-1)) {
			datesPub.append(",");
			dataPub.append(",");
		}
		iP++;
	} 
%>

<div class="col-md-12">
	<div class="row">
		<h2>
			Total Publications <span id="pub"> <i class="icon-info-sign"></i>
			</span>
		</h2>
		<table class="table table-condensed">
		    <thead>
		      <tr>
		        <th>Date</th>
		        <th>Total Publications</th>
		      </tr>
		    </thead>
		    <tbody>
		    <% for (Date date: dataMap.keySet()){ 
		    	
				String ndate = ImpactUtils.getDateByLocale(date,locale2);		
		    %>
			      <tr>
			        <td><%= ndate %></td>
			        <td><%= dataMap.get(date).toString() %></td>
			      </tr>
		      <% } %>
		    </tbody>
		  </table>
	</div>
	<div class="row">
		<canvas id="chartPub" width="600" height="300"></canvas>
	</div>
</div>

<aui:script>
var canvas = document.getElementById("chartPub");
var labelstr = "<%= datesPub.toString() %>";
var labels = labelstr.split(",");
var datastr = "<%= dataPub.toString() %>";
var data = datastr.split(",");
var newData = new Array(data.length+2);
var timeFormat = 'MM/DD/YYYY';
for(var i=0; i < newData.length; i++) {
		if(i==0) {
			
		}
		else if(i == newData.length-1) {
			
		}else {
			newData[i] = data[i-1];
		}
}

var newLabels = new Array(labels.length+2);

for(var j = 0; j<labels.length+2; j++)
{
	if(j==0) {
		var temp = labels[j].split("/");

		if(temp.length == 3)
		{
			var t = parseInt(temp[0],10) - 1;
			var t2 = parseInt(temp[2],10)+2000;
			if(t == 0) {
				t = 12;
				t2 = (t2+1);
			}
			newLabels[j] = t+"/"+temp[1]+"/"+t2;
		}
	}else if(j == labels.length+1)
	{
		var temp = labels[j-2].split("/");
		if(temp.length == 3)
		{
			var t = parseInt(temp[0],10) + 1;
			var t2 = parseInt(temp[2],10)+2000;
			if(t == 13) {
				t = 1;
				t2 = (t2+1);
			}
			newLabels[j] = t+"/"+temp[1]+"/"+t2;
		}
		
	}else {
		var temp = labels[j-1].split("/");
		if(temp.length == 3)
		{
			var t = parseInt(temp[0],10);
			var t2 = parseInt(temp[2],10)+2000;
			newLabels[j] = t+"/"+temp[1]+"/"+t2;
		}
	}
}

var unitsStep = 1;
if(labels.length > 0) {
var last = new Date(newLabels[newLabels.length-1]);

var first = new Date(newLabels[0]); 
var date = Math.ceil(Math.abs(last.getTime() - first.getTime()) / (1000 * 3600 * 24));
var num = Math.floor((date/30));
unitsStep = Math.floor(date / 3);

var canvasData = {
  labels: newLabels,
  datasets: [{
    label: "Total Publications",
    data: newData,
    lineTension: 0,
    fill: false,
    borderColor: 'rgb(72,117,198)',
    backgroundColor: 'rgb(72,117,198)',
    pointRadius: 5
  }]
};

var chartOptions = {
  legend: {
    display: true,
    position: 'top',
    labels: {
      boxWidth: 30,
      fontColor: 'black'
    }
  },
  scales: {
	  xAxes: [{
	  		type: 'time',
	  		display: true,
			scaleLabel: {
				display: true,
				labelString: 'Date'
			},
			time: {
				unit:'day',
				format: timeFormat,
				round: 'day',
				unitStepSize:unitsStep,
				tooltipFormat: 'MM/DD/YYYY',
				displayFormats: {
						 day: 'MM/DD/YYYY'
					}
				 },	
				
	 	}],
		yAxes: [{
			type: 'linear',
			beginAtZero: true,
			ticks: {
				min: 0,
				stepSize: 1
			}
		}]				
	}	
};

var lineChart = new Chart(canvas, {
  type: 'line',
  data: canvasData,
  options: chartOptions
});

}
</aui:script> 