<%
	List<TeamMemberProjectRecognition> recognition = TeamMemberProjectRecognitionLocalServiceUtil.findByGroupId(groupId);
	Map<Date, Integer[]> dataMapR = new TreeMap<Date, Integer[]>();
	for(TeamMemberProjectRecognition p : recognition) {
		Calendar dateCal = Calendar.getInstance();
		dateCal.setTime(p.getDateOfRecognition());
		String date = dateCal.get(Calendar.DAY_OF_MONTH) + "/" + (dateCal.get(Calendar.MONTH)+1) + "/" +dateCal.get(Calendar.YEAR);
		Date c2 = sdf.parse(date);
		int l = 0;
		List<TeamMemberProjectRecognition> tempList = new ArrayList<TeamMemberProjectRecognition>();
		for(TeamMemberProjectRecognition p1 : recognition) {
			Calendar dateCal2 = Calendar.getInstance();
			dateCal2.setTime(p1.getDateOfRecognition());
			String tdate = dateCal2.get(Calendar.DAY_OF_MONTH) + "/" + (dateCal2.get(Calendar.MONTH)+1) + "/" +dateCal2.get(Calendar.YEAR);
			Date c3 = sdf.parse(tdate);
			if(c3.equals(c2))
				tempList.add(p1);
		}
		dataMapR.put(c2,ImpactUtils.putIntegerArrayRecognition(tempList, date.toString()));
	}
	
	for(int r=0; r<5; r++) {  
	    int t = 0;
	    for (Date date: dataMapR.keySet()){
	      Integer temp[] = new Integer[5];
	      temp = (Integer[])dataMapR.get(date);
	      temp[r] += t;
	      dataMapR.put(date, temp);
	      t = temp[r];
	    }
	  }
	  
	  for (Date date: dataMapR.keySet()){
	    int t = 0;
	    for(int r=0; r<5; r++) {  
	      Integer temp[] = new Integer[5];
	      temp = (Integer[])dataMapR.get(date);
	      temp[r] += t;
	      dataMapR.put(date, temp);
	      t = temp[r];
	    }
	    
	  }	
	
	List<List<String[]>> tableDatesR = new ArrayList<List<String[]>>();
	
	for (Date date: dataMapR.keySet()){
		List<String[]> temp = new ArrayList<String[]>();
		for(Integer t : dataMapR.get(date)) {
			String datesData[] = new String[2];
			Calendar dateCal = Calendar.getInstance();
			dateCal.setTime(date);
			String datestr = dateCal.get(Calendar.DAY_OF_MONTH) + "/" + (dateCal.get(Calendar.MONTH)+1) + "/" +dateCal.get(Calendar.YEAR);
			datesData[0] = datestr;
			datesData[1] = String.valueOf(t);	
			temp.add(datesData);
		}
		tableDatesR.add(temp);
	}
	
	StringBuilder datesR = new StringBuilder();
	StringBuilder dataR[] = new StringBuilder[5];
	
	int iR = 0;
	for (Date date: dataMapR.keySet()){

		String datestr = ImpactUtils.getDateByLocale(date,locale2);
		datesR.append(datestr);
		
		if(iR != (dataMapR.keySet().size()-1)) {
			datesR.append(",");
		}
		iR++;
	}

	int cR=0;
	
	for(int m=0; m<5; m++) {
		StringBuilder d = new StringBuilder();
		int p = 0;
		for(List<String[]> t : tableDatesR) {
			d.append(t.get(m)[1]);
			if( p == (tableDatesR.size()-1)) {
				d.append(";");
			}else {
				d.append(",");
			}
			p++;
		}
		dataR[cR] = d; 
		cR++;
	}
	
	StringBuilder datR = new StringBuilder();

	for(StringBuilder u : dataR)
	{
		datR.append(u.toString());
	}
	
%>

<div class="col-md-12">
	<div class="row">
		 <h2>Recognition <span  id="recog" >
					<i class="icon-info-sign"></i>
				</span></h2>
		 <table class="table table-condensed">
		    <thead>
		      <tr>
		        <th>Date</th>
		        <% for(ImpactWebPortletKeys.TeamCatRating t : ImpactWebPortletKeys.TeamCatRating.values()) { %>
		        	 <th> <%= t.getName() %></th>
		        <% } %>
		      </tr>
		    </thead>
		    <tbody>
		    <% for (Date date: dataMapR.keySet()){ 
		    	
				String datestr = ImpactUtils.getDateByLocale(date,locale2);
		    %>
		    <tr>
				<td><%= datestr %></td>
				<% for(Integer t : dataMapR.get(date)) { %>
			  		<td><%= t.toString() %></td>
			<% 		} %>
				</tr>
			<% 
		    	}
			%>
		    </tbody>
		  </table>
	</div>
	<div class="row">
		<canvas id="chartRecog" width="600" height="300"></canvas>
	</div>
</div>

<aui:script>
var canvas = document.getElementById("chartRecog");
var labelstr = "<%= datesR.toString() %>";
var labels = labelstr.split(",");

var datastr = "<%= datR %>";
var data = datastr.split(";");
var arrdata = new Array(data.length);
var timeFormat = 'MM/DD/YYYY';
for(var i=0; i < data.length; i++) {
	
	var p = data[i].split(",");
	var p2 = new Array(p.length+2);
	for(var o = 0; o < p2.length; o++) {
		if(o==0) {
			
		}
		else if(o == p2.length-1) {
			
		}else {
			p2[o] = p[o-1];
		}
	}
	arrdata[i] = p2;
}


var newLabels = new Array(labels.length+2);

for(var j = 0; j<labels.length+2; j++)
{
	if(j==0) {
		var temp = labels[j].split("/");

		if(temp.length == 3)
		{
			var t = parseInt(temp[0],10) - 1;
			var t2 = parseInt(temp[2],10)+2000;
			if(t == 0) {
				t = 12;
				t2 = (t2+1);
			}
			newLabels[j] = t+"/"+temp[1]+"/"+t2;
		}
	}else if(j == labels.length+1)
	{
		var temp = labels[j-2].split("/");
		if(temp.length == 3)
		{
			var t = parseInt(temp[0],10) + 1;
			var t2 = parseInt(temp[2],10)+2000;
			if(t == 13) {
				t = 1;
				t2 = (t2+1);
			}
			newLabels[j] = t+"/"+temp[1]+"/"+t2;
		}
		
	}else {
		var temp = labels[j-1].split("/");
		if(temp.length == 3)
		{
			var t = parseInt(temp[0],10);
			var t2 = parseInt(temp[2],10)+2000;
			newLabels[j] = t+"/"+temp[1]+"/"+t2;
		}
	}
}

if(labels.length > 0) {
var unitsStep = 1;
var last = new Date(newLabels[newLabels.length-1]);
var first = new Date(newLabels[0]); 
var date = Math.ceil(Math.abs(last.getTime() - first.getTime()) / (1000 * 3600 * 24));
var num = Math.floor((date/30));
unitsStep = Math.floor(date / 3);

var canvasData = {
  labels: newLabels,
  datasets: [{
    label: "Institutional award and/or recognition",
    data: arrdata[0],
    lineTension: 0,
    fill: false,
    borderColor: 'grey',
    backgroundColor: 'grey',
    pointRadius: 5
  
  }, 
  {
	    label: "Regional award and/or recognition",
	    data: arrdata[1],
	    lineTension: 0,
	    fill: false,
	    borderColor: 'rgb(72,117,198)',
	    backgroundColor: 'rgb(72,117,198)',
	    pointRadius: 5
	  
}, 

{
    label: "National award and/or recognition",
    data: arrdata[2],
    lineTension: 0,
    fill: false,
    borderColor: 'red',
    backgroundColor: 'red',
    pointRadius: 5
  
}, 
{
    label: "International award and/or recognition",
    data: arrdata[3],
    lineTension: 0,
    fill: false,
    borderColor: 'green',
    backgroundColor: 'green',
    pointRadius: 5
  
}, 
{
    label: "Highest award and/or recognition in field",
    data: arrdata[4],
    lineTension: 0,
    fill: false,
    borderColor: 'yellow',
    backgroundColor: 'yellow',
    pointRadius: 5
}
  ]

};


var chartOptions = {
  legend: {
    display: true,
    position: 'top',
    labels: {
      boxWidth: 30,
      fontColor: 'black'
    }
  },
  scales: {
	  xAxes: [{
	  		type: 'time',
	  		display: true,
			scaleLabel: {
				display: true,
				labelString: 'Date'
			},
			time: {
				unit:'day',
				format: timeFormat,
				round: 'day',
				unitStepSize:unitsStep,
				tooltipFormat: 'MM/DD/YYYY',
				displayFormats: {
						 day: 'MM/DD/YYYY'
					}
				 },	
				
	 	}],
		yAxes: [{
			type: 'linear',
			beginAtZero: true,
			ticks: {
				min: 0,
				stepSize: 1
			}
		}]				
	}	
};

var lineChart = new Chart(canvas, {
  type: 'line',
  data: canvasData,
  options: chartOptions
});

}
</aui:script>