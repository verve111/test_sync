<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %><%@
taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %><%@
taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %><%@
taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@page import="com.collaboratelab.impact.model.CareerImpact"%>
<%@page import="com.collaboratelab.impact.model.Collaborators"%>
<%@page import="com.collaboratelab.impact.model.IntellectualProperty"%>
<%@page import="com.collaboratelab.impact.model.PatientsImpacted"%>
<%@page import="com.collaboratelab.impact.model.SitesUsingSolution"%>
<%@page import="com.collaboratelab.impact.model.TeamMemberProjectRecognition"%>
<%@page import="com.collaboratelab.impact.model.Publications"%>
<%@page import="com.collaboratelab.impact.model.Jobs"%>
<%@page import="com.collaboratelab.impact.model.Support"%>

<%@page import="com.collaboratelab.project.service.ProjectPhaseLocalServiceUtil"%>
<%@page import="com.collaboratelab.project.service.ProjectStepLocalServiceUtil"%>
<%@page import="com.collaboratelab.project.service.ProjectStructureLocalServiceUtil"%>
<%@page import="com.collaboratelab.project.service.ProjectLocalServiceUtil"%>
<%@page import="com.collaboratelab.project.service.ProjectDomainLocalServiceUtil"%>
<%@page import="com.collaboratelab.project.service.ProjectDeliveryLocalServiceUtil"%>
<%@page import="com.collaboratelab.project.model.Project"%>
<%@page import="com.collaboratelab.project.model.ProjectDomain"%>
<%@page import="com.collaboratelab.project.model.ProjectPhase"%>
<%@page import="com.collaboratelab.project.model.ProjectStep"%>
<%@page import="com.collaboratelab.project.model.ProjectStructure"%>
<%@page import="com.collaboratelab.project.model.ProjectDelivery"%>

<%@page import="com.collaboratelab.impactweb.constants.ImpactWebPortletKeys"%>
<%@page import="com.collaboratelab.project.constants.enums.Enums"%>

<%@page import="com.collaboratelab.impact.service.CareerImpactLocalServiceUtil"%>
<%@page import="com.collaboratelab.impact.service.CollaboratorsLocalServiceUtil"%>
<%@page import="com.collaboratelab.impact.service.IntellectualPropertyLocalServiceUtil"%>
<%@page import="com.collaboratelab.impact.service.PatientsImpactedLocalServiceUtil"%>
<%@page import="com.collaboratelab.impact.service.SitesUsingSolutionLocalServiceUtil"%>
<%@page import="com.collaboratelab.impact.service.TeamMemberProjectRecognitionLocalServiceUtil"%>
<%@page import="com.collaboratelab.impact.service.PublicationsLocalServiceUtil"%>
<%@page import="com.collaboratelab.impact.service.SupportLocalServiceUtil"%>
<%@page import="com.collaboratelab.impact.service.JobsLocalServiceUtil"%>

<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.Comparator"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Collections"%>
<%@page import="com.liferay.portal.kernel.dao.orm.DynamicQuery"%>
<%@page import="com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.dao.orm.Order"%>
<%@page import="com.liferay.portal.kernel.dao.orm.OrderFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.dao.orm.Criterion"%>
<%@page import="com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.util.PortalClassLoaderUtil"%>
<%@page import="com.liferay.portal.kernel.bean.PortletBeanLocatorUtil"%>
<%@page import="java.lang.ClassLoader" %>

<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.Calendar"%>
<%@page import="com.liferay.portal.kernel.util.CalendarFactoryUtil"%>

<%@page import="com.liferay.portal.kernel.bean.PortletBeanLocatorUtil"%>
<%@page import="java.util.HashMap" %>
<%@page import="java.util.Map" %>
<%@page import="com.collaboratelab.gaits.portlet.reports.utils.ImpactUtils" %>

<%@page import="java.util.Locale"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayPortletMode"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.liferay.portal.kernel.util.DateUtil"%>
<%@page import="com.liferay.portal.kernel.util.LocaleUtil"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.collaboratelab.gaits.portlet.reports.utils.ImpactUtils"%>
<%@page import="com.collaboratelab.project.constants.Utils"%> 

<liferay-theme:defineObjects />
<portlet:defineObjects />

<%	

	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	long companyId = themeDisplay.getCompanyId();
	long globalGroupId = Utils.getGlobalGroupId(companyId);
	Locale locale2 = LocaleUtil.getSiteDefault();
%>
