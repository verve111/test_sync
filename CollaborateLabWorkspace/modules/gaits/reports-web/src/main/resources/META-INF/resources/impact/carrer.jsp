<%
	
	List<CareerImpact> carrers = CareerImpactLocalServiceUtil.findByGroupId(groupId);

	List modifiableList = new ArrayList(carrers);
	Comparator<CareerImpact> comparator = new Comparator<CareerImpact>() {
	    public int compare(CareerImpact c11, CareerImpact c22){
	    	return c11.getAsOfDate().compareTo(c22.getAsOfDate());
	 	}
	};
	Collections.sort(modifiableList, comparator);
	carrers = modifiableList;
	
	StringBuilder datesCarrer = new StringBuilder();
	StringBuilder dataCarrer = new StringBuilder();
	
	int iC = 0;
	
	for(CareerImpact p : carrers) {
		datesCarrer.append(ImpactUtils.getDateByLocale(p.getAsOfDate(),locale2));
		dataCarrer.append(p.getCategoryRaing());
		if(iC != (carrers.size()-1)) {
			datesCarrer.append(",");
			dataCarrer.append(",");
		}
		iC++;
	}
%>

<div class="col-md-12">
	<div class="row">
		<h2>
			Career Impact Rating 
			<span id="career"> <i
				class="icon-info-sign"></i>
			</span>
		</h2>

		<table class="table table-condensed">
		    <thead>
		      <tr>
		        <th>Date</th>
		        <th>Rating</th>
		      </tr>
		    </thead>
		    <tbody>
		    
		    <% 
	
		    for(CareerImpact p : carrers) { 
		    	
		    %>    
		    
		      <tr>
			        <td><%= ImpactUtils.getDateByLocale(p.getAsOfDate(),locale2) %></td>
			        <td><%= p.getCategoryRaing() %></td>
			      </tr>
		      <% 	
		    	
		    	}
		    	%>
		    </tbody>
		  </table>
	</div>
	<div class="row">
		<canvas id="chartCarrer" width="600" height="300"></canvas>
	</div>
</div>


<aui:script>
var canvas = document.getElementById("chartCarrer");
var labelstr = "<%= datesCarrer.toString() %>";
var labels = labelstr.split(",");
var datastr = "<%= dataCarrer.toString() %>";
var data = datastr.split(",");
var newData = new Array(data.length+2);
var timeFormat = 'MM/DD/YYYY';
for(var i=0; i < newData.length; i++) {
		if(i==0) {
			
		}
		else if(i == newData.length-1) {
			
		}else {
			newData[i] = data[i-1];
		}
}


var newLabels = new Array(labels.length+2);

for(var j = 0; j<labels.length+2; j++)
{
	if(j==0) {
		var temp = labels[j].split("/");

		if(temp.length == 3)
		{
			var t = parseInt(temp[0],10) - 1;
			var t2 = parseInt(temp[2],10)+2000;
			if(t == 0) {
				t = 12;
				t2 = (t2+1);
			}
			newLabels[j] = t+"/"+temp[1]+"/"+t2;
		}
	}else if(j == labels.length+1)
	{
		var temp = labels[j-2].split("/");
		if(temp.length == 3)
		{
			var t = parseInt(temp[0],10) + 1;
			var t2 = parseInt(temp[2],10)+2000;
			if(t == 13) {
				t = 1;
				t2 = (t2+1);
			}
			newLabels[j] = t+"/"+temp[1]+"/"+t2;
		}
		
	}else {
		var temp = labels[j-1].split("/");
		if(temp.length == 3)
		{
			var t = parseInt(temp[0],10);
			var t2 = parseInt(temp[2],10)+2000;
			newLabels[j] = t+"/"+temp[1]+"/"+t2;
		}
	}
}

if(labels.length > 0) {
var unitsStep = 1;
var last = new Date(newLabels[newLabels.length-1]);
var first = new Date(newLabels[0]); 
var date = Math.ceil(Math.abs(last.getTime() - first.getTime()) / (1000 * 3600 * 24));
var num = Math.floor((date/30));
unitsStep = Math.floor(date / 3);

var canvasData = {
  labels: newLabels,
  datasets: [{
    label: "Career Impact Rating",
    data: newData,
    lineTension: 0,
    fill: false,
    borderColor: 'rgb(72,117,198)',
    backgroundColor: 'rgb(72,117,198)',
    pointRadius: 5
  }]
};

var chartOptions = {
  legend: {
    display: true,
    position: 'top',
    labels: {
      boxWidth: 30,
      fontColor: 'black'
    }
  },
  scales: {
	  xAxes: [{
	  		type: 'time',
	  		display: true,
			scaleLabel: {
				display: true,
				labelString: 'Date'
			},
			time: {
				unit:'day',
				format: timeFormat,
				round: 'day',
				unitStepSize:unitsStep,
				tooltipFormat: 'MM/DD/YYYY',
				displayFormats: {
						 day: 'MM/DD/YYYY'
					}
				 },	
				
	 	}],
	  
		yAxes: [{
			type: 'linear',
			beginAtZero: true,
			ticks: {
				min: 0,
				stepSize: 1
			}
		}]				
	}	
};

var lineChart = new Chart(canvas, {
  type: 'line',
  data: canvasData,
  options: chartOptions
});
}

</aui:script>