
<%
	List<Support> supports = SupportLocalServiceUtil.findByGroupId(groupId);

	Map<Date, Double[]> dataMapSup = new TreeMap<Date, Double[]>();
	Map<Date, Integer[]> dataMapForTable = ImpactUtils.putIntArrayFundingForTable(supports);

	for (Support s : supports) {
		Calendar dateCal = Calendar.getInstance();
		dateCal.setTime(s.getStartDate());
		String date = dateCal.get(Calendar.DAY_OF_MONTH) + "/" + (dateCal.get(Calendar.MONTH) + 1) + "/"
				+ dateCal.get(Calendar.YEAR);
		Date c4 = sdf.parse(date);
		int l = 0;
		List<Support> tempList = new ArrayList<Support>();
		for (Support p1 : supports) {
			Calendar dateCal2 = Calendar.getInstance();
			dateCal2.setTime(p1.getStartDate());
			String tdate = dateCal2.get(Calendar.DAY_OF_MONTH) + "/" + (dateCal2.get(Calendar.MONTH) + 1) + "/"
					+ dateCal2.get(Calendar.YEAR);
			Date c5 = sdf.parse(tdate);
			if (c4.equals(c5))
				tempList.add(p1);
		}
		dataMapSup.put(c4, ImpactUtils.putDoubleArrayFunding(tempList));
	}

	for (int r = 0; r < 11; r++) {
		double t = 0;
		for (Date date : dataMapSup.keySet()) {
			Double temp[] = new Double[11];
			temp = (Double[]) dataMapSup.get(date);
			temp[r] += t;
			dataMapSup.put(date, temp);
			t = temp[r];
		}
	}

	for (Date date : dataMapSup.keySet()) {
		double t = 0;
		for (int r = 0; r < 11; r++) {
			Double temp[] = new Double[11];
			temp = (Double[]) dataMapSup.get(date);
			temp[r] += t;
			dataMapSup.put(date, temp);
			t = temp[r];
		}

	}

	List<List<String[]>> tableDatesSup = new ArrayList<List<String[]>>();

	for (Date date : dataMapSup.keySet()) {
		List<String[]> temp = new ArrayList<String[]>();
		for (Double t : dataMapSup.get(date)) {
			String datesData[] = new String[2];
			Calendar dateCal = Calendar.getInstance();
			dateCal.setTime(date);
			String datestr = dateCal.get(Calendar.DAY_OF_MONTH) + "/" + (dateCal.get(Calendar.MONTH) + 1) + "/"
					+ dateCal.get(Calendar.YEAR);

			datesData[0] = datestr;
			datesData[1] = String.valueOf(t);
			temp.add(datesData);
		}
		tableDatesSup.add(temp);
	}

	StringBuilder datesSup = new StringBuilder();
	StringBuilder dataSup[] = new StringBuilder[11];

	int iSup = 0;
	for (Date date : dataMapSup.keySet()) {
		String datestr = ImpactUtils.getDateByLocale(date, locale2);
		datesSup.append(datestr);
		if (iSup != (dataMapSup.keySet().size() - 1)) {
			datesSup.append(",");
		}
		iSup++;
	}

	int cSup = 0;

	for (int m = 0; m < 11; m++) {
		StringBuilder d = new StringBuilder();
		int p = 0;
		for (List<String[]> t : tableDatesSup) {
			d.append(t.get(m)[1]);
			if (p == (tableDatesSup.size() - 1)) {
				d.append(";");
			} else {
				d.append(",");
			}
			p++;
		}
		dataSup[cSup] = d;
		cSup++;
	}

	StringBuilder datSup = new StringBuilder();

	for (StringBuilder u : dataSup) {
		datSup.append(u.toString());
	}
%>

<div class="col-md-12">
	<div class="row">
		<h2>
			Funding <span id="support"> <i class="icon-info-sign"></i>
			</span>
		</h2>
		<table class="table table-condensed">
			<thead>
				<tr>
					<th>Date</th>
					<%
						for (ImpactWebPortletKeys.SponsorType t : ImpactWebPortletKeys.SponsorType.values()) {
					%>
					<th><%=t.getName()%></th>
					<%
						}
					%>
				</tr>
			</thead>
			<tbody>
				<%
					for (Date date : dataMapForTable.keySet()) {

						String datestr = ImpactUtils.getDateByLocale(date, locale2);
				%>
				<tr>
					<td><%=datestr%></td>
					<%
						for (Integer t : dataMapForTable.get(date)) {
					%>
					<td><%=t.toString()%></td>
					<%
						}
					%>
				</tr>
				<%
					}
				%>
			</tbody>
		</table>
	</div>
	<div class="row">
		<canvas id="chartSup" width="600" height="300"></canvas>
	</div>
</div>

<aui:script>
var canvas = document.getElementById("chartSup");
var labelstr = "<%= datesSup.toString() %>";
var labels = labelstr.split(",");

var datastr = "<%= datSup %>";
var data = datastr.split(";");
var arrdata = new Array(data.length);
var timeFormat = 'MM/DD/YYYY';
for(var i=0; i < data.length; i++) {
	
	var p = data[i].split(",");
	var p2 = new Array(p.length+2);
	for(var o = 0; o < p2.length; o++) {
		if(o==0) {
		}
		else if(o == p2.length-1) {
			
		}else {
			p2[o] = p[o-1];
		}
	}
	arrdata[i] = p2;
}


var newLabels = new Array(labels.length+2);

for(var j = 0; j<labels.length+2; j++)
{
	if(j==0) {
		var temp = labels[j].split("/");

		if(temp.length == 3)
		{
			var t = parseInt(temp[0],10) - 1;
			var t2 = parseInt(temp[2],10)+2000;
			if(t == 0) {
				t = 12;
				t2 = (t2+1);
			}
			newLabels[j] = t+"/"+temp[1]+"/"+t2;
		}
	}else if(j == labels.length+1)
	{
		var temp = labels[j-2].split("/");
		if(temp.length == 3)
		{
			var t = parseInt(temp[0],10) + 1;
			var t2 = parseInt(temp[2],10)+2000;
			if(t == 13) {
				t = 1;
				t2 = (t2+1);
			}
			newLabels[j] = t+"/"+temp[1]+"/"+t2;
		}
		
	}else {
		var temp = labels[j-1].split("/");
		if(temp.length == 3)
		{
			var t = parseInt(temp[0],10);
			var t2 = parseInt(temp[2],10)+2000;
			newLabels[j] = t+"/"+temp[1]+"/"+t2;
		}
	}
}

if(labels.length > 0) {
var unitsStep = 1;
var last = new Date(newLabels[newLabels.length-1]);
var first = new Date(newLabels[0]); 
var date = Math.ceil(Math.abs(last.getTime() - first.getTime()) / (1000 * 3600 * 24));
var num = Math.floor((date/30));
unitsStep = Math.floor(date / 3);


var canvasData = {
  labels: newLabels,
  datasets: [ 
	  {
		    label: "HOME Inst.",
		    data: arrdata[0],
		    lineTension: 0,
		    fill: false,
		    borderColor: 'pink',
		    backgroundColor: 'pink',
		    pointRadius: 5
		  },
  {
	    label: "CIMIT",
	    data: arrdata[1],
	    lineTension: 0,
	    fill: false,
	    borderColor: 'red',
	    backgroundColor: 'red',
	    pointRadius: 5
   },

   
   {
	    label: "NIH",
	    data: arrdata[2],
	    lineTension: 0,
	    fill: false,
	    borderColor: 'blue',
	    backgroundColor: 'blue',
	    pointRadius: 5
  },
  
  {
	    label: "DoD",
	    data: arrdata[3],
	    lineTension: 0,
	    fill: false,
	    borderColor: 'yellow',
	    backgroundColor: 'yellow',
	    pointRadius: 5
},
{
    label: "Other Fed",
    data: arrdata[4],
    lineTension: 0,
    fill: false,
    borderColor: 'grey',
    backgroundColor: 'grey',
    pointRadius: 5
},
{
label: "State",
data: arrdata[5],
lineTension: 0,
fill: false,
borderColor: 'black',
backgroundColor: 'black',
pointRadius: 5
},
{
label: "Foundation",
data: arrdata[6],
lineTension: 0,
fill: false,
borderColor: 'green',
backgroundColor: 'green',
pointRadius: 5
},
{
label: "Philantropy",
data: arrdata[7],
lineTension: 0,
fill: false,
borderColor: 'orange',
backgroundColor: 'orange',
pointRadius: 5
},
{
label: "Crowd",
data: arrdata[8],
lineTension: 0,
fill: false,
borderColor: 'brown',
backgroundColor: 'brown',
pointRadius: 5
},
{
label: "Industry",
data: arrdata[9],
lineTension: 0,
fill: false,
borderColor: 'purple',
backgroundColor: 'purple',
pointRadius: 5
},{
	label: "Equity",
	data: arrdata[10],
	lineTension: 0,
	fill: false,
	borderColor: 'aqua',
	backgroundColor: 'aqua',
	pointRadius: 5
	},
  ]

};

var chartOptions = {
  legend: {
    display: true,
    position: 'top',
    labels: {
      boxWidth: 30,
      fontColor: 'black'
    }
  },
  scales: {
	  xAxes: [{
	  		type: 'time',
	  		display: true,
			scaleLabel: {
				display: true,
				labelString: 'Date'
			},
			time: {
				unit:'day',
				format: timeFormat,
				round: 'day',
				unitStepSize:unitsStep,
				tooltipFormat: 'MM/DD/YYYY',
				displayFormats: {
						 day: 'MM/DD/YYYY'
					}
				 },	
				
	 	}],
		yAxes: [{
			type: 'linear',
			beginAtZero: true,
			ticks: {
				min: 0
			}
		}]				
   }	
};

var lineChart = new Chart(canvas, {
  type: 'line',
  data: canvasData,
  options: chartOptions
});
}

</aui:script>