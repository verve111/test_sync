package com.collaboratelab.gaits.portlet.reports.constants;

public class ReportsPortletKeys {

	public static final String Maturity_Reports = "Maturity Reports";
	public static final String Impact_Reports = "Impact Reports";
	public static final String  Planning_Reports = "Planning Reports";
	
	
}