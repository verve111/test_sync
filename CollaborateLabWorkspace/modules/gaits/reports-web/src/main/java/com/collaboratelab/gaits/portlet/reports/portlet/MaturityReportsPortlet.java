package com.collaboratelab.gaits.portlet.reports.portlet;

import java.io.IOException;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.collaboratelab.gaits.portlet.reports.constants.ReportsPortletKeys;
import com.collaboratelab.project.constants.Utils;
import com.collaboratelab.project.constants.enums.Enums;
import com.collaboratelab.project.model.ProjectDelivery;
import com.collaboratelab.project.model.ProjectStep;
import com.collaboratelab.project.service.ProjectDeliveryLocalServiceUtil;
import com.collaboratelab.project.service.ProjectLocalServiceUtil;
import com.collaboratelab.project.service.ProjectStepLocalServiceUtil;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;

@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.gaits",
		"com.liferay.portlet.instanceable=false",
		"javax.portlet.display-name=Maturity Reports",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + ReportsPortletKeys.Maturity_Reports,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user",
		"com.liferay.portlet.header-portlet-javascript=/js/Chart.bundle.min.js",
		"com.liferay.portlet.header-portlet-javascript=/js/chartjs-plugin-annotation.min.js"
	},
	service = Portlet.class
)
public class MaturityReportsPortlet extends MVCPortlet {
	
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay)renderRequest.getAttribute(
				WebKeys.THEME_DISPLAY);

		long groupId = themeDisplay.getSiteGroupId();
		long projId = ProjectLocalServiceUtil.findByGroupId(groupId).getProjectId();
		
		// generate 1st Maturity Report JSON
		JSONObject jsonObjClinical = getSolidLine(Enums.DOMAIN_ENUM.CLINICAL, true);
		JSONArray dataArrClinical = JSONFactoryUtil.createJSONArray();
		JSONObject jsonObjMarket = getSolidLine(Enums.DOMAIN_ENUM.MARKET, true);
		JSONArray dataArrMarket = JSONFactoryUtil.createJSONArray();	
		JSONObject jsonObjReg = getSolidLine(Enums.DOMAIN_ENUM.REGULATORY, true);
		JSONArray dataArrReg = JSONFactoryUtil.createJSONArray();
		JSONObject jsonObjTech = getSolidLine(Enums.DOMAIN_ENUM.TECHNOLOGY, true);
		JSONArray dataArrTech = JSONFactoryUtil.createJSONArray();
		
		for (ProjectStep step : ProjectStepLocalServiceUtil.findByProjectId(projId)) {
			List<ProjectDelivery> deliveries = ProjectDeliveryLocalServiceUtil.findByStepId(step.getProjectStepId());
			deliveries = Utils.sortDeliverablesByName(Utils.getGlobalGroupId(themeDisplay.getCompanyId()), deliveries);
			for (ProjectDelivery delivery : deliveries) {
				int domainOffset = Enums.getArrayNum(Enums.DOMAIN_ARRAY, delivery.getDomain());
				int phaseOffset = Enums.getArrayNum(Enums.PHASES_ARRAY, delivery.getPhase());
				if (domainOffset == 0 && delivery.getStartDate() != null /*&& delivery.getEndDate() != null*/) {
					addPoint(dataArrClinical, delivery.getStartDate(), phaseOffset, domainOffset);
					addPoint(dataArrClinical,
							delivery.getEndDate() != null ? delivery.getEndDate() : delivery.getStartDate(),
							phaseOffset, domainOffset);
					addInterruptingPoint(dataArrClinical, delivery.getStartDate());
				} else if (domainOffset == 1 && delivery.getStartDate() != null /*&& delivery.getEndDate() != null*/) {
					addPoint(dataArrMarket, delivery.getStartDate(), phaseOffset, domainOffset);
					addPoint(dataArrMarket,
							delivery.getEndDate() != null ? delivery.getEndDate() : delivery.getStartDate(),
							phaseOffset, domainOffset);
					addInterruptingPoint(dataArrMarket, delivery.getStartDate());					
				} else if (domainOffset == 2 && delivery.getStartDate() != null /*&& delivery.getEndDate() != null*/) {
					addPoint(dataArrReg, delivery.getStartDate(), phaseOffset, domainOffset);
					addPoint(dataArrReg,
							delivery.getEndDate() != null ? delivery.getEndDate() : delivery.getStartDate(),
							phaseOffset, domainOffset);
					addInterruptingPoint(dataArrReg, delivery.getStartDate());					
				} else if (domainOffset == 3 && delivery.getStartDate() != null && delivery.getEndDate() != null) {
					addPoint(dataArrTech, delivery.getStartDate(), phaseOffset, domainOffset);
					addPoint(dataArrTech,
							delivery.getEndDate() != null ? delivery.getEndDate() : delivery.getStartDate(),
							phaseOffset, domainOffset);
					addInterruptingPoint(dataArrTech, delivery.getStartDate());					
				}
			}
		}
		jsonObjClinical.put("data", dataArrClinical);
		jsonObjMarket.put("data", dataArrMarket);	
		jsonObjReg.put("data", dataArrReg);
		jsonObjTech.put("data", dataArrTech);
		JSONArray resJSON = JSONFactoryUtil.createJSONArray();
		resJSON.put(jsonObjClinical);
		resJSON.put(jsonObjMarket);
		resJSON.put(jsonObjReg);
		resJSON.put(jsonObjTech);
		renderRequest.setAttribute("gantt1Data", resJSON.toJSONString());
		//renderRequest.setAttribute("gantt2Data", generate2ndMaturityReportJSON(projId, themeDisplay));
		super.render(renderRequest, renderResponse);
	}
	
	private JSONObject getSolidLine(Enums.DOMAIN_ENUM domain, boolean hasPoints, boolean isCompleted) {
		JSONObject obj = JSONFactoryUtil.createJSONObject();
		obj.put("label", domain + (isCompleted ? "" : " Started"));
		obj.put("lineTension", 0);
		String color = domain == Enums.DOMAIN_ENUM.CLINICAL ? "#0D1F43"
				: domain == Enums.DOMAIN_ENUM.MARKET ? "#A8B139"
						: domain == Enums.DOMAIN_ENUM.REGULATORY ? "#19777a"
								: domain == Enums.DOMAIN_ENUM.TECHNOLOGY ? "#E35A25" : "black";
		obj.put("borderColor", color);
		obj.put("backgroundColor", color);
		if (!hasPoints) {
			obj.put("pointRadius", 0);
		}
		obj.put("fill", false);
		obj.put("pointStyle", "rectRot");
		obj.put("pointRadius", 5);
		return obj;
	}
	
	private JSONObject getSolidLine(Enums.DOMAIN_ENUM domain, boolean hasPoints) {
		return getSolidLine(domain, hasPoints, true);
		
	}
	
	private JSONObject getSolidLine(Enums.DOMAIN_ENUM domain) {
		return getSolidLine(domain, false);
	}	
	
	private JSONObject getDashedLine(Enums.DOMAIN_ENUM domain) {
		JSONObject obj = getSolidLine(domain, true, false);
		Integer[] arr = {15,5};
		obj.put("borderDash", arr);
		return obj;
	}
	
	private String generate2ndMaturityReportJSON(long projId, ThemeDisplay themeDisplay) {
		String res = "";
		JSONObject jsonObjClinical = getSolidLine(Enums.DOMAIN_ENUM.CLINICAL, true);
		JSONArray dataArrClinical = JSONFactoryUtil.createJSONArray();
		JSONObject jsonObjMarket = getSolidLine(Enums.DOMAIN_ENUM.MARKET, true);
		JSONArray dataArrMarket = JSONFactoryUtil.createJSONArray();	
		JSONObject jsonObjReg = getSolidLine(Enums.DOMAIN_ENUM.REGULATORY, true);
		JSONArray dataArrReg = JSONFactoryUtil.createJSONArray();
		JSONObject jsonObjTech = getSolidLine(Enums.DOMAIN_ENUM.TECHNOLOGY, true);
		JSONArray dataArrTech = JSONFactoryUtil.createJSONArray();
		
		byKey comparator = new byKey();
		Map<Integer, ProjectDelivery> clinicalMap = new TreeMap<Integer, ProjectDelivery>(comparator);
		Map<Integer, ProjectDelivery> marketMap = new TreeMap<Integer, ProjectDelivery>(comparator);	
		Map<Integer, ProjectDelivery> regMap = new TreeMap<Integer, ProjectDelivery>(comparator);		
		Map<Integer, ProjectDelivery> techMap = new TreeMap<Integer, ProjectDelivery>(comparator);
		
		JSONObject jsonObjClinicalDashed = getDashedLine(Enums.DOMAIN_ENUM.CLINICAL),
				jsonObjMarketDashed = getDashedLine(Enums.DOMAIN_ENUM.MARKET),
				jsonObjTechDashed = getDashedLine(Enums.DOMAIN_ENUM.TECHNOLOGY),
				jsonObjRegDashed = getDashedLine(Enums.DOMAIN_ENUM.REGULATORY);
		JSONArray dataArrClinicalDashed = JSONFactoryUtil.createJSONArray(),
				dataArrMarketDashed = JSONFactoryUtil.createJSONArray(),
				dataArrRegDashed = JSONFactoryUtil.createJSONArray(),
				dataArrTechDashed = JSONFactoryUtil.createJSONArray();
	
		for (ProjectStep step : ProjectStepLocalServiceUtil.findByProjectId(projId)) {
			ProjectDelivery pickedDelivery = null;
			List<ProjectDelivery> deliveries = ProjectDeliveryLocalServiceUtil.findByStepId(step.getProjectStepId());
			deliveries = Utils.sortDeliverablesByName(Utils.getGlobalGroupId(themeDisplay.getCompanyId()), deliveries);
			for (ProjectDelivery delivery : deliveries) {
				if (delivery.getStartDate() != null && delivery.getEndDate() != null) {
					pickedDelivery = delivery;
					break;
				}
			}
			if (pickedDelivery != null) {
				String phaseName = pickedDelivery.getPhase();
				String domainName = pickedDelivery.getDomain();
				int phaseOffset = Enums.getArrayNum(Enums.PHASES_ARRAY, phaseName);
				if (domainName.equals(Enums.DOMAIN_ENUM.CLINICAL.toString())) {
					clinicalMap.put(phaseOffset, pickedDelivery);
				} else if (domainName.equals(Enums.DOMAIN_ENUM.MARKET.toString())) {
					marketMap.put(phaseOffset, pickedDelivery);
				} else if (domainName.equals(Enums.DOMAIN_ENUM.REGULATORY.toString())) {
					regMap.put(phaseOffset, pickedDelivery);
				} else if (domainName.equals(Enums.DOMAIN_ENUM.TECHNOLOGY.toString())) {
					techMap.put(phaseOffset, pickedDelivery);
				} 				
			}
		}
		boolean isFirst = true;
		boolean isDashed = false;
		int milestone = -1;
		for (Map.Entry<Integer, ProjectDelivery> entry : clinicalMap.entrySet()) {
			ProjectDelivery d = entry.getValue();
			if (isFirst) {
				addStartingPoint(dataArrClinical, d.getStartDate());
			}
			if (d.getStatus() != Enums.Status.COMPLETED.getId()) {
				isDashed = true;
				addPointTo2ndChart(dataArrClinicalDashed, d.getStartDate(), entry.getKey());
				addPointTo2ndChart(dataArrClinicalDashed, d.getEndDate(), entry.getKey() + 1);
				addInterruptingPoint(dataArrClinicalDashed, d.getEndDate());
			}
			if (isDashed && d.getStatus() == Enums.Status.COMPLETED.getId()) {
				isDashed = false;
				milestone = -1;
				addPointTo2ndChart(dataArrClinicalDashed, d.getStartDate(), entry.getKey());
				addPointTo2ndChart(dataArrClinicalDashed, d.getEndDate(), entry.getKey() + 1);				
			}				
			if (isDashed && milestone == -1) {
				milestone = isFirst ? 0 : entry.getKey();
			}
			addPointTo2ndChart(dataArrClinical, d.getEndDate(), milestone > -1 ? milestone : entry.getKey() + 1);
			if (isFirst) isFirst = false;
		}
		isFirst = true;
		isDashed = false;
		milestone = -1;		
		for (Map.Entry<Integer, ProjectDelivery> entry : marketMap.entrySet()) {
			ProjectDelivery d = entry.getValue();
			if (isFirst) {
				addStartingPoint(dataArrMarket, d.getStartDate());
			}
			if (d.getStatus() != Enums.Status.COMPLETED.getId()) {
				isDashed = true;
				addPointTo2ndChart(dataArrMarketDashed, d.getStartDate(), entry.getKey());
				addPointTo2ndChart(dataArrMarketDashed, d.getEndDate(), entry.getKey() + 1);
				addInterruptingPoint(dataArrMarketDashed, d.getEndDate());
			}
			if (isDashed && d.getStatus() == Enums.Status.COMPLETED.getId()) {
				isDashed = false;
				milestone = -1;
				addPointTo2ndChart(dataArrMarketDashed, d.getStartDate(), entry.getKey());
				addPointTo2ndChart(dataArrMarketDashed, d.getEndDate(), entry.getKey() + 1);				
			}			
			if (isDashed && milestone == -1) {
				milestone = isFirst ? 0 : entry.getKey();
			}			
			addPointTo2ndChart(dataArrMarket, d.getEndDate(), milestone > -1 ? milestone : entry.getKey() + 1);
			if (isFirst) isFirst = false;
		}
		isFirst = true;
		isDashed = false;
		milestone = -1;		
		for (Map.Entry<Integer, ProjectDelivery> entry : regMap.entrySet()) {
			ProjectDelivery d = entry.getValue();
			if (isFirst) {
				addStartingPoint(dataArrReg, d.getStartDate());
			}
			if (d.getStatus() != Enums.Status.COMPLETED.getId()) {
				isDashed = true;
				addPointTo2ndChart(dataArrRegDashed, d.getStartDate(), entry.getKey());
				addPointTo2ndChart(dataArrRegDashed, d.getEndDate(), entry.getKey() + 1);
				addInterruptingPoint(dataArrRegDashed, d.getEndDate());
			}
			if (isDashed && d.getStatus() == Enums.Status.COMPLETED.getId()) {
				isDashed = false;
				milestone = -1;
				addPointTo2ndChart(dataArrRegDashed, d.getStartDate(), entry.getKey());
				addPointTo2ndChart(dataArrRegDashed, d.getEndDate(), entry.getKey() + 1);				
			}			
			if (isDashed && milestone == -1) {
				milestone = isFirst ? 0 : entry.getKey();
			}			
			addPointTo2ndChart(dataArrReg, d.getEndDate(), milestone > -1 ? milestone : entry.getKey() + 1);
			if (isFirst) isFirst = false;			
		}	
		isFirst = true;
		isDashed = false;
		milestone = -1;		
		for (Map.Entry<Integer, ProjectDelivery> entry : techMap.entrySet()) {
			ProjectDelivery d = entry.getValue();
			if (isFirst) {
				addStartingPoint(dataArrTech, d.getStartDate());
			}
			if (d.getStatus() != Enums.Status.COMPLETED.getId()) {
				isDashed = true;
				addPointTo2ndChart(dataArrTechDashed, d.getStartDate(), entry.getKey());
				addPointTo2ndChart(dataArrTechDashed, d.getEndDate(), entry.getKey() + 1);
				addInterruptingPoint(dataArrTechDashed, d.getEndDate());
			} 
			if (isDashed && d.getStatus() == Enums.Status.COMPLETED.getId()) {
				isDashed = false;
				milestone = -1;
				addPointTo2ndChart(dataArrTechDashed, d.getStartDate(), entry.getKey());
				addPointTo2ndChart(dataArrTechDashed, d.getEndDate(), entry.getKey() + 1);				
			}
			if (isDashed && milestone == -1) {
				milestone = isFirst ? 0 : entry.getKey();
			}	
			addPointTo2ndChart(dataArrTech, d.getEndDate(), milestone > -1 ? milestone : entry.getKey() + 1);
			if (isFirst) isFirst = false;			
		}
		jsonObjClinical.put("data", dataArrClinical);
		jsonObjMarket.put("data", dataArrMarket);	
		jsonObjReg.put("data", dataArrReg);
		jsonObjTech.put("data", dataArrTech);
		jsonObjClinicalDashed.put("data", dataArrClinicalDashed);
		jsonObjMarketDashed.put("data", dataArrMarketDashed);	
		jsonObjRegDashed.put("data", dataArrRegDashed);
		jsonObjTechDashed.put("data", dataArrTechDashed);		
		JSONArray resJSON = JSONFactoryUtil.createJSONArray();
		resJSON.put(jsonObjClinical);
		resJSON.put(jsonObjMarket);
		resJSON.put(jsonObjReg);
		resJSON.put(jsonObjTech);
		resJSON.put(jsonObjClinicalDashed);
		resJSON.put(jsonObjMarketDashed);
		resJSON.put(jsonObjRegDashed);
		resJSON.put(jsonObjTechDashed);		
		res = resJSON.toJSONString();
		return res;
	}
	
	//TODO rename
	private void addPoint(JSONArray dataArr, Date deliveryDate, int phaseOffset, int domainOffset) {
		JSONObject dataObj = JSONFactoryUtil.createJSONObject();
		dataObj.put("x", Utils.getDateAsString(deliveryDate));
		dataObj.put("y", phaseOffset == -1 ? "null" : (phaseOffset * 5 + domainOffset + 1));					
		dataArr.put(dataObj);
	}
	
	private void addPointTo2ndChart(JSONArray dataArr, Date date, int phaseOffset) {
		JSONObject dataObj = JSONFactoryUtil.createJSONObject();
		dataObj.put("x", Utils.getDateAsString(date));
		dataObj.put("y", phaseOffset);					
		dataArr.put(dataObj);
	}
	
	private void addStartingPoint(JSONArray dataArr, Date date) {
		addPointTo2ndChart(dataArr, date, 0);
	}
	
	// this 'invisible' point is added after each point pair to make a line interruption (otherwise a line is linked)
	// date can be any value in a range of current chart
	private void addInterruptingPoint(JSONArray dataArr, Date deliveryDate) {
		addPoint(dataArr, deliveryDate, -1, -1);
	}
	
	private class byKey implements Comparator<Integer> {
	    public int compare(Integer e1, Integer e2) {
	        if (e1 > e2){
	            return 1;
	        } else if (e1 == e2) {
	            return 0;
	        } else {
	            return -1;
	        }
	    }
	}
}