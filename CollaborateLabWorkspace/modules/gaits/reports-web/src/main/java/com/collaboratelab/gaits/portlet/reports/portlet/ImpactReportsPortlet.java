package com.collaboratelab.gaits.portlet.reports.portlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

import com.collaboratelab.gaits.portlet.reports.constants.ReportsPortletKeys;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

@Component(
		immediate = true,
		property = {
			"com.liferay.portlet.display-category=category.gaits",
			"com.liferay.portlet.instanceable=false",
			"javax.portlet.display-name=Impact Reports",
			"javax.portlet.init-param.template-path=/",
			"javax.portlet.init-param.view-template=/impact/view.jsp",
			"javax.portlet.name=" + ReportsPortletKeys.Impact_Reports,
			"javax.portlet.resource-bundle=content.Language",
			"javax.portlet.security-role-ref=power-user,user",
			"com.liferay.portlet.header-portlet-javascript=/js/Chart.bundle.min.js",
			"com.liferay.portlet.header-portlet-javascript=/js/tooltipster.bundle.min.js",
			"com.liferay.portlet.header-portlet-css=/css/tooltipster.bundle.min.css",
			"com.liferay.portlet.header-portlet-css=/css/tooltipster-sideTip-borderless.min.css",
			"com.liferay.portlet.header-portlet-javascript=/js/chartjs-plugin-annotation.min.js"
		},
		service = Portlet.class
	)
public class ImpactReportsPortlet extends MVCPortlet {

}
