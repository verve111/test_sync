package com.collaboratelab.gaits.portlet.reports.utils;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import com.collaboratelab.impact.model.Collaborators;
import com.collaboratelab.impact.model.IntellectualProperty;
import com.collaboratelab.impact.model.Support;
import com.collaboratelab.impact.model.TeamMemberProjectRecognition;
import com.collaboratelab.impactweb.constants.ImpactWebPortletKeys;

public class ImpactUtils {
	public static <K extends Comparable, V> Map<K, V> sortByKeys(Map<K, V> map) {
		return new TreeMap<>(map);
	}

	public static Integer[] putIntegerArray(List<Collaborators> collaborators, String date) {
		ImpactWebPortletKeys.Type[] types = ImpactWebPortletKeys.Type.values();
		Integer intArray[] = new Integer[types.length];
		int i = 0;
		for (ImpactWebPortletKeys.Type t : types) {
			int l = 0;
			for (Collaborators c : collaborators) {
				if (c.getType() == t.getId())
					l++;
			}
			intArray[i] = new Integer(l);
			i++;
		}
		return intArray;
	}

	public static Integer[] putIntegerArrayRecognition(List<TeamMemberProjectRecognition> recognition, String date) {
		ImpactWebPortletKeys.TeamCatRating cat[] = ImpactWebPortletKeys.TeamCatRating.values();
		Integer intArray[] = new Integer[cat.length];
		int i = 0;
		for (ImpactWebPortletKeys.TeamCatRating c : cat) {
			int l = 0;
			for (TeamMemberProjectRecognition r : recognition) {
				if (r.getCatRating() == c.getId())
					l++;
			}
			intArray[i] = new Integer(l);
			i++;
		}
		return intArray;
	}
	
	public static Integer[] putIntegerArrayIntellectualProperty(List<IntellectualProperty> properties, String date) {
		//ImpactWebPortletKeys.LicenseStatus licenses[] = ImpactWebPortletKeys.LicenseStatus.values();
		Integer intArray[] = new Integer[2];
		
		intArray[0] = properties.size();
		
		int l = 0;
		for (IntellectualProperty r : properties) {
			if (r.getLicenseStatus() != 0 )
					l++;
		}
		intArray[1] = new Integer(l);
		
		return intArray;
	}
	
	public static Double[] putDoubleArrayFunding(List<Support> support) {
		ImpactWebPortletKeys.SponsorType sp[] = ImpactWebPortletKeys.SponsorType.values();
		Double doubleArray[] = new Double[sp.length];
		int i = 0;
		for (ImpactWebPortletKeys.SponsorType s : sp) {
			double sum = 0;
			for (Support sup : support) {
				if (sup.getSponsorType() == s.getId())
					sum += sup.getTotalValue();
			}
			doubleArray[i] = new Double(sum);
			i++;
		}

		return doubleArray;
	}	
	
	public static Map<Date, Integer[]> putIntArrayFundingForTable(List<Support> support) {
		Map<Date, Integer[]> res = new TreeMap<Date, Integer[]>();
		ImpactWebPortletKeys.SponsorType sp[] = ImpactWebPortletKeys.SponsorType.values();
		for (Support sup : support) {
			int i = 0;
			Integer intArray[] = new Integer[sp.length];			
			for (ImpactWebPortletKeys.SponsorType s : sp) {
				if (s.getId() == sup.getSponsorType()) {
					intArray[i++] = (int) sup.getTotalValue();
				} else {
					intArray[i++] = 0;
				}
			}
			res.put(sup.getStartDate(), intArray);
		}
		return res;
	}		
	
	public static String getDateByLocale(Date date, Locale locale) {
		
		DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, locale);
		String formattedDate = df.format(date);
		
		return formattedDate;	
	}
                  

}
