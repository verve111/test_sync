package com.journalarticle.hook.action;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Stack;
import java.util.stream.Collectors;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.PortletURL;
import javax.portlet.WindowState;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.collaboratelab.project.constants.Utils;
import com.collaboratelab.project.constants.enums.Enums;
import com.collaboratelab.project.model.Project;
import com.collaboratelab.project.service.ProjectLocalServiceUtil;
import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.model.AssetVocabulary;
import com.liferay.asset.kernel.service.AssetEntryLocalServiceUtil;
import com.liferay.asset.kernel.service.AssetVocabularyLocalServiceUtil;
import com.liferay.document.library.kernel.service.DLAppLocalServiceUtil;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.storage.Field;
import com.liferay.dynamic.data.mapping.storage.FieldConstants;
import com.liferay.dynamic.data.mapping.storage.Fields;
import com.liferay.dynamic.data.mapping.util.DDMUtil;
import com.liferay.journal.constants.JournalPortletKeys;
import com.liferay.journal.exception.ArticleContentSizeException;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalArticleConstants;
import com.liferay.journal.model.JournalFolder;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.journal.service.JournalArticleService;
import com.liferay.journal.service.JournalContentSearchLocalService;
import com.liferay.journal.util.JournalConverter;
import com.liferay.petra.collection.stack.FiniteUniqueStack;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.portlet.LiferayWindowState;
import com.liferay.portal.kernel.portlet.PortletPreferencesFactoryUtil;
import com.liferay.portal.kernel.portlet.PortletProvider.Action;
import com.liferay.portal.kernel.portlet.PortletProviderUtil;
import com.liferay.portal.kernel.portlet.PortletURLFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.IndexerRegistryUtil;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.GroupLocalServiceUtil;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.LayoutLocalServiceUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.servlet.MultiSessionMessages;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.upload.LiferayFileItemException;
import com.liferay.portal.kernel.upload.UploadException;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ContentTypes;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.HttpUtil;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.kernel.util.MapUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.Element;
import com.liferay.portal.kernel.xml.Node;
import com.liferay.portal.kernel.xml.SAXReaderUtil;
import com.liferay.portal.kernel.xml.XPath;
import com.liferay.portal.util.PropsValues;

@Component(immediate = true, property = { "javax.portlet.name=com_liferay_journal_web_portlet_JournalPortlet",
		"mvc.command.name=addArticle", "service.ranking:Integer=1700" }, service = MVCActionCommand.class)
public class AddJournalArticleImpl extends BaseMVCActionCommand {

	public static final String VERSION_SEPARATOR = "_version_";
	public static final int MAX_STACK_SIZE = 20;
	private static final Log logger = LogFactoryUtil.getLog(AddJournalArticleImpl.class);
	
	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		updateArticle(actionRequest, actionResponse);
	}
	

	protected void updateArticle(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		UploadException uploadException = (UploadException) actionRequest.getAttribute(WebKeys.UPLOAD_EXCEPTION);

		if (uploadException != null) {
			Throwable cause = uploadException.getCause();

			if (uploadException.isExceededLiferayFileItemSizeLimit()) {
				throw new LiferayFileItemException(cause);
			}

			if (uploadException.isExceededFileSizeLimit() || uploadException.isExceededUploadRequestSizeLimit()) {

				throw new ArticleContentSizeException(cause);
			}

			throw new PortalException(cause);
		}

		UploadPortletRequest uploadPortletRequest = _portal.getUploadPortletRequest(actionRequest);

		if (_log.isDebugEnabled()) {
			_log.debug("Updating article " + MapUtil.toString(uploadPortletRequest.getParameterMap()));
		}

		String actionName = ParamUtil.getString(actionRequest, ActionRequest.ACTION_NAME);

		long groupId = ParamUtil.getLong(uploadPortletRequest, "groupId");
		long folderId = ParamUtil.getLong(uploadPortletRequest, "folderId");
		long classNameId = ParamUtil.getLong(uploadPortletRequest, "classNameId");
		long classPK = ParamUtil.getLong(uploadPortletRequest, "classPK");

		String articleId = ParamUtil.getString(uploadPortletRequest, "articleId");

		boolean autoArticleId = ParamUtil.getBoolean(uploadPortletRequest, "autoArticleId");
		double version = ParamUtil.getDouble(uploadPortletRequest, "version");

		Map<Locale, String> titleMap = LocalizationUtil.getLocalizationMap(actionRequest, "title");
		Map<Locale, String> descriptionMap = LocalizationUtil.getLocalizationMap(actionRequest, "description");

		ServiceContext serviceContext = ServiceContextFactory.getInstance(JournalArticle.class.getName(),
				uploadPortletRequest);

		String ddmStructureKey = ParamUtil.getString(uploadPortletRequest, "ddmStructureKey");

		DDMStructure ddmStructure = _ddmStructureLocalService.getStructure(_portal.getSiteGroupId(groupId),
				_portal.getClassNameId(JournalArticle.class), ddmStructureKey, true);

		Fields fields = DDMUtil.getFields(ddmStructure.getStructureId(), serviceContext);

		String structureContent = _journalConverter.getContent(ddmStructure, fields);

		Map<String, byte[]> structureImages = getImages(structureContent, fields);

		Object[] contentAndImages = new Object[] { structureContent, structureImages };

		String content = (String) contentAndImages[0];

		Map<String, byte[]> images = (HashMap<String, byte[]>) (contentAndImages[1]);

		String ddmTemplateKey = ParamUtil.getString(uploadPortletRequest, "ddmTemplateKey");
		String layoutUuid = ParamUtil.getString(uploadPortletRequest, "layoutUuid");

		Layout targetLayout = getArticleLayout(layoutUuid, groupId);

		if (targetLayout == null) {
			layoutUuid = null;
		}

		int displayDateMonth = ParamUtil.getInteger(uploadPortletRequest, "displayDateMonth");
		int displayDateDay = ParamUtil.getInteger(uploadPortletRequest, "displayDateDay");
		int displayDateYear = ParamUtil.getInteger(uploadPortletRequest, "displayDateYear");
		int displayDateHour = ParamUtil.getInteger(uploadPortletRequest, "displayDateHour");
		int displayDateMinute = ParamUtil.getInteger(uploadPortletRequest, "displayDateMinute");
		int displayDateAmPm = ParamUtil.getInteger(uploadPortletRequest, "displayDateAmPm");

		if (displayDateAmPm == Calendar.PM) {
			displayDateHour += 12;
		}

		int expirationDateMonth = ParamUtil.getInteger(uploadPortletRequest, "expirationDateMonth");
		int expirationDateDay = ParamUtil.getInteger(uploadPortletRequest, "expirationDateDay");
		int expirationDateYear = ParamUtil.getInteger(uploadPortletRequest, "expirationDateYear");
		int expirationDateHour = ParamUtil.getInteger(uploadPortletRequest, "expirationDateHour");
		int expirationDateMinute = ParamUtil.getInteger(uploadPortletRequest, "expirationDateMinute");
		int expirationDateAmPm = ParamUtil.getInteger(uploadPortletRequest, "expirationDateAmPm");
		boolean neverExpire = ParamUtil.getBoolean(uploadPortletRequest, "neverExpire");

		if (!PropsValues.SCHEDULER_ENABLED) {
			neverExpire = true;
		}

		if (expirationDateAmPm == Calendar.PM) {
			expirationDateHour += 12;
		}

		int reviewDateMonth = ParamUtil.getInteger(uploadPortletRequest, "reviewDateMonth");
		int reviewDateDay = ParamUtil.getInteger(uploadPortletRequest, "reviewDateDay");
		int reviewDateYear = ParamUtil.getInteger(uploadPortletRequest, "reviewDateYear");
		int reviewDateHour = ParamUtil.getInteger(uploadPortletRequest, "reviewDateHour");
		int reviewDateMinute = ParamUtil.getInteger(uploadPortletRequest, "reviewDateMinute");
		int reviewDateAmPm = ParamUtil.getInteger(uploadPortletRequest, "reviewDateAmPm");

		boolean neverReview = ParamUtil.getBoolean(uploadPortletRequest, "neverReview");

		if (!PropsValues.SCHEDULER_ENABLED) {
			neverReview = true;
		}

		if (reviewDateAmPm == Calendar.PM) {
			reviewDateHour += 12;
		}

		boolean indexable = ParamUtil.getBoolean(uploadPortletRequest, "indexable");

		boolean smallImage = ParamUtil.getBoolean(uploadPortletRequest, "smallImage");
		String smallImageURL = ParamUtil.getString(uploadPortletRequest, "smallImageURL");
		File smallFile = uploadPortletRequest.getFile("smallFile");

		String articleURL = ParamUtil.getString(uploadPortletRequest, "articleURL");

		JournalArticle article = null;
		String oldUrlTitle = StringPool.BLANK;

		if (actionName.equals("addArticle")) {

			// Add article

			article = _journalArticleService.addArticle(groupId, folderId, classNameId, classPK, articleId,
					autoArticleId, titleMap, descriptionMap, content, ddmStructureKey, ddmTemplateKey, layoutUuid,
					displayDateMonth, displayDateDay, displayDateYear, displayDateHour, displayDateMinute,
					expirationDateMonth, expirationDateDay, expirationDateYear, expirationDateHour,
					expirationDateMinute, neverExpire, reviewDateMonth, reviewDateDay, reviewDateYear, reviewDateHour,
					reviewDateMinute, neverReview, indexable, smallImage, smallImageURL, smallFile, images, articleURL,
					serviceContext);
			
			
			/* Copy articles in childs project by type */
			
			globalSiteId= getGlobalSite();
			
			if(article.getGroupId() == globalSiteId) {
				
				boolean isCopyResourceOnProjects = isCopyResourceOnProjects(article, actionRequest);
				
				if(isCopyResourceOnProjects) {
				
					List<Project> projects = ProjectLocalServiceUtil.getProjects(0, ProjectLocalServiceUtil.getProjectsCount());
					String categoryStr = ParamUtil.getString(actionRequest, Utils.getRequestParamIdForMedicalPrj(getGlobalSite()));
					if(!categoryStr.equals("")) {
						String[] categoryIdsStr = categoryStr.split(",");
						long[] categoryIds2 = new long[categoryIdsStr.length];
						int j = 0;
						for(String catId : categoryIdsStr) {
							categoryIds2[j] = Long.parseLong(catId);
							j++;
						}
						List<AssetCategory> categories = Utils.getCategories(categoryIds2);
						if(categories != null) {
							for(AssetCategory cat: categories)
							{
								List<Project> projectsByType = Utils.getProjectsByType(Enums.TypeProject.MEDICAL.getId());
									
								for(Project p : projectsByType) {
									long grId = p.getGroupId();
									Group group = GroupLocalServiceUtil.fetchGroup(grId);
									if(group != null){			
										long userId = article.getUserId();
										
											
										addJournalArticleInPopulatedFolder(article,p.getGroupId(),userId,globalSiteId, actionRequest);
									}
									
								}
							}
						}
					}
				}
				if(article.getExpandoBridge().hasAttribute("parentEntry"))
					article.getExpandoBridge().setAttribute("parentEntry", article.getArticleId());
				
			}
			
		} else {

			// Update article
			String categoryIds = ParamUtil.getString(actionRequest, Utils.getRequestParamIdForMedicalPrj(getGlobalSite()));
			
			article = _journalArticleService.getArticle(groupId, articleId, version);
			
			String tempOldUrlTitle = article.getUrlTitle();

			if (actionName.equals("previewArticle") || actionName.equals("updateArticle")) {

				article = _journalArticleService.updateArticle(groupId, folderId, articleId, version, titleMap,
						descriptionMap, content, ddmStructureKey, ddmTemplateKey, layoutUuid, displayDateMonth,
						displayDateDay, displayDateYear, displayDateHour, displayDateMinute, expirationDateMonth,
						expirationDateDay, expirationDateYear, expirationDateHour, expirationDateMinute, neverExpire,
						reviewDateMonth, reviewDateDay, reviewDateYear, reviewDateHour, reviewDateMinute, neverReview,
						indexable, smallImage, smallImageURL, smallFile, images, articleURL, serviceContext);
				
			}

			if (!tempOldUrlTitle.equals(article.getUrlTitle())) {
				oldUrlTitle = tempOldUrlTitle;
			}
		}

		// Recent articles

		addRecentArticle(actionRequest, article);

		// Journal content

		String portletResource = ParamUtil.getString(actionRequest, "portletResource");

		long referringPlid = ParamUtil.getLong(actionRequest, "referringPlid");

		if (Validator.isNotNull(portletResource) && (referringPlid > 0)) {
			Layout layout = _layoutLocalService.getLayout(referringPlid);

			PortletPreferences portletPreferences = PortletPreferencesFactoryUtil.getStrictPortletSetup(layout,
					portletResource);

			if (portletPreferences != null) {
				portletPreferences.setValue("groupId", String.valueOf(article.getGroupId()));
				portletPreferences.setValue("articleId", article.getArticleId());

				portletPreferences.store();

				updateContentSearch(actionRequest, portletResource, article.getArticleId());
			}
		}

		sendEditArticleRedirect(actionRequest, actionResponse, article, oldUrlTitle);

		long ddmStructureClassNameId = _portal.getClassNameId(DDMStructure.class);

		if (article.getClassNameId() == ddmStructureClassNameId) {
			String ddmPortletId = PortletProviderUtil.getPortletId(DDMStructure.class.getName(), Action.EDIT);

			MultiSessionMessages.add(actionRequest, ddmPortletId + "requestProcessed");
		}

	}
	
	private void addJournalArticleInPopulatedFolder(JournalArticle entrArticle,long targetGroupId,long userId,long globalGroupId, ActionRequest actionRequest) {
		
		ServiceContext serviceContext = new ServiceContext();
		serviceContext.setScopeGroupId(targetGroupId);
		
		JournalFolder folder = Utils.createWebContentFolder(targetGroupId, userId, 0, "Populated from global");

		String categoryStr = ParamUtil.getString(actionRequest, Utils.getRequestParamIdForMedicalPrj(getGlobalSite()));
		if (!categoryStr.equals("")) {
		
			long[] categoryIds = Utils.getCategoryIds(categoryStr);
			
			List<AssetCategory> cats = Utils.getCategories(categoryIds);
			if(cats != null) {
				
				JournalArticle article = null;
				try {
					
					// skip if duplicate by category. Needed if one entry has 2 cats at the same time (we don't need 2 copies of this)
					if (Utils.isWcDuplicateExists(targetGroupId, entrArticle.getTitleCurrentValue(), folder.getFolderId(), cats, false)) {
						return;
					}					
					
					article = JournalArticleLocalServiceUtil.addArticle(userId, targetGroupId, folder.getFolderId(),
							entrArticle.getTitleMap(), entrArticle.getDescriptionMap(), entrArticle.getContent(),
							entrArticle.getDDMStructureKey(), entrArticle.getDDMTemplateKey(), serviceContext);
				} catch (PortalException e1) {
					logger.error("add article ex while propagation for resource", e1);
				}
				
				if (article.getExpandoBridge().hasAttribute("parentEntry")) {
					article.getExpandoBridge().setAttribute("parentEntry", entrArticle.getArticleId());
				}						
				
				try {
					AssetEntryLocalServiceUtil.updateEntry(userId, article.getGroupId(), article.getCreateDate(),
							article.getModifiedDate(), JournalArticle.class.getName(), article.getResourcePrimKey(),
							article.getUuid(), 0, categoryIds, null, true, true, null, null,
							article.getCreateDate(), null, ContentTypes.TEXT_HTML, article.getTitle(), null, null, null, null,
							0, 0, null);
				} catch (PortalException e) {
					logger.error("update article ex while propagation for resource", e);
				}
		
				Indexer<JournalArticle> indexer = IndexerRegistryUtil.nullSafeGetIndexer(JournalArticle.class);
				
				try {
					indexer.reindex(article);
				} catch (SearchException e) {
					e.printStackTrace();
				}
				
				try {
					Utils.copyPermissionForJournalArticle(entrArticle, article);
				} catch (PortalException e) {
					
					logger.error("can't copy permissions for resource", e);
				}
				
			}
		}
	}
	
	private boolean isCopyResourceOnProjects(JournalArticle model,ActionRequest actionRequest) {
		List<Enums.TypeProject> types = Arrays.asList(Enums.TypeProject.values());
		List<String> typesName = new ArrayList<String>();
		
		for(Enums.TypeProject t : types) {
			typesName.add(t.getName());
		}
		String categoryStr = ParamUtil.getString(actionRequest, Utils.getRequestParamIdForMedicalPrj(getGlobalSite()));
		if(!categoryStr.equals("")) {		
			long[] categoryIds = Utils.getCategoryIds(categoryStr);
			
			List<AssetCategory> categories = Utils.getCategories(categoryIds);
			if(categories != null) {
				for(AssetCategory cat : categories) {
					AssetVocabulary voc = null;
					try {
						voc = AssetVocabularyLocalServiceUtil.getVocabulary(cat.getVocabularyId());
					} catch (PortalException e) {
						e.printStackTrace();
					}
				
					if(voc != null && typesName.contains(voc.getName()))
						return true;
				}
			}
		}
		return false;
	}
	
	
	private long getGlobalSite() {
		if (globalSiteId == 0) {
			long companyId = CompanyThreadLocal.getCompanyId();
			globalSiteId = Utils.getGlobalGroupId(companyId);
		}
		return globalSiteId;
	}
	
	
	public static void addRecentArticle(
			PortletRequest portletRequest, JournalArticle article) {

			if (article != null) {
				Stack<JournalArticle> stack = getRecentArticles(portletRequest);

				stack.push(article);
			}
	}
	
	public static Layout getArticleLayout(String layoutUuid, long groupId) {
		if (Validator.isNull(layoutUuid)) {
			return null;
		}

		// The target page and the article must belong to the same group

		Layout layout = LayoutLocalServiceUtil.fetchLayoutByUuidAndGroupId(
			layoutUuid, groupId, false);

		if (layout == null) {
			layout = LayoutLocalServiceUtil.fetchLayoutByUuidAndGroupId(
				layoutUuid, groupId, true);
		}

		return layout;
	}
	
	public static Stack<JournalArticle> getRecentArticles(
			PortletRequest portletRequest) {

			PortletSession portletSession = portletRequest.getPortletSession();

			Stack<JournalArticle> recentArticles =
				(Stack<JournalArticle>)portletSession.getAttribute(
					WebKeys.JOURNAL_RECENT_ARTICLES);

			if (recentArticles == null) {
				recentArticles = new FiniteUniqueStack<>(MAX_STACK_SIZE);

				portletSession.setAttribute(
					WebKeys.JOURNAL_RECENT_ARTICLES, recentArticles);
			}

			return recentArticles;
		}
	
	public static Map<String, byte[]> getImages(String content, Fields fields)
			throws Exception {

			Map<String, byte[]> images = new HashMap<>();

			for (Field field : fields) {
				String dataType = field.getDataType();

				if (!dataType.equals(FieldConstants.IMAGE)) {
					continue;
				}

				Map<Locale, List<Serializable>> valuesMap = field.getValuesMap();

				for (Map.Entry<Locale, List<Serializable>> entry :
						valuesMap.entrySet()) {

					List<Serializable> values = entry.getValue();

					for (int i = 0; i < values.size(); i++) {
						JSONObject jsonObject = JSONFactoryUtil.createJSONObject(
							(String)values.get(i));

						String type = jsonObject.getString("type");

						if (type.equals("document")) {
							continue;
						}

						String uuid = jsonObject.getString("uuid");
						long groupId = jsonObject.getLong("groupId");

						if (Validator.isNotNull(uuid) && (groupId > 0)) {
							StringBundler sb = new StringBundler(7);

							sb.append(
								getElementInstanceId(content, field.getName(), i));
							sb.append(StringPool.UNDERLINE);
							sb.append(field.getName());
							sb.append(StringPool.UNDERLINE);
							sb.append(LanguageUtil.getLanguageId(entry.getKey()));

							FileEntry fileEntry =
								DLAppLocalServiceUtil.getFileEntryByUuidAndGroupId(
									uuid, groupId);

							byte[] bytes = FileUtil.getBytes(
								fileEntry.getContentStream());

							images.put(sb.toString(), bytes);
						}
					}
				}
			}

			return images;
		}
	
	protected static String getElementInstanceId(
			String content, String fieldName, int index)
		throws Exception {

		Document document = SAXReaderUtil.read(content);

		String xPathExpression =
			"//dynamic-element[@name = " +
				HtmlUtil.escapeXPathAttribute(fieldName) + "]";

		XPath xPath = SAXReaderUtil.createXPath(xPathExpression);

		List<Node> nodes = xPath.selectNodes(document);

		if (index > nodes.size()) {
			return StringPool.BLANK;
		}

		Element dynamicElementElement = (Element)nodes.get(index);

		return dynamicElementElement.attributeValue("instance-id");
	}
	
	protected void sendEditArticleRedirect(
			ActionRequest actionRequest, ActionResponse actionResponse)
		throws Exception {

		sendEditArticleRedirect(
			actionRequest, actionResponse, null, StringPool.BLANK);
	}

	protected void sendEditArticleRedirect(
			ActionRequest actionRequest, ActionResponse actionResponse,
			JournalArticle article, String oldUrlTitle)
		throws Exception {

		String actionName = ParamUtil.getString(
			actionRequest, ActionRequest.ACTION_NAME);

		String redirect = ParamUtil.getString(actionRequest, "redirect");

		int workflowAction = ParamUtil.getInteger(
			actionRequest, "workflowAction", WorkflowConstants.ACTION_PUBLISH);

		String portletId = _httpUtil.getParameter(redirect, "p_p_id", false);

		String namespace = _portal.getPortletNamespace(portletId);

		if (Validator.isNotNull(oldUrlTitle)) {
			String oldRedirectParam = namespace + "redirect";

			String oldRedirect = _httpUtil.getParameter(
				redirect, oldRedirectParam, false);

			if (Validator.isNotNull(oldRedirect)) {
				String newRedirect = _httpUtil.decodeURL(oldRedirect);

				newRedirect = StringUtil.replace(
					newRedirect, oldUrlTitle, article.getUrlTitle());
				newRedirect = StringUtil.replace(
					newRedirect, oldRedirectParam, "redirect");

				redirect = StringUtil.replace(
					redirect, oldRedirect, newRedirect);
			}
		}

		if ((actionName.equals("deleteArticle") ||
			 actionName.equals("deleteArticles")) &&
			!hasArticle(actionRequest)) {

			ThemeDisplay themeDisplay =
				(ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);

			PortletURL portletURL = PortletURLFactoryUtil.create(
				actionRequest, themeDisplay.getPpid(),
				themeDisplay.getPlid(), PortletRequest.RENDER_PHASE);

			redirect = portletURL.toString();
		}

		if ((article != null) &&
			(workflowAction == WorkflowConstants.ACTION_SAVE_DRAFT)) {

			redirect = getSaveAndContinueRedirect(
				actionRequest, article, redirect);

			if (actionName.equals("previewArticle")) {
				SessionMessages.add(actionRequest, "previewRequested");

				hideDefaultSuccessMessage(actionRequest);
			}
		}
		else {
			WindowState windowState = actionRequest.getWindowState();

			if (windowState.equals(LiferayWindowState.POP_UP)) {
				redirect = _portal.escapeRedirect(redirect);

				if (Validator.isNotNull(redirect)) {
					if (actionName.equals("addArticle") && (article != null)) {
						redirect = _httpUtil.addParameter(
							redirect, namespace + "className",
							JournalArticle.class.getName());
						redirect = _httpUtil.addParameter(
							redirect, namespace + "classPK",
							getClassPK(article));
					}
				}
			}
		}

		actionRequest.setAttribute(WebKeys.REDIRECT, redirect);
	}
	
	public static long getClassPK(JournalArticle article) {
		if ((article.isDraft() || article.isPending()) &&
			(article.getVersion() != JournalArticleConstants.VERSION_DEFAULT)) {

			return article.getPrimaryKey();
		}
		else {
			return article.getResourcePrimKey();
		}
	}
	
	public static boolean hasArticle(ActionRequest actionRequest)
			throws Exception {

			ThemeDisplay themeDisplay = (ThemeDisplay)actionRequest.getAttribute(
				WebKeys.THEME_DISPLAY);

			String articleId = ParamUtil.getString(actionRequest, "articleId");

			if (Validator.isNull(articleId)) {
				String[] articleIds = StringUtil.split(
					ParamUtil.getString(actionRequest, "rowIds"));

				if (articleIds.length <= 0) {
					return false;
				}

				articleId = articleIds[0];
			}

			int pos = articleId.lastIndexOf(VERSION_SEPARATOR);

			if (pos != -1) {
				articleId = articleId.substring(0, pos);
			}

			JournalArticle article = JournalArticleLocalServiceUtil.fetchArticle(
				themeDisplay.getScopeGroupId(), articleId);

			if (article == null) {
				return false;
			}

			return true;
		}

	
	protected String getSaveAndContinueRedirect(
			ActionRequest actionRequest, JournalArticle article,
			String redirect)
		throws Exception {

		String referringPortletResource = ParamUtil.getString(
			actionRequest, "referringPortletResource");
		
		
		long referringPlid = ParamUtil.getLong(actionRequest, "referringPlid");
		
		PortletURL portletURL = PortletURLFactoryUtil.create(
			actionRequest, JournalPortletKeys.JOURNAL,
			referringPlid, PortletRequest.RENDER_PHASE);

		portletURL.setParameter("mvcPath", "/edit_article.jsp");
		portletURL.setParameter("redirect", redirect);
		portletURL.setParameter(
			"referringPortletResource", referringPortletResource);
		portletURL.setParameter(
			"resourcePrimKey", String.valueOf(article.getResourcePrimKey()));
		portletURL.setParameter(
			"groupId", String.valueOf(article.getGroupId()));
		portletURL.setParameter(
			"folderId", String.valueOf(article.getFolderId()));
		portletURL.setParameter("articleId", article.getArticleId());
		portletURL.setParameter(
			"version", String.valueOf(article.getVersion()));
		portletURL.setWindowState(actionRequest.getWindowState());

		return portletURL.toString();
	}

	protected void updateContentSearch(
			ActionRequest actionRequest, String portletResource,
			String articleId)
		throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay)actionRequest.getAttribute(
			WebKeys.THEME_DISPLAY);

		Layout layout = themeDisplay.getLayout();

		long referringPlid = ParamUtil.getLong(actionRequest, "referringPlid");

		if (referringPlid > 0) {
			layout = _layoutLocalService.fetchLayout(referringPlid);
		}

		_journalContentSearchLocalService.updateContentSearch(
			layout.getGroupId(), layout.isPrivateLayout(), layout.getLayoutId(),
			portletResource, articleId, true);
	}
	
	@Reference
	protected void setDDMStructureLocalService(DDMStructureLocalService ddmStructureLocalService) {

		_ddmStructureLocalService = ddmStructureLocalService;
	}

	protected void unsetDDMStructureLocalService(DDMStructureLocalService ddmStructureLocalService) {

		_ddmStructureLocalService = null;
	}
	
	protected void unsetLayoutLocalService(
			LayoutLocalService layoutLocalService) {

			_layoutLocalService = null;
	}
	
	@Reference
	protected void setLayoutLocalService(
		LayoutLocalService layoutLocalService) {

		_layoutLocalService = layoutLocalService;
	}
	
	protected void unsetJournalArticleService(
			JournalArticleService journalArticleService) {

			_journalArticleService = null;
	}
	
	@Reference
	protected void setJournalArticleService(
		JournalArticleService journalArticleService) {

		_journalArticleService = journalArticleService;
	}
	
	protected void unsetJournalContentSearchLocalService(
			JournalContentSearchLocalService journalContentSearchLocalService) {

			_journalContentSearchLocalService = null;
	}
	
	@Reference
	protected void setJournalContentSearchLocalService(
		JournalContentSearchLocalService journalContentSearchLocalService) {

		_journalContentSearchLocalService = journalContentSearchLocalService;
	}
	
	@Reference
	protected void setJournalConverter(JournalConverter journalConverter) {
		_journalConverter = journalConverter;
	}
	
	protected void unsetJournalConverter(JournalConverter journalConverter) {
		_journalConverter = null;
	}
	@Reference
	private HttpUtil _httpUtil;
	private long globalSiteId;
	@Reference
	private Portal _portal;
	private JournalContentSearchLocalService _journalContentSearchLocalService;
	private JournalArticleService _journalArticleService;
	private static final Log _log = LogFactoryUtil.getLog(AddJournalArticleImpl.class);
	private JournalConverter _journalConverter;
	private DDMStructureLocalService _ddmStructureLocalService;
	private LayoutLocalService _layoutLocalService;
}