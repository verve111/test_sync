package  com.journalarticle.hook.action;

import java.util.ArrayList;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.collaboratelab.project.constants.Utils;
import com.collaboratelab.project.model.Project;
import com.collaboratelab.project.service.ProjectLocalServiceUtil;
import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.service.DLAppServiceUtil;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalArticleResource;
import com.liferay.journal.model.JournalFolder;
import com.liferay.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.journal.service.JournalArticleResourceLocalServiceUtil;
import com.liferay.journal.service.JournalFolderLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import com.liferay.portal.kernel.service.ClassNameLocalServiceUtil;
import com.liferay.portal.kernel.service.GroupLocalServiceUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.trash.kernel.model.TrashEntry;
import com.liferay.trash.kernel.service.TrashEntryLocalServiceUtil;
import com.liferay.trash.kernel.service.TrashEntryService;

/**
 * Custom command. There's not such in kernel.
 * 
 *
 */
@Component(immediate = true, property = { "javax.portlet.name=com_liferay_trash_web_portlet_TrashPortlet",
		"mvc.command.name=deleteEntries", "service.ranking:Integer=1700" }, service = MVCActionCommand.class)
public class TrashPortletDeleteEntriesCommandExt extends BaseMVCActionCommand {

	private long globalSiteId;
	
	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		
		long trashEntryId = ParamUtil.getLong(actionRequest, "trashEntryId");

		if (trashEntryId > 0) {
			TrashEntry trashEntry = TrashEntryLocalServiceUtil.getEntry(trashEntryId);
			if(trashEntry.getClassNameId() == ClassNameLocalServiceUtil.getClassNameId(JournalArticle.class)) {
				JournalArticleResource jar = JournalArticleResourceLocalServiceUtil.getArticleResource(trashEntry.getClassPK());
				
				JournalArticle model = JournalArticleLocalServiceUtil.fetchLatestArticle(jar.getResourcePrimKey());
				
				if (model != null && getGlobalSite() == model.getGroupId()) {
					deleteJournalArticle(model);
				}
			}
			else if(trashEntry.getClassNameId() == ClassNameLocalServiceUtil.getClassNameId(DLFileEntry.class)) {
				FileEntry fileEntry = DLAppServiceUtil.getFileEntry(trashEntry.getClassPK());
				
				deleteFileEntries(fileEntry);
				
			}
			
			_trashEntryService.deleteEntry(trashEntryId);

			return;
		}

		long[] deleteEntryIds = ParamUtil.getLongValues(
			actionRequest, "rowIds");

		if (deleteEntryIds.length > 0) {
			for (int i = 0; i < deleteEntryIds.length; i++) {
				TrashEntry trashEntry = TrashEntryLocalServiceUtil.getEntry(deleteEntryIds[i]);
				if(trashEntry.getClassNameId() == ClassNameLocalServiceUtil.getClassNameId(JournalArticle.class)) {
					JournalArticleResource jar = JournalArticleResourceLocalServiceUtil.getArticleResource(trashEntry.getClassPK());
					
					JournalArticle model = JournalArticleLocalServiceUtil.fetchLatestArticle(jar.getResourcePrimKey());
					
					if (model != null && getGlobalSite() == model.getGroupId()) {
						deleteJournalArticle(model);
					}
				}
				else if(trashEntry.getClassNameId() == ClassNameLocalServiceUtil.getClassNameId(DLFileEntry.class)) {
					FileEntry fileEntry = DLAppServiceUtil.getFileEntry(trashEntry.getClassPK());
					
					deleteFileEntries(fileEntry);
					
				}
				
				_trashEntryService.deleteEntry(deleteEntryIds[i]);
			}

			return;
		}

		String className = ParamUtil.getString(actionRequest, "className");
		long classPK = ParamUtil.getLong(actionRequest, "classPK");

		if (Validator.isNotNull(className) && (classPK > 0)) {
			_trashEntryService.deleteEntry(className, classPK);
		}

		sendRedirect(actionRequest, actionResponse);
	}
	
	
	private void deleteFileEntries(FileEntry model) {
		
		List<Project> projects = ProjectLocalServiceUtil.getProjects(0, ProjectLocalServiceUtil.getProjectsCount());
		
		for(Project p : projects) {
			
			try {
				long grId = p.getGroupId();
				Group group = GroupLocalServiceUtil.fetchGroup(grId);
				if(group != null){	
					deleteFileEntry(model, p.getGroupId());
				}
			} catch (PortalException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
		}
		
	}
	
	private void deleteFileEntry(FileEntry fileEntry, long targetGroupId) throws PortalException {
		
		FileEntry childEntry = null;

		long targetSiteFolderId = Utils.getTargetSiteFolderId(fileEntry, targetGroupId, fileEntry.getUserId());
		
		List<FileEntry> fileEntries = DLAppServiceUtil.getFileEntries(targetGroupId, targetSiteFolderId);//getGroupFileEntries(targetGroupId, dLFileEntry.getUserId(), 0,DLAppServiceUtil.getGroupFileEntriesCount(targetGroupId,dLFileEntry.getUserId()));
		
		for(FileEntry ent : fileEntries) {
			if(Long.valueOf(String.valueOf(ent.getExpandoBridge().getAttribute("parentEntry"))).equals(new Long(fileEntry.getFileEntryId()))){
				childEntry  = ent;
			}
		}
		
		if (childEntry != null) {
			DLAppServiceUtil.deleteFileEntry(childEntry.getFileEntryId());			
		}
	}
	
	private void deleteJournalArticle(JournalArticle model) {
		List<Project> projects = ProjectLocalServiceUtil.getProjects(0, ProjectLocalServiceUtil.getProjectsCount());
		for(Project p : projects){
			JournalFolder folder = JournalFolderLocalServiceUtil.fetchFolder(p.getGroupId(), 0, "Populated from global");
			if (folder == null) {
				continue;
			}
			List<JournalArticle> journals = JournalArticleLocalServiceUtil.getArticles(p.getGroupId(), folder.getFolderId());
			List<JournalArticle> res = new ArrayList<JournalArticle>();
			for(JournalArticle ja: journals) {
				if(ja.getExpandoBridge().hasAttribute("parentEntry")) {
					if((ja.getExpandoBridge().getAttribute("parentEntry")).equals(model.getExpandoBridge().getAttribute("parentEntry")))
					{
						res.add(ja);
					}
				}
			}
			
			for (JournalArticle childArticle : res) {
				try {
					JournalArticleLocalServiceUtil.deleteArticle(childArticle);
				} catch (PortalException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
	private long getGlobalSite() {
		if (globalSiteId == 0) {
			long companyId = CompanyThreadLocal.getCompanyId();
			globalSiteId = Utils.getGlobalGroupId(companyId);
		}
		return globalSiteId;
	}

	@Reference(unbind = "-")
	protected void setTrashEntryService(TrashEntryService trashEntryService) {
		_trashEntryService = trashEntryService;
	}

	private TrashEntryService _trashEntryService;

	
}
