<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@ include file="/init.jsp" %>
<%@ page import="com.liferay.document.library.kernel.model.DLFileEntry"%>
<%@ page import="com.liferay.journal.model.JournalArticleResource"%>
<%@ page import="com.liferay.journal.service.JournalArticleResourceLocalServiceUtil"%>
<%@ page import="com.liferay.journal.model.JournalArticle"%>
<%@ page import="com.liferay.journal.service.JournalArticleLocalServiceUtil"%>
<%@ page import="com.liferay.portal.kernel.xml.Document"%>
<%@ page import="com.liferay.portal.kernel.xml.Node"%>
<%@ page import="com.liferay.portal.kernel.xml.SAXReaderUtil"%>
<%@ page import="java.util.Locale"%>
<%@ page import="com.collaboratelab.project.constants.Utils"%>


<%
	AssetEntry assetEntry = (AssetEntry) request.getAttribute("view.jsp-assetEntry");
	AssetRendererFactory<?> assetRendererFactory = (AssetRendererFactory<?>) request
			.getAttribute("view.jsp-assetRendererFactory");
	AssetRenderer<?> assetRenderer = (AssetRenderer<?>) request.getAttribute("view.jsp-assetRenderer");

	request.setAttribute("view.jsp-showIconLabel", true);

	String title = (String) request.getAttribute("view.jsp-title");

	if (Validator.isNull(title)) {
		title = assetRenderer.getTitle(locale);
	}

	boolean viewInContext = ((Boolean) request.getAttribute("view.jsp-viewInContext")).booleanValue();

	String viewURL = AssetPublisherHelper.getAssetViewURL(liferayPortletRequest, liferayPortletResponse,
			assetRenderer, assetEntry, viewInContext);
	
	PortletURL exportAssetURL2 = null;
	String exportAssetURLStr = new String();
	boolean isDLFile = false;

	exportAssetURL2 = assetRenderer.getURLExport(liferayPortletRequest, liferayPortletResponse);
	exportAssetURL2.setParameter("plid", String.valueOf(themeDisplay.getPlid()));
	exportAssetURL2.setParameter("portletResource", portletDisplay.getId());
	exportAssetURL2.setWindowState(LiferayWindowState.EXCLUSIVE);
	exportAssetURL2.setParameter("targetExtension", "PDF");

	boolean enableRatings = GetterUtil.getBoolean(portletPreferences.getValue("enableRatings", null));
	
	String type = "web";
	String externalLink = "";
	
	if (assetEntry.getMimeType().equals("application/pdf")) {
		type = "pdf";
	}
	if (assetEntry.getMimeType().equals("image/jpeg") || assetEntry.getMimeType().equals("image/png") || assetEntry.getMimeType().equals("image/gif")) {
		type = "pic";
	}
	else if (assetEntry.getMimeType().equals("application/msword") || assetEntry.getMimeType()
			.equals("application/vnd.openxmlformats-officedocument.wordprocessingml.document")) {
		type = "word";
	} else if (assetEntry.getMimeType().equals("application/msexcel") || assetEntry.getMimeType()
			.equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
		type = "excel";
	} else if (assetEntry.getMimeType()
			.equals("application/vnd.openxmlformats-officedocument.presentationml.presentation")) {
		type = "presentation";
	} else if (assetEntry.getMimeType().equals("text/html")) {
		JournalArticleResource journalArticleResourceObj = JournalArticleResourceLocalServiceUtil
				.getJournalArticleResource(assetEntry.getClassPK());
		JournalArticle journalArticleObj = JournalArticleLocalServiceUtil
				.getArticle(themeDisplay.getScopeGroupId(), journalArticleResourceObj.getArticleId());
		if (journalArticleObj.getDDMTemplateKey().equals(Utils.getTemplateByName(Utils.getGlobalGroupId(themeDisplay.getCompanyId()), "URL"))) {
			type = "url";
			Document document = SAXReaderUtil
					.read(journalArticleObj.getContentByLocale(Locale.ENGLISH.toString()));
			Node brandNode = document
					.selectSingleNode("/root/dynamic-element[@name='WebAddress']/dynamic-content");
			if (Validator.isNotNull(brandNode)) {
				externalLink = brandNode.getText();
			}
		} else if (journalArticleObj.getDDMTemplateKey().equals(Utils.getTemplateByName(Utils.getGlobalGroupId(themeDisplay.getCompanyId()), "Video"))) {
			type = "video";
			Document document = SAXReaderUtil
					.read(journalArticleObj.getContentByLocale(Locale.ENGLISH.toString()));
			Node brandNode = document
					.selectSingleNode("/root/dynamic-element[@name='YoutubeLink']/dynamic-content");
			if (Validator.isNotNull(brandNode)) {
				externalLink = brandNode.getText();
			}			
		}
	}

%>

<div
	class="asset-abstract <%=AssetUtil.isDefaultAssetPublisher(layout, portletDisplay.getId(),
					assetPublisherDisplayContext.getPortletResource()) ? "default-asset-publisher" : StringPool.BLANK%>">
	<liferay-util:include page="/asset_actions.jsp"
		servletContext="<%=application%>" />

	<span class="asset-anchor lfr-asset-anchor"
		id="<%=assetEntry.getEntryId()%>"></span>
	<div class="row">
		<div class="col-md-1 pull-left">
			<%
				if (type.equals("pdf")) {
			%>
			<i class="far fa-file-pdf"></i>
			<%
				} else if (type.equals("excel")) {
			%>
				<i class="fas fa-table"></i>
			<%
				} else if (type.equals("word")) {
			%>
			<i class="far fa-file-word"></i>
			<%
				} else if (type.equals("presentation")) {
			%>
			<i class="far fa-file-powerpoint"></i>
			<%
				} else if (type.equals("web")) {
			%>
			<i class="fas fa-external-link-square-alt"></i>
			<%
				} else if (type.equals("url")) {
			%>
			<i class="fas fa-globe"></i>
			<%
				} else if (type.equals("video")) {
			%>
			<i class="fas fa-video"></i>
			<%
				}
			%>
		</div>
		<div class="col-md-6 pull-left">
			<h4 class="asset-title">
			<%
				
				String viewURLButton, resUrl = "";

				if (type.equals("pdf") || type.equals("pic")) {
					viewURLButton = assetRenderer.getURLDownload(themeDisplay);
					resUrl = viewURLButton;
				} else if(type.equals("url") || type.equals("video")) {
					viewURLButton = externalLink;
				} else {
					viewURLButton = exportAssetURL2.toString();
					resUrl = (type.equals("word") || type.equals("excel") || type.equals("presentation")) ? assetRenderer.getURLDownload(themeDisplay) : viewURLButton;
				}
			%>			
			
				<c:if test="<%=Validator.isNotNull(viewURL)%>">
					<a target="_blank"  
						href="<%= (type.equals("url") || type.equals("video")) ? externalLink: viewURLButton%>">
				</c:if>
					
				<%=HtmlUtil.escape(title)%>

				<c:if test="<%=Validator.isNotNull(viewURL)%>">
					</a>
				</c:if>
			</h4>
			<div class="asset-content">
				<div class="asset-summary">
					<liferay-ui:asset-display
						abstractLength="<%=assetPublisherDisplayContext.getAbstractLength()%>"
						assetEntry="<%=assetEntry%>" assetRenderer="<%=assetRenderer%>"
						assetRendererFactory="<%=assetRendererFactory%>"
						template="<%=AssetRenderer.TEMPLATE_ABSTRACT%>"
						viewURL="<%=viewURL%>" />
				</div>
			</div>

			<liferay-ui:asset-metadata className="<%=assetEntry.getClassName()%>"
				classPK="<%=assetEntry.getClassPK()%>" filterByMetadata="<%=true%>"
				metadataFields="<%=assetPublisherDisplayContext.getMetadataFields()%>" />
		</div>
		<div class="col-md-4 pull-right">
			<div class="row" style="width: 90%;">
				<a class="btn btn-primary" style="color: #fff; background: #29353d;"
					target="_blank" href="<%= viewURLButton%>"
					role="button">view</a> 
				<% if (!type.equals("video") && !type.equals("url")) { %>
				<a class="btn btn-primary"
					style="color: #fff; background: #29353d;"
					href="<%= resUrl %>" download role="button">download</a>
				<% } %>
			</div>
		</div>
	</div>
	
    <%if (enableRatings) {%>
    <div class="row" style="margin-top: 10px">
        <div class="col-md-11 col-md-offset-1">
            <div class="asset-ratings">
                <liferay-ui:ratings className="<%= assetEntry.getClassName() %>"
                                    classPK="<%= assetEntry.getClassPK() %>"/>
            </div>
        </div>
    </div>
    <%}%>
	
</div>