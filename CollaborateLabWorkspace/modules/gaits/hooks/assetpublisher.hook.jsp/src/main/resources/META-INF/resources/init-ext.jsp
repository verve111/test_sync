<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">

<%
	String catIdToRender = (String) portletSession.getAttribute("catIdToRender",
			javax.portlet.PortletSession.APPLICATION_SCOPE);
	if (catIdToRender != null && !catIdToRender.isEmpty()) {
		portletPreferences.setValue("queryName0", "assetCategories");
		portletPreferences.setValue("queryContains0", "true");
		portletPreferences.setValue("queryValues0", catIdToRender);
		portletPreferences.setValue("extensions", "pdf");
	}
	assetPublisherDisplayContext = new AssetPublisherDisplayContext(assetPublisherCustomizer,
			liferayPortletRequest, liferayPortletResponse, portletPreferences);
%>

<style>
.portlet-asset-publisher .asset-abstract {
	overflow-x: hidden;
}

.btn-primary, .btn-primary:hover, .btn-primary:active,  .btn-primary:focus, .btn-primary:active:hover, .btn-primary.active:focus, .btn-primary.active:hover  {
	font-wight: bold;
	color: white;
	border-color: #29353d;
}

.btn {
	border-width: 2px;
	padding: 8px 24px;
	font-size: 14px;
	font-weight: bold;
	line-height: 1.42857;
	border-radius: 0px;
}
.lfr-asset-anchor:before {
    margin: 0px 0 0;
}
.far, .fal, .fas {
    font-size: 45px;
    color: #999;
    margin-top: 15px;
  }
</style>

